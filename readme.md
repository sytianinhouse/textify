

## Code review (12-23-2020)

- sms sending naka for loop ung http isa-isa kasi wala pang batch ang promotexter
--> suggest gawing queue kaso wala ung current hosting, pweding 10 jobs nalang per minute (fifo)
- transfer of jobs from tomorrow to today hindi dapat pa isa-isa kasi posible na mag fail pag madami
sql queries (fail due to timeout, gateway error, etc.)


// after database local import
UPDATE `mysql`.`proc` p SET definer = 'root@localhost' WHERE definer='asdfgsnlkffa@localhost' AND db='textify_20230405';

update `job_after_tomorrow_to_14th_day` set gateway_key = 2 WHERE client_id = 61;
update `job_next_month` set gateway_key = 2 WHERE client_id = 61;
update `job_today` set gateway_key = 2 WHERE client_id = 61;
update `jobs_failed` set gateway_key = 2 WHERE client_id = 61;
update `job_15th_day_to_end_month` set gateway_key = 2 WHERE client_id = 61;
update `job_tomorrow` set gateway_key = 2 WHERE client_id = 61;


select * from job_after_tomorrow_to_14th_day where client_id = 61 and gateway_key = 3;
select * from job_next_month where client_id = 61 and gateway_key = 3;
select * from job_today where client_id = 61 and gateway_key = 3;
select * from jobs_failed where client_id = 61 and gateway_key = 3;
select * from job_15th_day_to_end_month where client_id = 61 and gateway_key = 3;
select * from job_tomorrow where client_id = 61 and gateway_key = 3;


select * from customers




MIGRATION SCRIPTS


update script

update customers set client_id = 65 where client_id = 61;
update customers set subaccount_id = 102 where subaccount = 98;

update `job_after_tomorrow_to_14th_day` set client_id = 65 WHERE client_id = 61;
update `job_next_month` set client_id = 65 WHERE client_id = 61;
update `job_today` set client_id = 65 WHERE client_id = 61;
update `jobs_failed` set client_id = 65 WHERE client_id = 61;
update `job_15th_day_to_end_month` set client_id = 65 WHERE client_id = 61;
update `job_tomorrow` set client_id = 65 WHERE client_id = 61;
update `jobs_completed` set client_id = 65 WHERE client_id = 61;

update `job_after_tomorrow_to_14th_day` set subaccount_id = 102 WHERE subaccount_id = 98;
update `job_next_month` set subaccount_id = 102 WHERE subaccount_id = 98;
update `job_today` set subaccount_id = 102 WHERE subaccount_id = 98;
update `jobs_failed` set subaccount_id = 102 WHERE subaccount_id = 98;
update `job_15th_day_to_end_month` set subaccount_id = 102 WHERE subaccount_id = 98;
update `job_tomorrow` set subaccount_id = 102 WHERE subaccount_id = 98;
update `jobs_completed` set subaccount_id = 102 WHERE subaccount_id = 98;

update task set client_id = -65 where client_id = 65;
update task set subaccount_id = -102 where subaccount_id = 102;

update task set client_id = 65 where id in (1355, 1356, 1357, 1358, 1359, 1360, 1361, 1362, 1363, 1376, 1377, 1378, 1379);
update task set subaccount_id = 102 where id in (1355, 1356, 1357, 1358, 1359, 1360, 1361, 1362, 1363, 1376, 1377, 1378, 1379);

revert script

update customers set client_id = 61 where client_id = 65;
update customers set subaccount_id = 98 where subaccount = 102;

update `job_after_tomorrow_to_14th_day` set client_id = 61 WHERE client_id = 65;
update `job_next_month` set client_id = 61 WHERE client_id = 65;
update `job_today` set client_id = 61 WHERE client_id = 65;
update `jobs_failed` set client_id = 61 WHERE client_id = 65;
update `job_15th_day_to_end_month` set client_id = 61 WHERE client_id = 65;
update `job_tomorrow` set client_id = 61 WHERE client_id = 65;
update `jobs_completed` set client_id = 61 WHERE client_id = 65;

update `job_after_tomorrow_to_14th_day` set subaccount_id = 98 WHERE subaccount_id = 102;
update `job_next_month` set subaccount_id = 98 WHERE subaccount_id = 102;
update `job_today` set subaccount_id = 98 WHERE subaccount_id = 102;
update `jobs_failed` set subaccount_id = 98 WHERE subaccount_id = 102;
update `job_15th_day_to_end_month` set subaccount_id = 98 WHERE subaccount_id = 102;
update `job_tomorrow` set subaccount_id = 98 WHERE subaccount_id = 102;
update `jobs_completed` set subaccount_id = 98 WHERE subaccount_id = 102;

update task set client_id = 65 where client_id = -65;
update task set subaccount_id = 102 where subaccount_id = -102;

update task set client_id = 61 where id in (1355, 1356, 1357, 1358, 1359, 1360, 1361, 1362, 1363, 1376, 1377, 1378, 1379);
update task set subaccount_id = 98 where id in (1355, 1356, 1357, 1358, 1359, 1360, 1361, 1362, 1363, 1376, 1377, 1378, 1379);


transport script

INSERT INTO job_today(
    customer_id,
    customer_name,
    customer_number,
    gateway_key,
    campaign_id,
    credits_consumed,
    when_to_send,
    message,
    task_id,
    parent_task_id,
    subaccount_id,
    client_id,
    entry_id,
    zs_booking_id,
    SOURCE
)
SELECT
    customer_id,
    customer_name,
    customer_number,
    gateway_key,
    campaign_id,
    credits_consumed,
    when_to_send,
    message,
    task_id,
    parent_task_id,
    subaccount_id,
    client_id,
    entry_id,
    zs_booking_id,
    SOURCE
FROM
    job_after_tomorrow_to_14th_day
WHERE
    DATE(when_to_send) IN(
        '2024-07-31',
        '2024-08-01',
        '2024-08-02',
        '2024-08-03',
        '2024-08-04',
        '2024-08-05',
        '2024-08-06',
        '2024-08-07',
        '2024-08-08',
        '2024-08-09',
        '2024-08-10',
        '2024-08-11',
        '2024-08-12'
    );

DELETE
FROM
    job_after_tomorrow_to_14th_day
WHERE
    DATE(when_to_send) IN(
        '2024-07-31',
        '2024-08-01',
        '2024-08-02',
        '2024-08-03',
        '2024-08-04',
        '2024-08-05',
        '2024-08-06',
        '2024-08-07',
        '2024-08-08',
        '2024-08-09',
        '2024-08-10',
        '2024-08-11',
        '2024-08-12'
    );


INSERT INTO job_today(
    customer_id,
    customer_name,
    customer_number,
    gateway_key,
    campaign_id,
    credits_consumed,
    when_to_send,
    message,
    task_id,
    parent_task_id,
    subaccount_id,
    client_id,
    entry_id,
    zs_booking_id,
    SOURCE
)
SELECT
    customer_id,
    customer_name,
    customer_number,
    gateway_key,
    campaign_id,
    credits_consumed,
    when_to_send,
    message,
    task_id,
    parent_task_id,
    subaccount_id,
    client_id,
    entry_id,
    zs_booking_id,
    SOURCE
FROM
    job_15th_day_to_end_month
WHERE
    DATE(when_to_send) IN(
        '2024-07-31',
        '2024-08-01',
        '2024-08-02',
        '2024-08-03',
        '2024-08-04',
        '2024-08-05',
        '2024-08-06',
        '2024-08-07',
        '2024-08-08',
        '2024-08-09',
        '2024-08-10',
        '2024-08-11',
        '2024-08-12'
    );

DELETE
FROM
    job_15th_day_to_end_month
WHERE
    DATE(when_to_send) IN(
        '2024-07-31',
        '2024-08-01',
        '2024-08-02',
        '2024-08-03',
        '2024-08-04',
        '2024-08-05',
        '2024-08-06',
        '2024-08-07',
        '2024-08-08',
        '2024-08-09',
        '2024-08-10',
        '2024-08-11',
        '2024-08-12'
    );

INSERT INTO job_today(
    customer_id,
    customer_name,
    customer_number,
    gateway_key,
    campaign_id,
    credits_consumed,
    when_to_send,
    message,
    task_id,
    parent_task_id,
    subaccount_id,
    client_id,
    entry_id,
    zs_booking_id,
    SOURCE
)
SELECT
    customer_id,
    customer_name,
    customer_number,
    gateway_key,
    campaign_id,
    credits_consumed,
    when_to_send,
    message,
    task_id,
    parent_task_id,
    subaccount_id,
    client_id,
    entry_id,
    zs_booking_id,
    SOURCE
FROM
    job_tomorrow
WHERE
    DATE(when_to_send) IN(
        '2024-07-31',
        '2024-08-01',
        '2024-08-02',
        '2024-08-03',
        '2024-08-04',
        '2024-08-05',
        '2024-08-06',
        '2024-08-07',
        '2024-08-08',
        '2024-08-09',
        '2024-08-10',
        '2024-08-11',
        '2024-08-12'
    );

DELETE
FROM
    job_tomorrow
WHERE
    DATE(when_to_send) IN(
        '2024-07-31',
        '2024-08-01',
        '2024-08-02',
        '2024-08-03',
        '2024-08-04',
        '2024-08-05',
        '2024-08-06',
        '2024-08-07',
        '2024-08-08',
        '2024-08-09',
        '2024-08-10',
        '2024-08-11',
        '2024-08-12'
    );

    ------------------------ resend failed sms ---------------------------------

INSERT INTO job_today(
    customer_id,
    customer_name,
    customer_number,
    gateway_key,
    campaign_id,
    credits_consumed,
    when_to_send,
    message,
    task_id,
    parent_task_id,
    subaccount_id,
    client_id,
    entry_id,
    zs_booking_id,
    SOURCE
)
SELECT
    customer_id,
    customer_name,
    customer_number,
    gateway_key,
    campaign_id,
    credits_consumed,
    time_attempt_send,
    message,
    task_id,
    parent_task_id,
    subaccount_id,
    client_id,
    entry_id,
    zs_booking_id,
    SOURCE
FROM
    jobs_failed
WHERE
    DATE(time_attempt_send) IN( '2024-08-01',) 
    DATE(time_attempt_send) IN( '2024-08-01'2, 
    DATE(time_attempt_send) IN( '2024-08-01'3 ,
    DATE(time_attempt_send) IN( '2024-08-01'4 ,
    DATE(time_attempt_send) IN( '2024-08-01'5 
    AND subaccount_id in (113, 121);

delete from jobs_failed
WHERE
    DATE(time_attempt_send) IN( '2024-08-01',) 
    DATE(time_attempt_send) IN( '2024-08-01'2, 
    DATE(time_attempt_send) IN( '2024-08-01'3 ,
    DATE(time_attempt_send) IN( '2024-08-01'4 ,
    DATE(time_attempt_send) IN( '2024-08-01'5 
    AND subaccount_id in (113, 121);



    logger('SMS NUMBER - ' . $number);
    logger('CLIENT ID - ' . $subAccount->client_id);

    $smartSunDitoClients = [70, 1]; // beechoo
    if (in_array($subAccount->client_id, $smartSunDitoClients)) {
        $invalidPrefixes = invalidNumberPrefixes();
        $sanitized = str_replace('+', '', $number);
        // $jobNumPrefix = substr($jobs->customer_number, 0, 5);
        $jobNumPrefix = substr($sanitized, 0, 5);

        logger('JOB NUMBER PREFIX');
        logger($jobNumPrefix);
        logger('IS NUMBER INVALID');
        logger(in_array($jobNumPrefix, $invalidPrefixes));

        if (in_array($jobNumPrefix, $invalidPrefixes)) {
            $cost = $cost * 9;
        }
    }

update job_tomorrow set task_id = concat('-', job_tomorrow.task_id) WHERE client_id = 70 and date(when_to_send) < '2023-10-02';
update job_next_month set task_id = concat('-', job_next_month.task_id) WHERE client_id = 70 and date(when_to_send) < '2023-10-02';
update job_after_tomorrow_to_14th_day set task_id = concat('-', job_after_tomorrow_to_14th_day.task_id) WHERE client_id = 70 and date(when_to_send) < '2023-10-02';
update job_15th_day_to_end_month set task_id = concat('-', job_15th_day_to_end_month.task_id) WHERE client_id = 70 and date(when_to_send) < '2023-10-02';

SELECT
    GROUP_CONCAT(id),
    GROUP_CONCAT(when_to_send) AS sending_date,
    GROUP_CONCAT(task_id) AS task_ids,
    GROUP_CONCAT(customer_number) AS num
FROM
    `job_after_tomorrow_to_14th_day`
WHERE
    DATE(when_to_send) > '2023-10-27'
AND client_id = 70
GROUP BY
    customer_number
ORDER BY
    count(when_to_send)
DESC
limit 500
    ;



https://prnt.sc/ZN8hM_Tr8qB3
https://prnt.sc/0pImA24cjZaq
https://prnt.sc/hcbKO_u9-MR1
https://prnt.sc/IyCsPjXjKwQj
https://prnt.sc/9VUOWFi9jOZB
https://prnt.sc/raKs7pUgGoff