<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        'App\Console\Commands\CronJobsNextMonth',
        'App\Console\Commands\CronJobs15thDaytoEndMonth',
        'App\Console\Commands\CronJobsTomorrowTo14thDay',
        'App\Console\Commands\CronJobsTomorrow',
        'App\Console\Commands\CronJobsToday',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Log::info('Did run the windows scheduler');

		$schedule->command('jobs:today')
		          ->everyMinute();

		/*$schedule->command('jobs:today-check')
			->daily()->at('08:00');*/

		// $schedule->command('jobs:tomorrow')
        //          ->hourlyAt(17);

		$schedule->command('jobs:tomorrowto14thday')
                 ->dailyAt('02:00');

		$schedule->command('jobs:15thdaytoendmonth')
                 ->dailyAt('03:30');

		// $schedule->command('jobs:cron-half-month')
  //               ->weekly()->sundays()->at('10:00');

		$schedule->command('jobs:nextmonth')
		        //  ->weekly()->sundays()->at('12:00');
		         ->dailyAt('12:00');

		$schedule->command('jobs:birthday')
		        //  ->daily()->at('02:30');
		        //  ->dailyAt('02:30');
		        //  ->dailyAt('10:30');
		         ->dailyAt('13:30');

		// $schedule->command('credit:alert')
        // ->daily()->at('08:00');
        // ->dailyAt('10:00');

		$schedule->command('load:auto')
		         ->monthly()->at('01:00');

		// Backup run command
		$schedule->command('backup:clean')->daily()->at('05:30');
		$schedule->command('backup:run')->daily()->at('11:00');

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
