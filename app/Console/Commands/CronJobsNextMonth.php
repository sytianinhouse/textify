<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class CronJobsNextMonth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:nextmonth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Jobs for Next Month is running';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Cron Next month!');

        $today = Carbon::now();
        event(new \App\Events\CronJobsNextMonth($today));
    }
}
