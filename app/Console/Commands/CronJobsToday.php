<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class CronJobsToday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:today';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Jobs for Today is running';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get all jobs today
		logger('Cron today worked!');

        $today = Carbon::now();
        event(new \App\Events\CronJobsToday($today));
        
    }
}
