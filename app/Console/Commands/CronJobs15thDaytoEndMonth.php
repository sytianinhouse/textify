<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class CronJobs15thDaytoEndMonth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:15thdaytoendmonth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Jobs for 15th day to end month is running';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Cron 15th day to end month!');

        $today = Carbon::now();
        event(new \App\Events\CronJobs15thDaytoEndMonth($today));
    }
}
