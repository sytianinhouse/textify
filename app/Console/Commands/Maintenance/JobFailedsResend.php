<?php

namespace App\Console\Commands\Maintenance;

use ATM\Repositories\Tasks\JobsFailed\JobsFailed;
use ATM\Repositories\Tasks\JobsFailed\JobsFailedRepository;
use ATM\Repositories\Tasks\Task;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class JobFailedsResend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:failed-resend {subAccountId} {taskId} {dateFrom?} {dateTo?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$subAccountId = $this->argument('subAccountId');
		$taskId = $this->argument('taskId');

		$startDate = new Carbon($this->argument('dateFrom'));
		$endDate = new Carbon($this->argument('dateTo'));

        $jobsFailed = JobsFailed::where('subaccount_id', $subAccountId)
			->where('task_id', $taskId)
			->where('time_attempt_send', '>=', $startDate->startOfDay())
			->where('time_attempt_send', '<=', $endDate->endOfDay())
			->get();

        $jobFailedRepo = resolve(JobsFailedRepository::class);
		$request = new \Illuminate\Http\Request();

		foreach ($jobsFailed as $job) {

			$request['number'] = $job->customer_number;
			$request['name']   = $job->customer_name ;
			$request['job_failed_id'] = $job->id;
			$request['from_job_failed'] = true;
			$request['from_command_job_failed'] = true;
			$request['message'] = $job->message;

			$jobFailedRepo->resendFailedJob($request, $job);
		}

		$jobsFailed = JobsFailed::where('subaccount_id', $subAccountId)
			->where('task_id', $taskId)->get();

		// DB::table('task')->where('id', $taskId)->update(['jobs_failed', $jobsFailed->count()]);
    }
}
