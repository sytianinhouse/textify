<?php

namespace App\Console\Commands\Maintenance;

use ATM\Repositories\ApiGateway\SytianSmsFacade;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class JobTodayFailChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:today-check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command use at the end of day to observe the jobs failed to run on a day and notify devs!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$now = Carbon::now()->subDay();
		$jobsToday = JobToday::whereNotNull('on_queue')->whereDate('when_to_send', '<=', $now->endOfDay()->toDateTimeString())->get();

		if ($jobsToday->count() > 0) {
			$sytianGateway = resolve(SytianSmsFacade::class);
			$numbers = ['09215410341','09399078704'];
			$message = "Hi Dev, this is to inform you that there is a jobs scheduled yesterday that failed to send an sms. Total count of jobs {$jobsToday->count()}";
			foreach ($numbers as $number) {
				$sytianGateway->prepareSms($number, $message);
			}
		}
    }
}
