<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class CronJobsTomorrow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:tomorrow';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Jobs for Tomorrow is running';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get all jobs tomorrow
        $this->info('Cron tomorrow worked!');

        $today = Carbon::now();
        event(new \App\Events\CronJobsTomorrow($today));
    }
}
