<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ResetJobTodayOnqueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:reset-onqueue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command used to reset the job today onqueue indicator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$jobsToday = DB::table('job_today')->where('on_queue', 1)->update(['on_queue' => null]);
    }
}
