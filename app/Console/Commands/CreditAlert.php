<?php

namespace App\Console\Commands;

use ATM\Repositories\ApiGateway\SytianSmsFacade;
use ATM\Repositories\ApiGateway\PromoTexterFacade;
use ATM\Repositories\Credits\Credit;
use ATM\Repositories\SubAccount\SubAccountSetting;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CreditAlert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'credit:alert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Send email notification if SubAccount's Credit is below the Credit Alert Level" ;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get all jobs today
		$this->info('Credit Alert worked!');

        $today = Carbon::now();
        
        $credits = Credit::with('subAccount.client')->get();
        $sytianGatewaySms = resolve(SytianSmsFacade::class);
        $promotexterGatewaySms = resolve(PromoTexterFacade::class);
        $senderId = 'Zensoft';
        return true;
        // Gagawing weekly to soon

        foreach ($credits as $credit) {
            $subAccount = $credit->subAccount;
            $client = $subAccount->client;
            $senderId = $client->project_id == 1 ? 'Zensoft' : 'Textify';

            if ($credit->pending_balance <= $subAccount->credit_alert_level) {
            	$pendingBalance = $credit->pending_balance;
            	$name = $subAccount->account_name;

            	// Sending SMS Notif
				$mobileRecipient = $subAccount->getSetting(SubAccountSetting::CREDIT_BALANCE_NOTIF_RECIPIENT, null);
				if ($mobileRecipient) {
					$exploded = explode(',', $mobileRecipient);
					foreach ($exploded as $number) {
						$number = formattedNumber($number, true, true);
						if ($number) {
							// $message = "Hi, a gentle reminder that you only have '{$pendingBalance}' remaining pending credits on your Textify account under '{$name}'. Please contact us at 0917-155-5881 to replenish your credits.";
							$message = "Hi, a gentle reminder that you only have '{$pendingBalance}' remaining pending credits on your Textify account under '{$name}'. Please contact us at 0908-812-7936 to replenish your credits.";
							// $smsResponse = $sytianGatewaySms->prepareSms( $number, $message );
                            $options = [];
							$smsResponse = $promotexterGatewaySms->makeSingleTransaction($number, $message, false, $senderId, $options);
						}
					}
				}

                event(new \App\Events\SendEmail($credit, $subAccount));
            }
        }
    }
}
