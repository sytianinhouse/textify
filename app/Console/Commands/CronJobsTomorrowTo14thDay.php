<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class CronJobsTomorrowTo14thDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:tomorrowto14thday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Jobs for Tomorrow to 14th day is running';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get all jobs in Tomorrow to 14th day
        $this->info('Cron Tomorrow to 14th day worked!');

        $today = Carbon::now();
        event(new \App\Events\CronJobsTomorrowTo14thDay($today));
    }
}
