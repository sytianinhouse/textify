<?php

namespace App\Console\Commands;

use ATM\Repositories\Loads\Load;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use ATM\Repositories\Loads\AutoLoad as ModelAutoLoad;

class AutoLoad extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:auto';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for auto top-up of loading';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$autoLoads = ModelAutoLoad::with(['client', 'subAccount'])->actives()->get();
    	Log::info("Auto top-up is running with total count of {$autoLoads->count()}");

    	foreach ($autoLoads as $autoLoad) {
    		$now = Carbon::now();
    		$subAccount = $autoLoad->subAccount;
			$client = $autoLoad->client;
			$amount = $autoLoad->amount;

			$load = new Load();
			$load->date = $now;
			$load->subaccount_id = $subAccount->id;
			$load->client_id = $client->id;
			$load->load_amount = $amount;
			$load->notes = 'Monthly automated top-up';
			$load->save();
		}
    }
}
