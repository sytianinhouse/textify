<?php

namespace App\Console\Commands;

use App\Events\BirthdaySMS;
use Carbon\Carbon;
use Illuminate\Console\Command;

class BirthdaySMSGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $now = (new Carbon())->startOfDay();
      $lastDayOfMonth = new Carbon('last day of this month');
      $secondOfLast = $lastDayOfMonth->copy()->subDay()->startOfDay();

      $lastDayOfNextMonth = new Carbon('last day of next month');
      $nextMonth = $lastDayOfNextMonth->copy()->startOfMonth();

      $monthNow = $now->copy()->startOfMonth();

      logger('BIRTHDAY CRON WORKS !!!');

      // if ($now->eq($secondOfLast)) { // comment out for manual sending
        logger('BIRTHDAY CRON TRIGGERED');
        // event(new \App\Events\BirthdaySMS($nextMonth)); // comment out for manual sending
       event(new \App\Events\BirthdaySMS($monthNow)); // uncomment out for manual sending
      // } // comment out for manual sending
    }
}
