<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Unicode implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (strlen($value) != strlen(utf8_decode($value))) {
            // echo 'is unicode';
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must not contain unicode characters.';
    }
}