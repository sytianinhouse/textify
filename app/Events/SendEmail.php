<?php

namespace App\Events;

use ATM\Repositories\Credits\Credit;
use ATM\Repositories\SubAccount\SubAccount;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendEmail
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $credit;
    public $subAccount;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Credit $credit, SubAccount $subAccount)
    {
        $this->credit = $credit;
        $this->subAccount = $subAccount;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
