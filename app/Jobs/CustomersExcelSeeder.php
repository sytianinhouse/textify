<?php

namespace App\Jobs;

use ATM\ContextInterface;
use ATM\Repositories\Client\ClientRepository;
use ATM\Repositories\Customers\ApiCustomersRepository;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\SubAccount\SubAccountRepository;
use Carbon\Carbon;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;
use Maatwebsite\Excel\Collections\RowCollection;

class CustomersExcelSeeder// implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(RowCollection $customers, $request)
    {
        $this->customers = $customers;
		$this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ClientRepository $clientRepo, SubAccountRepository $subAccountRepo)
    {
        $request = $this->request;
		DB::transaction(function () use ($clientRepo, $subAccountRepo, $request) {

            $customers = [];
			$clientId = $this->customers->first()->client_id;
			$client = $clientRepo->with(['subAccounts', 'customers'])->find($clientId);
			$apiCustomerRepository = resolve(ApiCustomersRepository::class);

			if (!$client) {
                $messageBag = new MessageBag();
				$messageBag->add('import_failed', 'Client not found!');
				throw new \ATM\Validation\ExcelException($messageBag);
			}

			$insertedMobile = [];
            $subaccounts = $client->subAccounts->keyBy('id');
            // dd($this->customers);
			foreach ($this->customers as $row) {
				if ($row->mobile != NULL && $row->mobile != '') {
					$subaccount = $subaccounts->firstWhere('id', $row->sub_account_id);
					if (!$subaccount) {
						continue;
						$context = resolve(ContextInterface::class);
						$subaccount = $context->getInstance();
					}

					if ($subaccount) {

						$mobile = formattedNumber($row->mobile, true);

						if (!$mobile) { continue; }
						if (in_array($mobile, $insertedMobile)){ continue; }

						$insertedMobile[] = $mobile;
						$customer = NULL;

						$guid = NULL;
						if ($row->entry_id) {
							$guid = $row->entry_id;
							if (!$request->has('no_query')) {
								/*$customer = $client->customers->first(function ($customer) use ($row, $guid) {
									return $customer->guid == $guid;
								});*/
								$customer = $client->customers()->where('guid', $guid)->first();
							}
						}

						if (!$request->has('no_query') && $customer == null) {
							// check first if exist na si customer
							/*$customer = $client->customers->first(function ($customer) use ($row, $mobile) {
								// return strtolower($customer->full_name) == strtolower($row->first_name . ' ' . $row->last_name);
								// Find via mobile number nalang
								return $customer->mobile == $mobile;
							});*/
							$customer = $subaccount->customers()->where('mobile', $mobile)->first();
						}

						if (property_exists($row, 'full_name') && $row->full_name) {
				            $names = explode(' ', $row->full_name);
				            $row->first_name = array_shift($names);
				            $row->last_name = implode(' ', $names);
				        }

						$customer = $customer ?: new Customer();
						$customer->guid = $guid;
						$customer->client_id = $client->id;
						$customer->subaccount_id = $subaccount->id;
						$customer->first_name = ucfirst($row->first_name);
						$customer->last_name = ucfirst($row->last_name);
						$customer->email = $row->email;
						$customer->mobile = $mobile;
						$customer->telephone = $row->telephone;
						$customer->gender = $row->gender;

						if ($row->birthdate) {
							$birthdate = new Carbon($row->birthdate);
							$customer->b_day = $birthdate->format('d');
							$customer->b_month = $birthdate->format('m');
							$customer->b_year = $birthdate->format('Y');
						}

						$customer->occupation = $row->occupation;
						$customer->address_1 = $row->address_1;
						$customer->address_2 = $row->address_2;
						$customer->postal_code = $row->postal_code;
						$customer->city = $apiCustomerRepository->identifyCity($row->city);
						$customer->country = $apiCustomerRepository->identifyCountry($row->country);

						// Do grouping of customers;
						// Coming soon!

                        $customer->forceSave();

						$customerGroups = $client->customerGroups;

						if ($row->group && $customerGroups->count() > 0) {

							$groups = explode(',', $row->group);

							$filteredGroups = [];

							foreach ($groups as $key => $group) {
								$sanitizedGroup = trim($group);

								$filtGroups = $customerGroups->filter(function($grp) use ($sanitizedGroup) {
									return strtolower($grp->name) == strtolower($sanitizedGroup);
								})->pluck('id')->toArray();

								$filteredGroups = array_merge($filteredGroups, $filtGroups);
							}

							// $customer->customerGroups()->sync($row->group);
							$customer->customerGroups()->sync($filteredGroups);
						}

					}
				}
			}
		});
    }
}
