<?php

namespace App\Model;

use Auth;
use Schema;
use Request;
use Carbon\Carbon;
// use App\Model\AuditLog;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class BaseModel extends Model {

    const DATE_FORMAT = 'd-M-y';

	public function getSetting($name, $default = null)
    {
        $value = Arr::get($this->settings, $name);

        if (is_array($value)) return $value;

        return (null !== $value) ? trim($value) : $default;
    }

    public function setSetting($key, $value, $persist = true)
    {
        if (isset($this->settings[$key])) {
            $settings = $this->settings;
            $settings[$key] = $value;
        } else {

            $settings = array_add($this->settings, $key, $value);
        }
        
        $this->settings = (array) $settings;
        if ($persist) {
            $this->save();
        }
    }

    public function getOtherData($name, $default = null)
    {
    	$value = Arr::get($this->other_data, $name);

    	if (is_array($value)) return $value;

    	return (null !== $value) ? trim($value) : $default;
    }

    public function setOtherData($key, $value, $persist = true)
    {
        if (isset($this->other_data[$key])) {
            $other_data = $this->other_data;
            $other_data[$key] = $value;
        } else {
            $other_data = array_add($this->other_data, $key, $value);
        }

        $this->other_data = (array) $other_data;
        if ($persist) { 
            $this->save();
        }
    }

    // public function addNewLog($action, $ip, $username)
    // {
    //     $log = AuditLog::addLog($action, $ip, $username, $this);
    // }

    public function getContext()
    {
        if (!$this->context) {
            throw new \Exception("Model Context not defined.");
        }
        return new $this->context;
    }

    public function formatFormDate($attr = null, $format = 'm/d/Y') 
    {
        $carbon = $this->{$attr};
        return ($carbon instanceof \Carbon\Carbon) ? $carbon->format($format) : null;
    }

    public function formatDate($attr = null, $format = self::DATE_FORMAT) 
    {
        $carbon = $this->{$attr};
        return ($carbon instanceof \Carbon\Carbon) ? $carbon->format($format) : null;
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            if (!Auth::user()) return;
            if (Schema::hasColumn($model->getTable(), 'created_by')) {
                $model->created_by = Auth::user()->id;
            }
        });
    }


}