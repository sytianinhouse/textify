<?php

namespace App\Providers;

use ATM\Context;
use ATM\ContextInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer('*', function($view) {
            $view->with('authUser', Auth::user());
            $view->with('isRegularUser', (Auth::user() ? Auth::user()->account_type != 'account_user' : false));
        });

        view()->composer(['client.*', 'layouts.partials.client-menu', 'partials.report-export.template'], function($view) {
            $view->with('authClient', Auth::user()->client);
            $view->with('theSubs', (Auth::user()->client->subAccounts()->actives()->get()));
        });

        view()->composer(['sub-account.*', 'layouts.partials.sub-account-menu'], function($view) {
            // $view->with('theSubs', (Auth::user()->client->subAccounts()->actives()->get()));
            $view->with('theSubs', (Auth::user()->subAccounts()->actives()->get()));
        });

        view()->composer('sub.*', function($view) {
            $context = app(ContextInterface::class);
            $view->with('subContext', $context->getInstance());
        });

        Blade::directive('dateFormat', function($expression) {
             return "<?php echo with($expression)->format('d-M-y'); ?>";
        });

		Blade::directive('humanDateTimeFormat', function($expression) {
			return "<?php echo with($expression)->format('M d, Y, h:i A'); ?>";
		});

		Blade::directive('humanDateTimeSecondsFormat', function($expression) {
            if($expression) {
                return "<?php echo with($expression)->format('M d, Y, h:i A'); ?>";
            }
		});

        Blade::directive('dateTimeFormat', function($expression) {
            return "<?php echo with($expression)->format('d-M-y h:i A'); ?>";
        });

        Blade::directive('timeFormat', function($expression) {
            return "<?php echo with($expression)->format('h:i A'); ?>";
        });

        Blade::directive('dateDayFormat', function($expression) {
            return "<?php echo with(new Carbon\Carbon($expression))->toFormattedDateString() . ' (' . with(new Carbon\Carbon($expression))->format('D') .')'; ?>";
        });

        Blade::directive('moneyAlt', function($expression) {
            return "<?php echo number_format($expression, 2); ?>";
        });

        Blade::directive('number', function($expression) {
            return "<?php echo number_format($expression, 2); ?>";
        });

		Blade::directive('numberComma', function($expression) {
			return "<?php echo number_format($expression, 0); ?>";
		});

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ContextInterface::class, Context::class);
    }
}
