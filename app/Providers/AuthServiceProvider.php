<?php

namespace App\Providers;

use ATM\Repositories\Permission\Permission;
use ATM\Repositories\Roles\Role;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('access', function ($user, $actions) {
            $arrActions = explode('|', $actions);
            $permissions = Permission::whereIn('key', $arrActions)->get();
            // maging false to pag finalized lahat ng permissions
            if (!$permissions) return false;

            if ($user->isSuperAdmin()) return true;

            if ($user->isAccountUser()) {
                if ($permissions instanceof Collection) {
                    return $permissions->first(function($permission) use ($user) {
                        return $user->isPermitted($permission);
                    });
                }

                if ($permissions->first()) {
                    return $user->isPermitted($permissions->first());
                }
            }

            return false;
        });
    }
}
