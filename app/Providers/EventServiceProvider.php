<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\CronJobsToday' => [
            'App\Listeners\CronJobsTodayListener',
        ],
        'App\Events\CronJobsTomorrow' => [
            'App\Listeners\CronJobsTomorrowListener',
        ],
        'App\Events\CronJobsTomorrowTo14thDay' => [
            'App\Listeners\CronJobsTomorrowTo14thDayListener',
        ],
        'App\Events\CronJobs15thDaytoEndMonth' => [
            'App\Listeners\CronJobs15thDaytoEndMonthListener',
        ],
        'App\Events\CronJobsNextMonth' => [
            'App\Listeners\CronJobsNextMonthListener',
        ],
		'App\Events\CronJobsHalfOfMonth' => [
			'App\Listeners\CronJobsHalfOfMonthListener',
		],
		'App\Events\BirthdaySMS' => [
			'App\Listeners\BirthdaySMSListener',
		],
        'App\Events\SendEmail' => [
            'App\Listeners\SendEmailListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
