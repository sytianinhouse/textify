<?php
/**
 * Created by PhpStorm.
 * User: jr
 * Date: 4/24/2019
 * Time: 1:04 PM
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Pagination\LengthAwarePaginator;

class ApiController extends Controller
{
	protected function errorResponse($errors = [])
	{
		if ($errors instanceof Arrayable) {
			$errors = $errors->toArray();
		}
		$messages = is_array($errors) ? $errors : [$errors];

		return response()->json(array('messages' => $messages), 400);
	}

	protected function successResponse($msg)
	{
		return response()->json(array('message' => $msg));
	}

	protected function emptyResponse()
	{
		return response()->json(array());
	}

	public function paginateCollection($results, $currentPage, $perPage, $opt = null)
    {
        $temp = $results;
        $currentIndex = (($currentPage == 1 || $currentPage == 0) ? 0 : $currentPage - 1) * $perPage;
        $slice = $temp->slice($currentIndex, $perPage);

        $opt = ($opt == null) ? ['path' => ''] : $opt;

        return new LengthAwarePaginator($slice, count($results), $perPage, $currentPage, $opt);
    }
}