<?php

namespace App\Http\Controllers\Api\Customer;

use ATM\Repositories\Cities\City;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\CustomerGroups\CustomerGroupsRepository;
use ATM\Repositories\Customers\ApiCustomersRepository;
use ATM\Repositories\Customers\CustomersQueryRepository;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Date\Date;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Tasks\TaskRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CustomerController extends ApiController
{
	public function __construct(
        CustomerGroupsRepository $customerGroupRepo,
		ApiCustomersRepository $apiCustomerRepo,
        SubAccountRepository $subAccountRepo,
        CustomersQueryRepository $customersQueryRepo,
        Date $date
	)
	{
        $this->customerGroupRepo = $customerGroupRepo;
		$this->apiCustomerRepo = $apiCustomerRepo;
        $this->subAccountRepo = $subAccountRepo;
        $this->customersQueryRepo = $customersQueryRepo;
        $this->dateRepo = $date;
	}

	/**
	 * Important params is the Security Key - Way to identify what sub account and client this customers is
	 * Customers table has columnd 'guid' - which compose of {ClientID-SubAccountID-EntryID]
	 * Params for customer fetching from a task API
	 * entry_id As unique ID from db of client software
	 * @param Request $request
	 */
	public function updateRecord(Request $request)
	{
		try {
			$customer = $this->apiCustomerRepo->updateCustomer($request);

			if ($customer) {
				return response()->json(['success' => true, 'message' => 'Customer updated/created.']);
			}

		} catch (ValidationException $e) {
			return response()->json(['success' => true, 'message' => 'Customer updated/created.']);
		}
    }


    public function getRecipientList( Request $request ) {
        $request['per_page'] = 500;
        $request['subaccount_id'] = $request->subID;
        $request['sub_account_id'] = $request->subID;
		$request['client_id'] = $request->clientID;
        // $customers     = $this->customersQueryRepo->getRecipientsListFormatted($request, true,true, true);
        $customers     = $this->customersQueryRepo->getRecipientsListFormatted($request, true,true, false);
        $customergroup = $this->customerGroupRepo->search($request, [], true)->pluck('name', 'id');

        $getCities = City::get();

        $months = [];
        $monthCount = Date::$months;
        foreach( $monthCount as $key => $month ) {
            $months[] = [
                'label' => $month,
                'value' => $key
            ];
        }

        $days = [];
        $dayCount = $this->dateRepo->days();
        foreach( $dayCount as $day ) {
            $days[] = [
                'label' => $day,
                'value' => $day
            ];
        }

        $years = [];
        $yearCount = $this->dateRepo->years();
        foreach( $yearCount as $year ) {
            $years[] = [
                'label' => $year,
                'value' => $year
            ];
        }

        $cities = [];
        $getCities->each(function($value,$key) use (&$cities){
            $cities[] = [
                'label' => "{$value['name']}, {$value->province->name}",
                'value' => $value['id'],
            ];
        });

        $cGroup = [];
        $customergroup->each(function($item, $key) use (&$cGroup) {
            $cGroup[] = [
                'label' => $item,
                'value' => $key,
            ];
        });

        if ($customers) {
            return response()->json(['success' => true, 'customers' => $customers, 'customerGroup' => $cGroup, 'cities' => $cities, 'months' => $months,'days' => $days, 'years' => $years]);
        }
        return response()->json(['success' => false, 'customers' => false]);

    }

    public function loadMoreContact( Request $request )
    {
        $request['subaccount_id'] = $request->subID;
        $request['sub_account_id'] = $request->subID;
        // $customers = $this->customersQueryRepo->getRecipientsListFormatted($request, true,true, true);
    
        $perPage = 500; // $request->per_page;
        $skip = $request->offset * $perPage;
        $query = Customer::where('subaccount_id', $request->subID)
                    ->skip($skip)->take($perPage);
                    
        $customers = $query->get()->map(function($customer) {
            return [
                'label' => "{$customer->name} - {$customer->mobile}",
                'value' => $customer->id,
            ];
        });

        $loadMore = count($customers) > 0;
        return response()->json(['success' => $loadMore, 'customers' => $customers]);

    }

    public function generateContacts( Request $request )
	{
        $request["getOption"] = $request['data']['smart_filter_selected']['value'];
        $request["subaccount_id"] = $request['params']['subID'];
        $request["client_id"] = $request['params']['clientID'];
        $request["customer_group_id"] = $request['data']['customer_group'] ? $request['data']['customer_group']['value'] : null;
		$request["all_max_count"] = $request['data']['all_max_count'];
        $request["gender"] = $request['data']['gender'] ? $request['data']['gender']['value'] : null;
        $request["b_month"] = isset($request['data']['birthday'][0]['month']['value']) ? $request['data']['birthday'][0]['month']['value'] : null;
        $request["b_day"] = isset($request['data']['birthday'][0]['day']['value']) ? $request['data']['birthday'][0]['day']['value'] : null;
        $request["b_year"] = isset($request['data']['birthday'][0]['year']['value']) ? $request['data']['birthday'][0]['year']['value'] : null;
        $request["city"] = isset($request['data']['cities']['value']) ? $request['data']['cities']['value'] : null;
        $option = $request->getOption;
        if ( $option != 'select_option' ) {
			$request['not_empty_mobile'] = true;
            $customers = $this->customersQueryRepo->search($request, true);
			$customers = $customers->filter(function($item) {
				$number = formattedNumber($item->mobile, true);
				return $number ? true : false;
			});

			if ($request->filled('all_max_count')) {
				$customers = $customers->random($request->all_max_count);
			}

            $customersIds = $customers->implode('id', ',');
            return response()->json(['success' => true, 'customer_count' => $customers->count(), 'customer_ids' => $customersIds]);
        } else {
            return response()->json(['success' => false]);
        }
    }

    public function viewContacts( Request $request )
	{
        $request["getOption"] = $request['data']['smart_filter_selected']['value'];
        $request["subaccount_id"] = $request['params']['subID'];
        $request["client_id"] = $request['params']['clientID'];
        $request["customer_group_id"] = $request['data']['customer_group'] ? $request['data']['customer_group']['value'] : null;
        $request["gender"] = $request['data']['gender'] ? $request['data']['gender']['value'] : null;
        $request["b_month"] = $request['data']['birthday'][0]['month'] ? $request['data']['birthday'][0]['month']['value'] : null;
        $request["b_day"] = $request['data']['birthday'][0]['day'] ? $request['data']['birthday'][0]['day']['value'] : null;
        $request["b_year"] = $request['data']['birthday'][0]['year'] ? $request['data']['birthday'][0]['year']['value'] : null;
        $request["city"] = $request['data']['cities'] ? $request['data']['cities']['value'] : null;
        $option = $request->getOption;
        $customerIds = $request['params']['customerIds'];

        $request['not_empty_mobile'] = true;
        $request['per_page'] = 15;

        // if (isClientNailsGlow() || isClientLuxeEscape()) {
            $customers = $this->customersQueryRepo->search($request, true);
            if ($customerIds && count($customerIds) > 0) {
                $customers = $customers->filter(function($cust) use ($customerIds) { 
                    return in_array($cust->id, $customerIds);
                })->values();
            }

            $customers = $this->paginateCollection($customers, $request->page ?: 1, $request->per_page ?: 50);
        // } else {
        //     $customers = $this->customersQueryRepo->search($request, true, '*', [], true);
        // }
        
        if ( $option != 'select_option' ) {
            return CustomerResource::collection( $customers );
        } else {
            return response()->json(['success' => false]);
        }
    }

    public function newPage( Request $request )
	{
        $data = (object) json_decode(stripslashes($request['data']));

        $request["getOption"] = ($data->smart_filter_selected) ? $data->smart_filter_selected->value : '';
        $request["subaccount_id"] = $request['params']['subID'];
        $request["client_id"] = $request['params']['clientID'];
        $request["customer_group_id"] = ($data->customer_group) ? $data->customer_group->value : '';
        $request["gender"] = ($data->gender) ? $data->gender->value : '';
        $request["b_month"] = $data->birthday[0]->month;
        $request["b_day"] = $data->birthday[0]->day;
        $request["b_year"] = $data->birthday[0]->year;
        $request["city"] = ($data->cities) ? $data->cities->value : '';
        $option = $request->getOption;
        $customerIds = json_decode($request['customerIds']);

        $request['not_empty_mobile'] = true;
        $request['per_page'] = 15;
        // if (isClientNailsGlow() || isClientLuxeEscape()) {
            $customers = $this->customersQueryRepo->search($request, true);
            if ($customerIds && count($customerIds) > 0) {
                $customers = $customers->filter(function($cust) use ($customerIds) { 
                    return in_array($cust->id, $customerIds);
                })->values();
            }

            $customers = $this->paginateCollection($customers, $request->page ?: 1, $request->per_page ?: 50);
        // } else {
        //     $customers = $this->customersQueryRepo->search($request, true, '*', [], true);
        // }

        return CustomerResource::collection( $customers );
    }
}
