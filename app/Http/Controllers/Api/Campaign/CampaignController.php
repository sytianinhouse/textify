<?php

namespace App\Http\Controllers\Api\Campaign;

use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Client\Client;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CampaignController extends Controller
{
	public function __construct(
		CampaignRepository $campaignRepo,
		SubAccountRepository $subAccountRepo
	)
	{
		$this->campaignRepo = $campaignRepo;
		$this->subAccountRepo = $subAccountRepo;
	}

	public function create(Campaign $campaign, Request $request)
	{
        $sub_account_id = ( $request['sub_account_id'] ) ? $request['sub_account_id'] : $request->params['sub_account_id'];
        $subAccount = SubAccount::find($sub_account_id);
		$client = ( isset($subAccount) ) ? $subAccount->client : '';
		try {
            $request['client_id'] = $client->id;
			$request['sub_account_id'] = $sub_account_id;
			$request['name'] = ( $request->params ) ? $request->params['name'] : $request['name'];
            $campaignStore = $this->campaignRepo->store($campaign, $request);
			$campaign = $this->campaignRepo->search($request)->sortBy('id'); //->pluck('name', 'id')
			$lastCampaign = $campaign->last();
			$campaign = $campaign->pluck('name', 'id');
            if( isset( $request->params['type'] ) && $request->params['type'] == 'vue' ) {
                $formatedCampaign = [];
                $campaign->each(function($item, $key) use (&$formatedCampaign) {
                    $formatedCampaign[] = [
                        'label' => $item,
                        'value' => $key
                    ];
                });
                return response()->json(['success' => true, 'campaign' => $formatedCampaign]);
            } else {
                return view('partials.task.campaign-options', compact('campaign', 'lastCampaign'));
            }
		} catch (ValidationException $e) {
			return response()->json(['success' => false]);
		}
    }

    public function getCampaign(Request $request) {
        $formatedCampaign = [];
        $request['sub_account_id'] = $request->sub_account_id;
        $request['clientID'] = $request->clientID;
        $campaign = $this->campaignRepo->search($request)->sortBy('id');
        $campaign = $campaign->pluck('name', 'id');
        $sub = $this->subAccountRepo->search($request)->first();
        if( $campaign ) {
            $campaign->each(function($item, $key) use (&$formatedCampaign) {
                $formatedCampaign[] = [
                    'label' => $item,
                    'value' => $key
                ];
            });

            return response()->json([ 'success' => true, 'campaign' => $formatedCampaign, 'subAccountName' => $sub->account_name, 'subAccountId' => $sub->id ]);
        } else {
            return response()->json(['success' => false]);
        }
    }
}
