<?php

namespace App\Http\Controllers\Api\Task;

use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Client\Client;
use ATM\Repositories\CustomerGroups\CustomerGroupsRepository;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Project\Project;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\Tasks\OneTimeTaskRepository;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Tasks\TaskRepository;
use ATM\Repositories\Tasks\TaskType\TaskType;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class OneTimeSendController extends Controller
{
    public function __construct (
		CampaignRepository $campaignRepo,
		SubAccountRepository $subAccountRepo,
		CustomersRepository $customerRepo,
		CustomerGroupsRepository $customerGroupRepo,
        OneTimeTaskRepository $oneTimeRepo,
		TaskRepository $taskRepo,
        MessageBag $messageBag
	)
    {
    	$this->customerGroupRepo = $customerGroupRepo;
        $this->subAccountRepo = $subAccountRepo;
        $this->campaignRepo   = $campaignRepo;
        $this->customerRepo = $customerRepo;
        $this->taskRepo = $taskRepo;
        $this->oneTimeRepo = $oneTimeRepo;
        $this->messageBag = $messageBag;
    }

    public function createJob(Request $request)
    {
        try {
            $task = $request->app_key ? $this->taskRepo->searchByKey($request->app_key) : null;
            
            if ($task) {

                $result = $this->oneTimeRepo->addJob($request, $task);

                return response()->json(['success' => true, 'message' => $result]);
            } else {
                throw new \ATM\Validation\ValidationException($this->messageBag->add('fatal_error', 'Something went wrong. Please contact administrator.'));
            }

        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e, true);
        }
    }

	public function createJobBulk(Request $request)
	{
		try {
			$result = $this->oneTimeRepo->addJobs($request);

			return response()->json(['success' => true, 'message' => $result]);

		} catch (ValidationException $e) {
			return $this->redirectFormRequest($e, true);
		}
    }
}
