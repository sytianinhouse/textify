<?php

namespace App\Http\Controllers\Api\Task;

use ATM\ContextInterface;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Cities\City;
use ATM\Repositories\Client\Client;
use ATM\Repositories\CustomerGroups\CustomerGroupsRepository;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Shortcodes\Shortcodes;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\Tasks\BaseJob;
use ATM\Repositories\Tasks\BookingTaskRepository;
use ATM\Repositories\Tasks\OneTimeTaskRepository;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Tasks\TaskRepository;
use ATM\Repositories\Tasks\TaskType\TaskType;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use ATM\Repositories\Sms\SmsFacade;

use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsNextMonth\JobNextMonth;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;
use ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth;

class BookingTaskController extends Controller
{
    public function __construct (
        CampaignRepository $campaignRepo,
        SubAccountRepository $subAccountRepo,
        CustomersRepository $customerRepo,
        CustomerGroupsRepository $customerGroupRepo,
        OneTimeTaskRepository $oneTimeTaskRepo,
        TaskRepository $taskRepo,
        MessageBag $messageBag,
        BookingTaskRepository $bookingTaskRepo,
		BaseJob $baseJob,
		ContextInterface $context,
        SmsFacade $smsfacade
    )
    {
        $this->customerGroupRepo = $customerGroupRepo;
        $this->subAccountRepo = $subAccountRepo;
        $this->campaignRepo   = $campaignRepo;
        $this->customerRepo = $customerRepo;
        $this->oneTimeTaskRepo = $oneTimeTaskRepo;
        $this->context = $context;
        $this->baseJob = $baseJob;
        $this->taskRepo = $taskRepo;
        $this->bookingTaskRepo = $bookingTaskRepo;
        $this->messageBag = $messageBag;
        $this->sms = $smsfacade;
    }

    public function addJob(Request $request)
    {
        try {

            logger('ALL BOOKING REQUEST DATA', $request->all());

            if( !$request->filled('security_key') ) {
                logger('BOOKING NO SECURITY KEY');
                return $this->messageBag->add('parameter_error', 'Security Key is Required!');
            }

            if( !$request->filled('app_key') ) {
                logger('BOOKING NO APP KEY');
                return $this->messageBag->add('parameter_error', 'App Key is Required!');
            }

            if( !$request->filled('number') ) {
                logger('BOOKING NO NUMBER');
                return $this->messageBag->add('parameter_error', 'Number is Required!');
            }

            if( !$request->filled('date') ) {
                logger('BOOKING NO DATE');
                return $this->messageBag->add('parameter_error', 'Date is Required!');
            }

            if( !$request->filled('time') ) {
                logger('BOOKING NO TIME');
                return $this->messageBag->add('parameter_error', 'Time is Required!');
            }

            $task = $request->app_key ? $this->taskRepo->searchByKey($request->app_key, ['childs']) : null;

            logger('EXISTING BOOKING TASK');
            logger($task);

            logger('ADD BOOKING JOB REQUEST');
            logger($request->all());

            if ($task) {
                $taskHolder = collect();

                // inlude main task if send notif is set to true
                if( $task->send_notif ) {
                    $taskHolder->push($task);
                }

                if( $task->childs->count() > 0 ) {
                    $task->childs->each(function($item, $key) use (&$taskHolder) {
                        $taskHolder->push($item);
                    });
                }

                if( $taskHolder->count() < 1 ) {
                    logger('NO BOOKING JOB FOUND');
                    return $this->messageBag->add('jobs_error', 'No jobs found!');
                }

                $result = $this->bookingTaskRepo->addJob($request, $task, $taskHolder);
                logger('ADD BOOKING JOB WORKED!');
                return response()->json(['success' => true, 'message' => $result]);
            } else {
                throw new \ATM\Validation\ValidationException($this->messageBag->add('fatal_error', 'Something went wrong. Please contact administrator.'));
            }

        } catch (ValidationException $e) {
            logger('ERROR BOOKING ADD JOB');
            logger($e->getErrors());
            return $this->redirectFormRequest($e, true);
        }
    }

    public function deleteJob(Request $request)
    {
        try {

            if( !$request->filled('security_key') ) {
                return $this->messageBag->add('parameter_error', 'Security Key is Required!');
            }

            if( !$request->filled('app_key') ) {
                return $this->messageBag->add('parameter_error', 'App Key is Required!');
            }

            if( !$request->filled('zs_booking_id') ) {
                return $this->messageBag->add('parameter_error', 'Zensoft Booking Id is Required!');
            }

            $task = $request->app_key ? $this->taskRepo->searchByKey($request->app_key, ['childs']) : null;
            $subAccount = SubAccount::where('security_key', $request->security_key)->get()->first();

            if (!$task) {
                logger('BOOKING CANCEL TASK DOES NOT EXIST OR NOT ACTIVE');
                return $this->messageBag->add('parameter_error', 'BOOKING CANCEL TASK DOES NOT EXIST OR NOT ACTIVE');
            }

            if ($task) {

                $customerName = "{$request->first_name} {$request->last_name}";
                $customerNumber = formattedNumber($request->number);
                $bookingDateTime = new Carbon("{$request->date} {$request->time}");
                $bookingId = $request->zs_booking_id;

                $jobsTomorrow = JobTomorrow::where('zs_booking_id', $bookingId)->get();
                $jobsToday = JobToday::where('zs_booking_id', $bookingId)->get();
                $jobsNextMonth = JobNextMonth::where('zs_booking_id', $bookingId)->get();
                $jobsAfterTomorrowTo14thDay = JobAfterTomorrowTo14thDay::where('zs_booking_id', $bookingId)->get();
                $jobs15thDayToEndMonth = Job15thDayToEndMonth::where('zs_booking_id', $bookingId)->get();


                logger('BOOKING JOBS TOMORROW TO BE FAILED');
                logger($jobsTomorrow->count());
                if ($jobsTomorrow && $jobsTomorrow->count() > 0) {
                    logger($jobsTomorrow->toArray());

                    foreach ($jobsTomorrow as $key => $jobTomorrow) {
                        $this->sms->forceFailJobs($jobTomorrow, true, 'Booking is cancelled');
                    }
                }

                logger('BOOKING JOBS TODAY TO BE FAILED');
                logger($jobsToday->count());
                if ($jobsToday && $jobsToday->count() > 0) {
                    logger($jobsToday->toArray());

                    foreach ($jobsToday as $key => $jobToday) {
                        $this->sms->forceFailJobs($jobToday, true, 'Booking is cancelled');
                    }
                }

                logger('BOOKING JOBS NEXT MONTH TO BE FAILED');
                logger($jobsNextMonth->count());
                if ($jobsNextMonth && $jobsNextMonth->count() > 0) {
                    logger($jobsNextMonth->toArray());

                    foreach ($jobsNextMonth as $key => $jobNextMonth) {
                        $this->sms->forceFailJobs($jobNextMonth, true, 'Booking is cancelled');
                    }
                }

                logger('BOOKING JOBS AFTER TOMORROW TO 14TH DAY TO BE FAILED');
                logger($jobsAfterTomorrowTo14thDay->count());
                if ($jobsAfterTomorrowTo14thDay && $jobsAfterTomorrowTo14thDay->count() > 0) {
                    logger($jobsAfterTomorrowTo14thDay->toArray());

                    foreach ($jobsAfterTomorrowTo14thDay as $key => $jobAfterTomorrowTo14thDay) {
                        $this->sms->forceFailJobs($jobAfterTomorrowTo14thDay, true, 'Booking is cancelled');
                    }
                }

                logger('BOOKING JOBS 15th DAY TO END OF MONTH TO BE FAILED');
                logger($jobs15thDayToEndMonth->count());
                if ($jobs15thDayToEndMonth && $jobs15thDayToEndMonth->count() > 0) {
                    logger($jobs15thDayToEndMonth->toArray());

                    foreach ($jobs15thDayToEndMonth as $key => $job15thDayToEndMonth) {
                        $this->sms->forceFailJobs($job15thDayToEndMonth, true, 'Booking is cancelled');
                    }
                }


                // $taskHolder = collect();

                // // inlude main task if send notif is set to true
                // if( $task->send_notif ) {
                //     $taskHolder->push($task);
                // }

                // if( $task->childs->count() > 0 ) {
                //     $task->childs->each(function($item, $key) use (&$taskHolder) {
                //         $taskHolder->push($item);
                //     });
                // }

                // if( $taskHolder->count() < 1 ) {
                //     return $this->messageBag->add('jobs_error', 'No jobs found!');
                // }

                // $result = $this->taskRepo->delete($request, $task->id );
                $result = '';
                return response()->json(['success' => true, 'message' => $result]);
            } else {
                throw new \ATM\Validation\ValidationException($this->messageBag->add('fatal_error', 'Something went wrong. Please contact administrator.'));
            }

        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e, true);
        }
    }
}
