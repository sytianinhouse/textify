<?php

namespace App\Http\Controllers\SubAccount\CustomerGroup;

use ATM\Repositories\Client\Client;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\CustomerGroups\CustomerGroupsRepository;
use ATM\Repositories\Customers\Customer;

use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Project\Project;

use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;

use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;

class CustomerGroupController extends Controller
{
	public $customerGroupRepo;

	public function __construct(CustomerGroupsRepository $customerGroupRepo, SubAccountRepository $subAccountRepo)
    {
        $this->customerGroupRepo = $customerGroupRepo;
        $this->subAccountRepo = $subAccountRepo;
    }

    public function index(Request $request)
    {
        $authUser = Auth::user();

        $title         = 'Groups';
        $customergroup = $this->customerGroupRepo->search($request);

        $filters = [
        	'filter_search' => $request->filter_search,
			'per_page' => $request->per_page
		];

        if( $authUser->can('access', 'view-customer-group') ) {
            return view('sub-account.customergroups.index', compact('title', 'customergroup','filters'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $uuid)
    {
        $authUser = Auth::user();

        $client =  $request->user()->client;
        $title = $client->company_name . ' - Add Group';

        $request['active_only'] = true;
        // $subaccounts = $this->subAccountRepo->search($request, [], true)->pluck('account_name', 'id');
        $selectedSubAccnt = collect();

        if( $authUser->can('access', 'create-customer-group') ) {
            return view('sub-account.customergroups.create', compact('client', 'title'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $uuid, Client $client )
    {
        $authUser = Auth::user();

        if( $authUser->can('access', 'create-customer-group') ) {
            try {
                $input = $request->all();
                $rules = [
                    'name' => 'required',
                ];

                $validator = Validator::make($input, $rules);

                if ($validator->fails()) {
                    return redirect()->back()
                                ->withInput()
                                ->withErrors($validator);
                } else {
                    $customerGroup = $this->customerGroupRepo->add($request, $client);
                }

                if ( !$customerGroup->save() ) {
                    return redirect()->back()
                                ->withInput()
                                ->withErrors($customerGroup->getErrors())
                                ->withError("Please check required fields.");
                }

                return redirect()->route('sub.customer-group.index',$uuid)
                            ->withSuccess("Successfully added Groups.");
            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $uuid, CustomerGroup $customergroup )
    {
        $authUser = Auth::user();

        $client = $request->user()->client;
        $title  = $client->company_name . ' - Edit Group';

        $request['active_only'] = true;
        // $subaccounts = $this->subAccountRepo->search($request, [], true)->pluck('account_name', 'id');
        // $selectedSubAccnt = $customergroup->subAccounts->pluck('id');

        if( $authUser->can('access', 'update-customer-group') ) {
            return view('sub-account.customergroups.edit', compact('client', 'customergroup','title'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid, CustomerGroup $customergroup)
    {
        $authUser = Auth::user();
        if( $authUser->can('access', 'update-customer-group') ) {
            try {

                $customerGroup = $this->customerGroupRepo->update($customergroup, $request);

                if ( !$customerGroup->save() ) {
                    return redirect()->back()
                                ->withInput()
                                ->withErrors($customerGroup->getErrors())
                                ->withError("Please check required fields.");
                }

                return redirect()->route('sub.customer-group.index', $uuid)
                            ->withSuccess("Successfully Updated Group.");
            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $uuid, CustomerGroup $customergroup)
    {
        $authUser = Auth::user();
        if( $authUser->can('access', 'delete-customer-group') ) {
            try {
                $customerGroup = $this->customerGroupRepo->delete($customergroup);

                return redirect()->route('sub.customer-group.index', $uuid)
                            ->withSuccess("Successfully deleted $customerGroup.");
            } catch (\DomainException $e) {
                return redirect()->back()
                                ->withError($e->getMessage());
            }
        } else {
            abort(403);
        }
    }
}
?>
