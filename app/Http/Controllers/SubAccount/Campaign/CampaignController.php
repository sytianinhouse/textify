<?php

namespace App\Http\Controllers\SubAccount\Campaign;

use ATM\ContextInterface;
use ATM\Repositories\ApiKey\ApiKey;
use ATM\Repositories\ApiKey\ApiKeyRepository;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Countries\Country;
use ATM\Repositories\Project\Project;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\Tasks\JobsCompleted\JobsCompleted;
use ATM\Repositories\Tasks\JobsCompleted\JobsCompletedRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CampaignController extends Controller
{
    public $campaignRepo;

    public function __construct(
        CampaignRepository $campaignRepo,
        SubAccountRepository $subAccountRepo,
        ContextInterface $context,
        JobsCompleted $JobsCompleted,
        JobsCompletedRepository $JobsCompletedRepository
    )
    {
        $this->campaignRepo            = $campaignRepo;
        $this->subAccountRepo          = $subAccountRepo;
        $this->context                 = $context;
        $this->JobsCompleted           = $JobsCompleted;
        $this->JobsCompletedRepository = $JobsCompletedRepository;
    }

    public function index( Request $request)
    {
        $authUser = Auth::user();
        $title       = 'Campaigns';
        $campaign    = $this->campaignRepo->search($request);

        $request['active_only'] = true;
        $subaccounts = $this->subAccountRepo->search($request, [], true)->pluck('account_name', 'id');

        $query = [
            'per_page' => $request->per_page
        ];

        $filters = [
            'per_page' => $request->per_page,
            'from_date' => $request->from_date,
            'to_date' => $request->to_date,
        ];

        if($authUser->can('access', 'view-campaigns')) {
            return view('sub-account.campaign.index', compact('title', 'campaign','subaccounts','query','filters'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $authUser = Auth::user();
        $subaccounts = $this->subAccountRepo->search($request);
        $title       = 'Add Campaign';

        if($authUser->can('access', 'create-campaigns')) {
            return view('sub-account.campaign.create', compact('title','subaccounts'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Campaign $campaign)
    {
        $authUser = Auth::user();

        if( $authUser->can('access', 'create-campaigns') ) {
            try {
                $subAccount                = $this->context->getInstance();
                $request['active_only']    = true;
                $request['sub_account_id'] = $subAccount->id;
                $campaign                  = $this->campaignRepo->store($campaign, $request);

                if ( ! $campaign->save() ) {
                    return redirect()->back()
                                ->withInput()
                                ->withErrors($campaign->getErrors())
                                ->withError("Please check required fields.");
                }
                return redirect()->route('sub.camp.index',[$subAccount->uuid])
                                ->withSuccess("Campaign {$campaign->name} successfully added!");
            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $uuid, Campaign $campaign)
    {
        $authUser = Auth::user();
        $title = 'Edit Campaign - ' . $campaign->name;

        if( $authUser->can('access', 'update-campaigns') ) {
            return view('sub-account.campaign.edit', compact('campaign','title'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid, Campaign $campaign )
    {
        $authUser = Auth::user();
        if( $authUser->can('access', 'update-campaigns') ) {
            try {
                $subAccount                = $this->context->getInstance();
                $request['sub_account_id'] = $subAccount->id;
                $campaign = $this->campaignRepo->update($campaign, $request);

                return redirect()->route('sub.camp.index', $subAccount->uuid)
                            ->withSuccess("Campaign {$campaign->name} successfully updated!");
            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Project $project, Client $client, SubAccount $subaccount, Campaign $campaign)
    {
        $authUser = Auth::user();
        $name = $campaign->name;

        if( $authUser->can('access', 'delete-campaigns') ) {

            $tasks = $campaign->tasks;

            if ($tasks->count() > 1) {
                return redirect()->back()
                        ->withErrors("There is/are task associated with Campaign {$campaign->name}!");
            } else {
                $this->campaignRepo->delete($campaign);
            }

            return redirect()->back()
                        ->withSuccess("Campaign {$campaign->name} successfully deleted!");
        } else {
            abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sentSMS(Request $request, $uuid, Campaign $campaign)
    {
        $title = $campaign->name;
        $task = $campaign->task();
        $request['campaign_filter'] = $campaign->id;
        $jobs = $this->JobsCompletedRepository->search($request);

        return view('client.campaign.view-sms', compact('campaign','title','jobs'));
    }
}
