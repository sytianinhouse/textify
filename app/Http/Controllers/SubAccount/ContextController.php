<?php

namespace App\Http\Controllers\SubAccount;

use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContextController extends Controller {

	public function __construct(SubAccountRepository $subAccountRepo)
	{
		$this->subAccountRepo = $subAccountRepo;
	}

	public function changeContext($subaccountUuid, $beforeUuid, Request $request)
	{
		$subAccount = $this->subAccountRepo->getFirstBy('uuid', $subaccountUuid);

		if (!$subAccount) {
			return redirect()->back()
						->withError("Sub Account not found!");
		}

		$previousUrl = redirect()->getUrlGenerator()->previous();

		$url = $copy = str_replace($beforeUuid, $subAccount->uuid, $previousUrl);
		$offset = strrpos($url , $subaccountUuid);
		$substr = strpos($url , "/", $offset);

		if ($substr) {
			$url = substr($url, 0, $substr);
			$path = trim(substr($copy, $substr), '/');
			$paths = explode('/', $path);

			// check if has numeric then remove
			$numeric = array_first($paths, function ($value) {
			    return is_numeric($value);
			});

			// if uri has numeric then we will get rid of it from its position!!! gets mu
			if ($numeric) {
				$numericOffset = strpos($path, $numeric);
				$uri = substr($path, 0, $numericOffset);
				$url = $url . '/' . trim($uri, '/');
			} else {
				$url = $copy;
			}
		}
		session(['sub_account' => $subAccount->uuid]);

		return $url ? redirect($url) : redirect()->route('sub.dashboard', $subaccountUuid);
	}

}
