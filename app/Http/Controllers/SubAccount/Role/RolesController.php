<?php

namespace App\Http\Controllers\SubAccount\Role;

use ATM\ContextInterface;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Permission\Permission;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Roles\Role;
use ATM\Repositories\Roles\RoleRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;

class RolesController extends Controller
{

    public function __construct
    (
        RoleRepository $roleRepo,
        ContextInterface $context
    )
    {
        $this->context = $context;
        $this->roleRepo = $roleRepo;
    }

    public function index(Request $request)
    {
        // $this->police('list-user-roles');
        $authUser = Auth::user();
        $title = 'Roles';

        $client = $request->user()->client;
        $roles = $this->roleRepo->ofClient($client);

        if( $authUser->can('access', 'view-roles') ) {
            return view('sub-account.roles.index', compact('roles', 'client', 'title'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $this->police('create-user-role');
        $authUser = Auth::user();

        $title = 'Add Role';

        $client = $request->user()->client;
        $permissions = Permission::orderBy('position')->get()->groupBy('group');
        $permissions = $permissions->filter(function ($value,$key) {
            return $key != 'Gateway Keys';
        });
        // $permissions = $this->roleRepo->permissions();
        if( $authUser->can('access', 'create-roles') ) {
            return view('sub-account.roles.create', compact('client', 'title','permissions'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $authUser = Auth::user();
        if( $authUser->can('access', 'create-roles') ) {
            try {

                $input = $request->all();

                $subAccount = $this->context->getInstance();
                $client = $subAccount->client;

                $rules = [
                    'name' => 'required'
                ];

                $validator = Validator::make($input, $rules);
                if ($validator->fails()) {
                    return redirect()->back()
                                ->withInput()
                                ->withErrors($validator);
                } else {
                    $role = $this->roleRepo->add($request, $client);
                }

                if ( ! $role->save() ) {
                    return redirect()->back()
                                ->withInput()
                                ->withErrors($role->getErrors())
                                ->withError("Please check required fields.");
                }

                return redirect()->route('sub.role.index', $subAccount->uuid)
                            ->withSuccess("Successfully added Role.");
            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid, Request $request, Role $role)
    {
        $authUser = Auth::user();

        $title = 'Edit Role';

        // $this->police('edit-user-role');
        $permissions = Permission::orderBy('position')->get()->groupBy('group');
        $permissions = $permissions->filter(function ($value,$key) {
            return $key != 'Gateway Keys';
        });

        $client = $request->user()->client;

        // $permissions = $this->roleRepo->permissions();
        if( $authUser->can('access', 'update-roles') ) {
            return view('sub-account.roles.edit', compact('role', 'title','permissions'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($uuid, Request $request, Role $role)
    {
        $authUser = Auth::user();
        if( $authUser->can('access', 'update-roles') ) {
            try {

                $role = $this->roleRepo->update($role, $request);

                return redirect()->route('sub.role.index', $uuid)
                            ->withSuccess("Successfully Updated Role.");
            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid, Request $request, Role $role)
    {
        $authUser = Auth::user();
        // $this->police('delete-user-role');
        if( $authUser->can('access', 'delete-roles') ) {
            try {
                $roleName = $this->roleRepo->delete($role);

                return redirect()->route('sub.role.index', $uuid)
                            ->withSuccess("Successfully deleted Role $roleName.");
            } catch (\DomainException $e) {
                return redirect()->back()
                                ->withError($e->getMessage());
            }
        } else {
            abort(403);
        }
    }

}
