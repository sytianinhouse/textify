<?php

namespace App\Http\Controllers\SubAccount\Jobs\JobsFailed;

use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Settings\SiteSettingsRepository;
use ATM\Repositories\Sms\SmsFacade;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\TaskJobs\AuditLog\JobsAuditLogRepository;
use ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;
use ATM\Repositories\Tasks\JobsFailed\JobsFailed;
use ATM\Repositories\Tasks\JobsFailed\JobsFailedRepository;
use ATM\Repositories\Tasks\JobsNextMonth\JobNextMonth;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use ATM\Repositories\Tasks\Task;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class JobsFailedController extends Controller
{
    public function __construct(
        JobsFailed $jobsFailed,
        SmsFacade $smsFacade,
        JobsFailedRepository $jobsFailedRepo,
        SiteSettingsRepository $settingsRepo,
        JobsAuditLogRepository $jobsAuditLogsRepo,
        MessageBag $messageBag
    )
    {
        $this->jobsFailed        = $jobsFailed;
        $this->smsFacade         = $smsFacade;
        $this->jobsFailedRepo    = $jobsFailedRepo;
        $this->settingsRepo      = $settingsRepo;
        $this->jobsAuditLogsRepo = $jobsAuditLogsRepo;
        $this->messageBag        = $messageBag;
    }

    public function edit($uuid, Request $request, JobsFailed $jobsFailed)
    {
        $task     = Task::find($jobsFailed->task_id);
        $campaign = Campaign::find($jobsFailed->campaign_id);
        $date     = new Carbon($jobsFailed->time_attempt_send);
        $time     = new Carbon($jobsFailed->time_attempt_send);
        $title    = 'Edit Job - ' . $campaign->name;

        if( $task->isImmediateTask() ) {
			$breadCrumbsLabel = 'Custom API';
            $breadCrumbsUrl = sroute('sub.task.it.index');
        } elseif( $task->isOneTimeSendTask() ) {
			$breadCrumbsLabel = 'Template API';
            $breadCrumbsUrl = sroute('sub.task.ots.index');
        } elseif( $task->isTextBlastTask() ) {
			$breadCrumbsLabel = 'Text Blast';
            $breadCrumbsUrl = sroute('sub.task.tbs.index');
        } elseif( $task->isBirthdayTask() ) {
			$breadCrumbsLabel = 'Birthday SMS';
			$breadCrumbsUrl = sroute('sub.task.birthday-task.create');
		} elseif( $task->isBookingTask() ) {
			$breadCrumbsLabel = 'Booking API';
			$breadCrumbsUrl = sroute('sub.task.booking-task.create');
		}

        return view('sub-account.job.edit', compact('title', 'breadCrumbsLabel', 'breadCrumbsUrl', 'jobsFailed','campaign','time','date','task'));
    }

    public function resend($uuid, Request $request, JobsFailed $jobsFailed) {

        try {
            if( count($request->all()) > 0 ) {
                $this->jobsFailedRepo->update($jobsFailed, $request);
            }

            $subAccount = SubAccount::find($jobsFailed->subaccount_id);

            $task = Task::find($jobsFailed->task_id);

            $number = ( $request->customer_number ) ? $request->customer_number : $jobsFailed->customer_number;
            $request['number'] = preg_replace('/\D+/', '', $number);
            $request['name']   = ($request->customer_name) ? $request->customer_name : $jobsFailed->customer_name ;
            $request['from_job_failed'] = true;
            $request['job_failed_id'] = $jobsFailed->id;
            $request['message'] = $jobsFailed->message;

            $job = $this->jobsFailedRepo->resendFailedJob($request, $jobsFailed);

            if ( !$job['success'] ) {
                return redirect()->back()
						->withErrors($job['message']);
            }

            if( $task->isImmediateTask() ) {
                return redirect()->route('sub.task.it.edit', [$subAccount->uuid, $jobsFailed->task_id])
                ->withSuccess("Job {$jobsFailed->customer_name} successfully sent!");
            } elseif( $task->isOneTimeSendTask() ) {
                return redirect()->route('sub.task.ots.edit', [$subAccount->uuid, $jobsFailed->task_id])
                ->withSuccess("Job {$jobsFailed->customer_name} successfully sent!");
            } elseif( $task->isBookingTask() ) {
                return redirect()->route('sub.task.booking-task.edit', [$subAccount->uuid, ($jobsFailed->parent_task_id) ? $jobsFailed->parent_task_id : $jobsFailed->task_id ])
                ->withSuccess("Job {$jobsFailed->customer_name} successfully sent!");
            } else {
                return redirect()->route('sub.task.tbs.view', [$subAccount->uuid, $jobsFailed->task_id])
                ->withSuccess("Job {$jobsFailed->customer_name} successfully sent!");
            }


        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

	public function remove($uuid, Request $request, JobsFailed $jobsFailed )
	{
        $subAccount = SubAccount::find($jobsFailed->subaccount_id);

        $this->jobsFailedRepo->delete($jobsFailed->id);

        return redirect()->route('sub.task.tbs.view', [$subAccount->uuid, $jobsFailed->task_id])
                        ->withSuccess("Job {$jobsFailed->customer_name} successfully deleted!");
    }
}
