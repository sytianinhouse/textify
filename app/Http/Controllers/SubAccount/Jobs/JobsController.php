<?php

namespace App\Http\Controllers\SubAccount\Jobs;

use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Tasks\BaseJob;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class JobsController extends Controller
{
	public function __construct(
		BaseJob $baseJob
	)
	{
		$this->baseJob = $baseJob;
	}

	/**
	 * Intended for getting all jobs and return as table immediately
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function apiGet(Request $request)
	{
		$jobs = $this->baseJob->getAllJobs($request);
		$exclude_credit_costs = true;

		return view('templates.tasks.partials.task-jobs-list', compact('jobs', 'exclude_credit_costs'));
    }

	public function view($uuid, Request $request, $id, $instance)
	{
		if ($instance) {
			$class = new $instance;
			$job = $class->with('campaign', 'task', 'subAccount', 'client')->find($id);
			$task = $job->task;
			$campaign = $job->campaign;
			$date     = new Carbon($job->when_to_send);
			$title    = 'Edit Job - ' . $campaign->name;

			return view('client.job.edit', compact('title', 'job','campaign','time','date','task'));
		}

		return redirect()->back()
			->withErrors('SMS class not found');
    }

	public function update(Request $request, $id, $instance)
	{
		if ($instance) {
			try {
				$class = new $instance;
				$job = $class->find($id);
				if ($job) {
					$job = $this->baseJob->updateJob($request, $job);

					return redirect()->back()
						->withSuccess('SMS updated succesfully');
				}
			} catch (ValidationException $e) {
				return $this->redirectFormRequest($e);
			}
		}

		return redirect()->back()
			->withErrors('SMS class not found');
    }

	public function delete(Request $request, $id, $instance)
	{
		if ($instance) {
			$class = new $instance;
			$job = $class->find($id);
			$task = $job->task;
			$this->baseJob->deleteJob($job);

			$backUrl = '';
			if($task->isImmediateTask()) {
				$backUrl = route('client.task.it.edit', $task);
			} else if ($task->isOneTimeSendTask()) {
				$backUrl = route('client.task.ots.edit', $task);
			} else if ($task->isTextBlastTask()) {
				$backUrl = route('client.task.tbs.edit', $task);
			} else if ($task->isBirthdayTask()) {
				$backUrl = route('client.task.birthday-task.create');
			}

			return redirect()->to($backUrl)
				->withSuccess('SMS deleted succesfully');
		}

		return redirect()->back()
			->withErrors('SMS class not found');
    }
}
