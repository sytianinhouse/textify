<?php

namespace App\Http\Controllers\SubAccount\Profile;

use ATM\ContextInterface;
use ATM\Repositories\User\UserRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function __construct(
        UserRepository $userRepo,
        ContextInterface $context
    )
    {
        $this->userRepo = $userRepo;
        $this->context = $context;
    }

    public function edit( Request $request, User $user ) {
        $user = Auth::user();
        return view('sub-account.profile.index', compact('user'));
    }

    public function update( $uuid, Request $request, User $user ) {
        try {
            $user->load(['roles','subAccounts']);
            $user   = $request->user();
            $client = $user->client;
            $request['active'] = '1';
            $request['account_type'] = 'account_user';

            $roles =  $user->roles->pluck('id')->toArray();

            $request['roles'] = $roles;

            $subAccounts =  $user->subAccounts->pluck('id')->toArray();

            $request['sub_account'] = $subAccounts;

            $user = $this->userRepo->updateUser($client,$user, $request);

            return redirect()->route('sub.profile.edit',$uuid)
                        ->withSuccess("Successfully updated profile.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }
}
