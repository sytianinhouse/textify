<?php

namespace App\Http\Controllers\SubAccount\Customer;

use ATM\ContextInterface;
use ATM\Repositories\Cities\City;
use ATM\Repositories\Client\Client;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\Countries\Country;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\Customers\Customer;

use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Project\Project;

use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;

use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;

class CustomerController extends Controller
{
	public $customerRepo;

	public function __construct(CustomersRepository $customerRepo, SubAccountRepository $subAccountRepo, ContextInterface $context)
    {
        $this->customerRepo = $customerRepo;
        $this->subAccountRepo = $subAccountRepo;
        $this->context = $context;
    }

    public function index(Request $request)
    {
        $authUser = Auth::user();

        $title         = 'Contacts';
        $client        = $request->user()->client;
		$request['per_page'] = $request->per_page ?: self::PER_PAGE;
        $customer      = $this->customerRepo->search($request, ['subAccounts.client', 'customerGroups', 'cityRel']);
        $subAccounts   = $client->subAccounts->pluck('account_name', 'id');

        $filters = [
        	'filter_search' => $request->filter_search,
			'per_page' => $request->per_page,
            'subaccount_filter' => $request->subaccount_filter
		];

        if( $authUser->can('access', 'view-contacts') ) {
            return view('sub-account.customers.index', compact('title', 'customer','filters', 'subAccounts'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $authUser = Auth::user();

        $client =  $request->user()->client;
        $title = $client->company_name . ' - Add Contact';

        // $cities = City::pluck('name', 'id');
        $getCities = City::get();

        $cities = [];
        $getCities->each(function($value,$key) use (&$cities){
            $name = "{$value['name']}, {$value->province->name}";
            $cities[$value['id']] = $name;
        });

        // $selectedCity = 1367;
        $selectedCity = null;

        $countries = Country::pluck('name', 'name');
        $selectedCountry = 608;

        $request['active_only'] = true;
        $subaccounts = $this->subAccountRepo->search($request, [], true)->pluck('account_name', 'id');
        //$subaccounts = $client->subAccounts->pluck('account_name', 'id');
        $selectedSubAccnt = collect();

        $allCustomerGroups = CustomerGroup::where('client_id',$client->id)->get();

        if( $authUser->can('access', 'create-contacts') ) {
            return view('sub-account.customers.create', compact('cities','client', 'countries', 'selectedCountry', 'selectedCity','subaccounts', 'selectedSubAccnt', 'allCustomerGroups', 'title'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $uuid)
    {
        $authUser = Auth::user();
        // if( $authUser->can('access', 'create-customers') ) {
        if( $authUser->can('access', 'create-contacts') ) {
            try {
                $input = $request->all();

                if( $authUser->account_type == 'account_user' ) {
                    $rules = [
                        'first_name'    => 'required',
                        'mobile'        => 'required',
                    ];
                } else {
                    $rules = [
                        'first_name'    => 'required',
                        'mobile'        => 'required',
                        'sub_accounts'   => 'required',
                    ];
                }

                $validator = Validator::make($input, $rules);
                $client =  $request->user()->client;
                $request['sub_accounts'] = $this->context->getInstance()->id;
                if ($validator->fails()) {
                    return redirect()->back()
                                ->withInput()
                                ->withErrors($validator);
                } else {
                    $customer = $this->customerRepo->add($request, $client);
                }

                if ( !$customer->save() ) {
                    return redirect()->back()
                                ->withInput()
                                ->withErrors($customer->getErrors())
                                ->withError("Please check required fields.");
                }

                return redirect()->route('sub.customer.index',$uuid)
                            ->withSuccess("Successfully added A New Customer.");
            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $uuid, Customer $customer)
    {
        $authUser = Auth::user();

    	$client =  $request->user()->client;
        $title = $client->company_name . ' - Edit Contact';

        $cities = City::pluck('name', 'id');
        $selectedCity = $customer->city;

        $countries = Country::pluck('name', 'name');
        $selectedCountry = $customer->country;

        $request['active_only'] = true;

        $allCustomerGroups = CustomerGroup::where('client_id',$client->id)->get();

        $selectedCustomerGroups = $customer->customerGroups->pluck('id');
        $birthday = $customer->birthdate_form;
        if( $authUser->can('access', 'update-contacts') ) {
            return view('sub-account.customers.edit', compact('client', 'customer', 'countries', 'selectedCountry', 'subaccounts' , 'allCustomerGroups', 'selectedCustomerGroups' ,'title', 'cities', 'selectedCity', 'birthday'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$uuid, Customer $customer)
    {
        $authUser = Auth::user();
        if( $authUser->can('access', 'update-contacts') ) {
            try {
                $request['sub_accounts'] = $this->context->getInstance()->id;
                $customer = $this->customerRepo->update($customer, $request);

                if ( !$customer->save() ) {
                    return redirect()->back()
                                ->withInput()
                                ->withErrors($customer->getErrors())
                                ->withError("Please check required fields.");
                }

                return redirect()->route('sub.customer.index', $uuid)
                            ->withSuccess("Successfully Updated Customer Groups.");
            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $uuid, Customer $customer)
    {
        $authUser = Auth::user();
        if( $authUser->can('access', 'delete-contacts') ) {
            try {
                $customerName = $this->customerRepo->delete($customer);
                return redirect()->route('sub.customer.index', $uuid)
                            ->withSuccess("Successfully deleted contact $customerName.");
            } catch (\DomainException $e) {
                return redirect()->back()
                                ->withError($e->getMessage());
            }
        } else {
            abort(403);
        }
    }

    public function getcustomergroup(Request $request){

        $subaccount = $request->subaccount;
        $allData = SubAccount::with(['customerGroups'])->find($subaccount);

        $html = '';
        $html .= '<script>
                        $(".chzn-select").chosen({allow_single_deselect: true});
                </script>';
        if ( $allData ):
            $html .= '<div class="input-group input-group-prepen">
                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                <i class="fa fa-users" aria-hidden="true"></i>
                            </span>';
                            $html .= '<select id="customer_group" class="form-control chzn-select" multiple name="customer_group[]">';
                                foreach ( $allData->customerGroups as $args ):
                                    $html .= '<option value="'. $args->id.'">'.$args->name.'</option>';
                                endforeach;
                            $html .= '</select>';
            $html .= '</div>';
        endif;

        return response()->json(['success' => true, 'data' => $html]);
    }

    public function editcustomergroup(Request $request){

        $subaccount = $request->subaccount;
        $allData = SubAccount::with(['customerGroups'])->find($subaccount);

        $html = '';
        $html .= '<script>
                        $(".chzn-select").chosen({allow_single_deselect: true});
                </script>';
        if ( $allData ):
            $html .= '<div class="input-group input-group-prepen">
                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                <i class="fa fa-users" aria-hidden="true"></i>
                            </span>';
                            $html .= '<select id="customer_group" class="form-control chzn-select" multiple name="customer_group[]">';
                                foreach ( $allData->customerGroups as $args ):
                                    $html .= '<option value="'. $args->id.'">'.$args->name.'</option>';
                                endforeach;
                            $html .= '</select>';
            $html .= '</div>';
        endif;

        //return response()->json(['success' => true, 'data' => $allData->customerGroups]);
        return response()->json(['success' => true, 'edit' => true, 'data' => $html]);

    }
}

?>
