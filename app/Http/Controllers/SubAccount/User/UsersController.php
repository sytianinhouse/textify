<?php

namespace App\Http\Controllers\SubAccount\User;

use ATM\ContextInterface;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Roles\Role;
use ATM\Repositories\Roles\RoleRepository;
use ATM\Repositories\User\UserRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
	public function __construct
    (
        UserRepository $userRepo,
        RoleRepository $roleRepo,
        ContextInterface $context
    )
    {
        $this->context = $context;
        $this->userRepo = $userRepo;
        $this->roleRepo = $roleRepo;
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $authUser = Auth::user();

        $title = 'Users';
        $subAccount = $this->context->getInstance();

        $request['account_users_only'] = true;
        $request['subaccount_id'] = $subAccount->id;
        $users = $this->userRepo->search($request);
        unset($request['account_users_only']);

        $users = $users->filter(function ($value, $key) use($authUser) {
            return $value['username'] != 'kenneth' && $value['username'] != 'sytian' && $value['id'] != $authUser->id;
        });

        $filters = [
            'filter_search' => $request->filter_search,
            'per_page' => $request->per_page,
        ];

        if( $authUser->can('access', 'view-users') ) {
            return view('sub-account.users.index', compact('users', 'title', 'filters'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $authUser = Auth::user();

        $title = 'Add User';
        $accountTypes = User::$clientAccounts;
        $roles = $this->roleRepo->all()->pluck('display_name', 'id');
        $subAccount = $this->context->getInstance();
        $subAccounts = [$subAccount->id => $subAccount->account_name];

        if( $authUser->can('access', 'create-users') ) {
            return view('sub-account.users.create', compact('accountTypes', 'roles', 'title', 'subAccounts'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $authUser = Auth::user();
        if( $authUser->can('access', 'create-users') ) {
            try {
                $subAccount = $this->context->getInstance();
                $client =$subAccount->client;
                $user = $this->userRepo->addUser($client, $request);

                return redirect()->route('sub.user.index', $subAccount->uuid)
                            ->withSuccess("Successfully added user.");
            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Request $request)
    {
        $authUser = Auth::user();

        if( $authUser->can('access', 'view-users') ) {
            return view('client.clients.users.details', compact('user'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid, Request $request, User $user)
    {
        $authUser = Auth::user();

        $title = $user->name . ' - Edit User';
        $accountTypes = User::$clientAccounts;
        $roles = $this->roleRepo->all()->pluck('display_name', 'id');
        $subAccount = $this->context->getInstance();
        $subAccounts = [$subAccount->id => $subAccount->account_name];

        if( $authUser->can('access', 'update-users') ) {
            return view('sub-account.users.edit', compact('user', 'roles', 'accountTypes', 'title', 'subAccounts'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($uuid, Request $request, User $user)
    {
        $authUser = Auth::user();

        if( $authUser->can('access', 'update-users') ) {
            try {
                $subAccount = $this->context->getInstance();
                $client = $subAccount->client;

                $user = $this->userRepo->updateUser($client, $user, $request);
                $subAccount = $this->context->getInstance();

                return redirect()->route('sub.user.index', $subAccount->uuid)
                            ->withSuccess("Successfully updated client user.");
            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid, Request $request, User $user)
    {
        $authUser = Auth::user();

        if( $authUser->can('access', 'delete-users') ) {
            $name = $user->name;
            $user->delete();
            $subAccount = $this->context->getInstance();

            return redirect()->route('sub.user.index', $subAccount->uuid)
                        ->withSuccess("Successfully deleted user {$name}");
        } else {
            abort(403);
        }
    }

    protected function validatePassword($password, $pwdConfirmation)
    {
        $validator = Validator::make(
            [
            'password' => $password,
            'password_confirmation' => $pwdConfirmation,
            ],
            ['password' => 'required|min:6|confirmed']
        );

        return $validator->errors();
    }
}
