<?php

namespace App\Http\Controllers\SubAccount\Task\TextBlastSend;

use ATM\ContextInterface;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Cities\City;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Credits\Credit;
use ATM\Repositories\Credits\CreditRepository;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\CustomerGroups\CustomerGroupsRepository;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\Customers\CustomersQueryRepository;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Date\Date;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Settings\SiteSettings;
use ATM\Repositories\Shortcodes\Shortcodes;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\TaskJobs\TaskJobsRepository;
use ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;

use ATM\Repositories\Tasks\JobsNextMonth\JobNextMonth;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Tasks\TaskRepository;
use ATM\Repositories\Tasks\TextBlastRepository;

use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TextBlastSendController extends Controller
{
    public function __construct (
		CustomerGroupsRepository $customerGroupRepo,
        SubAccountRepository $subAccountRepo,
        CreditRepository $creditRepo,
        CustomersRepository $customerRepo,
        CampaignRepository $campaignRepo,
        TaskJobsRepository $JobTaskRepo,
		TaskRepository $taskRepo,
        TextBlastRepository $textBlastRepo,
        JobToday $JobsToday,
        ContextInterface $context,
        JobTomorrow $JobsTomorrow,
        JobAfterTomorrowTo14thDay $JobsAfterTomorrowTo14thDay,
        Job15thDayToEndMonth $Jobs15thDayToEndMonth,
        JobNextMonth $JobsNextMonth,
		CustomersQueryRepository $customersQueryRepo
    )
    {
    	$this->customerGroupRepo = $customerGroupRepo;
    	$this->customerRepo = $customerRepo;
    	$this->creditRepo = $creditRepo;
        $this->subAccountRepo = $subAccountRepo;
        $this->campaignRepo   = $campaignRepo;
        $this->JobTaskRepo = $JobTaskRepo;
        $this->taskRepo = $taskRepo;
        $this->textBlastRepo = $textBlastRepo;
        $this->JobsNextMonth = $JobsNextMonth;
        $this->Jobs15thDayToEndMonth = $Jobs15thDayToEndMonth;
        $this->JobsAfterTomorrowTo14thDay = $JobsAfterTomorrowTo14thDay;
        $this->JobsTomorrow = $JobsTomorrow;
        $this->JobsToday = $JobsToday;
        $this->customersQueryRepo = $customersQueryRepo;
        $this->context = $context;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $authUser = Auth::user();

        $title = 'Text Blast SMS';
        $user  = $request->user();
        $subAccount = $user->client->subAccounts->pluck('account_name', 'id');
        $campaigns = $this->campaignRepo->search($request)->pluck('name', 'id');
        $tasks = $this->textBlastRepo->search($request,['tasksjobsCompleted', 'tasksjobsFailed'], true);

        $filters = [
			'from_date' => $request->from_date,
			'to_date' => $request->to_date,
        	'filter_search' => $request->filter_search,
        	'campaign_filter' => $request->campaign_filter,
            'per_page' => $request->per_page,
            'filter_date_by' => $request->filter_date_by,
            'subaccount_filter' => $request->subaccount_filter,
        ];

        if( $authUser->can('access', 'view-text-blast-send-task') ) {
            if ( $tasks ) {
                return view('sub-account.tasks.text-blast-send.index', compact( 'tasks', 'title','campaigns','subAccount','filters'));
            }
        } else {
            abort(403);
        }
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Task $task)
    {
        $context = $this->context->getInstance();

        $authUser = Auth::user();

        $subAccount = $this->context->getInstance();
        $user   = $request->user();
        $title  = 'Add Task';
		$siteSettings = SiteSettings::getSettings();

        $client = $user->client;
        $clientID = $client->id;

        $request['client_id'] = $client->id;
        $request['sub_account_id'] = $subAccount->id;
        $request['uuid'] = $subAccount->uuid;

        $credit =  $subAccount->credit;

        $campaign = $this->campaignRepo->search($request)->pluck('name', 'id'); // get all campaign
        $selectedCampaign = collect();

        $customergroup = $this->customerGroupRepo->search($request)->pluck('name', 'id'); // for all customers
        $selectedCustomerGroup = collect();

		// selected customers
		$request['not_empty_mobile'] = true;
		$request['subaccount_id'] = $subAccount->id;
		$customers = $this->customersQueryRepo->getRecipientsList($request, true);
        $selectedCustomer = collect();

        $options = array('all_customer' => 'All Customers', /*'specific_customer' => 'Specific Customers',*/ 'custom_filter' => 'Custom Filter');
        $selectedOption = collect();

		// get total customer
        $allCounts = Task::withCount('taskCustomers', 'tasksJobToday',
                                    'tasksJobTomorrow', 'tasksJobAfterTomorrowTo14thDay',
                                    'tasksJob15thDayToEndMonth', 'tasksJobNextMonth', 'tasksjobsCompleted', 'tasksjobsFailed')->find( $task->id );

        $getTotal = $allCounts ? $allCounts->task_customers_count : 0;

        $messagePlaceholder = "Hi [first_name], [last_name] \n\nThis is our promo.";

        $shortCodes = Shortcodes::$shortCodes;
        $months = Date::$months;

        $cities = City::pluck('name', 'name'); // get all cities
        $selectedCities = collect();

        $api = $subAccount->apis;
        $apiID = $api->pluck('id')->first();

        if( $subAccount->sender_id == null && $apiID == 2 ) {
            $no_sender_id = true;
        } else {
            $no_sender_id = false;
        }

        if( $authUser->can('access', 'create-text-blast-send-task') ) {
            return view(
                'sub-account.tasks.text-blast-send.create',
                compact('campaign', 'subAccount','selectedCampaign', 'customergroup', 'selectedCustomerGroup', 'customers', 'selectedCustomer', 'cities', 'selectedCities', 'clientID', 'options', 'selectedOption', 'getTotal', 'allCounts', 'messagePlaceholder', 'credit', 'siteSettings', 'title', 'shortCodes', 'months', 'no_sender_id', 'context')
            );
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($uuid, Task $task, Request $request)
    {
        $authUser = Auth::user();
        if( $authUser->can('access', 'create-text-blast-send-task') ) {
            try {
                $task = $this->textBlastRepo->addTask($request, null,null, null);

                if ( isset($request->input_submit_task) == true ){
                    return redirect()
                            ->route('sub.task.tbs.index', $uuid)
                            ->withSuccess("Successfully added A New Task.");
                } else {
                    return redirect()->route('sub.task.tbs.view', [$uuid, $task])
                            ->withSuccess("Text blast successfully created.");

                }
            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid, Request $request, Task $task)
    {
        $authUser = Auth::user();

        $user  = $request->user();
        $title = $user->client->company_name . ' - Edit Task';

        $clientID = $task->client_id;
        $subAccount = SubAccount::find($task->subaccount_id);
		$credit = $subAccount->credit;

        $campaign = $subAccount->campaigns->pluck('name', 'id'); // get all campain
        $selectedCampaign = $task->campaign_id;

        $customergroup   = $this->customerGroupRepo->search($request, [], true)->pluck('name', 'id'); // groups
        $selectedCustomerGroup = collect();

		// selected customers
		$request['client_id'] = $clientID;
		$request['not_empty_mobile'] = true;
		$request['subaccount_id'] = $subAccount->id;
		$customers = $this->customersQueryRepo->getRecipientsList($request, true);
        $selectedCustomer = $task->taskCustomers->pluck('id');

        $options = array('all_customer' => 'All Customers', /*'specific_customer' => 'Specific Customers',*/ 'custom_filter' => 'Custom Filter');
        $selectedOption = collect();

        $allCounts = Task::withCount('taskCustomers', 'tasksJobToday',
                                    'tasksJobTomorrow', 'tasksJobAfterTomorrowTo14thDay',
                                    'tasksJob15thDayToEndMonth', 'tasksJobNextMonth')->find( $task->id ); // get total customer
        $getTotal = isset($allCounts) ? $allCounts->task_customers_count : 0;

        $messagePlaceholder = "Hi [first_name], [last_name] \n\nThis is our promo.";

		$shortCodes = Shortcodes::$shortCodes;
		$months = Date::$months;

        $cities = City::pluck('name', 'name'); // get all cities
        $selectedCities = collect();

        $subAcc = $user->client->subAccounts;
        $sub = $subAcc->pluck('sender_id')->first();
        $subID = $subAcc->first();
        $api = $subID->apis;
        $apiID = $api->pluck('id')->first();

        if( $sub == null && $apiID == 2 ) {
            $no_sender_id = true;
        } else {
            $no_sender_id = false;
        }

        if( $authUser->can('access', 'update-text-blast-send-task') ) {
            return view(
                'sub-account.tasks.text-blast-send.edit',
                compact('task', 'project', 'client', 'subAccount', 'campaign', 'selectedCampaign', 'customergroup', 'selectedCustomerGroup', 'customers', 'selectedCustomer', 'clientID', 'options', 'selectedOption', 'cities', 'selectedCities', 'getTotal', 'allCounts', 'messagePlaceholder', 'credit', 'title', 'shortCodes', 'months', 'no_sender_id')
            );
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($uuid,Request $request, Task $task)
    {
        $authUser = Auth::user();
        if( $authUser->can('access', 'update-text-blast-send-task') ) {
            try {
                // Process text blast creation
                $task = $this->textBlastRepo->addTask($request, $task, null, null);

                if ( isset($request->input_submit_task) == true ){
                    return redirect()->route( 'sub.task.tbs.index', [$uuid, $task] )
                    ->withSuccess("Successfully Updated Task.");
                } else {
                    return redirect()->route( 'sub.task.tbs.view', [$uuid, $task, $request] )
                            ->withSuccess("Text blast successfully created.");
                }
            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }
    }

    public function view($uuid, Request $request,Task $task) {
        $authUser = Auth::user();

        $client = Client::find($task->client_id);
        $title = $client->company_name . ' - View Task';

        $selectedCampaign = Campaign::find($task->campaign_id);

        $allCounts = Task::with('tasksjobsCompleted', 'tasksjobsFailed')
                            ->withCount('taskCustomers', 'tasksJobToday',
                            'tasksJobTomorrow', 'tasksJobAfterTomorrowTo14thDay',
                            'tasksJob15thDayToEndMonth', 'tasksJobNextMonth')->orderBy('id', 'ASC')->find( $task->id ); // get total customer
        $getTotal = isset($allCounts) ? $allCounts->task_customers_count : 0;

        $jobs = collect();
        $jobs = $jobs->merge($task->tasksJobToday);
        $jobs = $jobs->merge($task->tasksJobTomorrow);
        $jobs = $jobs->merge($task->tasksJobAfterTomorrowTo14thDay);
        $jobs = $jobs->merge($task->tasksJob15thDayToEndMonth);
        $jobs = $jobs->merge($task->tasksJobNextMonth);
        $jobs = $jobs->merge($task->tasksjobsCompleted);
        $jobs = $jobs->merge($task->tasksjobsFailed);

        $today = Carbon::now();
        $date = new Carbon($task->date_to_send);
        $time = $task->time_to_send;
        $timestamp = new Carbon($date->toDateString() . ' ' . $time);

        $cancelButton = ( $timestamp->gt($today) ) ? true : false;

        if( $authUser->can('access', 'view-text-blast-send-task') ) {
            return view( 'sub-account.tasks.text-blast-send.view', compact('task', 'jobs', 'selectedCampaign', 'getTotal', 'title', 'cancelButton') );
        } else {
            abort(403);
        }
    }

	public function updateStatus( $uuid, Request $request, Task $task)
	{
        if( isset($request->cancel_task) == true ) {
            $today = Carbon::now();
            $date = new Carbon($task->date_to_send);
            $time = $task->time_to_send;
            $timestamp = new Carbon($date->toDateString() . ' ' . $time);

            if( $timestamp->gt($today) ) {
                $this->destroy($uuid, $request, $task);
                return redirect()->route( 'sub.task.tbs.index', [$uuid, $task] )
                        ->withSuccess("Successfully Deleted Task.");
            } else {
                return redirect()->back()
                    ->withSuccess("Cannot cancel Task!");
            }

        } else {
            $task->active = $request->active ? true : false;
            $task->save();

            return redirect()->back()
                ->withSuccess("Text blast state updated.");
        }

	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $uuid, Request $request, Task $task )
    {
        $authUser = Auth::user();

        if( $authUser->can('access', 'delete-text-blast-send-task') ) {
            try {
                $request['dont_detach'] = true;
                $this->taskRepo->delete($request, $task->id);
                return redirect()->route('sub.task.tbs.index', [$uuid])
                            ->withSuccess("Task successfully deleted.");
            } catch (\DomainException $e) {
                return redirect()->back()
                                ->withError($e->getMessage());
            }
        } else {
            abort(403);
        }
    }

    public function ajaxCreateCampaign(Request $request, SubAccount $subaccount, Campaign $campaign) {
        try {
            $this->campaignRepo->store($campaign, $request);
            $campaign = $this->campaignRepo->search($request)->pluck('name', 'id');

            return view('partials.task.campaign-options', compact('campaign'));
        } catch (ValidationException $e) {
            return response()->json(['success' => false]);
        }
    }

    public function ajaxGenerateRecepient(Request $request)
	{
        $option = $request->getOption;
        if ( $option ) {
			$request['not_empty_mobile'] = true;
            $customers = $this->customersQueryRepo->search($request, true);
            $customersIds = $customers->implode('id', ',');
            return response()->json(['success' => true, 'data' => $customers->count(), 'ids' => $customersIds]);
        } else {
            return response()->json(['success' => false]);
        }
    }

    public function ajaxViewRecipient(Request $request)
	{
		$request['not_empty_mobile'] = true;
		$customers = $this->customersQueryRepo->search($request, true);
		$customerCount = $customers->count();

		return view('partials.task.ajax-table-recipients', compact('customers', 'customerCount'));
    }
}
