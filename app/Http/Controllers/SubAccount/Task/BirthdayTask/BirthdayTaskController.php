<?php

namespace App\Http\Controllers\SubAccount\Task\BirthdayTask;

use ATM\ContextInterface;
use App\Http\Controllers\Controller;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Shortcodes\Shortcodes;
use ATM\Repositories\Tasks\BirthdayTaskRepository;
use ATM\Validation\ValidationException;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BirthdayTaskController extends Controller
{
	public function __construct(
		BirthdayTaskRepository $birthdayRepo,
		CampaignRepository $campaignRepo,
		ContextInterface $context
	)
	{
		$this->birthdayRepo = $birthdayRepo;
		$this->campaignRepo = $campaignRepo;
		$this->context = $context;
	}

	public function create(Request $request)
	{
		$title = 'Birthday SMS';

		$client = $request->user()->client;
		$subAccounts = collect();
		$subAccounts = $subAccounts->merge($this->context);

		$request['subaccount_ids'] = $subAccounts->pluck('id');
		$tasks = $this->birthdayRepo->getTask($request)->keyBy('subaccount_id');

		$shortCodes = Shortcodes::$shortCodes;
		$curMnth = new Carbon();

		return view('sub-account.tasks.birthday-task.create', compact('title', 'tasks', 'subAccounts', 'client', 'shortCodes', 'curMnth'));
	}

	public function store(Request $request)
	{
		try {

			$task = $this->birthdayRepo->updateTask($request);

			if ($task){
				return redirect()
					->back()
					->withSuccess("Birthday SMS settings saved succesfully.");
			}
		} catch (ValidationException $e) {
			return $this->redirectFormRequest($e);
		}
	}
}