<?php

namespace App\Http\Controllers\SubAccount\Task\ImmediateTask;

use ATM\ContextInterface;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Tasks\BaseJob;
use ATM\Repositories\Tasks\ImmediateTaskRepository;
use ATM\Repositories\Tasks\Task;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ImmediateTaskController extends Controller
{
	public function __construct(
		CampaignRepository $campaignRepo,
		ImmediateTaskRepository $immediateTaskRepo,
		BaseJob $baseJob,
		ContextInterface $context
	)
	{
		$this->immediateTaskRepo = $immediateTaskRepo;
		$this->campaignRepo = $campaignRepo;
		$this->baseJob = $baseJob;
		$this->context  = $context;
	}

	public function index(Request $request)
	{
		$title = 'Custom API';
		$user  = $request->user();

		$subAccount = $this->context->getInstance();

		$request['subaccount_filter'] = $subAccount->id;
		$campaigns = $this->campaignRepo->search($request)->pluck('name', 'id');

		$tasks = $this->immediateTaskRepo->search($request);

		$filters = [
			'filter_date_by' => $request->filter_date_by,
			'campaign_filter' => $request->campaign_filter,
			'per_page' => $request->per_page,
		];

		return view('sub-account.tasks.immediate-task.index', compact( 'tasks', 'title','subAccounts','filters','campaigns'));
	}

	public function create(Request $request)
	{
		$subAccount = $this->context->getInstance();
		$client = $subAccount->client;
		$title = 'Immediate Task';

		$campaigns = $subAccount->campaigns->pluck('name', 'id');

		return view('sub-account.tasks.immediate-task.create', compact( 'client', 'subAccount', 'title', 'campaigns'));
	}

	public function store($uuid, Request $request)
	{
		try {
			$subAccount = $this->context->getInstance();
			$client = $subAccount->client;

			$request['client_id'] = $client->id;
			$task = $this->immediateTaskRepo->addTask($request);

			return redirect()
				->route('sub.task.it.edit', [$uuid, $task])
				->withSuccess("Created succesfully!");

		} catch (ValidationException $e) {
			return $this->redirectFormRequest($e);
		}
	}

	public function edit($uuid, Task $task, Request $request)
	{
		$client = $this->context->getInstance();
		$title = 'Edit Custom API';

		$subAccount = $task->subAccount;
		$campaigns = $subAccount->campaigns->pluck('name', 'id');

		$request['task_id'] = $task->id;
		$jobs = $this->baseJob->getAllJobs($request, true);

		return view('sub-account.tasks.immediate-task.edit', compact( 'client', 'subAccount', 'task', 'title', 'campaigns', 'jobs'));
	}

	public function update($uuid, Task $task, Request $request)
	{
		try {
			$task = $this->immediateTaskRepo->addTask($request, $task);

			return redirect()
				->route('sub.task.it.edit', [$uuid, $task])
				->withSuccess("Updated succesfully!");

		} catch (ValidationException $e) {
			return $this->redirectFormRequest($e);
		}
	}

	public function destroy($uuid, Task $task, Request $request)
	{
		if ($task->hasJobs()) {
			return redirect()
				->back()
				->withErrors('This task has current jobs, deletion is not allowed.');
		}

		$this->immediateTaskRepo->removeTask($task);

		return redirect()
			->back()
			->withSuccess('Task deleted!');
	}
}
