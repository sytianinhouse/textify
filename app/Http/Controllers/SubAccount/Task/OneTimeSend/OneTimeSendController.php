<?php

namespace App\Http\Controllers\SubAccount\Task\OneTimeSend;

use ATM\ContextInterface;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Cities\City;
use ATM\Repositories\Client\Client;
use ATM\Repositories\CustomerGroups\CustomerGroupsRepository;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Shortcodes\Shortcodes;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\Tasks\BaseJob;
use ATM\Repositories\Tasks\OneTimeTaskRepository;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Tasks\TaskType\TaskType;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OneTimeSendController extends Controller
{
    public function __construct (
        CampaignRepository $campaignRepo,
        SubAccountRepository $subAccountRepo,
        CustomersRepository $customerRepo,
        CustomerGroupsRepository $customerGroupRepo,
        OneTimeTaskRepository $oneTimeTaskRepo,
		BaseJob $baseJob
    )
    {
        $this->customerGroupRepo = $customerGroupRepo;
        $this->subAccountRepo = $subAccountRepo;
        $this->campaignRepo   = $campaignRepo;
        $this->customerRepo = $customerRepo;
        $this->oneTimeTaskRepo = $oneTimeTaskRepo;
        $this->baseJob = $baseJob;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $authUser = Auth::user();
        $title = 'Template API';
        $request['filter_task_type_id'] = \ATM\Repositories\Tasks\Task::ONE_TIME_SEND;
        $campaigns = $this->campaignRepo->search($request)->pluck('name', 'id');
        $tasks = $this->oneTimeTaskRepo->search($request);

        $filters = [
        	'filter_search' => $request->filter_search,
        	'campaign_filter' => $request->campaign_filter,
            'per_page' => $request->per_page,
            'filter_date_by' => $request->filter_date_by,
            'subaccount_filter' => $request->subaccount_filter,
        ];

        if( $authUser->can('access', 'view-roles') ) {
            return view('sub-account.tasks.one-time-send.index', compact('title', 'tasks', 'campaigns', 'title','filters'));
        } else {
            abort(403);
        }

    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $context = app(ContextInterface::class);
        $subAccount = $context->getInstance();

        $authUser = Auth::user();
        $title = 'Add Template API';
        // $taskTypes = TaskType::pluck('title', 'id'); // Di pa need

        $campaigns = $this->campaignRepo->search($request, [], true)->pluck('name', 'id'); // campaigns
        $cities = City::pluck('name', 'name'); // get all cities
        $messagePlaceholder = "Thank you for visiting us [first_name] [last_name]! \n\nPlease come see us again.";
        $shortCodes = Shortcodes::$shortCodes;

        if( $authUser->can('access', 'create-roles') ) {
            return view('sub-account.tasks.one-time-send.create', compact('title', 'subAccount','campaigns', 'cities', 'shortCodes', 'messagePlaceholder'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($uuid, Request $request)
    {
        $authUser = Auth::user();
        if( $authUser->can('access', 'create-roles') ) {
            try {

                $client = $request->user()->client;

                $context = app(ContextInterface::class);
                $subAccount = $context->getInstance();

                $request['sub_account_id'] = $subAccount->id;
                $request['task_type_id'] = \ATM\Repositories\Tasks\Task::ONE_TIME_SEND;
                $request['time_to_send'] = Carbon::now()->startOfDay()->hour($request->time_to_send[0])->minute($request->time_to_send[1])->format('H:i');
                $request['prevent_job_fetch'] = false;

                $task = $this->oneTimeTaskRepo->add($request, $client, 'one_time_send');

                return redirect()->back()
                            ->withSuccess("Successfully added A New Task.");

            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid, Task $task, Request $request)
    {
        $authUser = Auth::user();
        $request['sub_account_id'] = $task->subaccount_id;

        $context = app(ContextInterface::class);
        $subAccount = $context->getInstance();

        $title = 'Edit Task';
        $campaigns = $this->campaignRepo->search($request, [], true)->pluck('name', 'id'); // campaigns
        $cities = City::pluck('name', 'name'); // get all cities
        $shortCodes = Shortcodes::$shortCodes;

		$request['task_id'] = $task->id;
		$jobs = $this->baseJob->getAllJobs($request, true);

        if( $authUser->can('access', 'update-roles') ) {
            return view('sub-account.tasks.one-time-send.edit', compact('task', 'project', 'client', 'campaigns', 'selectedCampaign', 'title', 'shortCodes', 'messagePlaceholder', 'subAccounts', 'subAccount', 'jobs'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($uuid, Task $task, Request $request)
    {
        $authUser = Auth::user();
        if( $authUser->can('access', 'update-roles') ) {
            try {
                $client = $request->user()->client;

                $context = app(ContextInterface::class);
                $subAccount = $context->getInstance();

                $request['sub_account_id'] = $subAccount->id;
                $request['time_to_send'] = Carbon::now()->startOfDay()->hour($request->time_to_send[0])->minute($request->time_to_send[1])->format('H:i');
                $request['prevent_job_fetch'] = false;

                $task = $this->oneTimeTaskRepo->update($task->id, $request, $client);

                if ( !$task->save() ) {
                    return redirect()->back()
                                ->withInput()
                                ->withErrors($task->getErrors())
                                ->withError("Please check required fields.");
                }
                return redirect()->back()
                            ->withSuccess("Successfully Updated Task.");
            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid, Task $task, Request $request)
    {
        $authUser = Auth::user();
        if( $authUser->can('access', 'delete-roles') ) {
            try {

                $request['dont_detach'] = true;
                $this->oneTimeTaskRepo->delete($request, $task->id);

                return redirect()->back()
                            ->withSuccess("Task successfully deleted.");
            } catch (\DomainException $e) {
                return redirect()->back()
                                ->withError($e->getMessage());
            }
        } else {
            abort(403);
        }
    }

    /**
     * Able to create quick campaign creation
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajaxCreateCampaign(Request $request, SubAccount $subaccount, Campaign $campaign) {
        try {
            $this->campaignRepo->store($campaign, $request);

            $campaign = $this->campaignRepo->search($request)->sortBy('id');
            $lastCampaign = $campaign->last();

            $campaign = $campaign->pluck('name', 'id');

            return view('partials.task.campaign-options', compact('campaign', 'lastCampaign'));
        } catch (ValidationException $e) {
            return response()->json(['success' => false]);
        }
    }

}
