<?php

namespace App\Http\Controllers\SubAccount\Reports\Sms;

use ATM\ContextInterface;
use ATM\FileGenerator;
use ATM\Repositories\Campaign\CampaignQueryRepository;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Reports\ReportsRepository;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CampaignController extends Controller
{

    public function __construct
    (
        ReportsRepository $reportsRepo,
        CampaignQueryRepository $campaignQueryRepo,
        CampaignRepository $campaignRepo
    )
    {
        $this->campaignQueryRepo = $campaignQueryRepo;
        $this->campaignRepo = $campaignRepo;
        $this->reportsRepo = $reportsRepo;
    }

    public function smsPerCampaign(Request $request)
    {
		$title = 'SMS Per Campaign';
        $begin = $request->filled('start_filter') && $request->start_filter ? true : false;

		$context = app(ContextInterface::class);
		$subAccount = $context->getInstance();

		$request['subaccount_id'] = $subAccount->id;

        $request['from_date'] = Carbon::parse($request->from_date)->toDateString();
        $request['to_date'] = Carbon::parse($request->to_date)->toDateString();

        $campaigns = $this->campaignRepo->search($request)->pluck('name', 'id');
		$tasksPerCampaign = $this->reportsRepo->smsPerCampaign($request);

        if( $begin ) {

            if ($request->filled('export_excel')) {

                $client = $request->user()->client;
                $namePrefix = '';
                $moneyColumn = [];

                $namePrefix = $client->company_name;

                FileGenerator::makeExcel(
                    '',
                    $namePrefix . ' - SMS per Campaign - ' . $request['from_date'] . ' - ' . $request['to_date'],
                    'SMS per Campaign',
                    'partials.report-export.jobs.jobs-per-campaign',
                    compact('tasksPerCampaign'),
                    false,
                    $moneyColumn
                );
            }
        }

        $filters = [
            'start_filter'    => $request->start_filter,
            'from_date'       => $request->from_date,
            'to_date'         => $request->to_date,
            'campaign_filter' => $request->campaign_filter,
        ];
        $authUser = Auth::user();
        if($authUser->can('access', 'view-sms-per-campaign')) {
            return view('sub-account.reports.sms.campaign.per-campaign', compact('title','campaigns','tasksPerCampaign','filters','begin'));
        } else {
            abort(403);
        }
    }
}
