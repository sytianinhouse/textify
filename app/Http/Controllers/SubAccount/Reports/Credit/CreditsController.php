<?php

namespace App\Http\Controllers\SubAccount\Reports\Credit;

use ATM\FileGenerator;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Credits\CreditQueryRepository;
use ATM\Repositories\Credits\CreditRepository;
use ATM\Repositories\Reports\ReportsRepository;
use ATM\Repositories\SubAccount\SubAccountRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CreditsController extends Controller
{

    public function __construct
    (
        CreditRepository $creditRepo,
        SubAccountRepository $subRepo,
        CampaignRepository $campaign,
        CreditQueryRepository $creditQueryRepo,
        ReportsRepository $reportsRepo
    )
    {
        $this->creditRepo      = $creditRepo;
        $this->subRepo         = $subRepo;
        $this->campaignRepo    = $campaign;
        $this->creditQueryRepo = $creditQueryRepo;
        $this->reportsRepo = $reportsRepo;
    }

    public function creditBalance( Request $request ) {

        $title = 'Credit Balance';

        $begin = $request->filled('start_filter') && $request->start_filter ? true : false;

        $subaccounts = $this->subRepo->all()->pluck('account_name', 'id');
        $campaigns = $this->campaignRepo->search($request)->pluck('name', 'id');

        if ($begin) {
            $user = $request->user();
            $clientID = $user->client->id;
            $request['clientID'] = $clientID;
            $request['subAccountId'] = null;

            $credits = $this->creditQueryRepo->search($request,true);

            $results = $this->reportsRepo->creditBalance($request);

            if ($request->filled('export_excel')) {

                $client = $request->user()->client;
                $namePrefix = '';
                $moneyColumn = [];

                $namePrefix = $client->company_name;

                FileGenerator::makeExcel(
                    '',
                    $namePrefix . ' - Credit Balance - ' . $request['from_date'] . ' - ' . $request['to_date'],
                    'SMS per Campaign',
                    'partials.report-export.credits.credit-balance',
                    compact('results'),
                    false,
                    $moneyColumn
                );
            }
        }

        $filters = [
            'start_filter' => $request->start_filter,
            'from_date' => $request->from_date,
            'to_date' => $request->to_date,
            'campaign_filter' => $request->campaign_filter,
            'sub_account_id' => $request->sub_account_id,
        ];
        $authUser = Auth::user();
        if($authUser->can('access', 'view-credits')) {
            return view('sub-account.reports.credits.balance', compact('title', 'begin', 'credits','subaccounts','campaigns','filters'));
        } else {
            abort(403);
        }
    }
}
