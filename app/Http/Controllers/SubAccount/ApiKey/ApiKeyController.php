<?php

namespace App\Http\Controllers\SubAccount\ApiKey;

use ATM\ContextInterface;
use ATM\Repositories\ApiKey\ApiKey;
use ATM\Repositories\ApiKey\ApiKeyRepository;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Client\ClientRepository;
use ATM\Repositories\Countries\Country;
use ATM\Repositories\Project\Project;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ApiKeyController extends Controller
{
    public function __construct(ApiKeyRepository $apiKeyRepo, SubAccountRepository $subAccountRepo, ClientRepository $clientRepo, ContextInterface $context)
    {
        $this->apiKeyRepo     = $apiKeyRepo;
        $this->subAccountRepo = $subAccountRepo;
        $this->clientRepo     = $clientRepo;
        $this->context        = $context;
    }

    public function index(Request $request)
    {
        $authUser = Auth::user();

        $title = 'Gateway Keys';
        $apis  = $this->apiKeyRepo->search($request);

        $request['active_only'] = true;
        $subaccounts = $this->subAccountRepo->search($request, [], true)->pluck('account_name', 'id');

        $filters = [
            'sub_account_id' => $request->sub_account_id,
            'per_page' => $request->per_page,
            'filter_search' => $request->filter_search
        ];

        if($authUser->can('access', 'view-gateway-keys')) {
            return view('sub-account.apikey.index', compact('title', 'apis','subaccounts','filters'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $authUser    = Auth::user();
        $user        = $request->user();
        $subAccount  = $this->subAccountRepo->search($request);
        $title       = 'Add Api Key';
        $randomSting = Str ::random(5);
        $key         = $randomSting.'-'.$user->client->company_name.'-'.$user->client->cuid;
        $cuid        = $user->client->cuid;
        $apiKey      = sha1($key);

        if($authUser->can('access', 'create-gateway-keys')) {
            return view('sub-account.apikey.create', compact('apikey','cuid','apiKey','subAccount','title'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Client $client, ApiKey $apikey)
    {
        $authUser = Auth::user();

        if($authUser->can('access', 'create-gateway-keys')) {
            try {
                $subAccount           = $this->context->getInstance();
                $user                 = $request->user();
                $request['client_id'] = $user->client->id;
                $subaacount           = $user->client->subAccounts->where('active',1);

                if( $subaacount->count() == 1 ) {
                    $request['sub_account_id'] = $subaacount->first()->id;
                }

                $apikey = $this->apiKeyRepo->store($apikey, $request);

                if ( ! $apikey->save() ) {
                    return redirect()->back()
                                ->withInput()
                                ->withErrors($apikey->getErrors())
                                ->withError("Please check required fields.");
                }

                return redirect()->route('sub.api.index',$subAccount->uuid)
                                ->withSuccess("Api Key {$apikey->name} successfully added!");

            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $uuid, ApiKey $apikey )
    {
        $authUser   = Auth::user();
        $subAccount = $this->subAccountRepo->search($request);
        $title      = 'Edit Api Key - ' . $apikey->name;

        if($authUser->can('access', 'update-gateway-keys')) {
            return view('sub-account.apikey.edit', compact('title','subAccount','apikey'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid, ApiKey $apikey)
    {
        $authUser = Auth::user();
        if($authUser->can('access', 'update-gateway-keys')) {
            try {
                $subAccount = $this->context->getInstance();
                $apikey     = $this->apiKeyRepo->update($apikey, $request);

                return redirect()->route('sub.api.index', $subAccount->uuid)
                                ->withSuccess("Api Key {$apikey->name} successfully updated!");
            } catch (ValidationException $e) {
                return $this->redirectFormRequest($e);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(ApiKey $apikey, $uuid)
    {
        $name = $apikey->name;
        $authUser = Auth::user();

        if($authUser->can('access', 'delete-gateway-keys')) {
            $this->apiKeyRepo->delete($apikey);

            return redirect()->back()
                        ->withSuccess("Api Key {$apikey->name} successfully deleted!");
        } else {
            abort(403);
        }
    }
}
