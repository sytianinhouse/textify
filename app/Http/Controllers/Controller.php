<?php

namespace App\Http\Controllers;

use ATM\Validation\ValidationException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
	const PER_PAGE = 50;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function redirectFormRequest(ValidationException $e, $ajax = false)
    {
        if ($ajax) {
            return response()->json($e->getErrors(), 422);
        }
        
    	return redirect()->back()
						->withInput()
						->withErrors($e->getErrors())
						->withError("Please check required fields.");
    }

    public function reportTemplate(Request $request)
    {
        $title = 'Report Template';

        $begin = false;

        if ($request->filled('start_filter')) {
            $begin = $request->start_filter;
        }

        if ($begin) {
            if (!$request->filled('from_date')) {
                $request['from_date'] = \Carbon\Carbon::now()->subDays(7)->startOfDay()->toDateString();
            }

            if (!$request->filled('to_date')) {
                $request['to_date'] = \Carbon\Carbon::now()->endOfDay()->toDateString();
            }
        }

        return view('asset_pages.report_template', compact('title', 'begin'));
    }
}
