<?php

namespace App\Http\Controllers\Client\Role;

use ATM\Repositories\Client\Client;
use ATM\Repositories\Permission\Permission;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Roles\Role;
use ATM\Repositories\Roles\RoleRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class RolesController extends Controller
{
    public $role;

    public function __construct(RoleRepository $role)
    {
        $this->roleRepo = $role;
    }

    public function index(Request $request)
    {
        $client = $request->user()->client;
        $title = $client->company_name . ' - Roles';
        $roles = $client->roles;

        $this->roleRepo->search($request);

        $filters = [
        	'filter_search' => $request->filter_search,
			'per_page' => $request->per_page
		];

    	return view('client.roles.index', compact('roles', 'client', 'title','filters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Add Role';
        $permissions = Permission::orderBy('position')->get()->groupBy('group');
        $permissions = $permissions->filter(function ($value,$key) {
            return $key != 'Gateway Keys';
        });
        return view('client.roles.create', compact('title','permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $input = $request->all();
            $client = $request->user()->client;

            $rules = [
                'name' => 'required'
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } else {
                $role = $this->roleRepo->add($request, $client);
            }

            if ( ! $role->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($role->getErrors())
                            ->withError("Please check required fields.");
            }

            return redirect()->route('client.role.index')
                        ->withSuccess("Successfully added Role.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client, Role $role)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $title = 'Edit User Role';
        $permissions = Permission::orderBy('position')->get()->groupBy('group');
        $permissions = $permissions->filter(function ($value,$key) {
            return $key != 'Gateway Keys';
        });
        return view('client.roles.edit', compact('role', 'title','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project, Client $client, Role $role)
    {
        try {

            $role = $this->roleRepo->update($role, $request);

            return redirect()->route('client.role.index')
                        ->withSuccess("Successfully Updated Role.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Client $client, Role $role)
    {
        try {
            $roleName = $this->roleRepo->delete($role);

            return redirect()->route('client.role.index')
                        ->withSuccess("Successfully deleted Role $roleName.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

}
