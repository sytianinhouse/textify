<?php

namespace App\Http\Controllers\Client;

use ATM\ContextInterface;
use ATM\Repositories\Credits\CreditQueryRepository;
use ATM\Repositories\Credits\CreditRepository;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Tasks\BaseJob;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDayRepository;
use ATM\Repositories\Tasks\JobsFailed\JobsFailed;
use ATM\Repositories\Tasks\JobsFailed\JobsFailedRepository;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTodaySend\JobTodayRepository;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrowRepository;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct
    (
        ContextInterface $context,
        BaseJob $baseJob,
        JobsFailed $JobsFailed,
        JobToday $JobToday,
        JobTomorrow $jobTomorrow,
        JobAfterTomorrowTo14thDay $JobAfterTomorrowTo14thDay,

        JobsFailedRepository $JobsFailedRepo,
        JobTodayRepository $JobTodayRepo,
        JobTomorrowRepository $JobTomorrowRepo,
        JobAfterTomorrowTo14thDayRepository $JobAfterTomorrowTo14thDayRepo,

        CreditQueryRepository $creditQueryRepo,
        CreditRepository $creditRepo
    )
    {
        $this->baseJob = $baseJob;
        $this->JobsFailed = $JobsFailed;
        $this->JobToday = $JobToday;
        $this->jobTomorrow = $jobTomorrow;
        $this->JobAfterTomorrowTo14thDay = $JobAfterTomorrowTo14thDay;

        $this->JobsFailedRepo = $JobsFailedRepo;
        $this->JobTodayRepo = $JobTodayRepo;
        $this->JobTomorrowRepo = $JobTomorrowRepo;
        $this->JobAfterTomorrowTo14thDayRepo = $JobAfterTomorrowTo14thDayRepo;

        $this->creditRepo      = $creditRepo;
        $this->creditQueryRepo = $creditQueryRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, SubAccount $subaccount)
    {
        $user = $request->user();
        $request['client_id'] = $user->client_id;
        $request['dashboard_jobs'] = true;
        $request['dashboard_jobs_failed'] = false;

        $jobs = $this->baseJob->getAllJobs($request,false,true, false, ['subAccount']);

        $request['per_page'] = 10;
        $JobsFailed = $this->JobsFailedRepo->search($request);

        $now = Carbon::now();
        $fifteenDaysBefore = $now->copy()->subDays(7);
        $fromDate = $fifteenDaysBefore->startOfDay()->toDateTimeString();
        $toDate = $now->endOfDay()->toDateTimeString();

        $request['from_date'] = $fromDate;
        $request['to_date'] = $toDate;

        $credits = $this->creditQueryRepo->search($request,true);

        $range = [];
        for($x = 0; $x <= 7; $x++ ) {
            $thisDate = $now->copy()->subDay($x);
            $range[$thisDate->format('m/d/y')] = 0;
        }

        $datesData = [];
        $credits->each(function($item) use (&$datesData) {
            $date = new Carbon($item->when_to_send);
            $datesData[$date->format('m/d/y')] = $item->credits_consumed;
        });

        $credits = array_replace($range, $datesData);
        ksort($credits);

        return view('client.dashboard.index',compact('subaccount','jobs', 'JobsFailed', 'credits'));
    }

}
