<?php

namespace App\Http\Controllers\Client\Profile;

use ATM\ContextInterface;
use ATM\Repositories\User\UserRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function __construct(
        UserRepository $userRepo,
        ContextInterface $context
    )
    {
        $this->userRepo = $userRepo;
        $this->context = $context;
    }

    public function edit( Request $request, User $user ) {
        $user = Auth::user();
        return view('client.profile.index', compact('user'));
    }

    public function update( Request $request, User $user ) {
        try {
            $user   = $request->user();
            $client = $user->client;
            $request['active'] = '1';
            $request['account_type'] = 'client';
            $user = $this->userRepo->updateUser($client,$user, $request);

            return redirect()->route('client.profile.edit',$user->id)
                        ->withSuccess("Successfully updated profile.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }
}
