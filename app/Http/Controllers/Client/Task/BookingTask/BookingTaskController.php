<?php

namespace App\Http\Controllers\Client\Task\BookingTask;

use ATM\ContextInterface;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Cities\City;
use ATM\Repositories\Client\Client;
use ATM\Repositories\CustomerGroups\CustomerGroupsRepository;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Shortcodes\Shortcodes;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\Tasks\BaseJob;
use ATM\Repositories\Tasks\BookingTaskRepository;
use ATM\Repositories\Tasks\OneTimeTaskRepository;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Tasks\TaskType\TaskType;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BookingTaskController extends Controller
{
    public function __construct (
        CampaignRepository $campaignRepo,
        SubAccountRepository $subAccountRepo,
        CustomersRepository $customerRepo,
        CustomerGroupsRepository $customerGroupRepo,
        OneTimeTaskRepository $oneTimeTaskRepo,
		BaseJob $baseJob,
        ContextInterface $context,
        BookingTaskRepository $bookingTaskRepo
    )
    {
        $this->customerGroupRepo = $customerGroupRepo;
        $this->subAccountRepo = $subAccountRepo;
        $this->campaignRepo   = $campaignRepo;
        $this->customerRepo = $customerRepo;
        $this->oneTimeTaskRepo = $oneTimeTaskRepo;
        $this->context = $context;
        $this->baseJob = $baseJob;
        $this->bookingTaskRepo = $bookingTaskRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $title = 'Booking API';

        $subAccounts = $request->user()->client->subAccounts->pluck('account_name', 'id');

        $request['filter_task_type_id'] = \ATM\Repositories\Tasks\Task::BOOKING_TASK;
        $request['per_page'] = ($request->per_page != null ) ? $request->per_page : 10;
        $campaigns = $this->campaignRepo->search($request)->pluck('name', 'id');
        $tasks = $this->bookingTaskRepo->search($request, ['campaign']);

        $filters = [
        	'filter_search' => $request->filter_search,
        	'campaign_filter' => $request->campaign_filter,
            'per_page' => $request->per_page,
            'filter_date_by' => $request->filter_date_by,
            'subaccount_filter' => $request->subaccount_filter,
		];

        return view('client.tasks.booking-task.index', compact('title', 'subAccounts', 'tasks', 'title','filters','campaigns'));
    }

   /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Add Booking API';
		$client = $this->context->getInstance();
        $subAccount = null;
        // $taskTypes = TaskType::pluck('title', 'id'); // Di pa need
        $jobs = null;
		if ($request->filled('sub_account_id')) {
			$subAccount = $client->subAccounts->firstWhere('id', $request->sub_account_id);
		} elseif( $request->filled('subaccount_id') ) {
			$subAccount = $client->subAccounts->firstWhere('id', $request->subaccount_id);
		} else {
			$subAccount = $client->subAccounts->first();
		}

        if ($subAccount) {
            $subAccounts = $request->user()->client->subAccounts->pluck('account_name', 'id');
            $campaigns = $this->campaignRepo->search($request, [], true)->pluck('name', 'id'); // campaigns
            $cities = City::pluck('name', 'name'); // get all cities
            $messagePlaceholder = "Hi [first_name] [last_name]! Thank you for trusting [business_name].\n\nThis is to confirm your Booking with us on [date_time] with the service of [items].";
            $reminderMessagePlaceholder = "[business_name] appointment reminder: [items] tomorrow at [time].\n\nAny questions please call [business_phone_number].";
            $shortCodes = Shortcodes::$bookingShortcodes;

            return view('client.tasks.booking-task.create', compact('title', 'subAccount', 'campaigns', 'cities', 'shortCodes', 'messagePlaceholder','reminderMessagePlaceholder', 'subAccounts','client', 'jobs'));
        } else {
            abort('404');
        }

        return view('client.tasks.booking-task.create', compact('title', 'subAccount', 'campaigns', 'cities', 'shortCodes', 'messagePlaceholder', 'subAccounts','client', 'jobs', 'reminderMessagePlaceholder'));
    }

   /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Task $task)
    {
        $title = 'Edit Task';
        $client = $this->context->getInstance();
        $request['sub_account_id'] = $task->subaccount_id;

        $subAccount = $task->subAccount;
        $subAccounts = $request->user()->client->subAccounts->pluck('account_name', 'id');

        $messagePlaceholder =  "Hi [first_name] [last_name]! Thank you for trusting [business_name].\n\nThis is to confirm your Booking with us on [date_time] with the service of [items].";
        $reminderMessagePlaceholder = "[business_name] appointment reminder: [items] tomorrow at [time].\n\nAny questions please call [business_phone_number].";

        $campaigns = $this->campaignRepo->search($request, [], true)->pluck('name', 'id'); // campaigns
        $cities = City::pluck('name', 'name'); // get all cities

        // $shortCodes = Shortcodes::$oneTimeshortCodes;
        $shortCodes = Shortcodes::$bookingShortcodes;

        $request['parent_task_id'] = $task->id;
        $request['campaign_id'] = $task->campaign_id;
        $subName =  $this->subAccountRepo->search($request, [])->first();
        $camp =  $this->campaignRepo->search($request)->first();
        $sendImmediate = ( $task->date_to_send == NULL && $task->time_to_send == NULL ) ? true : false;
        $smsReminder = $this->bookingTaskRepo->search($request, [], FALSE);
        $daysToCarbon = ( $task->date_to_send != NULL ) ? new Carbon($task->date_to_send) : $task->date_to_send;
        $timeToCarbon = ( $task->time_to_send != NULL ) ? new Carbon($task->time_to_send) : $task->time_to_send;
		$timeComparison = $timeToCarbon ? $timeToCarbon->copy()->startOfDay()->addHour() : null;

        $day = ( $daysToCarbon != NULL ) ? $task->ots_days : NULL;
        $hours = '0 hour';
        if ($timeComparison && $timeToCarbon->gte($timeComparison)) {
			$hours = ( $timeToCarbon != NULL ) ? ( $timeToCarbon->format('g') > 1 ? $timeToCarbon->format('g').' hours' : $timeToCarbon->format('g'). ' hour')  : '00';
		}

        $mins = ( $timeToCarbon != NULL ) ? $timeToCarbon->format('i').' mins' : '00';

        $sms_to_be_send = 0;
        $sent_sms = 0;
        $failed_sms = 0;
        $credits_consumed = 0;

        $post = array(
            'apiUrl' => $task->getApiUrl(),
            'id' => $task->id,
            'subaccount_name' => optional($subName)->account_name,
            'subaccount_id' => $task->subaccount_id,
            'send_immediate' => $sendImmediate,
            'sms_reminder' => ($smsReminder->count() > 0) ? true : false,
            'sms_notification' => ( $task->send_notif > 0 ) ? true : false,
            'status' => ($task->active) ? true : false,
            'campaign' => [
                'label' => optional($camp)->name,
                'value' => optional($camp)->id,
            ],
            'message' => $task->message,
            'send_after' => [
                'days' => $day,
                'hours' => $hours,
                'mins' => $mins,
            ],
            'toBeRemoved' => [],
			'subacc_security_key' => $task->subacc_security_key,
			'task_key' => $task->task_key
        );

        $sms_to_be_send += (int) $task->jobs_count;
        $sent_sms += (int) $task->success_jobs_count;
        $failed_sms += (int) $task->jobs_failed;
        $credits_consumed += (int) ($task->actual_credits_consumed) ? $task->actual_credits_consumed : 0;

        $sms_reminder_holder = [];
        $jobsReminder = [$task['id']];
        $smsReminder->each(function($item, $index) use ( &$sms_reminder_holder, &$request, &$jobsReminder, &$sms_to_be_send, &$sent_sms, &$failed_sms, &$credits_consumed ) {

            $daysToCarbon = ( $item->date_to_send != NULL ) ? new Carbon($item->date_to_send) : $item->date_to_send;
            $timeToCarbon = ( $item->time_to_send != NULL ) ? new Carbon($item->time_to_send) : $item->time_to_send;
			$timeComparison = $timeToCarbon ? $timeToCarbon->copy()->startOfDay()->addHour() : null;
            $day = ( $daysToCarbon != NULL ) ? $item->ots_days : NULL;
			$hours = '0 hour';
			if ($timeComparison && $timeToCarbon->gte($timeComparison)) {
				$hours = ($timeToCarbon != NULL) ? ($timeToCarbon->format('g') > 1 ? $timeToCarbon->format('g') . ' hours' : $timeToCarbon->format('g') . ' hour') : '00';
			}
            $mins = ( $timeToCarbon != NULL ) ? $timeToCarbon->format('i').' mins': '00';

            $sms_reminder_holder[] = array(
                'id' => $item->id,
                'message' => $item->message,
                'send_before' => [
                    'days' => $day,
                    'hours' => $hours,
                    'mins' => $mins
                ],
            );

            $sms_to_be_send += (int) $item->jobs_count;
            $sent_sms += (int) $item->success_jobs_count;
            $failed_sms += (int) $item->jobs_failed;
            $credits_consumed += (int) ($item->actual_credits_consumed) ? $item->actual_credits_consumed : 0;

            $jobsReminder[] = $item->id;
        });

        $post['sms_to_be_send'] = $sms_to_be_send;
        $post['sent_sms'] = $sent_sms;
        $post['failed_sms'] = $failed_sms;
        $post['credits_consumed'] = $credits_consumed;
        $post['sms_reminder_holder'] = $sms_reminder_holder;
        $task = $post;

        $task_ids = "(" . implode(",", $jobsReminder) . ")";
        $request['task_ids'] = $jobsReminder;
        $request['per_page'] = 50;
        $request['offset'] = 1;
        $request['page'] = 1;

        $jobs = $this->baseJob->getAllJobs($request, true, false, true);

        return view('client.tasks.booking-task.edit', compact('task','subAccount', 'client', 'campaigns', 'title', 'shortCodes', 'subAccounts', 'jobs', 'messagePlaceholder', 'reminderMessagePlaceholder'));
    }

    public function destroy(Task $task, Request $request)
	{
		if ($task->hasJobs()) {
			return redirect()
				->back()
				->withErrors('This task has current jobs, deletion is not allowed.');
        }

		$this->bookingTaskRepo->delete($task, $request);

		return redirect()
			->back()
			->withSuccess('Task deleted!');
	}

    /**
     * =====================================
     * API CALLS
     * =====================================
     * Script below are use for api calls
     * =====================================
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $currentSub = $this->subAccountRepo->find($request->params['sub_account_id']);
            if ($currentSub) {
                $hours = ( $request->data['send_after']['hours'] != null ) ? preg_replace('/\D/', '', $request->data['send_after']['hours']) : null;
                $mins = ( $request->data['send_after']['mins'] != null ) ? preg_replace('/\D/', '', $request->data['send_after']['mins']) : null;

                $client = $request->user()->client;
                $request['task_type_id'] = \ATM\Repositories\Tasks\Task::BOOKING_TASK;
                $request['time_to_send'] = Carbon::now()->startOfDay()->hour($hours)->minute($mins)->format('H:i');
                $request['prevent_job_fetch'] = false;

                $task = $this->bookingTaskRepo->add($request, $client);

                return response()->json([ 'success' => true, 'message' => 'Successfully added A New Task.' ]);

            } else {
                abort('404');
            }

        } catch (ValidationException $e) {
            $errorMessage = $e->getErrors();
            return response()->json(['sucess' => false, 'message' => $errorMessage]);
        }
    }

        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task) {
        $client = $request->user()->client;

        try {
            $hours = ( isset($request->data['send_after']['hours']) ) ? preg_replace('/\D/', '', $request->data['send_after']['hours']) : null;
            $mins = ( isset($request->data['send_after']['mins']) ) ? preg_replace('/\D/', '', $request->data['send_after']['mins']) : null;

            $request['task_type_id'] = \ATM\Repositories\Tasks\Task::BOOKING_TASK;
            $request['time_to_send'] = Carbon::now()->startOfDay()->hour($hours)->minute($mins)->format('H:i');
            $request['prevent_job_fetch'] = false;

            $task = $this->bookingTaskRepo->update($request->params['taskID'], $request, $client);
            return response()->json([ 'success' => true, 'message' => 'Successfully Updated Task.' ]);

        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    public function addJob(Request $request)
    {
        try {
            $task = $request->app_key ? $this->taskRepo->searchByKey($request->app_key) : null;

            $taskHolder = collect();
            $request['parent_task_id'] = $task->id;
            $taskHolder->push($task);

            if( isset($task->has_children) ) {
                $taskChild = $this->bookingTaskRepo->search($request, [], false);
                $taskChild->each(function($item, $key) use (&$taskHolder) {
                    $taskHolder->push($item);
                });
            }

            if ($task) {

                $result = $this->bookingTaskRepo->addJob($request, $task, $taskHolder);

                return response()->json(['success' => true, 'message' => $result]);
            } else {
                throw new \ATM\Validation\ValidationException($this->messageBag->add('fatal_error', 'Something went wrong. Please contact administrator.'));
            }

        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e, true);
        }
    }

    public function getPage( Request $request ) {
        try {
            if( $request ) {
                if( !is_array($request->task_ids) ) {
                    $request['task_ids'] = explode(",",$request->task_ids);
                }

                $jobs = $this->baseJob->getAllJobs($request, true, false, true);

                return response()->json(['success' => true, 'jobs' => $jobs]);
            }

        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e, true);
        }
    }
}
