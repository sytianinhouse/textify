<?php

namespace App\Http\Controllers\Client\Task\BirthdayTask;

use App\Http\Controllers\Controller;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Shortcodes\Shortcodes;
use ATM\Repositories\Tasks\BirthdayTaskRepository;
use ATM\Validation\ValidationException;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BirthdayTaskController extends Controller
{
	public function __construct(
		BirthdayTaskRepository $birthdayRepo,
		CampaignRepository $campaignRepo
	)
	{
		$this->birthdayRepo = $birthdayRepo;
		$this->campaignRepo = $campaignRepo;
	}

	public function create(Request $request)
	{
		$title = 'Birthday SMS';

		$client = $request->user()->client;
		$subAccounts = $client->subAccounts()->actives()->get();

		$request['subaccount_ids'] = $subAccounts->pluck('id');
		$tasks = $this->birthdayRepo->getTask($request)->keyBy('subaccount_id');

		$shortCodes = Shortcodes::$shortCodes;
		$curMnth = new Carbon();

		return view('client.tasks.birthday-task.create', compact('title', 'tasks', 'subAccounts', 'client', 'shortCodes', 'curMnth'));
	}

	public function store(Request $request)
	{
		try {

			$task = $this->birthdayRepo->updateTask($request);

			if ($task){
				return redirect()
					->route('client.task.birthday-task.create')
					->withSuccess("Birthday SMS settings saved succesfully.");
			}
		} catch (ValidationException $e) {
			return $this->redirectFormRequest($e);
		}
	}
}