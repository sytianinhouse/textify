<?php

namespace App\Http\Controllers\Client\Task\ImmediateTask;

use ATM\ContextInterface;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Sms\SmsFacade;
use ATM\Repositories\Tasks\BaseJob;
use ATM\Repositories\Tasks\ImmediateTaskRepository;
use ATM\Repositories\Tasks\JobsFailed\JobsFailed;
use ATM\Repositories\Tasks\Task;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ImmediateTaskController extends Controller
{
	public function __construct(
		CampaignRepository $campaignRepo,
		ImmediateTaskRepository $immediateTaskRepo,
		BaseJob $baseJob,
		ContextInterface $context
	)
	{
		$this->immediateTaskRepo = $immediateTaskRepo;
		$this->campaignRepo = $campaignRepo;
		$this->baseJob = $baseJob;
		$this->context  = $context;
	}

	public function index(Request $request)
	{
		$title = 'Custom API';
		$user  = $request->user();

		$subAccounts = $user->client->subAccounts->pluck('account_name', 'id');
		$campaigns = $this->campaignRepo->forFiltersList($request);

		$request['per_page'] = self::PER_PAGE;
		$tasks = $this->immediateTaskRepo->search(
			$request,
			['tasksJobToday', 'tasksJobTomorrow', 'tasksJobAfterTomorrowTo14thDay', 'tasksJob15thDayToEndMonth', 'tasksJobNextMonth', 'tasksjobsCompleted', 'tasksjobsFailed']
		);

		$filters = [
			'filter_date_by' => $request->filter_date_by,
			'campaign_filter' => $request->campaign_filter,
			'subaccount_filter' => $request->subaccount_filter,
			'per_page' => $request->per_page,
		];

		return view('client.tasks.immediate-task.index', compact( 'tasks', 'title','subAccounts','filters','campaigns'));
    }

	public function create(Request $request)
	{
		$client = $this->context->getInstance();
		$title = 'Custom API';
		$subAccount = null;
		$tasks = [];

		if ($request->filled('sub_account_id')) {
			$subAccount = $client->subAccounts->firstWhere('id', $request->sub_account_id);
		} elseif( $request->filled('subaccount_id') ) {
			$subAccount = $client->subAccounts->firstWhere('id', $request->subaccount_id);
		} else {
			$subAccount = $client->subAccounts->first();
		}

        if ($subAccount) {
            $campaigns = $subAccount->campaigns->pluck('name', 'id');

            return view('client.tasks.immediate-task.create', compact( 'client', 'subAccount', 'tasks', 'title', 'campaigns'));
        } else {
            abort('404');
        }
	}

	public function store(Request $request)
	{
		try {
			$client = $this->context->getInstance();

			$request['active'] = ($request->active) ? $request->active : 1;
			$request['client_id'] = $client->id;
			$task = $this->immediateTaskRepo->addTask($request);

			return redirect()
				->route('client.task.it.edit', $task)
				->withSuccess("Created succesfully!");

		} catch (ValidationException $e) {
			return $this->redirectFormRequest($e);
		}
	}

	public function edit(Task $task, Request $request)
	{
		$client = $this->context->getInstance();
		$title = 'Edit Custom API';

		$subAccount = $task->subAccount;
		$campaigns = $subAccount->campaigns->pluck('name', 'id');

		$request['task_id'] = $task->id;
		$jobs = $this->baseJob->getAllJobs($request, true);

		return view('client.tasks.immediate-task.edit', compact( 'client', 'subAccount', 'task', 'title', 'campaigns', 'jobs'));
	}

	public function update(Task $task, Request $request)
	{
		try {
			$task = $this->immediateTaskRepo->addTask($request, $task);

			return redirect()
				->route('client.task.it.edit', $task)
				->withSuccess("Updated succesfully!");

		} catch (ValidationException $e) {
			return $this->redirectFormRequest($e);
		}
	}

	public function destroy(Task $task, Request $request)
	{
		if ($task->hasJobs()) {
			return redirect()
				->back()
				->withErrors('This task has current jobs, deletion is not allowed.');
		}

		$this->immediateTaskRepo->removeTask($task);

		return redirect()
			->back()
			->withSuccess('Task deleted!');
	}
}
