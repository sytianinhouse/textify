<?php

namespace App\Http\Controllers\Client\Task\OneTimeSend;

use ATM\ContextInterface;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Cities\City;
use ATM\Repositories\Client\Client;
use ATM\Repositories\CustomerGroups\CustomerGroupsRepository;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Shortcodes\Shortcodes;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\Tasks\BaseJob;
use ATM\Repositories\Tasks\OneTimeTaskRepository;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Tasks\TaskType\TaskType;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OneTimeSendController extends Controller
{
    public function __construct (
        CampaignRepository $campaignRepo,
        SubAccountRepository $subAccountRepo,
        CustomersRepository $customerRepo,
        CustomerGroupsRepository $customerGroupRepo,
        OneTimeTaskRepository $oneTimeTaskRepo,
		BaseJob $baseJob,
		ContextInterface $context
    )
    {
        $this->customerGroupRepo = $customerGroupRepo;
        $this->subAccountRepo = $subAccountRepo;
        $this->campaignRepo   = $campaignRepo;
        $this->customerRepo = $customerRepo;
        $this->oneTimeTaskRepo = $oneTimeTaskRepo;
        $this->context = $context;
        $this->baseJob = $baseJob;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Template API';

        $subAccounts = $request->user()->client->subAccounts()->actives()->get()->pluck('account_name', 'id');

        $request['filter_task_type_id'] = \ATM\Repositories\Tasks\Task::ONE_TIME_SEND;
		$campaigns = $this->campaignRepo->forFiltersList($request);
        $tasks = $this->oneTimeTaskRepo->search(
        	$request,
			[
            'tasksJobToday', 
            // 'tasksJobTomorrow', 
            // 'tasksJobAfterTomorrowTo14thDay', 
            // 'tasksJob15thDayToEndMonth', 
            // 'tasksJobNextMonth', 
            // 'tasksjobsCompleted', 
            // 'tasksjobsFailed'
            ]
		);

        $filters = [
        	'filter_search' => $request->filter_search,
        	'campaign_filter' => $request->campaign_filter,
            'per_page' => $request->per_page,
            'filter_date_by' => $request->filter_date_by,
            'subaccount_filter' => $request->subaccount_filter,
		];

        return view('client.tasks.one-time-send.index', compact('title', 'subAccounts', 'tasks', 'title','filters','campaigns'));
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Add Template API';
		$client = $this->context->getInstance();
		$subAccount = null;
        // $taskTypes = TaskType::pluck('title', 'id'); // Di pa need

		if ($request->filled('sub_account_id')) {
			$subAccount = $client->subAccounts->firstWhere('id', $request->sub_account_id);
		} elseif( $request->filled('subaccount_id') ) {
			$subAccount = $client->subAccounts->firstWhere('id', $request->subaccount_id);
		} else {
			$subAccount = $client->subAccounts->first();
		}

        if ($subAccount) {
            $subAccounts = $request->user()->client->subAccounts->pluck('account_name', 'id');
            $campaigns = $this->campaignRepo->search($request, [], true)->pluck('name', 'id'); // campaigns
            $cities = City::pluck('name', 'name'); // get all cities
            $messagePlaceholder = "Thank you for visiting us [first_name] [last_name]! \n\nPlease come see us again.";
            $shortCodes = Shortcodes::$oneTimeshortCodes;

            return view('client.tasks.one-time-send.create', compact('title', 'subAccount', 'campaigns', 'cities', 'shortCodes', 'messagePlaceholder', 'subAccounts'));
        } else {
            abort('404');
        }

        return view('client.tasks.one-time-send.create', compact('title', 'subAccount', 'campaigns', 'cities', 'shortCodes', 'messagePlaceholder', 'subAccounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $currentSub = $this->subAccountRepo->find($request->sub_account_id);

            if ($currentSub) {
                $client = $request->user()->client;
                $request['task_type_id'] = \ATM\Repositories\Tasks\Task::ONE_TIME_SEND;
                // $request['time_to_send'] = Carbon::now()->startOfDay()->hour($request->time_to_send[0])->minute($request->time_to_send[1])->format('H:i');

                $timeToSend = Carbon::now()->createFromTimeString('10:00');
                if ($request->filled('time_to_send.0')) {
                    $timeToSend = $timeToSend->hour($request->time_to_send[0]);
                }

                if ($request->filled('time_to_send.1')) {
                    $timeToSend = $timeToSend->minute($request->time_to_send[1]);
                }

                $request['time_to_send'] = $timeToSend->format('H:i');
                $request['prevent_job_fetch'] = false;

                $task = $this->oneTimeTaskRepo->add($request, $client, 'one_time_send');

                return redirect()->route('client.task.ots.index')
                            ->withSuccess("Successfully added A New Task.");
            } else {
                abort('404');
            }

        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Task $task)
    {
        $title = 'Edit Task';

        $request['sub_account_id'] = $task->subaccount_id;

        $subAccount = $task->subAccount;
        $subAccounts = $request->user()->client->subAccounts->pluck('account_name', 'id');

        $campaigns = $this->campaignRepo->search($request, [], true)->pluck('name', 'id'); // campaigns
        $cities = City::pluck('name', 'name'); // get all cities

        $shortCodes = Shortcodes::$oneTimeshortCodes;

		$request['task_id'] = $task->id;
		$jobs = $this->baseJob->getAllJobs($request, true);

        return view('client.tasks.one-time-send.edit', compact('task','subAccount', /* 'project', 'client', */ 'subAccount', 'campaigns', /* 'selectedCampaign', */ 'title', 'shortCodes', /* 'messagePlaceholder', */ 'subAccounts', 'jobs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        try {
            $client = $request->user()->client;

            // $request['time_to_send'] = Carbon::now()->startOfDay()->hour($request->time_to_send[0])->minute($request->time_to_send[1])->format('H:i');
            // $request['time_to_send'] = Carbon::now()->startOfDay()->hour($request->time_to_send[0])->minute($request->time_to_send[1])->format('H:i');

            $timeToSend = Carbon::now()->createFromTimeString('10:00');
            if ($request->filled('time_to_send.0')) {
                $timeToSend = $timeToSend->hour($request->time_to_send[0]);
            }

            if ($request->filled('time_to_send.1')) {
                $timeToSend = $timeToSend->minute($request->time_to_send[1]);
            }

            $request['time_to_send'] = $timeToSend->format('H:i');
            $request['prevent_job_fetch'] = false;

            $task = $this->oneTimeTaskRepo->update($task->id, $request, $client);

            if ( !$task->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($task->getErrors())
                            ->withError("Please check required fields.");
            }
            return redirect()->route('client.task.ots.index')
                        ->withSuccess("Successfully Updated Task.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Task $task)
    {
        try {

            $request['dont_detach'] = true;
            $this->oneTimeTaskRepo->delete($request, $task->id);

            return redirect()->route('client.task.ots.index')
                        ->withSuccess("Task successfully deleted.");
        } catch (\DomainException $e) {
            return redirect()->back()
                            ->withError($e->getMessage());
        }
    }

    /**
     * Able to create quick campaign creation
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajaxCreateCampaign(Request $request, SubAccount $subaccount, Campaign $campaign) {

        try {
            $this->campaignRepo->store($campaign, $request);

            $campaign = $this->campaignRepo->search($request)->sortBy('id');
            $lastCampaign = $campaign->last();

            $campaign = $campaign->pluck('name', 'id');

            return view('partials.task.campaign-options', compact('campaign', 'lastCampaign'));
        } catch (ValidationException $e) {
            return response()->json(['success' => false]);
        }
    }

}
