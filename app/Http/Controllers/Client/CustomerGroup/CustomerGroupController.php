<?php

namespace App\Http\Controllers\Client\CustomerGroup;

use ATM\Repositories\Client\Client;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\CustomerGroups\CustomerGroupsRepository;
use ATM\Repositories\Customers\Customer;

use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Project\Project;

use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;

use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class CustomerGroupController extends Controller
{
	public $customerGroupRepo;

	public function __construct(CustomerGroupsRepository $customerGroupRepo, SubAccountRepository $subAccountRepo)
    {
        $this->customerGroupRepo = $customerGroupRepo;
        $this->subAccountRepo = $subAccountRepo;
    }

    public function index(Request $request, Project $project, Client $client)
    {
        $title       = 'Groups';
        $customergroup   = $this->customerGroupRepo->search($request);

        $filters = [
        	'filter_search' => $request->filter_search,
			'per_page' => $request->per_page
		];

        return view('client.customergroups.index', compact('title', 'customergroup', 'filters' ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Project $project, Client $client, Customer $customer)
    {
        $client =  $request->user()->client;
        $title = $client->company_name . ' - Add Group';

        $request['active_only'] = true;
        // $subaccounts = $this->subAccountRepo->search($request, [], true)->pluck('account_name', 'id');
        $selectedSubAccnt = collect();

        return view('client.customergroups.create', compact('project', 'client', /* 'customergroup', */ 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Client $client, Request $request)
    {

        try {

            $input = $request->all();

            $rules = [
                'name' => 'required',
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } else {
                $customerGroup = $this->customerGroupRepo->add($request, $client);
            }

            if ( !$customerGroup->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($customerGroup->getErrors())
                            ->withError("Please check required fields.");
            }

            return redirect()->route('client.customer-group.index')
                        ->withSuccess("Successfully added Group.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, CustomerGroup $customergroup)
    {
        $client =  $request->user()->client;
        $title = $client->company_name . ' - Edit Group';
       
        // $request['active_only'] = true;
        // $subaccounts = $this->subAccountRepo->search($request, [], true)->pluck('account_name', 'id');
        // $selectedSubAccnt = $customergroup->subAccounts->pluck('id');

        return view('client.customergroups.edit', compact('project', 'client', 'customergroup', 'subaccounts' , 'selectedSubAccnt' ,'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project, Client $client, CustomerGroup $customergroup)
    {
        try {

            $customerGroup = $this->customerGroupRepo->update($customergroup, $request);

            if ( !$customerGroup->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($customerGroup->getErrors())
                            ->withError("Please check required fields.");
            }

            return redirect()->route('client.customer-group.index')
                        ->withSuccess("Successfully Updated Group.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Client $client, CustomerGroup $customergroup)
    {
        try {

            $customerGroup = $this->customerGroupRepo->delete($customergroup);

            return redirect()->route('client.customer-group.index')
                        ->withSuccess("Successfully deleted $customerGroup.");
        } catch (\DomainException $e) {
            return redirect()->back()
                            ->withError($e->getMessage());
        }
    }
}
?>
