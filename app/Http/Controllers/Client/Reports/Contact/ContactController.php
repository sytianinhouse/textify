<?php

namespace App\Http\Controllers\Client\Reports\Contact;

use ATM\FileGenerator;
use App\Http\Controllers\Controller;
use ATM\Repositories\Cities\City;
use ATM\Repositories\CustomerGroups\CustomerGroupsRepository;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\SubAccount\SubAccountRepository;
use Illuminate\Http\Request;

class ContactController extends Controller
{
	public function __construct(
		CustomersRepository $customersRepo,
		SubAccountRepository $subRepo,
		CustomerGroupsRepository $groupRepo
	)
	{
		$this->customersRepo = $customersRepo;
		$this->subRepo = $subRepo;
		$this->groupRepo = $groupRepo;
	}

	public function statistics(Request $request)
	{
		$title = 'Contacts Information';
		$begin = $request->filled('start_filter') && $request->start_filter ? true : false;
		$subaccounts = $this->subRepo->all()->pluck('account_name', 'id');
		$moreFilterGroups = $this->groupRepo->all()->pluck('name', 'id');

		$getCities = City::get();
		$moreFilterCities = [];
		$getCities->sortBy('name')->each(function($value,$key) use (&$moreFilterCities){
			$name = "{$value['name']}, {$value->province->name}";
			$moreFilterCities[$value['id']] = $name;
		});

		$contact = null;

		if ($begin) {
			$contact = $this->customersRepo->search($request);

			if ($request->filled('export_excel')) {

				$moneyColumn = [];
				$namePrefix = '';

				$reportTitle = 'Statistics';
				FileGenerator::makeExcel(
					'',
					$namePrefix . ' - Statistics - ' . $request['year'],
					'Statistics',
					'partials.report-export.jobs.customer-statistics',
					compact('contact', 'subaccounts'),
					false,
					$moneyColumn
				);
			}
		}

		$filters = [
			'start_filter' => $request->start_filter,
			'filter_search' => $request->filter_search,
			'filter_group' => $request->filter_group,
			'filter_gender' => $request->filter_gender,
			'filter_city' => $request->filter_city,
			'from_date' => $request->from_date,
			'to_date' => $request->to_date,
			'sub_account_id' => $request->sub_account_id,
			'per_page' => $request->per_page,
		];

		return view('client.reports.contacts.information', compact('title', 'begin', 'filters', 'contact', 'subaccounts', 'moreFilterGroups', 'moreFilterCities'));
	}
}