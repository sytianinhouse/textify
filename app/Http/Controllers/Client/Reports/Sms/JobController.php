<?php

namespace App\Http\Controllers\Client\Reports\Sms;

use ATM\Date;
use ATM\FileGenerator;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Reports\ReportsRepository;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\Tasks\Task;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class JobController extends Controller
{

    public function __construct
    (
        ReportsRepository $reportsRepo,
        SubAccountRepository $subRepo,
        CampaignRepository $campaignRepo
    )
    {
        $this->reportsRepo = $reportsRepo;
        $this->subRepo = $subRepo;
        $this->campaignRepo = $campaignRepo;
    }

    public function smsPerMonth(Request $request)
    {
        $title = 'SMS Per Month';

        $begin = $request->filled('start_filter') && $request->start_filter ? true : false;

        $filterYears = Carbon::now()->format('Y');
        $filterTaskTypes = Task::$typesArr;
		$campaigns = $this->campaignRepo->forFiltersList($request);
        $subaccounts = $this->subRepo->all()->pluck('account_name', 'id');

		$results = [];

        if ($begin) {

            $results = $this->reportsRepo->smsPerMonth($request);

            if ($request->filled('export_excel')) {

                $moneyColumn = [];
                $namePrefix = '';
                $client = $request->user()->client;

                if ($request->filled('sub_account_id')) {
                    $subAccount = $this->subRepo->find($request->sub_account_id);
                    $namePrefix = $client->company_name . ' - ' . $subAccount->account_name;
                } else {
                    $namePrefix = $client->company_name;
                }

				$reportTitle = 'SMS Per Month';
                FileGenerator::makeExcel(
                    '',
                    $namePrefix . ' - SMS per Month - ' . $request['year'],
                    'SMS per Month',
                    'partials.report-export.jobs.jobs-per-month',
                    compact('results', 'subAccount', 'reportTitle'),
                    false,
                    $moneyColumn
                );
            }
        }

        $filters = [
            'start_filter' => $request->start_filter,
            // 'month' => $request->month,
            'year' => $request->year,
            'task_type_id' => $request->task_type_id,
            'campaign_filter' => $request->campaign_filter,
            'sub_account_id' => $request->sub_account_id,
        ];

        return view('client.reports.sms.jobs.per-month', compact('title', 'begin', 'filters', 'subaccounts', 'filterYears', 'filterTaskTypes', 'campaigns', 'results'));
    }

	public function smsPerDay(Request $request)
	{
		$title = 'SMS Per Day';

		$begin = $request->filled('start_filter') && $request->start_filter ? true : false;

		$filterTaskTypes = Task::$typesArr;
		$campaigns = $this->campaignRepo->forFiltersList($request);
		$subaccounts = $this->subRepo->all()->pluck('account_name', 'id');

		$results = [];

		if ($begin) {

			$results = $this->reportsRepo->smsPerDay($request);

			$request['from_date'] = Carbon::parse($request->from_date)->toDateString();
			$request['to_date'] = Carbon::parse($request->to_date)->toDateString();

			if ($request->filled('export_excel')) {

				$client = $request->user()->client;
				$namePrefix = '';
				$moneyColumn = [];

				if ($request->filled('sub_account_id')) {
					$subAccount = $this->subRepo->find($request->sub_account_id);
					$namePrefix = $client->company_name . ' - ' . $subAccount->account_name;
				} else {
					$namePrefix = $client->company_name;
				}

				$reportTitle = 'SMS Per Day';
				FileGenerator::makeExcel(
					'',
					$namePrefix . ' - SMS per Day - ' . $request['from_date'] . ' - ' . $request['to_date'],
					'SMS per Day',
					'partials.report-export.jobs.jobs-per-day',
					compact('results', 'subAccount', 'reportTitle'),
					false,
					$moneyColumn
				);
			}
		}

		$filters = [
			'start_filter' => $request->start_filter,
			'from_date' => $request->from_date,
			'to_date' => $request->to_date,
			'task_type_id' => $request->task_type_id,
			'campaign_filter' => $request->campaign_filter,
			'sub_account_id' => $request->sub_account_id,
		];

		return view('client.reports.sms.jobs.per-day', compact('title', 'begin', 'filters', 'subaccounts', 'filterTaskTypes', 'campaigns', 'results'));
	}

	public function smsPerSubAccount(Request $request)
	{
		$title = 'SMS per Sub-Account';

		$begin = $request->filled('start_filter') && $request->start_filter ? true : false;

		$filterTaskTypes = Task::$typesArr;
		$campaigns = $this->campaignRepo->forFiltersList($request);
		$subaccounts = $this->subRepo->all()->pluck('account_name', 'id');

		$results = [];

		if ($begin) {

			$results = $this->reportsRepo->smsPerSubAccount($request, null);

			if ($request->filled('export_excel')) {

				$moneyColumn = [];
				$namePrefix = '';
				$client = $request->user()->client;

				if ($request->filled('sub_account_id')) {
					$subAccount = $this->subRepo->find($request->sub_account_id);
					$namePrefix = $client->company_name . ' - ' . $subAccount->account_name;
				} else {
					$namePrefix = $client->company_name;
				}

				$reportTitle = 'SMS Per Sub Account';
				FileGenerator::makeExcel(
					'',
					$namePrefix . ' - SMS Sent - ' . $request['year'],
					'SMS Sent',
					'partials.report-export.jobs.jobs-per-sub-account',
					compact('results', 'subAccount', 'subaccounts', 'reportTitle'),
					false,
					$moneyColumn
				);
			}
		}

		$filters = [
			'start_filter' => $request->start_filter,
			'from_date' => $request->from_date,
			'to_date' => $request->to_date,
			'task_type_id' => $request->task_type_id,
			'campaign_filter' => $request->campaign_filter,
			'sub_account_id' => $request->sub_account_id,
		];

		return view('client.reports.sms.jobs.per-sub-account', compact('title', 'begin', 'filters', 'subaccounts', 'filterTaskTypes', 'campaigns', 'results'));
	}

	public function smsSent(Request $request)
	{
		$title = 'SMS Sent';

		$begin = $request->filled('start_filter') && $request->start_filter ? true : false;

		$filterTaskTypes = Task::$typesArr;
		$campaigns = $this->campaignRepo->forFiltersList($request);
		$subaccounts = $this->subRepo->all()->pluck('account_name', 'id');
		$subAccount = null;

		if (!$request->filled('per_page')) {
			$request['per_page'] = self::PER_PAGE;
		}

		$results = [];

		if ($begin) {
			$results = $this->reportsRepo->successSms($request);

			if ($request->filled('export_excel')) {

				$moneyColumn = [];
				$namePrefix = '';
				$client = $request->user()->client;

				if ($request->filled('sub_account_id')) {
					$subAccount = $this->subRepo->find($request->sub_account_id);
					$namePrefix = $client->company_name . ' - ' . $subAccount->account_name;
				} else {
					$namePrefix = $client->company_name;
				}

				$reportTitle = 'SMS Sent';
				FileGenerator::makeExcel(
					'',
					$namePrefix . ' - SMS Sent - ' . $request['year'],
					'SMS Sent',
					'partials.report-export.jobs.jobs-success',
					compact('results', 'subAccount', 'subaccounts', 'reportTitle'),
					false,
					$moneyColumn
				);
			}
		}

		$filters = [
			'start_filter' => $request->start_filter,
			'from_date' => $request->from_date,
			'to_date' => $request->to_date,
			'task_type_id' => $request->task_type_id,
			'campaign_filter' => $request->campaign_filter,
			'sub_account_id' => $request->sub_account_id,
			'per_page' => $request->per_page
		];

		return view('client.reports.sms.jobs.sms-success', compact('title', 'begin', 'filters', 'subaccounts', 'filterTaskTypes', 'campaigns', 'results'));
    }

	public function smsPending(Request $request)
	{
		$title = 'SMS Pending';

		$begin = $request->filled('start_filter') && $request->start_filter ? true : false;

		$results = [];
		$filterTaskTypes = Task::$typesArr;
		$campaigns = $this->campaignRepo->forFiltersList($request);
		$subaccounts = $this->subRepo->all()->pluck('account_name', 'id');

		if ($begin) {
			$results = $this->reportsRepo->smsPending($request, null);

			if ($request->filled('export_excel')) {

				$moneyColumn = [];
				$namePrefix = '';
				$client = $request->user()->client;

				if ($request->filled('sub_account_id')) {
					$subAccount = $this->subRepo->find($request->sub_account_id);
					$namePrefix = $client->company_name . ' - ' . $subAccount->account_name;
				} else {
					$namePrefix = $client->company_name;
				}

				$reportTitle = 'SMS Pending';
				FileGenerator::makeExcel(
					'',
					$namePrefix . ' - SMS Pending - ' . $request['year'],
					'SMS Sent',
					'partials.report-export.jobs.jobs-pending',
					compact('results', 'subAccount', 'subaccounts', 'reportTitle'),
					false,
					$moneyColumn
				);
			}
		}

		$filters = [
			'start_filter' => $request->start_filter,
			'from_date' => $request->from_date,
			'to_date' => $request->to_date,
			'task_type_id' => $request->task_type_id,
			'campaign_filter' => $request->campaign_filter,
			'sub_account_id' => $request->sub_account_id,
			'per_page' => $request->per_page
		];

		return view('client.reports.sms.jobs.sms-pending', compact('title', 'begin', 'filters', 'subaccounts', 'filterTaskTypes', 'campaigns', 'results'));
    }

	public function smsFailed(Request $request)
	{
		$title = 'SMS Failed';

		$begin = $request->filled('start_filter') && $request->start_filter ? true : false;

		$filterTaskTypes = Task::$typesArr;
		$campaigns = $this->campaignRepo->forFiltersList($request);
		$subaccounts = $this->subRepo->all()->pluck('account_name', 'id');

		$results = [];
		if ($begin) {
			$results = $this->reportsRepo->smsFailed($request, null);

			if ($request->filled('export_excel')) {

				$moneyColumn = [];
				$namePrefix = '';
				$client = $request->user()->client;

				if ($request->filled('sub_account_id')) {
					$subAccount = $this->subRepo->find($request->sub_account_id);
					$namePrefix = $client->company_name . ' - ' . $subAccount->account_name;
				} else {
					$namePrefix = $client->company_name;
				}

				$reportTitle = 'SMS Failed';
				FileGenerator::makeExcel(
					'',
					$namePrefix . ' - SMS Failed - ' . $request['year'],
					'SMS Sent',
					'partials.report-export.jobs.jobs-failed',
					compact('results', 'subAccount', 'subaccounts', 'reportTitle'),
					false,
					$moneyColumn
				);
			}
		}

		$filters = [
			'start_filter' => $request->start_filter,
			'from_date' => $request->from_date,
			'to_date' => $request->to_date,
			'task_type_id' => $request->task_type_id,
			'campaign_filter' => $request->campaign_filter,
			'sub_account_id' => $request->sub_account_id,
			'per_page' => $request->per_page
		];

		return view('client.reports.sms.jobs.sms-failed', compact('title', 'begin', 'filters', 'subaccounts', 'filterTaskTypes', 'campaigns', 'results'));
    }
}
