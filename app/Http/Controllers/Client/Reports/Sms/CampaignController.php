<?php

namespace App\Http\Controllers\Client\Reports\Sms;

use ATM\FileGenerator;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Campaign\CampaignQueryRepository;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Reports\ReportsRepository;
use ATM\Repositories\SubAccount\SubAccountRepository;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CampaignController extends Controller
{

    public function __construct
    (
        ReportsRepository $reportsRepo,
        CampaignQueryRepository $campaignQueryRepo,
        CampaignRepository $campaignRepo,
		SubAccountRepository $subAccountRepo
    )
    {
    	$this->subAccountRepo = $subAccountRepo;
        $this->campaignQueryRepo = $campaignQueryRepo;
        $this->campaignRepo = $campaignRepo;
        $this->reportsRepo = $reportsRepo;
    }

    public function smsPerCampaign(Request $request)
    {
		$title = 'SMS Per Campaign';
        $begin = true;

        if (!$request->filled('from_date') && !$request->filled('to_date') && !$request->filled('campaign_filter')) {
        	$campaigns = $this->campaignRepo->getLastCreated($request, ['id','created_at']);
        	$request['last_created_ids'] = Arr::flatten($campaigns->pluck('id')->toArray());
			$request['from_date'] = $campaigns->min('created_at')->toDateString();
			$request['to_date'] = $campaigns->max('created_at')->toDateString();
		}

		$subaccounts = $this->subAccountRepo->all()->pluck('account_name', 'id');
        $campaigns = $this->campaignRepo->forFiltersList($request);

		if (!$request->filled('per_page')) {
			$request['per_page'] = self::PER_PAGE;
		}

        $tasksPerCampaign = $this->reportsRepo->smsPerCampaign($request);

        if( $begin ) {
            if ($request->filled('export_excel')) {

                $client = $request->user()->client;
                $moneyColumn = [];

				if ($request->filled('sub_account_id')) {
					$subAccount = $this->subAccountRepo->find($request->sub_account_id);
					$namePrefix = $client->company_name . ' - ' . $subAccount->account_name;
				} else {
					$namePrefix = $client->company_name;
				}

				$reportTitle = 'SMS Per Campaign';
                FileGenerator::makeExcel(
                    '',
                    $namePrefix . ' - SMS per Campaign - ' . $request['from_date'] . ' - ' . $request['to_date'],
                    'SMS per Campaign',
                    'partials.report-export.jobs.jobs-per-campaign',
                    compact('tasksPerCampaign', 'reportTitle'),
                    false,
                    $moneyColumn
                );
            }
        }

        $filters = [
            'start_filter'    => $request->start_filter,
            'from_date'       => $request->from_date,
            'to_date'         => $request->to_date,
            'campaign_filter' => $request->campaign_filter,
			'sub_account_id'  => $request->sub_account_id,
			'per_page' 		  => $request->per_page
        ];

        return view('client.reports.sms.campaign.per-campaign', compact('title','campaigns','tasksPerCampaign','filters','begin', 'subaccounts'));
    }
}
