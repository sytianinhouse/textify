<?php

namespace App\Http\Controllers\Client\Reports\Credit;

use ATM\FileGenerator;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Credits\CreditQueryRepository;
use ATM\Repositories\Credits\CreditRepository;
use ATM\Repositories\Loads\LoadRepository;
use ATM\Repositories\Reports\ReportsRepository;
use ATM\Repositories\SubAccount\SubAccountRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use ATM\Repositories\Tasks\BaseJobQueryRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CreditsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct
    (
        CreditRepository $creditRepo,
        SubAccountRepository $subRepo,
        CampaignRepository $campaign,
        CreditQueryRepository $creditQueryRepo,
        BaseJobQueryRepository $baseJobQueryRepo,
        ReportsRepository $reportsRepo,
		LoadRepository $loadRepo
    )
    {
        $this->creditRepo      = $creditRepo;
        $this->subRepo         = $subRepo;
        $this->campaignRepo    = $campaign;
        $this->creditQueryRepo = $creditQueryRepo;
        $this->baseJobQueryRepo = $baseJobQueryRepo;
        $this->reportsRepo = $reportsRepo;
		$this->loadRepo = $loadRepo;
    }

    public function creditBalance( Request $request ) {

        $title = 'Credit Balance';

        $subAccounts = $this->subRepo->all();
		$subaccounts = $subAccounts->pluck('account_name', 'id');

        if (!$request->filled('sub_account_id')) {
        	$subAccount = $subAccounts->first();
        	$request['sub_account_id'] = $subAccount->id;
		} else {
        	$subAccount = $subAccounts->first(function ($item) use($request) {
        		return $item->id == $request->sub_account_id;
			});
		}

        $credits = $this->creditRepo->search($request, null);
        $loads = $this->loadRepo->search($request);
        $usage = $this->baseJobQueryRepo->allJobsCompletedCredits($request, true);
        $usage = $usage->map(function($u) {
            $u->total_credits = (float) $u->total_credits; // null paminsan kaya nag `NaN` sa frontend display
            return $u;
        });

        $filters = [
            'start_filter' => $request->start_filter,
            'campaign_filter' => $request->campaign_filter,
            'sub_account_id' => $request->sub_account_id,
        ];

    	return view('client.reports.credits.balance', compact('title', 'credits', 'loads', 'usage', 'subaccounts','filters','subAccount'));
    }
}
