<?php

namespace App\Http\Controllers\Client\User;

use ATM\Repositories\Client\Client;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Roles\Role;
use ATM\Repositories\Roles\RoleRepository;
use ATM\Repositories\User\UserRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
	public function __construct(
        UserRepository $userRepo,
        RoleRepository $roleRepo
    )
    {
        $this->userRepo = $userRepo;
        $this->roleRepo = $roleRepo;
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Users';
        $client = $request->user()->client;
        $users = $this->userRepo->search($request);
        $authUser = Auth::user();
        $users = $users->filter(function ($value, $key) use($authUser) {
            return $value['username'] != 'kenneth' && $value['username'] != 'sytian' && $value['id'] != $authUser->id;
        });

        $filters = [
            'filter_search' => $request->filter_search,
            'per_page' => $request->per_page,
        ];

        return view('client.users.index', compact(/* 'project',  */'client', 'users', 'title', 'filters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $title = 'Add User';
        $accountTypes = User::$clientAccounts;
        $client = $request->user()->client;
        $subAccounts = $client->subAccounts->pluck('account_name', 'id');
        $roles = $this->roleRepo->ofClient($client)->pluck('display_name', 'id');

        return view('client.users.create', compact('client', 'accountTypes', 'roles', 'title', 'subAccounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $client = $request->user()->client;
            $user = $this->userRepo->addUser($client, $request);

            return redirect()->route('client.user.index')
                        ->withSuccess("Successfully added user.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project, Client $client, User $user, Request $request)
    {
        return view('client.clients.users.details', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, Request $request)
    {
        $title = $user->name . ' - Edit User';
        $accountTypes = User::$clientAccounts;
        $client = $request->user()->client;
        $subAccounts = $client->subAccounts->pluck('account_name', 'id');
        $roles = $this->roleRepo->all()->pluck('display_name', 'id');

        return view('client.users.edit', compact(/* 'project', */ 'user', 'client', 'roles', 'accountTypes', 'title', 'subAccounts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Client $client, User $user, Request $request)
    {
        try {
            $user = $this->userRepo->updateUser($client, $user, $request);

            return redirect()->route('client.user.index')
                        ->withSuccess("Successfully updated client user.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Client $client, User $user)
    {
        $name = $user->name;
        $user->delete();

        return redirect()->route('client.user.index')
                    ->withSuccess("Successfully deleted user {$name}");
    }

    protected function validatePassword($password, $pwdConfirmation)
    {
        $validator = Validator::make(
            [
            'password' => $password,
            'password_confirmation' => $pwdConfirmation,
            ],
            ['password' => 'required|min:6|confirmed']
        );

        return $validator->errors();
    }
}
