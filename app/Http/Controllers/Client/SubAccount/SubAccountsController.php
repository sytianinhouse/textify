<?php

namespace App\Http\Controllers\Client\SubAccount;

use ATM\Repositories\ApiKey\ApiKey;
use ATM\Repositories\Cities\City;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Countries\Country;
use ATM\Repositories\Project\Project;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Stevebauman\Location\Location;

class SubAccountsController extends Controller
{
    public $subAccountRepo;

    public function __construct(SubAccountRepository $subAccountRepo)
    {
        $this->subAccountRepo = $subAccountRepo;
    }

    public function index(Request $request)
    {
        $title = 'Sub Accounts';
        $subaccountslists = $this->subAccountRepo->search($request)->where('active',1);

        $filters = [
            'filter_search' => $request->filter_search,
            'per_page' => $request->per_page
        ];

        return view('client.subaccounts.index', compact('title', 'subaccountslists','filters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Client $client,SubAccount $subaccount,Location $location)
    {
        $user                = $request->user();
        $client              = $user->client;
        $title               = 'Add Sub Account';
        $client_company_name = $user->client->company_name;
        $countries           = Country ::pluck('name', 'iso_3166_2');
        $ip                  = \Request::ip();
        $ipDetails           = $location->get($ip);
        $cCode               = $ipDetails->countryCode;
        $apiKeys		     = $user->client->apis->pluck('name', 'id');

        return view('client.subaccounts.create', compact('client','subaccount','title','countries','client_company_name','cCode','apiKeys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SubAccount $subaccount)
    {
        try {
            $user                 = $request->user();
            $request['client_id'] = $user->client->id;
            $subaccount           = $this->subAccountRepo->store($subaccount, $request);
            if ( !$subaccount->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($subaccount->getErrors())
                            ->withError("Please check required fields.");
            }
            return redirect()->route('client.sub.index')
                            ->withSuccess("Sub Account {$subaccount->account_name} successfully added!");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,SubAccount $subaccount,Location $location, Client $client)
    {
        $user      = $request->user();
        $title     = 'Edit Sub Account - ' . $subaccount->account_name;
        $countries = Country::pluck('name', 'iso_3166_2');
        $ip        = \Request::ip();
        $ipDetails = $location->get($ip);
        $cCode     = $ipDetails->countryCode;
        // $apiKeys   = $user->client->apis->pluck('name', 'id');

        $getAllCities = City::get();
        $cities = [];
        $getAllCities->each(function($value,$key) use (&$cities){
            $name = "{$value['name']}, {$value->province->name}";
            $cities[$value['id']] = $name;
        });

        $getCities = City::where('id', '=', $subaccount->city)->first();
        if( $getCities ) {
            $selectedCity = $getCities->id;
        } else {
            $selectedCity = '';
        }

        $currentCountry = $subaccount->where('id','=',$subaccount->id )->pluck('country')->first();
        return view('client.subaccounts.edit', compact('apiKeys','client', 'selectedCity', 'countries', 'cities', 'title', 'subaccount','cCode','currentCountry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubAccount $subaccount, Request $request)
    {
        try {
            $subaccount = $this->subAccountRepo->update($subaccount, $request);
            return redirect()->back()
                        ->withSuccess("Sub Account {$subaccount->account_name} successfully updated!");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(SubAccount $subaccount)
    {
        $name = $subaccount->account_name;
        $subaccount->delete();

        return redirect()->back()
                    ->withSuccess("Sub Account {$name} successfully deleted!");
    }

    /**
     * Get the settings of the current Sub Account.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function settings(SubAccount $subaccount)
    {
        $title = 'Edit Sub Account Settings';

        $subAccount = $subaccount;

        return view('client.subaccounts.settings', compact('title', 'project', 'client', 'subAccount'));
    }

    /**
     * Update the settings of the current Sub Account.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateSettings(SubAccount $subaccount, Request $request)
    {
        try {
            $input = $request->all();

            $subaccount = $this->subAccountRepo->updateSettings($subaccount, $request);

            return redirect()->route('client.sub.index')
                        ->withSuccess("Sub Account {$subaccount->account_name} settings successfully updated!");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

}
