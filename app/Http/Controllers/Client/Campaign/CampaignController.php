<?php

namespace App\Http\Controllers\Client\Campaign;

use ATM\Repositories\ApiKey\ApiKey;
use ATM\Repositories\ApiKey\ApiKeyRepository;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Countries\Country;
use ATM\Repositories\Project\Project;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\Tasks\BaseJob;
use ATM\Repositories\Tasks\JobsCompleted\JobsCompleted;
use ATM\Repositories\Tasks\JobsCompleted\JobsCompletedRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CampaignController extends Controller
{
    public $campaignRepo;

    public function __construct
    (
        CampaignRepository $campaignRepo,
        SubAccountRepository $subAccountRepo,
        JobsCompleted $JobsCompleted,
        JobsCompletedRepository $JobsCompletedRepository,
        BaseJob $baseJob
    )
    {
        $this->baseJob                 = $baseJob;
        $this->campaignRepo            = $campaignRepo;
        $this->subAccountRepo          = $subAccountRepo;
        $this->JobsCompleted           = $JobsCompleted;
        $this->JobsCompletedRepository =  $JobsCompletedRepository;
    }

    public function index( Request $request)
    {
        $title    = 'Campaigns';
        $allCampaigns  = $this->campaignRepo->search($request, 'subAccounts');
        $campaigns  = $this->campaignRepo->search($request)->pluck('name','id');

        $request['active_only'] = true;
        $subAccounts = $this->subAccountRepo->all()->pluck('account_name', 'id');

        $filters = [
            'sub_account_id'    => $request->sub_account_id,
            'per_page'          => $request->per_page,
            'subaccount_filter' => $request->subaccount_filter,
            'campaign_filter'   => $request->campaign_filter,
            'filter_search'     => $request->filter_search,
        ];

        return view('client.campaign.index', compact('title', 'campaigns', 'allCampaigns', 'subAccounts','filters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user        = $request->user();
        $subaccounts = $user->client->subAccounts->where('active',1);
        $title       = 'Add Campaign';

        return view('client.campaign.create', compact('title','subaccounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Campaign $campaign)
    {
        try {
            $user        = $request->user();
            $subaccounts = $user->client->subAccounts->where('active',1);

            if( $subaccounts->count() == 1 ) {
                $request['sub_account_id'] = $subaccounts->pluck('id')->first();
            }

            $campaign = $this->campaignRepo->store($campaign, $request);
            if ( ! $campaign->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($campaign->getErrors())
                            ->withError("Please check required fields.");
            }
            return redirect()->route('client.camp.index')
                            ->withSuccess("Campaign {$campaign->name} successfully added!");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Campaign $campaign)
    {
        $title       = 'Edit Campaign - ' . $campaign->name;
        $user        = $request->user();
        $subaccounts = $user->client->subAccounts;

        return view('client.campaign.edit', compact('subaccounts','campaign','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubAccount $subaccount, Campaign $campaign, Request $request)
    {
        try {
            $campaign = $this->campaignRepo->update($campaign, $request);
            return redirect()->back()
                        ->withSuccess("Campaign {$campaign->name} successfully updated!");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Project $project, Client $client, SubAccount $subaccount, Campaign $campaign)
    {
        $name = $campaign->name;

        $tasks = $campaign->tasks;

        // if ($tasks->count() > 1) {
        if ($tasks->count() > 0) {
            return redirect()->back()
                    ->withErrors("There are tasks associated with Campaign {$campaign->name}!");
        } else {
            $this->campaignRepo->delete($campaign);
        }

        return redirect()->back()
                    ->withSuccess("Campaign {$campaign->name} successfully deleted!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sentSMS(Request $request, SubAccount $subaccount, Campaign $campaign)
    {
        $title = $campaign->name;
        $task = $campaign->task();
        $request['per_page'] = 100;
        $request['campaign_filter'] = $campaign->id;
        $jobs = $this->JobsCompletedRepository->search($request);

        return view('client.campaign.view-sms', compact('campaign','title','jobs'));
    }
}
