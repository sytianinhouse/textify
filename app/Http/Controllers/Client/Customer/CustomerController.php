<?php

namespace App\Http\Controllers\Client\Customer;

use ATM\Repositories\Cities\City;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Countries\Country;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\Customers\Customer;

use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Project\Project;

use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;

use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class CustomerController extends Controller
{
	public $customerRepo;

	public function __construct(CustomersRepository $customerRepo, SubAccountRepository $subAccountRepo)
    {
        $this->customerRepo = $customerRepo;
        $this->subAccountRepo = $subAccountRepo;
    }

    public function index(Request $request, Project $project, Client $client)
    {
        $title               = 'Contacts';
        $client              = $request->user()->client;
        $request['per_page'] = $request->per_page ?: self::PER_PAGE;
        $customer            = $this->customerRepo->search($request, ['subAccounts.client', 'customerGroups', 'cityRel']);
        $subAccounts         = $client->subAccounts->pluck('account_name', 'id');

        $filters = [
        	'filter_search' => $request->filter_search,
            'per_page' => $request->per_page,
            'subaccount_filter' => $request->subaccount_filter
		];

        return view('client.customers.index', compact('title', 'customer', 'filters', 'subAccounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Project $project, Client $client, Customer $customer)
    {
        $client =  $request->user()->client;
        $title = $client->company_name . ' - Add Contact';

        // $cities = City::orderBy('name')->pluck('name', 'id');
        $getCities = City::get();

        $cities = [];
        $getCities->each(function($value,$key) use (&$cities){
            $name = "{$value['name']}, {$value->province->name}";
            $cities[$value['id']] = $name;
        });

        // $selectedCity = 1367;
        $selectedCity = null;

        $countries = Country::orderBy('name')->pluck('name', 'id');
        $selectedCountry = 608;

        $request['active_only'] = true;
        $subaccounts = $this->subAccountRepo->search($request, [], true)->pluck('account_name', 'id');

        //$subaccounts = $client->subAccounts->pluck('account_name', 'id');
        $selectedSubAccnt = collect();

        $allCustomerGroups = CustomerGroup::where('client_id',$client->id)->get();

        return view('client.customers.create', compact('cities','project', 'client', 'customer', 'countries', 'selectedCountry', 'selectedCity','subaccounts', 'selectedSubAccnt', 'allCustomerGroups', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Client $client, Customer $customer, Request $request)
    {
        try {
            $input = $request->all();
            $client = $request->user()->client;
            $number = str_replace(['-', ' ', '(', ')', '+'], '', $request->mobile);
            $startsWith = startsWith($number,'639');
            $request['mobile'] = $number;

            $rules = [
                'first_name'   => 'required',
                'mobile'       => 'required',
                'sub_accounts' => 'required',
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } elseif(!$startsWith) {
                return redirect()->back()
                                ->withInput()
                                ->withErrors('Invalid Mobile number format!');
            } else {
                $customer = $this->customerRepo->add($request, $client);
            }

            if ( !$customer ) {
                return redirect()->back()
                    ->withInput()
                    ->withErrors('Mobile Number already exists!');
            } elseif( !$customer->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($customer->getErrors())
                            ->withError("Please check required fields.");
            }

            return redirect()->route('client.customer.index')
                        ->withSuccess("Successfully added A New Customer.");

        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Project $project, Client $client, Customer $customer, SubAccount $subaccount)
    {
    	$client =  $request->user()->client;
        $title = $client->company_name . ' - Edit Customer';

        // $cities = City::pluck('name', 'id');
        $selectedCity = $customer->city;

        $countries = Country::pluck('name', 'id');
        $selectedCountry = $customer->country;

        $request['active_only'] = true;
        $subaccounts = $this->subAccountRepo->search($request, [], true)->pluck('account_name', 'id');
        //$subaccounts = $client->subAccounts->pluck('account_name', 'id');
        $selectedSubAccnt = $customer->subaccount_id;

        $getCityList = City::get();
        $cities = [];
        $getCityList->each(function($value,$key) use (&$cities){
            $name = "{$value['name']}, {$value->province->name}";
            $cities[$value['id']] = $name;
        });

        $allCustomerGroups = CustomerGroup::where('client_id',$client->id)->get();
        $birthday = $customer->birthdate_form;
        $selectedCustomerGroups = $customer->customerGroups->pluck('id');

        return view('client.customers.edit', compact('project', 'client', 'customer', 'countries', 'selectedCountry', 'subaccounts' , 'selectedSubAccnt' , 'allCustomerGroups', 'selectedCustomerGroups' ,'title', 'cities', 'birthday', 'selectedCity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project, Client $client, Customer $customer)
    {
        try {
            $client =  $request->user()->client;
            $request['client_id'] = $client->id;
            $customer = $this->customerRepo->update($customer, $request);

            if ( !$customer->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($customer->getErrors())
                            ->withError("Please check required fields.");
            }

            return redirect()->route('client.customer.index')
                        ->withSuccess("Successfully Updated Contact.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Project $project, Client $client, Customer $customer)
    {
        try {

            $customerName = $this->customerRepo->delete($customer);

            return redirect()->route('client.customer.index')
                        ->withSuccess("Successfully deleted contact $customerName.");
        } catch (\DomainException $e) {
            return redirect()->back()
            				->withError($e->getMessage());
        }
    }

    public function getcustomergroup(Request $request){

        $subaccount = $request->subaccount;
        $allData = SubAccount::with(['customerGroups'])->find($subaccount);

        $html = '';
        $html .= '<script>
                        $(".chzn-select").chosen({allow_single_deselect: true});
                </script>';
        if ( $allData ):
            $html .= '<div class="input-group input-group-prepen">
                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                <i class="fa fa-users" aria-hidden="true"></i>
                            </span>';
                            $html .= '<select id="customer_group" class="form-control chzn-select" multiple name="customer_group[]">';
                                foreach ( $allData->customerGroups as $args ):
                                    $html .= '<option value="'. $args->id.'">'.$args->name.'</option>';
                                endforeach;
                            $html .= '</select>';
            $html .= '</div>';
        endif;

        return response()->json(['success' => true, 'data' => $html]);
    }

    public function editcustomergroup(Request $request){

        $subaccount = $request->subaccount;
        $allData = SubAccount::with(['customerGroups'])->find($subaccount);

        $html = '';
        $html .= '<script>
                        $(".chzn-select").chosen({allow_single_deselect: true});
                </script>';
        if ( $allData ):
            $html .= '<div class="input-group input-group-prepen">
                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                <i class="fa fa-users" aria-hidden="true"></i>
                            </span>';
                            $html .= '<select id="customer_group" class="form-control chzn-select" multiple name="customer_group[]">';
                                foreach ( $allData->customerGroups as $args ):
                                    $html .= '<option value="'. $args->id.'">'.$args->name.'</option>';
                                endforeach;
                            $html .= '</select>';
            $html .= '</div>';
        endif;

        //return response()->json(['success' => true, 'data' => $allData->customerGroups]);
        return response()->json(['success' => true, 'edit' => true, 'data' => $html]);

    }
}

?>
