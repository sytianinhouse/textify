<?php

namespace App\Http\Controllers\Client\Jobs\JobsFailed;

use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Settings\SiteSettingsRepository;
use ATM\Repositories\Sms\SmsFacade;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\TaskJobs\AuditLog\JobsAuditLogRepository;
use ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;
use ATM\Repositories\Tasks\JobsFailed\JobsFailed;
use ATM\Repositories\Tasks\JobsFailed\JobsFailedRepository;
use ATM\Repositories\Tasks\JobsNextMonth\JobNextMonth;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use ATM\Repositories\Tasks\Task;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class JobsFailedController extends Controller
{
    public function __construct(
        JobsFailed $jobsFailed,
        SmsFacade $smsFacade,
        JobsFailedRepository $jobsFailedRepo,
        SiteSettingsRepository $settingsRepo,
        JobsAuditLogRepository $jobsAuditLogsRepo,
        MessageBag $messageBag
    )
    {
        $this->jobsFailed        = $jobsFailed;
        $this->smsFacade         = $smsFacade;
        $this->jobsFailedRepo    = $jobsFailedRepo;
        $this->settingsRepo      = $settingsRepo;
        $this->jobsAuditLogsRepo = $jobsAuditLogsRepo;
        $this->messageBag        = $messageBag;
    }

    public function edit(Request $request, JobsFailed $jobsFailed)
    {
        $task     = Task::find($jobsFailed->task_id);
        $campaign = Campaign::find($jobsFailed->campaign_id);
        $date     = new Carbon($jobsFailed->time_attempt_send);
        $time     = new Carbon($jobsFailed->time_attempt_send);
        $title    = 'Edit Job - ' . $campaign->name;

        if( $task->isImmediateTask() ) {
            $breadCrumbsLabel = 'Custom API';
            $breadCrumbsUrl = route('client.task.it.index');
        } else if( $task->isOneTimeSendTask() ) {
            $breadCrumbsLabel = 'Template API';
            $breadCrumbsUrl = route('client.task.ots.index');
        } else if( $task->isTextBlastTask() ) {
            $breadCrumbsLabel = 'Text Blast';
            $breadCrumbsUrl = route('client.task.tbs.index');
        }  else if( $task->isBirthdayTask() ) {
			$breadCrumbsLabel = 'Birthday SMS';
			$breadCrumbsUrl = route('client.task.birthday-task.create');
		} else if(  $task->isBookingTask() ) {
			$breadCrumbsLabel = 'Booking Task';
			$breadCrumbsUrl = route('client.task.booking-task.index');
        }

        return view('client.job.failed', compact('title', 'breadCrumbsLabel', 'breadCrumbsUrl', 'jobsFailed','campaign','time','date','task'));
    }

    public function resend(Request $request, JobsFailed $jobsFailed) {

        try {
            $fromAdmin = $request->has('admin');
            unset($request['admin']);

            if( count($request->all()) > 0 ) {
                $this->jobsFailedRepo->update($jobsFailed, $request);
            }

            $task = $jobsFailed->task;

            $number = ( $request->customer_number ) ? $request->customer_number : $jobsFailed->customer_number;
            $request['number'] = preg_replace('/\D+/', '', $number);
            $request['name']   = ($request->customer_name) ? $request->customer_name : $jobsFailed->customer_name ;
            $request['from_job_failed'] = true;
            $request['job_failed_id'] = $jobsFailed->id;
            $request['message'] = $jobsFailed->message;

			$job = $this->jobsFailedRepo->resendFailedJob($request, $jobsFailed);

            if ( !$job['success'] ) {
                return redirect()->back()
						->withErrors($job['message']);
            }

            if ($fromAdmin) {
                return redirect()->back()
						->withSuccess("Job {$jobsFailed->customer_name} successfully sent!");
            }

            if( $task->isImmediateTask() ) {
                return redirect()->route('client.task.it.edit', [$jobsFailed->task_id])
                    ->withSuccess("Job {$jobsFailed->customer_name} successfully sent!");
            } elseif( $task->isOneTimeSendTask() ) {
                return redirect()->route('client.task.ots.edit', [$jobsFailed->task_id])
                    ->withSuccess("Job {$jobsFailed->customer_name} successfully sent!");
            } elseif( $task->isBookingTask() ) {
                return redirect()->route('client.task.booking-task.edit', [ ($jobsFailed->parent_task_id) ? $jobsFailed->parent_task_id : $jobsFailed->task_id ])
                    ->withSuccess("Job {$jobsFailed->customer_name} successfully sent!");
            } else {
                return redirect()->route('client.task.tbs.view', [$jobsFailed->task_id])
                    ->withSuccess("Job {$jobsFailed->customer_name} successfully sent!");
            }


        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

	public function remove( Request $request, JobsFailed $jobsFailed )
	{
		$task = $jobsFailed->task;
        $this->jobsFailedRepo->delete($jobsFailed->id);

		$breadCrumbsUrl = route('client.task.it.index');
		if( $task->isImmediateTask() ) {
			$breadCrumbsUrl = route('client.task.it.edit', $task);
		} else if( $task->isOneTimeSendTask() ) {
			$breadCrumbsUrl = route('client.task.ots.edit', $task);
		} else if( $task->isTextBlastTask() ) {
			$breadCrumbsUrl = route('client.task.tbs.edit', $task);
		}  else if( $task->isBirthdayTask() ) {
			$breadCrumbsUrl = route('client.task.birthday-task.create');
		}
        return redirect()->to($breadCrumbsUrl)
                        ->withSuccess("Job {$jobsFailed->customer_name} successfully deleted!");
    }
}
