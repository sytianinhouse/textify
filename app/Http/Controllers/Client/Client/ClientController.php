<?php

namespace App\Http\Controllers\Client\Client;

use ATM\Repositories\Client\Client;
use ATM\Repositories\Client\ClientRepository;
use ATM\Repositories\Countries\Country;
use ATM\Repositories\Project\Project;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Stevebauman\Location\Location;

class ClientController extends Controller
{
    public function __construct(ClientRepository $clientRepo)
    {
        $this->clientRepo = $clientRepo;
    }

    public function show(Request $request)
    {
        $title = 'Client Profile';
        $client =  $request->user()->client;
        $countries  = Country::pluck('name', 'iso_3166_2');

        return view('client.client.profile', compact('title', 'client', 'countries'));
    }

    public function update(Request $request)
    {
        try {
            $input = $request->all();
            $client = $request->user()->client;
            $project = $client->project;

            $rules = [
                'company_name' => 'required',
                // 'city' => 'required',
                // 'address_1' => 'required',
                'contact_first_name' => 'required',
                'contact_email' => 'email|nullable',
                'cuid' => [
                    'required',
                    'min:4',
                    'max:25',
                    Rule::unique('clients')->ignore($client->id)->where(function ($query) use($project) {
                        return $query->where('project_id', $project->id);
                    })
                ],
            ];

            $messages = [
                'cuid.required' => 'Unique Client ID is required.',
                'cuid.unique' => 'Unique Client ID has already been taken!',
            ];

            $validator = Validator::make($input, $rules, $messages);

            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } else {
                $client = $this->fillData($client, $request);
            }

            if ( ! $client->save()) {
                return redirect()->back()
                            ->withErrors($client->getErrors())
                            ->withInput();
            }

            if ($request->filled('thumb')) {
                $this->uploadMedia($request, $client);
            }

            //$this->roleRepo->updateDefaults($client);

            return redirect()->back()
                        ->withSuccess("Successfully updated client.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    public function fillData(Client $client, Request $request)
    {
        $client->fill($request->input());
        $client->active = $request->active ? true : false;

        return $client;
    }

}
