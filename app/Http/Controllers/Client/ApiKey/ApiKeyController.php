<?php

namespace App\Http\Controllers\Client\ApiKey;

use ATM\Repositories\ApiKey\ApiKey;
use ATM\Repositories\ApiKey\ApiKeyRepository;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Client\ClientRepository;
use ATM\Repositories\Countries\Country;
use ATM\Repositories\Project\Project;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ApiKeyController extends Controller
{
    public $apiKeyRepo;

    public function __construct(ApiKeyRepository $apiKeyRepo, SubAccountRepository $subAccountRepo, ClientRepository $clientRepo)
    {
        $this->apiKeyRepo     = $apiKeyRepo;
        $this->subAccountRepo = $subAccountRepo;
        $this->clientRepo     = $clientRepo;
    }

    public function index(Request $request)
    {
        $title = 'Api Keys';
        $request['active_only'] = true;

        $subaccounts = $this->subAccountRepo->search($request, [], true)->pluck('account_name', 'id');
        $apis  = $this->apiKeyRepo->search($request);

        $filters = [
            'sub_account_id' => $request->sub_account_id,
            'per_page' => $request->per_page,
            'filter_search' => $request->filter_search
        ];

        return view('client.apikey.index', compact('title', 'apis','subaccounts','filters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user        = $request->user();
        $subAccount  = $user->client->subAccounts->where('active',1);
        $title       = 'Add Api Key';
        $randomSting = Str::random(5);
        $key         = $randomSting.'-'.$user->client->company_name.'-'.$user->client->cuid;
        $cuid        = $user->client->cuid;
        $apiKey      = sha1($key);

        return view('client.apikey.create', compact('apikey','cuid','apiKey','client','subAccount','title','project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Client $client, ApiKey $apikey)
    {
        try {
            $user                 = $request->user();
            $request['client_id'] = $user->client->id;
            $subaacount           = $user->client->subAccounts->where('active',1);

            if( $subaacount->count() == 1 ) {
                $request['sub_account_id'] = $subaacount->first()->id;
            }

            $apikey = $this->apiKeyRepo->store($apikey, $request);

            if ( ! $apikey->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($apikey->getErrors())
                            ->withError("Please check required fields.");
            }

            return redirect()->route('client.api.index')
                            ->withSuccess("Api Key {$apikey->name} successfully added!");

        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ApiKey $apikey, Request $request)
    {
        $subAccount        = $this->subAccountRepo->search($request);
        $currentSubAccount = $apikey->subaccounts->pluck('id');
        $title             = 'Edit Api Key - ' . $apikey->name;
        return view('client.apikey.edit', compact('title','subAccount','currentSubAccount','apikey'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ApiKey $apikey, Request $request)
    {
        try {
            $apikey = $this->apiKeyRepo->update($apikey, $request);
            return redirect()->back()
                        ->withSuccess("Api Key {$apikey->name} successfully updated!");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(ApiKey $apikey)
    {
        $name = $apikey->name;
        $this->apiKeyRepo->delete($apikey);

        return redirect()->back()
                    ->withSuccess("Api Key {$apikey->name} successfully deleted!");
    }
}
