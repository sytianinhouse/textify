<?php

namespace App\Http\Controllers\Admin\Customer;

use ATM\Repositories\Cities\City;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Countries\Country;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Province\Province;

use ATM\Repositories\SubAccount\SubAccount;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;

class ClientCustomersController extends Controller
{
    public $customer;

    	public function __construct(CustomersRepository $customer)
    {
        $this->customerRepo = $customer;
    }

    public function index(Request $request,Project $project, Client $client, Customer $customer, SubAccount $subaccount)
    {
        $title = $client->company_name . ' - Contact';

        /*$query = Customer::with(['subAccounts.client', 'customerGroups', 'cityRel'])->where('client_id', $client->id);
        $customers = collect();*/

        /*if ($request->filled('per_page') && $request->per_page != 'all') {
        	$customers = $query->paginate($request->per_page ?: self::PER_PAGE);
		} else {
        	$customers = $query->get();
		}*/

        $query = Customer::with(['subAccounts.client', 'customerGroups', 'cityRel'])->where('client_id', $client->id);
        $subAccounts = $client->subAccounts->pluck('account_name', 'id');

		if ($request->filled('filter_search')) {
            $strings = explode(' ', $request->filter_search);
		 	foreach ($strings as $string) {
                 $query = $query->where(function($q) use ($string) {
                     $q->where('first_name', 'LIKE', "%{$string}%")
                     ->orWhere('last_name', 'LIKE', "%{$string}%")
                     ->orWhereRaw('CONCAT(first_name, " ", last_name) LIKE ? ', '%' . $string . '%');
		 		});
		 	}
        }

        if ($request->filled('subaccount_filter')) {
            $query = $query->where('subaccount_id', $request->subaccount_filter);
        }

        // if ($request->filled('replicate')) {
        //     $customers = $query->skip(0)->take(1000)->get();
        //     foreach ($customers as $c) {
        //         $cust = $c->replicate();
        //         $cust->subaccount_id = 52;
        //         $cust->save();
        //     }
        //     return 'good';
        // }

        $perPage = $request->input('per_page', self::PER_PAGE);
    	if ($perPage == 'all') {
            $customers = $query->get();
    	} else {
            $customers = $query->paginate($perPage);
        }

        // $customers = $this->customerRepo->search($request);

        $filters = [
            'filter_search' => $request->filter_search,
            'per_page' => $request->per_page,
            'subaccount_filter' => $request->subaccount_filter
        ];

    	return view('admin.customers.index', compact('customers', 'client', 'project', 'title','filters', 'subAccounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project, Client $client, Customer $customer, CustomerGroup $customergroup, SubAccount $subaccount)
    {
        $title = $client->company_name . ' - Add Contact';

        $countries = Country::pluck('name', 'name');
        $selectedCountry = 608;

        $getCities = City::get();

        $cities = [];
        $getCities->each(function($value,$key) use (&$cities){
            $name = "{$value['name']}, {$value->province->name}";
            $cities[$value['id']] = $name;
        });

        // $selectedCity = 1367;
        $selectedCity = null;

        $subaccounts = $client->subAccounts->pluck('account_name', 'id');
        $selectedSubAccnt = collect();

        $allCustomerGroups = $client->customerGroups()->get();
        /*$allCustomerGroups = CustomerGroup::with(['SubAccounts.client'])->whereHas('SubAccounts', function($q) use ($client) {
            return $q->where('subaccount_id', $client->id);
        })->get();*/

        return view('admin.customers.create', compact('project', 'client', 'customer', 'countries', 'selectedCountry', 'cities', 'selectedCity', 'subaccounts', 'selectedSubAccnt', 'allCustomerGroups', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Client $client, Customer $customer, Request $request)
    {
        try {
            if ($request->filled('city')) {
                $getCities = City::find($request->city);
                $request['city'] = $getCities->name;
            }

            $input = $request->all();
            $rules = [
                'first_name'    => 'required',
                'mobile'        => 'required',
                'sub_accounts'   => 'required',
            ];
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withInput()
                ->withErrors($validator);
            } else {
                $customer = $this->customerRepo->add($request, $client);
            }
            if ( !$customer->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($customer->getErrors())
                            ->withError("Please check required fields.");
            }
            return redirect()->route('admin.project.client.customer.index', [$project, $client] )
                        ->withSuccess("Successfully added A New Contact.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

	/**
	 * Import list of customers
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function import(Request $request)
	{
		try {
			$this->customerRepo->import($request);

			return redirect()->back()->withSuccess("Successfully imported, please wait for 5 second to see the full updated list of customer.");
		} catch (ValidationException $e) {
			return $this->redirectFormRequest($e);
		}
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client, Customer $customer)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, Client $client, Customer $customer, CustomerGroup $customergroup, SubAccount $subaccount)
    {
        $title = $client->company_name . ' - Edit Contact';

		$countries = Country::pluck('name', 'name');
        $selectedCountry = $customer->country; // get selected

        $subaccounts = $client->subAccounts->pluck('account_name', 'id'); // get all subaccount
        $selectedSubAccnt = $customer->subaccount_id; // get selected

        $allCustomerGroups = CustomerGroup::where('client_id', '=', $client->id)->get(); // get all groups
        $selectedCustomerGroups = $customer->customerGroups->pluck('id'); // get selected id

        // $cities = City::pluck('name', 'id'); // get all cities
        $getAllCities = City::get();

        $cities = [];
        $getAllCities->each(function($value,$key) use (&$cities){
            $name = "{$value['name']}, {$value->province->name}";
            $cities[$value['id']] = $name;
        });

        $getCities = City::where('name', '=', $customer->city)->first();
        $selectedCity = ($getCities) ? $getCities->id :'';

        $bdCustomer = Customer::where('id', '=', $customer->id)->first();
        $birthday = strftime('%F', strtotime($bdCustomer->b_year.'-'.$bdCustomer->b_month.'-'.$bdCustomer->b_day)); // get customer birthdate.

        return view('admin.customers.edit', compact('project', 'client', 'customer', 'countries', 'selectedCountry', 'cities', 'selectedCity', 'subaccounts' , 'selectedSubAccnt' , 'allCustomerGroups', 'selectedCustomerGroups', 'birthday', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project, Client $client, Customer $customer)
    {
        try {
            $request['cient_id'] = $client->id;
            if ($request->filled('city')) {
                $getCities = City::find($request->city);
                $request['city'] = $getCities->name;
            }

            $input = $request->all();
            $rules = [
                'first_name'    => 'required',
                'mobile'        => 'required',
                'sub_accounts'   => 'required',
            ];
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } else {
                $customer = $this->customerRepo->update($customer, $request);
            }

            if ( !$customer->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($customer->getErrors())
                            ->withError("Please check required fields.");
            }
            return redirect()->route('admin.project.client.customer.index', [$project, $client] )
                        ->withSuccess("Successfully Updated Contact.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Client $client, Customer $customer)
    {
        try {
            $customerName = $this->customerRepo->delete($customer);
            return redirect()->route('admin.project.client.customer.index', [$project, $client] )
                        ->withSuccess("Successfully deleted contact $customerName.");
        } catch (\DomainException $e) {
            return redirect()->back()
            				->withError($e->getMessage());
        }
    }

    public function getcustomergroup(Request $request, $subaccount){

        $subaccount = $request->subaccount;
        $allData = Client::with(['customerGroups'])->find($subaccount);
        $html = '';
        $html .= '<script>
                        $(".chzn-select").chosen({allow_single_deselect: true});
                </script>';
        if ( $allData ):
            $html .= '<select id="customer_group" class="form-control chzn-select" multiple name="customer_group[]">';
                foreach ( $allData->customerGroups as $args ):
                    $html .= '<option value="'. $args->id .'">'.$args->name.'</option>';
                endforeach;
            $html .= '</select>';
        endif;
        return response()->json(['success' => true, 'data' => $html]);
    }

    public function editcustomergroup(Request $request, $subaccount){

        $subaccount = $request->subaccount;
        $allData = SubAccount::with(['customerGroups'])->find($subaccount);
        $html = '';
        $html .= '<script>
                        $(".chzn-select").chosen({allow_single_deselect: true});
                </script>';
        if ( $allData ):
            $html .= '<select id="customer_group" class="form-control chzn-select" multiple name="customer_group[]">';
                foreach ( $allData->customerGroups as $args ):
                    $html .= '<option value="'. $args->id.'">'.$args->name.'</option>';
                endforeach;
            $html .= '</select>';
        endif;
        return response()->json(['success' => true, 'edit' => true, 'data' => $html]);

    }
}
