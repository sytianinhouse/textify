<?php

namespace App\Http\Controllers\Admin\Role;

use ATM\Repositories\Client\Client;
use ATM\Repositories\Permission\Permission;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Roles\Role;
use ATM\Repositories\Roles\RoleRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class ClientRolesController extends Controller
{
    public $role;

    	public function __construct(RoleRepository $role)
    {
        $this->roleRepo = $role;
    }

    public function index(Project $project, Client $client, Role $role)
    {
        $title = $client->company_name . ' - Roles';
        $roles = $client->roles;

    	return view('admin.roles.index', compact('roles', 'client', 'project', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project, Client $client, Role $role)
    {
        $title = $client->company_name . ' - Add Role';

        $permissions = Permission::orderBy('position')->get()->groupBy('group');

        return view('admin.roles.create', compact('project', 'client', 'role', 'title','permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Client $client, Role $role, Request $request)
    {
        try {

            $input = $request->all();

            $rules = [
                'name' => 'required'
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } else {
                $role = $this->roleRepo->add($request, $client);
            }

            if ( ! $role->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($role->getErrors())
                            ->withError("Please check required fields.");
            }

            return redirect()->route('admin.project.client.role.index', [$project, $client] )
                        ->withSuccess("Successfully added Role.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client, Role $role)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, Client $client, Role $role)
    {
        $title       = $client->company_name . ' - Edit User Role';
        $permissions = Permission::orderBy('position')->get()->groupBy('group');

        return view('admin.roles.edit', compact('project', 'client', 'role', 'roles','title','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project, Client $client, Role $role)
    {
        try {

            $input = $request->all();

            $rules = [
                'name' => 'required'
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } else {
                $role = $this->roleRepo->update($role, $request);
            }

            return redirect()->route('admin.project.client.role.index', [$project, $client] )
                        ->withSuccess("Successfully Updated Role.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Client $client, Role $role)
    {
        try {
            $roleName = $this->roleRepo->delete($role);

            return redirect()->route('admin.project.client.role.index', [$project, $client] )
                        ->withSuccess("Successfully deleted Role $roleName.");
        } catch (\DomainException $e) {
            return redirect()->back()
            				->withError($e->getMessage());
        }
    }

}
