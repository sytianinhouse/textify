<?php

namespace App\Http\Controllers\Admin\Profile;

use ATM\ContextInterface;
use ATM\Repositories\User\UserRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function __construct(
        UserRepository $userRepo,
        ContextInterface $context
    )
    {
        $this->userRepo = $userRepo;
        $this->context = $context;
    }

    public function edit( Request $request, User $user ) {
        $user = Auth::user();
        return view('admin.profile.index', compact('user'));
    }

    public function update( Request $request, User $user ) {
        dd($request->all());
        try {
            $request['admin'] = true;
            $request['active'] = '1';
            $request['account_type'] = 'superadmin';
            $user = $this->userRepo->updateUserAdmin($user, $request);

            return redirect()->route('admin.profile.edit',$user->id)
                        ->withSuccess("Successfully updated profile.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }
}
