<?php

namespace App\Http\Controllers\Admin\SubAccount;

use ATM\Repositories\Client\Client;
use ATM\Repositories\Project\Project;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\Tasks\TaskType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MonitoringController extends Controller
{
	public function __construct()
	{

	}

	public function monitoring(Request $request)
	{
		$title = 'Account Monitoring';

		$projects = Project::orderBy('name')->with('clients')->get();
		$clients = Client::orderBy('company_name')->pluck('company_name', 'id');
		$taskTypes = TaskType::get();

		$records = collect();
		if ($request->filled('project_id')) {
			$query = Client::with([
				'subAccounts' => function($q) {
					$q->with([
						'task' => function($query) {
							$query->select('id','task_type_id','active','subaccount_id');
						},
						'credit',
						'customers' => function ($query) {
							$query->select('id', 'subaccount_id');
						}
					]);
				}
			])
				->orderBy('company_name');

			if ($request->filled('client_ids')) {
				$query = $query->whereIn('id', $request->client_ids[$request->project_id]);
				$records = $query->get();
			}
		}

		$filters = [
			'project_id' => $request->project_id,
			'client_ids' => $request->client_ids[$request->project_id]
		];

		return view('admin.monitoring.index', compact('title','clients','projects','filters','records','taskTypes'));
    }
}
