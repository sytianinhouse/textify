<?php

namespace App\Http\Controllers\Admin\SubAccount;

use ATM\Repositories\ApiKey\ApiKey;
use ATM\Repositories\ApiKey\ApiKeyRepository;
use ATM\Repositories\Cities\City;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Countries\Country;
use ATM\Repositories\Project\Project;
use ATM\Repositories\SenderIds\SenderIds;
use ATM\Repositories\SenderIds\SenderIdsRepository;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Stevebauman\Location\Location;

class SubAccountsController extends Controller
{
    public $subaccount;

    public function __construct(
    	SubAccountRepository $subaccountRepo,
        ApiKeyRepository $apiKeyRepo,
        SenderIdsRepository $senderIds
    )
    {
        $this->subaccountRepo = $subaccountRepo;
        $this->senderIdsRepo  = $senderIds;
        $this->apiKeyRepo     = $apiKeyRepo;
    }

    public function index(Request $request, Project $project, Client $client, SubAccount $subaccount)
    {
        $title = $client->company_name . '`s - Sub Accounts';
        $subaccountslists = $client->SubAccounts;

        $query = [
            'per_page' => $request->per_page
        ];

        return view('admin.subaccounts.index', compact('title', 'subaccountslists', 'project', 'client','query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project, Client $client, SubAccount $subaccount, Location $location, Request $request)
    {
        $title               = $client->company_name . ' - Add Sub Account';
        $client_company_name = $client->company_name;
        $countries           = Country::pluck('name', 'iso_3166_2');
        $ip                  = \Request::ip();
        $ipDetails           = $location->get($ip);
        $cCode               = $ipDetails->countryCode;
        $apiKeys			 = ApiKey::getGateways()->pluck('name', 'id');
        $selectedCity        = $client->city;
        $request['client_id'] = $client->id;

        $getAllCities = City::get();
        $cities = [];
        $getAllCities->each(function($value,$key) use (&$cities){
            $name = "{$value['name']}, {$value->province->name}";
            $cities[$value['id']] = $name;
        });

        $allSenderids = $this->senderIdsRepo->search($request)->pluck('name','id');

        return view('admin.subaccounts.create', compact('allSenderids','client','client_company_name','subaccount', 'selectedCity', 'cities', 'countries', 'title','project','cCode','apiKeys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Client $client, Request $request, SubAccount $subaccount)
    {
        try {
            $request['client_id'] = $client->id;
            $input = $request->all();

			$subaccount = $this->subaccountRepo->store($subaccount, $request);

            if ( ! $subaccount->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($subaccount->getErrors())
                            ->withError("Please check required fields.");
            }
            return redirect()->route('admin.project.client.sub.index',[$project, $client])
                            ->withSuccess("Sub Account {$subaccount->account_name} successfully added!");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, Client $client, SubAccount $subaccount, Location $location,Request $request)
    {
        $title          = 'Edit Sub Account - ' . $subaccount->account_name;
        $countries      = Country ::pluck('name', 'iso_3166_2');
        $ip             = \Request::ip();
        $ipDetails      = $location->get($ip);
        $cCode          = $ipDetails->countryCode;
        $currentCountry = $subaccount->where('id','=',$subaccount->id )->pluck('country')->first();
        $apiKeys		= ApiKey::getGateways()->pluck('name', 'id');

        $getAllCities = City::get();
        $cities = [];
        $getAllCities->each(function($value,$key) use (&$cities){
            $name = "{$value['name']}, {$value->province->name}";
            $cities[$value['id']] = $name;
        });

        $request['client_id'] = $client->id;
        $allSenderids = $this->senderIdsRepo->search($request)->pluck('name','id');

        return view('admin.subaccounts.edit', compact('allSenderids','project', 'client', 'countries', 'title', /* 'selectedCity', */ 'cities', 'subaccount', 'cCode', 'currentCountry', 'apiKeys'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Client $client, SubAccount $subaccount, Request $request)
    {
        try {
            $input = $request->all();

			$subaccount = $this->subaccountRepo->update($subaccount, $request);

            return redirect()->route('admin.project.client.sub.index',[$project, $client])
                        ->withSuccess("Sub Account {$subaccount->account_name} successfully updated!");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Project $project, Client $client, SubAccount $subaccount)
    {
        $name = $subaccount->account_name;
        $subaccount->delete();

        return redirect()->back()
                    ->withSuccess("Sub Account {$name} successfully deleted!");
    }

    /**
     * Get the settings of the current Sub Account.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function settings(Project $project, Client $client, SubAccount $subaccount)
    {
        $title = 'Edit Sub Account Settings';

        $subAccount = $subaccount;

        return view('admin.subaccounts.settings', compact('title', 'project', 'client', 'subAccount'));
    }

    /**
     * Update the settings of the current Sub Account.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateSettings(Project $project, Client $client, SubAccount $subaccount, Request $request)
    {
        try {
            $input = $request->all();

            $subaccount = $this->subaccountRepo->updateSettings($subaccount, $request);

            return redirect()->route('admin.project.client.sub.index',[$project, $client])
                        ->withSuccess("Sub Account {$subaccount->account_name} settings successfully updated!");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

}
