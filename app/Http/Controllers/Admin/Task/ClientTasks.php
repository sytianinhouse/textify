<?php

namespace App\Http\Controllers\Admin\Task;

use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Tasks\Task;
use Carbon\Carbon;

use ATM\Repositories\CustomerGroups\CustomerGroupsRepository;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Tasks\TaskRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClientTasks extends Controller {

	public function __construct (
		CampaignRepository $campaignRepo,
		SubAccountRepository $subAccountRepo,
		CustomersRepository $customerRepo,
		CustomerGroupsRepository $customerGroupRepo,
		TaskRepository $taskRepo
	)
    {
    	$this->customerGroupRepo = $customerGroupRepo;
        $this->subAccountRepo = $subAccountRepo;
        $this->campaignRepo   = $campaignRepo;
        $this->customerRepo = $customerRepo; 
        $this->taskRepo = $taskRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project, Client $client, Task $task)
    {
    	$title = $client->company_name . ' - Task';
        $tasks = Task::with('campaigns')->get();
        if ( $tasks ) {
        	return view('admin.tasks.index', compact( 'tasks', 'client', 'project', 'title'));
        }
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project, Client $client, SubAccount $subaccount, Request $request)
    {
        $title = $client->company_name . ' - Add Task';

        $request['sub_account_id'] = $client->id;
		$request['not_empty_mobile'] = true;
        $campaign    = $this->campaignRepo->search($request, [])->pluck('name', 'id'); // campaigns
        $selectedCampaign = collect();

        $customergroup   = $this->customerGroupRepo->search($request, [])->pluck('name', 'id'); // groups
        $selectedCustomerGroup = collect();

        $customer   = $this->customerRepo->search($request)->pluck('first_name', 'id'); // customers
        $selectedCustomer = collect();
       
        return view('admin.tasks.create', compact('task', 'project', 'client', 'campaign', 'selectedCampaign', 'customergroup', 'selectedCustomerGroup', 'customer', 'selectedCustomer', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Client $client, Request $request)
    {
        try {
            $input = $request->all();
            $rules = [
                'campaign'    => 'required',
                'date_to_send'        => 'required|after_or_equal:'. date('Y-m-d'),
                'time_to_send'        => 'required',
                'message'   => 'required'
            ];
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } else {
                $request['sub_account_id'] = isset($client->id)? $client->id : 0; // get sub_account_id
                $task = $this->taskRepo->add($request, $client);
            }
            if ( !$task->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($task->getErrors())
                            ->withError("Please check required fields.");
            }
            return redirect()->route('admin.project.client.task.index', [$project, $client] )
                        ->withSuccess("Successfully added A New Customer.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Project $project, Client $client, $taskId)
    {
        $title = $client->company_name . ' - Edit User Role';

        $task = Task::find($taskId);

        $request['sub_account_id'] = $client->id;
        $campaign = $this->campaignRepo->search($request, [], true)->pluck('name', 'id'); // campaigns
        $selectedCampaign = $task->campaign_id;

        $customergroup   = $this->customerGroupRepo->search($request, [], true)->pluck('name', 'id'); // groups
        $selectedCustomerGroup = $task->taskCustomerGroups->pluck('id');

		$request['not_empty_mobile'] = true;
        $customer = $this->customerRepo->search($request)->pluck('first_name', 'id'); // customers
        $selectedCustomer = $task->taskCustomers->pluck('id');

        return view('admin.tasks.edit', compact('task', 'project', 'client', 'campaign', 'selectedCampaign', 'customergroup', 'selectedCustomerGroup', 'customer', 'selectedCustomer', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Client $client, Request $request, $taskId)
    {
        try {

            $input = $request->all();
            $rules = [
                'campaign'    => 'required',
                'date_to_send'        => 'required|after_or_equal:'. date('Y-m-d'),
                'time_to_send'        => 'required',
                'message'   => 'required'
            ];
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } else {
            	$request['sub_account_id'] = isset($client->id)? $client->id : 0; // get sub_account_id
                $task = $this->taskRepo->update($taskId, $request, $client);
            }

            if ( !$task->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($task->getErrors())
                            ->withError("Please check required fields.");
            }
            return redirect()->route('admin.project.client.task.index', [$project, $client] )
                        ->withSuccess("Successfully Updated Customer Groups.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Client $client, Request $request, $taskId)
    {
        try {
            $taskName = $this->taskRepo->delete($request, $taskId);
            return redirect()->route('admin.project.client.task.index', [$project, $client] )
                        ->withSuccess("Task successfully deleted.");
        } catch (\DomainException $e) {
            return redirect()->back()
            				->withError($e->getMessage());
        }
    }
}