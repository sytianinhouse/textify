<?php

namespace App\Http\Controllers\Admin\Task\TextBlastSend;

use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Cities\City;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Credits\Credit;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\CustomerGroups\CustomerGroupsRepository;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\Customers\CustomersQueryRepository;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Date\Date;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Settings\SiteSettings;
use ATM\Repositories\Shortcodes\Shortcodes;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\TaskJobs\TaskJobsRepository;
use ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;
use ATM\Repositories\Tasks\JobsNextMonth\JobNextMonth;

use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Tasks\TaskRepository;
use ATM\Repositories\Tasks\TextBlastRepository;

use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TextBlastSendController extends Controller
{
    public function __construct (
		CustomerGroupsRepository $customerGroupRepo,
        SubAccountRepository $subAccountRepo,
        CustomersRepository $customerRepo,
        CampaignRepository $campaignRepo,
        TaskJobsRepository $JobTaskRepo,
		TaskRepository $taskRepo,
        TextBlastRepository $textBlastRepo,

        JobToday $JobsToday,
        JobTomorrow $JobsTomorrow,
        JobAfterTomorrowTo14thDay $JobsAfterTomorrowTo14thDay,
        Job15thDayToEndMonth $Jobs15thDayToEndMonth,
        JobNextMonth $JobsNextMonth,

		CustomersQueryRepository $customersQueryRepo
    )
    {
    	$this->customerGroupRepo = $customerGroupRepo;
        $this->subAccountRepo = $subAccountRepo;
        $this->campaignRepo   = $campaignRepo;
        $this->JobTaskRepo = $JobTaskRepo;
        $this->taskRepo = $taskRepo;
        $this->textBlastRepo = $textBlastRepo;

        $this->JobsNextMonth = $JobsNextMonth;
        $this->Jobs15thDayToEndMonth = $Jobs15thDayToEndMonth;
        $this->JobsAfterTomorrowTo14thDay = $JobsAfterTomorrowTo14thDay;
        $this->JobsTomorrow = $JobsTomorrow;
        $this->JobsToday = $JobsToday;

        $this->customersQueryRepo = $customersQueryRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project, Client $client, SubAccount $subAccount, Request $request)
    {
        $title = $client->company_name . ' -  Text Blast SMS';

   //      $tasks = Task::whereHas('campaigns', function ($query) use ( $subAccount ) {
			// 	$query->where('task_type_id', '=', Task::TEXT_BLAST_SEND)
			// 	->where('subaccount_id', '=', $subAccount->id);
			// })
        $tasks = Task::has('campaign')
			->with('tasksjobsCompleted', 'tasksjobsFailed')
			->withCount('tasksJobToday', 'tasksJobTomorrow',
						'tasksJobAfterTomorrowTo14thDay', 'tasksJob15thDayToEndMonth',
						'tasksJobNextMonth', 'tasksjobsCompleted', 'tasksjobsFailed')->orderBy('date_to_send', 'DESC')->get();

        if ( $tasks ) {
        	return view('admin.tasks.text-blast-send.index', compact( 'client', 'project', 'subAccount', 'tasks', 'title'));
        }
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project, Client $client, SubAccount $subAccount, Request $request, Task $task)
    {
        $title = $client->company_name . ' - Add Task';
        $siteSettings = SiteSettings::getSettings();
        // dd($siteSettings);
        $request['subaccount_id'] = $subAccount->id;

        $clientID = $client->id;
        $credit = $subAccount->credit;

        $campaign = $subAccount->campaigns->pluck('name', 'id'); // get all campaign
        $selectedCampaign = [];

        $customergroup = $client->customerGroups->pluck('name', 'id'); // for all customers
        $selectedCustomerGroup = collect();

		// selected customers
		$request['client_id'] = $clientID;
		$request['not_empty_mobile'] = true;
		$customers = $this->customersQueryRepo->getRecipientsList($request, true,true);

        $selectedCustomer = [];

        $options = array('all_customer' => 'All Contacts', /*'specific_customer' => 'Specific Customers',*/ 'custom_filter' => 'Custom Filter');
        $selectedOption = [];

		// get total customer
        $allCounts = Task::withCount('taskCustomers', 'tasksJobToday',
                                    'tasksJobTomorrow', 'tasksJobAfterTomorrowTo14thDay',
                                    'tasksJob15thDayToEndMonth', 'tasksJobNextMonth', 'tasksjobsCompleted', 'tasksjobsFailed')->find( $task->id );
        $getTotal = $allCounts ? $allCounts->task_customers_count : 0;

        $messagePlaceholder = "Hi [first_name], [last_name] \n\nThis is our promo.";

        $shortCodes = Shortcodes::$shortCodes;
        $months = Date::$months;

        $cities = City::pluck('name', 'name'); // get all cities
        $selectedCities = [];

        $sub = $subAccount->sender_id;
        $api = $subAccount->apis;
        $apiID = $api->pluck('id');

        if( $sub == null && $apiID->first() == 2 ) {
            $no_sender_id = true;
        } else {
            $no_sender_id = false;
        }

        return view(
        	'admin.tasks.text-blast-send.create',
			compact('project', 'client', 'subAccount', 'campaign', 'selectedCampaign', 'customergroup', 'selectedCustomerGroup', 'customers', 'selectedCustomer', 'cities', 'selectedCities', 'clientID', 'options', 'selectedOption', 'getTotal', 'allCounts', 'messagePlaceholder', 'credit', 'siteSettings', 'title', 'shortCodes', 'months', 'no_sender_id')
		);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Client $client, SubAccount $subAccount, Task $task, Request $request)
    {
        // dd($request->all());
        try {
            $task = $this->textBlastRepo->addTask($request, null, $subAccount, $client);

            if ( isset($request->input_submit_task) == true ) {
                return redirect()
                    ->route('admin.project.client.sub.task.tbs.index', [$project, $client, $subAccount] )
                    ->withSuccess("Successfully added A New Task.");
            } else {
                return redirect()
                    ->route('admin.project.client.sub.task.tbs.view', [$project, $client, $subAccount, $task] )
                    ->withSuccess("Text blast successfully created.");
            }

        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Project $project, Client $client, SubAccount $subAccount, Task $task)
    {
        $title = $client->company_name . ' - Edit Task';

        $clientID = $client->id;
		$credit = $subAccount->credit;

        $campaign = $subAccount->campaigns->pluck('name', 'id'); // get all campain
        $selectedCampaign = $task->campaign_id;

        $customergroup   = $this->customerGroupRepo->search($request, [], true)->pluck('name', 'id'); // groups
        $selectedCustomerGroup = collect();

		// selected customers
		$request['client_id'] = $clientID;
		$request['not_empty_mobile'] = true;
		$customers = $this->customersQueryRepo->search($request, true)->pluck('first_name', 'id');
        $selectedCustomer = $task->taskCustomers->pluck('id');

        $options = array('all_customer' => 'All Customers', 'specific_customer' => 'Specific Customers', 'custom_filter' => 'Custom Filter');
        $selectedOption = collect();

        $allCounts = Task::withCount('taskCustomers', 'tasksJobToday',
                                    'tasksJobTomorrow', 'tasksJobAfterTomorrowTo14thDay',
                                    'tasksJob15thDayToEndMonth', 'tasksJobNextMonth')->find( $task->id ); // get total customer
        $getTotal = isset($allCounts) ? $allCounts->task_customers_count : 0;

        $messagePlaceholder = "Hi [first_name], [last_name] \n\nThis is our promo.";

		$shortCodes = Shortcodes::$shortCodes;
		$months = Date::$months;

        $cities = City::pluck('name', 'name'); // get all cities
        $selectedCities = collect();

        $sub = $subAccount->sender_id;
        $api = $subAccount->apis;
        $apiID = $api->pluck('id');

        if( $sub == null && $apiID->first() == 2 ) {
            $no_sender_id = true;
        } else {
            $no_sender_id = false;
        }

        return view(
        	'admin.tasks.text-blast-send.edit',
			compact('task', 'project', 'client', 'subAccount', 'campaign', 'selectedCampaign', 'customergroup', 'selectedCustomerGroup', 'customers', 'selectedCustomer', 'clientID', 'options', 'selectedOption', 'cities', 'selectedCities', 'getTotal', 'allCounts', 'messagePlaceholder', 'credit', 'title', 'shortCodes', 'months','no_sender_id')
		);
    }

    public function view(Request $request, Project $project, Client $client, SubAccount $subAccount, Task $task){

        $title = $client->company_name . ' - View Task';
        $selectedCampaign = Campaign::find($task->campaign_id);

        $allCounts = Task::with('tasksjobsCompleted', 'tasksjobsFailed')
                            ->withCount('taskCustomers', 'tasksJobToday',
                            'tasksJobTomorrow', 'tasksJobAfterTomorrowTo14thDay',
                            'tasksJob15thDayToEndMonth', 'tasksJobNextMonth')->orderBy('id', 'ASC')->find( $task->id ); // get total customer
        $getTotal = isset($allCounts) ? $allCounts->task_customers_count : 0;

        $jobs = collect();
        $jobs = $jobs->merge($task->tasksJobToday);
        $jobs = $jobs->merge($task->tasksJobTomorrow);
        $jobs = $jobs->merge($task->tasksJobAfterTomorrowTo14thDay);
        $jobs = $jobs->merge($task->tasksJob15thDayToEndMonth);
        $jobs = $jobs->merge($task->tasksJobNextMonth);
        $jobs = $jobs->merge($task->tasksjobsCompleted);
        $jobs = $jobs->merge($task->tasksjobsFailed);


        return view('admin.tasks.text-blast-send.view', compact('task', 'jobs', 'project', 'client', 'subAccount', 'selectedCampaign', 'getTotal', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Client $client, SubAccount $subAccount, Request $request, Task $task)
    {
        try {
			// Process text blast creation
			$task = $this->textBlastRepo->addTask($request, $task, $subAccount, $client);

            if ( isset($request->input_submit_task) == true ){
                return redirect()->route('admin.project.client.sub.task.tbs.index', [$project, $client, $subAccount] )
                        ->withSuccess("Successfully Updated Task.");
            } else {

                return redirect()->route('admin.project.client.sub.task.tbs.view', [$project, $client, $subAccount, $task] )
                        ->withSuccess("Text blast successfully created.");
            }
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateFormView(Project $project, Client $client, SubAccount $subAccount, Request $request, Task $task) {
        try {
            // Process text blast creation
            $taskProcess = $this->textBlastRepo->updateTask($request, $task);

            return redirect()->route('admin.project.client.sub.task.tbs.view', [$project, $client, $subAccount, $task->id] )
                        ->withSuccess("Text blast successfully created.");

        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Client $client, SubAccount $subAccount, Request $request, Task $task)
    {
        try {
            $request['dont_detach'] = true;
            $this->taskRepo->delete($request, $task->id);
            return redirect()->route('admin.project.client.sub.task.tbs.index', [$project, $client, $subAccount] )
                        ->withSuccess("Task successfully deleted.");
        } catch (\DomainException $e) {
            return redirect()->back()
            				->withError($e->getMessage());
        }
    }

    // @depreciated na to, ginawa na siyang global nasa Api/Campaign/CampaignController
    public function ajaxCreateCampaign(Project $project, Client $client, SubAccount $subaccount, Request $request, Campaign $campaign) {
        try {
            $this->campaignRepo->store($campaign, $request);
            $campaign = $this->campaignRepo->search($request)->where('subaccount_id','=',$request->sub_account_id)->pluck('name', 'id');

            return view('partials.task.campaign-options', compact('campaign'));
        } catch (ValidationException $e) {
            return response()->json(['success' => false]);
        }
    }

    public function ajaxGenerateRecepient(Request $request)
	{
        $option = $request->getOption;
        if ( $option ) {
			$request['not_empty_mobile'] = true;
            $customers = $this->customersQueryRepo->search($request, true);
            $customersIds = $customers->implode('id', ',');
            return response()->json(['success' => true, 'data' => $customers->count(), 'ids' => $customersIds]);
        } else {
            return response()->json(['success' => false]);
        }
    }

    public function ajaxViewRecipient(Request $request)
	{
		$request['not_empty_mobile'] = true;
		$customers = $this->customersQueryRepo->search($request, true);
		$customerCount = $customers->count();

		return view('partials.task.ajax-table-recipients', compact('customers', 'customerCount'));
    }
}
