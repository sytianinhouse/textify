<?php

namespace App\Http\Controllers\Admin\Task\ImmediateTask;

use ATM\Repositories\Client\Client;
use ATM\Repositories\Project\Project;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Tasks\ImmediateTaskRepository;
use ATM\Repositories\Tasks\Task;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ImmediateTaskController extends Controller
{
	public function __construct(
		ImmediateTaskRepository $immediateTaskRepo
	)
	{
		$this->immediateTaskRepo = $immediateTaskRepo;
	}

	public function index(Project $project, Client $client, SubAccount $subAccount, Request $request)
	{
		$title = $client->company_name . ' - Custom API';
		$tasks = Task::with('campaigns')
			->where('subaccount_id', $subAccount->id)
			->where('task_type_id', \ATM\Repositories\Tasks\Task::IMMEDIATE_TASK)->get();

		return view('admin.tasks.immediate-task.index', compact( 'client', 'project', 'subAccount', 'tasks', 'title'));
    }

	public function create(Project $project, Client $client, SubAccount $subAccount, Request $request)
	{
		$title = $client->company_name . ' - Custom API';
		$campaigns = $subAccount->campaigns->pluck('name', 'id');

		return view('admin.tasks.immediate-task.create', compact( 'client', 'project', 'subAccount', 'tasks', 'title', 'campaigns'));
    }

	public function store(Project $project, Client $client, SubAccount $subAccount, Request $request)
	{
		try {
			$request['client_id'] = $client->id;
			$request['subaccount_id'] = $subAccount->id;
			$task = $this->immediateTaskRepo->addTask($request);

			return redirect()
				->route('admin.project.client.sub.task.it.edit', [$project, $client, $subAccount, $task] )
				->withSuccess("Created succesfully!");

		} catch (ValidationException $e) {
			return $this->redirectFormRequest($e);
		}
    }

	public function edit(Project $project, Client $client, SubAccount $subAccount, Task $task, Request $request)
	{
		$title = $client->company_name . ' - Edit Custom API';
		$campaigns = $subAccount->campaigns->pluck('name', 'id');

		return view('admin.tasks.immediate-task.edit', compact( 'client', 'project', 'subAccount', 'task', 'title', 'campaigns'));
    }

	public function update(Project $project, Client $client, SubAccount $subAccount, Task $task, Request $request)
	{
		try {
			$request['client_id'] = $client->id;
			$request['subaccount_id'] = $subAccount->id;
			$task = $this->immediateTaskRepo->addTask($request, $task);

			return redirect()
				->route('admin.project.client.sub.task.it.edit', [$project, $client, $subAccount, $task] )
				->withSuccess("Updated succesfully!");

		} catch (ValidationException $e) {
			return $this->redirectFormRequest($e);
		}
    }

	public function destroy(Project $project, Client $client, SubAccount $subAccount, Task $task, Request $request)
	{
		if ($task->hasJobs()) {
			return redirect()
					->back()
					->withErrors('This task has current jobs, deletion is not allowed.');
		}

		$this->immediateTaskRepo->removeTask($task);

		return redirect()
				->back()
				->withSuccess('Task deleted!');
    }
}
