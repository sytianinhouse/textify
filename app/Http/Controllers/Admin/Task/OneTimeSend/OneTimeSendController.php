<?php

namespace App\Http\Controllers\Admin\Task\OneTimeSend;

use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Cities\City;
use ATM\Repositories\Client\Client;
use ATM\Repositories\CustomerGroups\CustomerGroupsRepository;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Shortcodes\Shortcodes;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\Tasks\OneTimeTaskRepository;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Tasks\TaskType\TaskType;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OneTimeSendController extends Controller
{
    public function __construct (
		CampaignRepository $campaignRepo,
		SubAccountRepository $subAccountRepo,
		CustomersRepository $customerRepo,
		CustomerGroupsRepository $customerGroupRepo,
		OneTimeTaskRepository $oneTimeTaskRepo
	)
    {
    	$this->customerGroupRepo = $customerGroupRepo;
        $this->subAccountRepo = $subAccountRepo;
        $this->campaignRepo   = $campaignRepo;
        $this->customerRepo = $customerRepo;
        $this->oneTimeTaskRepo = $oneTimeTaskRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project, Client $client, SubAccount $subAccount, Request $request)
    {
    	$title = $client->company_name . ' - Template API';
        $tasks = Task::with('campaigns')
                    ->where('subaccount_id', $subAccount->id)
                    ->where('task_type_id', \ATM\Repositories\Tasks\Task::ONE_TIME_SEND)
					->orderByDesc('created_at')->get();

        return view('admin.tasks.one-time-send.index', compact( 'client', 'project', 'subAccount', 'tasks', 'title'));

    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project, Client $client, SubAccount $subAccount, Request $request)
    {
        $title = $client->company_name . ' - Add Task';
        // $taskTypes = TaskType::pluck('title', 'id'); // Di pa need
        $clientID = $client->id;
       	$request['sub_account_id'] = $subAccount->id;
        $campaigns = $this->campaignRepo->search($request, [], true)->pluck('name', 'id'); // campaigns
        $selectedCampaign = collect();
        $cities = City::pluck('name', 'name'); // get all cities
        $selectedCities = 'Quezon city';

        $messagePlaceholder = "Thank you for visiting us [first_name] [last_name]! \n\nPlease come see us again.";
        $shortCodes = Shortcodes::$shortCodes;

        return view('admin.tasks.one-time-send.create', compact('task', 'project', 'client', 'subAccount', 'campaigns', 'selectedCampaign', 'cities', 'selectedCities', 'clientID', 'title', 'shortCodes', 'messagePlaceholder'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Client $client, SubAccount $subAccount, Request $request)
    {
        try {

            $request['task_type_id'] = \ATM\Repositories\Tasks\Task::ONE_TIME_SEND;
        	$request['sub_account_id'] = isset($subAccount->id)? $subAccount->id : 0;
            $request['time_to_send'] = Carbon::now()->startOfDay()->hour($request->time_to_send[0])->minute($request->time_to_send[1])->format('H:i');
            $request['prevent_job_fetch'] = false;

            $task = $this->oneTimeTaskRepo->add($request, $client);

            return redirect()->route('admin.project.client.sub.task.ots.index', [$project, $client, $subAccount] )
                        ->withSuccess("Successfully added A New Task.");

        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Project $project, Client $client, SubAccount $subAccount, Task $task)
    {
        $title = $client->company_name . ' - Edit Task';

        $request['sub_account_id'] = $subAccount->id;
        $campaigns = $this->campaignRepo->search($request, [], true)->pluck('name', 'id'); // campaigns
        $selectedCampaign = $task->campaign_id;
        $shortCodes = Shortcodes::$shortCodes;
        $messagePlaceholder = "Hi [first_name], [last_name] \n\nThis is our promo.";

        return view('admin.tasks.one-time-send.edit', compact('task', 'project', 'client', 'subAccount', 'campaigns', 'selectedCampaign', 'title', 'shortCodes', 'messagePlaceholder'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Client $client, SubAccount $subAccount, Request $request, Task $task)
    {
        try {
            $request['time_to_send'] = Carbon::now()->startOfDay()->hour($request->time_to_send[0])->minute($request->time_to_send[1])->format('H:i');
            $request['prevent_job_fetch'] = false;

            $task = $this->oneTimeTaskRepo->update($task->id, $request, $client);

            if ( !$task->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($task->getErrors())
                            ->withError("Please check required fields.");
            }
            return redirect()->route('admin.project.client.sub.task.ots.index', [$project, $client, $subAccount] )
                        ->withSuccess("Successfully Updated Customer Groups.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Client $client, SubAccount $subAccount, Request $request, Task $task)
    {
        try {

            $request['dont_detach'] = true;
            $this->oneTimeTaskRepo->delete($request, $task->id);

            return redirect()->route('admin.project.client.sub.task.ots.index', [$project, $client, $subAccount] )
                        ->withSuccess("Task successfully deleted.");
        } catch (\DomainException $e) {
            return redirect()->back()
            				->withError($e->getMessage());
        }
    }

    /**
     * Able to create quick campaign creation
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajaxCreateCampaign(Request $request, SubAccount $subaccount, Campaign $campaign) {
        try {
            $this->campaignRepo->store($campaign, $request);

            $campaign = $this->campaignRepo->search($request)->sortBy('id');
            $lastCampaign = $campaign->last();

            $campaign = $campaign->pluck('name', 'id');

            return view('partials.task.campaign-options', compact('campaign', 'lastCampaign'));
        } catch (ValidationException $e) {
            return response()->json(['success' => false]);
        }
    }

}
