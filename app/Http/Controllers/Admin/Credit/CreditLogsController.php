<?php

namespace App\Http\Controllers\Admin\Credit;

use ATM\Repositories\Client\ClientRepository;
use ATM\Repositories\Credits\Credit;
use ATM\Repositories\Credits\CreditLog;
use ATM\Repositories\Credits\CreditLogRepository;
use ATM\Repositories\Credits\CreditRepository;
use ATM\Repositories\Project\ProjectRepository;
use ATM\Repositories\SubAccount\SubAccountRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CreditLogsController extends Controller
{

	public $rules = [
				'notes' => 'required',
				'balance' => 'required|numeric',
            ];

    public function __construct 
    ( 
    	CreditLogRepository $creditLogRepo,
        ProjectRepository $projectRepo,
        ClientRepository $clientRepo,
        SubAccountRepository $subAccountRepo
    )
    {
        $this->creditLogRepo = $creditLogRepo;
        $this->projectRepo = $projectRepo;
        $this->clientRepo = $clientRepo;
        $this->subAccountRepo = $subAccountRepo;
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $title = 'Credit Logs';

        $logTypes = CreditLog::$types;
        $projects = $this->projectRepo->all()->pluck('name', 'id');
        $clients = collect();
        $subAccounts = collect();

        if ($request->filled('project_id')) {
            $clients = $this->projectRepo->find($request->project_id)->clients->pluck('company_name', 'id');
        } 
        
        if ($request->filled('client_id')) {
            $subAccounts = $this->clientRepo->find($request->client_id)->subAccounts->pluck('account_name', 'id');
        } 

        $creditLogs = $this->creditLogRepo->search($request, $request->per_page, ['credit', 'subAccount']);
        
        $filters = [
            'project_id' => $request->project_id,
            'client_id' => $request->client_id,
            'sub_account_id' => $request->sub_account_id,
            'type' => $request->type,
            'from_date' => $request->from_date,
            'to_date' => $request->to_date,
            'start_filter' => $request->start_filter,
            'per_page' => $request->per_page,
        ];
       
        return view('admin.credits.logs.index', compact('projects','clients','subAccounts', 'title', 'creditLogs', 'filters', 'logTypes'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Credit $credit, Request $request)
    {
        try {
            $roleName = $this->creditLogRepo->delete($credit);
            
            return redirect()->route('admin.load.index')
                        ->withSuccess("Successfully deleted Credit.");
        } catch (\DomainException $e) {
            return redirect()->back()
            				->withError($e->getMessage());
        }
    }
}
