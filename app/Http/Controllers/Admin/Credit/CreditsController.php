<?php

namespace App\Http\Controllers\Admin\Credit;

use ATM\Repositories\Client\ClientRepository;
use ATM\Repositories\Credits\Credit;
use ATM\Repositories\Credits\CreditRepository;
use ATM\Repositories\Project\ProjectRepository;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CreditsController extends Controller
{

	public $rules = [
		'notes' => 'required',
		'balance' => 'required|numeric',
	];

    public function __construct
    (
    	CreditRepository $creditRepo,
        ProjectRepository $projectRepo,
        ClientRepository $clientRepo,
        SubAccountRepository $subAccountRepo
    )
    {
        $this->creditRepo = $creditRepo;
        $this->projectRepo = $projectRepo;
        $this->clientRepo = $clientRepo;
        $this->subAccountRepo = $subAccountRepo;
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Credits';

        $projects = $this->projectRepo->all()->pluck('name', 'id');
        $clients = collect();
        $subAccounts = collect();

        if ($request->filled('project_id')) {
            $clients = $this->projectRepo->find($request->project_id)->clients->pluck('company_name', 'id');
        }

        if ($request->filled('client_id')) {
            $subAccounts = $this->clientRepo->find($request->client_id)->subAccounts->pluck('account_name', 'id');
        }

        $credits = $this->creditRepo->search($request, $request->per_page, [
        	'client' => function($q) {
        		return $q->withTrashed();
			},
			'subAccount' => function($q) {
				return $q->withTrashed();
			}
        ]);

        // $credits = $credits->orderBy('updated_at', 'desc');

        $filters = [
            'project_id' => $request->project_id,
            'client_id' => $request->client_id,
            'sub_account_id' => $request->sub_account_id,
            'per_page' => $request->per_page,
            'active' => $request->active,
        ];

        return view('admin.credits.index', compact('projects','clients','subAccounts', 'title', 'credits', 'filters'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Credit $credit, Request $request)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Credit $credit, Request $request)
    {
        $title = 'Edit Credit';

        $logs = $credit->creditLogs()->orderBy('date_time', 'desc')->take(5)->get();

        return view('admin.credits.edit', compact('credit', 'title', 'logs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Credit $credit, Request $request)
    {
        try {

            $input = $request->all();

			$credit = $this->creditRepo->updateManual($credit, $request);

            if ( ! $credit->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($credit->getErrors())
                            ->withError("Please check required fields.");
            }

            return redirect()->route('admin.credit.index')
                        ->withSuccess("Successfully Updated Credit.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Credit $credit, Request $request)
    {
        try {
            $roleName = $this->creditRepo->delete($credit);

            return redirect()->route('admin.load.index')
                        ->withSuccess("Successfully deleted Credit.");
        } catch (\DomainException $e) {
            return redirect()->back()
            				->withError($e->getMessage());
        }
    }
}
