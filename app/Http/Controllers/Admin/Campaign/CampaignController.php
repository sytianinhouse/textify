<?php

namespace App\Http\Controllers\Admin\Campaign;

use ATM\Repositories\ApiKey\ApiKey;
use ATM\Repositories\ApiKey\ApiKeyRepository;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Countries\Country;
use ATM\Repositories\Project\Project;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CampaignController extends Controller
{
    public $campaign;

    public function __construct(
        CampaignRepository $campaign
    )
    {
        $this->campaign = $campaign;
    }

    public function index(Request $request, Project $project, Client $client, SubAccount $subaccount, Campaign $campaign)
    {
        $title    = $subaccount->account_name . "'s - Campaigns";
        $campaign = $subaccount->campaigns;
        $sbcount = $subaccount->all();

        $query = [
            'per_page' => $request->per_page
        ];

        return view('admin.campaign.index', compact('project','client','subaccount','sbcount','title', 'campaign','query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project, Client $client, SubAccount $subaccount, Campaign $campaign)
    {
        $campaign = Campaign::all();
        $title    = $subaccount->account_name. ' - Add Campaign';

        return view('admin.campaign.create', compact('project','client','subaccount','campaign','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Project $project, Client $client, SubAccount $subaccount, Campaign $campaign)
    {
        try {
            $request['sub_account_id'] = $subaccount->id;
            $request['client_id'] = $client->id;
            $campaign = $this->campaign->store($campaign, $request);
            if ( ! $subaccount->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($campaign->getErrors())
                            ->withError("Please check required fields.");
            }
            return redirect()->route('admin.project.client.sub.camp.index',[$project, $client,$subaccount])
                            ->withSuccess("Campaign {$campaign->name} successfully added!");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, Client $client, SubAccount $subaccount , Campaign $campaign)
    {
        $title = 'Edit Campaign - ' . $campaign->name;

        return view('admin.campaign.edit', compact('project','client','subaccount','campaign','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Client $client, SubAccount $subaccount, Campaign $campaign, Request $request)
    {
        try {
            $request['sub_account_id'] = $subaccount->id;
            $campaign = $this->campaign->update($campaign, $request);
            return redirect()->back()
                        ->withSuccess("Campaign {$campaign->name} successfully updated!");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Project $project, Client $client, SubAccount $subaccount, Campaign $campaign)
    {
        $name = $campaign->name;

        $tasks = $campaign->tasks;

        if ($tasks->count() > 1) {
            return redirect()->back()
                    ->withErrors("There is/are task associated with Campaign {$campaign->name}!");
        } else {
            $this->campaign->delete($campaign);
        }


        return redirect()->back()
                    ->withSuccess("Campaign {$campaign->name} successfully deleted!");
    }
}
