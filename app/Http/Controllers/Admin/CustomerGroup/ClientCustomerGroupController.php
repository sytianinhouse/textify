<?php

namespace App\Http\Controllers\Admin\CustomerGroup;

use ATM\Repositories\Client\Client;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\CustomerGroups\CustomerGroupsRepository;
use ATM\Repositories\Project\Project;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class ClientCustomerGroupController extends Controller
{
    public $customerGroup;

    public function __construct(CustomerGroupsRepository $customerGroup)
    {
        $this->customerGroupRepo = $customerGroup;
    }

    public function index(Project $project, Client $client, CustomerGroup $customergroup, SubAccount $subaccount)
    {
        $title = $client->company_name . ' - Groups';
        $customergroup = $client->customerGroups()->get();
        /*$customergroup = CustomerGroup::with(['subAccounts.client'])->whereHas('subAccounts', function($q) use ($client) {
           return $q->where('subaccount_id', $client->id);
        })->get();*/

    	return view('admin.customer-groups.index', compact('customergroup', 'client', 'project', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project, Client $client, SubAccount $subaccount, CustomerGroup $customergroup)
    {
        // dd( $subaccount );
        $title = $client->company_name . ' - Add Group';

        return view('admin.customer-groups.create', compact('project', 'client', 'customergroup', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Client $client, Request $request)
    {
        try {

            $input = $request->all();
            $rules = [
                'name' => 'required',
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } else {
                $customerGroup = $this->customerGroupRepo->add($request, $client);
            }
            if ( !$customerGroup->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($customerGroup->getErrors())
                            ->withError("Please check required fields.");
            }
            return redirect()->route('admin.project.client.customer-group.index', [$project, $client] )
                        ->withSuccess("Successfully added Group.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client, CustomerGroup $customergroup)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, Client $client, CustomerGroup $customergroup, SubAccount $subaccount)
    {
        $title = $client->company_name . ' - Edit Group';
        //$subaccounts = SubAccount::where('client_id', $client->id)->pluck('account_name', 'id');
        //$selectedSubAccnt = $customergroup->subAccounts->pluck('id');
        return view('admin.customer-groups.edit', compact('project', 'client', 'customergroup', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project, Client $client, CustomerGroup $customergroup)
    {
        try {
            $customerGroup = $this->customerGroupRepo->update($customergroup, $request);
            if ( !$customerGroup->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($customerGroup->getErrors())
                            ->withError("Please check required fields.");
            }
            return redirect()->route('admin.project.client.customer-group.index', [$project, $client] )
                        ->withSuccess("Successfully Updated Group.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Client $client, CustomerGroup $customergroup)
    {
        try {
            $roleName = $this->customerGroupRepo->delete($customergroup);
            return redirect()->route('admin.project.client.customer-group.index', [$project, $client] )
                        ->withSuccess("Successfully deleted $roleName.");
        } catch (\DomainException $e) {
            return redirect()->back()
            				->withError($e->getMessage());
        }
    }

}
