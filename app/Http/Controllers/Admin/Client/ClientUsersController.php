<?php

namespace App\Http\Controllers\Admin\Client;

use ATM\Repositories\Client\Client;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Roles\Role;
use ATM\Repositories\Roles\RoleRepository;
use ATM\Repositories\User\UserRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClientUsersController extends Controller
{
	public function __construct(
        UserRepository $userRepo,
        RoleRepository $roleRepo
    )
    {
        $this->userRepo = $userRepo;
        $this->roleRepo = $roleRepo;
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project, Client $client, Request $request)
    {
        $title = $client->company_name . ' - Users';
        $users = $client->users;

        return view('admin.users.index', compact('project', 'client', 'users', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project, Client $client, Request $request)
    {
        
        $title = 'Add User';
        $accountTypes = User::$clientAccounts;
        //dd( $accountTypes );
        $subAccounts = $client->subAccounts->pluck('account_name', 'id');
        $roles = $this->roleRepo->ofClient($client)->pluck('display_name', 'id');
        
        return view('admin.users.create', compact('project', 'client', 'accountTypes', 'roles', 'title', 'subAccounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Client $client, Request $request)
    {
        try {
            $user = $this->userRepo->addUser($client, $request);

            // return $user;
            return redirect()->route('admin.project.client.users.index', [$project, $client])
                        ->withSuccess("Successfully added user.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project, Client $client, User $user, Request $request)
    {
        return view('admin.clients.users.details', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, Client $client, User $user, Request $request)
    {
        $title = $user->name . ' - Edit User';
        $accountTypes = User::$clientAccounts;
        $subAccounts = $client->subAccounts->pluck('account_name', 'id');
        $roles = $this->roleRepo->ofClient($client)->pluck('display_name', 'id');

        return view('admin.users.edit', compact('project', 'user', 'client', 'roles', 'accountTypes', 'title', 'subAccounts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Client $client, User $user, Request $request)
    {
        try {
            $user = $this->userRepo->updateUser($client, $user, $request);

            return redirect()->route('admin.project.client.users.index', [$project, $client])
                        ->withSuccess("Successfully updated user.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Client $client, User $user)
    {
        $name = $user->name;
        $user->delete();

        return redirect()->back()
                    ->withSuccess("Successfully deleted user {$name}");
    }

    protected function validatePassword($password, $pwdConfirmation)
    {
        $validator = Validator::make(
            [
            'password' => $password,
            'password_confirmation' => $pwdConfirmation,
            ],
            ['password' => 'required|min:6|confirmed']
        );

        return $validator->errors();
    }
}
