<?php

namespace App\Http\Controllers\Admin\Client;

use ATM\Repositories\Cities\City;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Client\ClientRepository;
use ATM\Repositories\Countries\Country;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Roles\Role;
use ATM\Repositories\Roles\RoleRepository;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\Uploadable\UploadableRepository;
use ATM\Repositories\User\UserRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;


class ClientsController extends Controller
{
    public function __construct(
        ClientRepository $clientRepo,
        UploadableRepository $uploadRepo,
        RoleRepository $roleRepo,
        UserRepository $userRepo,
        SubAccountRepository $subRepo
    )
    {
        $this->clientRepo = $clientRepo;
        $this->uploadRepo = $uploadRepo;
        $this->roleRepo = $roleRepo;
        $this->userRepo = $userRepo;
        $this->subRepo = $subRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project)
    {
        $clients = $project->clients;
        $title = 'Clients';

        $clients = $clients->sortByDesc(function($client) {
            $active = $client->subAccounts->first(function($subAccount) {
                return $subAccount->isActive();
            });
            return $active ? $client->created_at->timestamp : 0;
        });

        return view('admin.clients.index', compact('clients', 'project', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project)
    {
        $countries  = Country::pluck('name', 'iso_3166_2');
        $title = 'Add Client';

        $getCities = City::get();
        $cities = [];
        $getCities->each(function($value,$key) use (&$cities){
            $name = "{$value['name']}, {$value->province->name}";
            $cities[$value['id']] = $name;
        });

        $selectedCity = 1367;

        return view('admin.clients.create', compact('project', 'countries', 'cities', 'selectedCity', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Client $client, Request $request)
    {
        try {
            $request['project_id'] = $project->id;

            $input = $request->all();

            $rules = [
                'company_name' => 'required',
                // 'city' => 'required',
                // 'address_1' => 'required',
                'contact_first_name' => 'required',
                'contact_email' => 'email|nullable',
                'cuid' => [
                    'required',
                    'min:4',
                    'max:25',
                    Rule::unique('clients')->where(function ($query) use($project) {
                        return $query->where('project_id', $project->id);
                    })
                ],
            ];

            $messages = [
                'cuid.required' => 'Unique Client ID is required.',
                'cuid.unique' => 'Unique Client ID has already been taken!',
            ];

            $validator = Validator::make($input, $rules, $messages);

            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } else {
                $client = $this->fillData(new Client, $request);
            }

            if ( ! $client->save()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($client->getErrors())
                            ->withError("Please check required fields.");
            }

            $this->uploadMedia($request, $client);

            $role = $this->roleRepo->createDefaults($client);
            $subAccount = $this->subRepo->createDefault($client);

            // $client['contact_firstname'] = $client->contact_first_name;
            // unset($client['contact_firstname']);

            $this->userRepo->createDefaultUser($client);
            $this->userRepo->createDefaultUser($client, true, $subAccount, $role);

            return redirect()->route('admin.project.client.sub.index', [$project, $client] )
                        ->withSuccess("Successfully added client.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return view('admin.clients.details', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, Client $client)
    {
        $title = 'Edit Client - ' . $client->company_name;
        $countries  = Country::pluck('name', 'iso_3166_2');

        $getCityList = City::get();
        $cities = [];
        $getCityList->each(function($value,$key) use (&$cities){
            $name = "{$value['name']}, {$value->province->name}";
            $cities[$value['id']] = $name;
        });

        return view('admin.clients.edit', compact('project','client', 'countries', 'cities', /* 'selectedCity', */ 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Client $client, Request $request)
    {
        try {

            $getCities = City::find($request->city);
            $request['city'] = ( $getCities ) ? $getCities->name : 'Select City';
            $request['active'] = ( $request->active == 1 ) ? 1 : 0;

            $input = $request->all();

            $rules = [
                'company_name' => 'required',
                'city' => 'required',
                // 'address_1' => 'required',
                'contact_first_name' => 'required',
                'contact_email' => 'email|nullable',
                'cuid' => [
                    'required',
                    'min:4',
                    'max:25',
                    Rule::unique('clients')->ignore($client->id)->where(function ($query) use($project) {
                        return $query->where('project_id', $project->id);
                    })
                ],
            ];

            $messages = [
                'cuid.required' => 'Unique Client ID is required.',
                'cuid.unique' => 'Unique Client ID has already been taken!',
            ];

            $validator = Validator::make($input, $rules, $messages);

            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } else {
                $client = $this->fillData($client, $request);
            }

            if ( ! $client->save()) {
                return redirect()->back()
                            ->withErrors($client->getErrors())
                            ->withInput();
            }

            $this->uploadMedia($request, $client);

            return redirect()->route('admin.project.client.index', [$project, $client] )
                        ->withSuccess("Successfully updated client.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Client $client)
    {
        $client->delete();

        return redirect()->back()
                ->withSuccess("Successfully deleted client");
    }

    public function fillData(Client $client, Request $request)
    {
        $client->fill($request->input());
        $client->project_id = $client->project_id ?: $request->project_id;
        $client->active = $request->active ? true : false;

        return $client;
    }

    private function uploadMedia(Request $request, $client)
    {
        if ($request->hasFile('icon')) {
            $this->uploadRepo->createUpload($client, $request, 'image', [
                'key' => Client::IMG_ICON,
                'field_key' => 'icon',
                'path' => 'uploads/client',
                'filename' => 'icon',
            ]);
        }

        if ($request->hasFile('thumb')) {
            $this->uploadRepo->createUpload($client, $request, 'image', [
                'key' => Client::IMG_THUMB,
                'field_key' => 'thumb',
                'path' => 'uploads/client',
                'filename' => 'thumb',
                'width' => 320,
                'height' => 200,
            ]);
        }

        if ($request->hasFile('banner')) {
            $this->uploadRepo->createUpload($client, $request, 'image', [
                'key' => Client::IMG_BANNER,
                'field_key' => 'banner',
                'path' => 'uploads/client',
                'filename' => 'banner',
                'width' => 1366,
                'height' => 345
            ]);
        }
    }

    public function validateCuid(Request $request)
    {
        $client = $this->clientRepo->getFirstBy('id', $request->client_id);
        try {
            $this->clientRepo->validateCuid($request->cuid, $client);

            if ($request->ajax()) {
                return response()->json(['success' => true]);
            }

            return true;
        } catch (ValidationException $e) {
            // if ($request->ajax()) {
            //     return response()
            // }
            return $this->redirectFormRequest($e, $request->ajax());
        }
    }

    public function setDefaultSettings($client)
    {

    }
}
