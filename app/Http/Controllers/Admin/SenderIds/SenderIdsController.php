<?php

namespace App\Http\Controllers\Admin\SenderIds;

use ATM\Repositories\Client\Client;
use ATM\Repositories\Client\ClientRepository;
use ATM\Repositories\Project\Project;
use ATM\Repositories\SenderIds\SenderIds;
use ATM\Repositories\SenderIds\SenderIdsRepository;
use ATM\Repositories\Tasks\Task;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SenderIdsController extends Controller
{
    public function __construct(
        SenderIdsRepository $senderIdsRepo,
        ClientRepository $clientRepo
    )
    {
        $this->senderIdsRepo = $senderIdsRepo;
        $this->clientRepo     = $clientRepo;
    }

    /**
     * SenderIds Index
     * Show List of Index
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request, Project $project, Client $client )
    {
        $title = $client->company_name . '`s - Sender Ids';
        $senderIds = $client->senderids;

        // $smsFacade = resolve(\ATM\Repositories\Sms\SmsFacade::class);
        // $task = Task::find(99);
        // $request['name'] = 'Jr';
        // $request['number'] = '09399078704';
        // $request['message'] = 'This is a test message only!';
        // $request['task_id'] = $task->id;

        // foreach ($senderIds as $senderId) {
        //     $result = $smsFacade->createImmediateJob($request, $client->subAccounts()->first(), $task, $senderId->name);
        // }

        // $jobsCompleted = \DB::table('jobs_completed')
        //     ->selectRaw("SUM(credits_consumed) as total_credits_consumed, client_id")
        //     ->where('client_id', $client->id)
        //     ->get();
        // dd($jobsCompleted);

        $jobsCompleted = \ATM\Repositories\Tasks\JobsCompleted\JobsCompleted::where('client_id', $client->id)
                            ->get();

        $group = $jobsCompleted->groupBy(function ($job, $key) {
            return $job->created_at->year;
        });

        return view('admin.senderids.index', compact('title','project','client','senderIds', 'group'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project, Client $client, SenderIds $senderIds)
    {
        $title = $client->company_name . ' - Add Sender Ids';

        return view('admin.senderids.create', compact('title','client','project','senderIds'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Client $client, Request $request, SenderIds $senderIds)
    {
        try {
            $request['client_id'] = $client->id;
            $input = $request->all();

			$senderIds = $this->senderIdsRepo->store($senderIds, $request);

            if ( ! $senderIds->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($senderIds->getErrors())
                            ->withError("Please check required fields.");
            }
            return redirect()->route('admin.project.client.senderids.index',[$project, $client])
                            ->withSuccess("Sender Id {$senderIds->name} successfully added!");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, Client $client, SenderIds $senderids)
    {
        $title = 'Edit Sender Id - ' . $senderids->name;

        return view('admin.senderids.edit', compact('project','client','senderids', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Client $client, Request $request,  SenderIds $senderids)
    {
        try {
            $input = $request->all();

			$senderids = $this->senderIdsRepo->update($senderids, $request);

            return redirect()->route('admin.project.client.senderids.index',[$project, $client])
                        ->withSuccess("Sender Id {$senderids->name} successfully updated!");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Client $client, Request $request, SenderIds $senderids)
    {
        $name = $senderids->name;
        $senderids->delete();

        return redirect()->back()
                    ->withSuccess("Sender Id {$name} successfully deleted!");
    }
}
