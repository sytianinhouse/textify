<?php

namespace App\Http\Controllers\Admin\Gateway;

use ATM\Repositories\ApiGateway\GatewayException;
use ATM\Repositories\ApiGateway\GlobeLabsFacade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class GlobeLabsController extends Controller
{
	public function __construct(GlobeLabsFacade $globeFacade)
	{
		$this->globeFacade = $globeFacade;
	}

	public function sms(Request $request)
	{
		try {
			$call = $this->globeFacade->prepareSms($request->number, $request->message);
			Log::info($call);

			if ($call) {
				return redirect()
					->route('admin.project.index')
					->withSuccess('SMS has been send!');
			}

		} catch (GatewayException $e) {
			return redirect()
				->route('admin.project.index')
				->withError($e->getMessage());
		}
    }

	public function receiveAuthorize(Request $request)
	{
		try {
			$call = $this->globeFacade->getAccessToken($request);

			return redirect()
				->route('admin.project.index')
				->withSuccess('Token receive!');
		} catch (GatewayException $e) {
			//return $this->redirectFormRequest($e);
			return redirect()
				->route('admin.project.index')
				->withErrors($e->getMessage());
		}
    }
}
