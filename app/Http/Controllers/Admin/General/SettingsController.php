<?php

namespace App\Http\Controllers\Admin\General;

use ATM\Repositories\Settings\SiteSettings;
use ATM\Repositories\Settings\SiteSettingsRepository;
use ATM\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
	public function __construct(
		SiteSettingsRepository $siteSettingsRepo
	)
	{
		$this->siteSettingsRepo = $siteSettingsRepo;
	}

	public function settings()
	{
		$title = 'General Settings';
		$settings = SiteSettings::getSettings();

		return view('admin.general.settings', compact('title', 'settings'));
	}

	public function settingsSave(Request $request)
	{
		try {

			$settings = $this->siteSettingsRepo->saveGeneralSettings($request);

			return redirect()->back()
				->withSuccess("Settings Successfully Updated.");

		} catch (ValidationException $e) {
			return $this->redirectFormRequest($e);
		}
	}

	public function smsSettings()
	{
		$title = 'SMS Settings';
		$settings = SiteSettings::getSettings();
		$gateways = SiteSettings::$gateways;

		return view('admin.general.sms-settings', compact('title', 'gateways', 'settings'));
	}

	public function smsSettingsSave(Request $request)
	{
		try {

			$textBlastSetting = $this->siteSettingsRepo->saveSmsSettings($request);

			return redirect()->back()
				->withSuccess("Settings Successfully Updated.");

		} catch (ValidationException $e) {
			return $this->redirectFormRequest($e);
		}
	}
}
