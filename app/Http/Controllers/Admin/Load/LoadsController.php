<?php

namespace App\Http\Controllers\Admin\Load;

use ATM\Repositories\Client\ClientRepository;
use ATM\Repositories\Loads\AutoLoad;
use ATM\Repositories\Loads\AutoLoadRepository;
use ATM\Repositories\Loads\Load;
use ATM\Repositories\Loads\LoadRepository;
use ATM\Repositories\Project\ProjectRepository;
use ATM\Repositories\SubAccount\SubAccountRepository;
use App\Http\Controllers\Controller;
use ATM\Validation\ValidationException;
use Illuminate\Http\Request;

class LoadsController extends Controller
{

	public $rules = [
        'project' => 'required|numeric',
        'client' => 'required|numeric',
    	'sub_account' => 'required|numeric',
		'date' => 'required|date',
		'load_amount' => 'required|numeric',
    ];

    public function __construct
    (
    	LoadRepository $loadRepo,
    	AutoLoadRepository $autoLoadRepo,
    	ClientRepository $clientRepo,
        ProjectRepository $projectRepo,
        SubAccountRepository $subRepo
    )
    {
        $this->loadRepo = $loadRepo;
        $this->clientRepo = $clientRepo;
        $this->projectRepo = $projectRepo;
        $this->subRepo = $subRepo;
        $this->autoLoadRepo = $autoLoadRepo;
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Loads';

        $projects = $this->projectRepo->all()->pluck('name', 'id');
        $clients = collect();
        $subAccounts = collect();
        $request['per_page'] = ($request->per_page) ? $request->per_page : 100;

        if ($request->filled('project_id')) {
            $clients = $this->projectRepo->find($request->project_id)->clients->pluck('company_name', 'id');
        }

        if ($request->filled('client_id')) {
            $subAccounts = $this->clientRepo->find($request->client_id)->subAccounts->pluck('account_name', 'id');
        }

        $loads = $this->loadRepo->search($request, $request->per_page ?: null, ['client.project', 'subAccount']);

        $query = [
            'project_id' => $request->project_id,
            'client_id' => $request->client_id,
            'sub_account_id' => $request->filled('sub_account_id') ? $request->sub_account_id : '',
            'from_date' => $request->from_date,
            'to_date' => $request->to_date,
            'per_page' => ($request->per_page) ? $request->per_page : 100,
        ];

        return view('admin.loads.index', compact('title', 'query', 'projects', 'loads', 'subAccounts', 'clients'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Add Load';

        $projects = $this->projectRepo->all();

        $clients = collect();

        $subAccounts = collect();

        return view('admin.loads.create', compact('clients', 'title', 'projects', 'subAccounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $input = $request->all();
            // dd($input);

            $validator = \Validator::make($input, $this->rules);

            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } else {
                $load = $this->loadRepo->add($request);
            }

            if ( ! $load->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($load->getErrors())
                            ->withError("Please check required fields.");
            }

            return redirect()->route('admin.load.index')
                        ->withSuccess("Successfully added Load.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client, Role $role)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Load $load, Request $request)
    {
        $title = 'Edit Load';
        $clients = $this->clientRepo->all()->load('subAccounts');

        return view('admin.loads.edit', compact('load', 'clients', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Load $load, Request $request)
    {
        try {

            $input = $request->all();

            $validator = \Validator::make($input, $this->rules);

            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } else {

                $role = $this->loadRepo->update($load, $request);
            }

            if ( ! $load->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($load->getErrors())
                            ->withError("Please check required fields.");
            }

            return redirect()->route('admin.load.index')
                        ->withSuccess("Successfully Updated Load.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Load $load, Request $request)
    {
        try {

            $loadName = $this->loadRepo->delete($load);

            return redirect()->route('admin.load.index')
                        ->withSuccess("Successfully deleted Load.");
        } catch (\DomainException $e) {
            return redirect()->back()
            				->withError($e->getMessage());
        }
    }

    public function getClients(Request $request)
    {
        try {
            $results = [];

            if ($request->filled('project_id')) {
                $clients = $this->clientRepo->search($request);
                $results['clients'] = $clients->pluck('company_name', 'id');
                unset($request['project_id']);

                $firstClient = $clients->first();

                $subAccounts = [];

                if ($firstClient) {
                    $request['client_id'] = $firstClient->id;
                    $subAccounts = $this->subRepo->search($request)->pluck('account_name', 'id');
                    unset($request['client_id']);
                }

                $results['sub_accounts'] = $subAccounts;
            }

            return json_encode($results);

        } catch (Exception $e) {

        }

    }

    public function getSubAccounts(Request $request)
    {
        try {
            $results = null;

            if ($request->filled('client_id')) {
                $results = $this->subRepo->search($request)->pluck('account_name', 'id');
            }

            return $results;
        } catch (Exception $e) {

        }
    }

	public function autoLoadIndex(Request $request)
	{
		$title = 'Auto Loads';

		$projects = $this->projectRepo->all()->pluck('name', 'id');
		$clients = collect();
		$subAccounts = collect();
		$request['per_page'] = ($request->per_page) ? $request->per_page : 100;

		if ($request->filled('project_id')) {
			$clients = $this->projectRepo->find($request->project_id)->clients->pluck('company_name', 'id');
		}

		if ($request->filled('client_id')) {
			$subAccounts = $this->clientRepo->find($request->client_id)->subAccounts->pluck('account_name', 'id');
		}

		$autoLoads = $this->autoLoadRepo->search($request, $request->per_page ?: null, ['client.project', 'subAccount']);

		$filters = [
			'project_id' => $request->project_id,
			'client_id' => $request->client_id,
			'sub_account_id' => $request->filled('sub_account_id') ? $request->sub_account_id : '',
			'per_page' => ($request->per_page) ? $request->per_page : 100,
		];

		return view('admin.auto-loads.index', compact('title', 'filters', 'projects', 'autoLoads', 'subAccounts', 'clients'));
    }

	public function createAutoLoad(Request $request)
	{
		$title = 'Create Auto Load';

		$projects = $this->projectRepo->all();
		$clients = collect();
		$subAccounts = collect();

		return view('admin.auto-loads.create', compact('clients', 'title', 'projects', 'subAccounts'));
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function storeAutoload(Request $request)
	{
		try {

			$load = $this->autoLoadRepo->add($request);

			if ( ! $load->save() ) {
				return redirect()->back()
					->withInput()
					->withErrors($load->getErrors())
					->withError("Please check required fields.");
			}

			return redirect()->route('admin.load.auto-loads.index')
				->withSuccess("Successfully created Auto Load.");

		} catch (ValidationException $e) {
			return $this->redirectFormRequest($e);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroyAutoload(AutoLoad $load, Request $request)
	{
		try {

			$loadName = $this->autoLoadRepo->delete($load);

			return redirect()->route('admin.load.auto-loads.index')
				->withSuccess("Successfully deleted Auto Load.");
		} catch (\DomainException $e) {
			return redirect()->back()
				->withError($e->getMessage());
		}
	}

	public function editAutoload(AutoLoad $load, Request $request)
	{
		$title = 'Update Auto Load';

		$projects = $this->projectRepo->all();
		$clients = collect();
		$subAccounts = collect();

		return view('admin.auto-loads.edit', compact('clients', 'title', 'projects', 'subAccounts', 'load'));
	}

	public function updateAutoload(AutoLoad $load, Request $request)
	{
		try {

			$load = $this->autoLoadRepo->update($load, $request);

			if ( ! $load->save() ) {
				return redirect()->back()
					->withInput()
					->withErrors($load->getErrors())
					->withError("Please check required fields.");
			}

			return redirect()->route('admin.load.auto-loads.index')
				->withSuccess("Successfully updated Auto Load.");

		} catch (ValidationException $e) {
			return $this->redirectFormRequest($e);
		}
	}
}
