<?php

namespace App\Http\Controllers\Admin;

use ATM\Repositories\Client\ClientRepository;
use ATM\Repositories\Project\ProjectRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        ClientRepository $clientRepo,
        ProjectRepository $projectRepo
    )
    {
        $this->clientRepo = $clientRepo;
        $this->projectRepo = $projectRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getClientsAjax(Request $request)
    {
        try {

            if ($request->filled('project_id')) {
                $project = $this->projectRepo->find($request->project_id);
            } else {
                $project = $request->user()->client->project;
            }

            $clients = $project->clients->pluck('company_name', 'id');
            
            return response()->json($clients);

        } catch (\DomainException $e) {
            return response()->withError($e->getMessage());
        }
    }

    public function getSubsAjax(Request $request)
    {
        try {
            
            if ($request->filled('client_id')) {
                $client = $this->clientRepo->find($request->client_id);
            } else {
                $client = $request->user()->client;
            }

            $subs = $client->subAccounts->pluck('account_name', 'id');
            
            return response()->json($subs);

        } catch (\DomainException $e) {
            return response()->withError($e->getMessage());
        }
    }
}
