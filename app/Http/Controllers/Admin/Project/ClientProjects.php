<?php

namespace App\Http\Controllers\Admin\Project;

use ATM\Repositories\Client\Client;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Project\ProjectRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClientProjects extends Controller {

	public function __construct ( ProjectRepository $projectRepo )
    {
        $this->projectRepo = $projectRepo;
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project, Client $client)
    {
        $title = 'Projects';
        $projects = Project::with(['clients'])->get();
        return view('admin.projects.index', compact('projects', 'title'));

    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project)
    {
        $title = $project->name . 'Add Project';
        return view('admin.projects.create', compact('project','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Request $request)
    {

        try {

            $project = $this->projectRepo->addProject($project, $request);

            if ( ! $project->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($project->getErrors())
                            ->withError("Please check required fields.");
            }

            return redirect()->route('admin.project.index', $project)
                        ->withSuccess("Successfully added Project.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //return view('admin.clients.users.details', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $title = 'Edit Project -'. $project->name;
        return view('admin.projects.edit', compact('project', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Request $request)
    {
        //dd( $request );
        try {

            $input = $request->all();

            $rules = [
                'name'    => 'required',
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } else {
                // $customer = $this->customerRepo->add($request, $client);
                $project = $this->projectRepo->updateProject($project, $request);
            }

            if ( ! $project->save()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($project->getErrors())
                            ->withError("Please check required fields.");
            }

            return redirect()->back()
                        ->withSuccess("Successfully updated.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        try {

            $projectName = $this->projectRepo->delete($project);

            return redirect()->back()
                        ->withSuccess("Successfully deleted project $projectName.");
        } catch (ValidationException $e) {
            return redirect()->back()
                            ->withError($e->getMessage());
        }
    }
}
