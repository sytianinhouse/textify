<?php

namespace App\Http\Controllers\Auth;

use ATM\Repositories\User\UserRepository;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

	protected $username = 'username';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;

        $this->middleware('guest', ['except' => ['logout', 'manageRedirect']]);
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        $credentials = $this->credentials($request);

        $errors = [];
        // if Client ID is of atimar admin then will attempt auth immediately
        if ($request->cuid == User::ADMIN_CLIENT_ID) {
            $credentials['account_type'] = 'superadmin';
            if ($this->guard()->attempt($credentials, $request->filled('remember'))) {
                return $this->sendLoginResponse($request);
            }
        } else {
            // find all with username and then filter by cuid
            $cuid = $request->cuid;
            $users = $this->userRepo->getAllBy('username', $request->username, ['client']);
            $user = $users->filter(function($user) use ($cuid) {
                return $user->client && $user->client->cuid == $cuid;
            })->first();

            if ($user) {
                // finally compare the hash password
                if (\Hash::check($request->password, $user->password)) {
                    Auth::login($user);
                    return $this->sendLoginResponse($request);
                }
                $errors['password'] = 'Invalid password';
            } else {
                $errors['username'] = 'Invalid username';
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    // private function checkIfActiveSubscription(User $user, $isOwner = false)
    // {
    //     if ($isOwner) {
    //         $branches = $user->client->branches()->actives()->get();
    //     } else {
    //         $branches = $user->branches()->actives()->get();
    //     }

    //     if ($branches->count() == 0) {
    //         Auth::logout();
    //         return false;
    //     }

    //     return true;
    // }

    public function authenticated(Request $request, User $user)
    {
        if (!$user->isActive()) {
            Auth::logout();
            return redirect()->back()
                        ->withErrors(__('auth.account.inactive'));
        }

        // set last login attribute
        $user->last_login = new \DateTime;
        $user->save();

        if ($user->isSuperadmin()) {
            return redirect()->route('admin.project.index')
                    ->cookie(
                        'cuid', $request->cuid, (60 * 24 * 5)
                    );
        }

        if ($user->isOwner()) {

            $flag = $this->checkIfActiveSubscription($user, true);
            if (!$flag) {
                return redirect()->back()
                    ->withErrors(__('auth.account.nosubaccounts'));
            }

            return redirect()->route('client.index')
                    ->cookie(
                        'cuid', $request->cuid, (60 * 24 * 5)
                    );

        } else if ($user->isAccountUser()) {

            $flag = $this->checkIfActiveSubscription($user);

            if (!$flag) {
                return redirect()->back()
                    ->withErrors(__('auth.account.inactive'));
            }

            $subAccount = $user->subAccounts()->actives()->first();

            if (!$subAccount) {
                Auth::logout();
                return redirect()->back()
                            ->withErrors("Access denied.");
            }

            session(['sub_account' => $subAccount->uuid]);
            return redirect()->intended(route('sub.index', $subAccount->uuid))
                        ->cookie(
                            'cuid', $request->cuid, (60 * 24 * 15)
                        );
        }

        return 'access denied!!!';
    }

    private function checkIfActiveSubscription(User $user, $isOwner = false)
    {
        if ($isOwner) {
            $subAccounts = $user->client->subAccounts()->actives()->get();
        } else {
            $subAccounts = $user->subAccounts()->actives()->get();
        }

        if ($subAccounts->count() == 0) {
            Auth::logout();
            return false;
        }

        return true;
    }

    public function username()
    {
        return 'username';
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required',
            'password' => 'required',
            'cuid' => 'required',
        ]);
    }

    /**
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return redirect()->back()
            ->withInput($request->only($this->username(), 'cuid', 'remember'))
            ->withErrors([
                $this->username() => Lang::get('auth.failed'),
            ]);
    }

    public function manageRedirect(Request $request)
    {
        $user = $request->user();
        if ($user) {
            if ($user->isSuperadmin()) {
                return redirect()->route('admin.index');
            }

            if ($user->isOwner()) {
                return redirect()->route('client.index');
            } else if ($user->isAccountUser()) {
                $subAccount = $user->subAccounts->first();
                if (!$subAccount) {
                    Auth::logout();
                    return redirect('login');
                }
                session(['sub_account' => $subAccount->uuid]);
                return redirect()->route('sub.index', $subAccount->uuid);
            }
        }

        return redirect('login');
    }
}
