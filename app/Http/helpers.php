<?php

if(! function_exists('getJobInstance')) {
	/**
	 * @param $param Can be object or collection
	 */
	function getJobInstance($instance) {
		if ($instance == 'today') {
			return 'ATM\Repositories\Tasks\JobsTodaySend\JobToday';
		}
		if ($instance == 'tomorrow') {
			return 'ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow';
		}
		if ($instance == '14thday') {
			return 'ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay';
		}
		if ($instance == '15thday') {
			return 'ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth';
		}
		if ($instance == 'nextmonth') {
			return 'ATM\Repositories\Tasks\JobsNextMonth\JobNextMonth';
		}
		if ($instance == 'completed') {
			return 'ATM\Repositories\Tasks\JobsCompleted\JobsCompleted';
		}
		if ($instance == 'failed') {
			return 'ATM\Repositories\Tasks\JobsFailed\JobsFailed';
		}

		return $instance;
	}

}

if(! function_exists('tooltipMessage')) {
	/**
	 * @param $param Can be object or collection
	 */
	function tooltipMessage($content, $label = 'SMS Text') {
		if ($content) {
			$heading = '';
			$heading .= "<div style='text-align:left; padding: 5px;'>";
			$heading .= "<h6 style='color: #fff; border-bottom: 1px solid #e5e5e5; padding-bottom: 5px;'>{$label}:</h6>";
			$heading .= htmlentities($content);
			$heading .= '</div>';

			return $heading;
		}

		return null;
	}

}

if(! function_exists('getTaskType')) {
	/**
	 * @param $param Can be object or collection
	 */
	function getTaskType($typeId) {
		if ($typeId) {
			return isset(ATM\Repositories\Tasks\Task::$typesArr[$typeId]) ? ATM\Repositories\Tasks\Task::$typesArr[$typeId] : '';
		}

		return null;
	}

}

if(! function_exists('makeCarbon')) {
	/**
	 * @param $param Can be object or collection
	 */
	function makeCarbon($date) {
		if ($date) {
			return new Carbon\Carbon($date);
		}

		return $date;
	}

}

if(! function_exists('rmZeroFirst')) {
	/**
	 * @param $param Can be object or collection
	 */
	function rmZeroFirst($number) {
		$result = \Illuminate\Support\Str::startsWith($number, '0');

		if ($result) {
			$formatted = \Illuminate\Support\Str::replaceFirst('0', '', $number);

			return $formatted;
		}

		return $number;
	}

}

if(! function_exists('formattedNumber')) {
	/**
	 * @param $param Can be object or collection
	 */
	function formattedNumber($number, $noHtml = false, $returnNUmber = false, $rawNumber = false) {
        $number = str_replace(['-', ' ', '(', ')', '+'], '', $number);
        $startsWith639 = startsWith($number, '639');
        $startsWith0 = startsWith($number, '0');
        $startsWith9 = startsWith($number, '9');
        $invalid = 'invalid-number';

        $formattedNumber = null;
        if( $startsWith639 && strlen($number) == 12 ) {
            $number = ltrim($number, '63');
			if ($rawNumber) { return $number; }
			return '+63'.$number;
        } elseif( $startsWith0 && strlen($number) == 11 ) {
            $number = ltrim($number, '0');
			if ($rawNumber) { return $number; }
			return '+63'.$number;
        } elseif( $startsWith9 && strlen($number) == 10 ) {
			return '+63'.$number;
        } elseif ($rawNumber) {
			return $number;
		} else {
        	if ($noHtml) {
        		$error = false;
			} elseif($returnNUmber) {
        		$error = $number;
            } else {
				$error = "<span class='invalid-number'>{$number}</span>";
            }
            return $error;
        }

		return $invalid;
	}

}

if( ! function_exists('startWith')  ) {
    function startsWith($string, $startString) {
        $len = strlen($startString);
        return (substr($string, 0, $len) === $startString);
    }
}

if(! function_exists('isThisClients')) {
    /**
     * @param $param Can be object or collection
     */
    function isThisClients($user, $ids) {
        $clientId = $user->client_id;
        if (is_array($ids)) {
            return in_array($clientId, $ids);
        } else {
            return $clientId == $ids;
        }

        return collect();
    }

}

if(! function_exists('hasSubAccounts')) {
    /**
     * @param $param Can be object or collection
     */
    function hasSubAccounts($subAccounts) {
        if ($subAccounts) {
            if (is_array($subAccounts)) {
                return count($subAccounts) > 1;
            } else {
                return $subAccounts->count() > 1;
            }
        }

        return false;
    }

}

if(! function_exists('formatTime')) {
    /**
     * @param $param Can be object or collection
     */
    function formatTime($param, $forgetZero = false) {
        $total = 0;
        if($param > 60){
        	$hours = floor($param / 60);
        	$minutes = $param % 60;

        	return $hours . ' hour' . ($hours > 1 ? 's ' : ' ') . ($minutes > 0 ? $minutes . ' minute' . ($minutes > 1 ? 's' : '') : '');
        }

        if($param == 0 && $forgetZero){
            return '--';
        }

        return $param . ' minute' . ($param > 1 ? 's' : '');
    }

}

if (! function_exists('isLengthAware')) {

	/**
	 * Will active string if current page route name is equal to parameters route name
	 * @param  String / Array  $routes Can accept single route name or multiple in form of array
	 * @return String
	 */
	function isLengthAware($entities) {
		return $entities instanceof \Illuminate\Contracts\Pagination\LengthAwarePaginator;
	}
}

if (! function_exists('sroute')) {

	/**
	 * Route generator for SubAccount context
	 *
	 * @param  string $name
	 * @return string
	 */
	function sroute($name, $params = []) {
		if (!is_array($params)) {
			$params = [$params];
		}
		$context = resolve(ATM\ContextInterface::class);
		if ($context && $context->getInstance()) {
			array_unshift($params, $context->getInstance()->uuid);
		}

		return route($name, $params);
	}

}

if (! function_exists('isActivePage')) {

	/**
	 * Will active string if current page route name is equal to parameters route name
	 * @param  String / Array  $routes Can accept single route name or multiple in form of array
	 * @return String
	 */
	function isActivePage($names){
		$curName = Illuminate\Support\Facades\Route::currentRouteName();
	    $state = '';

	    if(is_array($names)){
	        $state = in_array($curName, $names) ? 'active' : '';
	    } else {
	        $state = $curName == $names ? 'active' : '';
	    }

	    return $state;
	}
}

if (! function_exists('perPageDropdown')) {

    /**
     * Will active string if current page route name is equal to parameters route name
     * @param  String / Array  $routes Can accept single route name or multiple in form of array
     * @return String
     */
    function perPageDropdown($times = 0, $plus = 100) {
        $values = [
			/*5 => 'Display 5',
			10 => 'Display 10',*/
            20 => 'Display 20',
            50 => 'Display 50',
            100 => 'Display 100',
        ];
        if ($times > 0) {
            $initialVal = 100;
            while ($times >= 1) {
                $initialVal += $plus;
                $values[$initialVal] = 'Display '.$initialVal;
                $times--;
            }
        }
        $values['all'] = 'Display All';
        return $values;
    }
}

if(! function_exists('issSet()')) {

    /**
     * @param $param Can be object or collection
     */
    function issSet($variableStr, $default = null) {
        return isset(${$variableStr}) ? ${$variableStr} : $default;
    }

}

if(! function_exists('getAppTitle')) {

    /**
     * @param $param Can be object or collection
     */
    function getAppTitle() {
        return env('APP_NAME');
    }

}

if(! function_exists('successPercentage')) {

    /**
     * @param $param Can be object or collection
     */
    function successPercentage($exp1, $exp2) {

        if ($exp1 > 0) {
            $percentage = (($exp1 - $exp2) / $exp1) * 100;
            return number_format($percentage, 2) . ' %';
        } else {
            return number_format(0, 2) . ' %';
        }
    }

}

if(! function_exists('invalidNumberPrevixes')) {

	/**
	 * @param $param Can be object or collection
	 */
	function invalidNumberPrefixes() {
		return [
			// sun
			'63972', '63962', '63952', '63974', '63984', '63944', '63941', '63931', '63924', '63922', '63923', '63925', '63932', '63933', '63942', '63934', '63943',

			// smart 
			'63985', '63982', '63980', '63969', '63968', '63960', '63959', '63958', '63957', '63971', '63963', '63981', '63970', '63961', '63973', '63950', '63951', '63914', '63913', '63911', '63900', '63940', '63813', '63907', '63908', '63909', '63910', '63918', '63912', '63919', '63920', '63921', '63928', '63929', '63930', '63938', '63939', '63946', '63947', '63948', '63949', '63989', '63998', '63999',

			// dito 
			'63898', '63897', '63896', '63895', '63993', '63991', '63992', '63994'
		];
	}

}


if(! function_exists('isClientNailsGlow')) {
	function isClientNailsGlow() {
		return in_array(auth()->user()->client_id, [/* 1, */ 65]);
	}
}

if(! function_exists('isClientDreamNails')) {
	function isClientDreamNails() {
		return in_array(auth()->user()->client_id, [1, 93]);
	}
}

if(! function_exists('isClientBeechoo')) {
	function isClientBeechoo() {
		return in_array(auth()->user()->client_id, [/* 1, */ 70]);
	}
}

if(! function_exists('isClientMachoMucho')) {
	function isClientMachoMucho() {
		return in_array(auth()->user()->client_id, [/* 1, */ 96]);
	}
}

if(! function_exists('smartSunDitoClients')) {
	function smartSunDitoClients() {
		return in_array(auth()->user()->client_id, [70]);
	}
}

if(! function_exists('isClientRelux')) {
	function isClientRelux() {
		return in_array(auth()->user()->client_id, [1, 95]);
	}
}

if(! function_exists('isClientLuxeEscape')) {
	function isClientLuxeEscape() {
		return in_array(auth()->user()->client_id, [1, 66]);
	}
}

if(! function_exists('isClientRoyalAest')) {
	function isClientRoyalAest() {
		return in_array(auth()->user()->client_id, [1, 114]);
	}
}

if(! function_exists('isClientNailLuxe')) {
	function isClientNailLuxe() {
		return in_array(auth()->user()->client_id, [1, 117]);
	}
}