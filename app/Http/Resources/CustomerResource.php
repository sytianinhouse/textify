<?php

namespace App\Http\Resources;

use ATM\Repositories\Customers\Customer;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->b_month && $this->b_day && $this->b_year) {
            $birthday = date("M d, Y",mktime(0, 0, 0, $this->b_month, $this->b_day, $this->b_year));
        }
        return [
            'id' => $this->id,
            'full_name' => $this->first_name.' '.$this->last_name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'mobile_number' => $this->mobile,
            'gender' => $this->gender,
            'birthday' => $birthday ?? '',
            'b_day' => $this->b_day,
            'b_month' => $this->b_month,
            'b_year' => $this->b_year,
            'email' => $this->email,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
