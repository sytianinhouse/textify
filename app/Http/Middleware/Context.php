<?php

namespace ATM\Http\Middleware;

use ATM\ContextInterface;
use ATM\Repositories\Client\Client;
use ATM\Repositories\SubAccount\SubAccount;
use Closure;
use Illuminate\Support\Facades\Auth;

class Context
{
    public function __construct(ContextInterface $context)
    {
        $this->context = $context;
    }
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }

        $user = Auth::guard($guard)->user();
        if ($user->isRegUser()) {
            if ($request->route('SubAccount')) {
                $context = SubAccount::whereUuid($request->route('SubAccount'))->firstOrFail();
            } else {
                abort(403);
            }
        } else if ($user->isOwner()) {
            $context = $user->client;
        } else {
            $context = null;
        }

        if ($context && $user->cannot('access', $context)) {
            abort(403);
        }
        
        if ($context) {
            $this->context->set($context);
        }

        return $next($request);
    }

    /**
     * Handle an incoming request if login user is Admin
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handleAdmin($request, Closure $next) 
    {
        $client = $request->route('client');
        $client = ($client instanceof Client) ? $client : Client::findOrFail($client);

        if ($request->user()->cannot('access', $client)) {
            abort(403);
        }

        // dd($client);
        $this->context->set($client);

        return $next($request);
    }
}
