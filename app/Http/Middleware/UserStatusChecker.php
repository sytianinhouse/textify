<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserStatusChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $authGuard = Auth::guard($guard);
        if ( $authGuard ) {
            $status        = $authGuard->user()->isActive();
            if ( $status != true ) {
                Auth::logout();
                return redirect('/login')
                ->withErrors("Your account has been deactivated. Please contact the administrator!");
            }
        }
        return $next($request);
    }
}
