<?php

namespace App\Http\Middleware;

use ATM\ContextInterface;
use ATM\Repositories\Client\Client;
use Closure;
use Illuminate\Support\Facades\Auth;

class ClientContext
{
    public function __construct(ContextInterface $context)
    {
        $this->context = $context;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }

        $user = Auth::guard($guard)->user();

        // if from mobile app (e.g. timeclock app and zenapp)
        if ($request->has('api_token') && $guard == 'api') {
            $this->context->set($user->client);
            return $next($request);
        }

        if ($user->isAdmin()) {
            return $this->handleAdmin($request, $next);
        } else if ($user->isAccountUser()) {
            return redirect()->route('sub.index', session('sub_account'));
        }
        $client = $user->client;

        // Pag fix na permissions, ayusin to
        /*if ($user->cannot('access', $client)) {
            abort(403);
        }*/

        $this->context->set($client);

        return $next($request);
    }

    /**
     * Handle an incoming request if login user is Admin
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handleAdmin($request, Closure $next)
    {
        $client = $request->route('client');
        $client = ($client instanceof Client) ? $client : Client::findOrFail($client);

        if ($request->user()->cannot('access', $client)) {
            abort(403);
        }
        $this->context->set($client);

        return $next($request);
    }
}
