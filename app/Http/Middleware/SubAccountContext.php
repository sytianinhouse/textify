<?php

namespace App\Http\Middleware;

use ATM\ContextInterface;
use ATM\Repositories\SubAccount\SubAccount;
use Closure;
use Illuminate\Support\Facades\Auth;

class SubAccountContext
{
    public function __construct(ContextInterface $context)
    {
        $this->context = $context;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $authGuard = Auth::guard($guard);
        if ($authGuard->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }

        $user = $authGuard->user();
        if ($request->route('subAccount')) {
            $subAccount = SubAccount::whereUuid($request->route('subAccount'))->actives()->firstOrFail();
        } else if (session()->has('sub_account')) {
            $subAccount = SubAccount::whereUuid(session('sub_account'))->firstOrFail();
        } else if ($request->has('subaccount_id')) {
            $subAccount = SubAccount::findOrFail($request->subaccount_id);
        } else {
            abort(403);
        }

        // if ($user->cannot('access', $subAccount)) {
        //     abort(403);
        // }
        $this->context->set($subAccount);

        return $next($request);
    }
}
