<?php

namespace App\Listeners;

use ATM\Repositories\Settings\SiteSettingsRepository;
use ATM\Repositories\TaskJobs\AuditLog\JobsAuditLogRepository;
use ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;
use ATM\Repositories\Tasks\JobsNextMonth\JobNextMonth;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use App\Events\CronJobsNextMonth;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class CronJobsNextMonthListener // implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        JobsAuditLogRepository $jobsAuditLogsRepo,
        SiteSettingsRepository $settingsRepo
    )
    {
        $this->jobsAuditLogsRepo = $jobsAuditLogsRepo;
        $this->settingsRepo = $settingsRepo;
    }

    /**
     * Handle the event.
     *
     * @param  CronJobsNextMonth  $event
     * @return void
     */
    public function handle(CronJobsNextMonth $event)
    {
        $today = $event->today;

        $nextMonthStart = $today->copy()->startOfMonth();
        $nextMonthEnd = $today->copy()->endOfMonth();

        $settings = $this->settingsRepo->getSettings();

        $jobsNextMonth = JobNextMonth::with('task')
            ->where('when_to_send', '>=', $nextMonthStart->toDateTimeString())
            ->where('when_to_send', '<=', $nextMonthEnd->toDateTimeString())
            ->whereHas('task', function ($q) use ($nextMonthStart, $nextMonthEnd) {
                $q = $q->where('active', 1);
                return $q;
            })
			->whereHas('subAccount', function ($query) {
				return $query->where('active', 1);
			})
            // ->take(50)
            // ->take(500)
			->get();

		$startOfThisMonth = $today->copy()->startOfMonth();
		$firstDayofNextMonth = $startOfThisMonth->copy()->addMonth()->startOfMonth(); // get the first day of next month

        if ( $jobsNextMonth->count() > 0 ) {
            Log::info('Cron next month is running ...'.$jobsNextMonth->count());
           
            $getAllIds = [];

            // Insert sa job for next month
            $allArraysJob15thDay = [];
            $allArraysJob14thDay = [];
            $allArraysJobTomorrow = [];
            $allArraysJobToday = [];

            foreach ($jobsNextMonth as $key => $job) {
                
                $whenToSend =  Carbon::parse($job->when_to_send); // get the date set
                $matchDate = $whenToSend->copy()->startOfDay(); // reset time part, to prevent partial comparison

                $diff = $today->diff($matchDate);
                $diffDays = (integer) $diff->days;

                if ($diffDays) {
                    if ($diffDays == 1) {
                        $allArraysJobTomorrow[] = $this->buildArrays( $job );
                        array_push($getAllIds, $job->id);
                    } elseif($diffDays >= 2 && $diffDays <= 14) {
                        $allArraysJob14thDay[] = $this->buildArrays( $job );
                        array_push($getAllIds, $job->id);
                    } elseif($diffDays >= 15) {
                        if ($matchDate < $firstDayofNextMonth) {
                            $allArraysJob15thDay[] = $this->buildArrays( $job );
                            array_push($getAllIds, $job->id);
                        }
                    }
                } else {
                    if ($diffDays == 0) {
                        $allArraysJobToday[] = $this->buildArrays( $job );
                        array_push($getAllIds, $job->id);
                    } else {
                        Log::error("Date is from previous month.");
                    }   
                }
            }

            
            if ($settings->jobs_audit_log_disabled) {
                if (count($allArraysJob15thDay) > 0) {
                    Job15thDayToEndMonth::insert( $allArraysJob15thDay );
                }

                if (count($allArraysJob14thDay) > 0) {
                    JobAfterTomorrowTo14thDay::insert( $allArraysJob14thDay );
                }

                if (count($allArraysJobTomorrow) > 0) {
                    JobTomorrow::insert( $allArraysJobTomorrow );
                }

                if (count($allArraysJobToday) > 0) {
                    JobToday::insert( $allArraysJobToday );
                }
            } else {

                if (count($allArraysJob15thDay) > 0) {
                    foreach ($allArraysJob15thDay as $job15th) {
                        $oldJob15th = $job15th;

                        $newJob15th = new Job15thDayToEndMonth();

                        $newJob15th->customer_id = $oldJob15th->customer_id;
                        $newJob15th->customer_name = $oldJob15th->customer_name;
                        $newJob15th->customer_number = $oldJob15th->customer_number;
                        $newJob15th->gateway_key = $oldJob15th->gateway_key;
                        $newJob15th->campaign_id = $oldJob15th->campaign_id;
                        $newJob15th->credits_consumed = $oldJob15th->credits_consumed;
                        $newJob15th->when_to_send = $oldJob15th->when_to_send;
                        $newJob15th->message = $oldJob15th->message;
                        $newJob15th->task_id = $oldJob15th->task_id;
                        $newJob15th->subaccount_id = $oldJob15th->subaccount_id;
                        $newJob15th->client_id = $oldJob15th->client_id;
						$newJob15th->entry_id = $oldJob15th->entry_id;
                        $newJob15th->source = $oldJob15th->source;

                        $newJob15th->save();

                        $this->jobsAuditLogsRepo->addLog($oldJob15th, $newJob15th);
                    }
                }

                if (count($allArraysJob14thDay) > 0) {
                    foreach ($allArraysJob14thDay as $job14th) {
                        $oldJob14th = $job14th;

                        $newJob14th = new JobAfterTomorrowTo14thDay();

                        $newJob14th->customer_id = $oldJob14th->customer_id;
                        $newJob14th->customer_name = $oldJob14th->customer_name;
                        $newJob14th->customer_number = $oldJob14th->customer_number;
                        $newJob14th->gateway_key = $oldJob14th->gateway_key;
                        $newJob14th->campaign_id = $oldJob14th->campaign_id;
                        $newJob14th->credits_consumed = $oldJob14th->credits_consumed;
                        $newJob14th->when_to_send = $oldJob14th->when_to_send;
                        $newJob14th->message = $oldJob14th->message;
                        $newJob14th->task_id = $oldJob14th->task_id;
                        $newJob14th->subaccount_id = $oldJob14th->subaccount_id;
                        $newJob14th->client_id = $oldJob14th->client_id;
						$newJob14th->entry_id = $oldJob14th->entry_id;
                        $newJob14th->source = $oldJob14th->source;

                        $newJob14th->save();

                        $this->jobsAuditLogsRepo->addLog($oldJob14th, $newJob14th);
                    }
                }

                if (count($allArraysJobTomorrow) > 0) {
                    foreach ($allArraysJobTomorrow as $jobTom) {
                        $oldJobTom = $jobTom;

                        $newJobTom = new JobTomorrow();

                        $newJobTom->customer_id = $oldJobTom->customer_id;
                        $newJobTom->customer_name = $oldJobTom->customer_name;
                        $newJobTom->customer_number = $oldJobTom->customer_number;
                        $newJobTom->gateway_key = $oldJobTom->gateway_key;
                        $newJobTom->campaign_id = $oldJobTom->campaign_id;
                        $newJobTom->credits_consumed = $oldJobTom->credits_consumed;
                        $newJobTom->when_to_send = $oldJobTom->when_to_send;
                        $newJobTom->message = $oldJobTom->message;
                        $newJobTom->task_id = $oldJobTom->task_id;
                        $newJobTom->subaccount_id = $oldJobTom->subaccount_id;
                        $newJobTom->client_id = $oldJobTom->client_id;
						$newJobTom->entry_id = $oldJobTom->entry_id;
                        $newJobTom->source = $oldJobTom->source;

                        $newJobTom->save();

                        $this->jobsAuditLogsRepo->addLog($oldJobTom, $newJobTom);
                    }
                }

                if (count($allArraysJobToday) > 0) {
                    foreach ($allArraysJobToday as $jobToday) {
                        $oldJobToday = $jobToday;

                        $newJobToday = new JobToday();

                        $newJobToday->customer_id = $oldJobToday->customer_id;
                        $newJobToday->customer_name = $oldJobToday->customer_name;
                        $newJobToday->customer_number = $oldJobToday->customer_number;
                        $newJobToday->gateway_key = $oldJobToday->gateway_key;
                        $newJobToday->campaign_id = $oldJobToday->campaign_id;
                        $newJobToday->credits_consumed = $oldJobToday->credits_consumed;
                        $newJobToday->when_to_send = $oldJobToday->when_to_send;
                        $newJobToday->message = $oldJobToday->message;
                        $newJobToday->task_id = $oldJobToday->task_id;
                        $newJobToday->subaccount_id = $oldJobToday->subaccount_id;
                        $newJobToday->client_id = $oldJobToday->client_id;
						$newJobToday->entry_id = $oldJobToday->entry_id;
                        $newJobToday->source = $oldJobToday->source;

                        $newJobToday->save();

                        $this->jobsAuditLogsRepo->addLog($oldJobToday, $newJobToday);
                    }
                }

            }
            
            if (count($getAllIds)){
                $this->removeNextMonth($getAllIds);
            } else {
                Log::error("No ids found for next month.");
            } 
            

        } else {
            Log::info("No Jobs Found for next month.");
        }
    }

    private function buildArrays( $allArrays ) {

        $settings = $this->settingsRepo->getSettings();

        if ($settings->jobs_audit_log_disabled) {
            $drawArrays = [
                'customer_id' => $allArrays->customer_id,
                'customer_name' => $allArrays->customer_name,
                'customer_number' => $allArrays->customer_number,
                'gateway_key' => $allArrays->gateway_key,
                'campaign_id' => $allArrays->campaign_id,
                'credits_consumed' => $allArrays->credits_consumed,
                'when_to_send' => $allArrays->when_to_send,
                'message' => $allArrays->message,
                'task_id' => $allArrays->task_id,
                'subaccount_id' => $allArrays->subaccount_id,
                'client_id' => $allArrays->client_id,
                'source' => $allArrays->source,
				'entry_id' => $allArrays->entry_id,
                'created_at' => carbon::now(),
                'updated_at' => carbon::now(),
            ];
        } else {
            $drawArrays = $allArrays;
        }

        return $drawArrays;
    }

    private function removeNextMonth($getAllIds){
        return JobNextMonth::destroy($getAllIds); // remove data after inserting to other jobs table
    }
}
