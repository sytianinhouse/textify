<?php

namespace App\Listeners;

use App\Events\CronJobsHalfOfMonth;
use ATM\Repositories\Settings\SiteSettingsRepository;
use ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class CronJobsHalfOfMonthListener // implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
    	SiteSettingsRepository $siteSettingsRepo
	)
    {
        $this->siteSettingsRepo = $siteSettingsRepo;
    }

    /**
     * Handle the event.
     *
     * @param  CronJobsHalfOfMonth  $event
     * @return void
     */
    public function handle(CronJobsHalfOfMonth $event)
    {
        $today = $event->today;

		$nextMonth = $today->addMonth()->startOfMonth();
		$nextPlus14 = $nextMonth->copy()->addDays(13);

		$settings = $this->settingsRepo->getSettings();

		$jobsHalfNextMonth = JobNextMonth::with('task')
			->where('when_to_send', '>=', $nextMonth->copy()->toDateTimeString())
			->where('when_to_send', '<=', $nextPlus14->copy()->toDateTimeString())
			->whereHas('task', function ($q) {
				$q = $q->where('active', 1);
				return $q;
			}) // Active task lang yung makukuha
			->whereHas('subAccount', function ($query) {
				return $query->where('active', 1);
			}) 
			->take(50)
			->get();

		if ( $jobsHalfNextMonth->count() > 0 ) {
			Log::info('Cron half of next month is running ...');

			$getAllIds = [];
			$allArraysJob15thDay = [];
			foreach ($jobsHalfNextMonth as $key => $job) {

				$whenToSend =  Carbon::parse($job->when_to_send); // get the date set

				if ($whenToSend->gte($nextMonth) && $whenToSend->lte($nextPlus14)) {
					$allArraysJob15thDay[] = $this->buildArrays( $job );
					array_push($getAllIds, $job->id);
				}
			}

			if ($settings->jobs_audit_log_disabled) {
				if (count($allArraysJob15thDay) > 0) {
					Job15thDayToEndMonth::insert( $allArraysJob15thDay );
				}
			} else {
				if (count($allArraysJob15thDay) > 0) {
					foreach ($allArraysJob15thDay as $job15th) {
						$oldJob15th = $job15th;

						$newJob15th = new Job15thDayToEndMonth();

						$newJob15th->customer_id = $oldJob15th->customer_id;
						$newJob15th->customer_name = $oldJob15th->customer_name;
						$newJob15th->customer_number = $oldJob15th->customer_number;
						$newJob15th->gateway_key = $oldJob15th->gateway_key;
						$newJob15th->campaign_id = $oldJob15th->campaign_id;
						$newJob15th->credits_consumed = $oldJob15th->credits_consumed;
						$newJob15th->when_to_send = $oldJob15th->when_to_send;
						$newJob15th->message = $oldJob15th->message;
						$newJob15th->task_id = $oldJob15th->task_id;
						$newJob15th->subaccount_id = $oldJob15th->subaccount_id;
						$newJob15th->client_id = $oldJob15th->client_id;
						$newJob15th->entry_id = $oldJob15th->entry_id;
						$newJob15th->source = $oldJob15th->source;

						$newJob15th->save();

						$this->jobsAuditLogsRepo->addLog($oldJob15th, $newJob15th);
					}
				}
			}

			if (count($getAllIds)){
				$this->removeNextMonth($getAllIds);
			} else {
				Log::error("No ids found for next month.");
			}
		} else {
			Log::info("No Jobs Found for next month.");
		}
    }
}
