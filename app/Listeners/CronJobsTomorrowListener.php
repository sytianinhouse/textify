<?php

namespace App\Listeners;

use ATM\Repositories\Settings\SiteSettingsRepository;
use ATM\Repositories\TaskJobs\AuditLog\JobsAuditLogRepository;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use App\Events\CronJobsTomorrow;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class CronJobsTomorrowListener // implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        JobsAuditLogRepository $jobsAuditLogsRepo,
        SiteSettingsRepository $settingsRepo
    )
    {
        $this->jobsAuditLogsRepo = $jobsAuditLogsRepo;
        $this->settingsRepo = $settingsRepo;
    }

    /**
     * Handle the event.
     *
     * @param  CronJobsTomorrow  $event
     * @return void
     */
    public function handle(CronJobsTomorrow $event)
    {
        $settings = $this->settingsRepo->getSettings();

        $today = $event->today;

        $todayEndofDay = Carbon::today()->endOfDay()->toDateTimeString();

        $jobsTomorrow = JobTomorrow::with('task')->whereHas('task', function ($query) {
            return $query->where('active', 1);
        }) // Active task lang yung makukuha
		->whereHas('subAccount', function ($query) {
			return $query->where('active', 1);
		}) // Active sub account lang yung makukuha
		->whereDate('when_to_send', '<=', $todayEndofDay)
        // ->take(100)
        ->get();

        Log::info("Cron for tommorow is running: total of {$jobsTomorrow->count()} jobs!");
        if ( $jobsTomorrow->count() > 0 ):
            Log::info("Cron tomorrow is running with total jobs: {$jobsTomorrow->count()}");
            $allArraysJobTomorrow = [];
            $BuildAllIds = [];
            foreach ($jobsTomorrow as $jobs):
                $allArraysJobTomorrow[] = $this->buildArrays( $jobs );
                $BuildAllIds[] = explode(",", $jobs->id); // get all ids
            endforeach;

            if ( count($allArraysJobTomorrow) == 0 ) {
                Log::error("There was an error in saving data for jobs tomorrow.");
            } else {
                if ($settings->jobs_audit_log_disabled) { // Pasok dito if naka-disable yung settings ng audit logs
                    $result = JobToday::insert( $allArraysJobTomorrow ); // insert in today table
                } else {
                    foreach ($allArraysJobTomorrow as $jobTom) {

                        $jobToday = new JobToday();
                        $jobToday->customer_id = $jobTom->customer_id;
                        $jobToday->customer_name = $jobTom->customer_name;
                        $jobToday->customer_number = $jobTom->customer_number;
                        $jobToday->gateway_key = $jobTom->gateway_key;
                        $jobToday->campaign_id = $jobTom->campaign_id;
                        $jobToday->credits_consumed = $jobTom->credits_consumed;
                        $jobToday->when_to_send = $jobTom->when_to_send;
                        $jobToday->message = $jobTom->message;
                        $jobToday->task_id = $jobTom->task_id;
                        $jobToday->subaccount_id = $jobTom->subaccount_id;
                        $jobToday->client_id = $jobTom->client_id;
						$jobToday->entry_id = $jobTom->entry_id;
                        $jobToday->source = $jobTom->source;
                        $jobToday->save();

                        $this->jobsAuditLogsRepo->addLog($jobTom, $jobToday);
                    }
                }

                /*if( JobToday::count() > 0 ):
                    Log::info('Success to move in table today ...');
                endif;*/
                $this->removeTomorrowData($jobsTomorrow->pluck('id')->toArray());
            }
        else:
            Log::error("No Jobs Found for tomorrow.");
        endif;
    }

    private function buildArrays( $allArrays ) {

        $settings = $this->settingsRepo->getSettings();

        if ($settings->jobs_audit_log_disabled) {
            $drawArrays = [
                'customer_id' => $allArrays->customer_id,
                'customer_name' => $allArrays->customer_name,
                'customer_number' => $allArrays->customer_number,
                'gateway_key' => $allArrays->gateway_key,
                'campaign_id' => $allArrays->campaign_id,
                'credits_consumed' => $allArrays->credits_consumed,
                'when_to_send' => $allArrays->when_to_send,
                'message' => $allArrays->message,
                'task_id' => $allArrays->task_id,
                'subaccount_id' => $allArrays->subaccount_id,
                'client_id' => $allArrays->client_id,
				'entry_id' => $allArrays->entry_id,
                'source' => $allArrays->source,
                'created_at' => carbon::now(),
                'updated_at' => carbon::now(),
            ];
        } else {
            $drawArrays = $allArrays;
        }
        return $drawArrays;
    }

    private function removeTomorrowData($getAllIds){
        Log::info('Removing tomorrow data...');
        return JobTomorrow::destroy($getAllIds); // remove data after inserting in job today table
    }
}
