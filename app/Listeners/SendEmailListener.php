<?php

namespace App\Listeners;

use ATM\Repositories\Sms\SmsFacade;
use ATM\Validation\ValidationException;
use App\Events\SendEmail;
use App\Mail\CreditAlertLevel;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendEmailListener //implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  CronJobsToday  $event
     * @return void
     */
    public function handle(SendEmail $event)
    {
        $credit = $event->credit;
        $subAccount = $event->subAccount;

        $emailRaw = $subAccount->contact_email;

        $email = filter_var($emailRaw, FILTER_VALIDATE_EMAIL);
        
        if ($email) {

            Mail::to($email)->send(new CreditAlertLevel($credit, $subAccount, $email));
                
        }
    }
}
