<?php

namespace App\Listeners;

use ATM\Repositories\Sms\SmsFacade;
use ATM\Repositories\Tasks\JobsCompleted\JobsCompleted;
use ATM\Repositories\Tasks\JobsFailed\JobsFailed;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Validation\ValidationException;
use App\Events\CronJobsToday;

use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Arr;

use Illuminate\Support\Facades\Log;

class CronJobsTodayListener // implements ShouldQueue
{
	// use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SmsFacade $smsfacade)
    {
        //
        $this->sms = $smsfacade;
    }

    /**
     * Handle the event.
     *
     * @param  CronJobsToday  $event
     * @return void
     */
    public function handle(CronJobsToday $event)
    {
        $today = $event->today;

        $now = $today->copy();
        $jobsToday = JobToday::with('task', 'subAccount', 'subAccount.apis', 'subAccount.credit', 'gateway')->whereHas('task', function ($query) {
            return $query->where('active', 1); // Active task lang yung makukuha
        })
		->whereHas('subAccount', function ($query) {
			return $query->where('active', 1); // Active sub account lang yung makukuha
		})
		// ->where('when_to_send', '<=', $now)->whereNull('on_queue')->take(15)->get();
		->where('when_to_send', '<=', $now)->whereNull('on_queue')->take(30)->get();

        Log::info("Cron for today is running: total of {$jobsToday->count()} jobs!");
        if ( $jobsToday->count() > 0 ):
        	try {
				$this->sms->getAllJobsForToday( $jobsToday );
			} catch (ValidationException $e) {
        		$errors = $e->getErrors();
        		$message = $errors->getMessages();

        		Log::error($message);
			}
        else:
            JobToday::whereHas('subAccount', function ($query) {
                return $query->where('active', 0);
            })
            ->delete();
        endif;
    }
}
