<?php

namespace App\Listeners;

use ATM\Repositories\Settings\SiteSettingsRepository;
use ATM\Repositories\TaskJobs\AuditLog\JobsAuditLogRepository;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use App\Events\CronJobsTomorrowTo14thDay;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class CronJobsTomorrowTo14thDayListener // implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        JobsAuditLogRepository $jobsAuditLogsRepo,
        SiteSettingsRepository $settingsRepo
    )
    {
        $this->jobsAuditLogsRepo = $jobsAuditLogsRepo;
        $this->settingsRepo = $settingsRepo;
    }

    /**
     * Handle the event.
     *
     * @param  CronJobsTomorrowTo14thDay  $event
     * @return void
     */
    public function handle(CronJobsTomorrowTo14thDay $event)
    {
        $settings = $this->settingsRepo->getSettings();

        $today = $event->today;

        $tomorrowNextEndofDay = Carbon::tomorrow()->addDays(1)->endOfDay()->toDateTimeString(); // Carbon::today()->subMonth()->toDateTimeString();
        $jobsAfterTomorrowTo14thDay = JobAfterTomorrowTo14thDay::with('task')->whereHas('task', function ($q) {
            return $q->where('active', 1);
        }) // Active task lang yung makukuha
		->whereHas('subAccount', function ($query) {
			return $query->where('active', 1);
		}) // Active sub account lang yung makukuha
        ->where(function($q) use ($tomorrowNextEndofDay) {
            $q->whereDate('when_to_send', '<=', $tomorrowNextEndofDay)
              /* ->where('task_id', 959) */;
        })
        // ->take(50)->get();
        // ->take(100)
        ->get();
		// ->whereDate('when_to_send', '<=', $tomorrowNextEndofDay)->take(50)->get();
        
        if ( $jobsAfterTomorrowTo14thDay->count() > 0 ):
            Log::info('Cron after tomorrow to 14th day is running ...'.$jobsAfterTomorrowTo14thDay->count());

            $AllArraySendInTomorrow = [];
            $getIdsforTomorrow = [];

            $AllArraySendInToday = [];
            $getIdsforToday = [];

            foreach ($jobsAfterTomorrowTo14thDay as $job){
                $today = Carbon::now()->startOfDay(); // This object represents current date/time
                
                $whenToSend =  Carbon::parse($job->when_to_send); // get the date set
                $matchDate = $whenToSend->copy()->startOfDay(); // reset time part, to prevent partial comparison

                $diff = $today->diff($matchDate);
                $diffDays = (integer) $diff->days;

                if ($diffDays == 2) {
                    // send in tomorrow
                    $AllArraySendInTomorrow[] = $this->buildArrays($job);
                    $getIdsforTomorrow[] = [$job->id]; // get all ids
                } elseif ( $diffDays == 1 || $diffDays == 0 || $diffDays > 2 ) {
                    // send in today
                    $AllArraySendInToday[] = $this->buildArrays($job);
					$getIdsforToday[] =  [$job->id];
                } else {
                    // no jobs found
                    Log::error("no jobs found.");
                }
            }

            if (count($AllArraySendInTomorrow) == 0 ){
                Log::error("There was an error in saving data for jobs tomorrow.");
            } else {
                if ($settings->jobs_audit_log_disabled) { 
                    JobTomorrow::insert( $AllArraySendInTomorrow ); // insert in tomorrow table
                    Log::info('Success to move in table tomorrow ...');
                } else {
                    foreach ($AllArraySendInTomorrow as $job14th) {

                        $jobTom = new JobTomorrow();

                        $jobTom->customer_id = $job14th->customer_id;
                        $jobTom->customer_name = $job14th->customer_name;
                        $jobTom->customer_number = $job14th->customer_number;
                        $jobTom->gateway_key = $job14th->gateway_key;
                        $jobTom->campaign_id = $job14th->campaign_id;
                        $jobTom->credits_consumed = $job14th->credits_consumed;
                        $jobTom->when_to_send = $job14th->when_to_send;
                        $jobTom->message = $job14th->message;
                        $jobTom->task_id = $job14th->task_id;
                        $jobTom->subaccount_id = $job14th->subaccount_id;
                        $jobTom->client_id = $job14th->client_id;
						$jobTom->entry_id = $job14th->entry_id;
                        $jobTom->source = $job14th->source;

                        $jobTom->save();

                        $this->jobsAuditLogsRepo->addLog($job14th, $jobTom);
                    }
                }
            
                if (is_array($getIdsforTomorrow)){
                    $this->removeTomorrowTo14thDayData($getIdsforTomorrow);
                } else {
                    Log::error("No ids found in 14th day. sending data tomorrow table.");
                } 
            }

            if ( count($AllArraySendInToday) == 0 ){
                Log::error("There was an error in retrieving for jobs today.");
            } else {
                if ($settings->jobs_audit_log_disabled) { 
                    JobToday::insert($AllArraySendInToday); // insert in today table
                    Log::info('Success to move in table today ...');
                } else {
                    foreach ($AllArraySendInToday as $job14th) {

                        $jobToday = new JobToday();

                        $jobToday->customer_id = $job14th->customer_id;
                        $jobToday->customer_name = $job14th->customer_name;
                        $jobToday->customer_number = $job14th->customer_number;
                        $jobToday->gateway_key = $job14th->gateway_key;
                        $jobToday->campaign_id = $job14th->campaign_id;
                        $jobToday->credits_consumed = $job14th->credits_consumed;
                        $jobToday->when_to_send = $job14th->when_to_send;
                        $jobToday->message = $job14th->message;
                        $jobToday->task_id = $job14th->task_id;
                        $jobToday->subaccount_id = $job14th->subaccount_id;
                        $jobToday->client_id = $job14th->client_id;
						$jobToday->entry_id = $job14th->entry_id;
                        $jobToday->source = $job14th->source;

                        $jobToday->save();

                        $this->jobsAuditLogsRepo->addLog($job14th, $jobToday);
                    }
                }
                
                if (is_array($getIdsforToday)) {
                    $this->removeTomorrowTo14thDayData($getIdsforToday); 
                } else {
                    Log::error("No ids found in 14th day. sending data for today table.");
                }
            }
        else:
            Log::error("No Jobs Found for tomorrow to 14th day.");
        endif;
    }

    private function buildArrays( $allArrays ) {

        $settings = $this->settingsRepo->getSettings();

        if ($settings->jobs_audit_log_disabled) {
            $drawArrays = [
                'customer_id' => $allArrays->customer_id,
                'customer_name' => $allArrays->customer_name,
                'customer_number' => $allArrays->customer_number,
                'gateway_key' => $allArrays->gateway_key,
                'campaign_id' => $allArrays->campaign_id,
                'credits_consumed' => $allArrays->credits_consumed,
                'when_to_send' => $allArrays->when_to_send,
                'message' => $allArrays->message,
                'task_id' => $allArrays->task_id,
                'subaccount_id' => $allArrays->subaccount_id,
                'client_id' => $allArrays->client_id,
				'entry_id' => $allArrays->entry_id,
                'source' => $allArrays->source,
                'created_at' => carbon::now(),
                'updated_at' => carbon::now(),
            ];
        } else {
            $drawArrays = $allArrays;
        }
        return $drawArrays;
    }

    private function removeTomorrowTo14thDayData($getAllIds){
        return JobAfterTomorrowTo14thDay::destroy($getAllIds); // remove data after inserting in job today table
    }
}
