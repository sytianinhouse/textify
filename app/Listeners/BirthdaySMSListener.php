<?php

namespace App\Listeners;

use App\Events\BirthdaySMS;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Tasks\BirthdayTaskRepository;
use ATM\Repositories\Tasks\Task;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class BirthdaySMSListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
    	BirthdayTaskRepository $birthTaskRepo
	)
    {
    	$this->birthTaskRepo = $birthTaskRepo;
    }

    /**
     * Handle the event.
     *
     * @param  BirthdaySMS  $event
     * @return void
     */
    public function handle(BirthdaySMS $event)
    {
    	$date = $event->now;
		$birthdayTasks = Task::birthdays()->actives()->whereHas('subAccount', function($q) {
			$q->where('active', 1);
		})->whereHas('campaign')->get();

		$this->birthTaskRepo->generateJobs($birthdayTasks, $date);
		Log::info('Birthday task run with total count of task: ' . $birthdayTasks->count());
    }
}
