<?php

namespace App\Listeners;

use ATM\Repositories\Settings\SiteSettingsRepository;
use ATM\Repositories\TaskJobs\AuditLog\JobsAuditLogRepository;
use ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use App\Events\CronJobs15thDaytoEndMonth;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class CronJobs15thDaytoEndMonthListener // implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        JobsAuditLogRepository $jobsAuditLogsRepo,
        SiteSettingsRepository $settingsRepo
    )
    {
        $this->jobsAuditLogsRepo = $jobsAuditLogsRepo;
        $this->settingsRepo = $settingsRepo;
    }

    /**
     * Handle the event.
     *
     * @param  CronJobs15thDaytoEndMonth  $event
     * @return void
     */
    public function handle(CronJobs15thDaytoEndMonth $event)
    {
        $settings = $this->settingsRepo->getSettings();

        $today = $event->today;
        $plus13thDays = $today->addDays(13)->endOfDay();

        $jobs15thDayToEndMonth = Job15thDayToEndMonth::with('task')
			->whereHas('task', function ($q) {
				return $q->where('active', 1);
			}) // Active task lang yung makukuha
			->whereHas('subAccount', function ($query) {
				return $query->where('active', 1);
			}) // Active sub account lang yung makukuha
			->whereDate('when_to_send', '<=', $plus13thDays->toDateTimeString())
            // ->take(50)
            // ->take(100)
			->get();

        if ( $jobs15thDayToEndMonth->count() > 0 ) {
            Log::info('Cron after 15th day to end month is running ...'.$jobs15thDayToEndMonth->count());
            $allArraysjob15thDayToEndMonth = [];
            $getAllIds = [];

            foreach ($jobs15thDayToEndMonth as $jobs) {
                if (!empty($jobs)) {
                    $allArraysjob15thDayToEndMonth[] = $this->buildArrays( $jobs );
                    $getAllIds[] = [$jobs->id]; // get all ids
                }
            }

            if ( count($allArraysjob15thDayToEndMonth) == 0 ){
                Log::error("There was an error in saving data for jobs 15th day to end.");
            } else {

                if ($settings->jobs_audit_log_disabled) {
                    JobAfterTomorrowTo14thDay::insert( $allArraysjob15thDayToEndMonth ); // insert in 14th day table
                    Log::info('Success to move in table today ...');
                } else {
                    foreach ($allArraysjob15thDayToEndMonth as $job15th) {

                        $job14th = new JobAfterTomorrowTo14thDay();

                        $job14th->customer_id = $job15th->customer_id;
                        $job14th->customer_name = $job15th->customer_name;
                        $job14th->customer_number = $job15th->customer_number;
                        $job14th->gateway_key = $job15th->gateway_key;
                        $job14th->campaign_id = $job15th->campaign_id;
                        $job14th->credits_consumed = $job15th->credits_consumed;
                        $job14th->when_to_send = $job15th->when_to_send;
                        $job14th->message = $job15th->message;
                        $job14th->task_id = $job15th->task_id;
                        $job14th->subaccount_id = $job15th->subaccount_id;
                        $job14th->client_id = $job15th->client_id;
						$job14th->entry_id = $job15th->entry_id;
                        $job14th->source = $job15th->source;

                        $job14th->save();

                        $this->jobsAuditLogsRepo->addLog($job15th, $job14th);
                    }
                }

                if (is_array($getAllIds)) {
                    $this->remove15thDayToEndMonth($getAllIds); 
                } else {
                    Log::error("No ids found for 15th day.");
                } 
            }
        } else {
            Log::error("No Jobs Found for 15th day to end month.");
        }
    }

    private function buildArrays( $allArrays ) {

        $settings = $this->settingsRepo->getSettings();

        if ($settings->jobs_audit_log_disabled) {
            $drawArrays = [
                'customer_id' => $allArrays->customer_id,
                'customer_name' => $allArrays->customer_name,
                'customer_number' => $allArrays->customer_number,
                'gateway_key' => $allArrays->gateway_key,
                'campaign_id' => $allArrays->campaign_id,
                'credits_consumed' => $allArrays->credits_consumed,
                'when_to_send' => $allArrays->when_to_send,
                'message' => $allArrays->message,
                'task_id' => $allArrays->task_id,
                'subaccount_id' => $allArrays->subaccount_id,
				'client_id' => $allArrays->client_id,
				'entry_id' => $allArrays->entry_id,
                'source' => $allArrays->source,
                'created_at' => carbon::now(),
                'updated_at' => carbon::now(),
            ];
        } else {
            $drawArrays = $allArrays;
        }
        return $drawArrays;
    }

    private function remove15thDayToEndMonth($getAllIds){
        return Job15thDayToEndMonth::destroy($getAllIds); // remove data after inserting in job today table
    }
}
