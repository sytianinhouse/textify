<?php

namespace App;

use ATM\ContextInterface;
use ATM\ContextScope;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Permission\Permission;
use ATM\Repositories\Roles\Role;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Uploadable\Uploadable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Watson\Validating\ValidatingTrait;

class User extends Authenticatable implements ContextScope
{
    use SoftDeletes;
    use Notifiable, ValidatingTrait;

    const ADMIN_CLIENT_ID = 'textify';

    const ACCOUNT_ADMIN = 'admin';
    const ACCOUNT_CLIENT = 'client';
    const ACCOUNT_SUB = 'account_user';
    const ACCOUNT_SUPERADMIN = 'superadmin';

    const DEF_CLIENT_U = 'kenneth';
	const DEF_USER_U = 'user';

    public static $clientAccounts = [
        self::ACCOUNT_CLIENT => 'Client',
        self::ACCOUNT_SUB => 'Sub Account',
    ];

    public $allAccounts = [
        self::ACCOUNT_ADMIN => 'Admin',
        self::ACCOUNT_SUPERADMIN => 'Superadmin',
        self::ACCOUNT_CLIENT => 'Client',
        self::ACCOUNT_SUB => 'Account User',
    ];

    const IMG_ICON = 'icon';
    const IMG_THUMB = 'thumb';
    const IMG_BANNER = 'banner';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'mobile_no', 'telephone', 'client_id', 'account_type', 'username',
    ];

    protected $rules = [
        'email' => 'email|nullable',
        //'last_name' => 'required',
        'first_name' => 'required',
        //'username' => 'required|unique:users,username', @depreciated kasi sa repo ginagawa yung validation ng username
        'password' => 'required|min:6',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        // return $this->belongsToMany(Role::class);
        return $this->belongsToMany(Role::class, 'roles_user', 'user_id', 'role_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function subAccounts()
    {
        // return $this->belongsToMany(SubAccount::class);
        return $this->belongsToMany(SubAccount::class, 'sub_account_users', 'user_id', 'subaccount_id');
    }

    public function uploads()
    {
        return $this->morphMany(Uploadable::class, 'uploadable');
    }

    public function isSuperAdmin()
    {
        if($this->account_type == self::ACCOUNT_SUPERADMIN) return true;
        return false;
    }

    public function isAdmin()
    {
        if($this->isSuperadmin() || $this->account_type == self::ACCOUNT_ADMIN) return true;
        return false;
    }

    public function isOwner()
    {
        if($this->account_type == self::ACCOUNT_CLIENT) return true;
        return false;
    }

    public function isAccountUser()
    {
        return $this->account_type == self::ACCOUNT_SUB;
    }

    public function isActive()
    {
        return $this->active ? true : false;
    }

    public function isPermitted(Permission $permission)
    {
        if ($this->isSuperAdmin()) return true;

        foreach ($this->roles as $role) {
            if ( $role->permissions->contains($permission) ) {
                return true;
            }
        }
        return false;
    }
    // ----------------------------------
    // Get Attributes
    // ----------------------------------

    public function getNameAttribute()
    {
        $name = $this->attributes['first_name'].' '.$this->attributes['last_name'];
        return ucwords($name);
    }

    public function getAcctTypeAttribute()
    {
        $name = str_replace('_', ' ', $this->attributes['account_type']) ;
        return ucwords($name);
    }

    public function getImgIconAttribute() //img_icon
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_ICON ? true : false;
        })->first();

        return $uploads;
    }

    public function getImgIconPathAttribute() // imc_icon_path
    {
        return $this->img_icon ? $this->img_icon->path : Uploadable::IMG_PLACEHOLDER;
    }

    public function getImgThumbAttribute()
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_THUMB ? true : false;
        })->first();

        return $uploads;
    }

    public function getImgThumbPathAttribute()
    {
        return $this->img_thumb ? $this->img_thumb->path : Uploadable::IMG_PLACEHOLDER;
    }

    public function getProfileImgPathAttribute()
    {
        return $this->img_thumb ? $this->img_thumb->path : Uploadable::HUMAN_PLACEHOLDER;
    }

    public function getImgBannerAttribute()
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_BANNER ? true : false;
        })->first();

        return $uploads;
    }

    public function getImgBannerPathAttribute()
    {
        return $this->img_banner ? $this->img_banner->path : Uploadable::IMG_PLACEHOLDER;
    }

    // ----------------------------------
    // Scope Queries
    // ----------------------------------

    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->where($context->column(), $context->id());
        } else if ($context->getInstance() instanceof SubAccount) {
            return $query->whereHas('client.subAccounts', function($q) use ($context) {
                return $q->where('id', $context->id());
            });
        }

        return $query;
    }

}
