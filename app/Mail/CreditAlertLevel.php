<?php

namespace App\Mail;

use ATM\Repositories\Credits\Credit;
use ATM\Repositories\SubAccount\SubAccount;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CreditAlertLevel extends Mailable
{
    use Queueable, SerializesModels;

    public $credit;
    public $subAccount;
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Credit $credit, SubAccount $subAccount, $email)
    {
        $this->credit = $credit;
        $this->subAccount = $subAccount;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.credit.credit-alert');
    }
}
