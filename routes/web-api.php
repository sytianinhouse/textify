<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');

// All API's
// , 'middleware' => 'cors'
Route::group(['prefix' => 'api', 'namespace' => 'Api', 'as' => 'api.'], function() {

	// Routes for admin backend purposes
    Route::post('campaign/quick-create', 'Campaign\CampaignController@create')->name('create.quick-campaign');

	Route::get('campaign/get-campaign', 'Campaign\CampaignController@getCampaign')->name('campaign.get-campaign');

	// Api for One Time Send Task request
	Route::get('one-time-sms', 'Task\OneTimeSendController@createJob')->name('task.create.job');
	Route::get('one-time-sms-bulk', 'Task\OneTimeSendController@createJobBulk')->name('task.create.job-bulk');

	// Api for immediate tesk request
	Route::get('immediate-sms', 'Task\ImmediateTaskController@createJob')->name('it.create.job');
	Route::get('immediate-sms-batch', 'Task\ImmediateTaskController@createJobBatch')->name('it.create.job-batch');

	// Api to update Customers data from client system
	Route::group(['prefix' => 'customers', 'namespace' => 'Customer', 'as' => 'customer.'], function() {
		Route::get('update', 'CustomerController@updateRecord');
    });

	// Api to call recipient
	Route::group(['prefix' => 'contacts', 'namespace' => 'Customer', 'as' => 'customer.'], function() {
		Route::get('/', 'CustomerController@getRecipientList')->name('index');
		Route::post('/generate-contacts', 'CustomerController@generateContacts')->name('generateContacts');
		Route::post('/view-contacts', 'CustomerController@viewContacts')->name('viewContacts');
		Route::get('/get-page', 'CustomerController@newPage')->name('newPage');
		Route::get('/async-contact', 'CustomerController@loadMoreContact')->name('loadMoreContact');
    });

    // Api to call booking task
    Route::group(['prefix' => 'booking-task', 'namespace' => 'Task', 'as' => 'booking-task.'], function() {
        Route::get('/add-job', 'BookingTaskController@addJob')->name('add-job');
        Route::get('/delete-job', 'BookingTaskController@deleteJob')->name('delete-job'); // for cancelled bookings
    });
});
