<?php
	Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.', 'middleware' => 'auth.admin'], function() {

		Route::get('/', 'Dashboard\DashboardController@index')->name('index');

		Route::group(['prefix' => 'profile', 'as' => 'profile.'], function() {
			Route::get('edit/{user}', 'Profile\ProfileController@edit')->name('edit');
			Route::post('edit/{user}', 'Profile\ProfileController@update')->name('update');
		});

		Route::group(['prefix' => 'projects', 'as' => 'project.'], function() {
			Route::group(['namespace' => 'Project'], function() {
				Route::get('/', 'ClientProjects@index')->name('index');
				Route::get('create', 'ClientProjects@create')->name('create');
				Route::post('create', 'ClientProjects@store')->name('store');
				Route::get('edit/{project}', 'ClientProjects@edit')->name('edit');
				Route::put('edit/{project}', 'ClientProjects@update')->name('update');
				Route::get('delete/{project}', 'ClientProjects@destroy')->name('destroy');
			});
			Route::group(['prefix' => '{project}/clients', 'as' => 'client.'], function() {
				Route::get('/', 'Client\ClientsController@index')->name('index');
				Route::get('create', 'Client\ClientsController@create')->name('create');
				Route::post('create', 'Client\ClientsController@store')->name('store');
				Route::get('{client}/edit', 'Client\ClientsController@edit')->name('edit');
				Route::post('{client}/edit', 'Client\ClientsController@update')->name('update');
				Route::get('{client}/delete', 'Client\ClientsController@destroy')->name('destroy');

				Route::group(['prefix' => '{client}/senderids', 'as' => 'senderids.'], function() {
					Route::group(['namespace' => 'SenderIds'], function() {
						Route::get('/', 'SenderIdsController@index')->name('index');
						Route::get('create', 'SenderIdsController@create')->name('create');
						Route::post('create', 'SenderIdsController@store')->name('store');
						Route::get('edit/{senderids}', 'SenderIdsController@edit')->name('edit');
						Route::put('edit/{senderids}', 'SenderIdsController@update')->name('update');
						Route::get('delete/{senderids}', 'SenderIdsController@destroy')->name('destroy');
					});
				});

				Route::group(['prefix' => '{client}/users', 'as' => 'users.'], function() {
					Route::get('/', 'Client\ClientUsersController@index')->name('index');
					Route::get('create', 'Client\ClientUsersController@create')->name('create');
					Route::get('edit/{user}', 'Client\ClientUsersController@edit')->name('edit');
					Route::post('create', 'Client\ClientUsersController@store')->name('store');
					Route::put('edit/{user}', 'Client\ClientUsersController@update')->name('update');
					Route::get('delete/{user}', 'Client\ClientUsersController@destroy')->name('destroy');
				});

				Route::group(['prefix' => '{client}/sub-account/','as' => 'sub.'], function () {
					Route::get('/', 'SubAccount\SubAccountsController@index')->name('index');
					Route::get('create', 'SubAccount\SubAccountsController@create')->name('create');
					Route::post('create', 'SubAccount\SubAccountsController@store')->name('store');
					Route::get('{subaccount}/settings', 'SubAccount\SubAccountsController@settings')->name('settings');
					Route::put('{subaccount}/settings', 'SubAccount\SubAccountsController@updateSettings')->name('settings');
					Route::get('{subaccount}/edit','SubAccount\SubAccountsController@edit')->name('edit');
					Route::put('{subaccount}/edit', 'SubAccount\SubAccountsController@update')->name('update');
					Route::get('{subaccount}/delete', 'SubAccount\SubAccountsController@delete')->name('delete');

					Route::group(['prefix' => '{subaccount}/campaigns/','as' => 'camp.'], function () {
						Route::group(['namespace' => 'Campaign'], function () {
							Route::get('/', 'CampaignController@index')->name('index');
							Route::get('create', 'CampaignController@create')->name('create');
							Route::post('create', 'CampaignController@store')->name('store');
							Route::get('{campaign}/edit','CampaignController@edit')->name('edit');
							Route::put('{campaign}/edit', 'CampaignController@update')->name('update');
							Route::get('{campaign}/delete', 'CampaignController@delete')->name('delete');
						});
					});

					Route::group(['prefix' => '{subAccount}/tasks/', 'namespace' => 'Task', 'as' => 'task.'], function() {
						// Route::get('/', 'ClientTasks@index')->name('index');
						// Route::get('create', 'ClientTasks@create')->name('create');
						// Route::post('create', 'ClientTasks@store')->name('store');
						// Route::get('{taskid}/edit','ClientTasks@edit')->name('edit');
						// Route::put('{taskid}/edit', 'ClientTasks@update')->name('update');
						// Route::get('{taskid}/delete', 'ClientTasks@destroy')->name('delete');

						Route::group(['prefix' => 'ots', 'namespace' => 'OneTimeSend', 'as' => 'ots.'], function() {
							Route::get('/', 'OneTimeSendController@index')->name('index');
							Route::get('create', 'OneTimeSendController@create')->name('create');
							Route::post('create', 'OneTimeSendController@store')->name('store');
							Route::get('{task}/edit','OneTimeSendController@edit')->name('edit');
							Route::put('{task}/edit', 'OneTimeSendController@update')->name('update');
							Route::get('{task}/delete', 'OneTimeSendController@destroy')->name('delete');

							Route::post('create/ajaxGenerateRecepient', 'OneTimeSendController@ajaxGenerateRecepient'); // ajax request generate recepient //
							Route::post('create/ajaxviewrecipient','OneTimeSendController@ajaxviewRecipient'); // view all recipients
							Route::post('create/ajaxCreateCampaign','OneTimeSendController@ajaxCreateCampaign'); // create  quick campaign
							Route::post('{task}/edit/ajaxCreateCampaign','OneTimeSendController@ajaxCreateCampaign'); // create  quick campaign
						});

						Route::group(['prefix' => 'tbs', 'namespace' => 'TextBlastSend', 'as' => 'tbs.'], function() {
							Route::get('/', 'TextBlastSendController@index')->name('index');
							Route::get('create', 'TextBlastSendController@create')->name('create');
							Route::post('create', 'TextBlastSendController@store')->name('store');
							Route::get('{task}/edit','TextBlastSendController@edit')->name('edit');
							Route::get('{task}/view','TextBlastSendController@view')->name('view');
							Route::put('{task}/view','TextBlastSendController@updateFormView')->name('update');
							Route::put('{task}/edit', 'TextBlastSendController@update')->name('update');
							Route::get('{task}/delete', 'TextBlastSendController@destroy')->name('delete');

							Route::post('create/ajaxGenerateRecipient', 'TextBlastSendController@ajaxGenerateRecepient'); // ajax request generate recepient //
							Route::post('create/ajaxViewRecipient','TextBlastSendController@ajaxViewRecipient'); // view all recipients
							Route::post('create/ajaxCreateCampaign','TextBlastSendController@ajaxCreateCampaign'); // create  quick campaign
							Route::post('{task}/edit/ajaxCreateCampaign','TextBlastSendController@ajaxCreateCampaign'); // create  quick campaign
							Route::post('{task}/edit/ajaxGenerateRecipient', 'TextBlastSendController@ajaxGenerateRecepient'); // ajax request generate recepient //
							Route::post('{task}/edit/ajaxViewRecipient','TextBlastSendController@ajaxviewRecipient'); // view all recipients
						});

						Route::group(['prefix' => 'it', 'namespace' => 'ImmediateTask', 'as' => 'it.'], function() {
							Route::get('/', 'ImmediateTaskController@index')->name('index');
							Route::get('create', 'ImmediateTaskController@create')->name('create');
							Route::post('create', 'ImmediateTaskController@store')->name('store');
							Route::get('{task}/edit','ImmediateTaskController@edit')->name('edit');
							Route::put('{task}/edit', 'ImmediateTaskController@update')->name('update');
							Route::get('{task}/delete', 'ImmediateTaskController@destroy')->name('delete');
						});
					});
				});

				Route::group(['prefix' => '{client}/gateway/','as' => 'api.'], function () {
					Route::group(['namespace' => 'ApiKey'], function () {
						Route::get('/', 'ApiKeyController@index')->name('index');
						Route::get('create', 'ApiKeyController@create')->name('create');
						Route::post('create', 'ApiKeyController@store')->name('store');
						Route::get('{apikey}/edit','ApiKeyController@edit')->name('edit');
						Route::put('{apikey}/edit', 'ApiKeyController@update')->name('update');
						Route::get('{apikey}/delete', 'ApiKeyController@delete')->name('delete');
					});
				});

				Route::group(['prefix' => '{client}/roles/','as' => 'role.'], function () {
					Route::get('/', 'Role\ClientRolesController@index')->name('index');
					Route::get('create', 'Role\ClientRolesController@create')->name('create');
					Route::post('create', 'Role\ClientRolesController@store')->name('store');
					Route::get('{role}/edit','Role\ClientRolesController@edit')->name('edit');
					Route::put('{role}/edit', 'Role\ClientRolesController@update')->name('update');
					Route::get('{role}/delete', 'Role\ClientRolesController@destroy')->name('delete');
				});

				Route::group(['prefix' => '{client}/customer-groups/','as' => 'customer-group.'], function () {
					Route::get('/', 'CustomerGroup\ClientCustomerGroupController@index')->name('index');
					Route::get('create', 'CustomerGroup\ClientCustomerGroupController@create')->name('create');
					Route::post('create', 'CustomerGroup\ClientCustomerGroupController@store')->name('store');
					Route::get('{customergroup}/edit','CustomerGroup\ClientCustomerGroupController@edit')->name('edit');
					Route::put('{customergroup}/edit', 'CustomerGroup\ClientCustomerGroupController@update')->name('update');
					Route::get('{customergroup}/delete', 'CustomerGroup\ClientCustomerGroupController@destroy')->name('delete');
				});

				Route::group(['prefix' => '{client}/customers/','as' => 'customer.'], function () {
					Route::post('import', 'Customer\ClientCustomersController@import')->name('import');
					Route::get('/', 'Customer\ClientCustomersController@index')->name('index');
					Route::get('create', 'Customer\ClientCustomersController@create')->name('create');
					Route::post('create', 'Customer\ClientCustomersController@store')->name('store');
					Route::get('{customer}/edit','Customer\ClientCustomersController@edit')->name('edit');
					Route::put('{customer}/edit', 'Customer\ClientCustomersController@update')->name('update');
					Route::get('{customer}/delete', 'Customer\ClientCustomersController@destroy')->name('delete');
					Route::post('create/ajax_subaccount', 'Customer\ClientCustomersController@getcustomergroup'); // ajax request in add new customer //
					Route::post('{customer}/edit/ajax_subaccount', 'Customer\ClientCustomersController@editcustomergroup'); // ajax request in edit customer //
				});

			});
		});

		Route::group(['prefix' => 'loads', 'as' => 'load.', 'namespace' => 'Load'], function() {
			Route::get('/', 'LoadsController@index')->name('index');
			Route::get('create', 'LoadsController@create')->name('create');
			Route::post('create', 'LoadsController@store')->name('store');
			Route::get('{load}/delete', 'LoadsController@destroy')->name('destroy');
			Route::post('create/clients', 'LoadsController@getClients')->name('get.clients');
			Route::post('create/sub-accounts', 'LoadsController@getSubAccounts')->name('get.sub.accounts');

			Route::group(['prefix' => 'auto-loads', 'as' => 'auto-loads.'], function() {
				Route::get('/', 'LoadsController@autoLoadIndex')->name('index');
				Route::get('create', 'LoadsController@createAutoload')->name('create');
				Route::post('create', 'LoadsController@storeAutoload')->name('store');
				Route::get('{load}/edit', 'LoadsController@editAutoload')->name('edit');
				Route::post('{load}/edit', 'LoadsController@updateAutoload')->name('update');
				Route::get('{load}/delete', 'LoadsController@destroyAutoload')->name('destroy');
			});
		});

		Route::group(['prefix' => 'credits', 'as' => 'credit.', 'namespace' => 'Credit'], function() {
			Route::get('/', 'CreditsController@index')->name('index');
			Route::get('create', 'CreditsController@create')->name('create');
			Route::post('create', 'CreditsController@store')->name('store');
			Route::get('logs', 'CreditLogsController@index')->name('logs');
			Route::get('{credit}/edit', 'CreditsController@edit')->name('edit');
			Route::put('{credit}/edit', 'CreditsController@update')->name('update');
			Route::get('{credit}/delete', 'CreditsController@destroy')->name('destroy');
		});

		Route::group(['prefix' => 'monitoring', 'as' => 'monitoring.', 'namespace' => 'SubAccount'], function() {
			Route::get('/', 'MonitoringController@monitoring')->name('index');
		});

		// System General Settings
		Route::group(['prefix' => 'general', 'as' => 'general.', 'namespace' => 'General'], function() {

			Route::get('settings', 'SettingsController@settings')->name('settings');
			Route::post('settings', 'SettingsController@settingsSave')->name('settings.save');

			Route::get('sms-settings', 'SettingsController@smsSettings')->name('sms-settings');
			Route::post('sms-settings', 'SettingsController@smsSettingsSave')->name('sms-settings.save');

		});

		// For integration purposes only
		Route::get('send-sms', 'Gateway\GlobeLabsController@sms');

		Route::get('receive-authorize', 'Gateway\GlobeLabsController@receiveAuthorize');
		Route::get('get-clients', 'HomeController@getClientsAjax')->name('get.clients');
		Route::get('get-subs', 'HomeController@getSubsAjax')->name('get.subs');

	});

	// Report Template
	Route::get('report-template', 'Controller@reportTemplate')->name('report-template');

?>
