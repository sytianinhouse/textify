<?php
	Route::group(['prefix' => '{subAccount}/sub', 'namespace' => 'SubAccount', 'as' => 'sub.', 'middleware' => ['context.sub.account','auth.check.status'] ], function() {

		Route::get('/', 'HomeController@index')->name('index');
		Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');

		Route::group(['prefix' => 'profile', 'as' => 'profile.'], function() {
			Route::get('edit/', 'Profile\ProfileController@edit')->name('edit');
			Route::post('edit/', 'Profile\ProfileController@update')->name('update');
		});

		Route::group(['prefix' => 'api/','as' => 'api.','namespace' => 'ApiKey'], function () {
			Route::get('/', 'ApiKeyController@index')->name('index');
			Route::get('create', 'ApiKeyController@create')->name('create');
			Route::post('create', 'ApiKeyController@store')->name('store');
			Route::get('{apikey}/edit','ApiKeyController@edit')->name('edit');
			Route::put('{apikey}/edit', 'ApiKeyController@update')->name('update');
			Route::get('{apikey}/delete', 'ApiKeyController@delete')->name('delete');
		});

		Route::group(['prefix' => '/campaigns','as' => 'camp.','namespace' => 'Campaign'], function () {
			Route::get('/', 'CampaignController@index')->name('index');
			Route::get('create', 'CampaignController@create')->name('create');
			Route::post('create', 'CampaignController@store')->name('store');
			Route::get('{campaign}/edit','CampaignController@edit')->name('edit');
			Route::put('{campaign}/edit', 'CampaignController@update')->name('update');
            Route::get('{campaign}/delete', 'CampaignController@delete')->name('delete');
            Route::get('{campaign}/sms', 'CampaignController@sentSMS')->name('sentSMS');
		});

		Route::group(['prefix' => 'users/', 'as' => 'user.', 'namespace' => 'User'], function () {
			Route::get('/', 'UsersController@index')->name('index');
			Route::get('create', 'UsersController@create')->name('create');
			Route::post('create', 'UsersController@store')->name('store');
			Route::get('edit/{user}', 'UsersController@edit')->name('edit');
			Route::put('edit/{user}', 'UsersController@update')->name('update');
			Route::get('delete/{user}', 'UsersController@destroy')->name('destroy');
		});

		Route::group(['prefix' => 'roles/', 'as' => 'role.', 'namespace' => 'Role'], function () {
			Route::get('/', 'RolesController@index')->name('index');
			Route::get('create', 'RolesController@create')->name('create');
			Route::post('create', 'RolesController@store')->name('store');
			Route::get('{role}/edit','RolesController@edit')->name('edit');
			Route::put('{role}/edit', 'RolesController@update')->name('update');
			Route::get('{role}/delete', 'RolesController@destroy')->name('delete');
		});

		Route::group(['prefix' => 'tasks/', 'namespace' => 'Task', 'as' => 'task.'], function() {
			Route::group(['prefix' => 'ots', 'namespace' => 'OneTimeSend', 'as' => 'ots.'], function() {
				Route::get('/', 'OneTimeSendController@index')->name('index');
				Route::get('create', 'OneTimeSendController@create')->name('create');
				Route::post('create', 'OneTimeSendController@store')->name('store');
				Route::get('{task}/edit','OneTimeSendController@edit')->name('edit');
				Route::put('{task}/edit', 'OneTimeSendController@update')->name('update');
				Route::get('{task}/delete', 'OneTimeSendController@destroy')->name('delete');

				Route::post('create/ajaxCreateCampaign','OneTimeSendController@ajaxCreateCampaign');
				Route::post('{task}/edit/ajaxCreateCampaign','OneTimeSendController@ajaxCreateCampaign');
				Route::post('create/ajaxGenerateRecepient', 'OneTimeSendController@ajaxGenerateRecepient'); // ajax request generate recepient //
				Route::post('create/ajaxviewrecipient','OneTimeSendController@ajaxviewRecipient'); // view all recipients
			});

			Route::group(['prefix' => 'tbs', 'namespace' => 'TextBlastSend', 'as' => 'tbs.'], function() {
				Route::get('/', 'TextBlastSendController@index')->name('index');
				Route::get('create', 'TextBlastSendController@create')->name('create');
				Route::post('create', 'TextBlastSendController@store')->name('store');
				Route::get('{task}/edit','TextBlastSendController@edit')->name('edit');
				Route::get('{task}/view','TextBlastSendController@view')->name('view');
				Route::put('{task}/view','TextBlastSendController@updateStatus')->name('view.update-status');
				Route::put('{task}/edit', 'TextBlastSendController@update')->name('update');
				Route::get('{task}/delete', 'TextBlastSendController@destroy')->name('delete');

				Route::post('create/ajaxGenerateRecipient', 'TextBlastSendController@ajaxGenerateRecepient'); // ajax request generate recepient //
				Route::post('create/ajaxViewRecipient','TextBlastSendController@ajaxViewRecipient'); // view all recipients
				Route::post('create/ajaxCreateCampaign','TextBlastSendController@ajaxCreateCampaign');
				Route::post('{task}/edit/ajaxCreateCampaign','TextBlastSendController@ajaxCreateCampaign');
				Route::post('{task}/edit/ajaxGenerateRecipient', 'TextBlastSendController@ajaxGenerateRecepient'); // ajax request generate recepient //
				Route::post('{task}/edit/ajaxViewRecipient','TextBlastSendController@ajaxviewRecipient'); // view all recipients
			});

			Route::group(['prefix' => 'it', 'namespace' => 'ImmediateTask', 'as' => 'it.'], function() {
				Route::get('/', 'ImmediateTaskController@index')->name('index');
				Route::get('create', 'ImmediateTaskController@create')->name('create');
				Route::post('create', 'ImmediateTaskController@store')->name('store');
				Route::get('{task}/edit','ImmediateTaskController@edit')->name('edit');
				Route::get('{task}/view','ImmediateTaskController@view')->name('view');
				Route::put('{task}/edit', 'ImmediateTaskController@update')->name('update');
				Route::get('{task}/delete', 'ImmediateTaskController@destroy')->name('delete');
            });

			Route::group(['prefix' => 'birthday-task', 'namespace' => 'BirthdayTask', 'as' => 'birthday-task.'], function() {
				Route::get('create', 'BirthdayTaskController@create')->name('create');
				Route::post('create', 'BirthdayTaskController@store')->name('store');
			});

            Route::group(['prefix' => 'booking-task', 'namespace' => 'BookingTask', 'as' => 'booking-task.'], function() {
                Route::get('/', 'BookingTaskController@index')->name('index');
                Route::get('create', 'BookingTaskController@create')->name('create');
                Route::get('{task}/edit', 'BookingTaskController@edit')->name('edit');
                Route::get('{task}/delete', 'BookingTaskController@destroy')->name('delete');
                // Api to call booking task
                // Route::get('/add-job', 'BookingTaskController@addJob')->name('add-job');
                Route::post('/store', 'BookingTaskController@store')->name('store');
                Route::post('/update', 'BookingTaskController@update')->name('update');
                Route::get('/get-page', 'BookingTaskController@getPage')->name('get-page');
            });

		});

		Route::group(['prefix' => 'customer-groups/','as' => 'customer-group.','namespace' => 'CustomerGroup'], function () {
			Route::get('/', 'CustomerGroupController@index')->name('index');
			Route::get('create', 'CustomerGroupController@create')->name('create');
			Route::post('create', 'CustomerGroupController@store')->name('store');
			Route::get('{customergroup}/edit','CustomerGroupController@edit')->name('edit');
			Route::put('{customergroup}/edit', 'CustomerGroupController@update')->name('update');
			Route::get('{customergroup}/delete', 'CustomerGroupController@destroy')->name('delete');
		});

		Route::group(['prefix' => 'customers/','as' => 'customer.','namespace' => 'Customer'], function () {
			Route::get('/', 'CustomerController@index')->name('index');
			Route::get('create', 'CustomerController@create')->name('create');
			Route::post('create', 'CustomerController@store')->name('store');
			Route::get('{customer}/edit','CustomerController@edit')->name('edit');
			Route::put('{customer}/edit', 'CustomerController@update')->name('update');
			Route::get('{customer}/delete', 'CustomerController@destroy')->name('delete');
			Route::post('create/ajax_subaccount', 'CustomerController@getcustomergroup'); // ajax request in add new customer //
			Route::post('{customer}/edit/ajax_subaccount', 'CustomerController@editcustomergroup'); // ajax request in edit customer //
		});

		Route::group(['prefix' => 'reports/', 'as' => 'report.', 'namespace' => 'Reports'], function () {
			Route::group(['namespace' => 'Credit', 'as' => 'credit.'], function() {
				Route::get('credit-balance', 'CreditsController@creditBalance')->name('balance');
			});

			Route::group(['namespace' => 'Sms', 'as' => 'job.'], function() {
				Route::get('sms-per-day', 'JobController@smsPerDay')->name('sms.day');
				Route::get('sms-per-month', 'JobController@smsPerMonth')->name('sms.month');
				Route::get('sms-per-campaign', 'CampaignController@smsPerCampaign')->name('sms.campaign');
				Route::get('sms-sent', 'JobController@smsSent')->name('sms.sent');
				Route::get('sms-per-sub-account', 'JobController@smsPerSubAccount')->name('sms.per-sub-account');
				Route::get('sms-pending', 'JobController@smsPending')->name('sms.pending');
				Route::get('sms-failed', 'JobController@smsFailed')->name('sms.failed');
			});

			Route::group(['namespace' => 'Contact', 'as' => 'contact.'], function() {
				Route::get('statistics', 'ContactController@statistics')->name('statistics');
			});
        });

        Route::group(['prefix' => 'jobs', 'namespace' => 'Jobs', 'as' => 'jobs.'], function() {

            Route::get('view/{id}/{instance}', 'JobsController@view')->name('view');
            Route::post('view/{id}/{instance}', 'JobsController@update')->name('view');
            Route::get('delete/{id}/{instance}', 'JobsController@delete')->name('delete');

            Route::group(['prefix' => 'JobsFailed', 'namespace' => 'JobsFailed', 'as' => 'jf.'], function() {
                Route::get('{jobsFailed}/edit', 'JobsFailedController@edit')->name('edit');
                Route::put('{jobsFailed}/edit', 'JobsFailedController@resend')->name('resend');
                Route::get('{jobsFailed}/resend', 'JobsFailedController@resend')->name('resend');
                Route::get('{jobsFailed}/remove', 'JobsFailedController@remove')->name('remove');
            });
        });

	});
?>
