<?php

use ATM\Repositories\Sms\SmsFacade;
use Illuminate\Support\Facades\DB;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use ATM\Repositories\Tasks\JobsNextMonth\JobNextMonth;
use ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('manual-run-sched/638gfhdfg-s0f7sd85f0f9hadf4as5fs7g32364', function () {
   event(new \App\Events\CronJobsTomorrow(today()));
   echo 'done';
});

Route::get('get-php-info', function() {
	phpinfo();
	die;
});

Route::get('beechoo-smart-globe-customers', function() {
	$customers = ATM\Repositories\Customers\Customer::where('client_id', 70)->get();
	$invalidPrefixes = invalidNumberPrefixes();

	$filteredSmart = $customers->filter(function($cust) {
		$sanitized = str_replace('+', '', $cust->mobile);
		$numPrefix = substr($sanitized, 0, 5);

		return in_array($numPrefix, ['63985', '63982', '63980', '63969', '63968', '63960', '63959', '63958', '63957', '63971', '63963', '63981', '63970', '63961', '63973', '63950', '63951', '63914', '63913', '63911', '63900', '63940', '63813', '63907', '63908', '63909', '63910', '63918', '63912', '63919', '63920', '63921', '63928', '63929', '63930', '63938', '63939', '63946', '63947', '63948', '63949', '63989', '63998', '63999']);
	})->values();

	$filteredSun = $customers->filter(function($cust) use ($invalidPrefixes) {
		$sanitized = str_replace('+', '', $cust->mobile);
		$numPrefix = substr($sanitized, 0, 5);

		return in_array($numPrefix, ['63972', '63962', '63952', '63974', '63984', '63944', '63941', '63931', '63924', '63922', '63923', '63925', '63932', '63933', '63942', '63934', '63943']);
	})->values();

	$filteredDito = $customers->filter(function($cust) use ($invalidPrefixes) {
		$sanitized = str_replace('+', '', $cust->mobile);
		$numPrefix = substr($sanitized, 0, 5);

		return in_array($numPrefix, ['63898', '63897', '63896', '63895', '63993', '63991', '63992', '63994']);
	})->values();

	$filteredGlobe = $customers->filter(function($cust) use ($invalidPrefixes) {
		$sanitized = str_replace('+', '', $cust->mobile);
		$numPrefix = substr($sanitized, 0, 5);

		return !in_array($numPrefix, $invalidPrefixes);
	})->values();

	dd('SMART', $filteredSmart, 'SUN', $filteredSun, 'DITO', $filteredDito, 'GLOBE', $filteredGlobe);
});

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/', 'Auth\LoginController@manageRedirect');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'asset-page'], function() {
	Route::get('/assets', function () {
		return view('asset_pages.asset_page');
	})->name('assetPage');
	Route::get('/two-column', function () {
		return view('asset_pages.two_columns');
	})->name('twoColumnPage');
	Route::get('/index-page', function () {
		return view('asset_pages.index_page');
	})->name('indexPage');
});

// Route::get('api/immediate-sm', 'Api\Task\ImmediateTaskController@createJob')->name('it.create.job');

Route::get('{subaccountUuid}/{beforeUuid}/change-context', 'SubAccount\ContextController@changeContext')->name('sub.context.change');

Route::get('move-smart-numbers-to-failed', function() {

	try {
		DB::beginTransaction();

		$smartNumberClients = [70]; // beechoo
		$invalidPrefixes = invalidNumberPrefixes();
	
		$jobs15thDayToEndMonth = Job15thDayToEndMonth::whereIn('client_id', $smartNumberClients)->get();
		$jobsAfterTomorrowTo14thDay = JobAfterTomorrowTo14thDay::whereIn('client_id', $smartNumberClients)->get();
		$jobsNextMonth = JobNextMonth::whereIn('client_id', $smartNumberClients)->get();
		$jobsTodaySend = JobToday::whereIn('client_id', $smartNumberClients)->get();
		$jobsTomorrow = JobTomorrow::whereIn('client_id', $smartNumberClients)->get();
	
		$smsFacade = resolve(\ATM\Repositories\Sms\SmsFacade::class);
	
		if ($jobs15thDayToEndMonth->count() > 0) {
			foreach($jobs15thDayToEndMonth as $job15thDayToEndMonth) {
				if ($job15thDayToEndMonth && $job15thDayToEndMonth->customer_number) {
					$jobNumPrefix = substr($job15thDayToEndMonth->customer_number, 0, 5);
		
					if (in_array($jobNumPrefix, $invalidPrefixes)) {
						$smsFacade->forceFailJobs($job15thDayToEndMonth, true, 'Smart/Sun/DITO Number', explode(",", $job15thDayToEndMonth->id));
					}
				}
			}
		}
	
		if ($jobsAfterTomorrowTo14thDay->count() > 0) {
			foreach($jobsAfterTomorrowTo14thDay as $jobAfterTomorrowTo14thDay) {
				if ($jobAfterTomorrowTo14thDay && $jobAfterTomorrowTo14thDay->customer_number) {
					$jobNumPrefix = substr($jobAfterTomorrowTo14thDay->customer_number, 0, 5);
		
					if (in_array($jobNumPrefix, $invalidPrefixes)) {
						$smsFacade->forceFailJobs($jobAfterTomorrowTo14thDay, true, 'Smart/Sun/DITO Number', explode(",", $jobAfterTomorrowTo14thDay->id));
					}
				}
			}
		}
	
		if ($jobsNextMonth->count() > 0) {
			foreach($jobsNextMonth as $jobNextMonth) {
				if ($jobNextMonth && $jobNextMonth->customer_number) {
					$jobNumPrefix = substr($jobNextMonth->customer_number, 0, 5);
					
					if (in_array($jobNumPrefix, $invalidPrefixes)) {
						$smsFacade->forceFailJobs($jobNextMonth, true, 'Smart/Sun/DITO Number', explode(",", $jobNextMonth->id));
					}
				}
			}
		}
	
		if ($jobsTodaySend->count() > 0) {
			foreach($jobsTodaySend as $jobToday) {
				if ($jobToday && $jobToday->customer_number) {
					$jobNumPrefix = substr($jobToday->customer_number, 0, 5);
		
					if (in_array($jobNumPrefix, $invalidPrefixes)) {
						$smsFacade->forceFailJobs($jobToday, true, 'Smart/Sun/DITO Number', explode(",", $jobToday->id));
					}
				}
			}
		}
	
		if ($jobsTomorrow->count() > 0) {
			foreach($jobsTomorrow as $jobTomorrow) {
				if ($jobTomorrow && $jobTomorrow->customer_number) {
					$jobNumPrefix = substr($jobTomorrow->customer_number, 0, 5);
		
					if (in_array($jobNumPrefix, $invalidPrefixes)) {
						$smsFacade->forceFailJobs($jobTomorrow, true, 'Smart/Sun/DITO Number', explode(",", $jobTomorrow->id));
					}
				}
			}
		}
		
		DB::commit();
		return 'sms failed successully !';
	} catch (\Exception $e) {
		DB::rollBack();
		
		dd($e->getErrors());
	}

});
/**
 * -----------------------------------------------------------
 * Sub Account Routes
 * -----------------------------------------------------------
 */

require base_path('routes/subaccount.php');

/**
 * -----------------------------------------------------------
 * Client Routes
 * -----------------------------------------------------------
 */

require base_path('routes/client.php');

/**
 * -----------------------------------------------------------
 * Admin Routes
 * -----------------------------------------------------------
 */

require base_path('routes/admin.php');

/**
 * -----------------------------------------------------------
 * WEB API's
 * -----------------------------------------------------------
 */

require base_path('routes/web-api.php');

