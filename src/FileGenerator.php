<?php

namespace ATM;

use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests;

class FileGenerator {

    /**
     * @param collection/array $collection
     * @param array $headings
     * @param array $data
     * @param string $fileName
     * @param string $template
     */
    public static function makeExcelData($collection, $headings, $data)
    {
        $rows = [];

        foreach ($collection as $object){
            $cells = [];

            foreach($headings as $key => $heading){
                $cell = $object->$data[$key];
                $cells[$heading] = $cell != null ? $cell : '--';
            }

            array_push($rows, $cells);
        }

        return $rows;
    }

    /**
     * @param $sheetName   Maximum 31 characters allowed in sheet title 
     */
    public static function makeExcel($data, $filename, $sheetName, $bladeview = null, $viewData = [], $store = false, $moneyColumn = [], $multipleSheets = false, $withImage = [])
    {

        for ($i = 'A', $j = 1; $j <= 26; $i++, $j++) {
            $letters[$i] = "#,##0.00_-";
        }

        if (!$multipleSheets) {
            $sheetName = substr($sheetName, 0, 31);
        }
        $excel = Excel::create($filename, function($excel) use ($data, $sheetName, $bladeview, $viewData, $moneyColumn, $letters, $multipleSheets, $withImage) {

            if ($multipleSheets) {
                foreach ($bladeview as $key => $view) {
                    $name = isset($sheetName[$key]) ? $sheetName[$key] : [];
                    $monCol = isset($moneyColumn[$key]) ? $moneyColumn[$key] : [];
                    $excel = self::makeSheet($excel, $name, $data, $view, $viewData, $monCol, $letters, $withImage);
                }
            } else {
                $excel = self::makeSheet($excel, $sheetName, $data, $bladeview, $viewData, $moneyColumn, $letters, $withImage);
            }
        });
        
        if (self::isCypressRequest()) {
            return response()->json(['success' => true]);
        }

        if ($store) {

            return $excel->store('xls', public_path('exports/'), true);
        } else {
            return $excel->export('xls');
        }

        // if ($store) {
        //     if (!self::isCypressRequest()) {
        //         return $excel->store('xls', public_path('exports/'), true);
        //     } else {
        //         return response()->json(['success' => true]);
        //     }
        // } else {
        //     if (!self::isCypressRequest()) {
        //         return $excel->export('xls');
        //     } else {
        //         return response()->json(['success' => true]);
        //     }
        // }
    }

    public static function makeSheet($excel, $sheetName, $data, $view, $viewData, $monCol, $letters, $withImage)
    {
        $excel->sheet($sheetName, function($sheet) use ($data, $view, $viewData, $monCol, $letters, $withImage) {

            if( $view ) {
                $sheet->loadView($view, $viewData);

                // Money Column (Array) = Gawing Value yung Equivalent Column sa Excel (Ex. A, B, C)
                if (count($monCol) > 0) {
                    $newMoneyColumn = array_intersect_key($letters, array_flip($monCol));
                    $sheet->setColumnFormat($newMoneyColumn);
                }

            } else {
                $sheet->cells('A1:AF1', function($cells){
                    $cells->setFont(array(
                        'size'          => '14',
                        'bold'       =>  true
                    ));
                    $cells->setBackground('#59BC5D');
                    $cells->setBorder('medium');
                });

                $sheet->fromArray($data, '--', 'A1');
            }

			if ($withImage && count($withImage) > 0) {
				if (isset($withImage['img']) && $withImage['cell']) {
					$objDrawing = new \PHPExcel_Worksheet_Drawing();
					$objDrawing->setPath($withImage['img']); //your image path
					$objDrawing->setCoordinates($withImage['cell']);
					$objDrawing->setWorksheet($sheet);
				}
			}

        });

        return $excel;
    }

    /**
	 * MultiBranch Data
	 */
	public static function makeMultiBranchExcel($data, $branches, $filename, $firstSheetName, $firstView, $bladeview, $viewData = [], $store = false, $moneyColumn = [])
	{

		for ($i = 'A', $j = 1; $j <= 26; $i++, $j++) {
			$letters[$i] = "#,##0.00_-";
		}

		$excel = Excel::create($filename, function($excel) use ($data, $firstSheetName, $branches, $firstView, $bladeview, $viewData, $moneyColumn, $letters) {

			$excel = self::makeSheet($excel, $firstSheetName, $data, $firstView, $viewData, $moneyColumn, $letters, false);

            
			foreach ($branches as $key => $branch) {
                
				$name = $branch->name;
				$monCol = $moneyColumn;
				$viewData['excelBranch'] = $branch;
                
                // if ($key == 1) {
                //     dd($excel, $firstSheetName, $data, $bladeview, $viewData, $monCol, $letters, false);
                // }
				$excel = self::makeSheet($excel, $firstSheetName, $data, $bladeview, $viewData, $monCol, $letters, false);
			}

		});

        if (self::isCypressRequest()) {
            return response()->json(['success' => true]);
        }
        
        if ($store) {
            return $excel->store('xls', public_path('exports/'), true);
        } else {
            return $excel->export('xls');
        }

        // if ($store) {
        //     if (!self::isCypressRequest()) {
        //         return $excel->store('xls', public_path('exports/'), true);
        //     } else {
        //         return response()->json(['success' => true]);
        //     }
        // } else {
        //     if (!self::isCypressRequest()) {
        //         return $excel->export('xls');
        //     } else {
        //         return response()->json(['success' => true]);
        //     }
        // }
	}

    private static function isCypressRequest() {
        return request()->user()->id == env('CYPRESS_LOGIN') ? true : false;
    }

}