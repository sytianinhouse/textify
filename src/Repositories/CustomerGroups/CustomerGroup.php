<?php

namespace ATM\Repositories\CustomerGroups;

use ATM\ContextInterface;
use ATM\ContextScope;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Tasks\Task;
use App\Model\BaseModel;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class CustomerGroup extends BaseModel implements ContextScope {

	use ValidatingTrait;

	protected $rules = [
		'name' => 'required',
	];

	protected $fillable = [
							'name',
							'description',
							'client_id'
						  ];

	protected $table = 'customer_groups';

	public function clients()
	{
		return $this->belongsTo(Client::class, 'client_id');
	}

    public function subAccount()
    {
        return $this->belongsTo(SubAccount::class, 'client_id');
    }

	// public function subAccounts()
    // {
    //     return $this->belongsToMany(SubAccount::class, 'customer_group_sub_accounts', 'customer_group_id', 'subaccount_id')->withTimestamps();
    // }

    public function customers() {
        return $this->belongsToMany(Customer::class, 'customer_group_customers', 'customer_group_id', 'customer_id');
    }

    public function taskCustomerGroups() {
        return $this->belongsToMany(Task::class, 'task_group_customers', 'customer_groups_id', 'task_id');
    }

	public function users()
    {
        return $this->belongsToMany(User::class);
    }

	// Query Scopes
	public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->where($context->column(), $context->id());
        } else if ($context->getInstance() instanceof SubAccount) {
            return $query->whereHas('clients', function($q) use($context){
                return $q->where('id', $context->getInstance()->client_id);
            });
        }
        return $query;
    }
}
