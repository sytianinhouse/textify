<?php

namespace ATM\Repositories\CustomerGroups;

use ATM\ContextInterface;
use ATM\Repositories\Client\Client;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CustomerGroupsRepository extends ContextRepository {

	protected $model;

	public function __construct(CustomerGroup $customergroup, ContextInterface $context)
	{
		$this->model = $customergroup;
		$this->context = $context;
	}

	public function search(Request $request, $with = [])
	{
		$query = $this->scope($with);

		if ($request->filled('client_id')) {
			$query = $query->whereHas('clients', function($q) use ($request) {
				$q->where('client_id', $request->client_id);
			});
		}

		if ($request->filled('filter_search')) {
			$query = $query->where('name', 'LIKE' ,"%{$request->filter_search}%");
		}

		if ($request->filled('per_page')) {
			return $query->paginate($request->per_page);
		} else {
			return $query->get();
		}
	}

	public function add(Request $request, Client $client)
	{
		$customerGroup = $this->model;
		$customerGroup->name = $request->name;
		$customerGroup->description = $request->description;
		$customerGroup->client_id = $request->client_id;
		$customerGroup->save();

		/*if ( $customerGroup->save() ) {
			$customerGroup->subAccounts()->sync($request->client_id);
		}*/

		return $customerGroup;
	}

	public function update(CustomerGroup $customergroup, Request $request)
	{
		$customergroup->name = $request->name;
		$customergroup->description = $request->description;
		$customergroup->client_id = $request->client_id;

		//$customergroup->subAccounts()->sync($request->client_id);
		$customergroup->save();

		return $customergroup;
	}

	public function delete(CustomerGroup $customergroup)
	{

		$name = $customergroup->name;
		$customergroup->delete();

		//$customergroup->subAccounts()->detach();

		return $name;
	}


}
