<?php

namespace ATM\Repositories\Province;

use ATM\Repositories\Cities\City;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
	public $table = "provinces";

	public function city()
	{
		return $this->hasOne(City::class, 'province_id');
	}
}
