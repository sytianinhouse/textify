<?php

namespace ATM\Repositories\Uploadable;

use App\Model\BaseModel;


class Uploadable extends BaseModel
{
    protected $table = 'uploadables';

    const IMG_PLACEHOLDER = 'images/placeholder.png';
    const HUMAN_PLACEHOLDER = 'images/human_placeholder.png';

    public function uploadable()
    {
        return $this->morphTo();
    }

    public function auditCreate()
    {
        return "New image '{$this->filename}'";
    }
}
