<?php

namespace ATM\Repositories\User;

use ATM\ContextInterface;
use ATM\Repositories\Client\Client;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\Uploadable\UploadableRepository;
use ATM\Validation\ValidationException;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

/**
 * Off context on other parts of query
 */
class UserRepository extends ContextRepository {

	protected $model;

	protected $throwValidationException = false;

	public function __construct(
		User $user,
		UploadableRepository $uploadRepo,
		ContextInterface $context)
	{
		$this->model = $user;
		$this->uploadRepo = $uploadRepo;
		$this->context = $context;
	}

	/**
	 * Find user of client with request username and password
	 *
	 * @param  Client $client
	 * @param  string $username
	 * @return string $password
	 */
	public function authorizeUser(Client $client, $username, $password) //, $fromPos = true)
	{
		$this->context->reset();
		$users = $this->getAllBy('username', $username, ['client']);
	    $user = $users->filter(function($user) use ($client) {
	        return $user->client && $user->client->cuid == $client->cuid;
	    })->first();

	    if (!$user || false === \Hash::check($password, $user->password)) {
		    $this->throwValidationException([
		    	'error' => 'User not authorized',
		    ]);
	    }
	    // see if from pos then check perform-overides permission
	    // if ($fromPos) {
	    // 	$permission = Permission::whereName('perform-overides')->first();
	    // 	if ($permission && !$user->isPermitted($permission)) {
	    // 		$this->throwValidationException([
			  //   	'error' => 'User not authorized',
			  //   ]);
	    // 	}
	    // }

	    return $user;
	}

	public function search(Request $request, $with = [])
	{
		$query = $this->scope($with);

		if ($request->filled('filter_search')) {
			$strings = explode(' ', $request->filter_search);
			foreach ($strings as $string) {
				$query = $query->where(function($q) use ($string) {
					$q->where('first_name', 'LIKE', "%{$string}%")
						->orWhere('last_name', 'LIKE', "%{$string}%");
				});
			}
		}

		if ($request->filled('account_users_only')) {
			$query->where('account_type', 'account_user');
			$query->whereHas('subAccounts', function($qry) use ($request) {
				$qry->where('sub_accounts.id', $request->subaccount_id);
			});
		}

		if ($request->has('per_page')) {
			return $query->paginate($request->per_page);
		} else {
			return $query->get();
		}
	}

	public function addUser(Client $client = null, Request $request)
	{
		$user = $this->model;
		// dd($request);
		// validate user datails, role, username and password
		// then collate all the errors if any
		$this->validateUserData($request);
		$this->validateUsernameAndEmail($client, $request->username, $request->email);
		$this->validatePassword($request->password, $request->password_confirmation);

		if (count($this->errors) > 0) {
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		$user->fill($request->input());
		$user->active = $request->has('active');
		$user->password = bcrypt($request->password);
		$user->client()->associate($client);

		$user->save();
		$this->uploadMedia($request, $user);

		// Attach roles and subAccounts if Account is Regular User
		if ($user->isAccountUser()) {
			$roles = (array) $request->roles;
			$user->roles()->attach($roles);
			$user->subAccounts()->attach((array) $request->sub_account);
		}

		return $user;
	}

	public function updateUser(Client $client, User $user, Request $request)
	{
		$this->validateUserData($request);

		$this->validateUsernameAndEmail($client, $request->username, $request->email, $user);

		if ($request->filled('password') || $request->filled('password_confirmation')) {
            $this->validatePassword($request->password, $request->password_confirmation);
            $user->password = bcrypt($request->password);
        }

		if (count($this->errors) > 0) {
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		$user->fill($request->input());
        $user->active = $request->has('active');
        // $user->client()->associate($client);

		$user->save();

		$this->uploadMedia($request, $user);

		if( $request->has_upload != null ) {
			if($user->uploads->count() >= 1) {
				$uploadable = $user->uploads->first();
				$this->uploadRepo->deleteThisUploadable($uploadable);
				$uploadable->delete();
			}
		}

        if ($user->isAccountUser()) {
			// dd($request->all());
			$roles = (array) $request->roles;
	        $user->roles()->sync($roles);
	        // detach muna para ma sort ung branches
	        $user->subAccounts()->detach();
            $user->subAccounts()->sync((array) $request->sub_account);
        }

		return $user;
	}

	public function updateUserAdmin(User $user, Request $request)
	{
		$this->validateUserData($request);

		$this->validateUsernameAndEmailAdmin($request, $user);

		if ($request->filled('password') || $request->filled('password_confirmation')) {
            $this->validatePassword($request->password, $request->password_confirmation);
            $user->password = bcrypt($request->password);
        }

		if (count($this->errors) > 0) {
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		$user->fill($request->input());
        $user->active = $request->has('active');
        // $user->client()->associate($client);

		$user->save();

		$this->uploadMedia($request, $user);

		if( $request->has_upload != null ) {
			if($user->uploads->count() >= 1) {
				$uploadable = $user->uploads->first();
				$this->uploadRepo->deleteThisUploadable($uploadable);
				$uploadable->delete();
			}
		}

        if ($user->isAccountUser()) {
        	$roles = (array) $request->roles;
	        $user->roles()->sync($roles);
	        // detach muna para ma sort ung branches
	        $user->subAccounts()->detach();
            $user->subAccounts()->sync((array) $request->sub_account);
        }

		return $user;
	}

	private function uploadMedia(Request $request, $user)
    {
        if ($request->hasFile('icon')) {
            $this->uploadRepo->createUpload($user, $request, 'image', [
                'key' => User::IMG_ICON,
                'field_key' => 'icon',
                'path' => 'uploads/user',
                'filename' => 'icon',
                'width' => 50,
                'height' => 50
            ]);
        }

        if ($request->hasFile('thumb')) {
            $this->uploadRepo->createUpload($user, $request, 'image', [
                'key' => User::IMG_THUMB,
                'field_key' => 'thumb',
                'path' => 'uploads/user',
                'filename' => 'thumb',
                'width' => 358,
                'height' => 182
            ]);
        }

        if ($request->hasFile('banner')) {
            $this->uploadRepo->createUpload($user, $request, 'image', [
                'key' => User::IMG_BANNER,
                'field_key' => 'banner',
                'path' => 'uploads/user',
                'filename' => 'banner',
                'width' => 1366,
                'height' => 345
            ]);
        }
    }

	public function changePassword(Request $request, User $user)
	{
		// validation for password confirmation, etc. is in the controller.
        if (false === \Hash::check($request->input('current_password'), $user->password)) {
        	$this->throwValidationException([
        		'current_password' => 'Current password did not match.',
        	]);
        }

        $user->password = bcrypt($request->input('password'));
        $user->save();
        return $user;
	}

	public function validateUserData(Request $request, User $user = null)
	{
		if( $request->filled('admin') && isset($request['admin']) ) {
			$validator = \Validator::make($request->all(),
		    [
		    	'first_name' => 'required',
		        'last_name' => 'required',
		    ]
		);

		} else {
			$validator = \Validator::make($request->all(),
				[
					'first_name' => 'required',
					'last_name' => 'required',
					'email' => 'email|nullable',
					'account_type' => 'required|in:'.User::ACCOUNT_SUB.','.User::ACCOUNT_CLIENT.','.User::ACCOUNT_SUPERADMIN,
					'roles' => 'required_if:account_type,'.User::ACCOUNT_SUB,
					'sub_account' => 'required_if:account_type,'.User::ACCOUNT_SUB,
				],
				[
					'roles.required_if' => 'Please select the Role to be assigned.',
					'sub_account.required_if' => 'Please select the Sub Account to be assigned.'
				]
			);
		}

		if ($validator->fails()) {
		    $this->errors = $validator->messages()->merge($this->errors);
		}
	}

	public function validatePassword($password, $passwordConfirmation)
	{
		$validator = \Validator::make(
			array(
				'password' => $password,
				'password_confirmation' => $passwordConfirmation,
			),
			array(
				'password' => 'required|min:6|confirmed'
			)
		);

		if ($validator->fails()) {
		    $this->errors = $validator->messages()->merge($this->errors);
		}
	}

	public function validateUsernameAndEmailAdmin(Request $request, User $user = null)
	{
		if( $request->filled('admin') && isset($request['admin']) ) {
			$rules = [
				'username' => ['required']
			];
		} else {
			$rules = [
				'username' => ['required'],
				'email' => ['nullable'],
			];
		}

		if ($user) {
			$rules['username'][] = Rule::unique('users')->ignore($user->id);
			$rules['email'][] = Rule::unique('users')->ignore($user->id);
		} else {
			$rules['username'][] = Rule::unique('users');
			$rules['email'][] = Rule::unique('users');
		}

		$validator = \Validator::make(['username' => $request->username, 'email' => $request->email], $rules);

		if ($validator->fails()) {
		    $this->errors = $validator->messages()->merge($this->errors);
		}
	}

	public function validateUsernameAndEmail(Client $client, $username, $email, User $user = null)
	{
		$rules = [
			'username' => ['required'],
			'email' => ['nullable'],
		];

		if ($user) {

			$rules['username'][] = Rule::unique('users')->ignore($user->id)
						->where(function ($query) use ($client) {
						    $query->where('client_id', $client->id);
						});
			$rules['email'][] = Rule::unique('users')->ignore($user->id)
						->where(function ($query) use ($client) {
						    $query->where('client_id', $client->id);
						});
		} else {
			$rules['username'][] = Rule::unique('users')
						->where(function ($query) use ($client) {
						    $query->where('client_id', $client->id);
						});
			$rules['email'][] = Rule::unique('users')
						->where(function ($query) use ($client) {
						    $query->where('client_id', $client->id);
						});
		}

		$validator = \Validator::make(['username' => $username, 'email' => $email], $rules);

		if ($validator->fails()) {
		    $this->errors = $validator->messages()->merge($this->errors);
		}
	}

	public function delete(User $user)
	{
		$name = $user->name;
        $user->delete();
        return $name;
	}

	public function createDefaultUser(Client $client, $regularUser = false, $subAccount = null, $role = null)
	{
		$user = new User;

		if (!$regularUser) {
			$user->first_name = 'Textify';
			$user->last_name = 'Support';
			$user->account_type = User::ACCOUNT_CLIENT;
			$user->username = User::DEF_CLIENT_U;
		} else {
			$user->first_name = 'Textify';
			$user->last_name = 'Support';
			$user->account_type = User::ACCOUNT_SUB;
			$user->username = User::DEF_USER_U;
		}

		$user->password = bcrypt('sytian2019');
		$user->active = 1;
		$user->client()->associate($client);

		$user->save();

		// Attach roles and subAccounts if Account is Regular User
		if ($regularUser) {
			$user->roles()->attach($role);
			$user->subAccounts()->attach($subAccount);
		}

		return $user;
	}

}
