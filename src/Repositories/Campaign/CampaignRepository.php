<?php

namespace ATM\Repositories\Campaign;

use ATM\ContextInterface;
use ATM\Repositories\ApiKey\ApiKey;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Client\ClientRepository;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CampaignRepository extends ContextRepository {

	protected $model;

	public function __construct(
		Campaign $campaign,
		ContextInterface $context,
		ClientRepository $clientRepo
	)
	{
		$this->model = $campaign;
		$this->context = $context;
		$this->clientRepo = $clientRepo;
	}

	public function search(Request $request, $with = [])
	{
		$query = $this->scope($with);

		if ($request->filled('subaccount_filter')) {
			$query = $query->whereHas('subAccounts', function($q) use ($request) {
				$q->where('sub_accounts.id', $request->subaccount_filter);
			});
		}

		if ($request->filled('subaccount_id')) {
			$query = $query->whereHas('subAccounts', function($q) use ($request) {
				$q->where('sub_accounts.id', $request->subaccount_id);
			});
        }

        if ($request->filled('campaign_id')) {
            $query = $query->where('id', $request->campaign_id);
        }

		if ($request->filled('sub_account_id')) {
			$query = $query->whereHas('subAccounts', function($q) use ($request) {
				$q->where('sub_accounts.id', $request->sub_account_id);
			});
		}

		if ($request->filled('filter_search')) {
			$query = $query->where('name','LIKE',"%{$request->filter_search}%");
		}

		if ($request->filled('per_page')) {
			return $query->paginate($request->per_page);
		} else {
			return $query->get();
		}
	}

	public function getLastCreated(Request $request, $select = ['*'], $count = 10)
	{
		$query = $this->scope([]);

		if (count($select) > 0) {
			$query = $query->select($select);
		}

		return $query->orderBy('created_at', 'desc')->take($count)->get();
	}

	public function forFiltersList(Request $request)
	{
		$query = $this->scope();

		return $query->get()->pluck('name', 'id');
	}

	public function store(Campaign $campaign, Request $request)
	{
        $client = ($request->user()->client) ? $request->user()->client : $request['client_id'];

		if ($request->has('client_id')) {
            $client = $this->clientRepo->search($request)->first();
		}

		$campaign = $this->model;
		$campaign->fill($request->input());

		if ($request->has('subaccount_id')) {
            $request['sub_account_id'] = $request->subaccount_id;
		}

		if ($request->has('sub_account_id')) {
			if (is_array($request->sub_account_id)) {
				$subAccountIds = (array) $request->sub_account_id;
			} else {
				$subAccountIds[] = $request->sub_account_id;
			}
		}

		if (!$client->isMultiSubaccounts()) {
			$subAccountIds = (array) $client->subAccounts->first()->id;
		}

		$campaign->client_id = $client->id;
		$this->validate($campaign);
		$campaign->save();
		$campaign->subAccounts()->sync($subAccountIds);

		return $campaign;
	}


	public function update(Campaign $campaign, Request $request)
	{
		$client = $request->user()->client;

		if ($request->has('sub_account_id')) {
			$subAccountIds = (array) $request->sub_account_id;
        }

		if (!$client->isMultiSubaccounts()) {
			$subAccountIds = (array) $client->subAccounts->first()->id;
		}

		$campaign->fill( $request->input() );
		$this->validate($campaign);
		$campaign->save();

		$campaign->subAccounts()->sync($subAccountIds);

		return $campaign;
	}

	public function delete(Campaign $campaign)
	{
		$campaign->delete();
		return $campaign;
	}
}
