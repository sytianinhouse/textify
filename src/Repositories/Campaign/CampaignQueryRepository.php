<?php

namespace ATM\Repositories\Campaign;

use ATM\ContextInterface;
use ATM\Repositories\BaseRepository;
use ATM\StoredProcedure\Task\CampaignProcedure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class CampaignQueryRepository extends BaseRepository
{
	protected $model;

	public function __construct(
		Campaign $campaign,
		ContextInterface $context,
		CampaignProcedure $campaignProcedure)
	{
		$this->model = $campaign;
		$this->context = $context;
		$this->campaignProcedure = $campaignProcedure;
	}

	public function search(Request $request, $useStored, $perPage = false)
	{
		if ($useStored) {
			return $this->searchUseStored($request, $perPage);
		}
	}

	public function searchUseStored(Request $request, $perPage = false)
	{
        $campaigns = $this->campaignProcedure->searchTaskPerCampaign($request);
        $campaigns = collect($campaigns);

		if ($perPage) {
			return $this->paginateCollection($campaigns, $request->page, $request->per_page ?: self::PER_PAGE);
		} else {
			return $campaigns;
		}
	}
}
