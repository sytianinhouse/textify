<?php

namespace ATM\Repositories\Campaign;

use ATM\ContextInterface;
use ATM\ContextScope;
use ATM\Repositories\Client\Client;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Tasks\Task;
use App\Model\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Campaign extends BaseModel implements ContextScope
{
    use ValidatingTrait, SoftDeletes;

	protected $rules = [
        'name'           => 'required',
    ];

    protected $fillable = [
        'name','description'
    ];

    public function subAccounts()
    {
        return $this->belongsToMany(SubAccount::class, 'campaign_sub_accounts', 'campaign_id', 'sub_account_id');
    }

    public function task()
    {
        return $this->hasMany(Campaign::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function isActive()
    {
        return $this->active ? true : false;
    }


    /**
	 * Mutators
	 */
	public function getDateTimeToSendAttribute() // date_time_to_send
	{
		return new Carbon("{$this->created_at->toDateString()}");
    }

	// ----------------------------------
    // Scope Queries
    // ----------------------------------

    public function scopeActives($query)
    {
        return $query->where('active', true);
    }

    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->whereHas('subAccounts', function($q) use($context) {
            	$q->where($context->column(), $context->id());
			});
        } else if ($context->getInstance() instanceof SubAccount) {
			return $query->whereHas('subAccounts', function($q) use($context) {
				$q->where('sub_accounts.id', $context->id());
			});
        }
        return $query;
    }
}
?>
