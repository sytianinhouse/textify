<?php
namespace ATM\Repositories\Settings;

use App\Model\BaseModel;

class SiteSettings extends BaseModel
{
	protected $table = 'site_settings';

	const PROMOTEXTER = 'promotexter';
	const GLOBELABS = 'globelabs';

	public static $gateways = [
		self::PROMOTEXTER => 'Promotexter',
		self::GLOBELABS => 'Globelabs'
	];

	/**
	 * Helper Methods
	 */
	public function theSettings($columns = '*', $with = [])
	{
		return $this->select($columns)->with($with)->find(1);
	}

	public function isPromotexter()
	{
		return $this->gateway_to_use == self::PROMOTEXTER ?: false;
	}

	public function isGlobeLabs()
	{
		return $this->gateway_to_use == self::GLOBELABS ?: false;
	}

	public static function getSettings()
	{
		$instance = new static;
		$thisSite = $instance->theSettings();

		return $thisSite;
	}

	public static function isDisabledSending()
 	{
		$instance = new static;
		$thisSite = $instance->theSettings();

		return $thisSite->sending_disabled;
	}

	public static function gatewayToUse()
	{
		$instance = new static;
		$thisSite = $instance->theSettings();

		return $thisSite->gateway_to_use;
	}

	public function getCharLimit()
	{
		return $this->sms_character_limit ? ($this->sms_character_limit > 0 ? $this->sms_character_limit : 160) : 160;
	}

	public function getCreditConversion()
	{
		return $this->credit_conversion ? $this->credit_conversion : 1;
	}

	public static function convertLoadToRate($amount, $int = false)
	{
		$instance = SiteSettings::getSettings();
		if( $int ) {
			return $instance->getCreditConversion() * $amount;
		} else {
			return number_format($instance->getCreditConversion() * $amount, 2);
		}
	}
}
