<?php

namespace ATM\Repositories\TaskJobs\AuditLog;

use ATM\ContextInterface;
use ATM\ContextScope;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Client\Client;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;
use ATM\Repositories\Tasks\JobsCompleted\JobsCompleted;
use ATM\Repositories\Tasks\JobsFailed\JobsFailed;
use ATM\Repositories\Tasks\JobsNextMonth\JobNextMonth;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;

use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use ATM\Repositories\Uploadable\Uploadable;
use App\Model\BaseModel;
use App\User;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class JobsAuditLog extends BaseModel {

	use SoftDeletes;

	public $table = "jobs_audit_logs";

    public function clients()
    {
        return $this->belongsTo(Client::class);
    }

    public function subAccount() {
        return $this->belongsTo(SubAccount::class, 'subaccount_id');
    }

    // Query Scopes
    // public function scopeOfContext($query, ContextInterface $context)
    // {

    //     if ($context->getInstance() instanceof Client) {
    //         return $query->whereHas('subAccount', function($q) use($context){
    //             return $q->where($context->column(), $context->id());
    //         });
    //     } else if ($context->getInstance() instanceof SubAccount) {
    //         return $query->where($context->column(), $context->id());
    //     }

    //     return $query;
    // }

}

?>
