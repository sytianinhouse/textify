<?php 

namespace ATM\Repositories\TaskJobs\AuditLog;

use ATM\Repositories\BaseRepository;
use ATM\Repositories\TaskJobs\AuditLog\JobsAuditLog;
use Carbon\Carbon;
use Illuminate\Http\Request;

class JobsAuditLogRepository extends BaseRepository {

	public function __construct(
        JobsAuditLog $jobsAuditLogs
    )
    {
    	$this->model = $jobsAuditLogs;
    }

    public function addLog($oldJob = null, $newJob)
	{
		$log = new JobsAuditLog();

		$log->job_id = $newJob->id;
		$log->subaccount_id = $newJob->subaccount_id;
		$log->client_id = $newJob->client_id;
		$log->customer_number = $newJob->customer_number;
		$log->when_to_send_previous = $oldJob ? $oldJob->when_to_send : null;
		$log->when_to_send_current = $newJob->when_to_send;
		$log->transferred_at = Carbon::now()->toDateTimeString();

		$log->save();

		return;
	}

}