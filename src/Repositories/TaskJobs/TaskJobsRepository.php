<?php

namespace ATM\Repositories\TaskJobs;

use Illuminate\Support\Str;
use ATM\Repositories\BaseRepository;
use ATM\Repositories\CustomerGroups\CustomerGroupsRepository;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Settings\SiteSettingsRepository;
use ATM\Repositories\TaskJobs\AuditLog\JobsAuditLogRepository;
use ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;
use ATM\Repositories\Tasks\JobsNextMonth\JobNextMonth;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Tasks\TaskRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class TaskJobsRepository extends BaseRepository {

	public function __construct(
        JobToday $JobsToday,
        JobTomorrow $JobsTomorrow,
        JobAfterTomorrowTo14thDay $JobsAfterTomorrowTo14thDay,
        Job15thDayToEndMonth $Jobs15thDayToEndMonth,
        JobNextMonth $JobsNextMonth,

        Customer $customer,
        CustomersRepository $customerRepo,
        CustomerGroupsRepository $customerGroupRepo,

        SiteSettingsRepository $settingsRepo,
        JobsAuditLogRepository $jobsAuditLogsRepo,
		MessageBag $messageBag
    )
    {
    	$this->customerGroupRepo = $customerGroupRepo;
    	$this->customerRepo = $customerRepo;
        $this->customerModel = $customer;
        $this->settingsRepo = $settingsRepo;
        $this->jobsAuditLogsRepo = $jobsAuditLogsRepo;
		$this->messageBag = $messageBag;
    }

    public function getJobListOld(Request $request, $task)
	{
		$taskRepo  = resolve(\ATM\Repositories\Tasks\TaskRepository::class);
		$messageBag = new MessageBag();

		$validator = Validator::make(
            ['data' => $task],
            ['data' => 'required']
        );

        if ($validator->fails()) {
			$this->errors = $messageBag->add('job_error', 'There was an error during the creation of jobs, please try again later.');
			throw new \ATM\Validation\ValidationException($this->errors);
        }

        $taskTypeId = isset($task->id) ? $task->id : new Task(); // get task type id
        $getAllCustomers = $task->taskCustomers; // get all customers
        $getCurrentDate = $task->date_to_send; // get the date set

        $lastDateOfMonth = date("Y-m-t", strtotime($getCurrentDate)); // get last day of the month
        $firstDayofNextMonth = date("Y-m-d", strtotime(date('m', strtotime('+1 month')).'/01/'.date('Y'))); // get the first day of next month
        $getDateAfterTomorrow = date('Y-m-d', strtotime(new Carbon() . ' +2 day'));

        $today = new Carbon(); // This object represents current date/time
        $today->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison
        $match_date = Carbon::createFromFormat( "Y-m-d", $getCurrentDate );
        $match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison
        $diff = $today->diff( $match_date );
        $diffDays = (integer) $diff->format( "%R%a" );

        // Insert sa job for next month
		$allArraysJobNextMonth = [];
		$allArraysJob15thDay = [];
		$allArraysJob14thDay = [];
		$allArraysJobTomorrow = [];
		$allArraysJobToday = [];
		$totalCredits = 0;

		$now = Carbon::now()->toDateTimeString();

		foreach ( $getAllCustomers as $customer ):

			if ( $customer->mobile && $customer->mobile != NULL && $customer->mobile != '' ) {

                // if( $task->date_to_send != NULL && $task->time_to_send != NULL ) {
                //     $whenToSend = strftime('%F %H:%M:%S', strtotime($task->date_to_send.' '.$task->time_to_send)); // get when to send.
                // } else {
                // }

                $whenToSend = strftime('%F %H:%M:%S', strtotime($task->date_to_send.' '.$task->time_to_send)); // get when to send.

				$message = $taskRepo->makeSmsFormatted($task, $customer);
				$creditsConsumed = $taskRepo->creditComputation($message);
				$totalCredits += $creditsConsumed;

				$draw = [
					'customer_id' => $customer->id,
					'customer_name' => $customer->first_name.' '.$customer->last_name,
					'customer_number' => preg_replace('/\D+/', '', $customer->mobile),
					'gateway_key' => $task->gateway_key,
					'campaign_id' => $task->campaign_id,
					'credits_consumed' => $creditsConsumed,
					'when_to_send' => $whenToSend,
					'message' => $message,
					'task_id' => $taskTypeId,
					'subaccount_id' => $task->subaccount_id,
					'client_id' => $task->client_id,
					'created_at' => $now,
					'updated_at' => $now,
				];

				// For next month job
				if ($firstDayofNextMonth == $getCurrentDate || $getCurrentDate >= $firstDayofNextMonth) {
					$allArraysJobNextMonth[] = $draw;

				// Job 15th
				} else if ($getCurrentDate == $lastDateOfMonth ||
					$getCurrentDate == date('Y-m-d', strtotime($getDateAfterTomorrow . ' +14 day')) ||
					($getCurrentDate < $lastDateOfMonth && $getCurrentDate > date('Y-m-d', strtotime($getDateAfterTomorrow . ' +14 day'))))
				{
					$allArraysJob15thDay[] = $draw;

				// Job 14th
				} else if (
					$getCurrentDate == $getDateAfterTomorrow ||
					$getCurrentDate == date('Y-m-d', strtotime($getDateAfterTomorrow . ' +13 day')) ||
					($getCurrentDate > $getDateAfterTomorrow && $getCurrentDate < date('Y-m-d', strtotime($getDateAfterTomorrow . ' +13 day')))
				) {
					$allArraysJob14thDay[] = $draw;

				// Job Tomorrow
				} else if ($diffDays == +1) {
					$allArraysJobTomorrow[] = $draw;

				// Job Today
				} else if (
					$diffDays == 0 ||
					$today->toDateString() == $getCurrentDate
				) {
					$allArraysJobToday[] = $draw;

				// Last action
				} else {
					$this->errors = $messageBag->add('task', 'No Jobs Found ...')->merge($this->errors);
					return;
				}
			}

		endforeach;

		if ($task->subAccount->credit) {
			$credit = $task->subAccount->credit;

			if ($totalCredits > $credit->pending_balance) {
				$this->errors = $messageBag->add('job_error', 'Your credit is not enough for the creation of this SMS task.');
				throw new \ATM\Validation\ValidationException($this->errors);
			}

			$credit->pending_balance = $credit->pending_balance - $totalCredits;
			$credit->save();
		}

		if (count($this->errors) > 0) {
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		if (count($allArraysJobNextMonth) > 0) {
			JobNextMonth::insert( $allArraysJobNextMonth );
		}

		if (count($allArraysJob15thDay) > 0) {
			Job15thDayToEndMonth::insert( $allArraysJob15thDay );
		}

		if (count($allArraysJob14thDay) > 0) {
			JobAfterTomorrowTo14thDay::insert( $allArraysJob14thDay );
		}

		if (count($allArraysJobTomorrow) > 0) {
			JobTomorrow::insert( $allArraysJobTomorrow );
		}

		if (count($allArraysJobToday) > 0) {
			JobToday::insert( $allArraysJobToday );
		}

		return $task;

        if (count($this->errors) > 0) {
            throw new \ATM\Validation\ValidationException($this->errors);
        }
	}

	// Creation ng different job from module creation of TASK
	public function createJobs(Request $request, $task)
	{
        $mobile_numbers = array_filter(explode(',', $request->mobile_number_to_be_send));

        $prime_id = array_filter(explode(',', $request->contacts_to_be_send));
		$secondary_id = array_filter(explode(',', $request->generated_contact));
		$ids = array_merge($prime_id,$secondary_id);
        $ids = array_unique($ids);

		$subAccount = $task->subAccount;
		$taskRepo  = resolve(\ATM\Repositories\Tasks\TaskRepository::class);
		$smsFacade = resolve(\ATM\Repositories\Sms\SmsFacade::class);
		$messageBag = new MessageBag();

		$validator = Validator::make(
            ['data' => $task],
            ['data' => 'required']
        );

        if ($validator->fails()) {
			$this->errors = $messageBag->add('job_error', 'There was an error during the creation of jobs, please try again later.');
			throw new \ATM\Validation\ValidationException($this->errors);
        }

        $taskTypeId = isset($task->id) ? $task->id : new Task(); // get task type id
        // $getAllCustomers = $task->taskCustomers; // get all customers
        $getAllCustomers = collect();
        foreach( $ids as $theID ) {
            $result = Customer::where('id' , '=' , $theID)->get();
            $getAllCustomers = $getAllCustomers->merge($result);
        }
        /*if ($getAllCustomers->count() > 700) {
			$getAllCustomers = $getAllCustomers->take(700);
		}*/

        $today = Carbon::now()->startOfDay(); // This object represents current date/time

        $dateToSend =  Carbon::parse($task->date_to_send); // get the date set

		$startOfThisMonth = $today->copy()->startOfMonth();
        $firstDayofNextMonth = $startOfThisMonth->copy()->addMonth()->startOfMonth(); // get the first day of next month

        $matchDate = $dateToSend->copy()->startOfDay(); // reset time part, to prevent partial comparison

        $diff = $today->diff($matchDate);
        $diffDays = (integer) $diff->days;

        // Insert sa job for next month
		$allArraysJobNextMonth = [];
		$allArraysJob15thDay = [];
		$allArraysJob14thDay = [];
		$allArraysJobTomorrow = [];
		$allArraysJobToday = [];
		$totalCredits = 0;

        $currentDateTime = Carbon::now()->toDateTimeString();
        $getAllCustomers = $getAllCustomers->concat($mobile_numbers);

		foreach ( $getAllCustomers as $key => $customer ) {
            $isCustomer = $customer instanceOf Customer ? true : false;
			if ( ($isCustomer && $customer->mobile && $customer->mobile != NULL && $customer->mobile != '') || count($mobile_numbers) > 0 ) {
                $whenToSend = Carbon::parse($task->date_to_send->toDateString() .' '. $task->time_to_send); // get when to send.
                $message = $taskRepo->makeSmsFormatted($task, $customer);
                $creditsConsumed = $subAccount->getCreditAmount(Str::length($message));
                // echo $message . ' --- ' . Str::length($message) . ' === ' . $creditsConsumed . '<br>';
                $totalCredits += $creditsConsumed;

                if( $whenToSend->gt($currentDateTime) ) {
                    $draw = [
                        'customer_id' => ($isCustomer && $customer->id) ? $customer->id : 0,
                        'customer_name' => ($isCustomer && $customer->first_name) ? $customer->first_name.' '.$customer->last_name : ' ',
                        'customer_number' => ($isCustomer && $customer->mobile) ? preg_replace('/\D+/', '', $customer->mobile) : preg_replace('/\D+/', '', $customer),
                        'gateway_key' => $task->gateway_key,
                        'campaign_id' => $task->campaign_id,
                        'credits_consumed' => $creditsConsumed,
                        'when_to_send' => $whenToSend->toDateTimeString(),
                        'message' => $message,
                        'task_id' => $taskTypeId,
                        'subaccount_id' => $task->subaccount_id,
                        'client_id' => $task->client_id,
                        'source' => $request->ip(),
                        'created_at' => $currentDateTime,
                        'updated_at' => $currentDateTime,
                    ];

                    if ($diffDays) {
                        if ($diffDays == 1) {
                            $allArraysJobTomorrow[] = $draw;
                        } elseif($diffDays >= 2 && $diffDays <= 14) {
                            $allArraysJob14thDay[] = $draw;
                        } elseif($diffDays >= 15) {
                            if ($matchDate->gte($firstDayofNextMonth)) {
                                $allArraysJobNextMonth[] = $draw;
                            } else {
                                $allArraysJob15thDay[] = $draw;
                            }
                        }
                    } else {
                        if ($diffDays == 0) {
                            $allArraysJobToday[] = $draw;
                        } else {
                            $this->errors = $messageBag->add('task', 'No Jobs Found ...')->merge($this->errors);
                            return;
                        }
                    }
                } else {
                    $request['first_name'] = ( $isCustomer ) ? $customer->first_name : ' ';
                    $request['last_name'] = ( $isCustomer ) ? $customer->last_name : ' ';
                    $request['number'] = ( $isCustomer ) ? $customer->mobile : preg_replace('/\D+/', '', $customer);
                    $this->immediateSend($request, $task);
                }
            }
        }

        if( !$request->filled('send_immediately') && $request->send_immediately != true ) {
            if ( $task->subAccount->credit ) {
                $credit = $task->subAccount->credit;
                if ($totalCredits > intval($credit->pending_balance)) {
                    $this->errors = $messageBag->add('job_error', 'Your credit is not enough for the creation of this SMS task.');
                    throw new \ATM\Validation\ValidationException($this->errors);
                }
                $smsFacade->minusPendingCreditPerSubaccount($totalCredits, $task->subAccount);
            }
        }

		if (count($this->errors) > 0) {
			$this->errors = $messageBag->add('job_error', 'There was an error during the creation of jobs, please try again later.');
			throw new \ATM\Validation\ValidationException($this->errors);
        }

		$jobsData = [
			'allArraysJobNextMonth' => $allArraysJobNextMonth,
			'allArraysJob15thDay' => $allArraysJob15thDay,
			'allArraysJob14thDay' => $allArraysJob14thDay,
			'allArraysJobTomorrow' => $allArraysJobTomorrow,
			'allArraysJobToday' => $allArraysJobToday,
        ];

		$this->continueSendingJobs($jobsData);
		return $task;
	}

    public function immediateSend($request, Task $task)
	{
		$subAccount = $task->subAccount;

    	if (!$subAccount) {
			$this->errors = $this->messageBag->add('fatal_error', 'Fatal error on making the request.');
		}

		$smsFacade = resolve(\ATM\Repositories\Sms\SmsFacade::class);

		$lName = $request->filled('last_name') ? $request->last_name : '';
        $fName = $request->filled('first_name') ? $request->first_name : '';
        $number = $request->filled('number') ? $request->number : '';

        $request['filter_search'] = $fName . ' ' . $lName;

        $existingContact = $this->customerRepo->search($request)->first();

        if ($existingContact) {
        	$request['existing_contact_id'] = $existingContact->id;
        }

		$message = str_replace(
			['[first_name]', '[last_name]', '[number]'],
			[$fName, $lName, $number],
			$task->message
		);
		$messageLength = Str::length($message);

		if ($subAccount->isBeechoo()) { // beechoo
			$formattedNumber = formattedNumber($number);
			$canSend = $subAccount->isCreditSufficientForTaskWithNumber($messageLength, $formattedNumber);
		} else {
			$canSend = $subAccount->isCreditSufficientForTask($messageLength);
		}

		if (!$task->active) {
			$this->errors = $this->messageBag->add('fatal_error', 'This SMS task is no longer active and can\' receive any more job request.');
		}

		if (count($this->errors) > 0) {
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		if ($canSend) {
			try {
				$request['name'] = $fName . ' ' . $lName;
				$request['message'] = $message;
				$request['from_text_blast'] = true;
				$result = $smsFacade->createImmediateJob($request, $subAccount, $task);

				return $result;
			} catch (ValidationException $e) {
				$errors = $e->getErrors();
				$this->errors = $this->messageBag->add('fatal_error', $errors[0]);
				throw new \ATM\Validation\ValidationException($this->errors);
			}
		} else {
			return false;
		}
    }

	public function continueSendingJobs($jobsToCreate = [])
	{
		$settings = $this->settingsRepo->getSettings();
		$allArraysJobNextMonth = $jobsToCreate['allArraysJobNextMonth'];
		$allArraysJob15thDay = $jobsToCreate['allArraysJob15thDay'];
		$allArraysJob14thDay = $jobsToCreate['allArraysJob14thDay'];
		$allArraysJobTomorrow = $jobsToCreate['allArraysJobTomorrow'];
		$allArraysJobToday = $jobsToCreate['allArraysJobToday'];

		if (count($allArraysJobNextMonth) > 0) {

			if ($settings->jobs_audit_log_disabled) {
				JobNextMonth::insert( $allArraysJobNextMonth ); // insert in 14th day table
			} else {
				foreach ($allArraysJobNextMonth as $jobsNextMonth) {

					$newJobNextMonth = new JobNextMonth();

					$newJobNextMonth->customer_id = $jobsNextMonth['customer_id'];
					$newJobNextMonth->customer_name = $jobsNextMonth['customer_name'];
					$newJobNextMonth->customer_number = $jobsNextMonth['customer_number'];
					$newJobNextMonth->gateway_key = $jobsNextMonth['gateway_key'];
					$newJobNextMonth->campaign_id = $jobsNextMonth['campaign_id'];
					$newJobNextMonth->credits_consumed = $jobsNextMonth['credits_consumed'];
					$newJobNextMonth->when_to_send = $jobsNextMonth['when_to_send'];
					$newJobNextMonth->message = $jobsNextMonth['message'];
					$newJobNextMonth->task_id = $jobsNextMonth['task_id'];
					$newJobNextMonth->parent_task_id = $jobsNextMonth['parent_task_id'];
					$newJobNextMonth->subaccount_id = $jobsNextMonth['subaccount_id'];
					$newJobNextMonth->client_id = $jobsNextMonth['client_id'];
					$newJobNextMonth->entry_id = $jobsNextMonth['entry_id'];
					$newJobNextMonth->zs_booking_id = $jobsNextMonth['zs_booking_id'] ?? null;
					$newJobNextMonth->source = $jobsNextMonth['source'];

					$newJobNextMonth->save();

					$this->jobsAuditLogsRepo->addLog(null, $newJobNextMonth);
				}
			}
		}

		if (count($allArraysJob15thDay) > 0) {

			if ($settings->jobs_audit_log_disabled) {
				Job15thDayToEndMonth::insert( $allArraysJob15thDay ); // insert in 14th day table
			} else {
				foreach ($allArraysJob15thDay as $jobs15th) {

					$newJob15th = new Job15thDayToEndMonth();

					$newJob15th->customer_id = $jobs15th['customer_id'];
					$newJob15th->customer_name = $jobs15th['customer_name'];
					$newJob15th->customer_number = $jobs15th['customer_number'];
					$newJob15th->gateway_key = $jobs15th['gateway_key'];
					$newJob15th->campaign_id = $jobs15th['campaign_id'];
					$newJob15th->credits_consumed = $jobs15th['credits_consumed'];
					$newJob15th->when_to_send = $jobs15th['when_to_send'];
					$newJob15th->message = $jobs15th['message'];
					$newJob15th->task_id = $jobs15th['task_id'];
					$newJob15th->parent_task_id = $jobs15th['parent_task_id'];
					$newJob15th->subaccount_id = $jobs15th['subaccount_id'];
					$newJob15th->client_id = $jobs15th['client_id'];
					$newJob15th->entry_id = $jobs15th['entry_id'];
					$newJob15th->zs_booking_id = $jobs15th['zs_booking_id'] ?? null;
					$newJob15th->source = $jobs15th['source'];

					$newJob15th->save();

					$this->jobsAuditLogsRepo->addLog(null, $newJob15th);
				}
			}
		}

		if (count($allArraysJob14thDay) > 0) {

			if ($settings->jobs_audit_log_disabled) {
				JobAfterTomorrowTo14thDay::insert( $allArraysJob14thDay );
			} else {
				foreach ($allArraysJob14thDay as $jobs14th) {

					$newJob14th = new JobAfterTomorrowTo14thDay();

					$newJob14th->customer_id = $jobs14th['customer_id'];
					$newJob14th->customer_name = $jobs14th['customer_name'];
					$newJob14th->customer_number = $jobs14th['customer_number'];
					$newJob14th->gateway_key = $jobs14th['gateway_key'];
					$newJob14th->campaign_id = $jobs14th['campaign_id'];
					$newJob14th->credits_consumed = $jobs14th['credits_consumed'];
					$newJob14th->when_to_send = $jobs14th['when_to_send'];
					$newJob14th->message = $jobs14th['message'];
					$newJob14th->task_id = $jobs14th['task_id'];
					$newJob14th->parent_task_id = $jobs14th['parent_task_id'];
					$newJob14th->subaccount_id = $jobs14th['subaccount_id'];
					$newJob14th->client_id = $jobs14th['client_id'];
					$newJob14th->entry_id = $jobs14th['entry_id'];
					$newJob14th->zs_booking_id = $jobs14th['zs_booking_id'] ?? null;
					$newJob14th->source = $jobs14th['source'];

					$newJob14th->save();

					$this->jobsAuditLogsRepo->addLog(null, $newJob14th);
				}
			}
		}

		if (count($allArraysJobTomorrow) > 0) {

			if ($settings->jobs_audit_log_disabled) {
				JobTomorrow::insert( $allArraysJobTomorrow );
			} else {
				foreach ($allArraysJobTomorrow as $jobsTom) {

					$newJobTom = new JobTomorrow();

					$newJobTom->customer_id = $jobsTom['customer_id'];
					$newJobTom->customer_name = $jobsTom['customer_name'];
					$newJobTom->customer_number = $jobsTom['customer_number'];
					$newJobTom->gateway_key = $jobsTom['gateway_key'];
					$newJobTom->campaign_id = $jobsTom['campaign_id'];
					$newJobTom->credits_consumed = $jobsTom['credits_consumed'];
					$newJobTom->when_to_send = $jobsTom['when_to_send'];
					$newJobTom->message = $jobsTom['message'];
					$newJobTom->task_id = $jobsTom['task_id'];
					$newJobTom->parent_task_id = $jobsTom['parent_task_id'];
					$newJobTom->subaccount_id = $jobsTom['subaccount_id'];
					$newJobTom->client_id = $jobsTom['client_id'];
					$newJobTom->entry_id = $jobsTom['entry_id'];
					$newJobTom->zs_booking_id = $jobsTom['zs_booking_id'] ?? null;
					$newJobTom->source = $jobsTom['source'];

					$newJobTom->save();

					$this->jobsAuditLogsRepo->addLog(null, $newJobTom);
				}
			}
		}

		if (count($allArraysJobToday) > 0) {

			if ($settings->jobs_audit_log_disabled) {
				JobToday::insert( $allArraysJobToday );
			} else {
				foreach ($allArraysJobToday as $jobsToday) {
					$newJobToday = new JobToday();

					$newJobToday->customer_id = $jobsToday['customer_id'];
					$newJobToday->customer_name = $jobsToday['customer_name'];
					$newJobToday->customer_number = $jobsToday['customer_number'];
					$newJobToday->gateway_key = $jobsToday['gateway_key'];
					$newJobToday->campaign_id = $jobsToday['campaign_id'];
					$newJobToday->credits_consumed = $jobsToday['credits_consumed'];
					$newJobToday->when_to_send = $jobsToday['when_to_send'];
					$newJobToday->message = $jobsToday['message'];
					$newJobToday->task_id = $jobsToday['task_id'];
					$newJobToday->parent_task_id = $jobsToday['parent_task_id'];
					$newJobToday->subaccount_id = $jobsToday['subaccount_id'];
					$newJobToday->client_id = $jobsToday['client_id'];
					$newJobToday->entry_id = $jobsToday['entry_id'];
					$newJobToday->zs_booking_id = $jobsToday['zs_booking_id'] ?? null;
					$newJobToday->source = $jobsToday['source'];

					$newJobToday->save();

					$this->jobsAuditLogsRepo->addLog(null, $newJobToday);
				}
			}
		}
	}

    public function validateData(Request $request, Task $task)
    {
        $rules = [];

        if ($task->task_type_id == \ATM\Repositories\Tasks\Task::ONE_TIME_SEND) {
            $rules = [
                'first_name'    => 'required|string',
                'last_name'     => 'required|string',
                'mobile'        => 'required|numeric',
                'date_time'     => 'required|date',
                'items'         => 'required'
            ];
        }

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

}
