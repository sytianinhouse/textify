<?php

namespace ATM\Repositories\Date;

use Carbon\Carbon;

class Date
{
	public static $months = [
		1 => "January",
		2 => "February",
		3 => "March",
		4 => "April",
		5 => "May",
		6 => "June",
		7 => "July",
		8 => "August",
		9 => "September",
		10 => "October",
		11 => "November",
		12 => "December"
	];

	public static function days() {
		$i = 1;
		$days = [];
		for ($x = $i; $x <= 31; $x++) {
			$days[] = $x;
		}

		return $days;
    }

    public static function years() {
        $now = Carbon::now();
        $i = $now->year;
        $years = [];
		for ($x = $i; $x >= 1920; $x--) {
			$years[] = $x;
		}

        return $years;
    }
}
