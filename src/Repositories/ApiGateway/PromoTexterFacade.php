<?php
/**
 * Created by PhpStorm.
 * User: jr
 * Date: 5/9/2019
 * Time: 4:26 PM
 */

namespace ATM\Repositories\ApiGateway;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Support\Facades\Log;

class PromoTexterFacade
{
	// const PASS_KEY = 'bf41b232938512d7dbe54017d63c0dec';
	// const SECRET_KEY = '1b261357ab690ed5bcf4d4a6fa6cef40';
	const PASS_KEY = 'o1-NCtRDj6FUaFJaXp98Osdtco7HE_';
	const SECRET_KEY = 'ptlLhsUD6nNuMTt8LD0A58J6YWb4grKl';
	const BASE_URL = 'https://api.promotexter.com';

	const CHECK_BALANCE = '/api/account/balance';
	const SINGLE_TRANSACTION = '/api/sms';
	const BATCH_TRANSACTION = '/api/batchsms';

	protected $guzzClient;

	public function __construct(
		Client $guzzClient
	)
	{
		$this->guzzClient = $guzzClient;
	}

	private function setRequestCreds($isPrio = false)
	{
		$default = [
			'apiKey' => self::PASS_KEY,
			'apiSecret' => self::SECRET_KEY
		];

		if ($isPrio) {
			$default['priority'] = 'priority';
		}

		return $default;
	}

	private function digestResponse($response)
	{
		$body = $response->getBody();
		$contents = $body->getContents();

		return $contents != '' ? json_decode($contents) : [];
	}

	/**
	 *  Response of request
	 * 	{ "availableBalance": 91.5, "accountBalance": -8.5, "creditLimit": 100, "withheld": 0 }
	 */
	public function disguiseCheckBalanceOfPromoTexterAccount()
	{
		$url = self::BASE_URL . self::CHECK_BALANCE;
		$response = $this->guzzClient->request('GET', $url, [
			'form_params' => $this->setRequestCreds()
		]);

		$code = $response->getStatusCode();

		if ($code == 200) {
			$body = $this->digestResponse($response);

			if ($body->availableBalance < 1) {
				$message = 'Promotexter account has zero balance.';
				Mail::to(env('ADMIN_EMAIL'))->send(new SMSCriticalError($message));
				return $this->throwException($message);
			}

			return $body;
		}

		return false;
	}

	/**
	 * @param $number
	 * @param $message
	 * @param string $senderName
	 * @return bool|void
	 * Sample Response
	 * 		[ id" => "a1c1d980-ca78-4939-b3b8-7ec6bcb49591", "unitCost" => 0.35, "transactionCost" => 0.35, "operatorCode" => "PHL_SMART"
	 *		"messageParts" => 1, "from" => "Kenneth", "to" => "639399078704", "source" => "122.52.195.190", "remaining" => 90.1]
	 */
	public function makeSingleTransaction($number, $message, $isPrio = false, $senderName = 'Textify', $options = [])
	{
		$url = self::BASE_URL . self::SINGLE_TRANSACTION;

		$params = [
			'from' => $senderName,
			'to' => $number,
			'text' => $message
		];
		$params = array_merge($params, $options);
		try {
			$response = $this->guzzClient->request('POST', $url, [
				'form_params' => array_merge($this->setRequestCreds($isPrio), $params)
			]);
            $code = $response->getStatusCode();

			if ($code == 200) {
				$body = (array)$this->digestResponse($response);

				if (isset($body['id'])) {
					return $body;
				}
			}
		} catch (ClientException $e) {
			$body = (array) $this->digestResponse($e->getResponse());
			Log::error($body);
			return $this->throwException(isset($body['message']) ? $body['message'] : 'undefined message from promo single transaction');
		} catch (ConnectException $e) {
            $message = 'Failed to connect to api.promotexter.com port 443: Network is unreachable (see http://curl.haxx.se/libcurl/c/libcurl-errors.html)';
            Log::error($message);
			return $this->throwException($message);
		} catch (ServerException $e) {
            $message = 'Promotexter resulted in a Bad Gateway.';
			Log::error($message);
			return $this->throwException($message);
        }

		return false;
	}

	/**
	 * @param $number
	 * @param $message
	 * @param bool $isPrio
	 * @param string $senderName
	 * @param array $options
	 * @return array|bool|void
	 * @throws GatewayException
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function makeSingleWithCallbackTransaction($number, $message, $isPrio = false, $senderName = 'Textify', $options = [])
	{
		$url = self::BASE_URL . self::SINGLE_TRANSACTION;

		$params = [
			'from' => $senderName,
			'to' => $number,
			'text' => $message
		];
		$params = array_merge($params, $options);

		try {
			$response = $this->guzzClient->request('POST', $url, [
				'form_params' => array_merge($this->setRequestCreds($isPrio), $params)
			]);

			$code = $response->getStatusCode();

			if ($code == 200) {
				$body = (array)$this->digestResponse($response);

				if (isset($body['id'])) {
					return $body;
				}
			}
		} catch (ClientException $e) {
			$body = (array) $this->digestResponse($e->getResponse());
			Log::error($body);
			return $this->throwException($body['message']);
		}

		return false;
	}

	/**
	 * @param $data
	 * @param string $senderName
	 */
	public function makeBatchTransaction($data, $isPrio = false, $senderName = 'Kenneth')
	{
		$url = self::BASE_URL . self::BATCH_TRANSACTION;

		$data = [
			['from' => $senderName, 'to' => '09399078704', 'text' => 'Hi Jr'],
			['from' => $senderName, 'to' => '09776236120', 'text' => 'Hi Mark'],
			['from' => $senderName, 'to' => '09397977393', 'text' => 'Hi Jessie']
		];
		$params = [
			'from' => 'Zensoft',
			'to' => null,
			'text' => null,
			'messages' => json_encode($data),
		];

		try {
			$response = $this->guzzClient->request('POST', $url, [
				'form_params' => array_merge($this->setRequestCreds($isPrio), $params)
			]);

			$code = $response->getStatusCode();

			if ($code == 200) {
				$body = (array) $this->digestResponse($response);

				if (isset($body['id'])) {
					return $body;
				}
			}
		} catch (ClientException $e) {
			$body = (array) $this->digestResponse($e->getResponse());

			return $this->throwException($body['message']);
		} catch (ServerException $e) {
			$body = (array) $this->digestResponse($e->getResponse());
			return $this->throwException($body['message']);
		}

		return false;
	}

	private function throwException($message = '')
	{
		throw new GatewayException($message);
	}
}
