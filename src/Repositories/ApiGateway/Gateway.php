<?php
/** Author: Jr Lalamoro */

namespace ATM\Repositories\ApiGateway;

use Globe\Connect\Oauth;

class Gateway
{
	protected $appId;
	protected $secret;

	protected $oauth;

	public function __construct($appId, $secret)
	{
		$this->appId = $appId;
		$this->secret= $secret;
	}

	public function getAppId()
	{
		return $this->appId;
	}

	public function getSecret()
	{
		return $this->secret;
	}

	/***
	 * Initialize
	 */
	public function initGlobeLabs()
	{
		$this->oauth = new Oauth($this->appId, $this->secret);

		if (!$this->oauth) {
			throw new GatewayException('Something went wrong while authentication API \'Globe Labs\'');
		}

		return $this->oauth;
	}
}