<?php
/** Author: Jr Lalamoro */

namespace ATM\Repositories\ApiGateway;

use App\Mail\SMSCriticalError;
use Globe\Connect\Sms;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class GlobeLabsFacade
{
	protected $gateway;
	protected $auth;
	protected $sender;
	protected $sms;

	public $isLive;
	public $address;
	public $message;
	public $accessToken;
	public $passPhrase;

	public $criticalStatus = [401, 403, 404, 502, 503];

	public function __construct()
	{
		$config = config('gateway.gateway.globelabs');
		$this->gateway = new Gateway($config['app_id'], $config['secret']);
		$this->sender = $config['sender'];
		$this->accessToken = $config['access_token'];
		$this->passPhrase = $config['passphrase'];
		$this->isLive = $config['is_live'];
		$this->start();
	}

	public function start()
	{
		try {
			$this->auth = $this->gateway->initGlobeLabs();
		} catch (GatewayException $e) {
			return $e->getMessage();
		}
	}

	public function getAccessToken($request)
	{
		$this->auth->setCode($request->code);
		$this->accessToken = $this->auth->getAccessToken();

		return $this->accessToken;
	}

	public function setSender($address, $message, $sender, $accessToken, $send = false)
	{
		$this->address = $address;
		$this->message = $message;
		$this->sender = $sender;
		$this->accessToken = $accessToken;
		$this->makeSms();

		if ($send) {
			return $this->prepareThenSendSms();
		}
	}

	public function prepareSms($number, $message)
	{
		if ($number) {
			$config = config('gateway.gateway.globelabs');
			$number = $number;

			// If live yung account no need tung token, getch?
			if (!$this->isLive) {
				$this->setSms();
				$this->accessToken = isset($config['tokens'][$number]) ? $config['tokens'][$number] : $config['access_token'];
			}

			$this->address = $number;
			$this->message = $message;

			return $this->prepareThenSendSms();
		}

		return false;
	}

	private function setSms()
	{
		$this->sms = new Sms($this->sender, $this->accessToken);

		return;
	}

	public function makeSms($sendImmediate = true)
	{
		if ($sendImmediate) {
			return $this->prepareThenSendSms();
		}

		return $this->sms;
	}

	private function prepareThenSendSms()
	{
		if ($this->address) {
			if ($this->isLive) {
				return $this->sendApiSms();
			} else {
				$this->prepare();
				return $this->sendSms();
			}
		}

		return $this->throwException('Address did not specify, please provide then try again');
	}

	public function prepare()
	{
		$this->sms->setReceiverAddress($this->address);
		$this->sms->setMessage($this->message ?: 'n/a');
	}

	private function sendSms()
	{
		$result = $this->sms->sendMessage();
		$decoded = json_decode($result);

		if (property_exists($decoded, 'error')) {
			return $this->throwException($decoded->error);
		}

		return $decoded;
	}

	private function sendApiSms()
	{
		try {
			$client = new Client();
			$config = config('gateway.gateway.globelabs');

			$endPoint = "https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/{$this->sender}/requests";
			$response = $client->request('POST', $endPoint, [
				'form_params' => [
					'app_id' => $config['app_id'],
					'app_secret' => $config['secret'],
					'message' => $this->message,
					'address' => $this->address,
					'passphrase' => $config['passphrase'],
				]
			]);

			$code = $response->getStatusCode();
			if ($code == 201) {
				return true;
			}

			return $this->throwException('Something went wrong, while sending the SMS.');
		} catch (\Exception $e) {
			$response = $e->getResponse();
			$body = $response->getBody();
			$statusCode = $response->getStatusCode();
			$error = json_decode($body->getContents(), true);
			$message = isset($error['error']) ? $error['error'] : implode(',', $error);

			if (in_array($statusCode, $this->criticalStatus)) {
				// Send email about this critical error
				Mail::to(env('ADMIN_EMAIL'))->send(new SMSCriticalError($statusCode, $message));
			}

			return $this->throwException("------------------ SMS sending failed for number '{$this->sender}' with an error message :'{$message}' ---------------------");
		}
	}

	private function throwException($message = '')
	{
		throw new GatewayException($message);
	}
}
