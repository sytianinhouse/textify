<?php 

namespace ATM\Repositories\Client;

use ATM\Repositories\BaseRepository;
use ATM\Repositories\Client\Client;
use Illuminate\Http\Request;

class ClientRepository extends BaseRepository {

	/*
	 * @var Illuminate\Database\Eloquent\Model
	 */
	protected $model;

	public function __construct(Client $client) 
	{
		$this->model = $client;
	}

	public function search(Request $request, $perPage = self::DISPLAY_ALL, $with = [])
	{
		
		$query = $this->model->with($with);

		if ($request->filled('project_id')) {
			$query->where('project_id', $request->project_id);
		}	

		if ($request->filled('client_id')) {
			$query->where('id', $request->client_id);
		}	

        if ($perPage == self::DISPLAY_ALL) {
            $results = $query->get();
        } else {
            $results = $query->paginate($perPage);
		}

		return $results;
	}

	public function generateUniqueCuid() 
	{
		do {
			$cuid = str_random(6);
		} while ($this->getFirstBy('cuid', $cuid));

		return $cuid;
	}

	public function validateCuid($cuid, Client $client = null)
	{
		if (strlen($cuid) < 4) {
			$this->throwValidationException([
				'cuid' => 'Client ID must be at least 4 characters.',
			]);
		}

		$query = $this->model->where('cuid', $cuid);

		if ($client) {
			$query = $query->where('id', '!=', $client->id);
		}

		if ($query->first()) {
			$this->throwValidationException([
				'cuid' => 'Already been taken',
			]);
		}

		return true;
	}

	public function add()
	{
		// 
	}

	public function update(Client $client)
	{
		// 
	}

}
