<?php

namespace ATM\Repositories\Client;

use ATM\ContextInterface;
use ATM\ContextScope;
use ATM\Repositories\ApiKey\ApiKey;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Cities\City;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Roles\Role;
use ATM\Repositories\SenderIds\SenderIds;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Uploadable\Uploadable;
use App\Model\BaseModel;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Watson\Validating\ValidatingTrait;

class Client extends BaseModel
{
	// use ValidatingTrait, SoftDeletes;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'company_name', 'cuid', 'address_1', 'address_2', 'city', 'country', 'zip', 'website', 'phone', 'mobile',
        'contact_first_name', 'contact_last_name', 'contact_email', 'contact_mobile', 'contact_telephone',
    ];

    protected $rules = [
        'company_name' => 'required',
        'cuid' => 'required|min:4|max:25|unique:clients,cuid',
        'city' => 'required',
        'address_1' => 'required',
        'contact_first_name' => 'required',
        'contact_mobile' => 'required',
        'contact_email' => 'email|nullable',
    ];

    // Rule::unique('clients')->where(function ($query) {
    //     return $query->where('project_id', $this->project->id);
    // })

    const IMG_ICON = 'icon';
    const IMG_THUMB = 'thumb';
    const IMG_BANNER = 'banner';

    const IS_BEECHOO = 70;

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function senderids()
    {
        return $this->hasMany(SenderIds::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function subAccounts()
    {
        return $this->hasMany(SubAccount::class, 'client_id');
    }

	public function campaigns()
	{
		return $this->hasManyThrough(Campaign::class,SubAccount::class, 'client_id', 'subaccount_id');
    }

    public function apis()
    {
        return $this->hasMany(ApiKey::class);
    }

    public function roles()
    {
        return $this->hasMany(Role::class);
    }

	public function customers()
	{
		return $this->hasMany(Customer::class, 'client_id');
	}

	/*public function customers()
	{
		return $this->hasManyThrough(Customer::class, SubAccount::class, 'client_id', 'subaccount_id');
    }*/

	public function cityObj()
	{
		return $this->belongsTo(City::class, 'city');
	}

    public function customerGroups()
    {
        return $this->hasMany(CustomerGroup::class);
    }

    public function uploads()
    {
        return $this->morphMany(Uploadable::class, 'uploadable');
    }

    // ----------------------------------
    //    Getter
    // ----------------------------------

    public function getContactNameattribute()
    {
        return $this->contact_first_name . ' ' . $this->contact_last_name;
    }

    public function hasSubAccounts() {
        return $this->subAccounts->count() > 0 ?: false;
    }

    public function getImgIconAttribute() //img_icon
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_ICON ? true : false;
        })->first();

        return $uploads;
    }


    public function getImgIconPathAttribute() // imc_icon_path
    {
        return $this->img_icon ? $this->img_icon->path : Uploadable::IMG_PLACEHOLDER;
    }

    public function getImgThumbAttribute()
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_THUMB ? true : false;
        })->first();

        return $uploads;
    }

    public function getImgThumbPathAttribute()
    {
        return $this->img_thumb ? $this->img_thumb->path : Uploadable::IMG_PLACEHOLDER;
    }

    public function getImgBannerAttribute()
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_BANNER ? true : false;
        })->first();

        return $uploads;
    }

    public function getImgBannerPathAttribute()
    {
        return $this->img_banner ? $this->img_banner->path : Uploadable::IMG_PLACEHOLDER;
    }

	// ----------------------------------
    //    Scope Queries
    // ----------------------------------

    public function scopeActives($query)
    {
        return $query->where('active', true);
    }

    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            //return $query->where($context->column(), $context->id());
            return $query->whereHas('subAccounts', function($q) use($context){
                return $q->where($context->column(), $context->id());
            });
        } else if ($context->getInstance() instanceof self) {
            return $query->where('id', $context->id());
        }

        return $query;
    }

	// ----------------------------------
	//    Helpers
	// ----------------------------------
	public function isMultiSubaccounts()
	{
		return $this->subAccounts->count() > 1 ? true : false;
	}
}

?>
