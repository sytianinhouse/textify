<?php

namespace ATM\Repositories\SenderIds;

use ATM\ContextInterface;
use ATM\ContextScope;
use ATM\Repositories\Client\Client;
use ATM\Repositories\SubAccount\SubAccount;
use App\Model\BaseModel;
use Watson\Validating\ValidatingTrait;

class SenderIds extends BaseModel
{
	const DEFAULT_SENDERID = 'Textify';

    use ValidatingTrait;

	protected $rules = [
        'name' => 'required'
    ];

    protected $fillable = [
        'name'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function subaccount()
    {
        return $this->hasMany(SubAccount::class,'sender_id');
    }
}
?>
