<?php

namespace ATM\Repositories\SenderIds;

use ATM\ContextInterface;
use ATM\Repositories\Client\Client;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\SenderIds\SenderIds;
use ATM\UuidGenerator;
use ATM\Validation\ValidationException;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class SenderIdsRepository extends ContextRepository {

	protected $model;

	public function __construct(
		SenderIds $senderIds,
        ContextInterface $context
    )
	{
		$this->model = $senderIds;
		$this->context = $context;
	}

	public function store(SenderIds $senderIds = null, Request $request)
	{
		$senderIds = $this->model;

		$senderIds->fill($request->input());
		$senderIds->client_id = $request->client_id;

		$this->validate($senderIds);

		$senderIds->save();

		return $senderIds;
	}

	public function search(Request $request, $with = [], $displayAll = false)
	{
		$query = $this->scope($with);

		if ($request->has('client_id')) {
			$query = $query->where('client_id', $request->client_id);
		}

		return $query->get();
;
	}

	public function update(SenderIds $senderids, Request $request)
	{
		$senderids->fill($request->input());
		$this->validate($senderids);

		$senderids->save();

		return $senderids;
	}

	public function delete( $senderids )
	{
		$senderids = $senderids->name;
		$senderids->delete();
		return $senderids;
	}
}
