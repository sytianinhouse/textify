<?php

namespace ATM\Repositories\Shortcodes;

class Shortcodes
{
	public static $shortCodes = [
		'first_name' => 'first_name',
		'last_name' => 'last_name',
		'email' => 'email',
		'number' => 'number',
		'address' => 'address',
	];

	public static $oneTimeshortCodes = [
		'first_name' => 'first_name',
		'last_name' => 'last_name',
		'items' => 'items',
		'total_amount' => 'total_amount',
    ];

	public static $bookingShortcodes = [
		'first_name' => 'first_name',
		'last_name' => 'last_name',
		'items' => 'items',
		'number' => 'number',
		'date' => 'date',
		'time' => 'time',
        // 'date_time' => 'date_time',
        'business_phone_number' => 'business_phone_number',
        'business_name' => 'business_name',
	];
}
