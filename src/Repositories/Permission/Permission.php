<?php

namespace ATM\Repositories\Permission;

use ATM\Repositories\Roles\Role;
use App\Model\BaseModel;

class Permission extends BaseModel
{
    protected $table = 'permissions';

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_permissions', 'permission_id', 'role_id');
    }

    public function getThisPermission($name)
    {
        if(is_array($name)){
            return $this->whereIn('key', $name)
                        ->get();
        }

        return $this->where('name', $name)
                    ->first();
	}
}
