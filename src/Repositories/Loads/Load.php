<?php

namespace ATM\Repositories\Loads;

use ATM\ContextScope;
use ATM\Repositories\Client\Client;
use ATM\Repositories\SubAccount\SubAccount;
use App\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Load extends BaseModel
{

	use ValidatingTrait;
	use SoftDeletes;

	protected $rules = [
		'subaccount_id' => 'required|numeric',
		'date'          => 'required|date',
		'load_amount'   => 'required|numeric',
	];

	protected $fillable = ['load_amount', 'notes'];

	protected $dates = ['date', 'deleted_at', 'created_at', 'updated_at'];

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function subAccount()
	{
		return $this->belongsTo(SubAccount::class, 'subaccount_id');
	}
}
