<?php

namespace ATM\Repositories\Loads;

use ATM\ContextInterface;
use ATM\Repositories\BaseRepository;
use ATM\Repositories\Credits\CreditRepository;
use ATM\Repositories\Loads\Load;
use ATM\Repositories\Settings\SiteSettingsRepository;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use ATM\Repositories\SubAccount\SubAccountSetting;
use ATM\Validation\ValidationException;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

/**
 * Off context on other parts of query
 */
class LoadRepository extends BaseRepository {

	protected $model;

	protected $throwValidationException = false;

	public function __construct(
		Load $load,
		SubAccountRepository $subRepo,
		CreditRepository $creditRepo,
		SiteSettingsRepository $settingsRepo
	)
	{
		$this->model = $load;
		$this->subRepo = $subRepo;
		$this->creditRepo = $creditRepo;
		$this->settingsRepo = $settingsRepo;
	}

	public function search(Request $request, $perPage = null, $with = [])
	{

		$query = $this->model->with($with)->orderByDesc('created_at');

		if ($request->filled('project_id')) {
			$query = $query->whereHas('client', function($query) use ($request) {

				if ($request->filled('client_id')) {
					$query->where('id', $request->client_id);
				}

				$query->whereHas('project', function($q) use ($request) {
					$q->where('id', $request->project_id);
				});
			});
		}

		if ($request->filled('sub_account_id')) {
			$query = $query->where('subaccount_id', $request->sub_account_id);
		}

		if ($request->filled('subaccount_id')) {
			$query = $query->where('subaccount_id', $request->subaccount_id);
		}

		if ($request->filled('from_date') && $request->filled('to_date')) {

			$fromDateString = Carbon::parse($request->from_date)->toDateString();
			$toDateString = Carbon::parse($request->to_date)->toDateString();

			$query = $query->whereDate('date', '>=' , $fromDateString)
							->whereDate('date', '<=', $toDateString);
		}

        if ($perPage) {
			$results = $query->paginate($perPage ?: self::PER_PAGE);
        } else {
			$results = $query->get();
		}

		return $results;
	}

	public function add(Request $request)
	{
		$load = $this->model;

		$settings = $this->settingsRepo->getSettings();

        // validate user datails, role, username and password
        // then collate all the errors if any

		$this->validateLoadData($request);
		if (count($this->errors) > 0) {
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		$subAccount = $this->subRepo->find($request->sub_account, ['client']);

		$client = $subAccount->client;

		$load->fill($request->input());
		$load->date = Carbon::parse($request->date)->toDateString();
		$load->cost = (float) ($subAccount->getSetting(SubAccountSetting::SMS_RATE_PER_TEXT) ?: SubAccount::DEF_CONVERSION_RATE);
		$load->client()->associate($client);
		$load->subAccount()->associate($subAccount);
		$load->save();

		$credit = $this->creditRepo->add($load, $request);

		return $load;
	}

	public function update(Load $load, Request $request)
	{
		abort(404);

		// $this->validateLoadData($request);

		// if (count($this->errors) > 0) {
		// 	throw new \ATM\Validation\ValidationException($this->errors);
		// }

		// $subAccount = $this->subRepo->find($request->sub_account, ['client']);
		// $client = $subAccount->client;
		// $previousDetails = [
		// 	'previousAmount' => $load->load_amount,
		// 	'type' => $load->load_amount > $request->load_amount ? 'sub' : 'add'
		// ];

		// $load->fill($request->input());
		// $load->date = Carbon::parse($request->date)->toDateString();
		// $load->client()->associate($client);
		// $load->subAccount()->associate($subAccount);
		// $load->update();

		// $credit = $this->creditRepo->update($previousDetails, $load, $request);

		// return $load;
	}

	public function validateLoadData(Request $request)
	{
		$rules = [
			'project'       => 'required',
			'client'        => 'required',
			'sub_account'   => 'required',
			'sub_account' => 'required|numeric',
			'date' => 'required|date',
			'load_amount' => 'required|numeric',
		];

		$validator = \Validator::make($request->all(), $rules);

		if ($validator->fails()) {
		    $this->errors = $validator->messages()->merge($this->errors);
		}
	}

	public function delete(Load $load)
	{
		// $this->creditRepo->sub($load);
		$load->delete();

		return true;
	}

}
