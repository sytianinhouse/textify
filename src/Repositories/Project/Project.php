<?php

namespace ATM\Repositories\Project;

use App\User;
use ATM\ContextScope;
use App\Model\BaseModel;
use ATM\ContextInterface;
use ATM\Repositories\Client\Client;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Uploadable\Uploadable;

class Project extends BaseModel implements ContextScope {

	const IMG_LOGO = 'logo';

	use ValidatingTrait;

	protected $rules = [
		'name' => 'required',
		'description' => 'nullable',
	];

	protected $fillable = ['name', 'description', 'project_url', 'active'];

	public function clients()
	{
		return $this->hasMany(Client::class, 'project_id');
	}

	public function uploads()
    {
        return $this->morphMany(Uploadable::class, 'uploadable');
    }

    public function getImgLogoAttribute() // img_icon
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_LOGO ? true : false;
        })->first();

        return $uploads;
    }

    public function getImgLogoPathAttribute() // img_icon_path
    {
        return $this->IMG_LOGO ? $this->img_logo->path : Uploadable::IMG_PLACEHOLDER;
    }

	public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->where($context->column(), $context->id());
        } else if ($context->getInstance() instanceof SubAccount) {
            return $query->whereHas('client.s', function($q) use ($context) {
                return $q->where('id', $context->id());
            });
        }

        return $query;
    }
}

?>