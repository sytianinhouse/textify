<?php

namespace ATM\Repositories\Project;

use ATM\ContextInterface;
use ATM\Repositories\Project\Project;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\Uploadable\UploadableRepository;
use ATM\Validation\ValidationException;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;


/**
 * Off context on other parts of query
 */
class ProjectRepository extends ContextRepository {
	
	protected $model;

	protected $throwValidationException = false;

	public function __construct(
		Project $project,
		UploadableRepository $uploadRepo,
		ContextInterface $context)
	{
		$this->model = $project;
		$this->uploadRepo = $uploadRepo;
		$this->context = $context;
	}

	public function search(Request $request, $perPage = self::PER_PAGE, $with = [])
	{
		
		$query = $this->model->with($with);

		if ($request->filled('project_id')) {
			$query->where('id', $request->project_id);
		}	

        if ($perPage == self::DISPLAY_ALL) {
            $results = $query->get();
        } else {
            $results = $query->paginate($perPage);
		}

		return $results;
	}

	public function addProject(Project $project = null, Request $request)
	{
		$project = $this->model;

        // validate user datails, role, username and password
        // then collate all the errors if any
		$this->validateProjectData($request);
		
		if (count($this->errors) > 0) {
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		$project->fill($request->input());
		$project->project_url = str_slug($request->name, '-');
        $project->active = $request->has('active');
		$project->save();

		$this->uploadMedia($request, $project);

		return $project;
	}

	public function updateProject(Project $project, Request $request)
	{
		// validate user datails, role, username and password
        // then collate all the errors if any

		//dd( $request );

		$this->validateProjectData($request);

		if (count($this->errors) > 0) {
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		$project->fill($request->input());
		$project->project_url = str_slug($request->name, '-');
        $project->active = $request->has('active');
		$project->save();

		$this->uploadMedia($request, $project);

		return $project;
	}

	private function uploadMedia(Request $request, $project)
    {
        if ($request->hasFile('icon')) {
            $this->uploadRepo->createUpload($project, $request, 'image', [
                'key' => Project::IMG_LOGO,
                'field_key' => 'icon',
                'path' => 'uploads/project',
                'filename' => 'icon',
                // 'width' => 50,
                // 'height' => 50
            ]);
        }

        if ($request->hasFile('thumb')) {
            $this->uploadRepo->createUpload($project, $request, 'image', [
                'key' => Project::IMG_LOGO,
                'field_key' => 'thumb',
                'path' => 'uploads/project',
                'filename' => 'thumb',
                // 'width' => 358,
                // 'height' => 182
            ]);
        }

        if ($request->hasFile('banner')) {
            $this->uploadRepo->createUpload($project, $request, 'image', [
                'key' => Project::IMG_LOGO,
                'field_key' => 'banner',
                'path' => 'uploads/project',
                'filename' => 'banner',
                // 'width' => 1366,
                // 'height' => 345
            ]);
        }
    }

	public function validateProjectData(Request $request)
	{
		$validator = \Validator::make($request->all(),
		    [
		    	'name' => 'required'
		    ]
		);

		if ($validator->fails()) {
		    $this->errors = $validator->messages()->merge($this->errors);
		}
	}

	public function ofProject(Project $project)
	{
		return $this->model
					->where('id', $project->id)
					->get();
	}

	public function delete(Project $project)
	{

		$name = $project->name;
		$project->uploads()->where('uploadable_id', '=', $project->id)->delete();
		$project->delete();

		return $name;
	}

}