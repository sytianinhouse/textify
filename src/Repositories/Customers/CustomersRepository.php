<?php

namespace ATM\Repositories\Customers;

use ATM\ContextInterface;
use ATM\Repositories\Client\Client;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\Customers\Customer;
use ATM\Validation\ExcelException;

use ATM\Validation\ValidationException;
use App\Jobs\CustomersExcelSeeder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CustomersRepository extends ContextRepository {

	public $excel;
    protected $model;

    const PER_PAGE = 20;

	public function __construct(Customer $customer, ContextInterface $context)
	{
		$this->model = $customer;
		$this->context = $context;
		$this->excel = App::make('excel');
	}

	public function search(Request $request, $with = [], $pagination = null)
	{
        $query = $this->scope($with);

		if ($request->filled('sub_account_id')) {
			$query = $query->where('subaccount_id', $request->sub_account_id);
        }

		if ($request->filled('subaccount_filter')) {
			$query = $query->where('subaccount_id', $request->subaccount_filter);
        }

        if ($request->filled('mobile')) {
			$query = $query->where('mobile', 'LIKE', "%{$request->mobile}%");
		}

		if ($request->filled('filter_search')) {
			$strings = explode(' ', $request->filter_search);
			foreach ($strings as $string) {
				$query = $query->where(function($q) use ($string) {
					$q->where('first_name', 'LIKE', "%{$string}%")
						->orWhere('last_name', 'LIKE', "%{$string}%")
						->orWhereRaw('CONCAT(first_name, " ", last_name) LIKE ? ', '%' . $string . '%');
				});
			}
		}

		if ($request->filled('guid')) {
			$query = $query->where('guid', $request->guid);
		}

		if($request->filled('number')) {
			$query = $query->where('mobile', 'LIKE', "%{$request->number}%");
		}

		if ($request->filled('filter_group')) {
			$query = $query->whereHas('customerGroups', function ($q) use ($request) {
				$q->where('customer_group_id', $request->filter_group);
			});
		}

		if ($request->filled('filter_gender')) {
			$query = $query->where('gender', $request->filter_gender);
		}

		if ($request->filled('filter_city')) {
			$query = $query->where('city', $request->filter_city);
		}

		if($request->filled('not_empty_mobile')) {
			$query = $query->where('mobile', '!=', null);
		}

		 if ( $request->filled('per_page') && $request->per_page != self::DISPLAY_ALL) {
		 	return $query->paginate($request->per_page ? $request->per_page : self::PER_PAGE);
		 } else {
		 	return $query->get();
         }
	}

	public function getRecipientsList($request)
	{
		$results = $this->search($request);

		$recipients = $results->pluck('first_name', 'id');

		return $recipients;
	}

	public function add(Request $request, Client $client = null)
	{
        $request['mobile'] = formattedNumber($request->mobile, true);
        $request['sub_account_id'] = $request->sub_accounts;

        $validateMobile = $this->search($request);

        if( $validateMobile->count() == 0 ) {
            $customer 					= $this->model;
            $customer->first_name 		= ucfirst($request->first_name);
            $customer->last_name 		= ucfirst($request->last_name);
            $customer->email 			= $request->email;
            $customer->mobile			= $request->mobile ? formattedNumber($request->mobile, true) : NULL;
            $customer->telephone 		= $request->telephone;
            $customer->gender 			= $request->gender;
            $customer->b_day 			= isset($request->birthdate)?date('d', strtotime($request->birthdate)): NULL;
            $customer->b_month 			= isset($request->birthdate)?date('m', strtotime($request->birthdate)): NULL;
            $customer->b_year 			= isset($request->birthdate)?date('Y', strtotime($request->birthdate)): NULL;
            $customer->occupation 		= $request->occupation;
            $customer->address_1 		= $request->address_1;
            $customer->address_2 		= $request->address_2;
            $customer->city 			= $request->city;
            $customer->postal_code 		= $request->postal_code;
            $customer->country 			= $request->country;
            $customer->notes 			= $request->notes;
            $customer->client_id 		= $client->id;
            $customer->subaccount_id 	= $request->sub_accounts;

            if ($customer->save()) {
                $customer->customerGroups()->sync($request->customer_group);
            }
            return $customer;
        } else {
            return false;
        }
	}

	public function update(Customer $customer, Request $request)
	{
		$customer->first_name 		= $request->first_name ?: $customer->first_name;
		$customer->last_name 		= $request->last_name ?: $customer->last_name;
		$customer->email 			= $request->email ?: $customer->email;
		$customer->mobile			= $request->mobile ? formattedNumber($request->mobile, true) : $customer->mobile;
		$customer->telephone 		= $request->telephone ?: $customer->telephone;
        $customer->gender 			= $request->gender ?: $customer->gender;

		if ($request->filled('birthdate')) {
			$birthdate					= new Carbon($request->birthdate);
			$customer->b_day 			= $birthdate ? $birthdate->format('d') : NULL;
			$customer->b_month 			= $birthdate ? $birthdate->format('m') : NULL;
			$customer->b_year 			= $birthdate ? $birthdate->format('Y') : NULL;
		}

		$customer->occupation 		= $request->occupation ?: $customer->occupation;
		$customer->address_1 		= $request->address_1 ?: $customer->address_1;
		$customer->address_2 		= $request->address_2 ?: $customer->address_2;
		$customer->city 			= $request->city ?: $customer->city;
		$customer->postal_code 		= $request->postal_code ?: $customer->postal_code;
		$customer->country 			= $request->country ?: $customer->country;
		$customer->notes 			= $request->notes ?: $customer->notes;
		$customer->client_id 	    = $request->client_id ?: $customer->client_id;
		$customer->subaccount_id 	= $request->sub_accounts ?: $customer->subaccount_id;

		if ($request->filled('customer_group')) {
			$customer->customerGroups()->sync($request->customer_group);
		}

   		$customer->save();

		return $customer;
	}

	public function delete(Customer $customer)
	{

		$name = $customer->first_name;
		$customer->delete();

		$customer->customerGroups()->detach();

		return $name;
	}

	public function import(Request $request)
	{
		ini_set('max_execution_time', '3000');

		$excel = $request->file('file');
		$this->excel->selectSheets('Customers')->load($excel, function($reader) use($request) {
			try {
				dispatch(new CustomersExcelSeeder($reader->get(), $request));
			} catch (ExcelException $e) {
				throw new \ATM\Validation\ValidationException($e->getErrors());
			}
		});

		return;
	}
}
