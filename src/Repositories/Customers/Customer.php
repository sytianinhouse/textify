<?php

namespace ATM\Repositories\Customers;

use ATM\ContextInterface;
use ATM\ContextScope;
use ATM\Repositories\Cities\City;
use ATM\Repositories\Client\Client;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Tasks\Task;
use App\Model\BaseModel;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class Customer extends BaseModel implements ContextScope {

    use ValidatingTrait;

	protected $casts = [
	    'other_data' => 'array',
	];

	protected $rules = [
		'first_name' 	=> 'required',
        //'mobile' 		=> 'required',
		'subaccount_id' => 'required',
	];

	protected $fillable = [
		'first_name',
		'last_name',
		'email',
		'mobile',
		'telephone',
		'gender',
		'birthdate',
		'occupation',
		'address_1',
		'address_2',
		'city',
		'postal_code',
		'country',
		'notes',
		'subaccount_id',
		//'group_id'
	  ];

	protected $appends = [
		'name'
	];

	protected $table = 'customers';

	public function clients()
	{
		return $this->belongsTo(Client::class);
	}

	// @deprecated use subAccount instead
	public function subAccounts()
    {
        return $this->belongsTo(SubAccount::class, 'subaccount_id');
    }

	public function subAccount()
	{
		return $this->belongsTo(SubAccount::class, 'subaccount_id');
	}

    public function customerGroups() {
        return $this->belongsToMany(CustomerGroup::class, 'customer_group_customers', 'customer_id', 'customer_group_id');
    }

    public function taskCustomers() {
        return $this->belongsToMany(Task::class, 'task_customers', 'customer_id', 'task_id');
    }

    public function cityRel()
	{
		return $this->belongsTo(City::class, 'city');
	}

	public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /*
     * Mutators
     */

	public function getBirthDateAttribute()
	{
		$now = Carbon::now();
		$year = $this->b_year ?: $now->format('Y');
		if (!$this->b_day && $this->b_month) {
			return new Carbon("{$year}-{$this->b_month}-{$now->format('d')}");
		}

		if ($this->b_day && $this->b_month && $year) {
			return new Carbon("{$year}-{$this->b_month}-{$this->b_day}");
		}

		return null;
    }

    public function getBirthdateFormAttribute()
    {
    	if (!$this->b_year || !$this->b_month || !$this->b_day) {
    		return null;
    	}
        return strftime('%F', strtotime($this->b_year.'-'.$this->b_month.'-'.$this->b_day));
    }

	public function getBirthDateFormattedAttribute()
	{
		$birthday = null;
		if ($this->b_month && $this->b_day && $this->b_year) {
            $birthday = date("M d, Y",mktime(0, 0, 0, $this->b_month, $this->b_day, $this->b_year));
        }

		return $birthday;
	}

    public function getFullNameAttribute($value)
    {
       return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

	public function getNameAttribute($value)
	{
		return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
	}

    public function getCustomerNumberAttribute($value)
    {
       return $this->mobile ? $this->mobile : $this->telephone;
    }

	// Query Scopes
	public function scopeWithNumbers($query)
	{
		return $query->whereNotNull('mobile');
	}

	public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->whereHas('subAccounts', function($q) use($context){
            	return $q->where($context->column(), $context->id());
            });
        } else if ($context->getInstance() instanceof SubAccount) {
            return $query->where($context->column(), $context->id());
        }

        return $query;
    }

}
