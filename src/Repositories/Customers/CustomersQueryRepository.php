<?php

namespace ATM\Repositories\Customers;

use ATM\ContextInterface;
use ATM\Repositories\BaseRepository;
use ATM\StoredProcedure\Customer\CustomerProcedure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class CustomersQueryRepository extends BaseRepository
{
	public $excel;
	protected $model;

	public function __construct(
		Customer $customer,
		ContextInterface $context,
		CustomerProcedure $customerProcedure)
	{
		$this->model = $customer;
		$this->context = $context;
		$this->customerProcedure = $customerProcedure;
		$this->excel = App::make('excel');
	}

	public function search(Request $request, $useStored, $columns = '*', $with = [], $perPage = false, $offset = false)
	{
		if ($useStored) {
			return $this->searchUseStored($request, $perPage, $offset);
		}
	}

	public function getRecipientsList(Request $request, $usesStored, $includeMobile = false )
	{
		$results = $this->search($request, $usesStored);
		if(isset($includeMobile)) {
			$recipients = [];
			$results->each(function($item) use(&$recipients) {
				$name = "{$item->first_name} - {$item->mobile}";
				$recipients[$item->id] = $name;
			});
		} else {
			$recipients = $results->pluck('first_name', 'id');
		}
		return $recipients;
    }

    public function getRecipientsListFormatted(Request $request, $usesStored, $includeMobile = false, $offset = false )
	{
        $request['not_empty_mobile'] = $includeMobile ? $includeMobile : false;
        if(isset($offset)) {
            $results = $this->search($request, $usesStored, '*', [], false, $offset);
        } else {
            $results = $this->search($request, $usesStored, '*', [], true, false);
        }
		if(isset($includeMobile)) {
			$recipients = [];
			$results->each(function($item) use(&$recipients) {
                $name =  preg_replace('/\s*/', '', $item->first_name);
                $name =  ucwords(mb_strtolower($name));

				$lastName =  preg_replace('/\s*/', '', $item->last_name);
                $lastName =  ucwords(mb_strtolower($lastName));
                $name = "{$name} {$lastName} - {$item->mobile}";
                // if( $item->mobile != '') {
                    $recipients[] = [
                        'label' => $name,
                        'value' => $item->id,
                    ];
                // }
			});
		} else {
            $recipients = $results->pluck('first_name', 'id');
        }

		// if (!isClientNailsGlow() && !isClientLuxeEscape()) {
			$recipients = array_values(array_sort($recipients, function($val) {
				return $val['label'];
			}));
		// }

        return $recipients;
	}

	public function searchUseStored(Request $request, $perPage = false, $offset = false)
	{
		// if (isClientNailsGlow() || isClientLuxeEscape()) {
			$customers = $this->customerProcedure->searchCustomersRandomized($request);
		// } else {
		// 	$customers = $this->customerProcedure->searchCustomers($request);
		// }

        $customers = collect($customers);

		if ($perPage) {
            return $this->paginateCollection($customers, $request->page, $request->per_page ?: self::PER_PAGE);
        } elseif( isset($offset) && $offset != false ) {
            return $customers->forPage($request['offset'], $request['per_page']);
        } else {
			return $customers;
		}
	}
}
