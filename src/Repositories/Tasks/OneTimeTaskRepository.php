<?php

namespace ATM\Repositories\Tasks;

use ATM\ContextInterface;
use ATM\Repositories\BaseRepository;
use ATM\Repositories\Client\Client;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\Credits\Credit;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Settings\SiteSettings;
use ATM\Repositories\Settings\SiteSettingsRepository;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\TaskJobs\AuditLog\JobsAuditLogRepository;
use ATM\Repositories\TaskJobs\TaskJobsRepository;
use ATM\Repositories\Tasks\JobsFailed\JobsFailedRepository;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Tasks\TaskRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;

class OneTimeTaskRepository extends ContextRepository {

	/*
	 * @var Illuminate\Database\Eloquent\Model
	 */
	protected $model;

	public function __construct(
		Task $task,
		TaskJobsRepository $taskJobRepo,
		SiteSettingsRepository $settingsRepo,
		CustomersRepository $customerRepo,
		ContextInterface $context,
		MessageBag $messageBag,
		JobsAuditLogRepository $jobsAuditLogsRepo,
        JobsFailedRepository $jobFailedRepo,
        TaskRepository $taskRepository
	)
	{
		$this->model = $task;
		$this->context = $context;
		$this->taskJobRepo = $taskJobRepo;
		$this->settingsRepo = $settingsRepo;
		$this->customerRepo = $customerRepo;
		$this->messageBag = $messageBag;
		$this->jobsAuditLogsRepo = $jobsAuditLogsRepo;
        $this->jobFailedRepo = $jobFailedRepo;
        $this->taskRepository = $taskRepository;
	}

	public function search(Request $request, $with = [])
	{
		$query = $this->scope($with);

		if ($request->filled('sub_account_id')) {
			$query = $query->whereHas('subAccounts', function($q) use ($request) {
				$q->where('subaccount_id', $request->sub_account_id);
			});
		}

		if ($request->filled('filter_task_type_id')) {
			$query = $query->where('task_type_id', $request->filter_task_type_id);
		}

		if ($request->filled('campaign_filter')) {
			$query = $query->where('campaign_id', $request->campaign_filter);
		}

		if ($request->filled('subaccount_filter')) {
			$query = $query->where('subaccount_id', $request->subaccount_filter);
		}

		if ( $request->filled('filter_date_by') ) {
			$fromDate = new Carbon($request->from_date);
			$toDate = new Carbon($request->to_date);

			if ($request->filter_date_by == 'created_at') {
				if ( $request->filled('from_date') && $request->filled('to_date') ) {
					$query = $query->whereBetween('created_at', [$fromDate->startOfDay()->toDateTimeString(),$toDate->endOfDay()->toDateTimeString()]);
				}
			} else {
				if ( $request->filled('from_date') && $request->filled('to_date') ) {
					$query = $query->whereBetween('date_to_send', [$fromDate->startOfDay()->toDateTimeString(),$toDate->endOfDay()->toDateTimeString()]);
				}
			}
		}

		if ($request->has('per_page') && $request->per_page != self::DISPLAY_ALL ) {
			return $query->paginate($request->per_page);
		} else {
			return $query->get();
		}
	}

	public function makeSmsFormatted($task, $customer)
	{
		$message = str_replace(
			['[first_name]', '[last_name]', '[email]', '[telephone]', '[address]'],
			[$customer->first_name, $customer->last_name, $customer->email, $customer->telephone, $customer->address_1],
			$task->message
		);

		return $message;
	}

	public function creditComputation($message)
	{
		$settings = SiteSettings::getSettings();
		$limit = $settings->sms_character_limit ?: Task::CHARACTER_LIMITER;
		$rate = $settings->sms_credit_pricec_per_limit ?: Task::CREDIT_LIMITER;

		$length = strlen( $message );
		if ( $length > 0 ) {
			if ( $length > $limit ) {
				return number_format(($length / $limit) * $rate, 2);
			} else {
				return number_format($rate);
			}
		}
	}

	public function validateData(Request $request)
    {
    	$rules = [];

        if ($request->task_type_id == \ATM\Repositories\Tasks\Task::ONE_TIME_SEND) {
            $rules = [
                'campaign'     => 'required',
                'time_to_send' => 'required',
                'message'      => 'required'
            ];
        }

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

	public function add(Request $request, Client $client)
	{
		$task = $this->model;
		$now = Carbon::now();

		$this->validateData($request);

        if (count($this->errors) > 0) {
            throw new \ATM\Validation\ValidationException($this->errors);
        }

        if ($request->filled('subaccount_id')) {
        	$reqSubAccount = $request->subaccount_id;
        }

        if ($request->filled('sub_account_id')) {
        	$reqSubAccount = $request->sub_account_id;
        }

        $subAccount = $client->subAccounts()->find($reqSubAccount);

		$task->client_id = $client->id;
		$task->subaccount_id = $reqSubAccount;
		$task->entry_id = null;
		$task->campaign_id = $request->campaign;

		$startOfYearTemp = Carbon::now()->startOfYear();
		$dateToSend = Carbon::now()->startOfYear();

        // if ($request->filled('days_to_send')) {
        if ($request->filled('days_to_send') && !$request->filled('send_immediately')) {
        	$toBeAdded = (int) $request->days_to_send;
            $dateToSend = $startOfYearTemp->addDays($toBeAdded);
			$task->date_to_send = $dateToSend;
        }

		// $task->date_to_send = $dateToSend;

		if ($request->filled('send_immediately')) {
			$task->time_to_send = date("H:i", strtotime('00:00'));
		} else {
			$task->time_to_send = date("H:i", strtotime($request->time_to_send));
		}

		$task->active = $request->active ? true : false;
		// $task->success_sent_msg_count = 1;
		// $task->msg_count = 1;
		$task->message = $request->filled('message') ? strip_tags($request->message) : null;
		$task->task_type_id = $request->filled('task_type_id') ? $request->task_type_id : null;
		$task->gateway_key = $subAccount->getApiKeyId();
		$task->is_nonreturning = $request->is_nonreturning ?: null;
		$task->accept_datesending = $request->accept_datesending ?: null;
    	$task->task_key = sha1($client->id . '--' . $task->subAccount->id . '--' . $now->toDateTimeString());
	    $task->save();

       	return $task;
	}

	// @depreciated for Text Blast
	// @depreciated use the specific repository instead for different task type
	// Merun na siyang sariling repository
	public function update($taskId, Request $request, $client)
	{
		$task = $this->model->find($taskId);

   		if ($request->filled('subaccount_id')) {
        	$reqSubAccount = $request->subaccount_id;
        }

        if ($request->filled('sub_account_id')) {
        	$reqSubAccount = $request->sub_account_id;
        }

		$task->subaccount_id = $reqSubAccount;
		$task->entry_id = null;
		$task->campaign_id = $request->campaign;

		$startOfYearTemp = Carbon::now()->startOfYear();
		$dateToSend = Carbon::now()->startOfYear();

        if ($request->filled('days_to_send')) {
        	$toBeAdded = (int) $request->days_to_send;
        	$dateToSend = $startOfYearTemp->addDays($toBeAdded);
        }

		$task->date_to_send = $dateToSend;

		if ($request->has('send_immediately')) {
			$task->time_to_send = null;
			$task->date_to_send = null;
		} else {
			$task->time_to_send = date("H:i", strtotime($request->time_to_send));
			if ($request->filled('date_to_send')) {
				$task->date_to_send = date("H:i", strtotime($request->date_to_send));
			}
		}

		$task->active = $request->active ? true : false;
		$task->message = $request->filled('message') ? strip_tags($request->message) : null;
		$task->is_nonreturning = $request->is_nonreturning ?: null;
		$task->accept_datesending = $request->accept_datesending ?: null;

    	$task->save();

       	return $task;
	}

	public function delete(Request $request, $taskId)
	{
		$task = $this->model->find($taskId); // removed data
        $task->delete();

        if ($request->filled('dont_detach')) {
			$task->taskCustomers()->detach(); // removed customers
        }

        return $task;
	}

	public function getDataOfCustomerFilter(Request $request){

		if ( $request ) {
			$query = Customer::whereHas('subAccounts', function($q) use($request) {
	            $q->where('subaccount_id', $request->subaccount_id);
	        })->whereNotNull('mobile');
	        // customer group
	        if ($request->filled('customergroup')) {
	            $query = $query->whereHas('customerGroups', function($q) use($request) {
	                $q->where('customer_group_id', $request->customergroup);
	            });
	        }
	        // monthfilter
	        if ($request->filled('monthfilter')) {
	            // $query = $query->whereMonth('b_month', '=',  $request->monthfilter);
	            $query = $query->where('b_month', $request->monthfilter);
	        }
	        // yearpicker
	        if ($request->filled('yearpicker')) {
	            // $query = $query->whereYear('birthdate', '=',  $request->yearpicker);
	            $query = $query->where('b_year', $request->yearpicker);
	        }
	        // dayfilter
	        if ($request->filled('dayfilter')) {
	            // $query = $query->whereDay('birthdate', '=',  $request->dayfilter);
	            $query = $query->where('b_day', $request->dayfilter);
	        }
	        // gender
	        if ($request->filled('gender')) {
	            $query = $query->where('gender', $request->gender);
	        }
	        // city
	        if ($request->filled('city')) {
	            $query = $query->where('city', $request->city);
	        }
		}
        return $customers = $query->get();
        // return $customers = $query->pluck('id');
	}

	public function getDataOfCustomerByIdFilter(Request $request){

		if ( $request ) {
			$query = Customer::whereHas('subAccounts', function($q) use($request) {
	            $q->where('subaccount_id', $request->sub_account_id);
	        })->whereNotNull('mobile');
	        // customer group
	        if ($request->filled('customergroup')) {
	            $query = $query->whereHas('customerGroups', function($q) use($request) {
	                $q->where('customer_group_id', $request->customergroup);
	            });
	        }
	        // monthfilter
	        if ($request->filled('monthfilter')) {
	            // $query = $query->whereMonth('b_month', '=',  $request->monthfilter);
	            $query = $query->where('b_month', $request->monthfilter);
	        }
	        // yearpicker
	        if ($request->filled('yearpicker')) {
	            // $query = $query->whereYear('birthdate', '=',  $request->yearpicker);
	            $query = $query->where('b_year', $request->yearpicker);
	        }
	        // dayfilter
	        if ($request->filled('dayfilter')) {
	            // $query = $query->whereDay('birthdate', '=',  $request->dayfilter);
	            $query = $query->where('b_day', $request->dayfilter);
	        }
	        // gender
	        if ($request->filled('gender')) {
	            $query = $query->where('gender', $request->gender);
	        }
	        // city
	        if ($request->filled('city')) {
	            $query = $query->where('city', $request->city);
	        }
		}
        // return $customers = $query->get();
        return $customers = $query->pluck('id');
	}

	public function searchByKey($taskKey)
	{
		$query = $this->model;

		$query = $query->where('task_key', $taskKey);

		return $query->first();
	}

	/*
	 * For bulk API of jobs
	 */
	public function addJobs(Request $request)
	{
		$taskRepo = resolve(TaskRepository::class);
		if ($request->filled('data')) {
			$data = $request->data;
			$totalJobs = count($data);
			$currentJob = 0;
			unset($request['data']);
			foreach ($data as $key => $datum) {
				$appKey = isset($datum['app_key']) ? $datum['app_key'] : null;
				$task = $appKey ? $taskRepo->searchByKey($appKey) : null;

				if ($task) {
					$request['security_key'] = isset($datum['security_key']) ? $datum['security_key'] : '';
					$request['app_key'] = isset($datum['app_key']) ? $datum['app_key'] : '';
					$request['last_name'] = isset($datum['last_name']) ? $datum['last_name'] : '';
					$request['first_name'] = isset($datum['first_name']) ? $datum['first_name'] : '';
					$request['number'] = isset($datum['number']) ? $datum['number'] : '';
					$request['items'] = isset($datum['items']) ? $datum['items'] : '';
					$request['total_amount'] = isset($datum['total_amount']) ? $datum['total_amount'] : '';
					$request['misc_shortcodes'] = isset($datum['misc_shortcodes']) ? $datum['misc_shortcodes'] : '';
					$request['date_of_sending'] = isset($datum['date_of_sending']) ? $datum['date_of_sending'] : '';
					$request['product_id'] = isset($datum['product_id']) ? $datum['product_id'] : '';

					$this->addJob($request, $task, true);
					$currentJob++;

					if ($currentJob >= $totalJobs) {
						return 'All jobs has been processed';
					}
				} else {
					$this->errors = $this->messageBag->add('fatal_error', 'Cannot find a created or active task with key provided');
				}

			}
		} else {
			$this->errors = $this->messageBag->add('fatal_error', 'No data or jobs found for SMS sending');
		}

		throw new \ATM\Validation\ValidationException($this->errors);
	}

	public function addJob(Request $request, Task $task, $fromApi = false)
    {
		if (!$request->filled('security_key')) {
			$this->errors = $this->messageBag->add('fatal_error', 'Fatal error on making the request.');
		}

		if (!$request->filled('app_key')) {
			$this->errors = $this->messageBag->add('fatal_error', 'App key does not exist.');
		}

		$subAccount = SubAccount::where('security_key', $request->security_key)->get()->first();

		if (!$subAccount) {
			$this->errors = $this->messageBag->add('fatal_error', 'Fatal error on making the request (Security key doesn\'t exist).');
		}

        $subAccount = $task->subAccount;
		$smsFacade = resolve(\ATM\Repositories\Sms\SmsFacade::class);
        $today = new Carbon(); // Current Date Time

		$startOfThisMonth = $today->copy()->startOfMonth();
		$firstDayofNextMonth = $startOfThisMonth->copy()->addMonth()->startOfMonth(); // get the first day of next month

        // $taskTimeToSend = Carbon::parse($today->copy()->toDateString() . ' ' . $task->time_to_send);
		// Task Date Time where the Time will be added to Request Date
        $addHours = $task->ots_hours; // Get Hours in Task Date Time
        $addMins = $task->ots_minutes; // Get Minutes in Task Date Time
        $addDays = $task->ots_days; // Get Days is Task Date Time

		if ($addDays) {
			$daysAdded = $today->createFromTimeString('10:00')->copy()->addDays($addDays);
			
			// $requestDateTime = $daysAdded->startOfDay()->addHours($addHours)->addMinutes($addMins); // Request Date Time and add Hours & Minutes

			$requestDateTime = $daysAdded;
			if ($addHours) {
				$requestDateTime = $requestDateTime->addHours($addHours);
			}
			if ($addMins) {
				$requestDateTime = $requestDateTime->addMinutes($addMins);
			}
		} else {
			$requestDateTime = $today->copy()->addHours($addHours)->addMinutes($addMins); // Request Date Time and add Hours & Minutes
		}

		if ($task->accept_datesending && $request->filled('date_of_sending')) {
			$requestDateTime = new Carbon($request->date_of_sending);
		}

		$diffDays = $today->copy()->diffInDays($requestDateTime);
		$taskKey = $request->app_key;
		$task = Task::where('task_key', $taskKey)->get()->first();

		if (!$task->active) {
			$this->errors = $this->messageBag->add('fatal_error', 'This SMS task is no longer active and can\'t receive any more request.');
		}

		if (count($this->errors) > 0) {
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		$number = formattedNumber($request->number, true);
		$forSearching = formattedNumber($request->number, true, false, true);
        if ($request->filled('number') && $request->number && $number) {

			$lName = $request->filled('last_name') ? $request->last_name : '';
			$fName = $request->filled('first_name') ? $request->first_name : '';
			$items = $request->filled('items') ? $request->items : '';

			$entryId = $request->product_id ?: null;
			$name = $fName.' '.$lName;

			$request['number'] = $forSearching;
			$existingContact = $this->customerRepo->search($request)->first();
			if ($existingContact) {
				if (!$request->filled('first_name')) {
					$request['first_name'] = $existingContact->first_name;
				}
				if (!$request->filled('last_name')) {
					$request['last_name'] = $existingContact->last_name;
				}
				$name = "{$existingContact->first_name} {$existingContact->last_name}";
			}

			$message = $this->taskRepository->makeTaskMessage($request, $task->message, $task);
			$messageLength = strlen($message);
			$cost = $subAccount->getCreditAmount($messageLength);
			// $canSend = $subAccount->isCreditSufficientForTask($messageLength);

			if ($subAccount->isBeechoo()) { // beechoo
				$hasNumber = $request->number && $request->number != NULL && $request->number != '';
				$formattedNumber = $hasNumber ? formattedNumber($request->number) : null;
				$canSend = $formattedNumber ? $subAccount->isCreditSufficientForTaskWithNumber($messageLength, $formattedNumber) : false;
			} else {
				$canSend = $subAccount->isCreditSufficientForTask($messageLength);
			}

			if ($task->is_nonreturning) { // Check if non returning yung task, then if has pending job it'll delete it first.
				$taskProcedure = resolve(\ATM\StoredProcedure\Task\TaskProcedure::class);
				$request['task_id'] = $task->id;
				$request['entry_id'] = $request->product_id ?: null;
				$existingJobs = $taskProcedure->searchTaskJobsForNonReturning($request, str_replace('+', '', $number));
				unset($request['task_id']);
				$this->processNonReturningApi($task, $existingJobs);
			}

			if ($requestDateTime->gt($today)) { // Condition to check if immediate send

				// Insert sa job for next month
				$allArraysJobNextMonth = [];
				$allArraysJob15thDay = [];
				$allArraysJob14thDay = [];
				$allArraysJobTomorrow = [];
				$allArraysJobToday = [];

				if ($canSend) {
					$currentDateTime = Carbon::now()->toDateTimeString();
					$draw = [
						'customer_id' => $existingContact ? $existingContact->id : null,
						'customer_name' => $name,
						'customer_number' => preg_replace('/\D+/', '', $number),
						// 'gateway_key' => $task->gateway_key,
						'gateway_key' => $subAccount->getApiKeyId(),
						'campaign_id' => $task->campaign_id,
						'credits_consumed' => $cost,
						'when_to_send' => $requestDateTime->toDateTimeString(),
						'message' => $message,
						'task_id' => $task->id,
						'subaccount_id' => $task->subaccount_id,
						'client_id' => $task->client_id,
						'zs_booking_id' => $request->zs_booking_id ?? null,
						'entry_id' => $entryId,
						'source' => $request->ip(),
						'created_at' => $currentDateTime,
						'updated_at' => $currentDateTime,
					];

					if ($diffDays) {
						if ($diffDays == 1) {
							$allArraysJobTomorrow[] = $draw;
						} elseif($diffDays >= 2 && $diffDays <= 14) {
							$allArraysJob14thDay[] = $draw;
						} elseif($diffDays >= 15) {
							if ($requestDateTime->gte($firstDayofNextMonth)) {
								$allArraysJobNextMonth[] = $draw;
							} else {
								$allArraysJob15thDay[] = $draw;
							}
						}
					} else {
						if ($diffDays == 0) {
							$allArraysJobToday[] = $draw;
						} else {
							$this->errors = $this->messageBag->add('task', 'No Jobs Found ...')->merge($this->errors);
							return;
						}
					}

					if ($subAccount->credit) {
						$credit = $subAccount->credit;
						if ($cost > $credit->pending_balance) {
							$this->errors = $this->messageBag->add('job_error', 'Your credit is not enough for the creation of this SMS task.');
							throw new \ATM\Validation\ValidationException($this->errors);
						}
						$smsFacade->minusPendingCreditPerSubaccount($cost, $subAccount);
					}

					$jobsData = [
						'allArraysJobNextMonth' => $allArraysJobNextMonth,
						'allArraysJob15thDay' => $allArraysJob15thDay,
						'allArraysJob14thDay' => $allArraysJob14thDay,
						'allArraysJobTomorrow' => $allArraysJobTomorrow,
						'allArraysJobToday' => $allArraysJobToday,
					];
					$this->taskJobRepo->continueSendingJobs($jobsData);

					// ADDITIONAL CONDITIONS DITO FOR OTHER JOBS
					$this->calculateTotalCountAndProjected($cost, $task);

					return 'Job created with new schedule';

				} else {
					// Create failed job dahil wala ng credits
					$errorResp = 'Job failed, no credits remaining in your account';
					$this->createFailed($request, $message, $cost, $name, $number, $requestDateTime, $task, $errorResp);
					$this->calculateTotalCountAndProjected($cost, $task);

					return $errorResp;
				}
			}
			else {
				$result = $this->immediateSend($request, $task);

				if ($result) {
					return 'Job created, SMS sent';
				}

				$errorResp = 'Job failed, no credits remaining in your account';
				$this->createFailed($request, $message, $cost, $name, $number, $requestDateTime, $task, $errorResp);
				$this->calculateTotalCountAndProjected($cost, $task);
				return $errorResp;
			}

		} else {
			$this->errors = $this->messageBag->add('fatal_error', 'Mobile number is invalid or not provided.');
			throw new \ATM\Validation\ValidationException($this->errors);
		}

        return false;
    }

	private function processNonReturningApi($task, $existingJobs)
	{
		$jtids = [];
		$jtmids = [];
		$jnmids = [];
		$jttdids = [];
		$jtdtids = [];
		$totCreditsPutBack = 0;

		foreach ($existingJobs as $job) {
			if ($job->job_today) {
				$exploded = explode('*`*', $job->job_today);
				$results = $this->partedIdAndJob($exploded);
				$jtids = $results['ids'];
				$totCreditsPutBack += $results['credits'];
			}
			if ($job->job_tomorrow) {
				$exploded = explode('*`*', $job->job_tomorrow);
				$results = $this->partedIdAndJob($exploded);
				$jtmids = $results['ids'];
				$totCreditsPutBack += $results['credits'];
			}
			if ($job->job_next_month) {
				$exploded = explode('*`*', $job->job_next_month);
				$results = $this->partedIdAndJob($exploded);
				$jnmids = $results['ids'];
				$totCreditsPutBack += $results['credits'];
			}
			if ($job->job_after_tomorrow_to_14th_day) {
				$exploded = explode('*`*', $job->job_after_tomorrow_to_14th_day);
				$results = $this->partedIdAndJob($exploded);
				$jttdids = $results['ids'];
				$totCreditsPutBack += $results['credits'];
			}
			if ($job->job_15th_day_to_end_month) {
				$exploded = explode('*`*', $job->job_15th_day_to_end_month);
				$results = $this->partedIdAndJob($exploded);
				$jtdtids = $results['ids'];
				$totCreditsPutBack += $results['credits'];
			}
		}

		$this->deleteIdsInThisTable('job_today', $jtids);
		$this->deleteIdsInThisTable('job_tomorrow', $jtmids);
		$this->deleteIdsInThisTable('job_next_month', $jnmids);
		$this->deleteIdsInThisTable('job_after_tomorrow_to_14th_day', $jttdids);
		$this->deleteIdsInThisTable('job_15th_day_to_end_month', $jtdtids);

		// Update now the credits
		if ($totCreditsPutBack) {
			Credit::where('subaccount_id', '=', $task->subaccount_id )
					->increment('pending_balance', $totCreditsPutBack);
			// DB::delete("UPDATE credits SET WHERE subaccount_id = {$task->subaccount_id}");
		}

		return true;
    }

	private function partedIdAndJob($arrData)
	{
		$ids = [];
		$credits = 0;
		foreach ($arrData as $item) {
			$innerExploded = explode('**', $item);
			if (count($innerExploded) > 1) {
				$ids[] = $innerExploded[0];
				$credits += $innerExploded[1];
			}
		}

		return ['ids' => $ids, 'credits' => $credits];
    }

	private function deleteIdsInThisTable($table, $ids)
	{
		if (count($ids) > 0) {
			$ids = implode(', ', $ids);
			DB::delete("DELETE FROM {$table} WHERE id IN ({$ids})");
		}

		return true;
    }

	private function createFailed($request, $message, $cost, $name, $number, $datetime, $task, $errorResponse = 'Mobile number format is invalid.')
	{
		$request['message'] = $message;
		$request['cost'] = $cost;
		$request['name'] = $name;
		$request['number'] = preg_replace('/\D+/', '', $number);
		$request['task_id'] = $task->id;
        $jobFailed = $this->jobFailedRepo->createFailedJob($request, $datetime, $task, $errorResponse);

        $failed_jobs_count = $this->taskRepository->search($request)->toArray();
        $jbFailed = ( $failed_jobs_count && $failed_jobs_count[0]['jobs_failed'] != NULL ) ? $failed_jobs_count[0]['jobs_failed'] + 1 : 1;
        $this->taskRepository->updateFailedCount($task->id, $jbFailed);

		return $jobFailed;
    }

    public function validateJobData(Request $request)
    {
        $rules = [
            'first_name'    => 'required|string',
            'last_name'     => 'required|string',
            'number'        => 'required|numeric',
            'date_time'     => 'required|date',
            'items'         => 'required'
        ];

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

    public function immediateSend(Request $request, Task $task)
	{
		$subAccount = $task->subAccount;

    	if (!$subAccount) {
			$this->errors = $this->messageBag->add('fatal_error', 'Fatal error on making the request.');
		}

		$smsFacade = resolve(\ATM\Repositories\Sms\SmsFacade::class);

		$lName = $request->filled('last_name') ? $request->last_name : '';
        $fName = $request->filled('first_name') ? $request->first_name : '';
        $number = $request->filled('number') ? $request->number : '';
		$items = $request->filled('items') ? $request->items : '';

        $request['filter_search'] = $fName . ' ' . $lName;

        $existingContact = $this->customerRepo->search($request)->first();

        if ($existingContact) {
        	$request['existing_contact_id'] = $existingContact->id;
        }

		$message = $this->taskRepository->makeTaskMessage($request, $task->message, $task);
		/*$message = str_replace(
			['[first_name]', '[last_name]', '[number]', '[items]'],
			[$fName, $lName, $number, $items],
			$task->message
		);*/
		$messageLength = strlen($message);

		// $canSend = $subAccount->isCreditSufficientForTask($messageLength);
		if ($subAccount->isBeechoo()) { // beechoo
			$hasNumber = $request->number && $request->number != NULL && $request->number != '';
			$formattedNumber = $hasNumber ? formattedNumber($request->number) : null;
			$canSend = $formattedNumber ? $subAccount->isCreditSufficientForTaskWithNumber($messageLength, $formattedNumber) : false;
		} else {
			$canSend = $subAccount->isCreditSufficientForTask($messageLength);
		}

		if (!$task->active) {
			$this->errors = $this->messageBag->add('fatal_error', 'This SMS task is no longer active and can\' receive any more job request.');
		}

		if (count($this->errors) > 0) {
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		if ($canSend) {
			try {
				$request['name'] = $fName . ' ' . $lName;
				$request['message'] = $message;
				$result = $smsFacade->createImmediateJob($request, $subAccount, $task);

				return $result;
			} catch (ValidationException $e) {
				$errors = $e->getErrors();
				$this->errors = $this->messageBag->add('fatal_error', $errors[0]);
				throw new \ATM\Validation\ValidationException($this->errors);
			}
		} else {
			return false;
		}
	}

	/* function for updating data in the task table */
    private function calculateTotalCountAndProjected($cost, $task)
    {
        $jobsCount = (int) $task->jobs_count;
        $projectedCreditsConsumed = (int) $task->projected_credits_consumed;

        $jobsCount++;
        $projectedCreditsConsumed = $projectedCreditsConsumed + $cost;

        $task->jobs_count = $jobsCount;
        $task->projected_credits_consumed = $projectedCreditsConsumed;
        $task->save();

        return;
    }

}
