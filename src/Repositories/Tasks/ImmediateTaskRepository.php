<?php
/**
 * Created by PhpStorm.
 * User: jr
 * Date: 5/6/2019
 * Time: 11:24 AM
 */

namespace ATM\Repositories\Tasks;

use ATM\ContextInterface;
use ATM\Repositories\Client\Client;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\SubAccount\SubAccount;
use DeepCopy\f008\A;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;

class ImmediateTaskRepository extends ContextRepository
{
	public function __construct(
		Task $task,
		ContextInterface $context,
		MessageBag $messageBag
	)
	{
		$this->model = $task;
		$this->context = $context;
		$this->messageBag = $messageBag;
	}

	public function search(Request $request, $with = [])
	{
		$query = $this->scope($with)->where('task_type_id', '=', Task::IMMEDIATE_TASK);

		if ($request->filled('subaccount_filter')) {
			$query = $query->where('subaccount_id', $request->subaccount_filter);
		}

		if ($request->filled('campaign_filter')) {
			$query = $query->where('campaign_id', $request->campaign_filter);
		}

		if ( $request->has('per_page') && $request->per_page != self::DISPLAY_ALL ) {
			return $query->paginate($request->per_page ?: self::PER_PAGE);
		} else {
			return $query->get();
		}
	}

	public function addTask(Request $request, $task = null)
	{
		$client = null;
		$subAccount = null;

		if ($task) {
			$client = $task->client;
			$subAccount = $task->subAccount;
		}

		if ($request->filled('client_id') && !$client) {
			$client = Client::find($request->client_id);
		}

		if ($request->filled('subaccount_id') && !$subAccount) {
			$subAccount = SubAccount::find($request->subaccount_id);
		}

		if (!$client || !$subAccount) {
			$this->errors = $this->messageBag->add('fatal_error', 'Something went wrong, please try again later.');
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		$credit = $subAccount->credit;
		$validator = \Validator::make(
			[
				'campaign' => $request->campaign,
				'credit' => $credit->pending_balance
			],
			[
				'campaign' => 'required',
				'credit' => 'required|min:1'
			]
		);

		if ($validator->fails()) {
			$this->errors = $validator->messages()->merge($this->errors);
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		$task = $task ?: $this->model;
		$task->name = $request->name;
		$task->client_id = $client->id;
		$task->subaccount_id = $subAccount->id;
		$task->campaign_id = $request->campaign;
		$task->active = $request->active ?: false;
		$task->task_type_id = Task::IMMEDIATE_TASK;
		$gateway = $subAccount->getApiKey();

		$task->gateway_key = $gateway ? $gateway->id : null;
		$task->save();

		$task->task_key = Str::limit(sha1($task->id . '--' . $task->client_id . '--' . $task->subaccount_id), 10, '');
		$task->save();

		return $task;
	}

	public function removeTask(Task $task)
	{
		$delTask = $task;

		$task->taskCustomers()->detach(); // removed customers
		$task->delete();

		return true;
	}

	public function createJob(Request $request)
	{
		if (!$request->filled('security_key')) {
			$this->errors = $this->messageBag->add('fatal_error', 'Fatal error on making the request.');
		}

		if (!$request->filled('app_key')) {
			$this->errors = $this->messageBag->add('fatal_error', 'App key does not exist.');
		}

		$subAccount = SubAccount::where('security_key', $request->security_key)->get()->first();

		if (!$subAccount) {
			$this->errors = $this->messageBag->add('fatal_error', 'Fatal error on making the request.');
		}

		$smsFacade = resolve(\ATM\Repositories\Sms\SmsFacade::class);
		$taskKey = $request->app_key;

		$message = $request->message;
		$messageLength = strlen($message);

		$task = Task::where('task_key', $taskKey)->get()->first();
		// $canSend = $subAccount->isCreditSufficientForTask($messageLength);

		if ($subAccount->isBeechoo()) { // beechoo
			$hasNumber = $request->number && $request->number != NULL && $request->number != '';
			$formattedNumber = $hasNumber ? formattedNumber($request->number) : null;
			$canSend = $formattedNumber ? $subAccount->isCreditSufficientForTaskWithNumber($messageLength, $formattedNumber) : false;
		} else {
			$canSend = $subAccount->isCreditSufficientForTask($messageLength);
		}

		if (is_null($task)) {
			$this->errors = $this->messageBag->add('fatal_error', 'No task found using that key.');
		}

		if ($task && !$task->active) {
			$this->errors = $this->messageBag->add('fatal_error', 'This SMS task is no longer active and can\'t receive any more job request.');
		}

		if (count($this->errors) > 0) {
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		if ($canSend) {
			try {
				$result = $smsFacade->createImmediateJob($request, $subAccount, $task);
				return $result;
			} catch (\ATM\Validation\ValidationException $e) {
				$errors = $e->getErrors();
				$this->errors = $this->messageBag->add('fatal_error', $errors->first());
				throw new \ATM\Validation\ValidationException($this->errors);
			}
		} else {
			$this->errors = $this->messageBag->add('fatal_error', 'You have 0 credits on your account.');
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		return false;
	}
}
