<?php

namespace ATM\Repositories\Tasks\JobsTodaySend;

use ATM\ContextInterface;
use ATM\Repositories\Client\Client;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use DeepCopy\f008\A;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;

class JobTodayRepository extends ContextRepository
{
	public function __construct(
		JobToday $JobToday,
		ContextInterface $context,
		MessageBag $messageBag
	)
	{
		$this->model = $JobToday;
		$this->context = $context;
		$this->messageBag = $messageBag;
	}

	public function search(Request $request, $with = [])
	{
		$query = $this->scope($with);

		if ($request->filled('from_date') && $request->filled('to_date')) {
			$query = $query->where('when_to_send', '>=' , $request->from_date)
						->where('when_to_send', '<=' , $request->to_date);
		}

		if ($request->filled('subaccount_id')) {
			$query = $query->where('subaccount_id', $request->subaccount_id);
		}

		if ($request->filled('sub_account_id')) {
			$query = $query->where('subaccount_id', $request->sub_account_id);
		}

		if ($request->filled('task_type_id')) {
			$query = $query->whereHas('task', function ($q) use ($request) {
				$q->where('task_type_id', $request->task_type_id);
			});
		}

		if ($request->filled('campaign_filter')) {
			$query = $query->where('campaign_id', $request->campaign_filter);
		}

		if ( $request->has('per_page') ) {
			return $query->paginate($request->per_page ?: self::PER_PAGE);
		} else {
			return $query->paginate(20);
			// return $query->get();
		}
	}
}
