<?php

namespace ATM\Repositories\Tasks\JobsCompleted;

use ATM\ContextInterface;
use ATM\ContextScope;
use ATM\Repositories\ApiGateway\Gateway;
use ATM\Repositories\ApiKey\ApiKey;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Client\Client;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Uploadable\Uploadable;
use App\Model\BaseModel;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class JobsCompleted extends BaseModel implements ContextScope {

	use ValidatingTrait;

	public $table = "jobs_completed";

	protected $rules = [];

	protected $dates = [
		'when_to_send', 'created_at', 'updated_at'
	];

	protected $fillable = [
		'customer_name',
		'customer_number',
		'api_key',
		'campaign_id',
		'message',
		'task_id',
		'subaccount_id',
		'client_id',
		'zs_booking_id'
	];

	// deprecated use client
    public function clients()
    {
        return $this->belongsTo(Client::class);
    }

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function subAccount()
	{
		return $this->belongsTo(SubAccount::class, 'subaccount_id');
	}

    // deprecated
    public function tasks() {
        return $this->belongsTo(Task::class, 'task_id');
    }

    public function task() {
        return $this->belongsTo(Task::class, 'task_id');
    }

	// deprecated use campaign()
    public function campaigns() {
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

	public function campaign() {
		return $this->belongsTo(Campaign::class, 'campaign_id');
	}

	public function customer() {
		return $this->belongsTo(Customer::class, 'customer_id');
	}

    public function taskCustomerGroups() {
        return $this->belongsToMany(CustomerGroup::class, 'task_group_customers', 'task_id', 'customer_groups_id');
    }

    public function taskCustomers() {
        return $this->belongsToMany(Customer::class, 'task_customers', 'task_id', 'customer_id');
    }

	public function getDateToSendAttribute() // date_time_to_send
	{
		return new Carbon("{$this->when_to_send->toDateString()}");
    }

	public function gateway()
	{
		return $this->belongsTo(ApiKey::class, 'gateway_key');
	}

    // Query Scopes
    public function scopeOfContext($query, ContextInterface $context)
    {
		return $query->where($context->column(), $context->id());
    }

	// Mutators
	public function getDateTimeSendingAttribute()
	{
		return $this->when_to_send;
	}
}
