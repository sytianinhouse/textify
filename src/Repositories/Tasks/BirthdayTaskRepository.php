<?php

namespace ATM\Repositories\Tasks;

use ATM\ContextInterface;
use ATM\Repositories\Sms\SmsFacade;
use ATM\Repositories\TaskJobs\TaskJobsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;

class BirthdayTaskRepository
{
	public function __construct(
		TaskRepository $taskRepository,
		TaskJobsRepository $taskJobRepo,
		MessageBag $messageBag,
		ContextInterface $context
	)
	{
		$this->taskJobRepo = $taskJobRepo;
		$this->taskRepo = $taskRepository;
		$this->messageBag = $messageBag;
		$this->context = $context;
	}

	public function getTask(Request $request)
	{
		$query = $this->taskRepo->scope()->where('task_type_id', Task::BIRTHDAY_SMS);

		if ($request->filled('subaccount_id')) {
			$query = $query->where('subaccount_id', $request->subaccount_id);
		}

		if ($request->filled('subaccount_ids')) {
			$query = $query->whereIn('subaccount_id', $request->subaccount_ids);
		}

		$task = $query->get();

		return $task ?: new Task();
	}

	public function updateTask(Request $request)
	{
		$client = $request->user()->client;
		$subAccounts = $client->subAccounts;

		$request['subaccount_ids'] = $subAccounts->pluck('id');
		$tasks = $this->getTask($request)->keyBy('subaccount_id');

		DB::transaction(function() use($request, $tasks, $subAccounts, $client){
			$data = $request->data;
			foreach ($subAccounts as $subAccount) {
				if (isset($data[$subAccount->id])) { 
					$thisData = isset($data[$subAccount->id]) ? $data[$subAccount->id] : [];
					$now = Carbon::now();
					$toSend = str_pad($thisData['day_to_send'] + 1, 2, '0', STR_PAD_LEFT);
					$dayToSend = new Carbon("{$now->format('Y')}-{$now->format('m')}-{$toSend} {$thisData['time_to_send']}");
					$task = isset($tasks[$subAccount->id]) ? $tasks[$subAccount->id] : new Task();
					$task->client_id = $client->id;
					$task->subaccount_id = $subAccount->id;
					$task->date_to_send = $dayToSend->toDateString();
					$task->time_to_send = $dayToSend->toTimeString();
					$task->campaign_id =  ($thisData && isset($thisData['campaign_id'])) ? $thisData['campaign_id'] : false;
					$task->active = ($thisData && isset($thisData['enable'])) ? true : false;
					$task->message = ($thisData && isset($thisData['message'])) ? strip_tags($thisData['message']) : null;
					$task->task_type_id = Task::BIRTHDAY_SMS;
	
					$gateway = $subAccount->apis->first();
					$task->gateway_key = $gateway ? $gateway->id : null;
					$task->save();
				}

			}
		});

		return $tasks;
	}

	public function generateJobs(Collection $tasks, Carbon $date)
	{
		// Get all customers na yung bmonth base sa variable na $date
		// generate jobs,
		// gawing bultuhan yung pag-inset ng jobs para hindi mabagal yung process
		$currentDateTime = Carbon::now();
		$forceInsert = true; // force create of job even if there is no more credits
		$allArraysJobNextMonth = [];
		$allArraysJob15thDay = [];
		$allArraysJob14thDay = [];
		$allArraysJobTomorrow = [];
		$allArraysJobToday = [];
		$totalCredits = 0;

		$smsFacade = resolve(SmsFacade::class);

		foreach ($tasks as $task) {
			$subAccount = $task->subAccount;
			$credit = $subAccount->credit;
			$pendingBalance = $credit->pending_balance;

			if ($pendingBalance < 1) {
				Log::error("Sub Account - {$subAccount->id} : {$subAccount->account_name} - failed to generate birthday SMS due to insufficient balance.");
				continue;
			}; // If no more credits failed to generate birthday SMS

			$customers = $subAccount->customers()->where('b_month', 'LIKE', $date->format('n'))->withNumbers()->get();
			$totalCredits = 0;
			$assumedCredits = 0;
			$jobsCount = 0;
			$thisJobs = [];
			$taskDate = $task->date_time_to_send;
			$whenToSend = new Carbon("{$date->format('Y')}-{$date->format('m')}-{$taskDate->format('d')} {$taskDate->format('h:i A')}");

			if ($customers->count() > 0) {
				foreach ($customers as $customer) {

					if ($customer->birth_date) {
						$birthdate = $customer->birth_date->copy();
						$diff = $currentDateTime->diff($birthdate);
						$diffDays = (integer) $diff->days;

						$request = new Request();
						$request['first_name'] = $customer->first_name;
						$request['last_name'] = $customer->last_name;
						$request['email'] = $customer->email;
						$request['telephone'] = $customer->telephone;
						$request['address_1'] = $customer->address_1;

						$message = $this->taskRepo->makeTaskMessage($request, $task->message, $task);
						//$message = $this->taskRepo->makeSmsFormatted($task, $customer);
						$creditsConsumed = $subAccount->getCreditAmount(strlen($message));
						$assumedCredits += $creditsConsumed;
						$jobsCount += 1;

						$draw = [
							'customer_id' => $customer->id,
							'customer_name' => $customer->first_name . ' ' . $customer->last_name,
							'customer_number' => preg_replace('/\D+/', '', $customer->mobile),
							'gateway_key' => $task->gateway_key,
							'campaign_id' => $task->campaign_id,
							'credits_consumed' => $creditsConsumed,
							'when_to_send' => $whenToSend->toDateTimeString(),
							'message' => $message,
							'task_id' => $task->id,
							'subaccount_id' => $task->subaccount_id,
							'client_id' => $task->client_id,
							'source' => 'system_generated',
							'created_at' => $currentDateTime,
							'updated_at' => $currentDateTime,
						];

						if ($assumedCredits < $pendingBalance) {
							$totalCredits += $creditsConsumed;
							if ($diffDays) {
								if ($diffDays == 1) {
									$allArraysJobTomorrow[] = $draw;
								} elseif($diffDays >= 2 && $diffDays <= 14) {
									$allArraysJob14thDay[] = $draw;
								} elseif($diffDays >= 15) {
									$allArraysJob15thDay[] = $draw;
								}
							} else {
								if ($diffDays == 0) {
									$allArraysJobToday[] = $draw;
								} else {
									$this->errors = $this->messageBag->add('task', 'No Jobs Found ...')->merge($this->errors);
									return;
								}
							}
						}
					}
				}

				Log::info("Birthday SMS for '{$subAccount->account_name}' occured with total customers count {$hasBdayCounter} and will send on {$whenToSend->toDateTimeString()}");
				$smsFacade->minusPendingCreditPerSubaccount($totalCredits, $subAccount);

				$task->jobs_count = $jobsCount;
				$task->projected_credits_consumed = $assumedCredits;
				$task->save();
			}
		}

		$jobsData = [
			'allArraysJobNextMonth' => $allArraysJobNextMonth,
			'allArraysJob15thDay' => $allArraysJob15thDay,
			'allArraysJob14thDay' => $allArraysJob14thDay,
			'allArraysJobTomorrow' => $allArraysJobTomorrow,
			'allArraysJobToday' => $allArraysJobToday,
		];

		$this->taskJobRepo->continueSendingJobs($jobsData);
	}
}