<?php

namespace ATM\Repositories\Tasks\Jobs15thDayToEndMonth;

use ATM\ContextInterface;
use ATM\ContextScope;
use ATM\Repositories\ApiKey\ApiKey;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Client\Client;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Uploadable\Uploadable;
use App\Model\BaseModel;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class Job15thDayToEndMonth extends BaseModel implements ContextScope {

	use ValidatingTrait;

	public $table = "job_15th_day_to_end_month";

	protected $rules = [];

	protected $dates = [
		'when_to_send', 'created_at', 'updated_at'
	];

	protected $fillable = [
		'customer_name',
		'customer_number',
		'api_key',
		'campaign_id',
		'message',
		'task_id',
		'subaccount_id',
		'client_id',
		'zs_booking_id'
	];

	// @depreciated use client()
    public function clients()
    {
        return $this->belongsTo(Client::class);
    }

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function subAccount()
	{
		return $this->belongsTo(SubAccount::class, 'subaccount_id');
	}

    // @depreciated use task()
    public function tasks() {
        return $this->belongsTo(Task::class, 'task_id');
    }

	public function task() {
		return $this->belongsTo(Task::class, 'task_id');
	}

	// @depreciated use campaign()
    public function campaigns() {
        return $this->belongsTo(Campaign::class, 'campaign_id');
	}

	public function campaign() {
		return $this->belongsTo(Campaign::class, 'campaign_id');
	}

    public function taskCustomers() {
        return $this->belongsToMany(Customer::class, 'task_customers', 'task_id', 'customer_id');
    }

	public function gateway()
	{
		return $this->belongsTo(ApiKey::class, 'gateway_key');
	}

	// Mutators
	public function getDateTimeSendingAttribute()
	{
		return $this->when_to_send;
	}

    // Query Scopes
    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->where($context->column(), $context->id());
        } else if ($context->getInstance() instanceof SubAccount) {
            return $query->whereHas('client.subAccounts', function($q) use ($context) {
                return $q->where('id', $context->id());
            });
        }

        return $query;
    }
}
