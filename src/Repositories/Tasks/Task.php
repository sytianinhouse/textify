<?php

namespace ATM\Repositories\Tasks;

use ATM\ContextInterface;
use ATM\ContextScope;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Client\Client;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;
use ATM\Repositories\Tasks\JobsCompleted\JobsCompleted;
use ATM\Repositories\Tasks\JobsFailed\JobsFailed;
use ATM\Repositories\Tasks\JobsNextMonth\JobNextMonth;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;

use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use ATM\Repositories\Uploadable\Uploadable;
use App\Model\BaseModel;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Watson\Validating\ValidatingTrait;

class Task extends BaseModel implements ContextScope {

	use ValidatingTrait, SoftDeletes;

	// Dont change the value it reflect the record in DB
    const ONE_TIME_SEND = 2;
    const TEXT_BLAST_SEND = 1;
	const BIRTHDAY_SMS = 3;
	const IMMEDIATE_TASK = 7;
	const BOOKING_TASK = 8;

    const CHARACTER_LIMITER = 160;
    const CREDIT_LIMITER = 1;

	public $table = "task";

	protected $rules = [
        'task_type_id'  => 'required',
    ];

	protected $dates = [
		'date_to_send', 'deleted_at', 'created_at', 'updated_at'
	];

    protected $fillable = [
        'task_type_id'
    ];

    public static $typesArr = [
		self::IMMEDIATE_TASK => 'Custom SMS',
        self::TEXT_BLAST_SEND => 'Text Blast',
		self::ONE_TIME_SEND => 'Template API',
		self::BIRTHDAY_SMS => 'Birthday SMS',
        self::BOOKING_TASK => 'Booking / Reservation API',
    ];

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

    // @depreciated use yung singular relation
    public function clients()
    {
        return $this->belongsTo(Client::class);
    }

	public function campaign() {
		return $this->belongsTo(Campaign::class, 'campaign_id');
	}

	// @depreciated use yung singular relation
    public function campaigns() {
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

    public function subAccount() {
        return $this->belongsTo(SubAccount::class, 'subaccount_id');
    }

    public function tasksJobToday() {
        return $this->hasMany(JobToday::class, 'task_id');
    }

    public function tasksJobTomorrow() {
        return $this->hasMany(JobTomorrow::class, 'task_id');
    }

    public function tasksJobAfterTomorrowTo14thDay() {
        return $this->hasMany(JobAfterTomorrowTo14thDay::class, 'task_id');
    }

    public function tasksJob15thDayToEndMonth() {
        return $this->hasMany(Job15thDayToEndMonth::class, 'task_id');
    }

    public function tasksJobNextMonth() {
        return $this->hasMany(JobNextMonth::class, 'task_id');
    }

    public function tasksjobsCompleted() {
        return $this->hasMany(JobsCompleted::class, 'task_id');
    }

    public function tasksjobsFailed() {
        return $this->hasMany(JobsFailed::class, 'task_id');
    }

    /*public function taskCustomerGroups() {
        return $this->belongsToMany(CustomerGroup::class, 'task_group_customers', 'task_id', 'customer_groups_id');
    }*/

    public function taskCustomers() {
        return $this->belongsToMany(Customer::class, 'task_customers', 'task_id', 'customer_id');
    }

	public function childs()
	{
		return $this->hasMany(Task::class, 'parent_task_id');
    }

    /**
	 * Mutators
	 */
	public function getDateTimeToSendAttribute() // date_time_to_send
	{
        // if( is_null($this->date_to_send) && is_null($this->time_to_send) ) {
        //     dd('test');
        //     return new Carbon("{$this->date_to_send->toDateString()} {$this->time_to_send}");
        // } else {
        // }
        return new Carbon("{$this->date_to_send->toDateString()} {$this->time_to_send}");
    }

	public function getDateSendAttribute() // date_to_send
	{
		return new Carbon("{$this->date_to_send->toDateString()}");
    }

	public function getTimeSendAttribute() // time_to_send
	{
		return new Carbon("{$this->time_to_send}");
    }

	public function getHumanStatusAttribute()
	{
		$class = $this->active ? 'btn-success' : 'btn-danger';
		$label = $this->active ? 'active' : 'inactive';

		return "<span class='btn btn-sm {$class}'>{$label}</span>";
    }

	public function getOtsDaysAttribute()
	{
		if (isset($this->date_to_send)) {
			$startOfYearTemp = Carbon::parse($this->date_to_send->copy()->toDateString())->startOfYear();
			$diff = $startOfYearTemp->diffInDays($this->date_to_send);
			return $diff;
		}

		return 0;
	}

	public function getOtsHoursAttribute()
	{
		if ( is_null($this->time_to_send) ) return 0;

		$date = $this->date_to_send ? $this->date_to_send : Carbon::now();

		return Carbon::parse($date->toDateString() . ' ' . $this->time_to_send)->format('G');
    }

	public function getOtsMinutesAttribute()
	{
		if ( is_null($this->time_to_send) ) return 0;

		$date = $this->date_to_send ? $this->date_to_send : Carbon::now();

		return Carbon::parse($date->toDateString() . ' ' . $this->time_to_send)->format('i');
	}

	public function getOtsFormattedAttribute()
	{
		$days = $this->ots_days == 0 ? '' : (($this->ots_days > 1) ? "{$this->ots_days} days" : "{$this->ots_days} day");
		$hour = $this->ots_hours == 0 ? '' : (($this->ots_hours > 1) ? "{$this->ots_hours} hours" : "{$this->ots_hours} hour");
		$mins = $this->ots_minutes == 0 ? '' : (($this->ots_minutes > 1) ? "{$this->ots_minutes} minutes" : "{$this->ots_minutes} minute");

		return $days . ($hour ? ' ' . $hour  : '') . ($mins ? ' ' . $mins  : '');
	}

    /*
     * Query Scopes
     */

    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->whereHas('subAccount', function($q) use($context){
                return $q->where($context->column(), $context->id());
            });
        } else if ($context->getInstance() instanceof SubAccount) {
            return $query->where('subaccount_id', $context->id());
        }

        return $query;
    }

	public function scopeBirthdays($query)
	{
		return $query->where('task_type_id', self::BIRTHDAY_SMS);
    }

	public function scopeActives($query)
	{
		return $query->where('active', 1);
	}

	/*
	 * Mutators!
	 */

	public function getSubaccSecurityKeyAttribute()
	{
		$subAccount = $this->subAccount;
		if ($subAccount) {
			return $subAccount->security_key;
		}

		return null;
	}

    /** Helper Methods */
	public function isImmediateTask()
	{
		return $this->task_type_id == self::IMMEDIATE_TASK;
    }

    public function isOneTimeSendTask()
    {
        return $this->task_type_id == self::ONE_TIME_SEND;
    }

    public function isTextBlastTask()
    {
        return $this->task_type_id == self::TEXT_BLAST_SEND;
    }

	public function isBirthdayTask()
	{
		return $this->task_type_id == self::BIRTHDAY_SMS;
	}

	public function isSendImmediate()
	{
		return $this->ots_formatted == '' ? true : false;
    }

	public function isBookingTask()
	{
		return $this->task_type_id == self::BOOKING_TASK;
	}

	public function getApiUrl()
	{
		$url = url('/') . '/api';
		if ($this->isImmediateTask()) {
			$url .= '/immediate-sms';
		} elseif ($this->isOneTimeSendTask()) {
			$url .= '/one-time-sms';
		} elseif ($this->isBookingTask()) {
			$url .= '/booking-task';
		}

		return $url;
	}

	public function showApiUrl()
	{
        $completedUrl = '';
        $subAccount = $this->subAccount;
		$url = $this->getApiUrl();

		if ($this->isImmediateTask()) {
            $url .= "?security_key={$this->subAccount->security_key}";

            $completedUrl = "{$url}&app_key={$this->task_key}&number=[NUMBER]&name=[NAME]&message=[MESSAGE]";

		} elseif ($this->isOneTimeSendTask()) {
            $url .= "?security_key={$subAccount->security_key}";
			$url .= "&app_key={$this->task_key}";
            $url .= "&first_name=[FIRST_NAME]";
            $url .= "&last_name=[LAST_NAME]";
            $url .= "&number=[NUMBER]";
            $url .= "&items=[ITEMS]";
            $url .= "&total_amount=[TOTAL_AMOUNT]";

            $completedUrl = $url;

        } elseif ( $this->isBookingTask() ) {
            $url .= '/add-job';
            $url .= "?security_key={$subAccount->security_key}";
			$url .= "&app_key={$this->task_key}";
            $url .= "&first_name=[FIRST_NAME]";
            $url .= "&last_name=[LAST_NAME]";
            $url .= "&number=[NUMBER]";
            $url .= "&items=[ITEMS]";
            $url .= "&date=[DATE]";
            $url .= "&time=[TIME]";
            $url .= "&business_phone_number=[BUSINESS_PHONE_NUMBER]";
            $url .= "&business_name=[BUSINESS_NAME]";

            $completedUrl = $url;
        }

		return $completedUrl;
    }

	public function hasJobs()
	{
		return $this->tasksJobToday->count() > 0
			|| $this->tasksJobTomorrow->count() > 0
			|| $this->tasksJobAfterTomorrowTo14thDay->count() > 0
			|| $this->tasksJob15thDayToEndMonth->count() > 0
			|| $this->tasksJobNextMonth->count() > 0
			|| $this->tasksjobsCompleted->count() > 0
			|| $this->tasksjobsFailed->count() > 0;
    }

	public function modifyCreditsConsumend($amount, $mode)
	{
		$acc = $this->actual_credits_consumed;

		if ($mode == 'add') {
			$acc += $amount;
		} else if ($mode == 'sub') {
			$acc -= $amount;
		}

		$this->actual_credits_consumed = $acc;
		return $this->save();
    }
}
