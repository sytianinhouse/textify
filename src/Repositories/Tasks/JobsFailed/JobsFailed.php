<?php

namespace ATM\Repositories\Tasks\JobsFailed;

use ATM\ContextInterface;
use ATM\ContextScope;
use ATM\Repositories\ApiGateway\Gateway;
use ATM\Repositories\ApiKey\ApiKey;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Client\Client;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Uploadable\Uploadable;
use App\Model\BaseModel;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class JobsFailed extends BaseModel implements ContextScope {

	use ValidatingTrait, SoftDeletes;

	public $table = "jobs_failed";

	protected $rules = [];

	protected $dates = [
		'time_attempt_send', 'created_at', 'updated_at'
	];

	protected $fillable = [
		'customer_name',
		'customer_number',
		'api_key',
		'campaign_id',
		'message',
		'task_id',
		'subaccount_id',
		'client_id',
		'zs_booking_id'
	];

	// @depreciated use client()
    public function clients()
    {
        return $this->belongsTo(Client::class);
    }

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function subAccount()
	{
		return $this->belongsTo(SubAccount::class, 'subaccount_id');
	}

	public function task() {
		return $this->belongsTo(Task::class, 'task_id');
	}

	// @deprecated use task()
    public function tasks() {
        return $this->belongsTo(Task::class, 'task_id');
    }

	// @deprecated use campaign()
    public function campaigns() {
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

	public function campaign() {
		return $this->belongsTo(Campaign::class, 'campaign_id');
	}

    public function taskCustomerGroups() {
        return $this->belongsToMany(CustomerGroup::class, 'task_group_customers', 'task_id', 'customer_groups_id');
    }

    public function taskCustomers() {
        return $this->belongsToMany(Customer::class, 'task_customers', 'task_id', 'customer_id');
    }

	public function gateway()
	{
		return $this->belongsTo(ApiKey::class, 'gateway_key');
	}

    // Mutators
	public function getDateTimeSendingAttribute()
	{
		return $this->time_attempt_send;
	}

    // Query Scopes
    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->where($context->column(), $context->id());
        } else if ($context->getInstance() instanceof SubAccount) {
            return $query->whereHas('client.subAccounts', function($q) use ($context) {
                return $q->where('id', $context->id());
            });
        }

        return $query;
    }
}
