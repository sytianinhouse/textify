<?php

namespace ATM\Repositories\Tasks\JobsFailed;

use ATM\ContextInterface;
use ATM\Repositories\Client\Client;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\Settings\SiteSettingsRepository;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\TaskJobs\AuditLog\JobsAuditLogRepository;
use ATM\Repositories\TaskJobs\TaskJobsRepository;
use ATM\Repositories\Tasks\BaseJob;
use ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;
use ATM\Repositories\Tasks\JobsFailed\JobsFailed;
use ATM\Repositories\Tasks\JobsNextMonth\JobNextMonth;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use Carbon\Carbon;
use DeepCopy\f008\A;
use Illuminate\Http\Request;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;

class JobsFailedRepository extends ContextRepository
{
	public function __construct(
		JobsFailed $jobsFailed,
		ContextInterface $context,
		MessageBag $messageBag,
		SiteSettingsRepository $settingsRepo,
		JobsAuditLogRepository $jobsAuditLogsRepo,
		TaskJobsRepository $taskJobRepo,
		BaseJob $baseJob
	)
	{
		$this->model = $jobsFailed;
		$this->context = $context;
		$this->messageBag = $messageBag;
		$this->settingsRepo = $settingsRepo;
		$this->jobsAuditLogsRepo = $jobsAuditLogsRepo;
		$this->taskJobRepo = $taskJobRepo;
		$this->baseJob = $baseJob;
	}

	public function search(Request $request, $with = [])
	{
		$query = $this->scope($with);

		if ($request->filled('from_date') && $request->filled('to_date')) {
			$query = $query->where('time_attempt_send', '>=' , $request->from_date)
				->where('time_attempt_send', '<=' , $request->to_date);
		}

		if ($request->filled('subaccount_filter')) {
			$query = $query->where('subaccount_id', $request->subaccount_filter);
        }

		if ($request->filled('subaccount_id')) {
			$query = $query->where('subaccount_id', $request->subaccount_id);
        }

		if ($request->filled('client_id')) {
			$query = $query->where('client_id', $request->client_id);
        }

		if ($request->filled('task_id')) {
			$query = $query->where('task_id', $request->task_id);
		}


		if ($request->filled('campaign_filter')) {
			$query = $query->where('campaign_id', $request->campaign_filter);
		}

		if ( $request->has('per_page') ) {
			return $query->paginate($request->per_page ?: self::PER_PAGE);
		} else {
			return $query->get();
		}
	}

	public function update(JobsFailed $JobsFailed, Request $request)
	{
		$date              = new Carbon( $request['date_to_send'] );
		$time              = new Carbon( $request['time_to_send'] );
		$time_attempt_send = "{$date->format('Y-m-d')} {$time->format('H:i:s')}";

		$JobsFailed->customer_name   = $request['customer_name'];
		$JobsFailed->customer_number = $request['customer_number'];
		$JobsFailed->time_attempt_send = $time_attempt_send;

		$this->validate($JobsFailed);
		$JobsFailed->save();

		return $JobsFailed;
	}

	public function delete( $JobsFailed )
	{
		$JobsFailed = $this->model->find($JobsFailed);
		$JobsFailed->delete();

		return $JobsFailed;
	}

	public function createFailedJob(Request $request, $datetimeAttempt, $task, $errorResponse = 'Something went wrong.')
	{
        // $jobFailed = $this->model;
        $jobFailed = new JobsFailed;

		$jobFailed->time_attempt_send = $datetimeAttempt->toDateTimeString();
		$jobFailed->sms_response = ''; // palitan to na dapat base sa response from third party
		$jobFailed->error_log = $errorResponse; // palitan to na dapat base sa response from third party

		$jobFailed->customer_name = $request->name ?: '';
		$jobFailed->customer_number = $request->number;
		$jobFailed->gateway_key = $task->gateway_key ?: null;
		$jobFailed->message = $request->message;
		$jobFailed->credits_consumed = $request->cost;
		$jobFailed->campaign_id = $task->campaign_id;
		$jobFailed->task_id = $task->id;
        $jobFailed->parent_task_id = $request->parent_task_id;
		$jobFailed->subaccount_id = $task->subaccount_id;
		$jobFailed->client_id = $task->client_id;
		$jobFailed->source = $request->ip();
		$jobFailed->save();

		return $jobFailed;
	}

	/**
	 * Use to create Job from failed record
	 * @param Request $request
	 * @param \ATM\Repositories\Tasks\JobsFailed\JobsFailed $jobsFailed
	 * @return array
	 */
	public function createJob(Request $request, JobsFailed $jobsFailed)
	{
		try {
			$this->baseJob->createJob($request, $jobsFailed);

			if( $request->filled('from_job_failed') && $request['from_job_failed'] ) {
				$this->delete($request['job_failed_id']);
			}

			return ['success' => true];
		} catch (\ATM\Validation\ValidationException $e) {
			return ['success' => false];
		}
	}

	public function resendFailedJob($request, $jobFailed)
	{
		$smsFacade = resolve(\ATM\Repositories\Sms\SmsFacade::class);
		$subAccount = $jobFailed->subAccount;
		$task = $jobFailed->task;

		$rules = [
			'number' => 'required',
		];

		$validator = Validator::make($request->all(), $rules, [
			'number.required' => 'Recipient number should have a value and must be a valid number'
		]);

		if ($validator->fails()) {
			$this->errors = $validator->messages()->merge($this->errors);
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		if(!$request->filled('change_date_time')) {
            $today = new Carbon();
            if( $jobFailed->time_attempt_send->gt($today) ) {
                $request['date_to_send'] = $jobFailed->time_attempt_send->format('m/d/Y');
                $request['time_to_send'] = $jobFailed->time_attempt_send->format('H:i:s');
                $job = $this->createJob($request, $jobFailed);
            } else {
                $job = $smsFacade->createImmediateJob($request, $subAccount, $task);
            }
        } else {
            $job = $this->createJob($request, $jobFailed);
		}

		if (isset($job['job'])) {
			if ($job['job'] instanceof JobFailed) {
				$this->errors = $this->messageBag->add('job_error', $job['message']);
				throw new \ATM\Validation\ValidationException($this->errors);
			}
		}

		return $job;
	}
}
