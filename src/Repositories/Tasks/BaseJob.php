<?php

namespace ATM\Repositories\Tasks;

use ATM\ContextInterface;
use ATM\Repositories\BaseRepository;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\TaskJobs\TaskJobsRepository;
use ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;
use ATM\Repositories\Tasks\JobsCompleted\JobsCompleted;
use ATM\Repositories\Tasks\JobsFailed\JobsFailed;
use ATM\Repositories\Tasks\JobsNextMonth\JobNextMonth;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class BaseJob extends BaseRepository
{
	protected $jobCompQuery;
	protected $jobFailQuery;
	protected $jobTodayQuery;
	protected $jobTomQuery;
	protected $job14thQuery;
	protected $job15thQuery;
	protected $jobNextMonthQuery;

	public function __construct(
		JobsCompleted $jobCompleted,
		JobsFailed $jobFailed,
		JobToday $jobToday,
		JobTomorrow $jobTomorrow,
		JobAfterTomorrowTo14thDay $job14th,
		Job15thDayToEndMonth $job15th,
		JobNextMonth $jobNextMonth,
		TaskJobsRepository $taskJobRepo,
		ContextInterface $context,
		MessageBag $messageBag
	)
	{
		$this->jobCompleted = $jobCompleted;
		$this->jobFailed = $jobFailed;
		$this->jobToday = $jobToday;
		$this->jobTomorrow = $jobTomorrow;
		$this->job14th = $job14th;
		$this->job15th = $job15th;
		$this->jobNextMonth = $jobNextMonth;
		$this->taskJobRepo = $taskJobRepo;
		$this->context = $context;
		$this->messageBag = $messageBag;
	}

	public function createJob(Request $request, $job)
	{
		$today = Carbon::now()->startOfDay(); // This object represents current date/time
		$subAccount = $job->subAccount;
		$dateToSend =  Carbon::parse($request->date_to_send); // get the date set

		$messageBag = new MessageBag();
		$smsFacade = resolve(\ATM\Repositories\Sms\SmsFacade::class);

		$startOfThisMonth = $today->copy()->startOfMonth();
		$firstDayofNextMonth = $startOfThisMonth->copy()->addMonth()->startOfMonth(); // get the first day of next month

		$matchDate = $dateToSend->copy()->startOfDay(); // reset time part, to prevent partial comparison

		$messageLength = strlen($job->message);

		$diffDays = $today->diffInDays($matchDate);

		if ($subAccount->isBeechoo()) { // beechoo
			$hasNumber = $request->number && $request->number != NULL && $request->number != '';
			$formattedNumber = $hasNumber ? formattedNumber($request->number) : null;
			$canSend = $formattedNumber ? $subAccount->isCreditSufficientForTaskWithNumber($messageLength, $formattedNumber) : false;
		} else {
			$canSend = $subAccount->isCreditSufficientForTask($messageLength,null);
		}

		// if(!$subAccount->isCreditSufficientForTask($messageLength,null) ) {
		if(!$canSend ) {
			$diffDays = null;
		}

		// Insert sa job for next month
		$allArraysJobNextMonth = [];
		$allArraysJob15thDay = [];
		$allArraysJob14thDay = [];
		$allArraysJobTomorrow = [];
		$allArraysJobToday = [];

		$currentDateTime = Carbon::now()->toDateTimeString();

		if ( $request->number && $request->number != NULL && $request->number != '' ) {

			$whenToSend = Carbon::parse($request->date_to_send .' '. $request->time_to_send); // get when to send.

			$draw = [
				'customer_id' => $job->customer_id,
				'customer_name' => strtoupper($job->customer_name),
				'customer_number' => $request->number,
				'gateway_key' => $job->gateway_key,
				'campaign_id' => $job->campaign_id,
				'credits_consumed' => $job->credits_consumed,
				'when_to_send' => $whenToSend->toDateTimeString(),
				'message' => $job->message,
				'task_id' => $job->task_id,
				'subaccount_id' => $job->subaccount_id,
				'client_id' => $job->client_id,
				'source' => $request->ip(),
				'created_at' => $currentDateTime,
				'updated_at' => $currentDateTime,
			];

			if ($diffDays) {
				if ($diffDays == 1) {
					$allArraysJobTomorrow[] = $draw;
				} elseif($diffDays >= 2 && $diffDays <= 14) {
					$allArraysJob14thDay[] = $draw;
				} elseif($diffDays >= 15) {
					if ($matchDate->gte($firstDayofNextMonth)) {
						$allArraysJobNextMonth[] = $draw;
					} else {
						$allArraysJob15thDay[] = $draw;
					}
				}
			} else {
				if ($diffDays == 0) {
					$allArraysJobToday[] = $draw;
				} else {
					$this->errors = $this->messageBag->add('job_error', 'No jobs to create.');
					throw new \ATM\Validation\ValidationException($this->errors);
				}
			}
		}

		$subAccount = $job->subAccount;
		$credits = $job->credits_consumed;
		if ($subAccount->credit) {
			$credit = $subAccount->credit;
			if ($credits > $credit->pending_balance) {

				$this->errors = $messageBag->add('job_error', 'Your credit is not enough for the creation of this SMS task.');
				throw new \ATM\Validation\ValidationException($this->errors);
			}
			$smsFacade->minusPendingCreditPerSubaccount($credits, $subAccount);
		}

		$jobsData = [
			'allArraysJobNextMonth' => $allArraysJobNextMonth,
			'allArraysJob15thDay' => $allArraysJob15thDay,
			'allArraysJob14thDay' => $allArraysJob14thDay,
			'allArraysJobTomorrow' => $allArraysJobTomorrow,
			'allArraysJobToday' => $allArraysJobToday,
		];

		return $this->taskJobRepo->continueSendingJobs($jobsData);
	}

	private function defaultCols($isFailed = false, $isCompleted = false)
	{
		$cols = [
			'id', 'customer_id', 'customer_name', 'customer_number', 'gateway_key', 'credits_consumed',
			'message', 'campaign_id', 'task_id', 'subaccount_id', 'client_id'
		];

		if ($isFailed) {
			$cols = array_merge($cols, ['error_log', 'time_attempt_send']);
		} else {
			$cols[] = 'when_to_send';
		}

		if ($isCompleted) {
			$cols[] = 'sms_response';
        }

		return $cols;
	}

	public function getAllJobs(Request $request, $paginate = false, $dashboard = false, $bookingTask = false, $with = [])
	{
        $this->jobCompQuery = $this->jobCompleted->ofContext($this->context)->select($this->defaultCols(false, true))->with($with)->orderBy('when_to_send');
		$this->jobFailQuery = $this->jobFailed->ofContext($this->context)->select($this->defaultCols(true))->with($with)->orderBy('time_attempt_send');
		$this->jobTodayQuery = $this->jobToday->ofContext($this->context)->select($this->defaultCols())->with($with)->orderBy('when_to_send');
		$this->jobTomQuery = $this->jobTomorrow->ofContext($this->context)->select($this->defaultCols())->with($with)->orderBy('when_to_send');
		$this->job14thQuery = $this->job14th->ofContext($this->context)->select($this->defaultCols())->with($with)->orderBy('when_to_send');
		$this->job15thQuery = $this->job15th->ofContext($this->context)->select($this->defaultCols())->with($with)->orderBy('when_to_send');
		$this->jobNextMonthQuery = $this->jobNextMonth->ofContext($this->context)->select($this->defaultCols())->with($with)->orderBy('when_to_send');

		if ($request->filled('from_date')) {
            $fromDate = (new Carbon($request->from_date))->startOfDay();
			$this->buildQueryWhere('where', 'when_to_send', '>=', $fromDate->toDateTimeString(), 'time_attempt_send');
		}

		if ($request->filled('to_date')) {
            $toDate = (new Carbon($request->to_date))->endOfDay();
			$this->buildQueryWhere('where', 'when_to_send', '<=', $toDate->toDateTimeString(), 'time_attempt_send');
		}

		if ($request->filled('task_id')) {
            $this->buildQueryWhere('where', 'task_id', '=', $request->task_id);
        } else if ($request->filled('task_ids')) {
            $this->buildQueryWhereIn('whereIn', 'task_id', $request->task_ids);
        }

		if ($request->filled('client_id')) {
			$this->buildQueryWhere('where', 'client_id', '=', $request->client_id);
        }

		if ($request->filled('subaccount_id')) {
			$this->buildQueryWhere('where', 'subaccount_id', '=', $request->subaccount_id);
        }

		$jobs = collect();
		$jobs = $jobs->merge($this->jobTodayQuery->get());
		$jobs = $jobs->merge($this->jobTomQuery->get());
		$jobs = $jobs->merge($this->job14thQuery->get());
		$jobs = $jobs->merge($this->job15thQuery->get());
		$jobs = $jobs->merge($this->jobNextMonthQuery->get());

        if( !$request->dashboard_jobs ) {
            $jobs = $jobs->merge($this->jobFailQuery->get());
            $jobs = $jobs->merge($this->jobCompQuery->get());
        }

		$jobs = $jobs->sortByDesc(function($item) {
			if ($item instanceof JobsFailed) {
				return $item->time_attempt_send;
			} else {
				return $item->when_to_send;
			}
		});

		if ($paginate) {
            if( $bookingTask != false ) {
                $jobs = $this->paginateCollection($jobs, $request->page, $request['per_page']);
                $jobs->map(function($item, $key) {
                    // dd(get_class($item));
                    $item['table_name'] = $item->table;
                    $item['customer_number'] = formattedNumber($item->customer_number, false, true);
                    if( $item->table == 'jobs_failed' ) {
                        $item['view_url'] = ( $this->context->getInstance() instanceof SubAccount ) ? sroute('sub.jobs.jf.edit', [$item]) : route('client.jobs.jf.edit', [$item]);
                        $item['resend_url'] = ( $this->context->getInstance() instanceof SubAccount ) ? sroute('sub.jobs.jf.resend', $item) : route('client.jobs.jf.resend', [$item]);
                        $item['date_time_formatted'] =  $item->time_attempt_send->format('M d, Y, h:i A');
                        $item['error_log'] =  $item->error_log;
                    } else {
                        $item['date_time_formatted'] = $item->when_to_send->format('M d, Y h:i A');
                        $item['view_url'] =( $this->context->getInstance() instanceof SubAccount ) ? sroute('sub.jobs.view', [$item, get_class($item)]) : route('client.jobs.view', [$item, get_class($item)]);
                    }
                    return $item;
                });
                $jobs = $jobs->sortBy('date_time_formatted');
                $task_ids = $request->task_ids;
                return compact('jobs', 'task_ids');
            } else {
                $jobs = $this->paginateCollection($jobs, $request->page, self::PER_PAGE);
            }
		} elseif($dashboard) {
            $jobs = $jobs->take(20); // check kung dapat ba na take(20) talaga to
        }

		return $jobs;
	}

	private function buildQueryWhere($clause, $key, $logic, $value, $failedSpecialCol = '')
	{
		$this->jobCompQuery->{$clause}($key, $logic, $value);
		$this->jobFailQuery->{$clause}($failedSpecialCol ?: $key, $logic, $value);
		$this->jobTodayQuery->{$clause}($key, $logic, $value);
		$this->jobTomQuery->{$clause}($key, $logic, $value);
		$this->job14thQuery->{$clause}($key, $logic, $value);
		$this->job15thQuery->{$clause}($key, $logic, $value);
		$this->jobNextMonthQuery->{$clause}($key, $logic, $value);
    }

	private function buildQueryWhereIn($clause, $key, $value, $failedSpecialCol = '')
	{
		$this->jobCompQuery->{$clause}($key, $value);
		$this->jobFailQuery->{$clause}($failedSpecialCol ?: $key, $value);
		$this->jobTodayQuery->{$clause}($key, $value);
		$this->jobTomQuery->{$clause}($key, $value);
		$this->job14thQuery->{$clause}($key, $value);
		$this->job15thQuery->{$clause}($key, $value);
		$this->jobNextMonthQuery->{$clause}($key, $value);
	}

	public function updateJob(Request $request, $job)
	{
		if ($job instanceof JobsCompleted || $job instanceof JobsFailed) return;

		$this->validateData($request);

		if (count($this->errors) > 0) {
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		$request['customer_number'] = preg_replace('/\D+/', '', $request->customer_number);
		$whenToSend = Carbon::parse($request->date_to_send .' '. $request->time_to_send); // get when to send.

		if ($whenToSend) {
			if($request->filled('change_date_time')) {
				if (!$job->when_to_send->eq($whenToSend) && $whenToSend->diffInDays($job->when_to_send) > 0) {
					$job = $this->createJob($request, $job);
				}
			} else {
				$job->customer_name = $request->customer_name;
				$job->customer_number = $request->customer_number;
				$job->when_to_send = $whenToSend->toDateTimeString();

				if (isClientRelux() || isClientDreamNails() || isClientNailLuxe()) {
					$subAccount = $job->subAccount;
					$messageLength = strlen($request->message);
					$cost = $subAccount->getCreditAmount($messageLength);
					$canSend = $subAccount->isCreditSufficientForTask($messageLength,null);
					if ($canSend) {
						$job->message = $request->message;
						$job->credits_consumed = $cost;
					}
				}

				$job->save();
			}

			return $job;
		}

		$this->errors = $this->messageBag->add('fatal_error', 'There was an error while saving the jobs, please try again later.');
		throw new \ATM\Validation\ValidationException($this->errors);
	}

	public function deleteJob($job)
	{
		$subAccount = $job->subAccount;

		if ($job instanceof JobsCompleted) return;

		if (!$job instanceof JobsCompleted && !$job instanceof JobsFailed) {
			$subAccount->modifyPendingBalance($job->credits_consumed);
			$job->delete();

			return true;
		}

		return;
	}

	public function validateData(Request $request)
	{
		$rules = [
			'customer_name'   => 'required',
			'customer_number' => 'required',
			'date_to_send'    => 'required|date',
			'time_to_send'    => 'required'
		];

		$validator = \Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			$this->errors = $validator->messages()->merge($this->errors);
		}
	}
}
