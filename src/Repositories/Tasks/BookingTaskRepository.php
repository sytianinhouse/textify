<?php

namespace ATM\Repositories\Tasks;

use ATM\ContextInterface;
use ATM\Repositories\BaseRepository;
use ATM\Repositories\Client\Client;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\Customers\CustomersRepository;
use ATM\Repositories\Settings\SiteSettings;
use ATM\Repositories\Settings\SiteSettingsRepository;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\TaskJobs\AuditLog\JobsAuditLogRepository;
use ATM\Repositories\TaskJobs\TaskJobsRepository;
use ATM\Repositories\Tasks\JobsFailed\JobsFailedRepository;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Tasks\TaskRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;

class BookingTaskRepository extends ContextRepository {

	/*
	 * @var Illuminate\Database\Eloquent\Model
	 */
	protected $model;

	public function __construct(
		Task $task,
		TaskJobsRepository $taskJobRepo,
		SiteSettingsRepository $settingsRepo,
		CustomersRepository $customerRepo,
		ContextInterface $context,
		MessageBag $messageBag,
		JobsAuditLogRepository $jobsAuditLogsRepo,
        JobsFailedRepository $jobFailedRepo,
        TaskRepository $taskRepository
	)
	{
		$this->model = $task;
		$this->context = $context;
		$this->taskJobRepo = $taskJobRepo;
		$this->settingsRepo = $settingsRepo;
		$this->customerRepo = $customerRepo;
		$this->messageBag = $messageBag;
		$this->jobsAuditLogsRepo = $jobsAuditLogsRepo;
        $this->jobFailedRepo = $jobFailedRepo;
        $this->taskRepository = $taskRepository;
	}

	public function search(Request $request, $with = [], $parentOnly = TRUE)
	{
		$query = $this->scope($with);

		if ($request->filled('sub_account_id')) {
            $query = $query->where('subaccount_id', $request->sub_account_id);
        }

		if ($request->filled('parent_task_id')) {
            $query = $query->where('parent_task_id', $request->parent_task_id);
        }

		if ($request->filled('filter_task_type_id')) {
			$query = $query->where('task_type_id', $request->filter_task_type_id);
		}

		if ($request->filled('campaign_filter')) {
			$query = $query->where('campaign_id', $request->campaign_filter);
		}

		if ($request->filled('subaccount_filter')) {
			$query = $query->where('subaccount_id', $request->subaccount_filter);
        }

        if ( $parentOnly ) {
			$query = $query->where('parent_task_id', null);
        }

		if ( $request->filled('filter_date_by') ) {
			$fromDate = new Carbon($request->from_date);
			$toDate = new Carbon($request->to_date);

			if ($request->filter_date_by == 'created_at') {
				if ( $request->filled('from_date') && $request->filled('to_date') ) {
					$query = $query->whereBetween('created_at', [$fromDate->startOfDay()->toDateTimeString(),$toDate->endOfDay()->toDateTimeString()]);
				}
			} else {
				if ( $request->filled('from_date') && $request->filled('to_date') ) {
					$query = $query->whereBetween('date_to_send', [$fromDate->startOfDay()->toDateTimeString(),$toDate->endOfDay()->toDateTimeString()]);
				}
			}
		}

		if ($request->filled('per_page')) {
			return $query->paginate($request->per_page);
		} else {
			return $query->get();
		}
	}

	public function makeSmsFormatted($task, $customer)
	{
		$message = str_replace(
			['[first_name]', '[last_name]', '[email]', '[telephone]', '[address]'],
			[$customer->first_name, $customer->last_name, $customer->email, $customer->telephone, $customer->address_1],
			$task->message
		);

		return $message;
	}

	public function creditComputation($message)
	{
		$settings = SiteSettings::getSettings();
		$limit = $settings->sms_character_limit ?: Task::CHARACTER_LIMITER;
		$rate = $settings->sms_credit_pricec_per_limit ?: Task::CREDIT_LIMITER;

		$length = strlen( $message );
		if ( $length > 0 ) {
			if ( $length > $limit ) {
				return number_format(($length / $limit) * $rate, 2);
			} else {
				return number_format($rate);
			}
		}
	}

	public function validateData(Request $request)
    {
        $rules = [];

        if ($request->task_type_id == \ATM\Repositories\Tasks\Task::BOOKING_TASK) {
            $rules = [
                'campaign'     => 'required',
                'time_to_send' => 'required',
                'message'      => 'required'
            ];
        }

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

	public function add(Request $request, Client $client)
	{
        $task = $this->model;
        $now = Carbon::now();

        $request['subaccount_id'] = $request->data['subaccount_id'];
        $request['send_immediate'] = $request->data['send_immediate'];
        $request['sms_reminder'] = $request->data['sms_reminder'];
        $request['campaign'] = $request->data['campaign']['value'];
        $request['message'] = $request->data['message'];

        $request['sub_account_id'] = $request->params['sub_account_id'];
        $request['client_id'] = $request->params['clientID'];

		$this->validateData($request);

        if (count($this->errors) > 0) {
            throw new \ATM\Validation\ValidationException($this->errors);
        }

        if ($request->filled('subaccount_id')) {
            $reqSubAccount = $request->subaccount_id;
        }

        if ($request->filled('sub_account_id')) {
        	$reqSubAccount = $request->sub_account_id;
        }

        $subAccount = $client->subAccounts()->find($reqSubAccount);

		$task->client_id = $client->id;
		$task->subaccount_id = $reqSubAccount;
		$task->entry_id = null;
		$task->campaign_id = $request->campaign;

        $startOfYearTemp = Carbon::now()->startOfYear();
		$dateToSend = Carbon::now()->startOfYear();

        if ( $request->filled('data.send_after.days') && $request->data['send_after']['days'] != NULL ) {
            $toBeAdded = (int) $request->data['send_after']['days'];
        	$dateToSend = $startOfYearTemp->addDays($toBeAdded);
        }

		$task->date_to_send = (isset($dateToSend)) ? $dateToSend : null ;

		if ( $request->filled('data.send_immediate') && $request->data['send_immediate'] != FALSE) {
            // $task->time_to_send = date("H:i", strtotime('00:00'));
            $task->time_to_send = null;
			$task->date_to_send = null;
		} else {
			$task->time_to_send = date("H:i", strtotime($request->time_to_send));
		}

		$task->active = $request->data['status'] ? true : false;
		// $task->success_sent_msg_count = 1;
		// $task->msg_count = 1;
		$task->has_children = (count($request->data['sms_reminder_holder']) > 0) ? true : false;
		$task->send_notif = $request->filled('data.sms_notification') ? $request->data['sms_notification'] : null;
		$task->message = $request->filled('data.message') ? strip_tags($request->data['message']) : null;
		$task->task_type_id = $request->filled('task_type_id') ? $request->task_type_id : null;
		$task->gateway_key = $subAccount->getApiKeyId();
    	$task->task_key = sha1($client->id . '--' . $task->subAccount->id . '--' . $now->toDateTimeString());
        $task->save();

        if( count($request->data['sms_reminder_holder']) > 0 ) {
            $reminder = $this->createReminder($request, $task->id, $client);
        }

        return $task;


	}

    public function createReminder(Request $request, $taskID, $client, $fromUpdate = false) {
        $active = $request->data['status'];
        if( count($request->data['sms_reminder_holder']) > 0 ) {
            foreach( $request->data['sms_reminder_holder'] as $key => $reminder ) {
                $taskModel = ( $fromUpdate && isset($reminder['id']) ) ? $this->model->find($reminder['id']) : new Task;
                $task = $taskModel;
                $now = Carbon::now();

                $send_immediate = null;
                $campaign = $request->data['campaign']['value'];
                $sub_account_id = $request->params['sub_account_id'];
                $client_id = $request->params['clientID'];
                $subAccount = $client->subAccounts()->find($sub_account_id);

                $task->client_id = $client_id;
                $task->parent_task_id = $taskID;
                $task->subaccount_id = $sub_account_id;
                $task->entry_id = null;
                $task->campaign_id = $campaign;

                $startOfYearTemp = Carbon::now()->startOfYear();
                if ( $reminder['send_before']['days'] != '') {
                    $toBeAdded = (int) $reminder['send_before']['days'];
                    $dateToSend = $startOfYearTemp->copy()->addDays($toBeAdded);
                }

                $task->date_to_send = (isset($dateToSend)) ? $dateToSend : null ;

                $hours = ($reminder['send_before']['hours'] != null ) ? preg_replace('/\D/', '',$reminder['send_before']['hours']) : null;
                $mins = ($reminder['send_before']['mins'] != null ) ? preg_replace('/\D/', '', $reminder['send_before']['mins']) : null;
                $time_to_send = Carbon::now()->startOfDay()->hour($hours)->minute($mins)->format('H:i');

                $task->time_to_send = $time_to_send;
                $task->active = $active ? true : false;
                // $task->success_sent_msg_count = 1;
                // $task->msg_count = 1;
                $task->message =  strip_tags($reminder['message']);
                $task->task_type_id = $request->filled('task_type_id') ? $request->task_type_id : null;
                // $task->gateway_key = $subAccount->getApiKeyId();
                // $task->task_key = sha1($client->id . '--' . $task->subAccount->id . '--' . $now->toDateTimeString());
                $task->save();
            }
            return true;
        }
    }

	public function update($taskId, Request $request, $client)
	{
        $task = $this->model->find($taskId);

        $request['subaccount_id'] = $request->data['subaccount_id'];
        $request['send_immediate'] = $request->data['send_immediate'];
        $request['sms_reminder'] = $request->data['sms_reminder'];
        $request['campaign'] = $request->data['campaign']['value'];
        $request['message'] = $request->data['message'];

        $request['sub_account_id'] = $request->params['sub_account_id'];
        $request['client_id'] = $request->params['clientID'];

		$task->subaccount_id = $request->subaccount_id;
		$task->entry_id = null;
		$task->campaign_id = $request->campaign;

        $startOfYearTemp = Carbon::now()->startOfYear()->startOfDay();
		$dateToSend = Carbon::now()->startOfYear();

        if ( $request->filled('data.send_after.days') && $request->data['send_after']['days'] != NULL ) {
            $toBeAdded = (int) $request->data['send_after']['days'];
            $dateToSend = $startOfYearTemp->addDays($toBeAdded);
        }
        $task->date_to_send = (isset($dateToSend)) ? $dateToSend : null ;

		if ( $request->filled('data.send_immediate') && $request->data['send_immediate'] != FALSE) {
            $task->time_to_send = null;
            $task->date_to_send = null ;
		} else {
            $task->time_to_send = date("H:i", strtotime($request->time_to_send));
            // if ( $request->filled('data.send_after.days') && $request->data['send_after']['days'] != NULL ) {
            //     $task->date_to_send = $task->date_to_send;
            // }
            // dd($task->time_to_send, $task->date_to_send);
		}

        $task->active = $request->data['status'] ? true : false;
        $task->has_children = (count($request->data['sms_reminder_holder']) > 0) ? true : false;
        $task->send_notif = $request->data['sms_notification'] ? true : false;
		$task->message = $request->filled('message') ? strip_tags($request->message) : null;

        $task->save();

        if( count($request->data['toBeRemoved']) > 0 ) {
            $this->model->whereIn('id', $request->data['toBeRemoved'])->delete();
        }

        if( count($request->data['sms_reminder_holder']) > 0 ) {
            $reminder = $this->createReminder($request, $task->id, $client, true);
        }

       	return $task;
	}

	public function delete(Task $task, Request $request)
	{
        if( $task->has_children == 1 ) {

            $toBeDeleted = array($task->id);
            $request['parent_task_id'] = $task->id;
            $request['parent_task_id'] = $task->id;
            $childTask = $this->search($request, [], FALSE);

            foreach( $childTask as $task ) {
                array_push($toBeDeleted, $task->id);
            }

            $task = DB::table('task')->whereIn('id', $toBeDeleted)->update(['deleted_at' => Carbon::now() ]);

        } else {

            $task = $this->model->find($task->id); // removed data
            $task->delete();

        }

        if ($request->filled('dont_detach')) {
			$task->taskCustomers()->detach(); // removed customers
        }

        return $task;
	}

	public function getDataOfCustomerFilter(Request $request){

		if ( $request ) {
			$query = Customer::whereHas('subAccounts', function($q) use($request) {
	            $q->where('subaccount_id', $request->subaccount_id);
	        })->whereNotNull('mobile');
	        // customer group
	        if ($request->filled('customergroup')) {
	            $query = $query->whereHas('customerGroups', function($q) use($request) {
	                $q->where('customer_group_id', $request->customergroup);
	            });
	        }
	        // monthfilter
	        if ($request->filled('monthfilter')) {
	            // $query = $query->whereMonth('b_month', '=',  $request->monthfilter);
	            $query = $query->where('b_month', $request->monthfilter);
	        }
	        // yearpicker
	        if ($request->filled('yearpicker')) {
	            // $query = $query->whereYear('birthdate', '=',  $request->yearpicker);
	            $query = $query->where('b_year', $request->yearpicker);
	        }
	        // dayfilter
	        if ($request->filled('dayfilter')) {
	            // $query = $query->whereDay('birthdate', '=',  $request->dayfilter);
	            $query = $query->where('b_day', $request->dayfilter);
	        }
	        // gender
	        if ($request->filled('gender')) {
	            $query = $query->where('gender', $request->gender);
	        }
	        // city
	        if ($request->filled('city')) {
	            $query = $query->where('city', $request->city);
	        }
		}
        return $customers = $query->get();
        // return $customers = $query->pluck('id');
	}

	public function getDataOfCustomerByIdFilter(Request $request){

		if ( $request ) {
			$query = Customer::whereHas('subAccounts', function($q) use($request) {
	            $q->where('subaccount_id', $request->sub_account_id);
	        })->whereNotNull('mobile');
	        // customer group
	        if ($request->filled('customergroup')) {
	            $query = $query->whereHas('customerGroups', function($q) use($request) {
	                $q->where('customer_group_id', $request->customergroup);
	            });
	        }
	        // monthfilter
	        if ($request->filled('monthfilter')) {
	            // $query = $query->whereMonth('b_month', '=',  $request->monthfilter);
	            $query = $query->where('b_month', $request->monthfilter);
	        }
	        // yearpicker
	        if ($request->filled('yearpicker')) {
	            // $query = $query->whereYear('birthdate', '=',  $request->yearpicker);
	            $query = $query->where('b_year', $request->yearpicker);
	        }
	        // dayfilter
	        if ($request->filled('dayfilter')) {
	            // $query = $query->whereDay('birthdate', '=',  $request->dayfilter);
	            $query = $query->where('b_day', $request->dayfilter);
	        }
	        // gender
	        if ($request->filled('gender')) {
	            $query = $query->where('gender', $request->gender);
	        }
	        // city
	        if ($request->filled('city')) {
	            $query = $query->where('city', $request->city);
	        }
		}
        // return $customers = $query->get();
        return $customers = $query->pluck('id');
	}

	public function searchByKey($taskKey)
	{
		$query = $this->model;

		$query = $query->where('task_key', $taskKey);

		return $query->first();
	}

	/*
	 * For bulk API of jobs
	 */
	public function addJobs(Request $request)
	{
		$taskRepo = resolve(TaskRepository::class);
		if ($request->filled('data')) {
			$data = $request->data;
			$totalJobs = count($data);
			$currentJob = 0;
			unset($request['data']);
			foreach ($data as $key => $datum) {
				$appKey = isset($datum['app_key']) ? $datum['app_key'] : null;
				$task = $appKey ? $taskRepo->searchByKey($appKey) : null;

				if ($task) {
					$request['security_key'] = isset($datum['security_key']) ? $datum['security_key'] : '';
					$request['app_key'] = isset($datum['app_key']) ? $datum['app_key'] : '';
					$request['last_name'] = isset($datum['last_name']) ? $datum['last_name'] : '';
					$request['first_name'] = isset($datum['first_name']) ? $datum['first_name'] : '';
					$request['number'] = isset($datum['number']) ? $datum['number'] : '';
					$request['items'] = isset($datum['items']) ? $datum['items'] : '';

					$this->addJob($request, $task, true);
					$currentJob++;

					if ($currentJob >= $totalJobs) {
						return 'All jobs has been processed';
					}
				} else {
					$this->errors = $this->messageBag->add('fatal_error', 'Cannot find a created or active task with key provided');
				}

			}
		} else {
			$this->errors = $this->messageBag->add('fatal_error', 'No data or jobs found for SMS sending');
		}

		throw new \ATM\Validation\ValidationException($this->errors);
	}

	public function addJob(Request $request, Task $task, $taskChild = [] ,$fromApi = false)
    {
        logger('TO ADD BOOKING JOB DATA');
		logger($request->all());
		
		Log::info('Booking job was called: ' . now());
		
		if (!$request->filled('security_key')) {
		    logger('BOOKING NO SECURITY KEY');
			$this->errors = $this->messageBag->add('fatal_error', 'Fatal error on making the request.');
		}

		if (!$request->filled('app_key')) {
		    logger('BOOKING NO APP KEY');
			$this->errors = $this->messageBag->add('fatal_error', 'App key does not exist.');
		}

		$subAccount = SubAccount::where('security_key', $request->security_key)->get()->first();

		if (!$subAccount) {
		    logger('BOOKING NO SUBACCOUNT');
			$this->errors = $this->messageBag->add('fatal_error', 'Fatal error on making the request.');
        };

		if ( !$request->filled('date')
			|| !$request->filled('time')
			|| !$request->filled('number') ) {
			    logger('BOOKING NO DATE, TIME OR NUMBER');
			$this->errors = $this->messageBag->add('fatal_error', 'Invalid parameters.');
		}
		$number = formattedNumber($request->number, true);
		
		logger('FORMATTED NUMBER FOR BOOKING - ' . $number);

        $returnMessage = '';
        if ($number) {
			$subAccount = SubAccount::where('security_key', $request->security_key)->first();
			
			logger($subAccount);
			logger($subAccount->account_name);
			$smsFacade = resolve(\ATM\Repositories\Sms\SmsFacade::class);
			$today = new Carbon(); // Current Date Time

			$startOfThisMonth = $today->copy()->startOfMonth();
			$firstDayofNextMonth = $startOfThisMonth->copy()->addMonth()->startOfMonth(); // get the first day of next month

        	$taskChild->each(function($item, $value) use (
        		&$request, &$returnMessage, &$task, &$number, &$subAccount, &$today, &$smsFacade, &$startOfThisMonth, &$firstDayofNextMonth
			) {
				$addDays = $item->ots_days; // Get Days is Task Date Time
				$addHours = $item->ots_hours; // Get Hours in Task Date Time
				$addMins = $item->ots_minutes; // Get Minutes in Task Date Time
				$booking_date_time = new Carbon($request->date.' '.$request->time);

				$notification = false;
				$reminder = false;
				$requestDateTime = new Carbon(); // Default value

				$isBeechoo = $item->client_id == 70;
				$isPiandre = $item->client_id == 67;
				$isDreamNails = $item->client_id == 93;
				
				logger('IS BEECHOO - ' . $isBeechoo);
				logger('IS PIANDRE - ' . $isPiandre);

				if( $item->parent_task_id == NULL ) {
					$notification = true;
					if ($addDays) {
						$daysAdded = $today->copy()->addDays($addDays);
						$requestDateTime = $daysAdded->startOfDay()->addHours($addHours)->addMinutes($addMins); // Request Date Time and add Hours & Minutes
					} else {
						$requestDateTime = $today->copy()->addHours($addHours)->addMinutes($addMins); // Request Date Time and add Hours & Minutes
					}
				} else if( $item->parent_task_id != NULL ) {
					$reminder = true;
					if ($addDays) {
						$daysAdded = $booking_date_time->copy()->subDays($addDays);
                        // $requestDateTime = $daysAdded->startOfDay()->addHours($addHours)->addMinutes($addMins); // Request Date Time and add Hours & Minutes
						if ($isBeechoo) {
							if ($addHours > 0 || $addMins > 0) {
								$requestDateTime = $daysAdded->startOfDay()->addHours($addHours)->addMinutes($addMins); // Request Date Time and add Hours & Minutes
								// logger('ADD HOURS OR MINUTES ONLY - ' . $requestDateTime->format('Y-m-d H:i:s'));
							} else {
								$requestDateTime = $daysAdded->startOfDay()->addHours(10);
								// logger('ADD DAY - ' . $requestDateTime->format('Y-m-d H:i:s'));
							}
						} else {
							$requestDateTime = $daysAdded->startOfDay()->addHours($addHours)->addMinutes($addMins); // Request Date Time and add Hours & Minutes
							// logger('ADD DAY AGAIN - ' . $requestDateTime->format('Y-m-d H:i:s'));
						}
					} else {
						$requestDateTime = $booking_date_time->copy()->subHours($addHours)->subMinutes($addMins); // Request Date Time and sub Hours & Minutes
						// logger('ELSE ADD HOURS OR MINUTES ONLY - ' . $requestDateTime->format('Y-m-d H:i:s'));
					}
				}

				// Trap kapag yung sending date is ahead sa booking date
				if ($requestDateTime->gte($booking_date_time)) {
					if ($notification) {
						// If yung notification is greater na sa booking time and date, gagawing immediate sending na siya
						$requestDateTime = $today->copy();
					}
				}

				if ($requestDateTime->lt($today)) {
					if ($reminder && !$isBeechoo) {
						$requestDateTime = $today->copy()->addMinutes(10);
						// If yung confirmation sending is less than na sa current time, gagawing current time plus 10 mins yung sending
					}
				}

				$diffDays = $today->copy()->startOfDay()->diffInDays($requestDateTime);
				$taskKey = $request->app_key;
				$task = Task::where('task_key', $taskKey)->get()->first();
				
				logger($task);
				$request['parent_task_id'] = ( $task->id != $item->id ) ? $task->id : null;

				if (!$item->active) {
				    logger('This SMS task is no longer active and can\'t receive any more job request.');
					$this->errors = $this->messageBag->add('fatal_error', 'This SMS task is no longer active and can\'t receive any more job request.');
				}

				if (count($this->errors) > 0) {
				    logger('HAS ERRORS');
					throw new \ATM\Validation\ValidationException($this->errors);
				}

				if ($request->filled('number') && $request->number) {
				    logger('HAS NUMBER');
					$lName = $request->filled('last_name') ? $request->last_name : '';
					$fName = $request->filled('first_name') ? $request->first_name : '';
					$items = $request->filled('items') ? $request->items : '';
					$number = $request->filled('number') ? $request->number : '';

					$name = $fName.' '.$lName;
					$date = ($request->filled('date') ) ? $request->date : '';
					$time = ($request->filled('time') ) ? $request->time : '';
					$date_time = "{$date} {$time}";
					$business_phone_number = ($request->filled('business_phone_number') ) ? $request->business_phone_number : '';
					$business_name = ($request->filled('business_name') ) ? $request->business_name : '';

					$message = str_replace(
						['[first_name]', '[last_name]', '[number]', '[items]', '[date]', '[time]', '[date_time]', '[business_phone_number]', '[business_name]'],
						[$fName, $lName, $number, $items, $date, $time, $date_time, $business_phone_number, $business_name],
						$item->message
					);

					$messageLength = strlen($message);
					$cost = $subAccount->getCreditAmount($messageLength);
					// $canSend = $subAccount->isCreditSufficientForTask($messageLength);

					if ($subAccount->isBeechoo()) { // beechoo
						$formattedNumber = formattedNumber($number);
						$canSend = $subAccount->isCreditSufficientForTaskWithNumber($messageLength, $formattedNumber);
					} else {
						$canSend = $subAccount->isCreditSufficientForTask($messageLength);
					}

					if ($number) {
					    
					    logger('HAS NUMBER ULIT');
						$request['filter_search'] = $fName . ' ' . $lName;
						$existingContact = $this->customerRepo->search($request)->first();

						if ($requestDateTime->gt($today)) { // Condition to check if immediate send
							// Insert sa job for next month
							
							logger('IS IMMEDIATE SEND');
							$allArraysJobNextMonth = [];
							$allArraysJob15thDay = [];
							$allArraysJob14thDay = [];
							$allArraysJobTomorrow = [];
							$allArraysJobToday = [];

							if ($canSend) {
								$currentDateTime = Carbon::now()->toDateTimeString();
								
								logger('CAN SEND');
								$draw = [
									'customer_id' => $existingContact ? $existingContact->id : null,
									'customer_name' => $name,
									'customer_number' => preg_replace('/\D+/', '', $number),
									'gateway_key' => $task->gateway_key,
									'campaign_id' => $item->campaign_id,
									'credits_consumed' => $cost,
									'when_to_send' => $isDreamNails && $time ? $requestDateTime->toDateString() . ' ' . $time : $requestDateTime->toDateTimeString(),
									'message' => $message,
									'task_id' => $item->id,
									'parent_task_id' => ( $task->id != $item->id ) ? $task->id : null,
									'subaccount_id' => $item->subaccount_id,
									'client_id' => $item->client_id,
									'zs_booking_id' => $request->zs_booking_id ?? null,
									'source' => $request->ip(),
									'created_at' => $currentDateTime,
									'updated_at' => $currentDateTime,
								];

								if ($diffDays) {
								    
								    logger('HAS DIFF DAYS');
									if ($diffDays == 1) {
										$allArraysJobTomorrow[] = $draw;
									} elseif($diffDays >= 2 && $diffDays <= 14) {
										$allArraysJob14thDay[] = $draw;
									} elseif($diffDays >= 15) {
										if ($requestDateTime->gte($firstDayofNextMonth)) {
											$allArraysJobNextMonth[] = $draw;
										} else {
											$allArraysJob15thDay[] = $draw;
										}
									}
								} else {
								    
								    logger('NO DIFF DAYS');
								    
									if ($diffDays == 0) {
										$allArraysJobToday[] = $draw;
									} else {
									    
									    logger('ERRORS - No Jobs Found ...');
										$this->errors = $this->messageBag->add('task', 'No Jobs Found ...')->merge($this->errors);
									}
								}

								if ($subAccount->credit) {
									$credit = $subAccount->credit;
									
									logger('CHECK CREDIT');
									if ($cost > $credit->pending_balance) {
									    
									    logger('ERRORS - Your credit is not enough for the creation of this SMS task.');
										$this->errors = $this->messageBag->add('job_error', 'Your credit is not enough for the creation of this SMS task.');
										throw new \ATM\Validation\ValidationException($this->errors);
									}
									$smsFacade->minusPendingCreditPerSubaccount($cost, $subAccount);
								}

								$jobsData = [
									'allArraysJobNextMonth' => $allArraysJobNextMonth,
									'allArraysJob15thDay'   => $allArraysJob15thDay,
									'allArraysJob14thDay'   => $allArraysJob14thDay,
									'allArraysJobTomorrow'  => $allArraysJobTomorrow,
									'allArraysJobToday'     => $allArraysJobToday,
								];
								
								logger('CONTINUE SENDING JOBS');

								$this->taskJobRepo->continueSendingJobs($jobsData);

								// ADDITIONAL CONDITIONS DITO FOR OTHER JOBS
								$this->calculateTotalCountAndProjected($cost, $task);

								$returnMessage = 'Job created with new schedule';

							} else {
								// Create failed job dahil wala ng credits
								$errorResp = 'Job failed, no credits remaining in your account';
								$this->createFailed($request, $message, $cost, $name, $number, $requestDateTime, $item, $errorResp);
								$this->calculateTotalCountAndProjected($cost, $item);
								$returnMessage = $errorResp;
								
								logger($returnMessage);
								// return $returnMessage;
							}
						} else {
							$result = $this->immediateSend($request, $task);

							if ($result) {
								$returnMessage = 'Job created, SMS sent';
								
								logger($returnMessage);
							} else {
								$errorResp = 'Job failed, no credits remaining in your account';
								$this->createFailed($request, $message, $cost, $name, $number, $requestDateTime, $item, $errorResp);
								$this->calculateTotalCountAndProjected($cost, $task);
								$returnMessage = $errorResp;
								
								logger($returnMessage);
								// return $errorResp;
							}
						}
					} else {
						// $this->createFailed($request, $message, $cost, $name, $number, $requestDateTime, $item);
						// $this->calculateTotalCountAndProjected($cost, $task);

						$returnMessage = 'Mobile number format is invalid.';
						logger($returnMessage);
					}
				} else {
				    logger('ERROR - Mobile number not provided.');
					$this->errors = $this->messageBag->add('fatal_error', 'Mobile number not provided.');
					throw new \ATM\Validation\ValidationException($this->errors);
				}
        	});
		} else {
		    
		    logger('ERROR - Provide a valid number.');
			$this->errors = $this->messageBag->add('fatal_error', 'Provide a valid number.');
		}

		if (count($this->errors) > 0) {
		    
		    logger('ERROR - FOUND!!!!!');
			throw new \ATM\Validation\ValidationException($this->errors);
		}

       return $returnMessage;
    }

	private function createFailed($request, $message, $cost, $name, $number, $datetime, $task, $errorResponse = 'Mobile number format is invalid.')
	{
		$request['message'] = $message;
		$request['cost'] = $cost;
		$request['name'] = $name;
		$request['number'] = preg_replace('/\D+/', '', $number);
		$request['task_id'] = $task->id;
        $jobFailed = $this->jobFailedRepo->createFailedJob($request, $datetime, $task, $errorResponse);

        $failed_jobs_count = $this->taskRepository->search($request)->toArray();
        $jbFailed = ( $failed_jobs_count && $failed_jobs_count[0]['jobs_failed'] != NULL ) ? $failed_jobs_count[0]['jobs_failed'] + 1 : 1;
        $this->taskRepository->updateFailedCount($task->id, $jbFailed);

		return $jobFailed;
    }

    public function validateJobData(Request $request)
    {
        $rules = [
            'first_name'    => 'required|string',
            'last_name'     => 'required|string',
            'number'        => 'required|numeric',
            'date_time'     => 'required|date',
            'items'         => 'required'
        ];

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

    public function immediateSend(Request $request, Task $task)
	{
		$subAccount = $task->subAccount;

    	if (!$subAccount) {
    	    logger('Fatal error on making the request.');
			$this->errors = $this->messageBag->add('fatal_error', 'Fatal error on making the request.');
		}

		$smsFacade = resolve(\ATM\Repositories\Sms\SmsFacade::class);

		$lName = $request->filled('last_name') ? $request->last_name : '';
        $fName = $request->filled('first_name') ? $request->first_name : '';
        $number = $request->filled('number') ? $request->number : '';
        $items = $request->filled('items') ? $request->items : '';

        $name = $fName.' '.$lName;
        $date = ($request->filled('date') ) ? $request->date : '';
        $time = ($request->filled('time') ) ? $request->time : '';
        $date_time = "{$date} {$time}";
        $business_phone_number = ($request->filled('business_phone_number') ) ? $request->business_phone_number : '';
        $business_name = ($request->filled('business_name') ) ? $request->business_name : '';

        $request['filter_search'] = $fName . ' ' . $lName;

        $existingContact = $this->customerRepo->search($request)->first();

        if ($existingContact) {
            logger('EXISTING CONTACT');
        	$request['existing_contact_id'] = $existingContact->id;
        }

        $message = str_replace(
            ['[first_name]', '[last_name]', '[number]', '[items]', '[date]', '[time]', '[date_time]', '[business_phone_number]', '[business_name]'],
            [$fName, $lName, $number, $items, $date, $time, $date_time, $business_phone_number, $business_name],
            $task->message
        );

		$messageLength = strlen($message);

		if ($subAccount->isBeechoo()) { // beechoo
			$formattedNumber = formattedNumber($number);
			$canSend = $subAccount->isCreditSufficientForTaskWithNumber($messageLength, $formattedNumber);
		} else {
			$canSend = $subAccount->isCreditSufficientForTask($messageLength);
		}

		if (!$task->active) {
		    logger(strtoupper('This SMS task is no longer active and can\' receive any more job request.'));
			$this->errors = $this->messageBag->add('fatal_error', 'This SMS task is no longer active and can\' receive any more job request.');
		}

		if (count($this->errors) > 0) {
		    logger(strtoupper('HAS ERRORS IMMEDIATE SEND'));
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		if ($canSend) {
		    logger(strtoupper('IMMEDIATE CAN SEND'));
			try {
				$request['name'] = $fName . ' ' . $lName;
				$request['message'] = $message;
				$result = $smsFacade->createImmediateJob($request, $subAccount, $task);
				
				logger('CREATE IMMEDIATE RESULT');
				
				logger($result);

				return $result;
			} catch (ValidationException $e) {
				$errors = $e->getErrors();
				logger(strtoupper('IMMEDIATE SEND ERROR!!!!!'));
				$this->errors = $this->messageBag->add('fatal_error', $errors[0]);
				throw new \ATM\Validation\ValidationException($this->errors);
			}
		} else {
			return false;
		}
	}

	/* function for updating data in the task table */
    private function calculateTotalCountAndProjected($cost, $task)
    {
        $jobsCount = (int) $task->jobs_count;
        $projectedCreditsConsumed = (int) $task->projected_credits_consumed;

        $jobsCount++;
        $projectedCreditsConsumed = $projectedCreditsConsumed + $cost;

        $task->jobs_count = $jobsCount;
        $task->projected_credits_consumed = $projectedCreditsConsumed;
        $task->save();

        return;
    }

}
