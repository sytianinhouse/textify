<?php

namespace ATM\Repositories\Tasks;

use Illuminate\Support\Str;
use ATM\ContextInterface;
use ATM\Repositories\BaseRepository;
use ATM\Repositories\Client\Client;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\Customers\CustomersQueryRepository;
use ATM\Repositories\Settings\SiteSettings;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\TaskJobs\TaskJobsRepository;
use ATM\Repositories\Tasks\BaseJob;
use ATM\Validation\ValidationException;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class TextBlastRepository extends ContextRepository
{
	protected $taskType;

	public function __construct(
		Task $task,
		TaskJobsRepository $taskJobRepo,
		CustomersQueryRepository $customersQueryRepo,
		ContextInterface $context,
		BaseJob $baseJob
	)
	{
		$this->model = $task;
		$this->taskJobRepo = $taskJobRepo;
		$this->context = $context;
		$this->customersQueryRepo = $customersQueryRepo;
		$this->baseJob = $baseJob;
		$this->taskType = TaskType::find(1);
	}

	public function search(Request $request, $with = [], $withCount = false)
	{
		$query = $this->scope($with);

		$query->where('task_type_id', '=', Task::TEXT_BLAST_SEND);

		if ($request->filled('sub_account_id')) {
			$query = $query->whereHas('subAccount', function($q) use ($request) {
				$q->where('subaccount_id', $request->sub_account_id);
			});
		}

		if( $withCount ) {
			$query = $query->withCount('tasksJobToday', 'tasksJobTomorrow','tasksJobAfterTomorrowTo14thDay', 'tasksJob15thDayToEndMonth','tasksJobNextMonth', 'tasksjobsCompleted', 'tasksjobsFailed')->orderBy('date_to_send', 'ASC');
		}

		// NO USE
		/* if ($request->filled('filter_task_type_id')) {
			$query = $query->where('task_type_id', $request->filter_task_type_id);
		}*/

		if ($request->filled('campaign_filter')) {
			$query = $query->where('campaign_id', $request->campaign_filter);
		}

		if ($request->filled('subaccount_filter')) {
			$query = $query->where('subaccount_id', $request->subaccount_filter);
		}

		if ( $request->filled('filter_date_by') ) {
			$fromDate = new Carbon($request->from_date);
			$toDate = new Carbon($request->to_date);

			if ($request->filter_date_by == 'created_at') {
				if ( $request->filled('from_date') && $request->filled('to_date') ) {
					$query = $query->whereBetween('created_at', [$fromDate->startOfDay()->toDateTimeString(),$toDate->endOfDay()->toDateTimeString()]);
				}
			} else {
				if ( $request->filled('from_date') && $request->filled('to_date') ) {
					$query = $query->whereBetween('date_to_send', [$fromDate->startOfDay()->toDateTimeString(),$toDate->endOfDay()->toDateTimeString()]);
				}
			}
		}

		if ($request->has('per_page') && $request->per_page != self::DISPLAY_ALL) {
			return $query->paginate($request->per_page);
		} else {
			return $query->get();
		}
	}

	public function searchIndex(Request $request, $with = [], $withCount = false)
	{
		$query = $this->scope($with);

		$query->where('task_type_id', '=', Task::TEXT_BLAST_SEND);

		if ($request->filled('sub_account_id')) {
			$query = $query->whereHas('subAccount', function($q) use ($request) {
				$q->where('subaccount_id', $request->sub_account_id);
			});
		}

		if( $withCount ) {
			// $query = $query->withCount('tasksJobToday', 'tasksJobTomorrow','tasksJobAfterTomorrowTo14thDay', 'tasksJob15thDayToEndMonth','tasksJobNextMonth', 'tasksjobsCompleted', 'tasksjobsFailed')->orderBy('date_to_send', 'ASC');
			$query = $query->with('tasksJobToday:id', 'tasksJobTomorrow:id','tasksJobAfterTomorrowTo14thDay:id', 'tasksJob15thDayToEndMonth:id','tasksJobNextMonth:id', 'tasksjobsCompleted:id', 'tasksjobsFailed:id')->orderBy('date_to_send', 'ASC');
		}

		// NO USE
		/* if ($request->filled('filter_task_type_id')) {
			$query = $query->where('task_type_id', $request->filter_task_type_id);
		}*/

		if ($request->filled('campaign_filter')) {
			$query = $query->where('campaign_id', $request->campaign_filter);
		}

		if ($request->filled('subaccount_filter')) {
			$query = $query->where('subaccount_id', $request->subaccount_filter);
		}

		if ( $request->filled('filter_date_by') ) {
			$fromDate = new Carbon($request->from_date);
			$toDate = new Carbon($request->to_date);

			if ($request->filter_date_by == 'created_at') {
				if ( $request->filled('from_date') && $request->filled('to_date') ) {
					$query = $query->whereBetween('created_at', [$fromDate->startOfDay()->toDateTimeString(),$toDate->endOfDay()->toDateTimeString()]);
				}
			} else {
				if ( $request->filled('from_date') && $request->filled('to_date') ) {
					$query = $query->whereBetween('date_to_send', [$fromDate->startOfDay()->toDateTimeString(),$toDate->endOfDay()->toDateTimeString()]);
				}
			}
		}

		if ($request->has('per_page') && $request->per_page != self::DISPLAY_ALL) {
			return $query->paginate($request->per_page);
		} else {
			return $query->get();
		}
	}

	public function addTask(Request $request, $task = null, $subAccount = null, $client = null)
	{
		if (!$subAccount) {
			$subAccount = SubAccount::find($request->subaccount_id);
		}

		if (!$client) {
			$client = Client::find($request->client_id);
		}

		$messageBag = new MessageBag();
		$ids = array_filter(explode(',', $request->customer_ids));
		$prime_id = array_filter(explode(',', $request->contacts_to_be_send));
		$secondary_id = array_filter(explode(',', $request->generated_contact));
		$ids = array_merge($prime_id, $secondary_id);
        $ids = array_unique($ids);

        $failCustomer = false;

        $mobile_numbers = array_filter(explode(',', $request->mobile_number_to_be_send));

		if ($request->filled('select_specific_customer')) {
			$ids =  array_merge($ids, array_filter($request->select_specific_customer));
		}

		// if (!$request->filled('select_option_filter') && count($ids) < 1) {
		// 	$failCustomer = true;
		// }

		// if ($request->filled('select_option_filter')) {
		// 	$option = $request->select_option_filter;
		// 	if ($option == 'specific_customer') {
		// 	} else if ($option == 'all_customer') {
		// 		$request['client_id'] = $client->id;
		// 		$request['not_empty_mobile'] = true;
		// 		$customers = $this->customersQueryRepo->search($request, true);
		// 		$ids = array_unique(array_merge($ids, $customers->pluck('id')->toArray())) ;
		// 	}
        // }

		if ( count($ids) < 0 && count($mobile_numbers) < 0 ) {
			$failCustomer = true;
		}

		if ($failCustomer) {
			$this->errors = $messageBag->add('customers', 'Recipients for this SMS task is required.');
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		if ( isset($request->input_send_task) == true ) {

            $credit = $subAccount->credit;

			$credValidator = \Validator::make(
				['credit' => $credit->pending_balance],
				['credit' => 'required|min:1']
			);

			if ($credValidator->fails()) {
				$this->errors = $credValidator->messages()->merge($this->errors);
				throw new \ATM\Validation\ValidationException($this->errors);
			}
		}

		$endTime = strtotime("+10 minutes", strtotime($request->date_to_send)); // set 15mins ahead time

		$now = Carbon::now();
        $dateToSend = null;

        $rules = [
            'campaign' => 'required',
            'message'  => ['required', new \App\Rules\Unicode],
        ];
		if ($request->filled('time_to_send')) {
			$dateToSend = (new Carbon(Carbon::parse($request->date_to_send)->toDateString() . ' ' . $request->time_to_send));
            $request['timedate_to_send'] = $dateToSend->toDateTimeString();

            $rules = [
                'campaign'         => 'required',
                'date_to_send'     => 'required|after_or_equal: '. $now->toDateString(),
                'timedate_to_send' => 'required|after: '. $now->toDateTimeString(),
                'message'          => ['required', new \App\Rules\Unicode],
                // 'select_option_filter' => 'required'
            ];

        }
        $validator = \Validator::make($request->all(), $rules, [
            'date_to_send.after_or_equal' => 'The date to send must date after ' . (Carbon::now())->format('m d, Y'),
            'timedate_to_send.after' => 'The time to send must be time after ' . (Carbon::now())->format('h:i a')
        ]);

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
            throw new \ATM\Validation\ValidationException($this->errors);
        }
        
		// $ids = array_slice($ids, 0, 350);
		// $sufficientCredit = $subAccount->isCreditSufficientForTask($request->echarc, count($ids));

		// if ($subAccount->isBeechoo()) { // beechoo
		// 	$hasNumber = $request->number && $request->number != NULL && $request->number != '';
		// 	$formattedNumber = $hasNumber ? formattedNumber($request->number) : null;
		// 	$sufficientCredit = $formattedNumber ? $subAccount->isCreditSufficientForTaskWithNumber($request->echarc, $formattedNumber, count($ids)) : false;
		// } else {
			$sufficientCredit = $subAccount->isCreditSufficientForTask($request->echarc, count($ids));
		// }

		if (!$sufficientCredit) {
			$this->errors = $messageBag->add('job_error', 'Your credit is not enough for the creation of this SMS task.');
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		// $totalProjectedCredit = $subAccount->isCreditSufficientForTask($request->echarc, count($ids), true);

		// if ($subAccount->isBeechoo()) { // beechoo
		// 	$hasNumber = $request->number && $request->number != NULL && $request->number != '';
		// 	$formattedNumber = $hasNumber ? formattedNumber($request->number) : null;
		// 	$totalProjectedCredit = $formattedNumber ? $subAccount->isCreditSufficientForTaskWithNumber($request->echarc, $formattedNumber, count($ids), true) : false;
		// } else {
			$totalProjectedCredit = $subAccount->isCreditSufficientForTask($request->echarc, count($ids), true);
		// }

		if ($request->filled('message')) {
			$cost = $subAccount->getCreditAmount(Str::length(strip_tags($request->message)));
        }

		// $task = $task ? : $this->model;
		$task = new Task;
		$task->client_id = $client->id;
		$task->subaccount_id = $subAccount->id;
		$task->entry_id = null;
		$task->campaign_id = $request->campaign;
		$task->date_to_send = $dateToSend ? $dateToSend->toDateString() : $now->toDateString();
		$task->time_to_send = $dateToSend ? $dateToSend->format('H:i') : $now->toTimeString();
		// $task->active = $request->active ? true : false;
		$task->active = true;
		$task->actual_credits_consumed = '0.00';
		// $task->projected_credits_consumed = $request->send_immediately ? 0.00 : (count($ids) + count($mobile_numbers)) * $cost;
		$task->projected_credits_consumed = !$dateToSend ? 0.00 : (count($ids) + count($mobile_numbers)) * $cost;
		$task->success_jobs_count = 0.00;
		// $task->jobs_count = $request->send_immediately ? 0.00 : count($ids) + count($mobile_numbers);
		$task->jobs_count = !$dateToSend ? 0.00 : count($ids) + count($mobile_numbers);
		$task->message = $request->filled('message') ? strip_tags($request->message) : null;
		$task->task_type_id = $this->taskType->id;

		// $gateway = $subAccount->apis->first();
        // $task->gateway_key = $gateway ? $gateway->id : null;

		$task->gateway_key = $subAccount->getApiKeyId();

		// if (is_array($ids) && count($ids) > 0) {
        //     $task->taskCustomers()->sync($ids);
        // }

        $taskCreation = $task->save();
        if( $taskCreation ) {
            try {
                if( $request->send_immediately == 1 ) {
                    $request['send_immediately'] = true;
                }

                $this->taskJobRepo->createJobs($request, $task);
            } catch (ValidationException $e) {
                $this->errors = $credValidator->messages()->merge($e->getErrors());
                throw new \ATM\Validation\ValidationException($this->errors);
            }
            return $task;
        }

        // If umabot na dito creation is successful na

		// if ( isset($request->input_send_task) == true ) {
		// 	try {
		// 		$this->taskJobRepo->createJobs($request, $task);
		// 	} catch (ValidationException $e) {
		// 		$this->errors = $credValidator->messages()->merge($e->getErrors());
		// 		throw new \ATM\Validation\ValidationException($this->errors);
		// 	}
		// }

    }

    public function updateTask(Request $request, Task $task) {
        if( $request ) {
            $task = isset($task['id']) ? $this->model->find($task['id']) : new Task();
            $task->active = $request->active == 1 ? true : false;
            $task->save();
        }
    }
}
