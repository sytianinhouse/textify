<?php

namespace ATM\Repositories\Tasks;

use ATM\ContextInterface;
use ATM\Repositories\BaseRepository;
use ATM\Repositories\Client\Client;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\Settings\SiteSettings;
use ATM\Repositories\TaskJobs\TaskJobsRepository;
use ATM\Repositories\Tasks\BaseJob;
use ATM\Repositories\Tasks\Jobs15thDayToEndMonth\Job15thDayToEndMonth;
use ATM\Repositories\Tasks\JobsAfterTomorrowTo14thDay\JobAfterTomorrowTo14thDay;
use ATM\Repositories\Tasks\JobsFailed\JobsFailed;
use ATM\Repositories\Tasks\JobsFailed\JobsFailedRepository;
use ATM\Repositories\Tasks\JobsNextMonth\JobNextMonth;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\JobsTomorrow\JobTomorrow;
use ATM\Repositories\Tasks\Task;
use App\Console\Commands\CronJobsNextMonth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TaskRepository extends ContextRepository {

	/*
	 * @var Illuminate\Database\Eloquent\Model
	 */
	protected $model;

	public function __construct(
		Task $task,
		TaskJobsRepository $taskJobRepo,
		ContextInterface $context,
		BaseJob $baseJob,
        JobsFailedRepository $JobsFailedRepository
	)
	{
		$this->model = $task;
		$this->context = $context;
		$this->taskJobRepo = $taskJobRepo;
		$this->baseJob = $baseJob;
		$this->JobsFailedRepository = $JobsFailedRepository;
	}

	public function search(Request $request, $with = [])
	{
		$query = $this->scope($with);

		if ($request->filled('sub_account_id')) {
			$query = $query->whereHas('subAccounts', function($q) use ($request) {
				$q->where('subaccount_id', $request->sub_account_id);
			});
		}

		if ($request->filled('filter_task_type_id')) {
			$query = $query->where('task_type_id', $request->filter_task_type_id);
		}

		if ($request->filled('campaign_filter')) {
			$query = $query->where('campaign_id', $request->campaign_filter);
		}

		if ($request->filled('subaccount_filter')) {
			$query = $query->where('subaccount_id', $request->subaccount_filter);
        }

        if ($request->filled('task_id')) {
			$query = $query->where('id', $request->task_id);
		}

		if ( $request->filled('filter_date_by') ) {
			$fromDate = new Carbon($request->from_date);
			$toDate = new Carbon($request->to_date);

			if ($request->filter_date_by == 'created_at') {
				if ( $request->filled('from_date') && $request->filled('to_date') ) {
					$query = $query->whereBetween('created_at', [$fromDate->startOfDay()->toDateTimeString(),$toDate->endOfDay()->toDateTimeString()]);
				}
			} else {
				if ( $request->filled('from_date') && $request->filled('to_date') ) {
					$query = $query->whereBetween('date_to_send', [$fromDate->startOfDay()->toDateTimeString(),$toDate->endOfDay()->toDateTimeString()]);
				}
			}
		}

		if ($request->has('per_page')) {
			return $query->orderBy('date_to_send', 'DESC')->paginate($request->per_page);
		} else {
			return $query->orderBy('date_to_send', 'DESC')->get();
		}
	}

	public function makeSmsFormatted($task, $customer)
	{
        $values = ['','','','',''];

        if ($customer instanceOf Customer) {
            $values = [$customer->first_name, $customer->last_name, $customer->email, $customer->telephone, $customer->address_1];
        }

		$message = str_replace(
			['[first_name]', '[last_name]', '[email]', '[telephone]', '[address]'],
			$values,
			$task->message
		);

		return $message;
	}

	public function creditComputation($message)
	{
		$settings = SiteSettings::getSettings();
		$limit = $settings->sms_character_limit ?: Task::CHARACTER_LIMITER;
		$rate = Task::CREDIT_LIMITER;

		$length = strlen( $message );
		if ( $length > 0 ) {
			if ( $length > $limit ) {
				return number_format(($length / $limit) * $rate, 2);
			} else {
				return number_format($rate);
			}
		}
	}

	public function validateData(Request $request)
    {
    	$rules = [];

        if ($request->task_type_id == \ATM\Repositories\Tasks\Task::ONE_TIME_SEND) {
            $rules = [
                'campaign'     => 'required',
                'time_to_send' => 'required',
                'message'      => 'required'
            ];
        }

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

    // @depreciated use the specific repository instead for different task type
	public function add(Request $request, Client $client, $type = null)
	{
		$task = $this->model;
		$now = Carbon::now();

		if ($request->task_type_id == \ATM\Repositories\Tasks\Task::ONE_TIME_SEND) {

			$this->validateData($request);

            if (count($this->errors) > 0) {
	            throw new \ATM\Validation\ValidationException($this->errors);
	        }

	        if ($request->filled('subaccount_id')) {
	        	$reqSubAccount = $request->subaccount_id;
	        }

	        if ($request->filled('sub_account_id')) {
	        	$reqSubAccount = $request->sub_account_id;
	        }

	        $subAccount = $client->subAccounts()->find($reqSubAccount);

			$task->client_id = $client->id;
			$task->subaccount_id = $reqSubAccount;
			$task->entry_id = null;
			$task->campaign_id = $request->campaign;

			$task->date_to_send = $request->date_to_send ? $request->date_to_send : date('Y-m-d');

			if ($request->has('send_immediately')) {
				// $task->time_to_send = date("H:i", strtotime('00:00'));
				$task->time_to_send = date("H:i", strtotime('10:00'));
			} else {
				$task->time_to_send = date("H:i", strtotime($request->time_to_send));
			}

			$task->active = 1;
			// $task->success_sent_msg_count = 1;
			// $task->msg_count = 1;
			$task->message = $request->filled('message') ? strip_tags($request->message) : null;
			$task->task_type_id = $request->filled('task_type_id') ? $request->task_type_id : null;
			$task->gateway_key = $subAccount->getApiKeyId();
	    	$task->task_key = sha1($client->id . '--' . $task->subAccount->id . '--' . $now->toDateTimeString());
	    	$task->save();
		}

		if ($request->task_type_id == \ATM\Repositories\Tasks\Task::TEXT_BLAST_SEND) {


			if ( isset($request->input_submit_task) == true ):
				$task->client_id = $client->id;
				$task->subaccount_id = $request->sub_account_id;
				$task->entry_id = 1;
				$task->campaign_id = $request->campaign;
				$task->date_to_send = $request->date_to_send ? $request->date_to_send : date('Y-m-d');
				$task->time_to_send = date("H:i", strtotime($request->time_to_send));
				$task->active = 1;
				// $task->success_sent_msg_count = 1;
				$task->jobs_count = $request->filled('customers') ? count($request->customers) : 0;
				$task->message = $request->filled('message') ? strip_tags($request->message) : null;
				$task->task_type_id = $request->filled('task_type_id') ? $request->task_type_id : null;
				$task->gateway_key = $request->filled('api_key') ? $request->api_key[0] : null;

		       	if ($task->save()) {
		       		if (isset($request->customers)) {
						$task->taskCustomers()->sync($request->customers);
		       		}
		       	}
	    	endif;
			if ( isset($request->input_send_task) == true ) :
		    	// send
				$task->client_id = $client->id;
				$task->subaccount_id = $request->sub_account_id;
				$task->entry_id = 1;
				$task->campaign_id = $request->campaign;
				$task->date_to_send = $request->date_to_send ? $request->date_to_send : date('Y-m-d');
				$task->time_to_send = date("H:i", strtotime($request->time_to_send));
				$task->active = 1;
				$task->success_jobs_count = 1;
				$task->jobs_count = $request->filled('customers') ? count($request->customers) : 0;
				$task->message = $request->filled('message') ? strip_tags($request->message) : null;
				$task->task_type_id = $request->filled('task_type_id') ? $request->task_type_id : null;
				$task->gateway_key = $request->filled('api_key') ? $request->api_key[0] : null;

			    if ($task->save()) {
		       		if (isset($request->customers)) {
						$task->taskCustomers()->sync($request->customers);
		       		}
		       	}

	       		$this->taskJobRepo->getJobList( $request, $task );
			endif;

		}

       	return $task;
	}

    public function updateFailedCount($taskId, $jobCount = NULL, $resend = false) {
        $task = ( isset($taskId) ) ? $this->model->find($taskId) : new Task();
        $task->jobs_failed = ( $resend ) ? $task->jobs_failed - 1 : $jobCount;
        $task->save();
    }

	// @depreciated for Text Blast
	// @depreciated use the specific repository instead for different task type
	// Merun na siyang sariling repository
	public function update($taskId, Request $request, $client)
	{
		$task = $this->model->find($taskId);

       	if ($task->task_type_id == \ATM\Repositories\Tasks\Task::ONE_TIME_SEND) {

       		if ($request->filled('subaccount_id')) {
	        	$reqSubAccount = $request->subaccount_id;
	        }

	        if ($request->filled('sub_account_id')) {
	        	$reqSubAccount = $request->sub_account_id;
	        }

			$task->subaccount_id = $reqSubAccount;
			$task->entry_id = 1;
			$task->campaign_id = $request->campaign;
			$task->date_to_send = $request->date_to_send ? $request->date_to_send : date('Y-m-d');

			if ($request->has('send_immediately')) {
				// $task->time_to_send = date("H:i", strtotime('00:00'));
				$task->time_to_send = date("H:i", strtotime('10:00'));
			} else {
				$task->time_to_send = date("H:i", strtotime($request->time_to_send));
			}

			// $task->success_sent_msg_count = 1;
			// $task->jobs_count = isset($request->customers) ? count($request->customers) : 0;
			$task->message = $request->filled('message') ? strip_tags($request->message) : null;

	    	$task->task_key = sha1($client->id . '--' . $task->subAccount->id . '--' . $task->id);
	    	$task->save();

		}

		if ($task->task_type_id == \ATM\Repositories\Tasks\Task::TEXT_BLAST_SEND) {

			if ( isset($request->input_submit_task) == true ):

				// if saving
				$task->client_id = $client->id;
				$task->subaccount_id = $request->sub_account_id;
				$task->entry_id = 1;
				$task->campaign_id = $request->campaign;
				$task->date_to_send = $request->date_to_send ? $request->date_to_send : date('Y-m-d');
				$task->time_to_send = date("H:i", strtotime($request->time_to_send));
				$task->active = 1;
				// $task->success_sent_msg_count = 1;
				$task->jobs_count = $request->filled('customers') ? count($request->customers) : 0;
				$task->message = $request->filled('message') ? strip_tags($request->message) : null;
				$task->task_type_id = $request->filled('task_type_id') ? $request->task_type_id : null;

				$task->taskCustomers()->syncWithoutDetaching($request->customers);
			    $task->save();

			endif;

			// if sending
			if ( isset($request->input_send_task) == true ) :

				$allCounts = Task::withCount('taskCustomers')->find( $taskId );

				$task->client_id = $client->id;
				$task->subaccount_id = $request->sub_account_id;
				$task->entry_id = 1;
				$task->campaign_id = $request->campaign;
				$task->date_to_send = $request->date_to_send ? $request->date_to_send : date('Y-m-d');
				$task->time_to_send = date("H:i", strtotime($request->time_to_send));
				$task->active = 1;
				// $task->success_sent_msg_count = 1;
				$task->jobs_count = isset($request->customer) ? count($request->customers) : $allCounts->task_customers_count;
				$task->message = $request->filled('message') ? strip_tags($request->message) : null;
				$task->task_type_id = $request->filled('task_type_id') ? $request->task_type_id : null;

				$task->taskCustomers()->syncWithoutDetaching($request->customers);
			    $task->save();

			    if ($task->save()) {
	       			$this->taskJobRepo->getJobList($request, $task);
			    }

			endif;

		}

       	return $task;
	}

	public function delete(Request $request, $taskId, $task = NULL)
	{
        $smsFacade = resolve(\ATM\Repositories\Sms\SmsFacade::class);

        $request['task_id'] = $taskId;
        $allJobs = $this->baseJob->getAllJobs($request);
        $job_15th_day_to_end_month = [];
        $job_after_tomorrow_to_14th_day = [];
        $job_next_month = [];
        $job_today = [];
        $job_tomorrow = [];
        $allJobs->each(function($job, $key) use (&$allJobs, &$job_15th_day_to_end_month, &$job_after_tomorrow_to_14th_day, &$job_next_month, &$job_today, &$job_tomorrow ) {
            if( $job instanceof Job15thDayToEndMonth ) {
                $job_15th_day_to_end_month[] = $job->id;
            } elseif( $job instanceof JobAfterTomorrowTo14thDay ) {
                $job_after_tomorrow_to_14th_day[] = $job->id;
            } elseif( $job instanceof JobNextMonth ) {
                $job_next_month[] = $job->id;
            } elseif( $job instanceof JobToday ) {
                $job_today[] = $job->id;
            } elseif( $job instanceof JobTomorrow ) {
                $job_tomorrow[] = $job->id;
            }
            $now = Carbon::now();
            $allJobs[$key]['time_attempt_send'] = $job->when_to_send;
            $allJobs[$key]['error_log'] = 'Task Cancelled';
            $allJobs[$key]['deleted_at'] = $now;
            $allJobs[$key]['created_at'] = $now;
            $allJobs[$key]['updated_at'] = $now;
            unset($allJobs[$key]['when_to_send']);
            unset($allJobs[$key]['id']);
        });

        $moveJobs = $this->insertToJobsFailed( $allJobs->toArray() );

        if( $moveJobs ) {
            if( count($job_15th_day_to_end_month) > 0 ) {
                $this->deleteJobs($job_15th_day_to_end_month, Job15thDayToEndMonth::class);
            } elseif( count($job_after_tomorrow_to_14th_day) > 0 ) {
                $this->deleteJobs($job_after_tomorrow_to_14th_day, JobAfterTomorrowTo14thDay::class);
            } elseif( count($job_next_month) > 0 ) {
                $this->deleteJobs($job_next_month, JobNextMonth::class);
            } elseif( count($job_today) > 0 ) {
                $this->deleteJobs($job_today, JobToday::class);
            } elseif( count($job_tomorrow) > 0 ) {
                $this->deleteJobs($job_tomorrow, JobTomorrow::class);
            }

            $task = $this->model->find($taskId); // removed data
            if ($task) {
                $task->delete();
            }
            if ($request->filled('dont_detach')) {
                $task->taskCustomers()->detach(); // removed customers
            }

            if( $task ) {
                $smsFacade->addPendingCreditPerSubaccountAccessor($task->projected_credits_consumed, $task->subAccount);
            }

            return $task;
        }
    }

    public function deleteJobs( $array, $model ) {
        if ( !is_array( $array ) ) {
            Log::error("Data provided is not in array format!");
        } else {
            $delete = $model::whereIn('id', $array)->delete();
            if( $delete ):
                Log::info('Moving Jobs to Jobs Failed Table success!');
            endif;
            return $delete;
        }
        return;
    }

    public function insertToJobsFailed( $array ) {
        if ( !is_array( $array ) ) {
            Log::error("Data provided is not in array format!");
        } else {
            $insert = JobsFailed::insert( $array );
            if( $insert ):
                Log::info('Moving Jobs to Jobs Failed Table success!');
            endif;
            return $insert;
        }
        return;
    }

    	/* function for sending to failed table */
    public function sendToJobsFailed($jobs, $errorResult, $errorType, $getAllIdshasError = []) {
		if ( $errorResult ):
            if ( $errorType == 'fail_mobile_format' ){
                $errorMessage = 'The Mobile of: '.$jobs->customer_name.' invalid or wrong format: '.$jobs->customer_number;
            } elseif ( $errorType == 'fail_sub_account_id' ){
                $errorMessage = 'Invalid Sub account id';
            } elseif ( $errorType == 'fail_insuficient_balance' ){
                $errorMessage = 'Insuficient Credit. '. $this->getSubAccount($jobs->subaccount_id)->account_name;
            } else {
                $errorMessage = $errorType;
            }
            $buildArrays = $this->buildArrays( $jobs, $errorMessage, 'table_failed' );
            $InsertToFailedTable = $this->InsertIntoFailed( $buildArrays, $getAllIdshasError ); // insert to failed table
            return $InsertToFailedTable; // insert to failed table
		endif;
	}

	public function getDataOfCustomerFilter(Request $request){

		if ( $request ) {
			$query = Customer::whereHas('subAccounts', function($q) use($request) {
	            $q->where('subaccount_id', $request->subaccount_id);
	        })->whereNotNull('mobile');
	        // customer group
	        if ($request->filled('customergroup')) {
	            $query = $query->whereHas('customerGroups', function($q) use($request) {
	                $q->where('customer_group_id', $request->customergroup);
	            });
	        }
	        // monthfilter
	        if ($request->filled('monthfilter')) {
	            // $query = $query->whereMonth('b_month', '=',  $request->monthfilter);
	            $query = $query->where('b_month', $request->monthfilter);
	        }
	        // yearpicker
	        if ($request->filled('yearpicker')) {
	            // $query = $query->whereYear('birthdate', '=',  $request->yearpicker);
	            $query = $query->where('b_year', $request->yearpicker);
	        }
	        // dayfilter
	        if ($request->filled('dayfilter')) {
	            // $query = $query->whereDay('birthdate', '=',  $request->dayfilter);
	            $query = $query->where('b_day', $request->dayfilter);
	        }
	        // gender
	        if ($request->filled('gender')) {
	            $query = $query->where('gender', $request->gender);
	        }
	        // city
	        if ($request->filled('city')) {
	            $query = $query->where('city', $request->city);
	        }
		}
        return $customers = $query->get();
        // return $customers = $query->pluck('id');
	}

	public function getDataOfCustomerByIdFilter(Request $request){

		if ( $request ) {
			$query = Customer::whereHas('subAccounts', function($q) use($request) {
	            $q->where('subaccount_id', $request->sub_account_id);
	        })->whereNotNull('mobile');
	        // customer group
	        if ($request->filled('cu
	        stomergroup')) {
	            $query = $query->whereHas('customerGroups', function($q) use($request) {
	                $q->where('customer_group_id', $request->customergroup);
	            });
	        }
	        // monthfilter
	        if ($request->filled('monthfilter')) {
	            // $query = $query->whereMonth('b_month', '=',  $request->monthfilter);
	            $query = $query->where('b_month', $request->monthfilter);
	        }
	        // yearpicker
	        if ($request->filled('yearpicker')) {
	            // $query = $query->whereYear('birthdate', '=',  $request->yearpicker);
	            $query = $query->where('b_year', $request->yearpicker);
	        }
	        // dayfilter
	        if ($request->filled('dayfilter')) {
	            // $query = $query->whereDay('birthdate', '=',  $request->dayfilter);
	            $query = $query->where('b_day', $request->dayfilter);
	        }
	        // gender
	        if ($request->filled('gender')) {
	            $query = $query->where('gender', $request->gender);
	        }
	        // city
	        if ($request->filled('city')) {
	            $query = $query->where('city', $request->city);
	        }
		}
        // return $customers = $query->get();
        return $customers = $query->pluck('id');
	}

	public function searchByKey($taskKey, $with = [])
	{
		$query = $this->model->with($with);

		$query = $query->where('task_key', $taskKey);

		return $query->first();
	}

	public function makeTaskMessage(Request $request, $messageTemplate, $task = null)
	{
		$message = $messageTemplate;
		if ($request->first_name) {
			$message = str_replace('[first_name]', $request->first_name, $message);
		}

		if ($request->last_name) {
			$message = str_replace('[last_name]', $request->last_name, $message);
		}

		if ($request->items) {
			$message = str_replace('[items]', $request->items, $message);
		}

		if ($request->total_amount) {
			$message = str_replace('[total_amount]', $request->total_amount, $message);
		}

		if ($request->number) {
			$message = str_replace('[number]', $request->number, $message);
		}

		if ($request->has('date')) {
			$message = str_replace('[date]', $request->date, $message);
		}

		if ($request->has('time')) {
			$message = str_replace('[time]', $request->time, $message);
		}

		if ($request->email) {
			$message = str_replace('[email]', $request->email, $message);
		}

		if ($request->telephone) {
			$message = str_replace('[telephone]', $request->telephone, $message);
		}

		if ($request->address_1) {
			$message = str_replace('[address_1]', $request->address_1, $message);
		}

		if ($request->business_name) {
			$message = str_replace('[business_name]', $request->business_name, $message);
		}

		if ($request->misc_shortcodes) {
			$codes = (string) $request->misc_shortcodes;
			$encoded = json_decode($codes, true);
			if ($encoded) {
				foreach ($encoded as $key => $value) {
					$message = str_replace("[{$key}]", $value, $message);
				}
			}
		}

		return $message;
	}
}
