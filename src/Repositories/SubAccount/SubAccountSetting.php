<?php 

namespace ATM\Repositories\SubAccount;

use ATM\Repositories\SubAccount\SubAccount;
use Illuminate\Http\Request;

class SubAccountSetting {

	// General Settings
	const IS_BETA = 'is_beta_user';

	// Credits settings
    const SMS_RATE_PER_TEXT = 'sms_rate_per_text';
	const CREDIT_BALANCE_NOTIF_RECIPIENT = 'credit_balance_notification_recipient';

    public static function setSettings(SubAccount $subAccount, Request $request)
    {
        $subAccount->setSetting(self::SMS_RATE_PER_TEXT, $request->input('settings.'.self::SMS_RATE_PER_TEXT), false);
		$subAccount->setSetting(self::CREDIT_BALANCE_NOTIF_RECIPIENT, $request->input('settings.'.self::CREDIT_BALANCE_NOTIF_RECIPIENT), false);
    }

}