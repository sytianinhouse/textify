<?php

namespace ATM\Repositories\SubAccount;

use ATM\ContextInterface;
use ATM\Repositories\ApiKey\ApiKey;
use ATM\Repositories\Client\Client;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountSetting;
use ATM\Repositories\Uploadable\UploadableRepository;
use ATM\UuidGenerator;
use ATM\Validation\ValidationException;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class SubAccountRepository extends ContextRepository {

	protected $model;

	public function __construct(
		SubAccount $subaccount,
		UploadableRepository $uploadRepo,
		ContextInterface $context)
	{
		$this->model = $subaccount;
		$this->uploadRepo = $uploadRepo;
		$this->context = $context;
		// $this->creditRepo = $creditRepo;
	}

	public function store(SubAccount $subaccount = null, Request $request)
	{
		$subaccount = $this->model;

		$subaccount->fill($request->input());
		$subaccount->uuid = UuidGenerator::make();
		$subaccount->security_key = $this->makeUniqueSecurityKey($subaccount);
        $subaccount->active = $request->active;
		$subaccount->client_id = $request->client_id;

		$this->validate($subaccount);

		$subaccount->save();
		$subaccount->apis()->sync($request->api_key);

		$this->uploadMedia($request, $subaccount);

		$creditRepo = resolve(\ATM\Repositories\Credits\CreditRepository::class);

		$creditRepo->createDefault($subaccount);

		return $subaccount;
	}

	public function search(Request $request, $with = [], $displayAll = false)
	{
		$query = $this->scope($with);

		if ($request->filled('active_only')) {
			$query = $query->actives();
		}

		if ($request->filled('filter_search')) {
			$query = $query->where('account_name', 'LIKE', "%{$request->filter_search}%");
		}

		if ($request->filled('sub_account_id')) {
			$query = $query->where('id', $request->sub_account_id);
        }

		if ($request->filled('client_id')) {
			$query = $query->where('client_id', $request->client_id);
		}

		$perPage = '';
		if ($request->filled('per_page') && !$displayAll) {
			return $query->paginate($request->per_page ?: $perPage);
		} else {
			return $query->get();
		}
	}

	public function update( SubAccount $subaccount, Request $request)
	{
		$subaccount->fill($request->input());

		$context = $this->context->getInstance();

		if (!$context) {
			$subaccount->sender_id = $request->sender_id;
			$subaccount->credit_alert_level = $request->credit_alert_level;
			$subaccount->active = $request->has('active');
			$subaccount->apis()->sync($request->api_key);
		}

		$this->validate($subaccount);

		$subaccount->save();

		$this->uploadMedia($request, $subaccount);

		return $subaccount;
	}

	public function delete( $subaccount )
	{
		$subaccount = $subaccount->account_name;
		$subaccount->delete();
		return $subaccount;
	}

	public function makeUniqueSecurityKey($subaccount, $keyOnly = false)
	{
		$rawString = $subaccount->client_id . Carbon::now()->toDateTimeString();
		$secKey = Str::limit(sha1($rawString), 15, '');

		$existing = $this->model->where('security_key', $secKey)->get();
		if ($existing->count() > 0) {
			return $this->makeUniqueSecurityKey($subaccount);
		}

		if ($keyOnly) {
			return $secKey;
		}

		$subaccount->security_key = $secKey;
		$subaccount->save();

		return $secKey;
	}

	public function createDefault(Client $client)
	{
		$subaccount = $this->model;

		$subaccount->account_name = $client->company_name . ' - Sub';
		$subaccount->contact_firstname = $client->contact_first_name;
		$subaccount->credit_alert_level = 100;
		$subaccount->uuid = UuidGenerator::make();
		$subaccount->security_key = $this->makeUniqueSecurityKey($subaccount);
        $subaccount->active = 1;
		$subaccount->client_id = $client->id;
		$subaccount->city = $client->city;
		$subaccount->apis()->sync(ApiKey::DEF_GATEWAY);

		$this->validate($subaccount);

		$subaccount->save();

		$creditRepo = resolve(\ATM\Repositories\Credits\CreditRepository::class);

		$creditRepo->createDefault($subaccount);

		return $subaccount;
	}

	public function updateSettings(SubAccount $subAccount, Request $request)
    {

        $settings = array_merge((array) $subAccount->settings, (array) $request->input('settings'));
        $subAccount->settings = $settings;

        SubAccountSetting::setSettings($subAccount, $request);

        $this->validate($subAccount);

        $subAccount->save();

        return $subAccount;
    }

    private function uploadMedia(Request $request, $subaccount)
    {

        if ($request->hasFile('icon')) {
            $this->uploadRepo->createUpload($subaccount, $request, 'image', [
                'key' => Client::IMG_ICON,
                'field_key' => 'icon',
                'path' => 'uploads/sub_account',
                'filename' => 'icon',
            ]);
        }

        if ($request->hasFile('thumb')) {
            $this->uploadRepo->createUpload($subaccount, $request, 'image', [
                'key' => Client::IMG_THUMB,
                'field_key' => 'thumb',
                'path' => 'uploads/sub_account',
                'filename' => 'thumb',
                'width' => 320,
                'height' => 200,
            ]);
        }

        if ($request->hasFile('banner')) {
            $this->uploadRepo->createUpload($subaccount, $request, 'image', [
                'key' => Client::IMG_BANNER,
                'field_key' => 'banner',
                'path' => 'uploads/sub_account',
                'filename' => 'banner',
                'width' => 1366,
                'height' => 345
            ]);
        }
    }

}
