<?php

namespace ATM\Repositories\SubAccount;

use ATM\ContextInterface;
use ATM\ContextScope;
use ATM\Repositories\ApiKey\ApiKey;
use ATM\Repositories\Campaign\Campaign;
use ATM\Repositories\Cities\City;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Credits\Credit;
use ATM\Repositories\CustomerGroups\CustomerGroup;
use ATM\Repositories\Customers\Customer;
use ATM\Repositories\SenderIds\SenderIds;
use ATM\Repositories\Settings\SiteSettings;
use ATM\Repositories\SubAccount\SubAccountSetting;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Uploadable\Uploadable;
use App\Model\BaseModel;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Watson\Validating\ValidatingTrait;

class SubAccount extends BaseModel implements ContextScope
{
    use ValidatingTrait, SoftDeletes;

    const DEF_CONVERSION_RATE = 1;

    protected $casts = [
        'settings' => 'array',
    ];

	protected $rules = [
        'account_name'      => 'required',
        'contact_firstname' => 'required',
        // 'address_1'         => 'required',
        // 'city'              => 'required',
    ];

    protected $fillable = [
        'account_name','address_1', 'address_2', 'city', 'country', 'zip_code', 'website', 'telephone', 'mobile_phone',
        'contact_firstname', 'contact_lastname', 'contact_mobile_phone', 'contact_telephone', 'contact_email'
    ];

    const IMG_ICON = 'icon';
    const IMG_THUMB = 'thumb';
    const IMG_BANNER = 'banner';

    // Bypass foreign key
	public function getForeignKey()
	{
		return 'subaccount_id';
	}

	public function client()
	{
		return $this->belongsTo(Client::class);
    }

    public function users()
    {
        // return $this->hasMany(User::class);
        return $this->belongsToMany(User::class, 'sub_account_users', 'user_id', 'subaccount_id');
    }

    public function apis()
	{
		return $this->belongsToMany(ApiKey::class, 'sub_account_api_keys', 'subaccount_id', 'api_id');
    }

    public function campaigns() {
        return $this->belongsToMany(Campaign::class, 'campaign_sub_accounts', 'sub_account_id', 'campaign_id');
    }

    // @depreciated
    public function customerGroups()
    {
        return $this->belongsToMany(CustomerGroup::class, 'customer_group_sub_accounts', 'subaccount_id', 'customer_group_id')->withTimestamps();
    }

    public function customers()
    {
        return $this->hasMany(Customer::class, 'subaccount_id');
    }

	public function credit()
	{
		return $this->hasOne(Credit::class, 'subaccount_id');
    }

    public function task()
    {
        return $this->hasMany(Task::class, 'subaccount_id');
    }

	public function senderid()
	{
		return $this->belongsTo(SenderIds::class,'sender_id');
	}

	public function cityObj()
	{
		return $this->belongsTo(City::class, 'city');
	}

	// @depreciated use senderid()
    public function senderids()
    {
        return $this->belongsTo(SenderIds::class,'sender_id');
    }

    public function isActive()
    {
        return $this->active ? true : false;
    }

    public function uploads()
    {
        return $this->morphMany(Uploadable::class, 'uploadable');
    }

	// ----------------------------------
    // Scope Queries
    // ----------------------------------

    public function scopeActives($query) // actives()
    {
        return $query->where('active', true);
    }

    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->where('client_id', $context->id());
        } else if ($context->getInstance() instanceof self) {
            return $query->where('id', $context->id());
        }

        return $query;
    }

    // Mutators
	public function getImgIconAttribute() //img_icon
	{
		$uploads = $this->uploads->filter(function ($ups) {
			return $ups->key == self::IMG_ICON ? true : false;
		})->first();

		return $uploads;
	}

	public function getImgIconPathAttribute() // imc_icon_path
	{
		return $this->img_icon ? $this->img_icon->path : Uploadable::IMG_PLACEHOLDER;
	}

	public function getImgThumbAttribute()
	{
		$uploads = $this->uploads->filter(function ($ups) {
			return $ups->key == self::IMG_THUMB ? true : false;
		})->first();

		return $uploads;
	}

	public function getImgThumbPathAttribute()
	{
		return $this->img_thumb ? $this->img_thumb->path : Uploadable::IMG_PLACEHOLDER;
	}

	public function getImgBannerAttribute()
	{
		$uploads = $this->uploads->filter(function ($ups) {
			return $ups->key == self::IMG_BANNER ? true : false;
		})->first();

		return $uploads;
	}

	public function getImgBannerPathAttribute()
	{
		return $this->img_banner ? $this->img_banner->path : Uploadable::IMG_PLACEHOLDER;
	}

    // Helper Methods
    // Modify ng getForeignKey para subaccount_id na yung gamitin
    // default 'Str::snake(class_basename($this)).'
    public function nKey()
    {
        return 'subaccount_id';
    }

    // Checker for Sub Account credit balance if it can make task creation
	public function getApiKey()
	{
		if ($this->apis->count() > 0) {
			return $this->apis->first();
		}

		return null;
	}

	public function getApiKeyId()
	{
		$apiKey = $this->getApiKey();

		return $apiKey ? $apiKey->id : null;
	}

	public function isCreditSufficientForTask($charLength, $recipientCount = 1, $returnTotal = null)
	{
		$siteSettings = SiteSettings::getSettings();
		$charLimit = $siteSettings->getCharLimit();
		$credPerLimit = Task::CREDIT_LIMITER;

        $perSmsCost = $this->computeAmount($charLength, $charLimit, $credPerLimit);
		$totalSmsCost = $perSmsCost * $recipientCount;

		$credit = $this->credit;

        if (!$returnTotal) {
            return $credit->pending_balance >= $totalSmsCost;
        } else {
            return $totalSmsCost;
        }
    }

    public function isCreditSufficientForTaskWithNumber($charLength, $number, $recipientCount = 1, $returnTotal = null)
	{
		$siteSettings = SiteSettings::getSettings();
		$charLimit = $siteSettings->getCharLimit();
		$credPerLimit = Task::CREDIT_LIMITER;

        $invalidPrefixes = invalidNumberPrefixes();
        $sanitized = str_replace('+', '', $number);
        // $jobNumPrefix = substr($jobs->customer_number, 0, 5);
        $jobNumPrefix = substr($sanitized, 0, 5);

        // logger('BEECHOO SMART SUBACCOUNT - JOB NUMBER PREFIX');
        // logger($jobNumPrefix);
        // logger('BEECHOO SMART SUBACCOUNT - IS NUMBER INVALID');
        // logger(in_array($jobNumPrefix, $invalidPrefixes));

        $perSmsCost = $this->computeAmount($charLength, $charLimit, $credPerLimit);

        $origCredits = $perSmsCost;
		$creditToConsumed = $origCredits * 9; // gawan ng config

		$totalSmsCost = $creditToConsumed * $recipientCount;

		$credit = $this->credit;

        if (!$returnTotal) {
            return $credit->pending_balance >= $totalSmsCost;
        } else {
            return $totalSmsCost;
        }
    }

	public function getCreditAmount($charLength)
	{
		$siteSettings = SiteSettings::getSettings();
		$charLimit = $siteSettings->getCharLimit();
		$credPerLimit = Task::CREDIT_LIMITER;

		$smsCost = $this->computeAmount($charLength, $charLimit, $credPerLimit);

		return $smsCost;
    }

	private function computeAmount($smsLength, $charLimit, $credPerLimit)
	{
		$smsCharLength = $smsLength / $charLimit;

        $cost = ceil($smsCharLength * $credPerLimit);

		return $cost;
    }

    public function getCreditConversion()
    {
        return $this->getSetting('sms_rate_per_text') ?: self::DEF_CONVERSION_RATE;
    }

    public function convertLoadToRate($amount, $int = false)
    {
        if( $int ) {
            return (float) $this->getCreditConversion() * $amount;
        } else {
            return number_format((float) $this->getCreditConversion() * $amount, 2);
        }
    }

	public function modifyPendingBalance($amount, $type = 'inc')
	{
		$credit = $this->credit;

		if ($credit) {
			if ($type == 'inc') {
				$credit->pending_balance += $amount;
			} else {
				$credit->pending_balance -= $amount;
			}
			$credit->save();
		}

		return false;
    }

    public function isBeechoo()
    {
        // return $this->id == 127; // madison
        return $this->client_id == Client::IS_BEECHOO;
    }
}
