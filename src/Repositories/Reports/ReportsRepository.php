<?php

namespace ATM\Repositories\Reports;

use ATM\Date;
use ATM\FileGenerator;
use ATM\Repositories\BaseRepository;
use ATM\Repositories\Campaign\CampaignQueryRepository;
use ATM\Repositories\Credits\CreditQueryRepository;
use ATM\Repositories\Tasks\JobsCompleted\JobsCompletedRepository;
use ATM\Repositories\Tasks\JobsFailed\JobsFailedRepository;
use ATM\StoredProcedure\Job\JobProcedure;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportsRepository extends BaseRepository {

	public function __construct(
        JobsCompletedRepository $jobsCompRepo,
		JobProcedure $jobsStoredRepo,
        CampaignQueryRepository $campaignQueryRepo,
        JobsFailedRepository $jobsFailedRepo,
        CreditQueryRepository $creditQueryRepo,
		ReportsFacade $reportsFacade
    )
    {
    	$this->jobsCompRepo = $jobsCompRepo;
    	$this->jobsStoredRepo = $jobsStoredRepo;
		$this->jobsFailedRepo = $jobsFailedRepo;
		$this->campaignQueryRepo = $campaignQueryRepo;
		$this->creditQueryRepo = $creditQueryRepo;
		$this->reportsFacade = $reportsFacade;
    }

    public function smsPerDay(Request $request)
	{
		$fromDate = Carbon::now()->subDays(7)->startOfDay();
		$toDate = Carbon::now()->endOfDay();

		if ($request->filled('from_date') && $request->filled('to_date')) {
			$fromDate = Carbon::parse($request->from_date)->startOfDay();
			$toDate = Carbon::parse($request->to_date)->endOfDay();
		}

		$request['from_date'] = $fromDate->toDateString();
		$request['to_date'] = $toDate->toDateString();

		$completed = $this->jobsStoredRepo->searchJobsCompleted($request);
		$failed = $this->jobsStoredRepo->searchJobsFailed($request);
		$jobs = $this->jobsStoredRepo->searchJobsToday($request);
		$jobs = $jobs->concat($this->jobsStoredRepo->searchJobsTomorrow($request));
		$jobs = $jobs->concat($this->jobsStoredRepo->searchJobsAfter14thToTomorrow($request));
		$jobs = $jobs->concat($this->jobsStoredRepo->searchJobs15thToEndMonth($request));
		$jobs = $jobs->concat($this->jobsStoredRepo->searchJobsNextMonth($request));

		$results = $this->preparePerDay($request, $completed, $failed);
		$results = $this->additionalJobsPerDayData($request, $results, $jobs);

		return $results;
	}

	private function preparePerDay(Request $request, $completed, $failed)
	{
		$fromDate = new Carbon($request->from_date);
		$toDate = new Carbon($request->to_date);

		$grouped = compact('completed', 'failed');

		$allArr = [];

		while ($fromDate <= $toDate) {
			$allArr[$fromDate->toDateString()] = [
				'recipients' => 0,
				'success_sms' => 0,
				'credits_used' => 0,
				'failed_sms' => 0,
			];
			$fromDate->addDay();
		}

		foreach ($grouped as $key => $group) {
			if ($key == 'completed') {
				$group = $group->sortBy('when_to_send');
			} else {
				$group = $group->sortBy('time_attempt_send');
			}
			foreach ($group as $job) {
				if ($key == 'completed') {
					$date = Carbon::parse($job->when_to_send)->toDateString();
					$allArr[$date]['success_sms'] += 1;
					$allArr[$date]['credits_used'] += $job->credits_consumed;
				} else {
					$date = Carbon::parse($job->time_attempt_send)->toDateString();
					$allArr[$date]['failed_sms'] += 1;
				}
				$allArr[$date]['recipients'] += 1;
			}
		}

		return $allArr;
	}

	public function additionalJobsPerDayData($request, $dataArr, $jobs)
	{
		$fromDate = new Carbon($request->from_date);
		$toDate = new Carbon($request->to_date);

		if (count($dataArr) < 1) {
			for( $i = 1; $i <= 12; $i++) {
				$dataArr[$fromDate->toDateString()] = [
					'recipients' => 0,
					'success_sms' => 0,
					'credits_used' => 0,
					'failed_sms' => 0,
				];
				$fromDate->addDay();
			}
		}

		foreach ($jobs as $job) {
			$date = Carbon::parse($job->when_to_send)->toDateString();
			$dataArr[$date]['recipients'] += 1;
		}

		return $dataArr;
	}

    public function smsPerMonth(Request $request)
	{
		if ($request->filled('year')) {
			$fromDate = Carbon::create($request->year, 1, 1)->startOfYear()->toDateTimeString();
			// $toDate = Carbon::now()->endOfDay()->toDateTimeString();
			$toDate = Carbon::create($request->year, 1, 1)->endOfYear()->toDateTimeString();
		}

		$request['from_date'] = $fromDate;
		$request['to_date'] = $toDate;

		// $completed = $this->jobsCompRepo->search($request);
		// $failed = $this->jobsFailedRepo->search($request);

		$completed = $this->jobsStoredRepo->searchJobsCompleted($request);
        $failed = $this->jobsStoredRepo->searchJobsFailed($request);
		$jobs = $this->jobsStoredRepo->searchJobsToday($request);
		$jobs = $jobs->concat($this->jobsStoredRepo->searchJobsTomorrow($request));
		$jobs = $jobs->concat($this->jobsStoredRepo->searchJobsAfter14thToTomorrow($request));
		$jobs = $jobs->concat($this->jobsStoredRepo->searchJobs15thToEndMonth($request));
		$jobs = $jobs->concat($this->jobsStoredRepo->searchJobsNextMonth($request));

		$results = $this->preparePerMonth($request, $completed, $failed);
		$results = $this->additionalMonthsJobsData($results, $jobs);

		return $results;
	}

	private function preparePerMonth(Request $request, $completed, $failed)
	{
		$fromDate = new Carbon($request->from_date);
		$toDate = new Carbon($request->to_date);

		$grouped = compact('completed', 'failed');

		$allArr = [];
		for( $i = 1; $i <= 12; $i++) {
			$allArr[$i] = [
				'recipients' => 0,
				'success_sms' => 0,
				'failed_sms' => 0,
				'credits_used' => 0,
			];
		}

		foreach ($grouped as $key => $group) {
			if ($key == 'completed') {
				$group = $group->sortBy('when_to_send');
			} else {
				$group = $group->sortBy('time_attempt_send');
			}
			foreach ($group as $job) {
				$monthNo = null;
				if ($key == 'completed') {
					$monthNo = Carbon::parse($job->when_to_send)->format('n');
					$allArr[$monthNo]['success_sms'] += 1; // $job->success_jobs_count;
					$allArr[$monthNo]['credits_used'] += $job->credits_used;
				} else {
					$monthNo = Carbon::parse($job->time_attempt_send)->format('n');
					$allArr[$monthNo]['failed_sms'] += 1;
				}
				$allArr[$monthNo]['recipients'] += 1;
			}
		}
		return $allArr;
	}

	public function additionalMonthsJobsData($dataArr = [], $jobs)
	{
		if (count($dataArr) < 1) {
			for( $i = 1; $i <= 12; $i++) {
				$dataArr[$i] = [
					'recipients' => 0,
					'success_sms' => 0,
					'failed_sms' => 0,
					'credits_used' => 0,
				];
			}
		}

		foreach ($jobs as $job) {
			$monthNo = null;
			$monthNo = Carbon::parse($job->when_to_send)->format('n');
			$dataArr[$monthNo]['recipients'] += 1;
		}

		return $dataArr;
	}

	public function creditBalance(Request $request)
	{
        $fromDate = null;
		if ($request->filled('from_date')) {
			$fromDate = new Carbon($request->from_date);
			$fromDate = $fromDate->startOfDay()->toDateTimeString();
        }

        $toDate = null;
		if ($request->filled('to_date')) {
			$toDate = new Carbon($request->to_date);
			$toDate = $toDate->endOfDay()->toDateTimeString();
		}

        $campaignId = null;
		if ($request->filled('campaign_filter')) {
            $campaignId = $request->campaign_filter;
		}

		$subAccountId = null;
		if ($request->filled('subaccount_id')) {
			$subAccountId = $request->subaccount_id;
		}

		if ($request->filled('sub_account_id')) {
			$subAccountId = $request->sub_account_id;
        }

		$request['from_date'] = $fromDate;
		$request['to_date'] = $toDate;
		$request['campaign_filter'] = $campaignId;
		$request['sub_account_id'] = $subAccountId;

		$results = $this->creditQueryRepo->search($request,true);

		return $results;
	}

	public function smsPerCampaign(Request $request)
	{
		$results = $this->campaignQueryRepo->search($request,true);
		$final = collect($this->reportsFacade->getPerCampaignsData($request, $results));

		return $final;
	}

	public function smsPerCampaignExport(Request $request)
	{
		$client = $request->user()->client;
		$moneyColumn = [];

		$namePrefix = $client->company_name;

		FileGenerator::makeExcel(
			'',
			$namePrefix . ' - SMS per Campaign - ' . $request['from_date'] . ' - ' . $request['to_date'],
			'SMS per Campaign',
			'partials.report-export.jobs.jobs-per-campaign',
			compact('tasksPerCampaign'),
			false,
			$moneyColumn
		);
	}

	public function successSms(Request $request, $perPage = null)
	{
		if (!$request->filled('from_date') && !$request->filled('to_date')) {
			$fromDate = Carbon::now()->subDays(7)->startOfDay();
			$toDate = Carbon::now()->endOfDay();
			$request['from_date'] = $fromDate->toDateString();
			$request['to_date'] = $toDate->toDateString();
		}
		
		$client = $request->user()->client;
		$request['client_id'] = $client->id;

		$completed = $this->jobsStoredRepo->searchJobsCompleted($request);

		if ($request->filled('per_page') && $request->per_page != 'all' && !$request->filled('export_excel')) {
			return $this->paginateCollection($completed, $request->page, $request->per_page ?: self::PER_PAGE);
		} else {
			return $completed;
		}
	}

	public function smsPerSubAccount(Request $request, $perPage = null)
	{
		if (!$request->filled('from_date') && !$request->filled('to_date')) {
			$fromDate = Carbon::now()->subDays(7)->startOfDay();
			$toDate = Carbon::now()->endOfDay();
			$request['from_date'] = $fromDate->toDateString();
			$request['to_date'] = $toDate->toDateString();
		}

		$client = $request->user()->client;
		$request['client_id'] = $client->id;

		$results = $this->jobsStoredRepo->searchJobsPerSubAccount($request);
		$results = collect($this->reportsFacade->getPerSubAccount($request, $results));

		if ($request->filled('sub_account_id')) {
			$results = $results->filter(function ($res) use ($request) {
				return isset($res['id']) && $request->sub_account_id == $res['id'];
			})->values();
		}

		if ($request->filled('per_page') && $request->per_page != 'all' && !$request->filled('export_excel')) {
			return $this->paginateCollection($results, $request->page, $request->per_page ?: self::PER_PAGE);
		} else {
			return $results;
		}
	}

	public function smsPending(Request $request, $perPage = null)
	{
		if (!$request->filled('from_date') && !$request->filled('to_date')) {
			$fromDate = Carbon::now()->subDays(7)->startOfDay();
			$toDate = Carbon::now()->endOfDay();
			$request['from_date'] = $fromDate->toDateString();
			$request['to_date'] = $toDate->toDateString();
		}

		$client = $request->user()->client;
		$request['client_id'] = $client->id;

		$results = collect($this->jobsStoredRepo->searchJobsToday($request));
		$results = $results->merge(collect($this->jobsStoredRepo->searchJobsTomorrow($request)));
		$results = $results->merge(collect($this->jobsStoredRepo->searchJobsAfter14thToTomorrow($request)));
		$results = $results->merge(collect($this->jobsStoredRepo->searchJobs15thToEndMonth($request)));
		$results = $results->merge(collect($this->jobsStoredRepo->searchJobsNextMonth($request)));

		if ($request->filled('per_page') && $request->per_page != 'all' && !$request->filled('export_excel')) {
			return $this->paginateCollection($results, $request->page, $request->per_page ?: self::PER_PAGE);
		} else {
			return $results;
		}
	}

	public function smsFailed(Request $request, $perPage = null)
	{
		if (!$request->filled('from_date') && !$request->filled('to_date')) {
			$fromDate = Carbon::now()->subDays(7)->startOfDay();
			$toDate = Carbon::now()->endOfDay();
			$request['from_date'] = $fromDate->toDateString();
			$request['to_date'] = $toDate->toDateString();
		}

		$client = $request->user()->client;
		$request['client_id'] = $client->id;

		$results = collect($this->jobsStoredRepo->searchJobsFailed($request));

		if ($request->filled('per_page') && $request->per_page != 'all' && !$request->filled('export_excel')) {
			return $this->paginateCollection($results, $request->page, $request->per_page ?: self::PER_PAGE);
		} else {
			return $results;
		}
	}
}
