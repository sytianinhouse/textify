<?php
/**
 * Created by PhpStorm.
 * User: jr
 * Date: 6/21/2019
 * Time: 11:37 AM
 */

namespace ATM\Repositories\Reports;

use ATM\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportsFacade extends BaseRepository
{
	public function __construct()
	{

	}

	public function getPerCampaignsData(Request $request, $data)
	{
		$final = [];

		foreach ($data as $item) {
			$thisData = [];
			$thisData['name'] = $item->name;
			$thisData['camp_date'] = Carbon::parse($item->created_at)->toDayDateTimeString();
			$thisData = $this->separateStats($thisData, $item->completed_stats, 'completed');
			$thisData = $this->separateStats($thisData, $item->failed_stats, 'failed');
			$thisData = $this->separateStats($thisData, $item->today_stats, 'today');
			$thisData = $this->separateStats($thisData, $item->tomorrow_stats, 'tomorrow');
			$thisData = $this->separateStats($thisData, $item->fourth_stats, 'fourth');
			$thisData = $this->separateStats($thisData, $item->fifth_stats, 'fifth');
			$thisData = $this->separateStats($thisData, $item->nextmonth_stats, 'nextmonth');

			$totalJobCount = array_sum([
				$thisData['completed_count'],
				$thisData['failed_count'],
				$thisData['today_count'],
				$thisData['tomorrow_count'],
				$thisData['fourth_count'],
				$thisData['fifth_count'],
				$thisData['nextmonth_count'],
			]);

			// Credits used lang to
			$totalCredits = array_sum([
				$thisData['completed_credits'],
			]);

			$thisData['total_count'] = $totalJobCount;
			$thisData['total_credits'] = $totalCredits;
			$final[] = $thisData;
		}

		return $final;
	}

	public function getPerSubAccount(Request $request, $data)
	{
		$final = [];

		foreach ($data as $item) {
			$thisData = [];
			$thisData['id'] = $item->id;
			$thisData['name'] = $item->account_name;
			$thisData['client_id'] = $item->client_id;
			$thisData = $this->makeSeparatedData($item, $thisData);

			$totalJobCount = array_sum([
				$thisData['completed_count'],
				$thisData['failed_count'],
				$thisData['today_count'],
				$thisData['tomorrow_count'],
				$thisData['fourth_count'],
				$thisData['fifth_count'],
				$thisData['nextmonth_count'],
			]);

			// Credits used lang to
			$totalCredits = array_sum([
				$thisData['completed_credits'],
			]);

			$thisData['total_count'] = $totalJobCount;
			$thisData['total_credits'] = $totalCredits;
			$final[] = $thisData;
		}

		return $final;
	}

	private function makeSeparatedData($data, $array)
	{
		$array = $this->separateStats($array, $data->completed_stats, 'completed');
		$array = $this->separateStats($array, $data->failed_stats, 'failed');
		$array = $this->separateStats($array, $data->today_stats, 'today');
		$array = $this->separateStats($array, $data->tomorrow_stats, 'tomorrow');
		$array = $this->separateStats($array, $data->fourth_stats, 'fourth');
		$array = $this->separateStats($array, $data->fifth_stats, 'fifth');
		$array = $this->separateStats($array, $data->nextmonth_stats, 'nextmonth');

		return $array;
	}

	private function separateStats($array, $value, $key)
	{
		if (!in_array($value, ['', NULL])) {
			$exploded = explode('--', $value);
			$array["{$key}_count"] = intval(isset($exploded[0]) ? $exploded[0] : 0);
			$array["{$key}_credits"] = intval(isset($exploded[1]) ? $exploded[1] : 0);

			return $array;
		}

		return $array;
	}
}
