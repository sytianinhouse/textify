<?php

namespace ATM\Repositories\Credits;

use ATM\ContextScope;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Credits\CreditLog;
use ATM\Repositories\SubAccount\SubAccount;
use App\Model\BaseModel;

class Credit extends BaseModel
{
    protected $rules = [
		'subaccount_id' => 'required',
		'client_id' => 'required',
		'balance' => 'required',
	];

    protected $fillable = ['balance', 'notes']; //'client_id', 'sub_account_id'

    protected $dates = ['created_at', 'updated_at'];

    public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function subAccount()
	{
		return $this->belongsTo(SubAccount::class, 'subaccount_id');
	}

	public function creditLogs()
	{
		return $this->hasMany(CreditLog::class);
	}

}
