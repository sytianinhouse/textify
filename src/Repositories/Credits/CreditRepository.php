<?php

namespace ATM\Repositories\Credits;

use ATM\ContextInterface;
use ATM\Repositories\BaseRepository;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\Credits\Credit;
use ATM\Repositories\Loads\Load;
use ATM\Repositories\Settings\SiteSettings;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountRepository;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

/**
 * Off context on other parts of query
 */
class CreditRepository extends BaseRepository {

	protected $model;

	public function __construct(
		Credit $credit,
		SubAccountRepository $subRepo,
		CreditLogRepository $logRepo
	)
	{
		$this->model = $credit;
		$this->subRepo = $subRepo;
		$this->logRepo = $logRepo;
	}

	public function search(Request $request, $perPage = self::PER_PAGE, $with = [])
	{

		$query = $this->model->with($with)
			->whereHas('subAccount', function($q) use ($request) {
				if ($request->active) {
					$q = $q->actives();
				}
			})
			->orderByDesc('created_at');

			if ($request->active) {
				$query = $query->whereHas('client', function($q) use ($request) {
					if ($request->active) {
						$q = $q->actives();
					}
				});
			}

		if ($request->filled('client_id')) {
			$query = $query->where('client_id', $request->client_id);
		}

		if ($request->filled('project_id')) {
			$query = $query->whereHas('client', function($q) use ($request) {
				if ($request->filled('client_id')) {
					$q->where('id', $request->client_id);
				}

				$q->whereHas('project', function($q) use ($request) {
					$q->where('id', $request->project_id);
				});
			});
		}

		if ($request->filled('sub_account_id')) {
			$query = $query->where('subaccount_id', $request->subaccount_id);
		}

		if ($request->filled('subaccount_id')) {
			$query = $query->where('subaccount_id', $request->subaccount_id);
		}

		if ($request->filled('from_date') && $request->filled('to_date')) {
			$fromDate = Carbon::parse($request->from_date)->toDateString();
			$toDate = Carbon::parse($request->to_date)->toDateString();

			$query = $query->whereDate('created_at', '>=', $fromDate)
						->whereDate('created_at', '<=', $toDate);

		}

        if ($perPage == self::DISPLAY_ALL) {
            $results = $query->get();
        } else if($perPage != null) {
            $results = $query->paginate($perPage);
		} else {
			$results = $query->get();
		}

		return $results;

    }

	public function add(Load $load, Request $request)
	{
		$subAccount = $load->subAccount;

		$credit = Credit::firstOrNew([
			'client_id' => $load->client_id,
			'subaccount_id' =>  $load->subaccount_id
		]);

		$credit->client_id = $load->client_id;
		$credit->subaccount_id = $load->subaccount_id;

		$rate = $subAccount->convertLoadToRate($load->load_amount, true);

		$actualAmount = $rate;
		$pendingAmount = $rate;

		if ($credit->exists) {
			$actualAmount = $credit->actual_balance + $actualAmount;
			$pendingAmount = $credit->pending_balance + $pendingAmount;
		}

		$credit->actual_balance = $actualAmount;
		$credit->pending_balance = $pendingAmount;
		$credit->save();
		$this->logRepo->add($credit, 'replenish', $load->load_amount, $request);

		if ( ! $credit->save() ) {
            return redirect()->back()
                        ->withInput()
                        ->withErrors($credit->getErrors())
                        ->withError("Something Has Gone Wrong.");
        }

	}

	// public function sub(Load $load)
	// {
	// 	$subAccount = $load->subAccount;

	// 	$credit = Credit::where('client_id', $load->client_id)
	// 		->where('subaccount_id', $load->subaccount_id)
	// 		->first();

	// 	$rate = $subAccount->convertLoadToRate($load->load_amount, true);

	// 	$credit->balance = $credit->balance - $rate;
	// 	$credit->save();

	// 	$this->logRepo->add($credit, 'expense', $load->load_amount, $request);

	// 	if ( ! $credit->save() ) {
 //            return redirect()->back()
 //                ->withInput()
 //                ->withErrors($credit->getErrors())
 //                ->withError("Something Has Gone Wrong.");
 //        }

	// }

	// public function update($previousDetails, Load $load, Request $request)
	// {
	// 	$subAccount = $load->subAccount;

	// 	$credit = $this->model->where('client_id', $load->client_id)
	// 		->where('subaccount_id', $load->subaccount_id)
	// 		->first();

	// 	$rate = $subAccount->convertLoadToRate($load->load_amount, true);

	// 	$actualAmount = $rate;
	// 	$pendingAmount = $rate;

	// 	if ($previousDetails['type'] == 'sub') {
	// 		$actualAmount = $credit->actual_balance - $actualAmount;
	// 		$pendingAmount = $credit->pending_balance - $pendingAmount;
	// 	} else {
	// 		$actualAmount = $credit->actual_balance + $actualAmount;
	// 		$pendingAmount = $credit->pending_balance + $pendingAmount;
	// 	}

	// 	$credit->actual_balance = $actualAmount;
	// 	$credit->pending_balance = $pendingAmount;
	// 	$credit->save();

	// 	$this->logRepo->add($credit, 'expense', $load->load_amount, $request);
	// 	$this->logRepo->add($credit, 'modify');

	// 	return $credit;
	// }

	public function updateManual(Credit $credit, Request $request)
	{
		$this->validateCreditData($request);

		if (count($this->errors) > 0) {
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		$oldActualBalance = $credit->actual_balance;
		$oldPendingBalance = $credit->pending_balance;

		// $previousAmount = $credit->balance;
		$credit->actual_balance = $request->actual_balance;
		$credit->pending_balance = $request->pending_balance;
		$credit->notes = $request->notes;
		$credit->save();

		if ($oldActualBalance != $request->actual_balance) {
			$this->logRepo->add($credit, 'modify', $request->actual_balance, $request, $oldActualBalance, null, 'actual_balance');
		}

		if ($oldPendingBalance != $request->pending_balance) {
			$this->logRepo->add($credit, 'modify', $request->pending_balance, $request, $oldPendingBalance, null, 'pending_balance');
		}

		return $credit;
	}

	public function createDefault(SubAccount $subaccount)
	{

		$client = $subaccount->client;

		$credit = $this->model;
		$credit->client_id = $client->id;
		$credit->subaccount_id = $subaccount->id;
		$credit->actual_balance = 0;
		$credit->pending_balance = 0;
		$credit->save();

	}

	public function validateCreditData(Request $request)
	{
		$rules = [
			'notes' => 'required',
			'actual_balance' => 'required|numeric',
			'pending_balance' => 'required|numeric',
		];

		$validator = \Validator::make($request->all(), $rules);

		if ($validator->fails()) {
		    $this->errors = $validator->messages()->merge($this->errors);
		}
	}

	public function delete(Credit $credit)
	{
		$credit->delete();

		return true;
	}

}
