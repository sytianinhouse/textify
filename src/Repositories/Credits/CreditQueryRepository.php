<?php

namespace ATM\Repositories\Credits;

use ATM\ContextInterface;
use ATM\Repositories\BaseRepository;
use ATM\StoredProcedure\Credits\CreditsProcedure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class CreditQueryRepository extends BaseRepository
{
	protected $model;

	public function __construct(
		Credit $credit,
		ContextInterface $context,
		CreditsProcedure $creditProd)
	{
		$this->model = $credit;
		$this->context = $context;
		$this->creditProd = $creditProd;
	}

	public function search(Request $request, $useStored, $columns = '*', $with = [], $perPage = false)
	{
		if ($useStored) {
			return $this->searchUseStored($request, $perPage);
		}
	}

	public function searchUseStored(Request $request, $perPage = false)
	{
		$credits = $this->creditProd->searchCredits($request);

		$credits = collect($credits);

        $credits->each(function($item) {
            $date = \Carbon\Carbon::parse($item->when_to_send);
            $item->when_to_send = $date->toFormattedDateString();
        });

		if ($perPage) {
			return $this->paginateCollection($credits, $request->page, $request->per_page ?: self::PER_PAGE);
		} else {
			return $credits;
		}
	}
}
