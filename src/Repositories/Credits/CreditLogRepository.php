<?php

namespace ATM\Repositories\Credits;

use ATM\ContextInterface;
use ATM\Repositories\BaseRepository;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\Credits\Credit;
use ATM\Repositories\Credits\CreditLog;
use ATM\Repositories\SubAccount\SubAccountRepository;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

/**
 * Off context on other parts of query
 */
class CreditLogRepository extends BaseRepository {
	
	protected $model;

	public function __construct(
		CreditLog $creditLog
	)	
	{
		$this->model = $creditLog;
	}

	public function search(Request $request, $perPage = self::PER_PAGE, $with = [])
	{
		
		$query = $this->model->with($with)->orderBy('date_time', 'desc');

		if ($request->filled('project_id')) {
			$query = $query->whereHas('subAccount', function($sub) use ($request) {

				if ($request->filled('sub_account_id')) {
					$sub->where('id', $request->sub_account_id);
				}

				if ($request->filled('subaccount_id')) {
					$sub->where('id', $request->subaccount_id);
				}

				$sub->whereHas('client', function($client) use ($request) {
					if ($request->filled('client_id')) {
						$client->where('id', $request->client_id);
					}
					$client->whereHas('project', function($proj) use ($request) {
						$proj->where('id', $request->project_id);
					});
				});
			});
		}	

		if ($request->filled('type')) {
			$query = $query->where('type', $request->type);
		}

		if ($request->filled('from_date') && $request->filled('to_date')) {
			$fromDateTimeString = Carbon::parse($request->from_date);
			$toDateTimeString = Carbon::parse($request->to_date);
		} else {
			$fromDateTimeString = Carbon::now()->startOfMonth();
			$toDateTimeString = Carbon::now()->endOfDay();
		}

		$request['from_date'] = $fromDateTimeString->copy()->toDateString();
		$request['to_date'] = $toDateTimeString->copy()->toDateString();

		$query = $query->where('date_time', '>=' , $fromDateTimeString->toDateTimeString())
					->where('date_time', '<=', $toDateTimeString->toDateTimeString());

		
        if ($perPage == self::DISPLAY_ALL) {
            $results = $query->get();
        } else {
            $results = $query->paginate($perPage);
		}

		return $results;
	}

	public function add(Credit $credit, $type, $amount = null, Request $request, $previousAmount = null, $customMessage = null, $balanceType = 'balance')
	{
		$user = $request->user();

		$log = $this->model;
		$log->credit_id = $credit->id;
		$log->subaccount_id = $credit->subAccount->id;
		$log->user_id = $user->id;
		$log->type = $type;
		$log->date_time = Carbon::now()->toDateTimeString();

		if ($customMessage) {
			$notes = $customMessage;
		} else {
			$balanceTypeStr = '';
			if ($balanceType == 'actual_balance') {
				$balanceTypeStr = 'actual balance';
			} else if($balanceType == 'pending_balance') {
				$balanceTypeStr = 'pending balance';
			}

			$notes = $this->createNotes($type, $amount, $credit, $user, $previousAmount, $balanceTypeStr);
		}

		$log->notes = $notes;

		$log->save();

		if ( ! $log->save() ) {
            return redirect()->back()
                        ->withInput()
                        ->withErrors($credit->getErrors())
                        ->withError("Something Has Gone Wrong.");
        }
	}

	public function createNotes($type, $amount, Credit $credit, User $user, $previousAmount = null, $balanceType = 'balance')
	{
		$string = '';
		if ($type == CreditLog::ADD) {
			$string = $user->username . ' ' . CreditLog::ADD . 'ed ' . number_format($amount,0) . ' to ' . $credit->subAccount->account_name . " {$balanceType}." ;
		} elseif ($type == CreditLog::DEDUCT) {
			$string = $user->username . ' ' . CreditLog::DEDUCT . 'ed ' . number_format($amount,0) . ' to ' . $credit->subAccount->account_name . " {$balanceType}." ;
		} elseif ($type == CreditLog::EDIT) {
			$string = $user->username . ' modified ' . " {$balanceType} from " . number_format($previousAmount,0) . ' to ' . number_format($amount,0) . ' on ' . Carbon::now()->format('M d, Y h:i:s A') ;
		}

		return $string;
	}

	public function sub(Load $load)
	{
		
		$credit = Credit::where('client_id', $load->client_id)
			->where('subaccount_id', $load->subaccount_id)
			->first();
		
		$credit->balance = $credit->balance - $load->load_amount;
		$credit->save();

		if ( ! $credit->save() ) {
            return redirect()->back()
                        ->withInput()
                        ->withErrors($credit->getErrors())
                        ->withError("Something Has Gone Wrong.");
        }

	}

	public function update($previousDetails, Load $load, Request $request)
	{

		$credit = $this->model->where('client_id', $load->client_id)
			->where('subaccount_id', $load->subaccount_id)
			->first();
		

		if ($previousDetails['type'] == 'sub') {
			$difference = $previousDetails['previousAmount'] - $request->load_amount;
			$result = $credit->balance - $difference;
		} else {
			$difference = $request->load_amount - $previousDetails['previousAmount'];
			$result = $credit->balance + $difference;
		}
		
		$credit->balance = $result;
		$credit->save();
		// dd($credit);
		return $credit;
	}

	public function updateManual(Credit $credit, Request $request)
	{

		$this->validateCreditData($request);

		if (count($this->errors) > 0) {
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		$credit->balance = $request->balance;
		$credit->notes = $request->notes;
		
		$credit->save();
		// dd($credit);
		return $credit;
	}


	public function validateCreditData(Request $request)
	{
		$rules = [
			'notes' => 'required',
			'balance' => 'required|numeric',
		];

		$validator = \Validator::make($request->all(), $rules);

		if ($validator->fails()) {
		    $this->errors = $validator->messages()->merge($this->errors);
		} 

	}

	public function delete(Credit $credit)
	{
		$credit->delete();

		return true;
	}

}