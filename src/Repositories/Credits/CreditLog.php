<?php

namespace ATM\Repositories\Credits;

use ATM\ContextScope;
use App\Model\BaseModel;
use ATM\Repositories\Credits\Credit;
use ATM\Repositories\SubAccount\SubAccount;

class CreditLog extends BaseModel
{
    public $timestamps = false;

    const ADD = 'replenish';
    const DEDUCT = 'expense';
    const EDIT = 'modify';

    public static $types = [
        self::ADD, self::DEDUCT, self::EDIT,
    ];

    protected $dates = ['date_time'];

    public function credit()
	{
		return $this->belongsTo(Credit::class);
	}

	public function subAccount()
	{
		return $this->belongsTo(SubAccount::class, 'subaccount_id');
	}

	// public function scopeOfContext($query, ContextInterface $context)
 //    {
 //        if ($context->getInstance() instanceof Client) {
 //            return $query->where($context->column(), $context->id());
 //        } else if ($context->getInstance() instanceof SubAccount) {
 //            return $query->whereHas('client.s', function($q) use ($context) {
 //                return $q->where('id', $context->id());
 //            });
 //        }

 //        return $query;
 //    }
}
