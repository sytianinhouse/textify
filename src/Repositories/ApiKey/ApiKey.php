<?php

namespace ATM\Repositories\ApiKey;

use ATM\ContextInterface;
use ATM\ContextScope;
use ATM\Repositories\Client\Client;
use ATM\Repositories\SubAccount\SubAccount;
use App\Model\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class ApiKey extends BaseModel
{
    use SoftDeletes;

    const DEF_GATEWAY = 3;

	protected $rules = [
        'name'           => 'required',
        'api_key'        => 'required'
    ];

    protected $fillable = [
        'name','api_key','description'
    ];

	public function subaccounts()
	{
        return $this->belongsToMany(SubAccount::class, 'sub_account_api_keys', 'api_id', 'subaccount_id' );
    }

	public function theGateways()
	{
		return $this->get();
    }

	public static function getGateways()
	{
		$instance = new static;

		return $instance->theGateways();
    }

    /**
	 * helper methods
	 */
	public function isGlobelabs()
	{
		return $this->id == 1;
    }

	public function isPromotexter()
	{
		return $this->id == 2;
    }

	public function isSytian()
	{
		return $this->id == 3;
    }
}
?>
