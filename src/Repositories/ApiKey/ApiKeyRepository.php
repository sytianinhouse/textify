<?php

namespace ATM\Repositories\ApiKey;

use ATM\ContextInterface;
use ATM\Repositories\ApiKey\ApiKey;
use ATM\Repositories\Client\Client;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\SubAccount\SubAccount;

use ATM\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ApiKeyRepository extends ContextRepository {

	protected $model;

	public function __construct(ApiKey $apikey, ContextInterface $context)
	{
		$this->model = $apikey;
		$this->context = $context;
	}

	public function search(Request $request, $with = [])
	{
		$query = $this->scope($with);

		if ($request->filled('sub_account_id')) {
			$query = $query->whereHas('subaccounts', function($q) use ($request) {
				$q->where('subaccount_id', $request->sub_account_id);
			});
		}

		if ($request->filled('filter_search')) {
			$query = $query->where('name','LIKE',"%{$request->filter_search}%");
		}

		if ($request->filled('per_page')) {
			return $query->paginate($request->per_page);
		} else {
			return $query->get();
		}
	}

	public function store(ApiKey $apikey = null, Request $request)
	{
		$apikey = $this->model;
		$apikey->fill($request->input());
		$apikey->client_id = $request->client_id;
		$this->validate($apikey);

		// @JR no need na to save yung sub account, sa sub account module magkakaroon ng dropdown of this API KEYS
		/*if (!$request->filled('sub_account_id')) {
			$this->throwValidationException([
        		'sub_account_id' => 'Sub account is required',
        	]);
		}*/

		$apikey->save();
		//$apikey->subaccounts()->sync($request->sub_account_id);

		return $apikey;
	}

	public function update(ApiKey $apikey, Request $request)
	{
		$apikey->fill($request->input());
		// @JR no need na to save yung sub account, sa sub account module magkakaroon ng dropdown of this API KEYS
		//$apikey->subaccounts()->sync($request->sub_account_id);

		$this->validate($apikey);

		$apikey->save();

		return $apikey;
	}

	public function delete(ApiKey $apikey)
	{
		$apikey->subaccounts()->detach();
		$apikey->delete();

		return $apikey;
	}
}
