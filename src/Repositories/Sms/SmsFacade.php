<?php

namespace ATM\Repositories\Sms;

use ATM\Repositories\ApiGateway\GatewayException;
use ATM\Repositories\ApiGateway\GlobeLabsFacade;
use ATM\Repositories\ApiGateway\PromoTexterFacade;
use ATM\Repositories\ApiGateway\SytianSmsFacade;
use ATM\Repositories\BaseRepository;
use ATM\Repositories\Credits\Credit;
use ATM\Repositories\SenderIds\SenderIds;
use ATM\Repositories\Settings\SiteSettings;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\Repositories\SubAccount\SubAccountSetting;
use ATM\Repositories\Tasks\JobsCompleted\JobsCompleted;
use ATM\Repositories\Tasks\JobsFailed\JobsFailed;
use ATM\Repositories\Tasks\JobsFailed\JobsFailedRepository;
use ATM\Repositories\Tasks\JobsTodaySend\JobToday;
use ATM\Repositories\Tasks\Task;
use ATM\Repositories\Tasks\TaskRepository;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;

class SmsFacade extends BaseRepository
{
	protected $jobsToday;

	public function __construct(
		GlobeLabsFacade $globelabs,
		PromoTexterFacade $promoTexter,
		SytianSmsFacade $sytianSms,
		MessageBag $messageBag,
		JobsFailedRepository $jobsFailedRepo,
		TaskRepository $taskRepository
	)
	{
		$this->globelabs = $globelabs;
		$this->messageBag = $messageBag;
		$this->promoTexter = $promoTexter;
		$this->sytianSms = $sytianSms;
		$this->jobsFailedRepo = $jobsFailedRepo;
		$this->taskRepository = $taskRepository;
	}

	public function getAllJobsForToday( $jobsToday ){
		$settings = SiteSettings::getSettings();
		$disabledSending = $settings ? $settings->sending_disabled : false;

		$today = Carbon::now();
		if ( sizeof($jobsToday) >= 1 ):
			$this->makeQueue($jobsToday); // Mark the jobs get into onqueue
			$success_jobs_count = 1;
			$failed_jobs_count = 1;
			Log::info("SMS jobs for today started with count of: {$jobsToday->count()}");

			$groupedByTask = $jobsToday->groupBy('task_id');
			foreach ($groupedByTask as $groupJobsToday) {
				$firstJob = $groupJobsToday->first();
				$subAccount = $firstJob->subAccount;

				$gateway = $subAccount->getApiKey();
				$task = $firstJob->task;

				$successCredits = 0;
				$successCount = 0;
				$failedCredits = 0;
				$failedCount = 0;

				foreach ( $groupJobsToday as $jobs ):

					// if ($jobs->gateway) { // temp remove for Sytian Gateway Error 
					// 	$gateway = $jobs->gateway;
					// }
					$isFailed = false;
					$isSuccess = false;

					$failedJobCreated = false;
					$errorKey = false;
					$senderId = $jobs->subAccount->senderid ? $jobs->subAccount->senderid->name : SenderIds::DEFAULT_SENDERID;
					
					// $smartSunDitoClients = [70, 1]; // beechoo
					// if (in_array($jobs->client_id, $smartSunDitoClients)) {
					// 	$invalidPrefixes = invalidNumberPrefixes();
					// 	$sanitized = str_replace('+', '', $jobs->customer_number);
					// 	// $jobNumPrefix = substr($jobs->customer_number, 0, 5);
					// 	$jobNumPrefix = substr($sanitized, 0, 5);

					// 	logger('JOB NUMBER PREFIX');
					// 	logger($jobNumPrefix);
					// 	logger('IS NUMBER INVALID');
					// 	logger(in_array($jobNumPrefix, $invalidPrefixes));

					// 	// if (in_array($jobNumPrefix, $invalidPrefixes)) {
					// 	// 	// Create fail job
					// 	// 	$failed = $this->sendToJobsFailed($jobs, true, 'Smart/Sun/DITO Number', explode(",", $jobs->id));
					// 	// 	$isFailed = true;
					// 	// 	$failedCredits += $jobs->credits_consumed;
					// 	// 	$failedCount += 1;
					// 	// 	$failed_jobs_count++;
					// 	// 	continue;
					// 	// }
					// }

					if ($jobs->when_to_send->diffInDays(today()) > 2) {
						$failed = $this->sendToJobsFailed($jobs, true, 'Failed to send');
						$isFailed = true;
						$failedCredits += $jobs->credits_consumed;
						$failedCount += 1;
						$failed_jobs_count++;
						continue;
					}

					if (!$gateway) {
						// Create fail job
						$failed = $this->sendToJobsFailed($jobs, true, 'No gateway assigned to this sub-account');
						$isFailed = true;
						$failedCredits += $jobs->credits_consumed;
						$failedCount += 1;
						$failed_jobs_count++;
						continue;
					}

					if ( $jobs->when_to_send->lte($today) ):
						try {
							$errorResult = false;
							$creditToConsumed = $jobs->credits_consumed;
							$number = formattedNumber($jobs->customer_number, true);

							$smartSunDitoClients = [70]; // beechoo
							// if ($subAccount->isBeechoo()) {
							if (in_array($jobs->client_id, $smartSunDitoClients)) {
								$invalidPrefixes = invalidNumberPrefixes();
								$sanitized = str_replace('+', '', $number);
								// $jobNumPrefix = substr($jobs->customer_number, 0, 5);
								$jobNumPrefix = substr($sanitized, 0, 5);

								logger('BEECHOO SMART - JOB NUMBER PREFIX');
								logger($jobNumPrefix);
								logger('BEECHOO SMART - IS NUMBER INVALID');
								logger(in_array($jobNumPrefix, $invalidPrefixes));

								if (in_array($jobNumPrefix, $invalidPrefixes)) {
									$origCredits = $jobs->credits_consumed;
									$creditToConsumed = $origCredits * 9; // gawan ng config
									$jobs->credits_consumed = $creditToConsumed;
									$jobs->save();

									logger('BEECHOO SMART - ORIGINAL CREDIT');
									logger($origCredits);

									logger('BEECHOO SMART - CREDITS CONSUMED');
									logger($creditToConsumed);

									$jobs->refresh();
									logger('BEECHOO SMART - APPLIED CREDITS CONSUMED');
									logger($jobs->credits_consumed);
								// 	// Create fail job
								// 	$failed = $this->sendToJobsFailed($jobs, true, 'Smart/Sun/DITO Number', explode(",", $jobs->id));
								// 	$isFailed = true;
								// 	$failedCredits += $jobs->credits_consumed;
								// 	$failedCount += 1;
								// 	$failed_jobs_count++;
								// 	continue;
								}
							}

							// Different validators before proceed to sending
							if ( !$number ): // validating the mobile number ...
								Log::error('The Mobile of: '.$jobs->customer_name.' wrong format: '.$jobs->customer_number);
								$errorResult = true;
								$errorKey = 'fail_mobile_format';
								$isFailed = true;
								$failedJobCreated = true;

								$failedCredits += $creditToConsumed;
								$failedCount += 1;
								$failed_jobs_count++;
							endif;

							// Check if pending balance of sub-account is sufficient for sending.
							$flag = $this->checkActualBalance( $creditToConsumed, $subAccount ); // deduction credit per subaccount ...

							if ( !$flag['flag'] ):
								Log::error('Insuficient Credit for Sub Account ...'. $subAccount->account_name .' Balance:'. isset($flag['credit']) ? $flag['credit'] : 'no credits');
								$errorResult = true;
								$errorKey = 'fail_insuficient_balance';
								$isFailed = true;
								$failedJobCreated = true;

								$failedCredits += $creditToConsumed;
								$failedCount += 1;
								$failed_jobs_count++;
							endif;

							$smsSuccess = false;
							$smsResponse = '';
							$options = [];

							if (!$errorResult) {
								if (!$disabledSending) {
									try {
										if ($gateway->isPromotexter()) {
											$smsSuccess = $this->promoTexter->makeSingleTransaction( $number, $jobs->message, false, $senderId, $options);
										} else if ($gateway->isGlobelabs()) {
											$smsSuccess = $this->globelabs->prepareSms( $number, $jobs->message ); // sending msg ...
										} else if ($gateway->isSytian()) {
											$smsSuccess = $this->sytianSms->prepareSms( $number, $jobs->message ); // sending msg ...
										}
									} catch (GatewayException $e) {
										$smsResponse = $e->getMessage();
										Log::error($e->getMessage());
									} catch (ServerException $e) {
										$smsResponse = $e->getMessage();
										Log::error($e->getMessage());
									}
								} else {
									$smsSuccess = true;
								}
							}

							// Log::info('Check the SMS Output: '.$smsSuccess);
							if ( $smsSuccess ) { // if success;
								$task = $jobs->task;
								$successCredits += $creditToConsumed;
								$successCount += 1;
								$success_jobs_count++;
								$this->sendToJobsSuccess($jobs, [$jobs->id]); // send jobs table completed ...
							} else {
								$isFailed = true;
								if (!$failedJobCreated) {
									$failedCredits += $creditToConsumed;
									$failedCount += 1;
								}
							}

							if ($isFailed) {
								$this->sendToJobsFailed($jobs, true, $errorKey ?: $smsResponse, explode(",", $jobs->id));
							}
						} catch (GatewayException $e) {
							return $e->getMessage();
							Log::error($e->getMessage());
						} catch (ServerException $e) {
							return $e->getMessage();

							Log::error($e->getMessage());
						}
					endif;
				endforeach;

				if ($failedCredits > 0) {
					$this->addPendingCreditPerSubaccount($failedCredits, $subAccount);
				}

				if ($failedCount > 0) {
					$this->taskRepository->updateFailedCount($task->id, $failedCount);
				}

				if ($successCredits > 0) {
					$task->modifyCreditsConsumend($successCredits, 'add');
					$this->minusCreditPerSubaccount($successCredits, $subAccount);
				}

				if ($successCount > 0) {
					if ($task->id == 1836) {
						$task->success_jobs_count = (int)$task->success_jobs_count + 1;
					} else {
						// $task->success_jobs_count = (int)$task->success_jobs_count + (int)$successCount;
						$task->success_jobs_count += (int)$successCount;
					}
					$task->save();
					// $this->increaseSuccessJobsCount($task->id, $successCount);
				}
			}

		else:
			Log::info('No Jobs for Today ...');
		endif;
	}

	public function createImmediateJob($request, $subAccount, $task = null, $senderId = null)
	{
	    if (!$senderId) {
            $senderId = $subAccount->senderid ? $subAccount->senderid->name : SenderIds::DEFAULT_SENDERID;
        }
		$settings = SiteSettings::getSettings();

		// Beta sending will do batchSMS of promotexter
		$isBeta = $subAccount->getSetting(SubAccountSetting::IS_BETA, false);
		if ($isBeta) {
			$smsFacadeBatch = resolve(SmsFacadeBatch::class);
			return $smsFacadeBatch->createImmediateJobs($request, $subAccount, $task);
		}

		$disabledSending = $settings ? $settings->sending_disabled : false;

		$today = Carbon::now();

		$name = $request->name;
		$number = formattedNumber($request->number, true);

		if (!$number) {
			$this->errors = $this->messageBag->add('gateway_error', 'Mobile number is invalid.');
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		$message = $request->message;
		$messageLength = strlen($message);
		$cost = $subAccount->getCreditAmount($messageLength);

		$smartSunDitoClients = [70]; // beechoo
		// if (in_array($subAccount->isBeechoo()) {
		if (in_array($subAccount->client_id, $smartSunDitoClients)) {
			$invalidPrefixes = invalidNumberPrefixes();
			$sanitized = str_replace('+', '', $number);
			// $jobNumPrefix = substr($jobs->customer_number, 0, 5);
			$jobNumPrefix = substr($sanitized, 0, 5);

			logger('BEECHOO SMART - JOB NUMBER PREFIX');
			logger($jobNumPrefix);
			logger('BEECHOO SMART - IS NUMBER INVALID');
			logger(in_array($jobNumPrefix, $invalidPrefixes));

			if (in_array($jobNumPrefix, $invalidPrefixes)) {
				$origCredits = $cost;
				$cost = $origCredits * 9; // gawan ng config

				logger('BEECHOO SMART - ORIGINAL CREDIT');
				logger($origCredits);

				logger('BEECHOO SMART - CREDITS CONSUMED');
				logger($cost);
			// 	// Create fail job
			// 	$failed = $this->sendToJobsFailed($jobs, true, 'Smart/Sun/DITO Number', explode(",", $jobs->id));
			// 	$isFailed = true;
			// 	$failedCredits += $jobs->credits_consumed;
			// 	$failedCount += 1;
			// 	$failed_jobs_count++;
			// 	continue;
			}
		}

		$flag = $this->checkActualBalance( $cost, $subAccount ); // deduction credit per subaccount ...

		$gateway = $subAccount->getApiKey();

		if (!$gateway) {
			$this->errors = $this->messageBag->add('gateway_error', 'No gateway assigned to process this job.');
			throw new \ATM\Validation\ValidationException($this->errors);
		}

		$smsSuccess = false;
		$smsResponse = '';

		$alreadyFailed = false;
		$request['task_id'] = $task->id;
		$failed_jobs_count = $this->taskRepository->search($request)->toArray();
		$jbFailed = ( $failed_jobs_count && $failed_jobs_count[0]['jobs_failed'] != NULL ) ? $failed_jobs_count[0]['jobs_failed'] + 1 : 1;

		$options = [];
		// 'categoryId' => "{$subAccount->account_name} - {$task->campaign->name}"

		if (!$disabledSending && !$alreadyFailed) {
			try {
				if ($gateway->isPromotexter()) {
					$smsSuccess = true;
					$smsResponse = $this->promoTexter->makeSingleTransaction($number, $message, false, $senderId, $options);
				} else if ($gateway->isGlobelabs()) {
					$smsSuccess = true;
					$smsResponse = $this->globelabs->prepareSms( $number, $message ); // sending msg ...
				} else if ($gateway->isSytian()) {
					$smsSuccess = true;
					$smsResponse = $this->sytianSms->prepareSms( $number, $message ); // sending msg ...
				}
			} catch (GatewayException $e) {
				$smsSuccess = false;
				$smsResponse = $e->getMessage();
			} catch (ServerException $e) {
				$smsSuccess = false;
				$smsResponse = $e->getMessage();
			}
		} elseif (!$alreadyFailed) {
			$smsSuccess = true;
			$smsResponse = 'SENDING DISABLED';
		}

		if ($request->filled('failed_only')) {
			$smsResponse = 'FORCE FAILED SMS';
		}

		if ($smsSuccess && !$alreadyFailed) {
			$job = new JobsCompleted();
			$job->when_to_send = $today->toDateTimeString();
			$job->sms_response = json_encode($smsResponse); // palitan to na dapat base sa response from third party

			if( $request->filled('from_job_failed') && $request['from_job_failed'] ) {
				$this->jobsFailedRepo->delete($request['job_failed_id']);
				$this->taskRepository->updateFailedCount($task->id, NULL, TRUE);
			}
		} else {
			$job = isset( $request->job_failed_id ) ? JobsFailed::find( $request->job_failed_id ) : new JobsFailed();
			$job->time_attempt_send = $today ? $today->toDateTimeString() : Carbon::now()->toDateTimeString();
			$job->sms_response = $smsResponse; // palitan to na dapat base sa response from third party Success to move in table completed
			$job->error_log = $request->error_log; // palitan to na dapat base sa response from third party
		}

		if ($request->filled('existing_contact_id') && $request->existing_contact_id) {
			$job->customer_id = $request->existing_contact_id;
		}

		$job->customer_name = $name ?: '';
		$job->customer_number = $number;
		$job->gateway_key = $gateway ? $gateway->id : null;
		$job->message = $message;
		$job->credits_consumed = $cost;
		$job->campaign_id = $task->campaign_id;
		$job->task_id = $task->id;
		$job->subaccount_id = $task->subaccount_id;
		$job->client_id = $task->client_id;
		$job->source = $request->ip();

		if ($request->filled('zs_booking_id')) {
			$job->zs_booking_id = $request->zs_booking_id;
		}

		$job->save();

		if( !$request->filled('from_job_failed') ) {
			$this->calculateTotalCountAndProjected($job, $task);
		}

		if ($job instanceof JobsCompleted) {
			$job->task->modifyCreditsConsumend($cost, 'add');
			/*$this->minusCreditPerSubaccount($job->credits_consumed, $subAccount);
			$this->minusPendingCreditPerSubaccount($job->credits_consumed, $subAccount);*/
			$this->lessPendingAndActualBalance( $job->credits_consumed, $subAccount );
			if ($subAccount->id == 1) {
				$time = now();
				/*Log::info("{$time} - credit deduction : [cost: {$job->credits_consumed} | actual balance: {$subAccount->credit->actual_balance} | pending balance: {$subAccount->credit->pending_balance}]");*/
			}
			if( $request->filled('from_job_failed') ) {
				$mainTask = $this->taskRepository->search($request)->first();
				$succJobCount = $mainTask->success_jobs_count;
				$succJobCount++;

				// $this->increaseSuccessJobsCount($task->id, $succJobCount);
				if ($task->id == 1836) {
					$task->success_jobs_count = (int)$task->success_jobs_count + 1;
				} else {
					// $task->success_jobs_count = (int)$task->success_jobs_count + (int)$succJobCount;
					$task->success_jobs_count =+ (int)$succJobCount;
				}
				$task->save();
			} else {
				if( $request->filled('from_text_blast') && $request['from_text_blast'] == TRUE ) {
					$mainTask = $this->taskRepository->search($request)->first();
					$succJobCount = $mainTask->success_jobs_count;
				} else {
					$succJobCount = $task->success_jobs_count;
				}

				$succJobCount++;
				$this->updateSuccessJobsCount($task->id, $succJobCount);
			}
		} else {
			if( $request->filled('from_job_failed') ) {
				$count = $task->jobs_failed;
			} else {
				$count = $task->jobs_failed + 1;
			}
			$this->taskRepository->updateFailedCount($task->id, $count);
		}

		return ['job' => $job, 'success' => $smsSuccess, 'message' => $smsResponse];
	}

	/* displaying sub account */
	private function getSubAccount($getSubAccountId){
		$subaccount = SubAccount::find( $getSubAccountId );
		return $subaccount;
	}

	/* validating mobile number */
	private function validate_mobile($mobile)
	{
		if ( preg_match('/^(?:09|\+?639)(?!.*-.*-)(?:\d(?:-)?){9}$/m', $mobile) ) : $mobileNumber = true; else: $mobileNumber = false; endif;
		return $mobileNumber;
	}

	/* validate if sub account id is correct */
	private function validate_subAccount( $subAccountID )
	{
		$getData = SubAccount::where('id', $subAccountID)->get();
		if ( count( $getData ) > 0 ): $result = true; else: $result = false; endif;
		return $result;
	}

	/* credits computation */
	private function creditComputationCharacters( $getStrings )
	{
		$countStrings = strlen( $getStrings );
		if ( $countStrings >= Task::CHARACTER_LIMITER ) { // credit string computation
			$totalCountOfStrings = number_format(round($countStrings / Task::CHARACTER_LIMITER + Task::CREDIT_LIMITER, 0));
		} else {
			$totalCountOfStrings = 1;
		}
		return isset($totalCountOfStrings) ? $totalCountOfStrings : 0;
	}

	private function checkActualBalance( $jobCredit, $subAccount )
	{
		$subCredit = $subAccount->credit;
		if ($subCredit) {
			if ($subCredit->actual_balance > $jobCredit) {
				return ['flag' => true, 'credit' => $subCredit->actual_balance];
			}
		}

		return ['flag' => false, 'credit' => 'no credit created!'];
	}

	/* decrement credit sa subaccount actual_balance */
	private function minusCreditPerSubaccount( $creditConsumed, $subAccount )
	{
		Credit::where('subaccount_id', '=', $subAccount->id ) // deduction eloquent query
		->decrement('actual_balance', $creditConsumed);
		return true;
	}

	public function addPendingCreditPerSubaccountAccessor( $creditConsumed, $subAccount )
	{
		if( $subAccount ) {
			$this->addPendingCreditPerSubaccount(  $creditConsumed, $subAccount );
		}
	}

	/* increment credit sa subaccount actual_balance */
	private function addCreditPerSubaccount( $creditConsumed, $subAccount )
	{
		Credit::where('subaccount_id', '=', $subAccount->id )
		->increment('actual_balance', $creditConsumed);
		return true;
	}

	/* increment credit sa subaccount pending_balance */
	private function addPendingCreditPerSubaccount( $creditConsumed, $subAccount )
	{
		Credit::where('subaccount_id', '=', $subAccount->id )
		->increment('pending_balance', $creditConsumed);
		return true;
	}

	/* decrement credit sa subaccount pending_balance */
	public function minusPendingCreditPerSubaccount( $creditConsumed, $subAccount )
	{
		 Credit::where('subaccount_id', '=', $subAccount->id )
		 ->decrement('pending_balance', $creditConsumed);
		/* $credit = ( isset($subAccount->id) ) ? Credit::Find($subAccount->id) : '';
		$computedCredit = (int) $credit->pending_balance - (int) $creditConsumed;
		$credit->pending_balance = $comput
		edCredit;
		$credit->save(); */
		return true;
	}

	public function lessPendingAndActualBalance($creditsConsumed, $subAccount)
	{
		Credit::where('subaccount_id', '=', $subAccount->id )->decrement('actual_balance', $creditsConsumed);
		Credit::where('subaccount_id', '=', $subAccount->id )->decrement('pending_balance', $creditsConsumed);
		return true;
	}

	/* auto increment if sending is success */
	private function updateSuccessJobsCount( $taskId, $successJobsCount )
	{
		$task = ( isset($taskId) ) ? Task::Find($taskId) : new Task();
		$task->success_jobs_count = (int)$successJobsCount;
		$task->save();

		return $task;
		// return Task::where('id', $taskId)->update(['success_jobs_count' => $successJobsCount]);
	}

	/* auto increment if sending is success */
	private function increaseSuccessJobsCount( $taskId, $amount )
	{
		// return Task::where('id', $taskId)->increment('success_jobs_count', $amount);
		$task = ( isset($taskId) ) ? Task::find($taskId) : new Task();
		$task->success_jobs_count = (int)$amount;
		$task->save();

		return $task;
	}

	/* function for sending to complete table */
	private function sendToJobsSuccess( $jobs, $ids = [] )
	{
		$buildArrays = $this->buildArrays( $jobs, $errorMessage = NULL , 'table_success' );
		$InsertCompletedTable = $this->InsertIntoCompleted( $buildArrays, $ids );
		return $InsertCompletedTable; // insert to today table
	}

	/* function for sending to failed table */
	private function sendToJobsFailed($jobs, $errorResult, $errorType, $getAllIdshasError = [])
	{
		if ( $errorResult ):
			if ( $errorType == 'fail_mobile_format' ){
				$errorMessage = 'The Mobile of: '.$jobs->customer_name.' invalid or wrong format: '.$jobs->customer_number;
			} elseif ( $errorType == 'fail_sub_account_id' ){
				$errorMessage = 'Invalid Sub account id';
			} elseif ( $errorType == 'fail_insuficient_balance' ){
				$errorMessage = 'Insuficient Credit. '. $this->getSubAccount($jobs->subaccount_id)->account_name;
			} else {
				$errorMessage = $errorType;
			}
			$buildArrays = $this->buildArrays( $jobs, $errorMessage, 'table_failed' );
			$InsertToFailedTable = $this->InsertIntoFailed( $buildArrays, $getAllIdshasError ); // insert to failed table
			return $InsertToFailedTable; // insert to failed table
		endif;
	}

	/* function for sending to failed table */
	public function forceFailJobs($jobs, $errorResult, $errorType, $getAllIdshasError = [])
	{
		if ( $errorResult ):
			if ( $errorType == 'fail_mobile_format' ){
				$errorMessage = 'The Mobile of: '.$jobs->customer_name.' invalid or wrong format: '.$jobs->customer_number;
			} elseif ( $errorType == 'fail_sub_account_id' ){
				$errorMessage = 'Invalid Sub account id';
			} elseif ( $errorType == 'fail_insuficient_balance' ){
				$errorMessage = 'Insuficient Credit. '. $this->getSubAccount($jobs->subaccount_id)->account_name;
			} else {
				$errorMessage = $errorType;
			}
			$buildArrays = $this->buildArrays( $jobs, $errorMessage, 'table_failed' );
			JobsFailed::insert( $buildArrays );

			$jobs->forceDelete();
			
			return true;
		endif;
	}

	/* build arrays */
	private function buildArrays( $allArrays, $errorMessage = NULL, $type = NULL )
	{
		if ( $type == 'table_failed' ) {
			$drawArrays = [
				'customer_id' => $allArrays->customer_id,
				'customer_name' => $allArrays->customer_name,
				'customer_number' => $allArrays->customer_number,
				'gateway_key' => $allArrays->gateway_key,
				'campaign_id' => $allArrays->campaign_id,
				'credits_consumed' => $allArrays->credits_consumed,
				'time_attempt_send' => $allArrays->when_to_send,
				'message' => $allArrays->message,
				'task_id' => $allArrays->task_id,
				'subaccount_id' => $allArrays->subaccount_id,
				'client_id' => $allArrays->client_id,
				'entry_id' => $allArrays->entry_id,
				'source' => $allArrays->source,
				'error_log' => isset($errorMessage)? $errorMessage : NULL,
				'created_at' => carbon::now(),
				'updated_at' => carbon::now(),
			];
		} else {
			$drawArrays = [
				'customer_id' => $allArrays->customer_id,
				'customer_name' => $allArrays->customer_name,
				'customer_number' => $allArrays->customer_number,
				'gateway_key' => $allArrays->gateway_key,
				'campaign_id' => $allArrays->campaign_id,
				'credits_consumed' => $allArrays->credits_consumed,
				'when_to_send' => $allArrays->when_to_send,
				'message' => $allArrays->message,
				'task_id' => $allArrays->task_id,
				'subaccount_id' => $allArrays->subaccount_id,
				'client_id' => $allArrays->client_id,
				'entry_id' => $allArrays->entry_id,
				'source' => $allArrays->source,
				'created_at' => carbon::now(),
				'updated_at' => carbon::now(),
			];
		}
		return $drawArrays;
	}

	/* insert to table completed */
	private function InsertIntoCompleted( $buildArrays, $getAllIdsCompleted = [] )
	{
		if ( !is_array( $buildArrays ) ) {
			Log::error("There was an error in saving data for jobs completed.");
		} else {
			JobsCompleted::insert( $buildArrays ); // insert in today table
			if (is_array($getAllIdsCompleted)) {
				$this->removeTodaysData($getAllIdsCompleted);
			} else { 
				Log::error("No ids found."); 
			}
			Log::info('Success to move in table completed ...');
		}
	}

	/* insert to table failed */
	private function InsertIntoFailed( $buildArrays, $getAllIdshasError  = [] )
	{
		if ( !is_array( $buildArrays ) ) {
			Log::error("There was an error in saving data for jobs failed table.");
		} else {
			JobsFailed::insert( $buildArrays ); // insert in today table
			if (is_array($getAllIdshasError)) {
				$this->removeTodaysData($getAllIdshasError); 
			} else {
				Log::error("No ids found.");
			}
			Log::info('Success to move in table failed ...');
		}
		return;
	}

	private function makeQueue($jobTodays)
	{
		$ids = $jobTodays->pluck('id')->toArray();
		return JobToday::whereIn('id', $ids)->update(['on_queue' => 1]);
	}

	/* function for removing data in the previous table */
	private function removeTodaysData($getAllIds)
	{
		Log::info('Removing data ...');
		JobToday::destroy($getAllIds);
		return; // remove data after inserting in job today table
	}

	/* function for updating data in the task table */
	private function calculateTotalCountAndProjected($job, $task)
	{
		$jobsCount = (int) $task->jobs_count;
		$projectedCreditsConsumed = (int) $task->projected_credits_consumed;

		$jobsCount++;
		$projectedCreditsConsumed = $projectedCreditsConsumed + $job->credits_consumed;

		$task->jobs_count = $jobsCount;
		$task->projected_credits_consumed = $projectedCreditsConsumed;
		$task->save();

		return;
	}

}
