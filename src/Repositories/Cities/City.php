<?php

namespace ATM\Repositories\Cities;

use ATM\Repositories\Province\Province;
use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class City extends Model
{
	use ValidatingTrait;

    public $table = "cities";

    public function getCityNameAttribute($value)
    {
       return ucfirst($this->name);
    }

	public function province()
	{
		return $this->belongsTo(Province::class);
	}
}


?>
