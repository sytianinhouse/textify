<?php

namespace ATM\Repositories\Roles;

use ATM\ContextInterface;
use ATM\Repositories\Client\Client;
use ATM\Repositories\ContextRepository;
use ATM\Repositories\Permission\Permission;
use ATM\Repositories\Roles\Role;
use ATM\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\MessageBag;

class RoleRepository extends ContextRepository {

	protected $model;

	public function __construct(
		Role $role,
		ContextInterface $context,
		Permission $permission
	)
	{
		$this->model = $role;
		$this->context = $context;
		$this->permission = $permission;
	}

	public function search(Request $request, $with = [])
	{
		$query = $this->scope($with);

		if ($request->filled('filter_search')) {
			$query = $query->where('name','LIKE',"%{$request->filter_search}%");
		}

		if ($request->filled('per_page')) {
			return $query->paginate($request->per_page);
		} else {
			return $query->get();
		}
	}

	public function add(Request $request, Client $client)
	{

		$role = $this->model;
		$role->name = snake_case($request->name);
		$role->client_id = $client->id;
		$role->display_name = $request->name;
		$role->description = $request->description;
		$role->save();
		$role->permissions()->sync($request->permissions);

		//$role->client()->associate($client);
		//$this->validateRole($role, $request, $client);
		// attach permissions
		//$role->permissions()->attach((array) $request->permissions);

		return $role;
	}

	public function update(Role $role, Request $request)
	{

		$role->name = snake_case($request->name);
		$role->display_name = $request->name;
		$role->description = $request->description;
		$role->permissions()->sync($request->permissions);
		$role->save();

		//$this->validateRole($role, $request, $role->client);
		// sync permissions
		//$role->permissions()->sync((array) $request->permissions);
		return $role;
	}

	public function delete(Role $role)
	{
		$users = $role->users;
		if ($users->count() > 0) {
			throw new ValidationException(new MessageBag([
				'Failed to Delete. There are users assigned to this role.'
			]));
			
		}
		// $result = $this->checkIfHasUsers($role);
		// if (count($this->errors) > 0) {
		// 	throw new ValidationException($this->errors);
		// }

		$role->users()->detach();
		$name = $role->display_name;
		$role->delete();

		// detach permissions
		//$role->permissions()->detach();
		return $name;
	}

	public function checkIfHasUsers(Role $role)
	{

		$request = request();
		$request['role_id'] = $role->id;

		$validator = \Validator::make($request->all(),
			[
				'role_id' => [
			        Rule::unique('roles_user')->where(function ($query) use ($request) {
			            $query->where('role_id', $request->role_id);
			        })
			    ]
			],
			[
				'role_id.unique' => 'Failed to Delete. There are users assigned to this role.'
			]
		);

		if ($validator->fails()) {
		    $this->errors = $validator->messages()->merge($this->errors);
		}
	}

	public function validateRole(Role $role, Request $request, Client $client)
	{
		$uniqueValidator = $this->validateUnique($role, $request, $client);
		if (!$this->validate($role) || $uniqueValidator->fails()) {
			throw new ValidationException($role->getErrors()->merge($uniqueValidator->errors()));
		}
		return true;
	}

	public function validateUnique(Role $role, Request $request, Client $client)
	{
		$nameRule = ['required'];
        if ($role->exists) {
        	$nameRule[] = Rule::unique('roles')
		        			->ignore($role->id)
		        			->where(function ($query) use ($client) {
							    $query->where('user_id', $client->id);
							});
        } else {
        	$nameRule[] = Rule::unique('roles')
				        	->where(function ($query) use ($client) {
							    $query->where('user_id', $client->id);
							});
        }

		$validator = Validator::make($request->all(), [
	            'display_name' => $nameRule,
	        ],
	        [
	        	'display_name.unique' => "The name has already been taken.",
	        ]
        );

		return $validator;
	}

	public function ofClient(Client $client)
	{
		return $this->model
					->where('client_id', $client->id)
					->get();
	}

	/*
	 * Upon creation ng client, tawagin tong method para mag-create ng default Roles (Client, Sub Account Manager)
	 */
	public function createDefaults(Client $client)
	{
		// Uncomment kapag may mga permission na
		$initPermission = $this->permission->all();

		$defRole = $this->model;
		$defRole->name = snake_case('All Access');
		$defRole->display_name = 'All Access';
		$defRole->description = 'Default role which provides all access to the system.';
		$defRole->client_id = $client->id;
		$defRole->save();

		// attach permissions;
		$defRole->permissions()->attach($initPermission->pluck('id')->toArray());

		return $defRole;
	}
}
