<?php

namespace ATM\Repositories\Roles;

use ATM\ContextInterface;
use ATM\ContextScope;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Permission\Permission;
use ATM\Repositories\SubAccount\SubAccount;
use App\Model\BaseModel;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class Role extends BaseModel implements ContextScope {

	use ValidatingTrait;

	protected $rules = [
		'name' => 'required',
	];

	protected $fillable = [
							'name',
							'display_name',
							'description'
						];

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

    public function users()
    {
        return $this->belongsToMany(User::class, 'roles_user', 'role_id', 'user_id');
	}

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permissions', 'role_id', 'permission_id');
    }

	// Query Scopes
	public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->where($context->column(), $context->id());
        } else if ($context->getInstance() instanceof SubAccount) {
            return $query->whereHas('client.subAccounts', function($q) use ($context) {
                return $q->where('id', $context->id());
            });
        }

        return $query;
    }

}
