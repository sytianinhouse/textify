<?php
namespace ATM\StoredProcedure\Credits;

use ATM\ContextInterface;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\StoredProcedure\BaseProcedure;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CreditsProcedure extends BaseProcedure
{
	public function __construct(
        ContextInterface $context
    )
    {
        $this->context = $context;
	}

	public function searchCredits(Request $request)
	{
		$clientID =  $request->user() && !$request->user()->isSuperAdmin() ? $request->user()->client_id : $request->client_id;
		$subAccountId = null;
		if ($request->filled('subaccount_id')) {
			$subAccountId = $request->subaccount_id;
		}

		if ($request->filled('sub_account_id')) {
			$subAccountId = $request->sub_account_id;
        }

        $campaignId = null;
		if ($request->filled('campaign_filter')) {
			$campaignId = $request->campaign_filter;
        }

        $fromDate = null;
		if ($request->filled('from_date')) {
			$fromDate = new Carbon($request->from_date);
			$fromDate = $fromDate->startOfDay()->toDateTimeString();
        }

        $toDate = null;
		if ($request->filled('to_date')) {
			$toDate = new Carbon($request->to_date);
			$toDate = $toDate->endOfDay()->toDateTimeString();
		}

		if( $this->context->getInstance() instanceof SubAccount ) {
			$subAccountId = $this->context->getInstance()->id;
		}

		$rows = DB::select(
			'call credit_balance (?, ?, ?, ?, ?)',
			[
				$clientID,
				$campaignId,
				$subAccountId,
				$fromDate,
				$toDate,
			]
		);

		$credits = collect($rows);

		return $credits;
	}
}
