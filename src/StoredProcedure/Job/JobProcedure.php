<?php
namespace ATM\StoredProcedure\Job;

use ATM\StoredProcedure\BaseProcedure;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JobProcedure extends BaseProcedure
{
	public function searchJobsCompleted(Request $request)
	{
		$clientId =  $request->user() && !$request->user()->isSuperAdmin() ? $request->user()->client_id : $request->client_id;

		$subAccountId = null;
		if ($request->filled('subaccount_id')) {
			$subAccountId = $request->subaccount_id;
		}

		if ($request->filled('sub_account_id')) {
			$subAccountId = $request->sub_account_id;
        }

		$subAccountIds = null;
		if ($request->filled('sub_account_ids')) {
			$subAccountIds = $request->sub_account_ids;
		}

        $campaignId = null;
		if ($request->filled('campaign_filter')) {
            $campaignId = $request->campaign_filter;
        }

		$campaignIds = null;
		if ($request->filled('last_created_ids')) {
			$campaignIds = implode(',', $request->last_created_ids);
		}

        $taskTypeId = null;
		if ($request->filled('task_type_id')) {
            $taskTypeId = $request->task_type_id;
        }

        $fromDate = null;
		if ($request->filled('from_date')) {
			$fromDate = $request->from_date;
        }

        $toDate = null;
		if ($request->filled('to_date')) {
			$toDate = $request->to_date;
		}

		$rows = DB::select(
			'call jobs_completed_daterange (?, ?, ?, ?, ?, ?, ?, ?)',
			[
				$clientId,
				$subAccountId,
				$subAccountIds,
				$fromDate,
				$toDate,
				$taskTypeId,
				$campaignId,
				$campaignIds
			]
		);

		$jobs = collect($rows);

		return $jobs;
	}

	public function searchJobsFailed(Request $request)
	{
		$clientId =  $request->user() && !$request->user()->isSuperAdmin() ? $request->user()->client_id : $request->client_id;

		$subAccountId = null;
		if ($request->filled('subaccount_id')) {
			$subAccountId = $request->subaccount_id;
		}

		if ($request->filled('sub_account_id')) {
			$subAccountId = $request->sub_account_id;
        }

		$subAccountIds = null;
		if ($request->filled('sub_account_ids')) {
			$subAccountIds = $request->sub_account_ids;
		}
        
        $campaignId = null;
		if ($request->filled('campaign_filter')) {
            $campaignId = $request->campaign_filter;
        }

		$campaignIds = null;
		if ($request->filled('last_created_ids')) {
			$campaignIds = implode(',', $request->last_created_ids);
		}

        $taskTypeId = null;
		if ($request->filled('task_type_id')) {
            $taskTypeId = $request->task_type_id;
        }

        $fromDate = null;
		if ($request->filled('from_date')) {
			$fromDate = $request->from_date;
        }

        $toDate = null;
		if ($request->filled('to_date')) {
			$toDate = $request->to_date;
		}

		$rows = DB::select(
			'call jobs_failed_daterange (?, ?, ?, ?, ?, ?, ?, ?)',
			[
				$clientId,
				$subAccountId,
				$subAccountIds,
				$fromDate,
				$toDate,
				$taskTypeId,
				$campaignId,
				$campaignIds
			]
		);

		$jobs = collect($rows);

		return $jobs;
	}

	public function searchJobsToday(Request $request)
	{
		$clientId =  $request->user() && !$request->user()->isSuperAdmin() ? $request->user()->client_id : $request->client_id;

		$subAccountId = null;
		if ($request->filled('subaccount_id')) {
			$subAccountId = $request->subaccount_id;
		}

		if ($request->filled('sub_account_id')) {
			$subAccountId = $request->sub_account_id;
		}

		$subAccountIds = null;
		if ($request->filled('sub_account_ids')) {
			$subAccountIds = $request->sub_account_ids;
		}

		$campaignId = null;
		if ($request->filled('campaign_filter')) {
			$campaignId = $request->campaign_filter;
		}

		$campaignIds = null;
		if ($request->filled('last_created_ids')) {
			$campaignIds = implode(',', $request->last_created_ids);
		}

		$taskTypeId = null;
		if ($request->filled('task_type_id')) {
			$taskTypeId = $request->task_type_id;
		}

		$fromDate = null;
		if ($request->filled('from_date')) {
			$fromDate = $request->from_date;
		}

		$toDate = null;
		if ($request->filled('to_date')) {
			$toDate = $request->to_date;
		}

		$noDeletedTask = null;
		if ($request->filled('not_deleted_task')) {
			$noDeletedTask = $request->not_deleted_task;
		}

		$rows = DB::select(
			'call jobs_today_daterange (?, ?, ?, ?, ?, ?, ?, ?, ?)',
			[
				$clientId,
				$subAccountId,
				$subAccountIds,
				$fromDate,
				$toDate,
				$taskTypeId,
				$campaignId,
				$campaignIds,
				$noDeletedTask
			]
		);

		$jobs = collect($rows);

		return $jobs;
	}

	public function searchJobsTomorrow(Request $request)
	{
		$clientId =  $request->user() && !$request->user()->isSuperAdmin() ? $request->user()->client_id : $request->client_id;

		$subAccountId = null;
		if ($request->filled('subaccount_id')) {
			$subAccountId = $request->subaccount_id;
		}

		if ($request->filled('sub_account_id')) {
			$subAccountId = $request->sub_account_id;
		}

		$subAccountIds = null;
		if ($request->filled('sub_account_ids')) {
			$subAccountIds = $request->sub_account_ids;
		}

		$campaignId = null;
		if ($request->filled('campaign_filter')) {
			$campaignId = $request->campaign_filter;
		}

		$campaignIds = null;
		if ($request->filled('last_created_ids')) {
			$campaignIds = implode(',', $request->last_created_ids);
		}

		$taskTypeId = null;
		if ($request->filled('task_type_id')) {
			$taskTypeId = $request->task_type_id;
		}

		$fromDate = null;
		if ($request->filled('from_date')) {
			$fromDate = $request->from_date;
		}

		$toDate = null;
		if ($request->filled('to_date')) {
			$toDate = $request->to_date;
		}

		$rows = DB::select(
			'call jobs_tomorrow_daterange (?, ?, ?, ?, ?, ?, ?, ?)',
			[
				$clientId,
				$subAccountId,
				$subAccountIds,
				$fromDate,
				$toDate,
				$taskTypeId,
				$campaignId,
				$campaignIds
			]
		);

		$jobs = collect($rows);

		return $jobs;
	}

	public function searchJobsAfter14thToTomorrow(Request $request)
	{
		$clientId =  $request->user() && !$request->user()->isSuperAdmin() ? $request->user()->client_id : $request->client_id;

		$subAccountId = null;
		if ($request->filled('subaccount_id')) {
			$subAccountId = $request->subaccount_id;
		}

		if ($request->filled('sub_account_id')) {
			$subAccountId = $request->sub_account_id;
		}

		$subAccountIds = null;
		if ($request->filled('sub_account_ids')) {
			$subAccountIds = $request->sub_account_ids;
		}

		$campaignId = null;
		if ($request->filled('campaign_filter')) {
			$campaignId = $request->campaign_filter;
		}

		$campaignIds = null;
		if ($request->filled('last_created_ids')) {
			$campaignIds = implode(',', $request->last_created_ids);
		}

		$taskTypeId = null;
		if ($request->filled('task_type_id')) {
			$taskTypeId = $request->task_type_id;
		}

		$fromDate = null;
		if ($request->filled('from_date')) {
			$fromDate = $request->from_date;
		}

		$toDate = null;
		if ($request->filled('to_date')) {
			$toDate = $request->to_date;
		}

		$rows = DB::select(
			'call jobs_after_tomorrow_to_14th_day_daterange (?, ?, ?, ?, ?, ?, ?, ?)',
			[
				$clientId,
				$subAccountId,
				$subAccountIds,
				$fromDate,
				$toDate,
				$taskTypeId,
				$campaignId,
				$campaignIds
			]
		);

		$jobs = collect($rows);

		return $jobs;
	}

	public function searchJobs15thToEndMonth(Request $request)
	{
		$clientId =  $request->user() && !$request->user()->isSuperAdmin() ? $request->user()->client_id : $request->client_id;

		$subAccountId = null;
		if ($request->filled('subaccount_id')) {
			$subAccountId = $request->subaccount_id;
		}

		if ($request->filled('sub_account_id')) {
			$subAccountId = $request->sub_account_id;
		}

		$subAccountIds = null;
		if ($request->filled('sub_account_ids')) {
			$subAccountIds = $request->sub_account_ids;
		}

		$campaignId = null;
		if ($request->filled('campaign_filter')) {
			$campaignId = $request->campaign_filter;
		}

		$campaignIds = null;
		if ($request->filled('last_created_ids')) {
			$campaignIds = implode(',', $request->last_created_ids);
		}

		$taskTypeId = null;
		if ($request->filled('task_type_id')) {
			$taskTypeId = $request->task_type_id;
		}

		$fromDate = null;
		if ($request->filled('from_date')) {
			$fromDate = $request->from_date;
		}

		$toDate = null;
		if ($request->filled('to_date')) {
			$toDate = $request->to_date;
		}

		$rows = DB::select(
			'call jobs_15th_day_to_end_month_daterange (?, ?, ?, ?, ?, ?, ?, ?)',
			[
				$clientId,
				$subAccountId,
				$subAccountIds,
				$fromDate,
				$toDate,
				$taskTypeId,
				$campaignId,
				$campaignIds
			]
		);

		$jobs = collect($rows);

		return $jobs;
	}

	public function searchJobsNextMonth(Request $request)
	{
		$clientId =  $request->user() && !$request->user()->isSuperAdmin() ? $request->user()->client_id : $request->client_id;

		$subAccountId = null;
		if ($request->filled('subaccount_id')) {
			$subAccountId = $request->subaccount_id;
		}

		if ($request->filled('sub_account_id')) {
			$subAccountId = $request->sub_account_id;
		}

		$subAccountIds = null;
		if ($request->filled('sub_account_ids')) {
			$subAccountIds = $request->sub_account_ids;
		}

		$campaignId = null;
		if ($request->filled('campaign_filter')) {
			$campaignId = $request->campaign_filter;
		}

		$campaignIds = null;
		if ($request->filled('last_created_ids')) {
			$campaignIds = implode(',', $request->last_created_ids);
		}

		$taskTypeId = null;
		if ($request->filled('task_type_id')) {
			$taskTypeId = $request->task_type_id;
		}

		$fromDate = null;
		if ($request->filled('from_date')) {
			$fromDate = $request->from_date;
		}

		$toDate = null;
		if ($request->filled('to_date')) {
			$toDate = $request->to_date;
		}

		$rows = DB::select(
			'call jobs_next_month_daterange (?, ?, ?, ?, ?, ?, ?, ?)',
			[
				$clientId,
				$subAccountId,
				$subAccountIds,
				$fromDate,
				$toDate,
				$taskTypeId,
				$campaignId,
				$campaignIds
			]
		);

		$jobs = collect($rows);

		return $jobs;
	}

	public function getAllCreditsConsumedFromCompleted(Request $request)
	{
		$clientId =  $request->user() && !$request->user()->isSuperAdmin() ? $request->user()->client_id : $request->client_id;

		$subAccountId = null;
		if ($request->filled('subaccount_id')) {
			$subAccountId = $request->subaccount_id;
		}

		if ($request->filled('sub_account_id')) {
			$subAccountId = $request->sub_account_id;
		}

		$subAccountIds = null;
		if ($request->filled('subaccount_ids')) {
			$subAccountIds = $request->subaccount_ids;
		}

		$campaignId = null;
		if ($request->filled('campaign_filter')) {
			$campaignId = $request->campaign_filter;
		}

		$taskTypeId = null;
		if ($request->filled('task_type_id')) {
			$taskTypeId = $request->task_type_id;
		}

		$fromDate = null;
		if ($request->filled('from_date')) {
			$fromDate = (new Carbon($request->from_date))->startOfDay();
		}

		$toDate = null;
		if ($request->filled('to_date')) {
			$toDate = (new Carbon($request->to_date))->endOfDay();
		}

		$rows = DB::select(
			'call jobs_completed_total_credits (?, ?, ?, ?, ?, ?, ?)',
			[
				$clientId,
				$subAccountId,
				$subAccountIds,
				$fromDate,
				$toDate,
				$taskTypeId,
				$campaignId
			]
		);

		$jobs = collect($rows);

		return $jobs;
	}

	public function searchJobsPerSubAccount(Request $request)
	{
		$clientId =  $request->user() && !$request->user()->isSuperAdmin() ? $request->user()->client_id : $request->client_id;

		$subAccountId = null;
		if ($request->filled('subaccount_id')) {
			$subAccountId = $request->subaccount_id;
		}

		if ($request->filled('sub_account_id')) {
			$subAccountId = $request->sub_account_id;
		}

		$subAccountIds = null;
		if ($request->filled('sub_account_ids')) {
			$subAccountIds = $request->sub_account_ids;
		}

		$campaignId = null;
		if ($request->filled('campaign_filter')) {
			$campaignId = $request->campaign_filter;
		}

		$campaignIds = null;
		if ($request->filled('last_created_ids')) {
			$campaignIds = implode(',', $request->last_created_ids);
		}

		$taskTypeId = null;
		if ($request->filled('task_type_id')) {
			$taskTypeId = $request->task_type_id;
		}

		$fromDate = null;
		if ($request->filled('from_date')) {
			$fromDate = $request->from_date;
		}

		$toDate = null;
		if ($request->filled('to_date')) {
			$toDate = $request->to_date;
		}

		$rows = DB::select(
			'call jobs_per_subaccount_daterange (?, ?, ?, ?, ?, ?, ?, ?)',
			[
				$clientId,
				$subAccountId,
				$subAccountIds,
				$fromDate,
				$toDate,
				$taskTypeId,
				$campaignId,
				$campaignIds
			]
		);

		$jobs = collect($rows);

		return $jobs;
	}
}
