<?php
namespace ATM\StoredProcedure\Task;

use ATM\ContextInterface;
use ATM\Repositories\Campaign\CampaignRepository;
use ATM\Repositories\SubAccount\SubAccount;
use ATM\StoredProcedure\BaseProcedure;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CampaignProcedure extends BaseProcedure
{
	public function __construct(
		ContextInterface $context,
		CampaignRepository $campaignRepo
    )
    {
		$this->context = $context;
		$this->campaignRepo = $campaignRepo;
	}

	public function searchTaskPerCampaign(Request $request)
	{
		$clientID =  $request->user() && !$request->user()->isSuperAdmin() ? $request->user()->client_id : $request->client_id;

		// $contextCampaigns = $this->campaignRepo->search($request)->pluck('id','name');

		$subAccountId = null;
		if ($request->filled('subaccount_id')) {
			$subAccountId = $request->subaccount_id;
		}

		if ($request->filled('sub_account_id')) {
			$subAccountId = $request->sub_account_id;
        }

		$subAccountIds = null;
		if ($request->filled('sub_account_ids')) {
			$subAccountIds = $request->sub_account_ids;
		}

		$campaignIds = null;
		if ($request->filled('last_created_ids')) {
            $campaignIds = implode(',', $request->last_created_ids);
		}

        $campaignId = null;
		if ($request->filled('campaign_filter')) {
            $campaignId = $request->campaign_filter;
        }

        $fromDate = null;
		if ($request->filled('from_date')) {
			$fromDate = new Carbon($request->from_date);
			$fromDate = $fromDate->startOfDay()->toDateTimeString();
        }

        $toDate = null;
		if ($request->filled('to_date')) {
			$toDate = new Carbon($request->to_date);
			$toDate = $toDate->endOfDay()->toDateTimeString();
		}

		if( $this->context->getInstance() instanceof SubAccount ) {
			$subAccountId = $this->context->getInstance()->id;
		}

		/*if($campaignId == null) {
			$campaignIds = $campaignIds;
		} else {
			$campaignIds = null;
		}*/

		$rows = DB::select(
			'call daterange_jobs_per_campaign (?, ?, ?, ?, ?, ?, ?)',
			[
				$clientID,
				$subAccountId,
				$subAccountIds,
				$campaignId,
				$campaignIds,
				$fromDate,
				$toDate,
			]
		);
		$tasks = collect($rows);

		return $tasks;
	}
}
