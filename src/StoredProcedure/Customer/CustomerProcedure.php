<?php
namespace ATM\StoredProcedure\Customer;

use ATM\StoredProcedure\BaseProcedure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerProcedure extends BaseProcedure
{
	public function searchCustomers(Request $request)
	{
        // dd($request->not_empty_mobile);
		$clientId =  $request->user() && !$request->user()->isSuperAdmin() ? $request->user()->client_id : $request->client_id;
		$subAccountId = null;
		if ($request->filled('subaccount_id')) {
			$subAccountId = $request->subaccount_id;
		}

		if ($request->filled('sub_account_id')) {
			$subAccountId = $request->sub_account_id;
        }

		$bDay = null;
		if ($request->filled('b_day')) {
			$bDay = $request->b_day;
		}

		$notEmptyMobile = null;
		if ($request->filled('not_empty_mobile')) {
			$notEmptyMobile = $request->not_empty_mobile;
		}

		$bMonth = null;
		if ($request->filled('b_month')) {
			$bMonth = $request->b_month;
		}

		$bYear = null;
		if ($request->filled('b_year')) {
			$bYear = $request->b_year;
		}

		$gender = null;
		if ($request->filled('gender')) {
			$gender = $request->gender;
		}

		$city = null;
		if ($request->filled('city')) {
			$city = $request->city;
		}

		$group = null;
		if ($request->filled('customer_group_id')) {
			$group = $request->customer_group_id;
        }

		$rows = DB::select(
			'call search_customers (?, ?, ?, ?, ?, ?, ?, ?, ?)',
			[
				$clientId,
				$subAccountId,
				$bDay,
				$bMonth,
				$bYear,
				$gender,
				$city,
				$group,
                $notEmptyMobile
			]
        );

		$customers = collect($rows);

		return $customers;
	}

	public function searchCustomersRandomized(Request $request)
	{
        // dd($request->not_empty_mobile);
		$clientId =  $request->user() && !$request->user()->isSuperAdmin() ? $request->user()->client_id : $request->client_id;
		$subAccountId = null;
		if ($request->filled('subaccount_id')) {
			$subAccountId = $request->subaccount_id;
		}

		if ($request->filled('sub_account_id')) {
			$subAccountId = $request->sub_account_id;
        }

		$bDay = null;
		if ($request->filled('b_day')) {
			$bDay = $request->b_day;
		}

		$notEmptyMobile = null;
		if ($request->filled('not_empty_mobile')) {
			$notEmptyMobile = $request->not_empty_mobile;
		}

		$bMonth = null;
		if ($request->filled('b_month')) {
			$bMonth = $request->b_month;
		}

		$bYear = null;
		if ($request->filled('b_year')) {
			$bYear = $request->b_year;
		}

		$gender = null;
		if ($request->filled('gender')) {
			$gender = $request->gender;
		}

		$city = null;
		if ($request->filled('city')) {
			$city = $request->city;
		}

		$group = null;
		if ($request->filled('customer_group_id')) {
			$group = $request->customer_group_id;
        }

		$rows = DB::select(
			'call search_customers_randomized (?, ?, ?, ?, ?, ?, ?, ?, ?)',
			[
				$clientId,
				$subAccountId,
				$bDay,
				$bMonth,
				$bYear,
				$gender,
				$city,
				$group,
                $notEmptyMobile
			]
        );

		$customers = collect($rows);

		return $customers;
	}
}
