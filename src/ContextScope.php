<?php

namespace ATM;

use ATM\ContextInterface;

interface ContextScope {

	public function scopeOfContext($query, ContextInterface $context);

}