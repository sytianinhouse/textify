<?php
$jobs_per_campaign = "
	SELECT
		id,
		name,
		subaccount_id,
		SUM(recipients) as recipients,
		SUM(credits_used) as credits_used,
		SUM(sms_failed) as sms_failed
	FROM
	(
		SELECT
			`camp`.`id`,
			`camp`.`name`,
			`camp`.`subaccount_id`,
			SUM(`task`.`jobs_count`) as recipients,
			SUM(`task`.`actual_credits_consumed`) as credits_used,
			0 sms_failed
		FROM `campaigns` camp
		JOIN `sub_accounts` sub ON `camp`.`subaccount_id` = `sub`.`id`
		JOIN `clients` ON `sub`.`client_id` = `clients`.`id`
		LEFT JOIN `task`
		ON `task`.`campaign_id` = `camp`.`id`
		AND `clients`.`id` = `task`.`client_id`
		AND `sub`.`id` = `task`.`subaccount_id`
		WHERE `clients`.`id` = clientID
		AND `task`.`date_to_send` >= fromDate
		AND `task`.`date_to_send` <= toDate
		GROUP BY 1, 2
			UNION
			SELECT
				`camp`.`id`,
				`camp`.`name`,
				`camp`.`subaccount_id`,
				0 as recipients,
				0 as credits_used,
				COUNT(`failed`.`id`) as sms_failed
			FROM
				`campaigns` camp
			JOIN
				`sub_accounts` sub
				ON `camp`.`subaccount_id` = `sub`.`id`
			JOIN
				`clients`
				ON `sub`.`client_id` = `clients`.`id`
			LEFT JOIN
				`jobs_failed` failed
				ON `failed`.`campaign_id` = `camp`.`id`
					AND `clients`.`id` = `failed`.`client_id`
					AND `sub`.`id` = `failed`.`subaccount_id`
			WHERE `clients`.`id` = clientID
				AND `failed`.`time_attempt_send` >= fromDate
				AND `failed`.`time_attempt_send` <= toDate
			GROUP BY 1, 2
	) res
	WHERE (
		(
			campaignIds IS NOT NULL
			AND FIND_IN_SET(`id`, campaignIds)
		) OR (
			campaignId IS NOT NULL
			AND `id` = campaignId
		)
		OR (campaignId IS NULL AND campaignIds IS NULL)
		AND (
			(subAccountId IS NOT NULL AND
				subaccount_id = subAccountId
			)
			OR (subAccountId IS NULL)
		)
	)
	GROUP BY 1, 2
";


$daterange_jobs_per_campaign = "
	SELECT
		`cps`.`id`,
		`cps`.`name`,
		`cps`.`created_at`,
		`cps`.`updated_at`,
		(
			SELECT CONCAT_WS('--', COUNT(`jc`.`id`), SUM(`jc`.`credits_consumed`))
			FROM `jobs_completed` jc
			WHERE `jc`.`campaign_id` = `cps`.`id`
			AND DATE(`jc`.`when_to_send`) >= fromDate
			AND DATE(`jc`.`when_to_send`) <= toDate
			AND (
				(subAccountIds IS NOT NULL
				AND FIND_IN_SET(`jc`.`subaccount_id`, subAccountIds)
				) OR (subAccountIds IS NULL)
			)
			AND (
				(subAccountId IS NOT NULL
				AND `jc`.`subaccount_id` = subAccountId
				) OR (subAccountId IS NULL)
			)
		) as `completed_stats`,
		
		(
			SELECT CONCAT_WS('--', COUNT(`jf`.`id`), SUM(`jf`.`credits_consumed`))
			FROM `jobs_failed` jf
			WHERE `jf`.`campaign_id` = `cps`.`id`
			AND DATE(`jf`.`time_attempt_send`) >= fromDate
			AND DATE(`jf`.`time_attempt_send`) <= toDate
			AND `jf`.`deleted_at` IS NULL
			AND (
				(subAccountIds IS NOT NULL
				AND FIND_IN_SET(`jf`.`subaccount_id`, subAccountIds)
				) OR (subAccountIds IS NULL)
			)
			AND (
				(subAccountId IS NOT NULL
				AND `jf`.`subaccount_id` = subAccountId
				) OR (subAccountId IS NULL)
			)
		) as `failed_stats`,
		
		(
			SELECT CONCAT_WS('--', COUNT(`jt`.`id`), SUM(`jt`.`credits_consumed`))
			FROM `job_today` jt
			WHERE `jt`.`campaign_id` = `cps`.`id`
			AND DATE(`jt`.`when_to_send`) >= fromDate
			AND DATE(`jt`.`when_to_send`) <= toDate
			AND (
				(subAccountIds IS NOT NULL
				AND FIND_IN_SET(`jt`.`subaccount_id`, subAccountIds)
				) OR (subAccountIds IS NULL)
			)
			AND (
				(subAccountId IS NOT NULL
				AND `jt`.`subaccount_id` = subAccountId
				) OR (subAccountId IS NULL)
			)
		) as `today_stats`,
		
		(
			SELECT CONCAT_WS('--', COUNT(`jtm`.`id`), SUM(`jtm`.`credits_consumed`))
			FROM `job_tomorrow` jtm
			WHERE `jtm`.`campaign_id` = `cps`.`id`
			AND DATE(`jtm`.`when_to_send`) >= fromDate
			AND DATE(`jtm`.`when_to_send`) <= toDate
			AND (
				(subAccountIds IS NOT NULL
				AND FIND_IN_SET(`jtm`.`subaccount_id`, subAccountIds)
				) OR (subAccountIds IS NULL)
			)
			AND (
				(subAccountId IS NOT NULL
				AND `jtm`.`subaccount_id` = subAccountId
				) OR (subAccountId IS NULL)
			)
		) as `tomorrow_stats`,
		
		(
			SELECT CONCAT_WS('--', COUNT(`jtfourth`.`id`), SUM(`jtfourth`.`credits_consumed`))
			FROM `job_after_tomorrow_to_14th_day` jtfourth
			WHERE `jtfourth`.`campaign_id` = `cps`.`id`
			AND DATE(`jtfourth`.`when_to_send`) >= fromDate
			AND DATE(`jtfourth`.`when_to_send`) <= toDate
			AND (
				(subAccountIds IS NOT NULL
				AND FIND_IN_SET(`jtfourth`.`subaccount_id`, subAccountIds)
				) OR (subAccountIds IS NULL)
			)
			AND (
				(subAccountId IS NOT NULL
				AND `jtfourth`.`subaccount_id` = subAccountId
				) OR (subAccountId IS NULL)
			)
		) as `fourth_stats`,
		
		(
			SELECT CONCAT_WS('--', COUNT(`jtfifth`.`id`), SUM(`jtfifth`.`credits_consumed`))
			FROM `job_15th_day_to_end_month` jtfifth
			WHERE `jtfifth`.`campaign_id` = `cps`.`id`
			AND DATE(`jtfifth`.`when_to_send`) >= fromDate
			AND DATE(`jtfifth`.`when_to_send`) <= toDate
			AND (
				(subAccountIds IS NOT NULL
				AND FIND_IN_SET(`jtfifth`.`subaccount_id`, subAccountIds)
				) OR (subAccountIds IS NULL)
			)
			AND (
				(subAccountId IS NOT NULL
				AND `jtfifth`.`subaccount_id` = subAccountId
				) OR (subAccountId IS NULL)
			)
		) as `fifth_stats`,
		
		(
			SELECT CONCAT_WS('--', COUNT(`jtnxtmonth`.`id`), SUM(`jtnxtmonth`.`credits_consumed`))
			FROM `job_next_month` jtnxtmonth
			WHERE `jtnxtmonth`.`campaign_id` = `cps`.`id`
			AND DATE(`jtnxtmonth`.`when_to_send`) >= fromDate
			AND DATE(`jtnxtmonth`.`when_to_send`) <= toDate
		) as `nextmonth_stats`
		
	FROM `campaigns` cps
	
	WHERE
		`cps`.`deleted_at` IS NULL 
		AND (
			clientID IS NOT NULL AND 
			`cps`.`client_id` = clientID
		)
		AND (
			(subAccountIds IS NOT NULL
			AND EXISTS (
				  SELECT `csa`.`sub_account_id`
				  FROM `campaign_sub_accounts` csa
				  WHERE FIND_IN_SET(`csa`.`sub_account_id`, subAccountIds)
				  AND `csa`.`campaign_id` = `cps`.`id`
				)
			) OR (subAccountIds IS NULL)
		)
		AND (
			(subAccountId IS NOT NULL
			AND EXISTS (
				  SELECT `csa`.`sub_account_id`
				  FROM `campaign_sub_accounts` csa
				  WHERE `csa`.`sub_account_id` = subAccountId
				  AND `csa`.`campaign_id` = `cps`.`id`
				)
			) OR (subAccountId IS NULL)
		)
		AND (
			(campaignIds IS NOT NULL
			AND FIND_IN_SET(`cps`.`id`, campaignIds)
			) OR (campaignIds IS NULL)
		)
		AND (
			(campaignId IS NOT NULL AND
			`cps`.`id` = campaignId
			) OR (campaignId IS NULL)
		)
		AND (
			(fromDate IS NOT NULL
			AND `cps`.`created_at` >= fromDate)
			OR (fromDate IS NULL)
		)
		AND (
			(toDate IS NOT NULL
			AND `cps`.`created_at` <= toDate)
			OR (toDate IS NULL)
		)
		ORDER BY `cps`.`name` ASC
";
