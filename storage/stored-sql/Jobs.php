<?php
$jobs_completed_total_credits = "
	SELECT
		COUNT(jc.id) as 'total_sms',
		SUM(jc.credits_consumed) as 'total_credits'
	FROM
		jobs_completed as jc
	WHERE
		(
			(clientID IS NOT NULL AND
				`jc`.`client_id` = clientID) 
			OR (clientID IS NULL)
		) 
		AND (
			(
				(
					subAccountIds IS NOT NULL
					OR subAccountId IS NOT NULL
				) AND
				(
					(
						subAccountIds IS NOT NULL AND
						FIND_IN_SET(`jc`.`subaccount_id`, subAccountIds)
					) OR (
						subAccountId IS NOT NULL AND
						`jc`.`subaccount_id` = subAccountId
					)
				)
			)
		)
		AND (
			(fromDate IS NOT NULL AND
				DATE(jc.when_to_send) >= fromDate)
			OR (fromDate IS NULL)
		)
		AND (
			(toDate IS NOT NULL AND
				DATE(jc.when_to_send) <= toDate)
			OR (toDate IS NULL)
		)
		AND (
			(taskTypeId IS NOT NULL AND
				EXISTS (
					SELECT task.task_type_id
					FROM task
					WHERE task.id = jc.task_id
					AND task.task_type_id = taskTypeId
				)
			)
			OR (taskTypeId IS NULL)
		)
		AND (
			(campaignId IS NOT NULL AND
				jc.campaign_id = campaignId) 
			OR (campaignId IS NULL)
		) 
	";

$jobs_completed_daterange = "
	SELECT
		`jobs_completed`.*,
		`tsk`.`success_jobs_count`,
		`tsk`.`jobs_count` as reciepient,
		`jobs_completed`.`credits_consumed` as credits_used,
		`cp`.`name` as campaign_name,
		`sc`.`account_name` as subaccount_name,
		`tsk`.`task_type_id`,
        CONCAT('completed') as `instance`
	FROM `jobs_completed`
		
	JOIN `task` tsk 
	ON `tsk`.`id` = `jobs_completed`.`task_id`
	
	LEFT JOIN `campaigns` cp
	ON `cp`.`id` = `tsk`.`campaign_id`
	
	LEFT JOIN `sub_accounts` sc
	ON `sc`.`id` = `tsk`.`subaccount_id`
	
	WHERE
		`jobs_completed`.`client_id` = clientId
		AND `tsk`.`deleted_at` IS NULL
		
		AND (
			(fromDate IS NOT NULL
			AND DATE(`jobs_completed`.`when_to_send`) >= fromDate)
			OR (fromDate IS NULL)
		)
		
		AND (
			(toDate IS NOT NULL
			AND DATE(`jobs_completed`.`when_to_send`) <= toDate)
			OR (toDate IS NULL)
		)
		
		AND (
			(taskTypeId IS NOT NULL AND EXISTS(
					SELECT `task`.`task_type_id`, `task`.`deleted_at`, `task`.`id`
					FROM `task`
					WHERE `jobs_completed`.`task_id` = `task`.`id` 
					AND `task`.`task_type_id` = taskTypeId 
					AND `task`.`deleted_at` IS NULL
				)
			) OR (taskTypeId IS NULL)
		)
		
		AND (
			(subAccountIds IS NOT NULL
			AND FIND_IN_SET(`jobs_completed`.`subaccount_id`, subAccountIds)
			) OR (subAccountIds IS NULL)
		)
		
		AND (
			(subAccountId IS NOT NULL AND
				`jobs_completed`.`subaccount_id` = subAccountId
			) OR (subAccountId IS NULL)
		)
		
		AND (
			(campaignId IS NOT NULL AND
				`jobs_completed`.`campaign_id` = campaignId
			) OR (campaignId IS NULL)
		)
		
		AND (
			(campaignIds IS NOT NULL
			AND FIND_IN_SET(`jobs_completed`.`campaign_id`, campaignIds)
			) OR (campaignIds IS NULL)
		)
";

$jobs_failed_daterange = "
	SELECT
		`jobs_failed`.*,
        `tsk`.`task_type_id`,
		`jobs_failed`.`credits_consumed` as credits_used,
		`cp`.`name` as campaign_name,
		`sc`.`account_name` as subaccount_name,
        CONCAT('failed') as `instance`
		
	FROM `jobs_failed`
		
	JOIN `task` tsk 
	ON `tsk`.`id` = `jobs_failed`.`task_id`
	
	LEFT JOIN `campaigns` cp
	ON `cp`.`id` = `tsk`.`campaign_id`
	
	LEFT JOIN `sub_accounts` sc
	ON `sc`.`id` = `tsk`.`subaccount_id`
	
	WHERE
		`jobs_failed`.`deleted_at` IS NULL
	AND `jobs_failed`.`client_id` = clientId 
	AND (
		(fromDate IS NOT NULL
		AND `jobs_failed`.`time_attempt_send` >= fromDate)
		OR (fromDate IS NULL)
	)
	AND (
		(toDate IS NOT NULL
		AND `jobs_failed`.`time_attempt_send` <= toDate)
		OR (toDate IS NULL)
	)
	AND (
		(taskTypeId IS NOT NULL AND EXISTS(
				SELECT `task`.`task_type_id`, `task`.`deleted_at`, `task`.`id`
				FROM `task`
				WHERE `jobs_failed`.`task_id` = `task`.`id` 
				AND `task`.`task_type_id` = taskTypeId 
				AND `task`.`deleted_at` IS NULL
			)
		)
		OR (taskTypeId IS NULL)
	)
	
	AND (
		(subAccountIds IS NOT NULL
		AND FIND_IN_SET(`jobs_failed`.`subaccount_id`, subAccountIds)
		) OR (subAccountIds IS NULL)
	)
		
	AND (
		(subAccountId IS NOT NULL AND
			`jobs_failed`.`subaccount_id` = subAccountId
		)
		OR (subAccountId IS NULL)
	)
	
	AND (
		(campaignId IS NOT NULL AND
			`jobs_failed`.`campaign_id` = campaignId
		)
		OR (campaignId IS NULL)
	)
	
	AND (
		(campaignIds IS NOT NULL
		AND FIND_IN_SET(`jobs_failed`.`campaign_id`, campaignIds)
		) OR (campaignIds IS NULL)
	)
";

$jobs_today_daterange = "
	SELECT
		`job_today`.*,
		`tsk`.`success_jobs_count`,
		`tsk`.`jobs_count` as reciepient,
		`job_today`.`credits_consumed` as credits_used,
		`cp`.`name` as campaign_name,
		`sc`.`account_name` as subaccount_name,
		`tsk`.`task_type_id`,
        CONCAT('today') as `instance`
	FROM `job_today`
		
	JOIN `task` tsk 
	ON `tsk`.`id` = `job_today`.`task_id`
	
	LEFT JOIN `campaigns` cp
	ON `cp`.`id` = `tsk`.`campaign_id`
	
	LEFT JOIN `sub_accounts` sc
	ON `sc`.`id` = `tsk`.`subaccount_id`
	
	WHERE
		`job_today`.`client_id` = clientId
		AND `tsk`.`deleted_at` IS NULL
		AND (
			(fromDate IS NOT NULL
			AND DATE(`job_today`.`when_to_send`) >= fromDate)
			OR (fromDate IS NULL)
		)
		
		AND (
			(toDate IS NOT NULL
			AND DATE(`job_today`.`when_to_send`) <= toDate)
			OR (toDate IS NULL)
		)
		
		AND (
			(taskTypeId IS NOT NULL AND EXISTS(
					SELECT `task`.`task_type_id`, `task`.`deleted_at`, `task`.`id`
					FROM `task`
					WHERE `job_today`.`task_id` = `task`.`id` 
					AND `task`.`task_type_id` = taskTypeId 
					AND `task`.`deleted_at` IS NULL
				)
			) OR (taskTypeId IS NULL)
		)
		
		AND (
			(subAccountIds IS NOT NULL
			AND FIND_IN_SET(`job_today`.`subaccount_id`, subAccountIds)
			) OR (subAccountIds IS NULL)
		)
		
		AND (
			(subAccountId IS NOT NULL AND
				`job_today`.`subaccount_id` = subAccountId
			) OR (subAccountId IS NULL)
		)
		
		AND (
			(campaignId IS NOT NULL AND
				`job_today`.`campaign_id` = campaignId
			) OR (campaignId IS NULL)
		)
		
		AND (
			(campaignIds IS NOT NULL
			AND FIND_IN_SET(`job_today`.`campaign_id`, campaignIds)
			) OR (campaignIds IS NULL)
		)
";

$jobs_tomorrow_daterange = "
	SELECT
		`job_tomorrow`.*,
		`tsk`.`success_jobs_count`,
		`tsk`.`jobs_count` as reciepient,
		`job_tomorrow`.`credits_consumed` as credits_used,
		`cp`.`name` as campaign_name,
		`sc`.`account_name` as subaccount_name,
		`tsk`.`task_type_id`,
        CONCAT('tomorrow') as `instance`
	FROM `job_tomorrow`
		
	JOIN `task` tsk 
	ON `tsk`.`id` = `job_tomorrow`.`task_id`
	
	LEFT JOIN `campaigns` cp
	ON `cp`.`id` = `tsk`.`campaign_id`
	
	LEFT JOIN `sub_accounts` sc
	ON `sc`.`id` = `tsk`.`subaccount_id`
	
	WHERE
		`job_tomorrow`.`client_id` = clientId
		AND `tsk`.`deleted_at` IS NULL
		
		AND (
			(fromDate IS NOT NULL
			AND DATE(`job_tomorrow`.`when_to_send`) >= fromDate)
			OR (fromDate IS NULL)
		)
		
		AND (
			(toDate IS NOT NULL
			AND DATE(`job_tomorrow`.`when_to_send`) <= toDate)
			OR (toDate IS NULL)
		)
		
		AND (
			(taskTypeId IS NOT NULL AND EXISTS(
					SELECT `task`.`task_type_id`, `task`.`deleted_at`, `task`.`id`
					FROM `task`
					WHERE `job_tomorrow`.`task_id` = `task`.`id` 
					AND `task`.`task_type_id` = taskTypeId 
					AND `task`.`deleted_at` IS NULL
				)
			) OR (taskTypeId IS NULL)
		)
		
		AND (
			(subAccountIds IS NOT NULL
			AND FIND_IN_SET(`job_tomorrow`.`subaccount_id`, subAccountIds)
			) OR (subAccountIds IS NULL)
		)
		
		AND (
			(subAccountId IS NOT NULL AND
				`job_tomorrow`.`subaccount_id` = subAccountId
			) OR (subAccountId IS NULL)
		)
		
		AND (
			(campaignId IS NOT NULL AND
				`job_tomorrow`.`campaign_id` = campaignId
			) OR (campaignId IS NULL)
		)
		
		AND (
			(campaignIds IS NOT NULL
			AND FIND_IN_SET(`job_tomorrow`.`campaign_id`, campaignIds)
			) OR (campaignIds IS NULL)
		)
";

$jobs_after_tomorrow_to_14th_day_daterange = "
	SELECT
		`job_after_tomorrow_to_14th_day`.*,
		`tsk`.`success_jobs_count`,
		`tsk`.`jobs_count` as reciepient,
		`job_after_tomorrow_to_14th_day`.`credits_consumed` as credits_used,
		`cp`.`name` as campaign_name,
		`sc`.`account_name` as subaccount_name,
		`tsk`.`task_type_id`,
        CONCAT('14thday') as `instance`
	FROM `job_after_tomorrow_to_14th_day`
		
	JOIN `task` tsk 
	ON `tsk`.`id` = `job_after_tomorrow_to_14th_day`.`task_id`
	
	LEFT JOIN `campaigns` cp
	ON `cp`.`id` = `tsk`.`campaign_id`
	
	LEFT JOIN `sub_accounts` sc
	ON `sc`.`id` = `tsk`.`subaccount_id`
	
	WHERE
		`job_after_tomorrow_to_14th_day`.`client_id` = clientId
		AND `tsk`.`deleted_at` IS NULL
		
		AND (
			(fromDate IS NOT NULL
			AND DATE(`job_after_tomorrow_to_14th_day`.`when_to_send`) >= fromDate)
			OR (fromDate IS NULL)
		)
		
		AND (
			(toDate IS NOT NULL
			AND DATE(`job_after_tomorrow_to_14th_day`.`when_to_send`) <= toDate)
			OR (toDate IS NULL)
		)
		
		AND (
			(taskTypeId IS NOT NULL AND EXISTS(
					SELECT `task`.`task_type_id`, `task`.`deleted_at`, `task`.`id`
					FROM `task`
					WHERE `job_after_tomorrow_to_14th_day`.`task_id` = `task`.`id` 
					AND `task`.`task_type_id` = taskTypeId 
					AND `task`.`deleted_at` IS NULL
				)
			) OR (taskTypeId IS NULL)
		)
		
		AND (
			(subAccountIds IS NOT NULL
			AND FIND_IN_SET(`job_after_tomorrow_to_14th_day`.`subaccount_id`, subAccountIds)
			) OR (subAccountIds IS NULL)
		)
		
		AND (
			(subAccountId IS NOT NULL AND
				`job_after_tomorrow_to_14th_day`.`subaccount_id` = subAccountId
			) OR (subAccountId IS NULL)
		)
		
		AND (
			(campaignId IS NOT NULL AND
				`job_after_tomorrow_to_14th_day`.`campaign_id` = campaignId
			) OR (campaignId IS NULL)
		)
		
		AND (
			(campaignIds IS NOT NULL
			AND FIND_IN_SET(`job_after_tomorrow_to_14th_day`.`campaign_id`, campaignIds)
			) OR (campaignIds IS NULL)
		)
";

$jobs_15th_day_to_end_month_daterange = "
	SELECT
		`job_15th_day_to_end_month`.*,
		`tsk`.`success_jobs_count`,
		`tsk`.`jobs_count` as reciepient,
		`job_15th_day_to_end_month`.`credits_consumed` as credits_used,
		`cp`.`name` as campaign_name,
		`sc`.`account_name` as subaccount_name,
		`tsk`.`task_type_id`,
        CONCAT('15thday') as `instance`
	FROM `job_15th_day_to_end_month`
		
	JOIN `task` tsk 
	ON `tsk`.`id` = `job_15th_day_to_end_month`.`task_id`
	
	LEFT JOIN `campaigns` cp
	ON `cp`.`id` = `tsk`.`campaign_id`
	
	LEFT JOIN `sub_accounts` sc
	ON `sc`.`id` = `tsk`.`subaccount_id`
	
	WHERE
		`job_15th_day_to_end_month`.`client_id` = clientId
		AND `tsk`.`deleted_at` IS NULL
		
		AND (
			(fromDate IS NOT NULL
			AND DATE(`job_15th_day_to_end_month`.`when_to_send`) >= fromDate)
			OR (fromDate IS NULL)
		)
		
		AND (
			(toDate IS NOT NULL
			AND DATE(`job_15th_day_to_end_month`.`when_to_send`) <= toDate)
			OR (toDate IS NULL)
		)
		
		AND (
			(taskTypeId IS NOT NULL AND EXISTS(
					SELECT `task`.`task_type_id`, `task`.`deleted_at`, `task`.`id`
					FROM `task`
					WHERE `job_15th_day_to_end_month`.`task_id` = `task`.`id` 
					AND `task`.`task_type_id` = taskTypeId 
					AND `task`.`deleted_at` IS NULL
				)
			) OR (taskTypeId IS NULL)
		)
		
		AND (
			(subAccountIds IS NOT NULL
			AND FIND_IN_SET(`job_15th_day_to_end_month`.`subaccount_id`, subAccountIds)
			) OR (subAccountIds IS NULL)
		)
		
		AND (
			(subAccountId IS NOT NULL AND
				`job_15th_day_to_end_month`.`subaccount_id` = subAccountId
			) OR (subAccountId IS NULL)
		)
		
		AND (
			(campaignId IS NOT NULL AND
				`job_15th_day_to_end_month`.`campaign_id` = campaignId
			) OR (campaignId IS NULL)
		)
		
		AND (
			(campaignIds IS NOT NULL
			AND FIND_IN_SET(`job_15th_day_to_end_month`.`campaign_id`, campaignIds)
			) OR (campaignIds IS NULL)
		)
";

$jobs_next_month_daterange = "
	SELECT
		`job_next_month`.*,
		`tsk`.`success_jobs_count`,
		`tsk`.`jobs_count` as reciepient,
		`job_next_month`.`credits_consumed` as credits_used,
		`cp`.`name` as campaign_name,
		`sc`.`account_name` as subaccount_name,
		`tsk`.`task_type_id`,
        CONCAT('nextmonth') as `instance`
	FROM `job_next_month`
		
	JOIN `task` tsk 
	ON `tsk`.`id` = `job_next_month`.`task_id`
	
	LEFT JOIN `campaigns` cp
	ON `cp`.`id` = `tsk`.`campaign_id`
	
	LEFT JOIN `sub_accounts` sc
	ON `sc`.`id` = `tsk`.`subaccount_id`
	
	WHERE
		`job_next_month`.`client_id` = clientId
		AND `tsk`.`deleted_at` IS NULL
		
		AND (
			(fromDate IS NOT NULL
			AND DATE(`job_next_month`.`when_to_send`) >= fromDate)
			OR (fromDate IS NULL)
		)
		
		AND (
			(toDate IS NOT NULL
			AND DATE(`job_next_month`.`when_to_send`) <= toDate)
			OR (toDate IS NULL)
		)
		
		AND (
			(taskTypeId IS NOT NULL AND EXISTS(
					SELECT `task`.`task_type_id`, `task`.`deleted_at`, `task`.`id`
					FROM `task`
					WHERE `job_next_month`.`task_id` = `task`.`id` 
					AND `task`.`task_type_id` = taskTypeId 
					AND `task`.`deleted_at` IS NULL
				)
			) OR (taskTypeId IS NULL)
		)
		
		AND (
			(subAccountIds IS NOT NULL
			AND FIND_IN_SET(`job_next_month`.`subaccount_id`, subAccountIds)
			) OR (subAccountIds IS NULL)
		)
		
		AND (
			(subAccountId IS NOT NULL AND
				`job_next_month`.`subaccount_id` = subAccountId
			) OR (subAccountId IS NULL)
		)
		
		AND (
			(campaignId IS NOT NULL AND
				`job_next_month`.`campaign_id` = campaignId
			) OR (campaignId IS NULL)
		)
		
		AND (
			(campaignIds IS NOT NULL
			AND FIND_IN_SET(`job_next_month`.`campaign_id`, campaignIds)
			) OR (campaignIds IS NULL)
		)
";

$jobs_per_subaccount_daterange = "
	SELECT
		`sb`.`id`,
		`sb`.`account_name`,
		`sb`.`client_id`,
		(
			SELECT CONCAT_WS('--', COUNT(`jc`.`id`), SUM(`jc`.`credits_consumed`))
			FROM `jobs_completed` jc
			WHERE `jc`.`subaccount_id` = `sb`.`id`
			AND (
				(fromDate IS NOT NULL
				AND DATE(`jc`.`when_to_send`) >= fromDate)
				OR (fromDate IS NULL)
			)
			
			AND (
				(toDate IS NOT NULL
				AND DATE(`jc`.`when_to_send`) <= toDate)
				OR (toDate IS NULL)
			)
			AND (
				(subAccountIds IS NOT NULL
				AND FIND_IN_SET(`jc`.`subaccount_id`, subAccountIds)
				) OR (subAccountIds IS NULL)
			)
			AND (
				(subAccountId IS NOT NULL
				AND `jc`.`subaccount_id` = subAccountId
				) OR (subAccountId IS NULL)
			)
			AND (
				(taskTypeId IS NOT NULL AND EXISTS(
					SELECT `task`.`task_type_id`, `task`.`deleted_at`, `task`.`id`
					FROM `task`
					WHERE `jc`.`task_id` = `task`.`id` 
					AND `task`.`task_type_id` = taskTypeId 
					AND `task`.`deleted_at` IS NULL
				)
				) OR (taskTypeId IS NULL)
			)
			AND (
				(campaignId IS NOT NULL AND
					`jc`.`campaign_id` = campaignId
				) OR (campaignId IS NULL)
			)
			AND (
				(campaignIds IS NOT NULL
				AND FIND_IN_SET(`jc`.`campaign_id`, campaignIds)
				) OR (campaignIds IS NULL)
			)
		) as completed_stats,
		
		(
			SELECT CONCAT_WS('--', COUNT(`jf`.`id`), SUM(`jf`.`credits_consumed`))
			FROM `jobs_failed` jf
			WHERE `jf`.`subaccount_id` = `sb`.`id`
			AND `jf`.`deleted_at` IS NULL
			AND (
				(fromDate IS NOT NULL
				AND `jf`.`time_attempt_send` >= fromDate)
				OR (fromDate IS NULL)
			)
			
			AND (
				(toDate IS NOT NULL
				AND `jf`.`time_attempt_send` <= toDate)
				OR (toDate IS NULL)
			)
			AND (
				(subAccountIds IS NOT NULL
				AND FIND_IN_SET(`jf`.`subaccount_id`, subAccountIds)
				) OR (subAccountIds IS NULL)
			)
			AND (
				(subAccountId IS NOT NULL
				AND `jf`.`subaccount_id` = subAccountId
				) OR (subAccountId IS NULL)
			)
			AND (
				(taskTypeId IS NOT NULL AND EXISTS(
					SELECT `task`.`task_type_id`, `task`.`deleted_at`, `task`.`id`
					FROM `task`
					WHERE `jf`.`task_id` = `task`.`id` 
					AND `task`.`task_type_id` = taskTypeId 
					AND `task`.`deleted_at` IS NULL
				)
				) OR (taskTypeId IS NULL)
			)
			AND (
				(campaignId IS NOT NULL AND
					`jf`.`campaign_id` = campaignId
				) OR (campaignId IS NULL)
			)
			AND (
				(campaignIds IS NOT NULL
				AND FIND_IN_SET(`jf`.`campaign_id`, campaignIds)
				) OR (campaignIds IS NULL)
			)
		) as failed_stats,
		
		(
			SELECT CONCAT_WS('--', COUNT(`jt`.`id`), SUM(`jt`.`credits_consumed`))
			FROM `job_today` jt
			WHERE `jt`.`subaccount_id` = `sb`.`id`
			AND (
				(fromDate IS NOT NULL
				AND DATE(`jt`.`when_to_send`) >= fromDate)
				OR (fromDate IS NULL)
			)
			
			AND (
				(toDate IS NOT NULL
				AND DATE(`jt`.`when_to_send`) <= toDate)
				OR (toDate IS NULL)
			)
			AND (
				(subAccountIds IS NOT NULL
				AND FIND_IN_SET(`jt`.`subaccount_id`, subAccountIds)
				) OR (subAccountIds IS NULL)
			)
			AND (
				(subAccountId IS NOT NULL
				AND `jt`.`subaccount_id` = subAccountId
				) OR (subAccountId IS NULL)
			)
			AND (
				(taskTypeId IS NOT NULL AND EXISTS(
					SELECT `task`.`task_type_id`, `task`.`deleted_at`, `task`.`id`
					FROM `task`
					WHERE `jt`.`task_id` = `task`.`id` 
					AND `task`.`task_type_id` = taskTypeId 
					AND `task`.`deleted_at` IS NULL
				)
				) OR (taskTypeId IS NULL)
			)
			AND (
				(campaignId IS NOT NULL AND
					`jt`.`campaign_id` = campaignId
				) OR (campaignId IS NULL)
			)
			AND (
				(campaignIds IS NOT NULL
				AND FIND_IN_SET(`jt`.`campaign_id`, campaignIds)
				) OR (campaignIds IS NULL)
			)
		) as today_stats,
		
		(
			SELECT CONCAT_WS('--', COUNT(`jtm`.`id`), SUM(`jtm`.`credits_consumed`))
			FROM `job_tomorrow` jtm
			WHERE `jtm`.`subaccount_id` = `sb`.`id`
			AND (
				(fromDate IS NOT NULL
				AND DATE(`jtm`.`when_to_send`) >= fromDate)
				OR (fromDate IS NULL)
			)
			
			AND (
				(toDate IS NOT NULL
				AND DATE(`jtm`.`when_to_send`) <= toDate)
				OR (toDate IS NULL)
			)
			AND (
				(subAccountIds IS NOT NULL
				AND FIND_IN_SET(`jtm`.`subaccount_id`, subAccountIds)
				) OR (subAccountIds IS NULL)
			)
			AND (
				(subAccountId IS NOT NULL
				AND `jtm`.`subaccount_id` = subAccountId
				) OR (subAccountId IS NULL)
			)
			AND (
				(taskTypeId IS NOT NULL AND EXISTS(
					SELECT `task`.`task_type_id`, `task`.`deleted_at`, `task`.`id`
					FROM `task`
					WHERE `jtm`.`task_id` = `task`.`id` 
					AND `task`.`task_type_id` = taskTypeId 
					AND `task`.`deleted_at` IS NULL
				)
				) OR (taskTypeId IS NULL)
			)
			AND (
				(campaignId IS NOT NULL AND
					`jtm`.`campaign_id` = campaignId
				) OR (campaignId IS NULL)
			)
			AND (
				(campaignIds IS NOT NULL
				AND FIND_IN_SET(`jtm`.`campaign_id`, campaignIds)
				) OR (campaignIds IS NULL)
			)
		) as tomorrow_stats,
		
		(
			SELECT CONCAT_WS('--', COUNT(`jatt14td`.`id`), SUM(`jatt14td`.`credits_consumed`))
			FROM `job_after_tomorrow_to_14th_day` jatt14td 
			WHERE `jatt14td`.`subaccount_id` = `sb`.`id`
			AND (
				(fromDate IS NOT NULL
				AND DATE(`jatt14td`.`when_to_send`) >= fromDate)
				OR (fromDate IS NULL)
			)
			
			AND (
				(toDate IS NOT NULL
				AND DATE(`jatt14td`.`when_to_send`) <= toDate)
				OR (toDate IS NULL)
			)
			AND (
				(subAccountIds IS NOT NULL
				AND FIND_IN_SET(`jatt14td`.`subaccount_id`, subAccountIds)
				) OR (subAccountIds IS NULL)
			)
			AND (
				(subAccountId IS NOT NULL
				AND `jatt14td`.`subaccount_id` = subAccountId
				) OR (subAccountId IS NULL)
			)
			AND (
				(taskTypeId IS NOT NULL AND EXISTS(
					SELECT `task`.`task_type_id`, `task`.`deleted_at`, `task`.`id`
					FROM `task`
					WHERE `jatt14td`.`task_id` = `task`.`id` 
					AND `task`.`task_type_id` = taskTypeId 
					AND `task`.`deleted_at` IS NULL
				)
				) OR (taskTypeId IS NULL)
			)
			AND (
				(campaignId IS NOT NULL AND
					`jatt14td`.`campaign_id` = campaignId
				) OR (campaignId IS NULL)
			)
			AND (
				(campaignIds IS NOT NULL
				AND FIND_IN_SET(`jatt14td`.`campaign_id`, campaignIds)
				) OR (campaignIds IS NULL)
			)
		) as fourth_stats,
		
		(
			SELECT CONCAT_WS('--', COUNT(`j15tdtem`.`id`), SUM(`j15tdtem`.`credits_consumed`))
			FROM `job_15th_day_to_end_month` j15tdtem 
			WHERE `j15tdtem`.`subaccount_id` = `sb`.`id`
			AND (
				(fromDate IS NOT NULL
				AND DATE(`j15tdtem`.`when_to_send`) >= fromDate)
				OR (fromDate IS NULL)
			)
			
			AND (
				(toDate IS NOT NULL
				AND DATE(`j15tdtem`.`when_to_send`) <= toDate)
				OR (toDate IS NULL)
			)
			AND (
				(subAccountIds IS NOT NULL
				AND FIND_IN_SET(`j15tdtem`.`subaccount_id`, subAccountIds)
				) OR (subAccountIds IS NULL)
			)
			AND (
				(subAccountId IS NOT NULL
				AND `j15tdtem`.`subaccount_id` = subAccountId
				) OR (subAccountId IS NULL)
			)
			AND (
				(taskTypeId IS NOT NULL AND EXISTS(
					SELECT `task`.`task_type_id`, `task`.`deleted_at`, `task`.`id`
					FROM `task`
					WHERE `j15tdtem`.`task_id` = `task`.`id` 
					AND `task`.`task_type_id` = taskTypeId 
					AND `task`.`deleted_at` IS NULL
				)
				) OR (taskTypeId IS NULL)
			)
			AND (
				(campaignId IS NOT NULL AND
					`j15tdtem`.`campaign_id` = campaignId
				) OR (campaignId IS NULL)
			)
			AND (
				(campaignIds IS NOT NULL
				AND FIND_IN_SET(`j15tdtem`.`campaign_id`, campaignIds)
				) OR (campaignIds IS NULL)
			)
		) as fifth_stats,
		
		(
			SELECT CONCAT_WS('--', COUNT(`jnm`.`id`), SUM(`jnm`.`credits_consumed`))
			FROM `job_next_month` jnm 
			WHERE `jnm`.`subaccount_id` = `sb`.`id`
			AND (
				(fromDate IS NOT NULL
				AND DATE(`jnm`.`when_to_send`) >= fromDate)
				OR (fromDate IS NULL)
			)
			
			AND (
				(toDate IS NOT NULL
				AND DATE(`jnm`.`when_to_send`) <= toDate)
				OR (toDate IS NULL)
			)
			AND (
				(subAccountIds IS NOT NULL
				AND FIND_IN_SET(`jnm`.`subaccount_id`, subAccountIds)
				) OR (subAccountIds IS NULL)
			)
			AND (
				(subAccountId IS NOT NULL
				AND `jnm`.`subaccount_id` = subAccountId
				) OR (subAccountId IS NULL)
			)
			AND (
				(taskTypeId IS NOT NULL AND EXISTS(
					SELECT `task`.`task_type_id`, `task`.`deleted_at`, `task`.`id`
					FROM `task`
					WHERE `jnm`.`task_id` = `task`.`id` 
					AND `task`.`task_type_id` = taskTypeId 
					AND `task`.`deleted_at` IS NULL
				)
				) OR (taskTypeId IS NULL)
			)
			AND (
				(campaignId IS NOT NULL AND
					`jnm`.`campaign_id` = campaignId
				) OR (campaignId IS NULL)
			)
			AND (
				(campaignIds IS NOT NULL
				AND FIND_IN_SET(`jnm`.`campaign_id`, campaignIds)
				) OR (campaignIds IS NULL)
			)
		) as nextmonth_stats
		
	FROM `sub_accounts` sb
	
	WHERE
		`sb`.`deleted_at` IS NULL
		AND `sb`.`active` = 1
		AND `sb`.`client_id` = clientId
";

$task_jobs_for_nonreturning = "
	SELECT 
		task.id,
		task.subaccount_id,
		task.client_id,
		(
			SELECT GROUP_CONCAT(CONCAT_WS('**', `jt`.`id`, `jt`.`credits_consumed`) SEPARATOR '*`*')
			FROM `job_today` jt 
			WHERE `jt`.`task_id` = `task`.`id`
			AND (
				(customerNumber IS NOT NULL
				AND `jt`.`customer_number` LIKE customerNumber
				) OR (customerNumber IS NULL)
			)
			AND (
				(customerId IS NOT NULL
				AND `jt`.`customer_id` = customerId
				) OR (customerId IS NULL)
			)
			AND (
				(entryId IS NOT NULL
				AND `jt`.`entry_id` = entryId
				) OR (entryId IS NULL)
			)
		) as 'job_today',
		
		(
			SELECT GROUP_CONCAT(CONCAT_WS('**', `jtm`.`id`, `jtm`.`credits_consumed`) SEPARATOR '*`*')
			FROM `job_tomorrow` jtm
			WHERE `jtm`.`task_id` = `task`.`id`
			AND (
				(customerNumber IS NOT NULL
				AND `jtm`.`customer_number` LIKE customerNumber
				) OR (customerNumber IS NULL)
			)
			AND (
				(customerId IS NOT NULL
				AND `jtm`.`customer_id` = customerId
				) OR (customerId IS NULL)
			)
			AND (
				(entryId IS NOT NULL
				AND `jtm`.`entry_id` = entryId
				) OR (entryId IS NULL)
			)
		) as 'job_tomorrow',
		
		(
			SELECT GROUP_CONCAT(CONCAT_WS('**', `jnm`.`id`, `jnm`.`credits_consumed`) SEPARATOR '*`*')
			FROM `job_next_month` jnm
			WHERE `jnm`.`task_id` = `task`.`id`
			AND (
				(customerNumber IS NOT NULL
				AND `jnm`.`customer_number` LIKE customerNumber
				) OR (customerNumber IS NULL)
			)
			AND (
				(customerId IS NOT NULL
				AND `jnm`.`customer_id` = customerId
				) OR (customerId IS NULL)
			)
			AND (
				(entryId IS NOT NULL
				AND `jnm`.`entry_id` = entryId
				) OR (entryId IS NULL)
			)
		) as 'job_next_month',
		
		(
			SELECT GROUP_CONCAT(CONCAT_WS('**', `jttd`.`id`, `jttd`.`credits_consumed`) SEPARATOR '*`*')
			FROM `job_after_tomorrow_to_14th_day` jttd
			WHERE `jttd`.`task_id` = `task`.`id`
			AND (
				(customerNumber IS NOT NULL
				AND `jttd`.`customer_number` LIKE customerNumber
				) OR (customerNumber IS NULL)
			)
			AND (
				(customerId IS NOT NULL
				AND `jttd`.`customer_id` = customerId
				) OR (customerId IS NULL)
			)
			AND (
				(entryId IS NOT NULL
				AND `jttd`.`entry_id` = entryId
				) OR (entryId IS NULL)
			)
		) as 'job_after_tomorrow_to_14th_day',
		
		(
			SELECT GROUP_CONCAT(CONCAT_WS('**', `jtdt`.`id`, `jtdt`.`credits_consumed`) SEPARATOR '*`*')
			FROM `job_15th_day_to_end_month` jtdt
			WHERE `jtdt`.`task_id` = `task`.`id`
			AND (
				(customerNumber IS NOT NULL
				AND `jtdt`.`customer_number` LIKE customerNumber
				) OR (customerNumber IS NULL)
			)
			AND (
				(customerId IS NOT NULL
				AND `jtdt`.`customer_id` = customerId
				) OR (customerId IS NULL)
			)
			AND (
				(entryId IS NOT NULL
				AND `jtdt`.`entry_id` = entryId
				) OR (entryId IS NULL)
			)
		) as 'job_15th_day_to_end_month'
		
	FROM task
	WHERE task.id = taskId
";