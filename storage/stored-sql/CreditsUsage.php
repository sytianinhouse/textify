<?php
$search_credits_usage = "
SELECT
    `comp`.`id` id,
    `comp`.`client_id` client_id,
    `comp`.`subaccount_id` subaccount_id,
    `comp`.`campaign_id` campaign_id,
    COUNT(`comp`.`id`) sms_sent,
    SUM(`comp`.`credits_consumed`) credits_consumed,
    `comp`.`when_to_send` when_to_send
FROM
    jobs_completed comp
WHERE
    `comp`.`client_id` = clientID
AND (
    (campaignId IS NOT NULL AND
        comp.`campaign_id` = campaignId
    )
    OR (campaignId IS NULL)
)
AND (
    (subAccountId IS NOT NULL AND
        comp.`subaccount_id` = subAccountId
    )
    OR (subAccountId IS NULL)
)
AND `comp`.`when_to_send` >= fromDate
AND `comp`.`when_to_send` <= toDate
GROUP BY CAST(when_to_send AS DATE)
ORDER BY when_to_send ASC
";
