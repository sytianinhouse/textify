@extends('layouts.fixed_header_admin')

{{-- Page title --}}
@section('title')
    {{ $title ?: 'Title goes here' }}
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.index') }}">Projects</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.index',[$project]) }}">Clients</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.sub.index', [$project, $client, $subAccount]) }}">SubAccounts</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.sub.task.tbs.index', [$project, $client, $subAccount]) }}">Text Blast SMS</a>
    </li>
    <li class="breadcrumb-item active">Add Task</li>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
            </div>
        </div>
    </div>

    @include('templates.tasks.text-blast.form', [
        'task' =>  new \ATM\Repositories\Tasks\Task,
        'method' => 'post',
        'type' => 'text_blast_send'
    ])

@stop
