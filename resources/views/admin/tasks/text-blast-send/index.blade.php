@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    Text Blast SMS
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.index') }}">Projects</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.index',[$project]) }}">Clients</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.sub.index',[$project, $client]) }}">SubAccounts</a>
    </li>
    <li class="active breadcrumb-item">Text Blast SMS</li>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? strtoupper($project->name)."'s ".$title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.project.client.sub.task.tbs.create', [$project, $client, $subAccount]) }}" class=" btn btn-success m-r-5">
                            Add Task &nbsp;&nbsp;<i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- @include('partials.filters') --}}

    <div class="card">
        <div class="card-body" id="project_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1" width="15%">Campaign Title</th>
                        <th width="40%">Statistics</th>
                        <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1" width="15%">Date and Time to send</th>
                        <th class="sorting wid-10" data-orderable="false" tabindex="0" rowspan="1" colspan="1" width="20%">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($tasks as $task)
                        <tr>
                            <td>{{ $task->campaigns->name }}</td>
                            <td>
                                <div class="row align-items-center">
                                    <div class="col-lg-4 col-sm-6">
                                        <div id="{{ 'gauge_'.$task->id.'-1' }}" class="gauge" data-type="jobs_count" data-color="#fdcb6e" data-total-count="{{ $task->jobs_count ? $task->jobs_count : 0 }}" data-count="{{ $task->jobs_count ? $task->jobs_count : 0 }}"></div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6">
                                        <div id="{{ 'gauge_'.$task->id.'-2' }}" class="gauge" data-type="success_jobs_count" data-color="#00b894" data-total-count="{{ $task->jobs_count ? $task->jobs_count : 0 }}" data-count="{{ $task->success_jobs_count ? $task->success_jobs_count : 0 }}"></div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6">
                                        <div id="{{ 'gauge_'.$task->id.'-3' }}" class="gauge" data-type="failed_jobs" data-color="#d63031" data-total-count="{{ $task->jobs_count ? $task->jobs_count : 0 }}" data-count="{{ $task->tasksjobsFailed ? $task->tasksjobsFailed->count() : 0 }}"></div>
                                    </div>
                                </div>
                            </td>
                            <td>{{ $task->date_time_to_send->toDayDateTimeString() }}</td>
                            <td style="text-align: center;">
                                @php
                                    if( $task->tasks_job_today_count > 0 || $task->tasks_job_tomorrow_count > 0 || $task->tasks_job_after_tomorrow_to14th_day_count > 0 || $task->tasks_job15th_day_to_end_month_count > 0 || $task->tasks_job_next_month_count > 0 || $task->tasksjobs_completed_count > 0 || $task->tasksjobs_failed_count > 0 ):
                                @endphp
                                    <a href="{{route('admin.project.client.sub.task.tbs.view', [$project, $client, $subAccount, $task])}}" title="View" class="btn btn-sm btn-info">View</a>
                                @php
                                    else:
                                @endphp
                                    <a href="{{route('admin.project.client.sub.task.tbs.edit', [$project, $client, $subAccount, $task])}}" title="Edit" class="btn btn-sm btn-info">Edit</a>
                                    <a href="{{route('admin.project.client.sub.task.tbs.delete', [$project, $client, $subAccount, $task])}}" title="Edit" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                @php
                                    endif;
                                @endphp
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="7">No Records Found</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@stop


{{-- page level scripts --}}
