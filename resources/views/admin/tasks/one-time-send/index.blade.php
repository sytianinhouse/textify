@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
        Template API
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.index') }}">Projects</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.index',[$project]) }}">Clients</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.sub.index',[$project, $client]) }}">SubAccounts</a>
    </li>
    <li class="active breadcrumb-item">Template API</li>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? strtoupper($project->name)."'s ".$title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.project.client.sub.task.ots.create', [$project, $client, $subAccount]) }}" class=" btn btn-success m-r-5">
                            Add Task &nbsp;&nbsp;<i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- @include('partials.filters') --}}

    <div class="card">
        <div class="card-body" id="project_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Campaign Title</th>
                        <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Send After</th>
                        <th class="sorting wid-20" tabindex="0" rowspan="1" colspan="1">App Key</th>
                        <th class="sorting wid-10" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($tasks as $task)
                        <tr>
                            <td>{{ $task->campaigns->name }}</td>
                            <td>{{ $task->isSendImmediate() ? 'Send Immediate' : $task->ots_formatted }}</td>
                            <td>{{ $task->task_key }}</td>
                            <td>
                                <a href="{{route('admin.project.client.sub.task.ots.edit', [$project, $client, $subAccount, $task])}}" title="Edit" class="btn btn-sm btn-info">Edit</a>
                                <a href="{{route('admin.project.client.sub.task.ots.delete', [$project, $client, $subAccount, $task])}}" title="Edit" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6">No Records Found</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@stop

{{-- @section('scripts')
    <script type="text/javascript">

        $('.btn-delete-item').on('click', function(event) {
            event.preventDefault();

            let deleteUrl = $(this).data('url');

            swal({
                title: 'Are you sure?',
                text: 'You won\'t be able to revert this.',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: 'deleteUrl'
                    })
                    .done(function() {
                        console.log("success");
                    })
                    .fail(function() {
                        console.log("error");
                    })
                    .always(function() {
                        console.log("complete");
                    });
                }
            });

            /* Act on the event */
        });

    </script>
@stop --}}
{{-- page level scripts --}}
