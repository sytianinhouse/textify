@extends('layouts.fixed_header_admin')

{{-- Page title --}}
@section('title')
    {{ $title ?: 'Title goes here' }}
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.index') }}">Projects</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.index',[$project]) }}">Clients</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.sub.index',[$project, $client]) }}">SubAccounts</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.sub.task.it.index', [$project, $client, $subAccount]) }}">Custom API</a>
    </li>
    <li class="breadcrumb-item active">Edit Task</li>
@stop

@section('header_styles')
    <style type="text/css">
        .chosen-container {
            width: 100% !important;
        }
    </style>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
            </div>
        </div>
    </div>

    @include('templates.tasks.immediate-task.form', [
        'task' =>  $task,
        'method' => 'PUT',
    ])

@stop
