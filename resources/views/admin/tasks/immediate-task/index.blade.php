@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    Custom API
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.index') }}">Projects</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.index',[$project]) }}">Clients</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.sub.index',[$project, $client]) }}">SubAccounts</a>
    </li>
    <li class="active breadcrumb-item">Custom API</li>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? strtoupper($project->name)."'s ".$title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.project.client.sub.task.it.create', [$project, $client, $subAccount]) }}" class=" btn btn-success m-r-5">
                            Add Task &nbsp;&nbsp;<i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body" id="project_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc wid-20">Name</th>
                        <th class="sorting_asc wid-20">Campaign</th>
                        <th class="sorting_asc wid-20">Date Created</th>
                        <th class="sorting_asc wid-10">Status</th>
                        <th class="sorting wid-30" data-orderable="false">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($tasks as $task)
                        <tr>
                            <td><a href="{{ route('admin.project.client.sub.task.it.edit', [$project, $client, $subAccount, $task]) }}">{{ $task->name ?: 'Untitled' }}</a></td>
                            <td>{{ $task->campaign->name }}</td>
                            <td>@dateFormat($task->created_at)</td>
                            <td>{!! $task->human_status !!}</td>
                            <td>
                                <a href="{{ route('admin.project.client.sub.task.it.edit', [$project, $client, $subAccount, $task]) }}" class="btn btn-sm btn-success">EDIT</a>
                                <a href="{{ route('admin.project.client.sub.task.it.delete', [$project, $client, $subAccount, $task]) }}" class="btn btn-sm btn-danger">DELETE</a>
                            </td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop
