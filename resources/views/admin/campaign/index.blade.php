@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="breadcrumb-item"><a href="{{ route('admin.project.index') }}">Projects</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.project.client.index',[$project]) }}">Clients</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.project.client.sub.index',[$project, $client]) }}">Sub Accounts</a></li>
    <li class="active breadcrumb-item">Campaigns</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.project.client.sub.camp.create', [$project, $client,$subaccount]) }}" data-subaccount-url="{{ route('admin.project.client.sub.index',[$project, $client]) }}" data-validate-subaccounts="{{ $sbcount->count() }}" data-confirm-text="You don't have any sub Accounts yet, would you like to create one?" class="btn btn-success m-r-5 campaignCreationCheckSubaccounts">
                            Add Campaign <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- @include('partials.form-success') --}}
    {{-- @include('partials.filters') --}}
    <div class="card">
        <div class="card-body" id="client_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer this-table" id="datatable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Campaign Name</th>
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Sub Account</th>
                            <th class="sorting wid-25" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse( $campaign as $theCampaign )
                            <tr>
                                <td>
                                    <a href="{{ route('admin.project.client.sub.camp.edit', [$project, $client,$subaccount,$theCampaign]) }}" title="Edit">
                                        {{ $theCampaign->name }}
                                    </a>
                                </td>
                                <td>
                                    @foreach ( $theCampaign->subAccounts->sortBy('id') as $key => $sub )
                                        {{ $sub->account_name }}@if(!$loop->last),@endif
                                    @endforeach
                                </td>
                                {{-- <td>{{ str_limit( $theCampaign->description, 100, "...." ) }}
                                </td>
                                <td>
                                    {{ $theCampaign->created_at->toDateTimeString() }}
                                </td> --}}
                                <td>
                                    <a href="{{ route('admin.project.client.sub.camp.edit', [$project, $client,$subaccount,$theCampaign]) }}" title="Edit" class="btn btn-sm btn-info">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
                                    <a href="{{ route('admin.project.client.sub.camp.delete', [$project, $client,$subaccount,$theCampaign]) }}" title="Delete" class="confirmDelete btn btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">No records found!</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- /.inner -->
    <!-- /.outer -->
@stop
