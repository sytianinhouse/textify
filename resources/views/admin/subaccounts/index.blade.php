@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="breadcrumb-item"><a href="{{ route('admin.project.index') }}">Projects</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.project.client.index',[$project]) }}">Clients</a></li>
    <li class="active breadcrumb-item">Sub Accounts</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.project.client.sub.create', [$project, $client]) }}" class=" btn btn-success m-r-5">
                            Add Sub Account  <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- @include('partials.form-success') --}}
    {{-- @include('partials.filters') --}}
    <div class="card">
        <div class="card-body" id="client_body">
            <div id="sample_1_wrapper" class="dataTables_wrapper dt-boostrap no-footer">
                <div class="table-responsive">
                    <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer dataTable dtr-inline this-table" id="datatable" role="grid">
                        <thead>
                            <tr role="row">
                                {{-- <th class="sorting_asc wid-5" tabindex="0" rowspan="1" colspan="1">#</th> --}}
                                <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Account Name</th>
                                <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Security Key</th>
                                <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Mobile #</th>
                                <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">Status</th>
                                <th class="sorting wid-10" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse( $subaccountslists as $theSubAccount )
                                <tr>
                                    <td>
                                        <a href="{{ route('admin.project.client.sub.edit', [$project, $client, $theSubAccount]) }}" title="Edit">
                                            {{ $theSubAccount->account_name }}
                                        </a>
                                    </td>
                                    <td>{{ $theSubAccount->security_key }}</td>
                                    <td>{{ $theSubAccount->mobile_phone }}</td>
                                    <td>{!! $theSubAccount->active == 1 ? "<div class='active-display'>active</div>" : "<div class='inactive-display'>inactive</div>" !!}</td>
                                    <td>
                                        <a href="{{ route('admin.project.client.sub.edit', [$project, $client, $theSubAccount]) }}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="{{ route('admin.project.client.sub.settings', [$project, $client, $theSubAccount]) }}" title="Edit" class="btn btn-sm btn-warning"><i class="fa fa-cog" aria-hidden="true"></i></a>
                                        {{--<a href="{{ route('admin.project.client.sub.camp.index', [$project, $client, $theSubAccount]) }}" title="Edit" class="btn btn-sm btn-info">Campaigns</a>
                                        <a href="{{ route('admin.project.client.sub.task.tbs.index', [$project, $client, $theSubAccount]) }}" title="Edit" class="btn btn-sm btn-info">Text Blast SMS</a>
                                        <a href="{{ route('admin.project.client.sub.task.ots.index', [$project, $client, $theSubAccount]) }}" title="Edit" class="btn btn-sm btn-info">Template API</a>
                                        <a href="{{ route('admin.project.client.sub.task.it.index', [$project, $client, $theSubAccount]) }}" title="Edit" class="btn btn-sm btn-info">Custom API</a>
                                        <a href="{{ route('admin.project.client.sub.delete', [$project, $client, $theSubAccount]) }}" title="Delete" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>--}}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9">No records found!</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- /.inner -->
    <!-- /.outer -->
@stop
