@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    Projects
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.credit.index') }}">Credits</a>
    </li>
    <li class="active breadcrumb-item">{{ isset($title) ? $title : 'Page Header Title' }}</li>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        {{-- <a href="{{ route('admin.credit.logs') }}" class=" btn btn-success m-r-5">
                            View Logs <i class="fa fa-book"></i>
                        </a> --}}
                        {{-- <a href="#" class=" btn btn-primary">
                            <i class="fa fa-file"></i>
                        </a> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.filters', [
        'filterProject' => true,
    ])

    <div class="card">
        <div class="card-body" id="project_body">
            <div id="sample_1_wrapper" class="dataTables_wrapper dt-boostrap no-footer">
                <div class="table-responsive">
                    <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                        <thead>
                        <tr role="row">
                            <th width="14.2%" class="sorting_asc" tabindex="0">Project</th>
                            <th width="14.2%" class="sorting_asc" tabindex="0">Client</th>
                            <th width="14.2%" class="sorting" tabindex="0">Sub Account</th>
                            <th width="14.2%" class="sorting" tabindex="0">Type</th>
                            <th width="14.2%" class="sorting" tabindex="0">Date</th>
                            <th width="14.2%" class="sorting" tabindex="0">Notes</th>
                            {{-- <th width="14.2%" class="sorting" tabindex="0" data-orderable="false">Actions</th> --}}
                        </tr>
                        </thead>
                         <tbody>

                            @foreach($creditLogs as $log)
                            <tr>
                                <td>{{ $log->subAccount->client->project->name }}</td>
                                <td>{{ $log->subAccount->client->company_name }}</td>
                                <td>{{ $log->subAccount->account_name }}</td>
                                <td>{{ ucfirst($log->type) }}</td>
                                <td>@dateTimeFormat($log->date_time)</td>
                                <td>{{ $log->notes }}</td>
                                {{-- <td>
                                    <a href="{{ route('admin.credit.destroy', $credit) }}" title="Delete" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td> --}}
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@stop
{{-- page level scripts --}}
