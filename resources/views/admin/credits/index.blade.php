@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    Projects
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
   {{--  <li class="breadcrumb-item">
        <a href="{{ route('admin.project.index') }}">Projects</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.index', $project) }}">Clients</a>
    </li> --}}
    <li class="active breadcrumb-item">Credits</li>
@stop

{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                {{-- <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.credit.logs') }}" class=" btn btn-warning m-r-5">
                            <i class="fa fa-book"></i>&nbsp;&nbsp;View Logs
                        </a>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>

    @include('partials.filters', [
        'filterProject' => true,
        'noDaterange' => true,
        'activeFilter' => true
    ])

    <div class="card">
        <div class="card-body" id="project_body">
            <div id="sample_1_wrapper" class="dataTables_wrapper dt-boostrap no-footer">
                <div class="table-responsive">
                    <table data-order='[[ 5, "DESC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                        <thead>
                        <tr role="row">
                            <th width="14.2%" class="sorting_asc" tabindex="0">Client</th>
                            <th width="14.2%" class="sorting" tabindex="0">Sub Account</th>
                            <th width="14.2%" class="sorting text-right" tabindex="0">Actual Balance</th>
                            <th width="14.2%" class="sorting text-right" tabindex="0">Pending Balance</th>
                            <th width="14.2%" class="sorting" tabindex="0">Last Updated</th>
                            <th width="10%" class="sorting" data-orderable="false" tabindex="0" data-orderable="false">Actions</th>
                        </tr>
                        </thead>
                         <tbody>
                            @foreach($credits as $credit)
                            <tr>
                                <td>{{ $credit->client->company_name }}</td>
                                <td>{{ $credit->subAccount->account_name }}</td>
                                <td class="text-right">@number($credit->actual_balance)</td>
                                <td class="text-right">@number($credit->pending_balance)</td>
                                <td>@dateTimeFormat($credit->updated_at)</td>
                                <td>
                                    <a href="{{ route('admin.credit.edit', $credit) }}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{ route('admin.credit.logs', [
                                            'project_id' => $credit->client->project->id,
                                            'client_id' => $credit->client->id,
                                            'sub_account_id' => $credit->subAccount->id
                                        ]) }}" class=" btn btn-sm btn-warning">
                                        <i class="fa fa-book"></i>
                                    </a>
                                    <a href="{{ route('admin.credit.destroy', $credit) }}" title="Delete" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="text-center">
                    @if(isLengthAware($credits))
                        {!! $credits->appends($filters)->links() !!}
                    @else
                        <div class="alert alert-dark" role="alert">
                            Showing all items
                        </div>
                    @endif
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@stop
{{-- page level scripts --}}
