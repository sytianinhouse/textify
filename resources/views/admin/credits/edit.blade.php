@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.credit.index') }}">Credits</a>
    </li>
    <li class="active breadcrumb-item">Edit Credit</li>
@stop

{{-- Page content --}}
@section('content')
    
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
            </div>
        </div>
    </div>

    @include('templates.credits.form', [
        'credit' => $credit,
        'method' => 'put',
        'logs' => $logs
    ])
@stop

