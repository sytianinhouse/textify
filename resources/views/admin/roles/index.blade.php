@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    Projects
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.index') }}">Projects</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.index', $project) }}">Clients</a>
    </li>
    <li class="active breadcrumb-item">Roles</li>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.project.client.role.create', [$project, $client]) }}" class=" btn btn-success m-r-5">
                            Add Role <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- @include('partials.filters') --}}

    <div class="card">
        <div class="card-body" id="project_body">
            <div>
                <div>
                    <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Name</th>
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Description</th>
                            <th class="sorting wid-25" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                        </tr>
                        </thead>
                         <tbody>
                            @if( $roles )
                                @forelse($roles as $role)
                                <tr>
                                    <td>{{ $role->display_name }}</td>
                                    @php
                                        $string = substr( strip_tags(htmlspecialchars_decode($role->description)), 0, 30) . ' ...';
                                    @endphp
                                    <td>{{ $string }}</td>
                                    <td>
                                        <a href="{{ route('admin.project.client.role.edit', [$project, $client, $role]) }}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        @if( $role->users->count() == 0 )
                                            <a href="{{ route('admin.project.client.role.delete', [$project, $client,$role]) }}" title="Delete" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        @endif
                                    </td>
                                 </tr>
                                @empty
                                 <tr>
                                    <td colspan="6">No Records Found</td>
                                </tr>
                                @endforelse
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@stop
{{-- page level scripts --}}
