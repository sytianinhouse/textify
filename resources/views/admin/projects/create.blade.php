@extends('layouts.fixed_header_admin')

{{-- Page title --}}
@section('title')
    Create Project
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.index') }}">Projects</a>
    </li>
    <li class="breadcrumb-item active">Add Project</li>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
            </div>
        </div>
    </div>
   
    @include('templates.projects.form', [
        'project' =>  new \ATM\Repositories\Project\Project,
        'method' => 'post',
    ])
    

@stop
