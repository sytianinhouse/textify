@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    Projects
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item active">Projects</li>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.project.create') }}" class=" btn btn-success m-r-5">
                            Add Project&nbsp;&nbsp;<i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- @include('partials.filters') --}}

    <div class="card">
        <div class="card-body" id="project_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Logo</th>
                        <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Name</th>
                        <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Description</th>
                        <th class="sorting wid-20" tabindex="0" rowspan="1" colspan="1">Clients</th>
                        <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">Status</th>
                        <th class="sorting wid-10" tabindex="0" data-orderable="false" rowspan="1" colspan="1">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($projects as $proj)
                        <tr>
                            <td><img src="{{ asset($proj->img_logo_path) }}" alt="" style="max-width: 50px;"></td>
                            <td>{{ $proj->name }}</td>
                            @php
                                $string = substr( strip_tags(htmlspecialchars_decode($proj->description)), 0, 25) . ' ...';
                            @endphp
                            <td>{{ $string }}</td>
                            <td>
                                {{ $proj->clients->count() }}
                            </td>
                            <td>{!! $proj->active == 1 ? "<div class='active-display'>active</div>" : "<div class='inactive-display'>inactive</div>" !!}</td>
                            <td>
                                <a href="{{route('admin.project.edit', [$proj])}}" title="Edit" class="btn btn-sm btn-info">Edit</a>
                                <a href="{{route('admin.project.client.index', [$proj])}}" title="Clients" class="btn btn-sm btn-success">Clients</a>
                                <a href="{{route('admin.project.destroy', [$proj])}}" title="Edit" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6">No Records Found</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@stop
{{-- page level scripts --}}
