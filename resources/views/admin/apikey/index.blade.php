@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="breadcrumb-item"><a href="{{ route('admin.project.index') }}">Projects</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.project.client.index',[$project]) }}">Clients</a></li>
    <li class="breadcrumb-item">Gateway Keys</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row no-gutters">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.project.client.api.create', [$project, $client]) }}" data-subaccount-url="{{ route('admin.project.client.sub.index',[$project, $client]) }}" data-validate-subaccounts="{{ $subaccounts->count() }}" data-confirm-text="You don't have any sub Accounts yet, would you like to create one?" class=" btn btn-success m-r-5 campaignCreationCheckSubaccounts">
                            Add Gateway Key  <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- @include('partials.filters') --}}
    <div class="card">
        <div class="card-body" id="user_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer this-table" id="datatable" role="grid">
                    <thead>
                        <tr role="row">
                            {{-- <th class="sorting_asc wid-5" tabindex="0" rowspan="1" colspan="1">#</th> --}}
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Api Name</th>
                            {{--<th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Sub Account</th>--}}
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Api Key</th>
                            {{--<th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Description</th>--}}
                            <th class="sorting wid-25 text-center" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse( $apis as $theApi )
                            <tr>
                                {{-- <td>{{ $loop->iteration }}</td> --}}
                                <td>
                                    <a href="{{ route('admin.project.client.api.edit', [$project, $client,$theApi]) }}" title="Edit">
                                        {{ $theApi->name }}
                                    </a>
                                {{--<td>
                                    {{ $theApi->subaccounts->implode('account_name', ', ') }}
                                </td>--}}
                                <td>{{ $theApi->api_key }}</td>
                                {{--<td>{{ str_limit( $theApi->description, 100, "...." ) }}</td>--}}
                                <td class="text-center">
                                    <a href="{{ route('admin.project.client.api.edit', [$project, $client,$theApi]) }}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{ route('admin.project.client.api.delete', [$project, $client,$theApi]) }}" title="Delete" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9">No records found!</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- /.inner -->
    <!-- /.outer -->
@stop
