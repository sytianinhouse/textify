@extends('layouts.fixed_header_admin')

{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : '' }}
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.index') }}">Projects</a>
    </li>
    <li class="active breadcrumb-item">Clients</li>
@stop

{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? strtoupper($project->name)."'s ".$title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.project.client.create', $project) }}" class=" btn btn-success m-r-5">
                            Add Client&nbsp;&nbsp;<i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
  {{--   @include('partials.form-success') --}}
    {{-- @include('partials.filters') --}}

    <div class="card">
        <div class="card-body" id="client_body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover no-footer table_res datatable_with_expand_details table-data" id="datatable">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0">#</th>
                            <th class="sorting_asc" tabindex="0">Logo</th>
                            <th class="sorting_asc" tabindex="0">Company Name</th>
                            <th class="sorting" tabindex="0">Client ID</th>
                            <th class="sorting d-none" tabindex="0">Contact Person</th>
                            <th class="sorting d-none" tabindex="0">Email</th>
                            <th class="sorting d-none" tabindex="0">Phone</th>
                            <th class="sorting" tabindex="0">Mobile</th>
                            <th class="sorting wid-10" data-orderable="false" tabindex="0">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($clients as $client)
                        <tr class="get-data">
                            <td>{{ $loop->iteration }}</td>
                            <td><img src="{{ asset($client->img_thumb_path) }}" alt="" style="max-width: 50px;"></td>
                            <td><a href="{{ route('admin.project.client.edit', [$project, $client]) }}" class="name-anchor">{{ $client->company_name }}</a><a href="javascript:;" class="textify-more-details">More Details</a></td>
                            <td>{{ $client->cuid }}</td>
                            <td class="d-none" data-more-details="true" data-more-details-head-title="Company Name">{{ $client->company_name }}</td>
                            <td class="d-none" data-more-details="true" data-more-details-head-title="Email">{{ $client->contact_email }}</td>
                            <td class="d-none" data-more-details="true" data-more-details-head-title="Phone">{{ $client->contact_telephone }}</td>
                            <td>{{ $client->contact_mobile }}</td>
                            <td>
                                <a href="{{ route('admin.project.client.edit', [$project, $client]) }}" title="Edit" class="btn btn-sm btn-info"
                                    data-toggle="tooltip" data-placement="top" title="">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                                {{--<a href="{{ route('admin.project.client.api.index', [$project, $client ]) }}" title="Gateway Keys" class="btn btn-sm btn-info">Gateway Keys</a>--}}
                                <a href="{{ route('admin.project.client.sub.index', [$project, $client]) }}" title="Sub Accounts" class="btn btn-sm btn-info">Sub Accounts</a>
                                <a href="{{ route('admin.project.client.senderids.index', [$project, $client]) }}" title="Sub Accounts" class="btn btn-sm btn-info">Sender Ids</a>
                                <a href="{{ route('admin.project.client.role.index', [$project, $client]) }}" title="Role" class="btn btn-sm btn-info">Role</a>
                                <a href="{{ route('admin.project.client.users.index', [$project, $client]) }}" title="Users" class="btn btn-sm btn-info">Users</a>
                                <a href="{{ route('admin.project.client.customer.index', [$project, $client]) }}" title="Customer Group" class="btn btn-sm btn-info">Contacts</a>
                                <a href="{{ route('admin.project.client.destroy', [$project, $client]) }}" title="Delete" class="btn btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="9">No records found!</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    
    <!--<tr class="details">
        <td class="details" colspan="6">
            <ul>
                @if( $client->subaccounts->count() > 0 )
                    <li><span>Sub Accounts:</span>
                        <ul>
                            @foreach ($client->subaccounts as $item)
                                <li>
                                    <span><strong>{{ $item->account_name }}</strong></span>
                                    <span>Credits: {{ $item->credit->pending_balance }}</span>
                                    <span>Contacts: {{ $item->customers->count() }}</span>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endif
                @if( $client->customers->count() )
                    <li><span>Total Contacts:</span> {{ $client->customers->count() }}</li>
                @endif
                @if( $client->company_name )
                    <li><span>Email:</span> {{ $client->company_name }}</li>
                @endif
                @if( $client->contact_email )
                    <li><span>Phone:</span> {{ $client->contact_email }}</li>
                @endif
                @if( $client->contact_telephone )
                    <li><span>Contact Person:</span> {{ $client->contact_telephone }}</li>
                @endif
            </ul>
        </td>
    </tr>-->
        <!-- /.inner -->
    <!-- /.outer -->
@stop
{{-- page level scripts --}}
