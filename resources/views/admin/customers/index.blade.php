@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    Projects
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.index') }}">Projects</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.index',[$project]) }}">Clients</a>
    </li>
    <li class="active breadcrumb-item">Contacts</li>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="javascript:;" data-toggle="modal" data-target="#normal" class=" btn btn-primary glow_button m-r-5">
                            Excel Import&nbsp;&nbsp;<i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        </a>
                        <a href="{{ route('admin.project.client.customer-group.index', [$project, $client]) }}" class=" btn btn-primary glow_button m-r-5">Groups&nbsp;&nbsp;<i class="fa fa-users" aria-hidden="true"></i></a>
                        <a href="{{ route('admin.project.client.customer.create', [$project, $client]) }}" class=" btn btn-success m-r-5">Add Contact&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.filters',[
        'noDaterange' => true,
        'filterSearchAnyKey' => true,
        'filterSubAccount' => true
    ])

    <div class="card">
        <div class="card-body" id="project_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Full Name</th>
                        <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Mobile</th>
                        <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Gender</th>
                        <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">City</th>
                        <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Group</th>
                        <th class="sorting wid-25" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                    </tr>
                    </thead>
                     <tbody>
                        @if( $customers )
                            @forelse($customers as $custom )
                            <tr>
                                <td><a href="{{ route('admin.project.client.customer.edit', [$project, $client, $custom]) }}">{{ $custom->first_name .' '. $custom->last_name }}</a></td>
                                <td>{!! $custom->mobile !!}</td>
                                <td>{{ ucwords($custom->gender) }}</td>
                                <td>{{ isset($custom->city) ? ucwords($custom->city) : '' }}</td>
                                <td>
                                    @php
                                        $groupName = array();
                                        foreach ($custom->customerGroups as $customerGroup):
                                            $groupName[] = $customerGroup->name;
                                        endforeach;
                                        echo implode(", ",$groupName);
                                    @endphp
                                </td>
                                <td>
                                    <a href="{{ route('admin.project.client.customer.edit', [$project, $client, $custom]) }}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{ route('admin.project.client.customer.delete', [$project, $client,$custom]) }}" title="Delete" class="btn btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                             </tr>
                            @empty
                             <tr>
                                <td colspan="6">No Records Found</td>
                            </tr>
                            @endforelse
                        @endif
                    </tbody>
                </table>
                {{-- <div class="col-12 data_tables">
                    <div class="row">
                        <table id="customer_listing_wrapper" data-order='[[ 0, "ASC" ]]' class="table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                                <tr>
                                <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Full Name</th>
                                <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Mobile</th>
                                <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Email</th>
                                <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Sub Account</th>
                                <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Group</th>
                                <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if( $customers )
                                    @forelse($customers as $custom )
                                    <tr>
                                        <td><a href="{{ route('admin.project.client.customer.edit', [$project, $client, $custom]) }}">{{ $custom->last_name .', '. $custom->first_name }}</a></td>
                                        <td>{{ $custom->mobile }}</td>
                                        <td>{{ $custom->email }}</td>
                                        <td>{{ $custom->subAccounts->account_name }}</td>
                                        <td>
                                            @php
                                                $groupName = array();
                                                foreach ($custom->customerGroups as $customerGroup):
                                                    $groupName[] = $customerGroup->name;
                                                endforeach;
                                                echo implode(", ",$groupName);
                                            @endphp
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.project.client.customer.edit', [$project, $client, $custom]) }}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                            <a href="{{ route('admin.project.client.customer.delete', [$project, $client,$custom]) }}" title="Delete" class="btn btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                     </tr>
                                    @empty
                                     <tr>
                                        <td colspan="6">No Records Found</td>
                                    </tr>
                                    @endforelse
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div> --}}
                <div class="text-center">
                    @if(isLengthAware($customers))
                        {!! $customers->appends($filters)->links() !!}
                    @else
                        <div class="alert alert-dark" role="alert">
                            Showing all items
                        </div>
                    @endif
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    @include('templates.customers.modal-import', [
        'actionRoute' => route('admin.project.client.customer.import', [$project, $client]),
        'clientId' => $client->id
    ])

@stop
{{-- page level scripts --}}
