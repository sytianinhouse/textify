@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="breadcrumb-item">Profile</li>
@stop
{{-- Page content --}}
@section('content')
    @include('templates.profile.form', [
        'user'   => $user,
        'method' => 'POST',
    ])
@stop
