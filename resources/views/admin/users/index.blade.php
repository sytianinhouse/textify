@extends('layouts.fixed_header_admin')

{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : '' }}
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.index') }}">Projects</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.index', $project) }}">Clients</a>
    </li>
    <li class="breadcrumb-item">Users</li>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.project.client.users.create', [$project, $client]) }}" class=" btn btn-success m-r-5">
                            Add User <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

  {{--   @include('partials.form-success') --}}

    {{-- @include('partials.filters') --}}

    <div class="card">
        <div class="card-body m-t-35" id="user_body">
            <div id="sample_1_wrapper" class="dataTables_wrapper dt-boostrap no-footer">
                <div class="table-responsive">
                    <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Name</th>
                            <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">Username</th>
                            <th class="sorting wid-20" tabindex="0" rowspan="1" colspan="1">Acount Type</th>
                            <th class="sorting wid-20" tabindex="0" rowspan="1" colspan="1">Sub Account</th>
                            <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">Last Login</th>
                            <th class="sorting wid-10" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($users as $user)
                                @if( $user->id != $authUser->id )
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->username }}</td>
                                        <td>{{ ( $user->acct_type == 'Account User' ) ? 'Regular User' : $user->acct_type }}</td>
                                        <td>
                                            @foreach( $user->subAccounts as $sub )
                                                {!! "{$sub->account_name}<br>" !!}
                                            @endforeach
                                        </td>
                                        {{-- <td>{{ $user->roles }}</td> --}}
                                        <td>{{ \Carbon\Carbon::parse($user->last_login)->format('Y-m-d h:i a') }}</td>
                                        <td>
                                            <a href="{{route('admin.project.client.users.edit', [$project, $client, $user])}}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                            <a href="{{route('admin.project.client.users.destroy', [$project, $client, $user])}}" title="Delete" class="btn btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            {{-- @include('partials.inline-delete', [
                                                'action' => route('admin.client.users.destroy', [$client, $user]),
                                            ]) --}}
                                        </td>
                                    </tr>
                                @endif
                            @empty
                            <tr>
                                <td colspan="6">No Records Found</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

@stop
