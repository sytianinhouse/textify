@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    Projects
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
   {{--  <li class="breadcrumb-item">
        <a href="{{ route('admin.project.index') }}">Projects</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.project.client.index', $project) }}">Clients</a>
    </li> --}}
    <li class="active breadcrumb-item">Loads</li>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.load.create') }}" class=" btn btn-success m-r-5">
                            Add Load <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- @include('partials.filters', [
        'filterProject' => true
    ]) --}}

    <div class="card">
        <div class="card-body" id="load_body">
            <div id="sample_1_wrapper" class="dataTables_wrapper dt-boostrap no-footer">
                <div class="table-responsive">
                    <table data-order='[[ 3, "DESC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                        <thead>
                            <tr role="row">
                                <th class="sorting">Project</th>
                                <th class="sorting_asc">Client</th>
                                <th class="sorting">Sub Account</th>
                                <th class="sorting">Date</th>
                                <th class="sorting text-right">Amount</th>
                                <th class="sorting" data-orderable="false">Conversion</th>
                                {{-- <th class="sorting" data-orderable="false">Actions</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($loads as $load)
                                <tr>
                                    <td>{{ ( $load->client ) ? $load->client->project->name : '' }}</td>
                                    <td>{{ ( $load->client ) ? $load->client->company_name : '' }}</td>
                                    <td>{{ $load->subAccount->account_name }}</td>
                                    <td data-order="{{ $load->date }}">@dateFormat($load->date)</td>
                                    <td class="text-right">@number($load->load_amount)</td>
                                    @php
                                        $string = substr( strip_tags(htmlspecialchars_decode($load->notes)), 0, 30) . ' ...';
                                    @endphp
                                    <td>{{ $load->cost }}</td>
                                    {{-- <td>
                                        <a href="{{ route('admin.load.destroy', $load) }}" title="Delete" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        @if(isLengthAware($loads))
                            {!! $loads->appends($query)->links() !!}
                        @else
                            <div class="alert alert-dark" role="alert">
                                Showing all items
                            </div>
                        @endif
                        {{-- {{ $loads->links() }} --}}
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@stop
{{-- page level scripts --}}
