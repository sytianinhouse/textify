@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="breadcrumb-item"><a href="{{ route('admin.project.index') }}">Projects</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.project.client.index',[$project]) }}">Clients</a></li>
    <li class="breadcrumb-item">Sender Ids</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row no-gutters">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route( 'admin.project.client.senderids.create',[$project,$client] ) }}" class=" btn btn-success m-r-5">
                            Add Sender Ids  <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body" id="user_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer this-table" id="datatable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Sender Id Name</th>
                            <th class="sorting wid-25 text-left" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse( $senderIds as $senderid )
                            <tr>
                                <td>
                                <a href="{{ route('admin.project.client.senderids.edit', [$project, $client,$senderid]) }}" title="Edit">
                                    {{ $senderid->name }}
                                </a>
                                <td class="text-left">
                                    <a href="{{ route('admin.project.client.senderids.edit', [$project, $client,$senderid]) }}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{ route('admin.project.client.senderids.destroy', [$project, $client,$senderid]) }}" title="Delete" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9">No records found!</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <br><br><br>
    <h4>Credits Consumed</h4>
    <ul class="list-group">
        @foreach($group as $year => $jobs)
        <li class="list-group-item">
            Year {{ $year }} = {{ number_format($jobs->sum('credits_consumed')) }}
        </li>
        @endforeach
    </ul>
    <!-- /.inner -->
    <!-- /.outer -->
@stop
