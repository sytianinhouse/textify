@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="breadcrumb-item"><a href="{{ route('admin.project.index') }}">Projects</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.project.client.index',[$project]) }}">Clients</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.project.client.senderids.index', [$project, $client]) }}">Sender Ids</a></li>
    <li class="breadcrumb-item">Edit Sender Id - {{ucwords($senderids->name) }}</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row no-gutters">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                {{-- <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.user.create') }}" class=" btn btn-success m-r-5">
                            Add Entity  <i class="fa fa-plus"></i>
                        </a>
                        <a href="{{ route('admin.user.create') }}" class=" btn btn-primary">
                            Print  <i class="fa fa-print"></i>
                        </a>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    @include('templates.senderids.form', [
        'senderids' => $senderids,
        'method' => 'put',
    ])
@stop
