@extends('layouts.fixed_header_admin')
{{-- Page title --}}
@section('title')
    {{-- {{ isset($title) ? $title : 'Title goes here' }} --}}
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/select2/css/select2.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datepicker/css/bootstrap-datepicker.min.css')}}" />
    <!--End of plugin styles-->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}"/>
    <!-- end of page level styles -->
@stop
{{-- Page content --}}
@section('content')
    
    <div class="row">
        <ol class="breadcrumb float-right  nav_breadcrumb_top_align">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.index') }}">
                    <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.project.index') }}">Projects</a>
            </li>
            <li class="active breadcrumb-item">Clients</li>
        </ol>
    </div>

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <!-- <h3 class="m-b-0">
                        {{-- {{ isset($title) ? $title : 'Page Header Title' }} --}}
                    </h3> -->
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{-- {{ route('admin.user.create') }} --}}" class=" btn btn-success m-r-5">
                            Add Entity  <i class="fa fa-plus"></i>
                        </a>
                        <a href="{{-- {{ route('admin.user.create') }} --}}" class=" btn btn-primary">
                            Print  <i class="fa fa-print"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.filters')
    
    <div class="card">
        <div class="card-body" id="user_body">
            <div>
                <div>
                    <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Name</th>
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Email</th>
                            <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">Username</th>
                            <th class="sorting wid-20" tabindex="0" rowspan="1" colspan="1">Acount Type</th>
                            {{-- <th class="sorting wid-15" tabindex="0" rowspan="1" colspan="1">Roles</th> --}}
                            <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">Last Login</th>
                            <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>Test Test</td>
                                <td>email@test.com</td>
                                <td>test</td>
                                <td>Client</td>
                                {{-- <td>{{ $user->roles }}</td> --}}
                                <td>{{ \carbon\carbon::now()->toDateTimeString() }}</td>
                                <td>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    {{-- @include('partials.inline-delete', [
                                        'action' => route('admin.client.users.destroy', [$client, $user]),
                                    ]) --}}
                                </td>
                            </tr>
                            <tr>
                                <td>aTest Test</td>
                                <td>aemail@test.com</td>
                                <td>atest</td>
                                <td>aClient</td>
                                {{-- <td>a{{ $user->roles }}</td> --}}
                                <td>{{ \carbon\carbon::now()->toDateTimeString() }}</td>
                                <td>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    {{-- @include('partials.inline-delete', [
                                        'action' => route('admin.client.users.destroy', [$client, $user]),
                                    ]) --}}
                                </td>
                            </tr>
                            <tr>
                                <td>bTest Test</td>
                                <td>bemail@test.com</td>
                                <td>btest</td>
                                <td>bClient</td>
                                {{-- <td>a{{ $user->roles }}</td> --}}
                                <td>{{ \carbon\carbon::now()->toDateTimeString() }}</td>
                                <td>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    {{-- @include('partials.inline-delete', [
                                        'action' => route('admin.client.users.destroy', [$client, $user]),
                                    ]) --}}
                                </td>
                            </tr>
                            <tr>
                                <td>Test Test</td>
                                <td>email@test.com</td>
                                <td>test</td>
                                <td>Account User</td>
                                {{-- <td>{{ $user->roles }}</td> --}}
                                <td>{{ \carbon\carbon::now()->toDateTimeString() }}</td>
                                <td>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    {{-- @include('partials.inline-delete', [
                                        'action' => route('admin.client.users.destroy', [$client, $user]),
                                    ]) --}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- /.inner -->
    <!-- /.outer -->
@stop
{{-- page level scripts --}}
@section('footer_scripts')
    <!--Plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/vendors/select2/js/select2.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.colVis.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.html5.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.print.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/moment/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js"></script>
    <!--End of plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/users.js')}}"></script>
    <!-- end page level scripts -->
@stop
