@extends('layouts.fixed_header_admin')

{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="#">Test</a>
    </li>
    <li class="active breadcrumb-item">Report Template</li>
@stop

@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{-- {{route('admin.project.client.index', [$project]') }} --}}" class=" btn btn-primary">
                            <i class="fa fa-file-excel-o"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.filters')
    
    @if($begin)
        <div class="card">
            <div class="card-body" id="user_body">
                <div>
                    <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Name</th>
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Email</th>
                            <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">Username</th>
                            <th class="sorting wid-20" tabindex="0" rowspan="1" colspan="1">Acount Type</th>
                            {{-- <th class="sorting wid-15" tabindex="0" rowspan="1" colspan="1">Roles</th> --}}
                            <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">Last Login</th>
                            <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>Test Test</td>
                                <td>email@test.com</td>
                                <td>test</td>
                                <td>Client</td>
                                {{-- <td>{{ $user->roles }}</td> --}}
                                <td>{{ \carbon\carbon::now()->toDateTimeString() }}</td>
                                <td>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    {{-- @include('partials.inline-delete', [
                                        'action' => route('admin.client.users.destroy', [$client, $user]),
                                    ]) --}}
                                </td>
                            </tr>
                            <tr>
                                <td>aTest Test</td>
                                <td>aemail@test.com</td>
                                <td>atest</td>
                                <td>aClient</td>
                                {{-- <td>a{{ $user->roles }}</td> --}}
                                <td>{{ \carbon\carbon::now()->toDateTimeString() }}</td>
                                <td>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    {{-- @include('partials.inline-delete', [
                                        'action' => route('admin.client.users.destroy', [$client, $user]),
                                    ]) --}}
                                </td>
                            </tr>
                            <tr>
                                <td>bTest Test</td>
                                <td>bemail@test.com</td>
                                <td>btest</td>
                                <td>bClient</td>
                                {{-- <td>a{{ $user->roles }}</td> --}}
                                <td>{{ \carbon\carbon::now()->toDateTimeString() }}</td>
                                <td>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    {{-- @include('partials.inline-delete', [
                                        'action' => route('admin.client.users.destroy', [$client, $user]),
                                    ]) --}}
                                </td>
                            </tr>
                            <tr>
                                <td>Test Test</td>
                                <td>email@test.com</td>
                                <td>test</td>
                                <td>Account User</td>
                                {{-- <td>{{ $user->roles }}</td> --}}
                                <td>{{ \carbon\carbon::now()->toDateTimeString() }}</td>
                                <td>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{-- {{route('admin.user.edit', [$user])}} --}}" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    {{-- @include('partials.inline-delete', [
                                        'action' => route('admin.client.users.destroy', [$client, $user]),
                                    ]) --}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    @else
        <div class="card m-t-5">
            <div class="card-body text-center">
                <div class="m-t-10 m-b-10">
                    <h4>@lang('descriptions.reports.introduction')</h4>
                </div>
            </div>
        </div>
    @endif

@stop
{{-- page level scripts --}}

