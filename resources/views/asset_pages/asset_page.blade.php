@extends('layouts.fixed_header_admin')

{{-- Page title --}}
@section('title')
    {{-- {{ $title ?: 'Title goes here' }} --}}
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <!-- plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>

    {{-- Picker Styles --}}
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/inputlimiter/css/jquery.inputlimiter.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jquery-tagsinput/css/jquery.tagsinput.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/daterangepicker/css/daterangepicker.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datepicker/css/bootstrap-datepicker.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datetimepicker/css/DateTimePicker.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/j_timepicker/css/jquery.timepicker.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/clockpicker/css/jquery-clockpicker.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/colorpicker_hack.css')}}" />
    {{-- end of Picker Styles --}}
    <!--end of page level css-->
    <style>
        .br-0{
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }
    </style>
@stop

{{-- Page content --}}
@section('content')

    <header class="head">

        <div class="main-bar">
            <div class="row">
                <div class="col-lg-6">
                    <h4 class="nav_top_align">
                        <i class="fa fa-cog"></i>
                        Asset Page
                    </h4>
                </div>
                <div class="col-lg-6">
                    <div class="float-right">
                        <ol class="breadcrumb nav_breadcrumb_top_align">
                            <li class="breadcrumb-item">
                                <a href="index">
                                    <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                    Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Users</a>
                            </li>
                            <li class="breadcrumb-item active">Create User</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="outer">
        <div class="inner bg-container">

            <div class="row">
                <div class="col">
                    <div class="card form_elements_datepicker">
                        <div class="card-header bg-white">
                            Date Picker
                        </div>
                        <div class="card-body" id="datePickerBlock">
                            <div class="row">
                                <div class="col-lg input_field_sections">
                                    <h5>Format specified via options</h5>
                                    <form>
                                        <input type="text" class="form-control" placeholder="dd-mm-yyyy"
                                               id="dp1">
                                    </form>
                                </div>
                                <div class="col-lg input_field_sections">
                                    <h5>Format specified via data tag</h5>
                                    <form>
                                        <input type="text" class="form-control" placeholder="dd/mm/yyyy"
                                               data-date-format="dd/mm/yyyy" id="dp2">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row m-t-15">
                <div class="col">
                    <div class="card form_elements_datepicker">
                        <div class="card-header bg-white">
                            Date Range Picker
                        </div>
                        <div class="card-body" id="datePickerBlock">
                            <div class="row">
                                <div class="col-lg input_field_sections">
                                    <h5>Input initially empty</h5>
                                    <form>
                                        <div class="input-group input-group-prepend">
                                                <span class="input-group-text border-right-0 rounded-left rounded_right">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            <input type="text" class="form-control" id="date_range"
                                                   name="date range">
                                        </div>
                                    </form>
                                </div>
                                <div class="col-lg-6 input_field_sections">
                                    <h5>Format specified via data tag</h5>
                                    <form>
                                        <div class="input-group input-group-prepend">
                                                <span class="input-group-text border-right-0 rounded-left rounded_right">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            <input type="text" name="reservation" id="reservation"
                                                   class="form-control"  data-date-format="mm-dd-yyyy" value="01-10-2016 - 02-10-2016">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row m-t-15">
                <div class="col">
                    <div class="card form_elements_datepicker">
                        <div class="card-header bg-white">
                            Time Picker
                        </div>
                        <div class="card-body" id="datePickerBlock">
                            <div class="row">
                                <div class="col-lg-6 input_field_sections">
                                    <h5>Auto Close</h5>
                                    <form>
                                        <div class="input-group clockpicker2 input-group-append" data-align="top"
                                             data-placement="top" data-autoclose="true">
                                            <input type="text" class="form-control" value="15:14">
                                            <span class="input-group-text border-left-0 rounded-right add-on">
                                                    <i class="fa fa-clock-o"></i>
                                                </span>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-lg-6 input_field_sections">
                                    <h5>Default Clockpicker</h5>
                                    <form>
                                        <div class="input-group clockpicker1 input-group-append">
                                            <input type="text" class="form-control" data-placement="top"
                                                   data-align="top" value="10:30">
                                            <span class="input-group-text add-on border-left-0 rounded-right">
                                                    <i class="fa fa-clock-o"></i>
                                                </span>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-lg-6 input_field_sections">
                                    <h5>Set Time</h5>
                                    <form>
                                        <div class="input-group">
                                            <input id="setTimeExample" type="text"
                                                   class="time ui-timepicker-input form-control"
                                                   autocomplete="off">
                                            <span class="input-group-append">
                                                <button class="btn btn-primary" id="setTimeButton" type="button">Set current time</button>
                                                </span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             <div class="row m-t-15">
                <div class="col">
                    <div class="card form_elements_datepicker">
                        <div class="card-header bg-white">
                            Color Picker
                        </div>
                        <div class="card-body" id="datePickerBlock">
                            <div class="row">
                                <div class="col-lg-6 col-12 input_field_sections">
                                    <h5>Picker with hexa format</h5>
                                    <form>
                                        <input type="text" class="form-control" placeholder="#8fff00" id="cp1">
                                    </form>
                                </div>
                                <div class="col-lg-6 col-12 input_field_sections">
                                    <h5>Picker with the rgba format</h5>
                                    <form>
                                        <input type="text" class="form-control" placeholder="rgb(0,194,255,1)"
                                               id="cp-2" data-color-format="rgba">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row m-t-15">
                <div class="col">
                    <div class="card">
                        <div class="card-body m-t-35">
                            <div>
                                <h4>Inputs</h4>
                            </div>
                            {!! Form::open(['route' => 'admin.index', 'method' => 'post', 'class' => 'form-horizontal login_validator', 'id' => 'create_user_form']) !!}

                                {!! Form::token() !!}

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group row m-t-25">
                                            <div class="col-lg-3 text-lg-right">
                                                {!! Form::label('first_name', 'First Name', ['class' => 'col-form-label']) !!}
                                            </div>
                                            <div class="col-xl-6 col-lg-8">
                                                <div class="input-group input-group-prepend">
                                                    <span class="input-group-text br-0 border-right-0 rounded-left">
                                                        <i class="fa fa-user text-default"></i>
                                                    </span>
                                                    {!! Form::text('first_name', null, ['class' => 'form-control', 'id' => 'first_name']) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row m-t-25">
                                            <div class="col-lg-3 text-lg-right">
                                                {!! Form::label('last_name', 'Last Name', ['class' => 'col-form-label']) !!}
                                            </div>
                                            <div class="col-xl-6 col-lg-8">
                                                <div class="input-group input-group-prepend">
                                                    <span class="input-group-text br-0 border-right-0 rounded-left">
                                                        <i class="fa fa-user text-default"></i>
                                                    </span>
                                                    {!! Form::text('last_name', null, ['class' => 'form-control', 'id' => 'last_name']) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row m-t-25">
                                            <div class="col-lg-3 text-lg-right">
                                                {!! Form::label('email', 'Email', ['class' => 'col-form-label']) !!}
                                            </div>
                                            <div class="col-xl-6 col-lg-8">
                                                <div class="input-group input-group-prepend">
                                                    <span class="input-group-text br-0 border-right-0 rounded-left">
                                                        <i class="fa fa-envelope text-default"></i></span>
                                                    </span>
                                                    {!! Form::text('email', null, ['class' => 'form-control', 'id' => 'email']) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row m-t-25">
                                            <div class="col-lg-3 text-lg-right">
                                                {!! Form::label('telephone', 'Telephone', ['class' => 'col-form-label']) !!}
                                            </div>
                                            <div class="col-xl-6 col-lg-8">
                                                <div class="input-group input-group-prepend">
                                                    <span class="input-group-text br-0 border-right-0 rounded-left">
                                                        <i class="fa fa-phone text-default"></i></span>
                                                    </span>
                                                    {!! Form::number('telephone', null, ['class' => 'form-control', 'id' => 'telephone']) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row m-t-25">
                                            <div class="col-lg-3 text-lg-right">
                                                {!! Form::label('mobile_no', 'Mobile #', ['class' => 'col-form-label']) !!}
                                            </div>
                                            <div class="col-xl-6 col-lg-8">
                                                <div class="input-group input-group-prepend">
                                                    <span class="input-group-text br-0 border-right-0 rounded-left">
                                                        <i class="fa fa-mobile text-default"></i></span>
                                                    </span>
                                                    {!! Form::number('mobile_no', null, ['class' => 'form-control', 'id' => 'mobile_no']) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row m-t-25">
                                            <div class="col-lg-3 text-lg-right">
                                                {!! Form::label('account_type', 'Account Type', ['class' => 'col-form-label']) !!}
                                            </div>
                                            <div class="col-xl-6 col-lg-8">
                                                <div class="input-group input-group-prepend">
                                                    <span class="input-group-text br-0 border-right-0 rounded-left">
                                                        <i class="fa fa-user text-default"></i>
                                                    </span>
                                                    {!! Form::select('account_type', ['1' => 'First', '2' => 'Second', '3' => 'Third'], null, ['class' => 'form-control', 'id' => 'account_type']) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row m-t-25">
                                            <div class="col-lg-3 text-lg-right">
                                                {!! Form::label('username', 'Username', ['class' => 'col-form-label']) !!}
                                            </div>
                                            <div class="col-xl-6 col-lg-8">
                                                <div class="input-group input-group-prepend">
                                                    <span class="input-group-text br-0 border-right-0 rounded-left">
                                                        <i class="fa fa-user text-default"></i>
                                                    </span>
                                                    {!! Form::text('username', null, ['class' => 'form-control', 'id' => 'username']) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-lg-3 text-lg-right">
                                                {!! Form::label('password', 'Password', ['class' => 'col-form-label']) !!}
                                            </div>
                                            <div class="col-xl-6 col-lg-8">
                                                <div class="input-group input-group-prepend">
                                                    <span class="input-group-text br-0 border-right-0 rounded-left"><i class="fa fa-lock text-default"></i></span>
                                                    {!! Form::password('password', ['class' => 'form-control', 'id' => 'password']) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-lg-3 text-lg-right">
                                                {!! Form::label('password_confirmation', 'Confirm Password', ['class' => 'col-form-label']) !!}
                                            </div>
                                            <div class="col-xl-6 col-lg-8">
                                                <div class="input-group input-group-prepend">
                                                    <span class="input-group-text br-0 border-right-0 rounded-left"><i class="fa fa-lock text-default"></i></span>
                                                    {!! Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'password_confirmation']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- </form> --}}
                                 {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row m-t-15">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <div class="m-t-25">
                                <h4>Buttons</h4>
                            </div>
                            <div class="col">
                                <div class="button_section_align text-center">
                                    <h5>Dropdown Buttons</h5>
                                    <div class="row">
                                        <div class="col m-t-10">
                                            <div class="dropdown">
                                                <button class="btn btn-secondary dropdown-toggle"
                                                        type="button" id="about-us1" data-toggle="dropdown"
                                                        aria-haspopup="true" aria-expanded="false">
                                                    Secondary
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="about-us1">
                                                    <a class="dropdown-item" href="#">Action</a>
                                                    <a class="dropdown-item" href="#">Another action</a>
                                                    <a class="dropdown-item" href="#">Something else
                                                        here</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col m-t-10">
                                            <div class="dropdown">
                                                <button class="btn btn-info dropdown-toggle" type="button"
                                                        id="about-us2" data-toggle="dropdown"
                                                        aria-haspopup="true" aria-expanded="false">
                                                    info
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="about-us2">
                                                    <a class="dropdown-item" href="#">Action</a>
                                                    <a class="dropdown-item" href="#">Another action</a>
                                                    <a class="dropdown-item" href="#">Something else
                                                        here</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col m-t-10">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle"
                                                        type="button" id="about-us" data-toggle="dropdown"
                                                        aria-haspopup="true" aria-expanded="false">
                                                    primary
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="about-us">
                                                    <a class="dropdown-item" href="#">Action</a>
                                                    <a class="dropdown-item" href="#">Another action</a>
                                                    <a class="dropdown-item" href="#">Something else
                                                        here</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h5 class="m-t-15">Glow Buttons</h5>
                                    <div class="row">
                                        <div class="col m-t-15">
                                            <button class="btn btn-secondary glow_button">Button
                                            </button>
                                        </div>
                                        <div class="col m-t-15">
                                            <button class="btn btn-primary glow_button">Button
                                            </button>
                                        </div>
                                        <div class="col m-t-15">
                                            <button class="btn btn-success glow_button">Button
                                            </button>
                                        </div>
                                        <div class="col m-t-15">
                                            <button class="btn btn-warning glow_button">Button
                                            </button>
                                        </div>
                                        <div class="col m-t-15">
                                            <button class="btn btn-info glow_button">Button</button>
                                        </div>
                                        <div class="col m-t-15">
                                            <button class="btn btn-danger glow_button">Button
                                            </button>
                                        </div>


                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- /.inner -->
    </div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pluginjs/jasny-bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/holderjs/js/holder.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/jquery.uniform/js/jquery.uniform.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/inputlimiter/js/jquery.inputlimiter.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/jquery-tagsinput/js/jquery.tagsinput.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pluginjs/jquery.validVal.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/inputmask/js/jquery.inputmask.bundle.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/moment/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/autosize/js/jquery.autosize.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/jasny-bootstrap/js/inputmask.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datetimepicker/js/DateTimePicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/j_timepicker/js/jquery.timepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/clockpicker/js/jquery-clockpicker.min.js')}}"></script>
    <!--end of plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/js/form.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pages/datetime_piker.js')}}"></script>
    <!-- end of plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/validation.js')}}"></script>
    <!-- end of page level js -->
@stop
