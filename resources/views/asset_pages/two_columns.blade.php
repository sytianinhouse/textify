@extends('layouts.fixed_header_admin')

{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <!-- plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/sweetalert/css/sweetalert2.min.css')}}"/>

    <!--end of page level css-->
    <style>
        .br-0{
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }
    </style>
    @parent
@stop

{{-- Page content --}}
@section('content')

    <div class="row">
        <ol class="breadcrumb float-right  nav_breadcrumb_top_align">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.index') }}">
                    <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.project.index') }}">Projects</a>
            </li>
            <li class="active breadcrumb-item">Clients</li>
        </ol>
    </div>

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                {{-- <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.user.create') }}" class=" btn btn-success m-r-5">
                            Add Entity  <i class="fa fa-plus"></i>
                        </a>
                        <a href="{{ route('admin.user.create') }}" class=" btn btn-primary">
                            Print  <i class="fa fa-print"></i>
                        </a>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>

    {{-- Should use @include for FORMS --}}
    <div class="row">
        <div class="col-sm-9 p-l-0">
            <div class="card">
                <div class="card-header">
                    
                        Create User Sample
                   
                </div>
                <div class="card-body">
                    {{-- from template/users/form.blade.php --}}
                    {!! Form::open(['method' => 'post', 'class' => 'form-horizontal login_validator', 'id' => 'user_form', 'enctype' => "multipart/form-data"]) !!}
                    
                        {{-- all form errors --}}
                        @include('partials.form-errors')
                        {{-- end of form errors --}}
                        <div class="row">
                            <div class="col-12">
                                
                                <div class="form-group row">
                                    <div class="col">
                                        {!! Form::label('first_name', 'First Name', ['class' => 'col-form-label']) !!}
                                        <div class="col p-d-0">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-text br-0 border-right-0 rounded-left">
                                                    <i class="fa fa-user text-primary"></i>
                                                </span>

                                                {{-- {!! Form::text('first_name', null, ['class' => 'form-control', 'id' => 'first_name']) !!} --}}

                                                <input type="text" name="first_name" class="form-control required input-error" id="first_name" value="{{ isset($client) && !empty($client->first_name) ? $client->first_name : '' }}">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col">
                                        
                                        {!! Form::label('last_name', 'Last Name', ['class' => 'col-form-label']) !!}
                                        
                                        <div class="col p-d-0">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-text br-0 border-right-0 rounded-left">
                                                    <i class="fa fa-user text-primary"></i>
                                                </span>
                                                {{-- {!! Form::text('last_name', null, ['class' => 'form-control', 'id' => 'last_name']) !!} --}}

                                                <input type="text" name="last_name" class="form-control required {{ $errors->has('last_name') ? 'input-error' : '' }}" id="last_name" value="{{ isset($client) && !empty($client->last_name) ? $client->last_name : '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row m-t-25">
                                    <div class="col-sm-4">
                                        
                                        {!! Form::label('email', 'Email', ['class' => 'col-form-label']) !!}
                                        
                                        <div class="col p-d-0">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-text br-0 border-right-0 rounded-left">
                                                    <i class="fa fa-envelope text-primary"></i></span>
                                                </span>
                                                {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        
                                        {!! Form::label('telephone', 'Telephone', ['class' => 'col-form-label']) !!}
                                        
                                        <div class="col p-d-0">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-text br-0 border-right-0 rounded-left">
                                                    <i class="fa fa-phone text-primary"></i></span>
                                                </span>
                                                {!! Form::number('telephone', null, ['class' => 'form-control', 'id' => 'telephone']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        
                                        {!! Form::label('mobile_no', 'Mobile #', ['class' => 'col-form-label']) !!}
                                        
                                        <div class="col p-d-0">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-text br-0 border-right-0 rounded-left">
                                                    <i class="fa fa-mobile text-primary"></i></span>
                                                </span>
                                                {!! Form::number('mobile_no', null, ['class' => 'form-control', 'id' => 'mobile_no']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row m-t-25">
                                    <div class="col">
                                        
                                        {!! Form::label('username', 'Username', ['class' => 'col-form-label']) !!}
                                        
                                        <div class="col p-d-0">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-text br-0 border-right-0 rounded-left">
                                                    <i class="fa fa-user text-primary"></i>
                                                </span>
                                                {!! Form::text('username', null, ['class' => 'form-control', 'id' => 'username']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        
                                        {!! Form::label('account_type', 'Account Type', ['class' => 'col-form-label']) !!}
                                        
                                        <div class="col p-d-0">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-text br-0 border-right-0 rounded-left">
                                                    <i class="fa fa-user text-primary"></i>
                                                </span>
                                                {!! Form::select('account_type', ['1' => 'First', '2' => 'Second', '3' => 'Third'], null, ['class' => 'form-control', 'id' => 'account_type']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <div class="col">
                                        
                                        {!! Form::label('password', 'Password', ['class' => 'col-form-label']) !!}
                                        
                                        <div class="col p-d-0">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-text br-0 border-right-0 rounded-left"><i class="fa fa-lock text-primary"></i></span>
                                                {!! Form::password('password', ['class' => 'form-control', 'id' => 'password']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        
                                        {!! Form::label('password_confirmation', 'Confirm Password', ['class' => 'col-form-label']) !!}
                                        
                                        <div class="col p-d-0">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-text br-0 border-right-0 rounded-left"><i class="fa fa-lock text-primary"></i></span>
                                                {!! Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'password_confirmation']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- <div class="form-group row">
                                    <div class="col-lg-9 ml-auto">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-user"></i>
                                            Add User
                                        </button>
                                        <button class="btn btn-warning" type="reset" id="clear">
                                            <i class="fa fa-refresh"></i>
                                            Reset
                                        </button>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                        {{-- </form> --}}
                    {{-- end of template/users/form.blade.php --}}
                </div> 
            </div>
        </div>

        {{-- start of saving column or COLUMN 2 --}}
        @include('partials.admin.saving', [
            'text' => isset($client) && $client->exists ? 'Update Client' : 'Add Client',
            'activation' => true,
            'withImage' => true,
            'imageName' => 'Profile Picture',
        ])
        {{-- end of saving column or COLUMN 2 --}}
                        {!! Form::close() !!}
    </div>
    {{-- End of Should use @include for FORMS --}}


@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pluginjs/jasny-bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/holderjs/js/holder.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <!-- end of plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/validation.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/sweetalert/js/sweetalert2.min.js')}}"></script>
    <!-- end of page level js -->
@stop
