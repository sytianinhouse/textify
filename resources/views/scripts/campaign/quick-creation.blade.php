<script>
	let id = null;
    @if(isset($multipleSubAccount) && $multipleSubAccount)
    $('body').on('click', '.btn-quick-create-campaign', function(e) {
    	id = $(this).data('id');
    	$('#create-quick-campaign').modal('show');
    });
    @endif
    $('body').on('click','#save_campaign', function (e) {
		e.preventDefault();
		var _self = $(this);
		var URL = '{{ route('api.create.quick-campaign') }}';
		$.ajax({
			url: URL,
			type:"POST",
            data: {
                name : $('input[name="campaign_name"]').val(),
                sub_account_id : id ? id : '{{ $subaccountId }}',
            },
			beforeSend: function (xhr) {
				_self.addClass('loading');
				var TOKEN = $('meta[name="csrf-token"]').attr('content');
				if (TOKEN) {
					return xhr.setRequestHeader('X-CSRF-TOKEN', TOKEN);
				}
				alert(TOKEN);
			},
			success:function(response){
				if( response ) {
					_self.removeClass('loading');
					$('input[name="campaign_name"]').val('');
					$('#create-quick-campaign').modal('toggle');
					if (id) {
						$('#campaignoptions-' + id + '.campaign-options').html(response);
                    } else {
						$('.campaign-options').html(response);
					}
					$('.alert-campaign .close').click();
					$('input[name="input_submit_task"]').attr('disabled',false);
					$('input[name="input_send_task"]').attr('disabled',false);
				}
			},
		});
	});
</script>
