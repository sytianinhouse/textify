@php
    // dd($charLimit);
@endphp
<script>
	var climiter = {{ $charLimit ? $charLimit : 0 }};

	var creditCount = 1;
	var isGroup = $('.textarea-group').length;

	// Applicable for text blast only
	function computeTotalCost() {
		if ($('#allIDs').val()) {
			let idsCount = parseInt($('#allIDs').val()),
                creditCount = parseInt($('#ecreditc').val());

			$('#total_cost').text(idsCount * creditCount);
        }
    }
	$('body').on('change', '#allIDs', function() { computeTotalCost(); });

	if (isGroup > 0) {
		$('.messagearea').each(function() {
			let $this = $(this);
			var parent = $(this).closest('.textarea-group');

			parent.find('.creditNum').text(Math.round(creditCount));
			parent.find('.charNum').text($this.val().length);

			$this.keyup(function() {
				var creditCount = 1;
				var len = $(this).val().length;
				parent.find('.charNum').text(len).change();
				parent.find('.echarc').val(len).change();

				if (len >= climiter) {
					creditCount = len / climiter;
					creditCount = Math.ceil(creditCount);
				}

				parent.find('.creditNum').text(creditCount);
				parent.find('.ecreditc').val(creditCount);
			});
		})
    } else {
		$('#creditNum').text(Math.round(creditCount));
		$('#charNum').text($('#message').val().length);

		$( "#message" ).keyup(function() {
			var creditCount = 1;
			var len = $(this).val().length;
			var parent = $(this).closest('.textarea-group');
			$('#charNum').text(len).change();
			$('#echarc').val(len).change();

			if (len >= climiter) {
				creditCount = len / climiter;
				creditCount = Math.ceil(creditCount);
			}

			$('#creditNum').text(creditCount);
			$('#ecreditc').val(creditCount);
			computeTotalCost();
		});
	}
</script>
