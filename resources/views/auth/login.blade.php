<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" href="{{asset('assets/img/logo1.ico')}}"/>
    <!--Global styles -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/components.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/custom.css')}}" />

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png') }}">
    <!--End of Global styles -->
    <!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>
    <!--End of Plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/login3.css')}}"/>

    <style type="text/css">
        .login-logo {
            max-width: 30%;
            height:auto;
        }
    </style>
</head>
<body style="background-color: #4e4c4c;">
<div class="preloader" style=" position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 100000;
  backface-visibility: hidden;
  background: #ffffff;">
    <div class="preloader_img" style="width: 200px;
  height: 200px;
  position: absolute;
  left: 48%;
  top: 48%;
  background-position: center;
z-index: 999999">
        <img src="{{asset('assets/img/loader.gif')}}"  style=" width: 40px;" alt="loading...">
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-12 mx-auto login_section">
            <div class="row">
                <div class=" col-lg-4 col-md-8 col-sm-12  mx-auto login2_border login_section_top">
                    <div class="login_logo login_border_radius1">
                        <h3 class="text-center text-white">
                            <img src="{{asset('images/textify-logo-only-white.png')}}" class="login-logo" alt="logo"><br /> <br />
                            <span class="m-t-15">LOG IN</span>
                        </h3>
                    </div>

                    @if ($errors->count() > 0)
                        <div class="text-center">
                            <span class="help-block text-danger">
                                <strong>{!! $errors->first() !!}</strong>
                            </span>
                        </div>
                    @endif
                    <div>
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                            <div class="form-group {{ $errors->has('cuid') ? ' has-error' : '' }}">
                                <label for="cuid" class="col-form-label text-white">Client ID</label>
                                <div class="input-group">
                                    <input id="cuid" type="text" class="form-control b_r_20 {{ $errors->has('email') ? ' is-invalid' : '' }}" name="cuid" value="{{ old('cuid') ?: resolve('request')->cookie('cuid') }}" required autofocus placeholder="Client">
                                    <span class="input-group-text bl-0">
                                        <i class="fa fa-user text-white"></i>
                                    </span>
                                </div>
                                {{-- @if ($errors->has('cuid'))
                                    <span class="help-block">
                                        <strong>{{ $errors->get('cuid') }}</strong>
                                    </span>
                                @endif --}}
                            </div>

                            <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                <label for="username" class="col-form-label text-white">Username</label>
                                <div class="input-group">
                                    <input id="username" type="text" class="form-control b_r_20 {{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required  placeholder="Username">
                                    <span class="input-group-text bl-0">
                                        <i class="fa fa-user text-white"></i>
                                    </span>
                                </div>

                                {{-- @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->get('username') }}</strong>
                                    </span>
                                @endif --}}
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-form-label text-white">Password</label>
                                <div class="input-group">
                                    <input type="password" class="form-control b_r_20" id="password" name="password" placeholder="Password">
                                    <span class="input-group-text  bl-0">
                                        <i class="fa fa-key text-white"></i>
                                    </span>
                                </div>

                                {{-- @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->get('password') }}</strong>
                                    </span>
                                @endif --}}
                            </div>

                            <div class="row">
                                <div class="col-6 m-t-10">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input form-control">
                                        <span class="custom-control-label"></span>
                                        <a class="text-white">Keep me logged in</a>
                                    </label>
                                </div>
                                {{-- <div class="col-6 p-l-0 m-t-10">
                                    <div class="float-right">
                                        <a href="forgot_password3" class="forgottxt_clr text-white">Forgot password ?</a>
                                    </div>
                                </div> --}}
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success btn-block b_r_20 m-t-20 m-b-20">LOG IN</button>
                                    </div>
                                </div>
                            </div>
                    </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- global js -->
<script type="text/javascript" src="{{asset('assets/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<!-- end of global js-->
<!--Plugin js-->
<script type="text/javascript" src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
{{-- <script type="text/javascript" src="{{asset('assets/vendors/jquery.backstretch/js/jquery.backstretch.js')}}"></script> --}}
<!--End of plugin js-->
<script type="text/javascript" src="{{asset('assets/js/pages/login3.js')}}"></script>
{{-- <script type="text/javascript">
    $.backstretch("images/garfield-interior.jpg");
</script> --}}
</body>

</html>
