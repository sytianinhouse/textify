<div class="row">
    <div class="col-12 col-sm-9">
        <div class="card">
            <div class="card-header">
                Credit Details
            </div>
            {{-- {{ dd(isset($credit) && !empty($credit->client)) }} --}}
            <div class="card-body">
                {!! Form::model($credit, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'credit_form', 'autocomplete' => 'off']) !!}
                <div class="row">
                    <div class="col-12">
                        <h5>Client Name: <strong>{{ isset($credit) && !empty($credit->client) ? $credit->client->company_name : '' }}</strong></h5>
                        <h5>Sub Account: <strong>{{ isset($credit) && !empty($credit->subAccount) ? $credit->subAccount->account_name : '' }}</strong></h5>
                    </div>
                    <div class="col-6">
                        <div class="form-group row">
                            <div class="col required">
                                {!! Form::label('actual_balance', 'Actual Balance', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                        </span>
                                        @php
                                            $class = $errors->has('actual_balance') ? 'form-control required input-error' : 'form-control required';
                                        @endphp
                                        {!! Form::number('actual_balance', null, ['class' => $class, 'id' => 'actual_balance', 'autocomplete' => 'off', 'required']) !!}
                                    </div>
                                </div>
                                @if($errors->has('actual_balance'))
                                    <span class="has-error">{{ $errors->first('actual_balance') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row">
                            <div class="col required">
                                {!! Form::label('pending_balance', 'Pending Balance', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                        </span>
                                        @php
                                            $class = $errors->has('pending_balance') ? 'form-control required input-error' : 'form-control required';
                                        @endphp
                                        {!! Form::number('pending_balance', null, ['class' => $class, 'id' => 'pending_balance', 'autocomplete' => 'off', 'required']) !!}
                                    </div>
                                </div>
                                @if($errors->has('pending_balance'))
                                    <span class="has-error">{{ $errors->first('pending_balance') }}</span>
                                @endif
                            </div>

                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <div class="col required">
                                {!! Form::label('notes', 'Notes', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                        </span>
                                        @php $classString = $errors->has('notes') ? 'form-control required input-error' : 'form-control required'; @endphp
                                        {!! Form::textarea('notes', null, ['class' => $classString, 'id' => 'notes', 'rows' => '3', 'autocomplete' => 'new-notes', 'required']) !!}
                                        {{-- <textarea type="text" name="notes" class="form-control {{ $errors->has('notes') ? 'input-error' : '' }}" id="notes"  rows="3">
                                            {{ isset($load) && !empty($load->notes) ? $load->notes : '' }}
                                        </textarea> --}}
                                    </div>
                                </div>
                                @if($errors->has('notes'))
                                    <span class="has-error">{{ $errors->first('notes') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.admin.saving', [
        'text' => $credit->exists ? 'Update Credit' : 'Add Credit',
        'activation' => false,
        'withImage' => false,
        'imageName' => 'Logo',
        'entity' => isset($load) ? $load : '',
        'imgPath' =>  isset($load) && $load->exists ? asset($load->img_logo_path) : asset(\ATM\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER),

    ])

    {!! Form::close() !!}
</div>
