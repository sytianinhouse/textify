<div class="row">
    <div class="col-12 col-sm-9">
        <div class="card">
            <div class="card-header">
                Project Details
            </div>
            <div class="card-body">
                {!! Form::model($project, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'client_form', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
                <div class="row">
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col required">
                                {!! Form::label('name', 'Project Name', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-text-width" aria-hidden="true"></i>
                                        </span>
                                        <input type="text" name="name" class="form-control required {{ $errors->has('name') ? 'input-error' : '' }}" required id="name" value="{{ isset($project) && !empty($project->name) ? $project->name : '' }}" autocomplete="new-name">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                {!! Form::label('description', 'Description', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                        </span>
                                        {!! Form::textarea('description', isset($project->description) ? $project->description : null, ['class' => 'form-control', 'id' => 'description', 'autocomplete' => 'new-description']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.admin.saving', [
        'text' => isset($project->exists) && $project->exists ? 'Update Project' : 'Add Project',
        'activation' => true,
        'withImage' => true,
        'imageName' => 'Logo',
        'entity' => isset($project) ? $project : '',
        'imgPath' =>  isset($project) && $project->exists ? asset($project->img_logo_path) : asset(\ATM\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER),
    ])
    {!! Form::close() !!}
</div>
