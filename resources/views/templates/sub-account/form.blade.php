<div class="row">
    <div class="col">
        {!! Form::model($subAccount, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'sub_account_form', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
            <div class="row">
                <div class="col-12 col-lg-9">
                    {{-- @include('partials.form-errors')
                    @include('partials.form-success') --}}
                    <div class="card">
                        <div class="card-header bg-black">
                            Primary Details
                        </div>
                        <div class="card-body">
                            <div class="row m-t-25">
                                @if($subAccount->exists)
                                <div class="col-12">
                                    <p class="">* Security Key: <strong>{{ $subAccount->security_key }}</strong>
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Use for API service calls."></i>
                                    </p>
                                </div>
                                @endif
                                <div class="col-12 col-lg-6 required">
                                    <div class="form-group required">
                                        {!! Form::label('account_name', 'Account Name', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-user text-default"></i>
                                            </span>
                                            <input type="text" name="account_name" class="form-control required {{ $errors->has('account_name') ? 'input-error' : '' }}" id="account_name" value="{{ isset($subAccount) && !empty($subAccount->account_name) ? $subAccount->account_name : $client_company_name.' - ' }}"  autocomplete="new-account-name" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('website', 'Website', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-globe text-default"></i>
                                            </span>
                                            <input type="text" name="website" class="form-control {{ $errors->has('website') ? 'input-error' : '' }}" id="website" value="{{ isset($subAccount) && !empty($subAccount->website) ? $subAccount->website : $client->website }}" autocomplete="new-website">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-10">
                                <div class="col-12">
                                    <div class="form-group">
                                        {!! Form::label('address_1', 'Address Line 1', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-home text-default"></i>
                                            </span>
                                            <input type="text" name="address_1" class="form-control {{ $errors->has('address_1') ? 'input-error' : '' }}" id="address_1" value="{{ isset($subAccount) && !empty($subAccount->address_1) ? $subAccount->address_1 : $client->address_1 }}"  autocomplete="new-address-1">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        {!! Form::label('address_2', 'Address Line 2', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-home text-default"></i>
                                            </span>
                                            <input type="text" name="address_2" class="form-control {{ $errors->has('address_2') ? 'input-error' : '' }}" id="address_2" value="{{ isset($subAccount) && !empty($subAccount->address_2) ? $subAccount->address_2 : $client->address_2 }}"  autocomplete="new-address-2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-10">
                                <div class="col col-lg-4">
                                    <div class="form-group">
                                        {!! Form::label('city', 'City', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-building text-default"></i>
                                            </span>
                                            @php $errorcities = $errors->has('city') ? 'form-control chzn-select input-error' : 'form-control chzn-select'; @endphp
                                            {!! Form::select('city', $cities + ['Select City' => 'Select City'], ( $subAccount->city ) ? $client->city : 'Select City', [
                                                'class' => $errorcities,
                                                'id' => 'city',
                                                'tabindex' => 2
                                            ]) !!}
                                        </div>
                                        {{-- {!! Form::label('city', 'City', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-building text-default"></i>
                                            </span>
                                            <input type="text" name="city" class="form-control {{ $errors->has('city') ? 'input-error' : '' }}" id="city" value="{{ isset($subAccount) && !empty($subAccount->city) ? $subAccount->city : $client->city }}"  autocomplete="new-city">
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="col col-lg-4">
                                    <div class="form-group">
                                        {!! Form::label('country', 'Country', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-flag text-default"></i>
                                            </span>
                                            <select class="form-control" id="country" name="country">
                                                @foreach($countries as $key => $country)
                                                    <option value="{{ $key }}" {{ (!empty($currentCountry) ? ( $currentCountry == $key ? 'selected' : '' ) : (($key == $cCode) ? 'selected' : '')) }}>{{ $country }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-lg-4">
                                    <div class="form-group">
                                        {!! Form::label('zip_code', 'Zip Code', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-map-pin text-default"></i>
                                            </span>
                                            <input type="text" name="zip_code" class="form-control {{ $errors->has('zip_code') ? 'input-error' : '' }}" id="zip_code" value="{{ isset($subAccount) && !empty($subAccount->zip_code) ? $subAccount->zip_code : $client->zip }}"  autocomplete="new-zip">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-10">
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('telephone', 'Phone', ['class' => 'col-form-label']) !!}
                                        <div class="input-group">
                                            <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                                <i class="fa fa-phone text-default"></i>
                                            </span>
                                            <input type="text" name="telephone" class="phone phone-instructions form-control {{ $errors->has('telephone') ? 'input-error' : '' }}" id="telephone" data-inputmask="'mask': '(02) 000-0000'" data-mask value="{{ isset($subAccount) && !empty($subAccount->telephone) ? $subAccount->telephone : $client->phone }}"  autocomplete="new-telephone">
                                            <span class="input-group-text input-group-append br-0 rounded-right">
                                                (02) 000-0000
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('mobile_phone', 'Mobile', ['class' => 'col-form-label']) !!}
                                        <div class="input-group">
                                            <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                                <i class="fa fa-mobile text-default"></i>
                                            </span>
                                            <input type="text" name="mobile_phone" class="mobile-phone form-control {{ $errors->has('mobile_phone') ? 'input-error' : '' }}" id="mobile_phone" data-inputmask="'mask': '9999-999-9999'" data-mask value="{{ isset($subAccount) && !empty($subAccount->mobile_phone) ? $subAccount->mobile_phone : $client->mobile }}"  autocomplete="new-mobile">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card m-t-15">
                        <div class="card-header bg-black">
                            Contact Details
                        </div>
                        <div class="card-body">
                            <div class="row m-t-25">
                                <div class="col-12 col-lg-6">
                                    <div class="form-group required">
                                        {!! Form::label('contact_firstname', 'First Name', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-user text-default"></i>
                                            </span>
                                            <input type="text" name="contact_firstname" class="form-control required {{ $errors->has('contact_firstname') ? 'input-error' : '' }}" id="contact_firstname" value="{{ isset($subAccount) && !empty($subAccount->contact_firstname) ? $subAccount->contact_firstname : $client->contact_first_name }}"  autocomplete="new-contact-fname" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('contact_lastname', 'Last Name', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-user text-default"></i>
                                            </span>
                                            <input type="text" name="contact_lastname" class="form-control {{ $errors->has('contact_lastname') ? 'input-error' : '' }}" id="contact_lastname" value="{{ isset($subAccount) && !empty($subAccount->contact_lastname) ? $subAccount->contact_lastname : $client->contact_last_name }}"  autocomplete="new-contact-lname">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-10">
                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        {!! Form::label('contact_email', 'Email', ['class' => 'col-form-label']) !!}
                                        <div class="input-group">
                                            <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                                <i class="fa fa-envelope text-default"></i>
                                            </span>
                                            <input type="text" name="contact_email" class="form-control {{ $errors->has('contact_email') ? 'input-error' : '' }}" id="contact_email" value="{{ isset($subAccount) && !empty($subAccount->contact_email) ? $subAccount->contact_email : $client->contact_email }}"  autocomplete="new-contact-email">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        {!! Form::label('contact_telephone', 'Phone', ['class' => 'col-form-label']) !!}
                                        <div class="input-group">
                                            <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                                <i class="fa fa-phone text-default"></i>
                                            </span>
                                            <input type="text" name="contact_telephone" class="phone phone-instructions form-control {{ $errors->has('contact_telephone') ? 'input-error' : '' }}" id="contact_telephone" data-inputmask="'mask': '(02) 000-0000'" data-mask value="{{ isset($subAccount) && !empty($subAccount->contact_telephone) ? $subAccount->contact_telephone : $client->contact_telephone }}"  autocomplete="new-contact-telephone">
                                            <span class="input-group-text input-group-append br-0 rounded-right">
                                                (02) 000-0000
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        {!! Form::label('contact_mobile_phone', 'Mobile', ['class' => 'col-form-label']) !!}
                                        <div class="input-group">
                                            <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                                <i class="fa fa-mobile text-default"></i>
                                            </span>
                                            <input type="text" name="contact_mobile_phone" class="mobile-phone form-control {{ $errors->has('contact_mobile_phone') ? 'input-error' : '' }}" id="contact_mobile_phone" data-inputmask="'mask': '+63 (999)-999-9999'" data-mask value="{{ isset($subAccount) && !empty($subAccount->contact_mobile_phone) ? $subAccount->contact_mobile_phone : $client->contact_mobile }}"  autocomplete="new-contact-mobile">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-3 p-l-0">
                    <div class="card" id="sticky">
                        @if(isset($allSenderids) && isset($apiKeys))
                        <div class="card-body">
                            @if( isset($allSenderids) && $allSenderids->count() >= 1 )
                                <div class="form-group row">
                                    <div class="col">
                                        <label for="api_key">
                                            Sender Id
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="This will show as the sender of each text."></i>
                                        </label>
                                        <div class="input-group">
                                            <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                                <i class="fa fa-key text-default"></i>
                                            </span>
                                            @php
                                                $errorapiKeys = 'form-control required';
                                                $errorapiKeys .= $errors->has('sender_id') ? 'input-error' : '';
                                            @endphp
                                            {!! Form::select('sender_id', $allSenderids, $subaccount->sender_id ?: null, ['class' => $errorapiKeys, 'id' => 'sender_id', 'placeholder' => '']) !!}
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if( isset($apiKeys) && $apiKeys->count() >= 1 )
                            <div class="form-group row">
                                <div class="col required">
                                    <label for="api_key">
                                        SMS Gateway
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Gateway to use for all SMS Task."></i>
                                    </label>
                                    <div class="input-group">
                                        <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                            <i class="fa fa-key text-default"></i>
                                        </span>
                                        @php
                                            $errorapiKeys = 'form-control required';
                                            $errorapiKeys .= $errors->has('api_key') ? 'input-error' : '';
                                        @endphp
                                        {!! Form::select('api_key', $apiKeys, $subaccount->apis->first() ? $subaccount->apis->first()->id : '', ['class' => $errorapiKeys, 'id' => 'api_keys', 'placeholder' => '', 'required']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col required">
                                    <label for="api_key">
                                        Credit Alert Level
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Provides notification once the credit alert level has been reached."></i>
                                    </label>
                                    <div class="input-group">
                                        <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                            <i class="fa fa-bell text-default"></i>
                                        </span>
                                        @php $errorapiKeys = $errors->has('credit_alert_level') ? 'form-control api-keys chzn-select input-error' : 'form-control chzn-select api-keys'; @endphp
                                        <input type="text" name="credit_alert_level" class="form-control required {{ $errors->has('credit_alert_level') ? 'input-error' : '' }}" id="credit_alert_level" value="{{ isset($subAccount) && !empty($subAccount->credit_alert_level) ? $subAccount->credit_alert_level : '100' }}" required autocomplete="new-credit-alert-level">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('active', 1, (isset($subAccount) && !$subAccount->exists) ? true : null, array('class' => 'custom-control-input')) !!}
                                        <span class="cr custom-control-indicator custom_checkbox_default"><i class="cr-icon fa fa-check text-success"></i></span>
                                        Activate
                                    </label>
                                </div>
                            </div>
                            @endif
                            <div class="form-group row">
                                <div class="col text-center text-lg-left">
                                    <label for="api_key">
                                        Thumbnail
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Sub Account thumbnail."></i>
                                    </label>
                                    <div class="alert alert-warning alert-dismissable">
                                        <strong>Info!</strong>
                                        Recommended resolution: 320 x 200
                                    </div>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        @php
                                            $imgPath = isset($subAccount) && $subAccount->exists ? asset($subAccount->img_thumb_path) : asset(\ATM\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER);
                                        @endphp
                                        <div class="fileinput-new img-thumbnail text-center">
                                            <img src="{{ $imgPath }}"  id="myImage"  alt="not found" style="width: 100%;">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists img-thumbnail"></div>
                                        <div class="m-t-10 text-center">
                                            <span class="btn btn-primary btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="thumb">
                                            </span>
                                            <a href="#" class="btn btn-warning fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col text-center text-lg-left">
                                    <label for="api_key">
                                        Thumbnail
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Sub Account thumbnail."></i>
                                    </label>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        @php
                                            $imgPath = isset($subAccount) && $subAccount->exists ? asset($subAccount->img_thumb_path) : (isset($subAccount->client->img_thumb_path) ? asset($subAccount->client->img_thumb_path) : asset(\ATM\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER));
                                        @endphp
                                        <div class="fileinput-new img-thumbnail text-center">
                                            <img src="{{ $imgPath }}"  id="myImage"  alt="not found" style="width: 100%;">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists img-thumbnail"></div>
                                        <div class="m-t-10 text-center">
                                            <span class="btn btn-primary btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="thumb">
                                            </span>
                                            <a href="#" class="btn btn-warning fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="card-footer">
                            @include('partials.admin.saving', [
                                'ownLayout' => true,
                                'text' => isset($subAccount) && $subAccount->exists ? 'Update Sub Account' : 'Add Sub Account'
                            ])
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
