<div class="row">
    <div class="col">
        {!! Form::model($subAccount, ['method' => $method, 'class' => 'form-horizontal', 'id' => 'sub_account_form', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
            <div class="row">
                <div class="col-12 col-lg-9">
                    {{-- @include('partials.form-errors')
                    @include('partials.form-success') --}}
                    <div class="card">
                        <div class="card-header bg-black">
                            General Settings
                        </div>
                        <div class="card-body">
                            <div class="row m-t-10">
                                <div class="col-4">
                                    <div class="checkbox centered-text">
                                        <label>
                                            {!! Form::checkbox('settings['.ATM\Repositories\SubAccount\SubAccountSetting::IS_BETA.']', 1, null, array('class' => 'custom-control-input')) !!}
                                            <span class="cr custom-control-indicator custom_checkbox_default"><i class="cr-icon fa fa-check text-success"></i></span>
                                            This account is Beta user.
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card m-t-20">
                        <div class="card-header bg-black">
                            Credits Settings
                        </div>
                        <div class="card-body">
                            <div class="row m-t-10">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="hour">Credit cost per SMS</label>
                                        {!! Form::number('settings['.ATM\Repositories\SubAccount\SubAccountSetting::SMS_RATE_PER_TEXT.']', 1,
                                            ['class' => 'form-control', 'step '=> "0.01"]) !!}
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="form-group">
                                        <label for="hour">Recipient for Balance SMS Notification (If multiple use comma to separate)</label>
                                        {!! Form::text('settings['.ATM\Repositories\SubAccount\SubAccountSetting::CREDIT_BALANCE_NOTIF_RECIPIENT.']', null,
                                            ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-3 p-l-0">
                    <div class="card" id="sticky">
                        <div class="card-body">
                            @include('partials.admin.saving', [
                                'ownLayout' => true,
                                'text' => 'Update Settings',
                            ])
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
