<div class="row">
    <div class="col-12 col-sm-9">
        <div class="card">
            <div class="card-header">
                Personal Details
            </div>
            <div class="card-body">
                {!! Form::model($user, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'user_form', 'enctype' => "multipart/form-data"]) !!}
                    {!! Form::token() !!}

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group row">
                                <div class="col-sm-6 required">
                                    {!! Form::label('first_name', 'First Name', ['class' => 'col-form-label', 'autocomplete' => 'off']) !!}
                                    <div class="col p-d-0">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-user text-default"></i>
                                            </span>
                                            <input type="text" name="first_name" class="form-control required {{ $errors->has('first_name') ? 'input-error' : '' }}" id="first_name" value="{{ isset($user) && !empty($user->first_name) ? $user->first_name : old('first_name') }}"  autocomplete="new-fname" required>
                                        </div>
                                    </div>
                                    @if($errors->has('first_name'))
                                        <span class="has-error">{{ $errors->first('first_name') }}</span>
                                    @endif
                                </div>
                                <div class="col-sm-6 required">
                                    {!! Form::label('last_name', 'Last Name', ['class' => 'col-form-label']) !!}
                                    <div class="col p-d-0">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-user text-default"></i>
                                            </span>
                                            <input type="text" name="last_name" class="form-control required {{ $errors->has('last_name') ? 'input-error' : '' }}" id="last_name" value="{{ isset($user) && !empty($user->last_name) ? $user->last_name : old('last_name') }}"  autocomplete="new-lname" required>
                                        </div>
                                    </div>
                                    @if($errors->has('last_name'))
                                        <span class="has-error">{{ $errors->first('last_name') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row m-t-15">
                                <div class="col">
                                    {!! Form::label('email', 'Email', ['class' => 'col-form-label']) !!}
                                    <div class="col p-d-0">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-envelope text-default"></i></span>
                                            </span>
                                            <input type="text" name="email" class="form-control required {{ $errors->has('email') ? 'input-error' : '' }}" id="email" value="{{ isset($user) && !empty($user->email) ? $user->email : old('email') }}"  autocomplete="new-email">
                                        </div>
                                    </div>
                                    @if($errors->has('email'))
                                        <span class="has-error">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="col">
                                    {!! Form::label('telephone', 'Phone', ['class' => 'col-form-label']) !!}

                                    <div class="col p-d-0">
                                        <div class="input-group">
                                            <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                                <i class="fa fa-phone text-default"></i>
                                            </span>
                                            <input class="phone phone-instructions form-control {{ $errors->has('telephone') ? 'input-error' : '' }}" id="telephone" name="telephone" type="text" data-inputmask="'mask': '(02) 000-0000'" data-mask value="{{ isset($user) && !empty($user->telephone) ? $user->telephone : old('telephone') }}" autocomplete="new-telephone">
                                            <span class="input-group-text input-group-append br-0 rounded-right">
                                                (02) 000-0000
                                            </span>
                                        </div>
                                    </div>
                                    @if($errors->has('telephone'))
                                        <span class="has-error">{{ $errors->first('telephone') }}</span>
                                    @endif
                                </div>
                                <div class="col">
                                    {!! Form::label('mobile_no', 'Mobile', ['class' => 'col-form-label']) !!}

                                    <div class="col p-d-0">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                                <i class="fa fa-mobile text-default"></i>
                                            </span>
                                            <input class="mobile-phone form-control {{ $errors->has('mobile_no') ? 'input-error' : '' }}" id="mobile_no" name="mobile_no" type="text" value="{{ isset($user) && !empty($user->mobile_no) ? $user->mobile_no : old('mobile_no') }}"  autocomplete="new-mobile">
                                        </div>
                                    </div>
                                    @if($errors->has('mobile_no'))
                                        <span class="has-error">{{ $errors->first('mobile_no') }}</span>
                                    @endif
                                </div>
                            </div>

                            @if(isset($defaultAccountType) && !$defaultAccountType)
                                <div class="form-group row m-t-15">
                                    <div class="col-sm-4 required position-relative">
                                        {!! Form::label('account_type', 'Account Type', ['class' => 'col-form-label']) !!}
                                        <div class="col p-d-0">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-addon input-group-text custom-divide">
                                                    <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                                </span>
                                                @php $errorAccountType = $errors->has('account_type') ? 'form-control chzn-select input-error' : 'form-control chzn-select'; @endphp
                                                {!! Form::select('account_type', $accountTypes, null, [
                                                    'class' => $errorAccountType,
                                                    'id' => 'account_type',
                                                    'tabindex' => 2,
                                                    'required'
                                                ]) !!}
                                            </div>
                                        </div>
                                        @if($errors->has('account_type'))
                                            <span class="has-error">{{ $errors->first('account_type') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 required position-relative" id="sub-account-select">

                                        {!! Form::label('sub_account', 'Sub Account', ['class' => 'col-form-label']) !!}
                                        <div class="col-sm-12 p-d-0">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-addon custom-divide">
                                                    <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                                </span>
                                                @php $errorSub = $errors->has('sub_account') ? 'form-control chzn-select input-error' : 'form-control chzn-select'; @endphp
                                                {!! Form::select(
                                                    'sub_account[]',
                                                    $subAccounts,
                                                    $user->subAccounts->pluck('id')->all(),
                                                    [
                                                        'class' => $errorSub,
                                                        'id' => 'sub_account',
                                                        'multiple',
                                                        'required'
                                                    ])
                                                !!}
                                            </div>
                                        </div>
                                        @if($errors->has('sub_account'))
                                            <span class="has-error">{{ $errors->first('sub_account') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 required position-relative" id="role-select">

                                        {!! Form::label('roles', 'Roles', ['class' => 'col-form-label']) !!}
                                        <div class="col-sm-12 p-d-0">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-addon custom-divide">
                                                    <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                                </span>
                                                @php $errorRoles = $errors->has('roles') ? 'form-control chzn-select input-error' : 'form-control chzn-select'; @endphp
                                                {!! Form::select(
                                                    'roles[]',
                                                    $roles,
                                                    $user->roles->pluck('id')->all(),
                                                    [
                                                        'class' => $errorRoles,
                                                        'id' => 'roles',
                                                        'multiple',
                                                        'required'
                                                    ])
                                                !!}
                                            </div>
                                        </div>
                                        @if($errors->has('roles'))
                                            <span class="has-error">{{ $errors->first('roles') }}</span>
                                        @endif
                                    </div>
                                </div>
                            @else
                                <div class="form-group row m-t-15">
                                    <div class="col-sm-4 required position-relative" hidden>
                                        {!! Form::label('account_type', 'Account Type', ['class' => 'col-form-label']) !!}
                                        <div class="col p-d-0">
                                            <div class="input-group input-group-prepend">
                                                @php $errorAccountType = $errors->has('account_type') ? 'form-control chzn-select input-error' : 'form-control chzn-select'; @endphp

                                                {!! Form::select('account_type', $accountTypes, $defaultAccountType, [
                                                    'class' => $errorAccountType,
                                                    'id' => 'account_type',
                                                    'tabindex' => 2,
                                                    'required'
                                                    ]) !!}
                                            </div>
                                        </div>
                                        @if($errors->has('account_type'))
                                            <span class="has-error">{{ $errors->first('account_type') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 required position-relative" id="sub-account-select">

                                        {!! Form::label('sub_account', 'Sub Account', ['class' => 'col-form-label']) !!}
                                        <div class="col-sm-12 p-d-0">
                                            <div class="input-group input-group-prepend">
                                                @php $errorSub = $errors->has('sub_account') ? 'form-control chzn-select input-error' : 'form-control chzn-select'; @endphp
                                                <span class="input-group-addon custom-divide">
                                                    <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                                </span>
                                                {!! Form::select('sub_account[]',
                                                    $subAccounts,
                                                    $user->subAccounts->pluck('id')->all(), [
                                                    'class' => $errorSub,
                                                    'id' => 'sub_account',
                                                    'multiple',
                                                    ]) !!}
                                            </div>
                                        </div>
                                        @if($errors->has('sub_account'))
                                            <span class="has-error">{{ $errors->first('sub_account') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 position-relative" id="role-select">

                                        {!! Form::label('roles', 'Roles', ['class' => 'col-form-label']) !!}
                                        <div class="col-sm-12 p-d-0">
                                            <div class="input-group input-group-prepend">
                                                @php $errorRoles = $errors->has('roles') ? 'form-control chzn-select input-error' : 'form-control chzn-select'; @endphp
                                                <span class="input-group-addon custom-divide">
                                                    <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                                </span>
                                                {!! Form::select('roles[]', $roles, $user->roles->pluck('id')->all(), [
                                                    'class' => $errorRoles,
                                                    'id' => 'roles',
                                                    'multiple',
                                                    ]) !!}
                                            </div>
                                        </div>
                                        @if($errors->has('roles'))
                                            <span class="has-error">{{ $errors->first('roles') }}</span>
                                        @endif
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row m-t-15">
                                <div class="col required">
                                    {!! Form::label('username', 'Username', ['class' => 'col-form-label']) !!}
                                    <div class="col p-d-0">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-user text-default"></i>
                                            </span>
                                            <input type="text" name="username" class="form-control required {{ $errors->has('username') ? 'input-error' : '' }}" id="username" value="{{ isset($user) && !empty($user->username) ? $user->username : old('username') }}"  autocomplete="new-username" required>
                                        </div>
                                    </div>
                                    @if($errors->has('username'))
                                        <span class="has-error">{{ $errors->first('username') }}</span>
                                    @endif
                                </div>

                                <div class="col required">
                                    {!! Form::label('password', 'Password', ['class' => 'col-form-label']) !!}
                                    <div class="col p-d-0">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-lock text-default"></i>
                                            </span>
                                            <input type="password" name="password" class="form-control required {{ $errors->has('password') ? 'input-error' : '' }}"  autocomplete="new-password">
                                        </div>
                                    </div>
                                    @if($errors->has('password'))
                                        <span class="has-error">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>

                                <div class="col required">
                                    {!! Form::label('password_confirmation', 'Confirm Password', ['class' => 'col-form-label']) !!}
                                    <div class="col p-d-0">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-lock text-default"></i>
                                            </span>
                                            <input type="password" name="password_confirmation" class="form-control required {{ $errors->has('password_confirmation') ? 'input-error' : '' }}"  autocomplete="new-password-confirm">
                                        </div>
                                    </div>
                                    @if($errors->has('password_confirmation'))
                                        <span class="has-error">{{ $errors->first('password_confirmation') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>


    @include('partials.admin.saving', [
        'text' => isset($user) && $user->exists ? 'Update User' : 'Add User',
        'activation' => true,
        'withImage' => true,
        'imageName' => 'Profile Picture',
        'entity' => isset($user) ? $user : '',
        'imgPath' =>  isset($user) && $user->exists ? asset($user->img_thumb_path) : asset(\ATM\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER),
    ])

    {!! Form::close() !!}
</div>

@section('footer_scripts')
    <script>

    $(function() {

        $("#account_type").change(function() {
            if ($(this).val() == 'account_user') {
                // $("#branches").show()
                $("#role-select").show();
                $("#roles_chosen").show();
                $("#roles_chosen").css('width', '90%');
                // $("#roles").attr('required', 'required');
                setTimeout(function() {
                    $("#roles").removeAttr('required');
                }, 200);

                $("#sub-account-select").show();
                $("#sub_accounts_chosen").show();
                $("#sub_accounts_chosen").css('width', '100%');
                $("#sub_account").attr('required', 'required');
            } else {
                // $("#branches").hide()
                $("#role-select").hide();
                $("#roles_chosen").hide();
                // $("#roles").removeAttr('required');

                $("#sub-account-select").hide();
                $("#sub_accounts_chosen").hide();
                $("#sub_account").removeAttr('required');
            }
        }).change();
    })

    </script>
@stop
