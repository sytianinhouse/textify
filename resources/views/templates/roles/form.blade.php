<div class="row">
    <div class="col-12 col-sm-9">
        <div class="card">
            <div class="card-header">
                Role Details
            </div>
            <div class="card-body">
                {!! Form::model($role, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'client_form', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
                <div class="row">
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col required">
                                {!! Form::label('name', 'Name', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-text-width" aria-hidden="true"></i>
                                        </span>
                                        <input type="text" name="name" class="form-control required {{ $errors->has('name') ? 'input-error' : '' }}" id="name" value="{{ isset($role) && !empty($role->name) ? $role->display_name : '' }}" autocomplete="new-name" required>
                                    </div>
                                </div>
                                @if($errors->has('name'))
                                    <span class="has-error">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                {!! Form::label('description', 'Description', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                        </span>
                                        {!! Form::textarea('description', isset($role->description) ? $role->description : null, ['class' => 'form-control role-description', 'id' => 'description', 'autocomplete' => 'new-description']) !!}
                                    </div>
                                </div>
                                @if($errors->has('description'))
                                    <span class="has-error">{{ $errors->first('description') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if( $permissions )
            <div class="card m-t-15">
                <div class="card-header">
                    Permissions
                </div>
                <div class="card-body cs-permissions">
                    <div class="form-group row">
                        @php
                            $counter = 0;
                        @endphp
                        @foreach($permissions as $key => $permission)
                            <div class="col col-md-6">
                                <h3>{{ $key }}</h3>
                                <hr style="margin:8px 0;">
                                <ul class="list-unstyled parent-ul nested-ul-checkbox">
                                    <li>
                                        {!! Form::checkbox('parent_permission','permission-all', true, ['class' => 'custom-checkbox parent-permission', 'id' => 'all-permission-'.$counter]) !!}
                                        <label for="all-permission-{{$counter}}" unselectable="on">Toggle All</label>
                                        <ul>
                                            @php
                                                $ctr = 0;
                                            @endphp
                                            @foreach($permission as $item)
                                                <li>
                                                    {!! Form::checkbox('permissions[]', $item->id, $role->exists ? (in_array($item->id, $role->permissions->pluck('id')->toArray()) ?: false) : true, ['class' => 'custom-checkbox childCheckBox', 'id' => 'this-permission-'.$ctr.'-'.$counter]) !!}
                                                    <label for="this-permission-{{$ctr}}-{{$counter}}" unselectable="on">{{ $item->title }}</label>
                                                </li>
                                                @php
                                                    $ctr++;
                                                @endphp
                                            @endforeach
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            @php
                                $counter++;
                            @endphp
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    </div>
    @include('partials.admin.saving', [
        'text' => $role->exists ? 'Update Role' : 'Add Role',
        'activation' => false,
        'withImage' => false,
        'imageName' => 'Logo',
        'entity' => isset($role) ? $role : '',
        'imgPath' =>  isset($role) && $role->exists ? asset($role->img_logo_path) : asset(\ATM\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER),
    ])
    {!! Form::close() !!}
</div>
