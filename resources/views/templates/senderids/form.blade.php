<div class="row">
    <div class="col">
        {!! Form::model($senderids, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'sender_id_form', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
            <div class="row">
                <div class="col-12 col-lg-9">
                    <div class="card">
                        <div class="card-header bg-black">
                            Primary Details
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 required">
                                    <div class="form-group required">
                                        {!! Form::label('name', 'Name', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-user text-default"></i>
                                            </span>
                                            <input type="text" name="name" class="form-control required {{ $errors->has('name') ? 'input-error' : '' }}" id="name" value="{{ isset($senderids) && !empty($senderids->name) ? $senderids->name : '' }}"  autocomplete="new-account-name" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-3 p-l-0">
                    <div class="card" id="sticky">
                        <div class="card-header">
                            Actions
                        </div>
                        <div class="card-body">
                        </div>
                        <div class="card-footer">
                            @include('partials.admin.saving', [
                                'ownLayout' => true,
                                'text' => isset($senderids) && $senderids->exists ? 'Update Sender Ids' : 'Add Sender Ids',
                            ])
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
