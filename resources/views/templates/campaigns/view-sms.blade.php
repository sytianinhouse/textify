<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                Sms Sent
            </div>
            <div class="card-body">
                @include('templates.tasks.partials.task-jobs-list', [
                    'jobs' => $jobs,
                    'campaingSentList' => true,
                ])
            </div>
        </div>
    </div>
</div>
