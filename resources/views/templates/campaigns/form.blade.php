<div class="row">
    <div class="col">
        {!! Form::model($campaign, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'sub_account_form', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
        <div class="row">
            <div class="col-12 col-lg-9">
                <div class="card">
                    <div class="card-header bg-black">
                        Primary Details
                    </div>
                    <div class="card-body">
                        <div class="row m-t-10">
                            <div class="col-12 {{ ($sub_form && $subaccounts->count() > 1 ) ? 'col-lg-6' : '' }}">
                                <div class="form-group required">
                                    {!! Form::label('name', 'Name', ['class' => 'col-form-label']) !!}
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-user text-default"></i>
                                        </span>
                                        <input type="text" name="name" class="form-control required {{ $errors->has('name') ? 'input-error' : '' }}" id="name" value="{{ isset($campaign) && !empty($campaign->name) ? $campaign->name : old('name') }}" required autocomplete="new-name">
                                    </div>
                                </div>
                            </div>
                            @if( $sub_form && $subaccounts->count() > 1 )
                                <div class="col-12 col-lg-6">
                                    <div class="form-group required">
                                        {!! Form::label('name', 'Sub Account', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-users" aria-hidden="true"></i>
                                            </span>
                                            <select name="sub_account_id[]" id="sub_account_id" required multiple="true" class="chzn-select form-control required {{ $errors->has('subaccount') ? 'input-error' : '' }}">
                                                @forelse($subaccounts as $sub)
                                                    <option value="{{ $sub->id }}" {{ isset($campaign) && in_array($sub->id, $campaign->subAccounts->pluck('id')->toArray())  ? 'selected' : '' }}>{{ $sub->account_name }}</option>
                                                @empty
                                                    <option value="empty">Empty Sub Accounts</option>
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="col-12">
                                <div class="form-group">
                                    {!! Form::label('description', 'Description', ['class' => 'col-form-label']) !!}
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-comments text-default"></i>
                                        </span>
                                        <textarea name="description" class="form-control {{ $errors->has('description') ? 'input-error' : '' }}" id="description" cols="30" rows="10"  autocomplete="new-description">{{ isset($campaign) && !empty($campaign->description) ? $campaign->description : old('description') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('partials.admin.saving', [
                'text'       => isset($campaign) && $campaign->exists ? 'Update Campaign': 'Add Campaign',
                'activation' => false,
                'withImage'  => false,
                'entity'     => isset($campaign) ? $campaign : '',
            ])
        </div>
        {!! Form::close() !!}
    </div>
</div>
