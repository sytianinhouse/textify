<div class="row">
    <div class="col-12 col-sm-9">
        <div class="card">
            <div class="card-header">
                Basic Details
            </div>
            <div class="card-body">
                {!! Form::model($client, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'client_form', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
                <div class="row">
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-sm-8 required">
                                {!! Form::label('company_name', 'Company Name', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-user text-default"></i>
                                        </span>
                                        <input type="text" name="company_name" class="form-control required {{ $errors->has('company_name') ? 'input-error' : '' }}" id="company_name" value="{{ isset($client) && !empty($client->company_name) ? $client->company_name : old('company_name') }}" autocomplete="new-company-name" required>
                                    </div>
                                </div>
                                @if($errors->has('company_name'))
                                    <span class="has-error">{{ $errors->first('company_name') }}</span>
                                @endif
                            </div>
                            <div class="col-sm-4 required">

                                {!! Form::label('cuid', 'Unique Client ID', ['class' => 'col-form-label']) !!}

                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-user text-default"></i>
                                        </span>
                                        <input type="text" name="cuid" class="form-control required {{ $errors->has('cuid') ? 'input-error' : '' }}" id="cuid" value="{{ isset($client) && !empty($client->cuid) ? $client->cuid : old('cuid') }}" autocomplete="new-cuid" required>
                                    </div>
                                </div>
                                @if($errors->has('cuid'))
                                    <span class="has-error">{{ $errors->first('cuid') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row m-t-15">
                            <div class="col">
                                {!! Form::label('address_1', 'Address 1', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-home text-default"></i>
                                        </span>
                                        <input class="form-control {{ $errors->has('address_1') ? 'input-error' : '' }}" id="address_1" name="address_1" type="text" value="{{ isset($client) && !empty($client->address_1) ? $client->address_1 : old('address_1') }}" autocomplete="new-address-1">
                                    </div>
                                @if($errors->has('address_1'))
                                    <span class="has-error">{{ $errors->first('address_1') }}</span>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row m-t-15">
                            <div class="col">
                                {!! Form::label('address_2', 'Address 2', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-home text-default"></i>
                                        </span>
                                        <input class="form-control  {{ $errors->has('address_2') ? 'input-error' : '' }}" id="address_2" name="address_2" type="text" value="{{ isset($client) && !empty($client->address_2) ? $client->address_2 : old('address_2') }}" autocomplete="new-address-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row m-t-15">
                            <div class="col">
                                {!! Form::label('city', 'City', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-building text-default"></i>
                                        </span>
                                        @php $errorcities = $errors->has('city') ? 'form-control chzn-select input-error' : 'form-control chzn-select'; @endphp
                                        {!! Form::select('city', $cities + ['' => 'Select City'], '', [
                                            'class' => $errorcities,
                                            'id' => 'city',
                                            'tabindex' => 2
                                        ]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                {!! Form::label('country', 'Country', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-flag text-default"></i>
                                        </span>
                                        <select class="form-control" id="country" name="country">
                                            @foreach($countries as $key => $country);
                                                <option value="{{ $key }}"
                                                    @if(isset($client) && !empty($client->country))
                                                        @if($client->country == $key)
                                                            selected
                                                        @endif
                                                    @else
                                                        @if(old('country'))
                                                            @if(old('country') == $key)
                                                                selected
                                                            @endif
                                                        @else
                                                            @if($key == 'PH')
                                                                selected
                                                            @endif
                                                        @endif
                                                    @endif
                                                >
                                                    {{ $country }}
                                                </option>
                                                {{-- ($key == 608 ? 'selected' : '') --}}
                                            @endforeach
                                        </select>
                                        {{-- <input class="form-control  {{ $errors->has('country') ? 'input-error' : '' }}" id="country" name="country" type="text" value="{{ isset($client) && !empty($client->country) ? $client->country : '' }}"> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                {!! Form::label('zip', 'Zipcode', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-map-pin text-default"></i>
                                        </span>
                                        <input class="form-control {{ $errors->has('zip') ? 'input-error' : '' }}" id="zip" name="zip" type="text" value="{{ isset($client) && !empty($client->zip) ? $client->zip : old('zip') }}" autocomplete="new-zip">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row m-t-15">
                            <div class="col">
                                {!! Form::label('website', 'Website', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-globe text-default"></i>
                                        </span>
                                        <input class="form-control {{ $errors->has('website') ? 'input-error' : '' }}" id="website" name="website" type="text" value="{{ isset($client) && !empty($client->website) ? $client->website : old('website') }}" autocomplete="new-website">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                {!! Form::label('phone', 'Phone', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group">
                                        <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                            <i class="fa fa-phone text-default"></i>
                                        </span>
                                        <input class="phone phone-instructions form-control {{ $errors->has('phone') ? 'input-error' : '' }}" id="phone" name="phone" type="text" data-inputmask="'mask': '(02) 999-9999'" data-mask value="{{ isset($client) && !empty($client->phone) ? $client->phone : old('phone') }}"  autocomplete="new-phone">
                                        <span class="input-group-text input-group-append br-0 rounded-right">
                                            (02) 000-0000
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                {!! Form::label('mobile', 'Mobile', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                            <i class="fa fa-mobile text-default"></i>
                                        </span>
                                        <input class="mobile-phone form-control {{ $errors->has('mobile') ? 'input-error' : '' }}" id="mobile" name="mobile" type="text" value="{{ isset($client) && !empty($client->mobile) ? $client->mobile : old('mobile') }}"  autocomplete="new-mobile">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card m-t-15">
            <div class="card-header">
                Primary Contact Details
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col required">
                                {!! Form::label('contact_first_name', 'Contact First Name', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-user text-default"></i>
                                        </span>
                                        <input class="form-control required {{ $errors->has('contact_first_name') ? 'input-error' : '' }}" id="contact_first_name" name="contact_first_name" type="text" value="{{ isset($client) && !empty($client->contact_first_name) ? $client->contact_first_name : old('contact_first_name') }}"  autocomplete="new-contact-fname" required>
                                    </div>
                                </div>
                                @if($errors->has('contact_first_name'))
                                    <span class="has-error">{{ $errors->first('contact_first_name') }}</span>
                                @endif
                            </div>
                            <div class="col">
                                {!! Form::label('contact_last_name', '', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-user text-default"></i>
                                        </span>
                                        <input class="form-control {{ $errors->has('contact_last_name') ? 'input-error' : '' }}" id="contact_last_name" name="contact_last_name" type="text" value="{{ isset($client) && !empty($client->contact_last_name) ? $client->contact_last_name : old('contact_last_name') }}" autocomplete="new-contact-lname">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row m-t-15">
                            <div class="col">
                                {!! Form::label('contact_email', 'Email', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                            <i class="fa fa-envelope text-default"></i>
                                        </span>
                                        <input class="form-control {{ $errors->has('contact_email') ? 'input-error' : '' }}" id="contact_email" name="contact_email" type="text" value="{{ isset($client) && !empty($client->contact_email) ? $client->contact_email : old('contact_email') }}" autocomplete="new-contact-email">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                {!! Form::label('contact_telephone', 'Phone', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                            <i class="fa fa-phone text-default"></i>
                                        </span>
                                        <input class="phone phone-instructions form-control {{ $errors->has('contact_telephone') ? 'input-error' : '' }}" id="contact_telephone" name="contact_telephone" type="text" data-inputmask="'mask': '(02) 000-0000'" data-mask value="{{ isset($client) && !empty($client->contact_telephone) ? $client->contact_telephone : old('contact_telephone') }}" autocomplete="new-contact-telephone">
                                        <span class="input-group-text input-group-append br-0 rounded-right">
                                            (02) 000-0000
                                        </span>
                                    </div>
                                </div>
                            </div>
                             <div class="col">
                                {!! Form::label('contact_mobile', 'Mobile', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group">
                                        <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                            <i class="fa fa-mobile text-default"></i>
                                        </span>
                                        <input class="mobile-phone form-control {{ $errors->has('contact_mobile') ? 'input-error' : '' }}" id="contact_mobile" name="contact_mobile" type="text" value="{{ isset($client) && !empty($client->contact_mobile) ? $client->contact_mobile : old('contact_mobile') }}"  autocomplete="new-contact-mobile">
                                    </div>
                                </div>
                                @if($errors->has('contact_mobile'))
                                    <span class="has-error">{{ $errors->first('contact_mobile') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- start of saving column or COLUMN 2 --}}
    @include('partials.admin.saving', [
        'text' => isset($client) && $client->exists ? 'Update Client' : 'Add Client',
        'activation' => true,
        'withImage' => true,
        'imageName' => 'Logo',
        'entity' => isset($client) ? $client : '',
        'activeState' => ($client->active != 0) ? true : false,
        'imgPath' =>  isset($client) && $client->exists ? asset($client->img_thumb_path) : asset(\ATM\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER),
    ])
    {{-- end of saving column or COLUMN 2 --}}
    {!! Form::close() !!}
</div>
