<div class="row">
    <div class="col-12 col-sm-9">
        <div class="card">
            <div class="card-header">
                Customer Group Details
            </div>
            <div class="card-body">
                {!! Form::model($customergroup, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'client_form', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
                <div class="row">
                    <div class="col-12">
                        <div class="form-group row">
                            <input type="hidden" name="client_id" value="<?php echo $client->id; ?>">
                            <div class="col col-lg-12 required">
                                {!! Form::label('name', 'Name', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-text-width" aria-hidden="true"></i>
                                        </span>
                                        <input type="text" name="name" class="form-control required {{ $errors->has('name') ? 'input-error' : '' }}" id="name" value="{{ isset($customergroup) && !empty($customergroup->name) ? $customergroup->name : old('name') }}" placeholder="Name" required autocomplete="new-name">
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="col col-lg-6 required">
                                {!! Form::label('sub_account', 'Sub Accounts', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-addon custom-divide">
                                            <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                        </span> --}}
                                        {{-- @php $errorSubaccount = $errors->has('sub_account') ? 'form-control chzn-select sub-account input-error' : 'form-control chzn-select sub-account'; @endphp --}}
                                        {{-- @php $errorSubaccount = $errors->has('sub_account') ? 'form-control sub-account input-error' : 'form-control sub-account'; @endphp
                                        {!! Form::select('sub_account', $subaccounts, $selectedSubAccnt, [ 'placeholder' => 'Select Sub Account', 'disabled' => true,
                                            'class' => $errorSubaccount,
                                            'id' => 'sub_account',
                                            'tabindex' => 2
                                        ]) !!}
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                {!! Form::label('description', 'Description', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                        </span>
                                        {!! Form::textarea('description', isset($customergroup->description) ? $customergroup->description : null, ['placeholder' => 'Description', 'class' => 'form-control', 'id' => 'description', 'autocomplete' => 'new-description']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.admin.saving', [
        'text' => $customergroup->exists ? 'Update Group' : 'Add Group',
        'activation' => false,
        'withImage' => false,
        'imageName' => 'Logo',
        'entity' => isset($customergroup) ? $customergroup : '',
        'imgPath' =>  isset($customergroup) && $customergroup->exists ? asset($customergroup->img_logo_path) : asset(\ATM\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER),
    ])

    {!! Form::close() !!}
</div>
