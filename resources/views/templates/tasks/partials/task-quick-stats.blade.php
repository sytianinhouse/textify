<h5>Statistic</h5>
<table class="table table-full-width table-striped">
    <tbody>
        <tr>
            <td width="50%">SMS Made:</td>
            <td width="50%">@numberComma($task->jobs_count)</td>
        </tr>
        <tr>
            <td width="50%">Sent SMS:</td>
            <td width="50%">@numberComma($task->success_jobs_count)</td>
        </tr>
        <tr>
            <td width="50%" class="font-red">Failed SMS:</td>
            <td width="50%" class="font-red">@numberComma($task->tasksjobsFailed->count())</td>
        </tr>
        <tr>
            <td width="50%">Credits Consumed:</td>
            <td width="50%">@numberComma($task->actual_credits_consumed)</td>
        </tr>
    </tbody>
</table>
