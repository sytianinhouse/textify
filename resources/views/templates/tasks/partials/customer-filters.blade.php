<div class="card m-b-10">
    <div class="card-header">
        Recipients
    </div>
    <div class="card-body">
        <span class="get-subaccount-id" data-subaccount-id="{{ $subAccount->id }}"></span>
        <span class="get-client-id" data-client-id="{{ $clientID }}"></span>
        <input type="text" name="contacts_to_be_send" id="contacts_to_be_send" hidden>
        <input type="text" name="mobile_number_to_be_send" id="mobile_number_to_be_send" hidden>
        <input type="text" name="generated_contact" id="generated_contact" hidden>
        <input type="text" name="allIDs" id="allIDs" hidden>
        <recipient-holder
            :sub-account-i-d="{{ (isset($context)) ? $context->id : $subAccount->id }}"
            :client-i-d="{{ (isset($context)) ? $context->client_id : $clientID }}"
        >
        </recipient-holder>
    </div>
</div>
@section('before_global_scripts')
    @parent
    <script type="text/javascript" src="{{asset('js/vue.js')}}"></script>
@stop
