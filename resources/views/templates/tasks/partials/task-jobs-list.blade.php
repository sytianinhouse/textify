<div class="table-responsive">
    <table id="example1" class="display table table-stripped table-bordered table-vertical-center">
        <thead>
            <tr>
                <th style="width: 20%;">Full Name</th>
                <th style="width: 20%;">Mobile #</th>
                <th style="width: 20%;">Date Time Sent/Send</th>
                <th style="width: 10%;" class="text-right {{ isset($exclude_credit_costs) ? 'd-none' : '' }}">Credit Cost</th>
                <th class="text-center" style="{{ (isset($campaingSentList)) ? 'width: 10%;' : 'width: 25%;' }}">{{ (isset($campaingSentList)) ? 'Actions' : 'Status / Actions' }}</th>
            </tr>
        </thead>
        <tbody>
        @forelse($jobs as $job)
            @if( $job->table == 'jobs_completed' || strstr($job->table, 'jobs_completed') )
                <td><a href="{{ route('client.customer.edit', $job->customer ? $job->customer->id : 0) }}">{{ ($job->customer_name != ' ') ? $job->customer_name : 'Custom Number' }}</a></td>
                <td>{!! formattedNumber($job->customer_number) !!}</td>
                <td>@humanDateTimeSecondsFormat($job->when_to_send ?: '')</td>
                <td class="text-right {{ isset($exclude_credit_costs) ? 'd-none' : '' }}">@numberComma($job->credits_consumed)</td>
                <td class="text-center">
                    @if( !isset($campaingSentList) )
                        <img src="{{ asset('images/sms-sent@20.png') }}" alt="sms-sent-icon" title="SMS Sent"> SMS Sent
                    @endif
                    <div class="mt-1">
                            @if( $authUser->isOwner() )
                                <a href="{{ route('client.jobs.view', [$job, get_class($job)]) }}" class="d-inline badge-sm badge badge-info p-2" target="_blank">View</a>
                                @elseif(  $authUser->isAccountUser() )
                                <a href="{{ sroute('sub.jobs.view', [$job, get_class($job)]) }}" class="d-inline badge-sm badge badge-info p-2" target="_blank">View</a>
                                {{-- <a href="{{ sroute('sub.jobs.view', [$job, get_class($job)]) }}" class="d-inline badge-sm badge badge-success p-2" target="_blank">View</a> --}}
                            @endif
                    </div>
                </td>
            @elseif( $job->table == 'jobs_failed' || strstr($job->table, 'jobs_failed') )
                <td>{{ ($job->customer_name != ' ') ? $job->customer_name : 'Custom Number' }}</td>
                <td>{!! formattedNumber($job->customer_number) !!}</td>
                <td>@humanDateTimeSecondsFormat($job->time_attempt_send ?: '')</td>
                <td class="text-right {{ isset($exclude_credit_costs) ? 'd-none' : '' }}">@numberComma($job->credits_consumed)</td>
                <td class="text-center">
                    <img src="{{ asset('images/sms-fail@20.png') }}" alt="sms-fail-icon" title="SMS Failed"> SMS Failed&nbsp;
                    <i class="fa fa-exclamation-circle font-red" data-toggle="tooltip" data-placement="right" title="" data-original-title="Error: {{ $job->error_log }}"></i>
                    <div class="mt-1">
                        @if( $authUser->isOwner() )
                            <a href="{{ route('client.jobs.jf.edit', $job) }}" class="d-inline badge-sm badge badge-info p-2">View</a>
                            <a href="{{ route('client.jobs.jf.resend', $job) }}" class="d-inline badge-sm badge badge-success p-2">Resend</a>
                            {{--<a href="{{ route('client.jobs.jf.remove', $job) }}" class="d-inline badge-sm badge badge-danger p-2">Remove</a>--}}
                        @elseif(  $authUser->isAccountUser() )
                            <a href="{{ sroute('sub.jobs.jf.edit', $job) }}" class="d-inline badge-sm badge badge-info p-2">View</a>
                            <a href="{{ sroute('sub.jobs.jf.resend', $job) }}" class="d-inline badge-sm badge badge-success p-2">Resend</a>
                            {{--<a href="{{ sroute('sub.jobs.jf.remove', $job) }}" class="d-inline badge-sm badge badge-danger p-2">Remove</a>--}}
                        @endif
                    </div>
                </td>
            @else
                <td>{{ ($job->customer_name != ' ') ? $job->customer_name : 'Custom Number' }}</td>
                <td>{!! formattedNumber($job->customer_number) !!}</td>
                <td>@humanDateTimeSecondsFormat($job->when_to_send ?: '')</td>
                <td class="text-right {{ isset($exclude_credit_costs) ? 'd-none' : '' }}">@numberComma($job->credits_consumed)</td>
                <td class="text-center {{ $job->table }}">
                    <img src="{{ asset('images/sms-onqueue@20.png') }}" alt="sms-onqueue-icon {{ $job->table }}" title="SMS Onqueue"> On Queue
                    <div class="mt-1">
                        @if( $authUser->isOwner() )
                            {{-- <a href="{{ route('client.jobs.view', [$job, get_class($job)]) }}" class="d-inline badge-sm badge badge-info p-2" target="_blank">View</a> --}}
                            <a href="{{ route('client.jobs.view', [$job, class_basename($job)]) }}" class="d-inline badge-sm badge badge-info p-2" target="_blank">View</a>
                        @elseif(  $authUser->isAccountUser() )
                            {{-- <a href="{{ sroute('sub.jobs.view', [$job, get_class($job)]) }}" class="d-inline badge-sm badge badge-info p-2" target="_blank">View</a> --}}
                            <a href="{{ sroute('sub.jobs.view', [$job, class_basename($job)]) }}" class="d-inline badge-sm badge badge-info p-2" target="_blank">View</a>
                            {{-- <a href="{{ sroute('sub.jobs.view', [$job, get_class($job)]) }}" class="d-inline badge-sm badge badge-success p-2" target="_blank">View</a> --}}
                         @endif
                    </div>
                </td>
            @endif
            <tr></tr>
        @empty
            <tr>
                <td colspan="5">No Records Found</td>
            </tr>
        @endforelse
        @if(isset($total))

        @endif
    </table>
    @if(isLengthAware($jobs))
        <div class="text-center m-t-15">
            {!! $jobs->links() !!}
        </div>
    @else
        <h6 class="text-center">Showing all items</h6>
    @endif
</div>
