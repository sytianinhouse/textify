{!! Form::open([
    'method' => $method,
    'class' => 'form-horizontal login_validator',
    'data-subaccount' => isset($subAccount) ? $subAccount->id : null,
    'id' => 'client_form',
    'autocomplete' => 'off'
]) !!}

<div class="row">
    <div class="col-12 col-sm-9">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        SMS Configuration
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <p class="m-b-0">* Tick checkbox below in which sub account you want to enable birthday SMS sending!</p>
                                <div class="list-of-short-code">
                                    <span class="shortcode-click" data-toggle="tooltip" data-placement="right" title="See all shortcodes">See list of shortcodes</span>
                                    <div class="listing">
                                        <ul class="ul-listing">
                                            <li class="first-label">
                                                <div class="span-label">Shortcodes</div>
                                            </li>
                                            @foreach($shortCodes as $key => $codes)
                                                <li>
                                                    <b>
                                                        <span id="p{{ $key }}">[{{ $codes }}]</span>
                                                        <span class="copy-btn c1"
                                                            data-toggle="tooltip" data-placement="right" title="Copy Shortcode"
                                                            onclick="copyToClipboard('#p{{ $key }}', '.c{{ $key }}')">COPY
                                                        </span>
                                                    </b>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush">
                        @foreach($subAccounts as $subAccount)
                            @php $task = ( isset($tasks[$subAccount->id]) ) ? $tasks[$subAccount->id] : null; @endphp
                            <li class="list-group-item">
                                <div class="checkbox m-b-10">
                                    <label>
                                        {!! Form::checkbox("data[{$subAccount->id}][enable]", 1, $task ? $task->active : false, array('class' => 'custom-control-input')) !!}
                                        <span class="cr custom-control-indicator custom_checkbox_default"><i class="cr-icon fa fa-check text-success"></i></span>
                                        {{ $subAccount->account_name }}
                                    </label>
                                    @if( $task != NULL && $task->exists )
                                     | <a href="javascript:;" class="btn-task-jobs-list btn btn-sm btn-success" data-task-id="{{ $task->id }}" data-url="{{ route('client.jobs.get-api') }}">VIEW JOBS</a>
                                    @endif
                                </div>
                                <div class="row textarea-group">
                                    <div class="col-7">
                                        <div class="form-group row position-relative">
                                            <div class="col required textarea-field">
                                                {!! Form::label('message', 'Message', ['class' => 'col-form-label']) !!}
                                                <div class="col p-d-0 m-b-10">
                                                    <div class="input-group input-group-prepend">
                                                        <span class="input-group-text br-0 border- right-0 rounded-left">
                                                            <i class="fa fa-list" aria-hidden="true"></i>
                                                        </span>
                                                        @php $errorMessage = $errors->has('message') ? 'form-control messagearea input-error required' : 'form-control messagearea required'; @endphp
                                                        {!! Form::textarea(
                                                            "data[{$subAccount->id}][message]",
                                                            isset($task->message) ? $task->message : '',
                                                            ['id' => "txtid-{$subAccount->id}", 'class' => $errorMessage, 'autocomplete' => 'new-description', 'required', 'rows' => 7]
                                                        ) !!}
                                                    </div>
                                                    <button type="button" class="btn btn-sm btn-group-toggle btn-textarea" onclick="copyToClipboard('#txtid-{{ $subAccount->id }}', '.c{{ $key }}', 'field')""><i class="fa fa-copy"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="form-group">
                                            <p class="roboto-light m-b-0 small">Estimated Character Count: <strong><span id="charNum" class="charNum"></span></strong></p>
                                            <input type="hidden" name="data[{{ $subAccount->id }}][echarc]" value="{{ isset($task->message) ? strlen($task->message) : '' }}" id="echarc" class="echarc">
                                            @php
                                                $settingCharLimit = isset($siteSettings) ? $siteSettings->sms_character_limit : 160;
                                                $messageLength = isset($task->message) ? strlen($task->message) : 0;

                                                $eCreditC = $messageLength ? ceil($messageLength/$settingCharLimit) : null;
                                            @endphp
                                            {{-- <p class="roboto-light m-b-0 small">Estimated Credit Cost: <strong><span id="creditNum" class="creditNum"></span></strong></p> --}}
                                            <p class="roboto-light m-b-0 small">Estimated Credit Cost: <strong>{{ $eCreditC }}</strong></p>
                                            
                                            <input type="hidden" name="data[{{ $subAccount->id }}][ecreditc]" value="{{ $eCreditC }}" id="ecreditc" class="ecreditc">
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <div id="campaignoptions-{{ $subAccount->id }}" class="col required position-relative campaign-options">
                                                {!! Form::label('campaign', 'Campaign', ['class' => 'col-form-label']) !!}
                                                <a href="javascript:;" class="plus-icon btn btn-success btn-quick-create-campaign" data-id="{{ $subAccount->id }}">
                                                    <i class="fa fa-plus" aria-hidden="true"></i><span>Add New</span>
                                                </a>
                                                @php
                                                    $errorCampaign = 'form-control chzn-select customer-group decrease-size custom-select required';
                                                    $errorCampaign .= $errors->has('campaign') ? 'input-error' : ''; @endphp
                                                {!! Form::select("data[{$subAccount->id}][campaign_id]", $subAccount->campaigns->pluck('name','id'), $task ? $task->campaign_id : null, [
                                                    'placeholder' => '',
                                                    'class' => $errorCampaign,
                                                    'id' => 'campaign',
                                                    'tabindex' => 2,
                                                    'required'
                                                ]) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-6">
                                                    {!! Form::label('campaign', 'Day to send', ['class' => 'col-form-label']) !!}
                                                    <div class="input-group clockpicker1 input-group-append">
                                                        {!! Form::select("data[{$subAccount->id}][day_to_send]", ATM\Repositories\Date\Date::days(), ($task) ? $task->date_time_to_send->format('j') - 1 : 1, [
                                                            'class' => 'form-control',
                                                            'required'
                                                        ]) !!}
                                                        <span class="input-group-text add-on rounded-right">
                                                            <i class="fa fa-calendar-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    {!! Form::label("time_to_send", 'Time' , ['class' => 'col-form-label']) !!}
                                                    <div class="input-group clockpicker1 input-group-append">
                                                        <input type="text" name="data[{{ $subAccount->id }}][time_to_send]" class="form-control required" data-placement="top"
                                                               data-align="top" value="{{ isset($task->time_to_send) ? ($task->date_time_to_send->format('h:iA') ?: '08:00AM') : (old('time_to_send') ?: '08:00AM') }}" required>
                                                        <span class="input-group-text add-on rounded-right">
                                                            <i class="fa fa-clock-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <div class="card-body">
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-12 col-lg-3 p-l-0">
        <div class="card" id="sticky" style="margin-bottom:10px;">
            <div class="card-header">
                Actions
            </div>
            <div class="card-body">
                <div class="alert alert-warning">
                    <strong>Attention <i class="fa fa-exclamation-circle"></i></strong>
                    <ul class="list-unstyled">
                        <li>- Any changes will take effect after scheduled generation of SMS.</li>
                        <li>- Birthday SMS for the month of {{ $curMnth->copy()->addMonth()->format('F') }} will generate on {{ $curMnth->copy()->endOfMonth()->subDay()->format('F d') }}.</li>
                        <li>- Make sure that your account have sufficient load to accommodate all SMS to send.</li>
                    </ul>
                </div>
                <?php /*@if($subAccounts->count() == 1)
                    @php $subAccount = $subAccounts->first(); @endphp
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox("enable[{$subAccount->id}]", 1, $task ? $task->active : true, array('class' => 'custom-control-input')) !!}
                            <span class="cr custom-control-indicator custom_checkbox_default"><i class="cr-icon fa fa-check text-success"></i></span>
                            Enable <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Check this box to enable Birthday SMS."></i>
                        </label>
                    </div>
                @else
                    <h6>
                        Enable SMS for:
                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Check the box below who will use this SMS featured."></i>
                    </h6>
                    @foreach($subAccounts as $subAccount)
                        <div class="checkbox m-b-10">
                            <label>
                                {!! Form::checkbox("enable[{$subAccount->id}]", 1, true, array('class' => 'custom-control-input')) !!}
                                <span class="cr custom-control-indicator custom_checkbox_default"><i class="cr-icon fa fa-check text-success"></i></span>
                                {{ $subAccount->account_name }}
                            </label>
                        </div>
                    @endforeach
                @endif*/ ?>
            </div>
            <div class="card-footer">
                @include('partials.admin.saving', [
                    'text' => 'Save Changes',
                    'ownLayout' => true
                ])
            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}
@include('partials.task.modal-campaign-create')

@include('partials.task.modal-ajax-jobs-list')

@section('footer_scripts')
    @include('scripts.campaign.quick-creation', [
       'subaccountId' => ( Request::input('sub_account_id') ) ? Request::input('sub_account_id'): $subAccount->id,
       'multipleSubAccount' => true
   ])
    @include('scripts.admin.credit-computation', [
        'charLimit' => isset($siteSettings) ? ( $siteSettings->sms_character_limit != null ? $siteSettings->sms_character_limit : 160 ) : 160,
        'credPerLimit' => isset($siteSettings) ? ( $siteSettings->sms_credit_price_per_limit != null ? $siteSettings->sms_credit_price_per_limit : 1 ) : 1
    ])
@endsection
