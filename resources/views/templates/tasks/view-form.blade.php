<div class="row">
    <div class="col-12">
        {!! Form::model($task, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'client_form', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
        <div class="row">
            <div class="col-12 col-sm-9">
                <div class="card">
                    <div class="card-header">
                        Task Details
                    </div>
                    <div class="card-body">
                        @if( $type == 'text_blast_send' )
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">
                                        <div class="col">
                                            @if( isset($totalSub) && $totalSub > 1)
                                                <h6>
                                                    <strong>
                                                        {{ $task->subAccount->account_name }}
                                                    </strong><br>
                                                    <i class="small text-uppercase">Sub Account:</i>
                                                </h6>
                                            @endif
                                            <h6>
                                                <strong>
                                                    {{ $selectedCampaign->name }}
                                                </strong><br>
                                                <i class="small text-uppercase">Campaign Name:</i>
                                            </h6>
                                            <h6>
                                                <code>
                                                    {!! $task->message !!}
                                                </code><br>
                                                <i class="small text-uppercase">Message:</i>
                                            </h6>
                                            <h6>
                                                <strong>
                                                    {{ $task->created_at->toDateString() }}
                                                </strong><br>
                                                <i class="small text-uppercase">Date Created:</i>
                                            </h6>
                                            <h6>
                                                <strong>
                                                    {{ $task->date_time_to_send->toDayDateTimeString() }}
                                                </strong><br>
                                                <i class="small text-uppercase">Date and time to Send:</i>
                                            </h6>
                                        </div>
                                        <div class="col">
                                            <h6>
                                                <span class="small text-uppercase roboto-light">Total Recipient:</span>
                                                <strong>
                                                    {{ $task->jobs_count }}
                                                </strong>
                                            </h6>
                                            <h6>
                                                <span class="small text-uppercase roboto-light">Total Credits:</span>
                                                <strong>
                                                    {{ $task->projected_credits_consumed }}
                                                    {{-- @numberComma($jobs->sum('credits_consumed')) --}}
                                                </strong>
                                            </h6>
                                            {{-- <h6>
                                                <span class="small text-uppercase roboto-light">Total Costs:</span>
                                                <strong>
                                                    @number($task->jobs_count * $task->projected_credits_consumed)
                                                </strong>
                                            </h6> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="card m-b-10 data_tables">
                    <div class="card-header">
                        Recipient Details
                    </div>
                    <div class="card-body">
                        @include('templates.tasks.partials.task-jobs-list')
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-3">
                <div class="card" id="sticky">
                    <div class="card-header">
                        Actions
                    </div>
                    <div class="card-body">
                        {{--<div class="checkbox m-b-10">
                            <label>
                                {!! Form::checkbox('active', 1, ( $task->active ) ? true : false, array('class' => 'custom-control-input')) !!}
                                <span class="cr custom-control-indicator custom_checkbox_default"><i class="cr-icon fa fa-check text-success"></i></span>
                                Active
                            </label>
                        </div>
                        <hr>--}}
                        @if($task->exists)
                            @include('templates.tasks.partials.task-quick-stats')
                        @endif
                    </div>
                    <div class="card-footer">
                        {{--<button class="btn btn-success form-control" type="submit">
                            <i class="fa fa-save"></i> &nbsp;
                            Update Task
                        </button>--}}
                        <button name="cancel_task" data-value="true" data-confirm="" data-confirm-text="This Task will be deleted permanently." class="confirmCancelSubmit btn btn-warning form-control m-t-10 {{ ( $cancelButton == false ) ? 'd-none' : '' }}" type="submit" value="true" {{ ( $cancelButton == false ) ? 'disabled' : '' }}>
                            <i class="fa fa-times" aria-hidden="true"></i>
                            Cancel
                        </button>
                        <button name="cancel_task" class="cancelTask d-none" data-value="true" type="submit" value="true"></button>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@php
    function wrap_text_with_tags( $haystack, $needle , $beginning_tag, $end_tag ) {
        $needle_start = stripos($haystack, $needle);
        $needle_end = $needle_start + strlen($needle);
        $return_string = substr($haystack, 0, $needle_start) . $beginning_tag . $needle . $end_tag . substr($haystack, $needle_end);
        return $return_string;
    }
@endphp
