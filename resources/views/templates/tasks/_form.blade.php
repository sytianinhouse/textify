<div class="row">
    <div class="col-12">
        @if($credit->pending_balance < 1)
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i>
                </button>
                <strong>Warning!</strong>
                Creation of this SMS blast will not proceed because you have 0 balance on your credits.
            </div>
        @endif
        @if($campaign->count() < 1)
            <div class="alert alert-primary">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i>
                </button>
                <strong>Info!</strong>
                Campaign is required for the reation of every task.
            </div>
        @endif
    </div>
    <div class="col-12 col-sm-9">
        <div class="row">
            <div class="col-12 {{ $type == 'text_blast_send' ? 'col-md-8' : '' }}">
                <div class="card m-b-10">
                    <div class="card-header">
                        Task Details
                    </div>
                    <div class="card-body {{ ($type == 'text_blast_send') ? 'text-blast-wrapper' : '' }}">
                        {!! Form::model($task, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'client_form', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row m-b-10">
                                        <div class="col">
                                            {!! Form::label('CREDIT', 'Remaining Credits: ', ['class' => 'col-form-label']) !!}
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-text br-0 border-right-0 rounded-left">
                                                    <i class="fa fa-money" aria-hidden="true"></i>
                                                </span>
                                                <input type="text" name="credit" class="form-control" disabled id="credit-val"
                                                       value="{{ $credit && $credit->pending_balance ? $credit->pending_balance : '' }}" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col required position-relative">
                                            {!! Form::label('campaign', 'Campaign', ['class' => 'col-form-label']) !!}
                                            @php $errorCampaign = $errors->has('campaign') ? 'form-control chzn-select customer-group input-error' : 'form-control chzn-select customer-group'; @endphp
                                            {!! Form::select('campaign', $campaign, $selectedCampaign, [ 'placeholder' => '',
                                                'class' => $errorCampaign,
                                                'id' => 'campaign',
                                                'tabindex' => 2,
                                                'name'=>'campaign',
                                                'required'
                                            ]) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row position-relative">
                                        <div class="col required">
                                            {!! Form::label('message', 'Message', ['class' => 'col-form-label']) !!}
                                            <div class="list-of-short-code">
                                                <span class="shortcode-click" data-toggle="tooltip" data-placement="right" title="See all shortcodes">See list of Shortcodes</span>
                                                <div class="listing">
                                                    <ul class="ul-listing">
                                                        <li class="first-label">
                                                            <div class="span-label">Shortcodes</div>
                                                        </li>
                                                        <li>
                                                            <b><span id="p1">[first_name]</span> <span class="copy-btn c1" data-toggle="tooltip" data-placement="right" title="Copy Shortcode" onclick="copyToClipboard('#p1', '.c1')">COPY</span></b>
                                                        </li>
                                                        <li>
                                                            <b><span id="p2">[last_name]</span> <span class="copy-btn c2" data-toggle="tooltip" data-placement="right" title="Copy Shortcode" onclick="copyToClipboard('#p2', '.c2')">COPY</span></b>
                                                        </li>
                                                        <li>
                                                            <b><span id="p3">[email]</span> <span class="copy-btn c3" data-toggle="tooltip" data-placement="right" title="Copy Shortcode" onclick="copyToClipboard('#p3', '.c3')">COPY</span></b>
                                                        </li>
                                                        <li>
                                                            <b><span id="p4">[telephone]</span> <span class="copy-btn c4" data-toggle="tooltip" data-placement="right" title="Copy Shortcode" onclick="copyToClipboard('#p4', '.c4')">COPY</span></b>
                                                        </li>
                                                        <li>
                                                            <b><span id="p5">[address]</span> <span class="copy-btn c5" data-toggle="tooltip" data-placement="right" title="Copy Shortcode" onclick="copyToClipboard('#p5', '.c5')">COPY</span></b>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col p-d-0 m-b-10">
                                                <div class="input-group input-group-prepend">
                                                    <span class="input-group-text br-0 border- right-0 rounded-left">
                                                        <i class="fa fa-list" aria-hidden="true"></i>
                                                    </span>
                                                    @php $errorMessage = $errors->has('message') ? 'form-control message input-error' : 'form-control message'; @endphp
                                                    {!! Form::textarea('message', isset($task->message) ? $task->message : $messagePlaceholder, ['class' => $errorMessage, 'id' => 'message', 'autocomplete' => 'new-description', 'required']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col">
                                            <div class="estimated-label-wrapper">
                                                <div class="estimated-character-count">
                                                    <strong>Estimated character count:</strong> <div id="charNum"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="estimated-label-wrapper">
                                                <div class="estimated-credit-count">
                                                    <strong>Estimated Credit Consumed:</strong> <div id="creditNum"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col required">
                                            {!! Form::label('date', 'Date' , ['class' => 'col-form-label']) !!}
                                            <div class="input-group input-append date input-group-append" id="datepicker3"
                                                 data-date-format="dd-mm-yyyy">
                                                @php $errorDate = $errors->has('date_to_send') ? 'date-send input-error' : 'date-send'; @endphp
                                                <input class="form-control {{ $errorDate }}" type="text" value="{{ isset($task->date_to_send) ? $task->date_to_send : old('date_to_send') }}" name="date_to_send" placeholder="yyyy-mm-dd" required>
                                                <span class="input-group-text add-on border-left-0 rounded-right">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col required">
                                            {!! Form::label('date', 'Time' , ['class' => 'col-form-label']) !!}
                                            @php $errorTime = $errors->has('time_to_send') ? 'date-send input-error' : 'date-send'; @endphp
                                            <div class="input-group clockpicker1 input-group-append {{ $errorTime }}">
                                                <input type="text" name="time_to_send" class="form-control" data-placement="top"
                                                       data-align="top" value="{{ isset($task->time_to_send) ? $task->time_to_send : old('time_to_send') }}" required>
                                                <span class="input-group-text add-on border-left-0 rounded-right">
                                                    <i class="fa fa-clock-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if ( $getTotal && $getTotal < 1 )
                                <div class="row">
                                    <div class="col-12">
                                        <h3>
                                            Total Recipient: <span class="span-count">{{ $getTotal }}</span>
                                        </h3>
                                    </div>
                                </div>
                            @endif
                        @elseif($type == 'one_time_send')
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">
                                        <div class="col-6 required">
                                            {!! Form::label('campaign', 'Campaign', ['class' => 'col-form-label']) !!}
                                            @php $errorCampaign = $errors->has('campaign') ? 'form-control chzn-select customer-group input-error' : 'form-control chzn-select customer-group'; @endphp
                                            {!! Form::select('campaign', $campaigns, $selectedCampaign, [ 'placeholder' => '',
                                                'class' => $errorCampaign,
                                                'id' => 'campaign',
                                                'tabindex' => 2,
                                                'name'=>'campaign'
                                            ]) !!}
                                        </div>
                                        @php
                                            $hoursSelect = collect();
                                            for ($i=0; $i <= 24; $i++) {
                                                $string = $i == 1 || $i == 0 ?  ' hour' : ' hours';
                                                $hoursSelect[$i] = $i . $string;
                                            }

                                            $minutesSelect = collect();
                                            for ($i=1; $i <= 12; $i++) {
                                                $minutesSelect[$i*5] = $i*5 . ' mins.';
                                            }
                                        @endphp
                                        <div class="col-6 required">
                                            @php
                                                $errorTimeToSend = $errors->has('time_to_send') ? 'form-control chzn-select customer-group input-error' : 'form-control chzn-select customer-group';
                                                $hourSelected = isset($task->time_to_send) ? (int) \Carbon\Carbon::parse('2019-01-01 ' . $task->time_to_send)->format('h') : 0;
                                            @endphp

                                            {!! Form::label('date', 'Send After' , ['class' => 'col-form-label']) !!}
                                            <div class="row">
                                                <div class="col-6">
                                                    {!! Form::select('time_to_send[]', $hoursSelect, (isset($task->time_to_send) ? $hourSelected : 0), [ 'placeholder' => '',
                                                        'class' => $errorTimeToSend,
                                                        'id' => 'time_to_send',
                                                        'tabindex' => 2,
                                                    ]) !!}
                                                </div>
                                                <div class="col-6">
                                                    {!! Form::select('time_to_send[]', $minutesSelect, (isset($task->time_to_send) ? \Carbon\Carbon::parse('2019-01-01 ' . $task->time_to_send)->format('i') : 5), [ 'placeholder' => '',
                                                        'class' => $errorTimeToSend,
                                                        'id' => 'time_to_send',
                                                        'tabindex' => 2,
                                                    ]) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col required">
                                            {!! Form::label('message', 'Message', ['class' => 'col-form-label']) !!}
                                            <div class="list-of-short-code">
                                                <b><span>[first_name]</span></b>, <b><span>[last_name]</span></b>, <b><span>[email]</span></b>, <b><span>[telephone]</span></b>, <b><span>[address]</span></b>
                                            </div>
                                            <div class="p-t-10">
                                                @php $errorMessage = $errors->has('message') ? 'form-control message input-error' : 'form-control message'; @endphp
                                                {!! Form::textarea('message', isset($task->message) ? $task->message : null, ['class' => 'summernote-editor '.$errorMessage, 'autocomplete' => 'new-description']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            @if($type == 'text_blast_send')
                <div class="col-12 col-md-4">
                    <div class="card m-b-10">
                        <div class="card-header">
                            RECIPIENTS
                        </div>
                        <div class="card-body">
                            <span class="get-subaccount-id" data-subaccount-id="{{ $subAccount->id }}"></span>
                            <span class="get-client-id" data-client-id="{{ $clientID }}"></span>
                            <div class="form-group row">
                                <div class="col">
                                    {!! Form::label('filter', 'Filter By' , ['class' => 'col-form-label']) !!}
                                    @php $errorFilter = $errors->has('select_option_filter') ? 'form-control chzn-select select-option-filter input-error' : 'form-control chzn-select select-option-filter'; @endphp
                                    {!! Form::select('select_option_filter', $options, null, [ 'placeholder' => '',
                                        'class' => $errorFilter,
                                        'id' => 'select_option_filter',
                                        'tabindex' => 2,
                                        'name'=> 'select_option_filter'
                                    ]) !!}
                                </div>
                            </div>
                            <div id="select-filter-option">
                                <div class="form-group row option-hide all_customer {{ (old('select_option_filter') == 'all_customer') ? 'show' : '' }}" data-option="all_customer">
                                    <div class="loader-wrapper">
                                        <div class="wrap">
                                            <div class="wap">
                                                <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <h6 class="custom-label">
                                            ALL CUSTOMERS
                                        </h6>
                                    </div>
                                    <div class="col-12">
                                        {{-- <a href="{{ route('admin.project.client.sub.task.tbs.vr.index', [$project, $client, $subAccount]) }}" title="View All Recipient" target="_blank" class="number-execution"></a> --}}
                                        <a href="#" title="View All Recipients" target="_blank" class="number-execution all-recipient" data-toggle="modal" data-target="#view-recipient"></a>
                                    </div>
                                    <div class="col-12">
                                        <div class="btn-group float-right">
                                            <span class="btn btn-success m-r-5 btn-generate-all">
                                                <i class="fa fa-hourglass-start" aria-hidden="true"></i>&nbsp;&nbsp;Generate recepient
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row option-hide specific_customer {{ (old('select_option_filter') == 'specific_customer') ? 'show' : '' }}" data-option="specific_customer">
                                    <div class="col">
                                        <h6 class="custom-label">
                                            SPECIFIC CUSTOMERS
                                        </h6>
                                        {!! Form::select('select_specific_customer', $customer, $selectedCustomer, [ 'placeholder' => '',
                                            'class' => 'form-control chzn-select customer',
                                            'id' => 'select_specific_customer',
                                            'tabindex' => 2,
                                            'multiple'=>'multiple',
                                            'name'=> 'select_specific_customer[]'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="form-group row option-hide custom_filter {{ (old('select_option_filter') == 'custom_filter') ? 'show' : '' }}" data-option="custom_filter">
                                    <div class="loader-wrapper">
                                        <div class="wrap">
                                            <div class="wap">
                                                <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <h6 class="custom-label">
                                            CUSTOM FILTER
                                        </h6>
                                    </div>
                                    <div class="col-12">
                                        {!! Form::label('group', 'Groups', ['class' => 'col-form-label']) !!}
                                        {!! Form::select('customergroup', $customergroup, $selectedCustomerGroup, [ 'placeholder' => '',
                                            'class' => 'form-control chzn-select customergroup',
                                            'id' => 'customergroup',
                                            'tabindex' => 2,
                                            'name'=> 'customergroup'
                                            //'multiple'=>'multiple',
                                            //'name'=> 'customergroup[]'
                                        ]) !!}
                                    </div>
                                    <div class="col-12">
                                        {!! Form::label('gender', 'Gender', ['class' => 'col-form-label']) !!}
                                        <select name="gender" class="form-control gender">
                                            <option value="">- Gender -</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                    <div class="col-12">
                                        {!! Form::label('birthday', 'Birthdate', ['class' => 'col-form-label']) !!}
                                        <div class="filter-wrap">
                                            <div class="list-filter date">
                                                <select name="b_day" id="dayfilter" class="form-control dayfilter">
                                                    <option value="">- Day -</option>
                                                </select>
                                            </div>
                                            <div class="list-filter month">
                                                <select name="b_month" class="form-control monthfilter">
                                                    <option value="">- Month -</option>
                                                    <option value="1">Jan</option>
                                                    <option value="2">Feb</option>
                                                    <option value="3">Mar</option>
                                                    <option value="4">Apr</option>
                                                    <option value="5">May</option>
                                                    <option value="6">Jun</option>
                                                    <option value="7">Jul</option>
                                                    <option value="8">Aug</option>
                                                    <option value="9">Sept</option>
                                                    <option value="10">Oct</option>
                                                    <option value="11">Nov</option>
                                                    <option value="12">Dec</option>
                                                </select>
                                            </div>
                                            <div class="list-filter year">
                                                <select name="b_year" id="yearpicker" class="form-control yearpicker">
                                                    <option value="">- Year -</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        {!! Form::label('city', 'City', ['class' => 'col-form-label']) !!}
                                        {!! Form::select('cities', $cities, $selectedCities, [ 'placeholder' => '',
                                            'class' => 'form-control chzn-select cities',
                                            'id' => 'cities',
                                            'tabindex' => 2,
                                            'name'=> 'cities'
                                            //'multiple'=>'multiple',
                                            //'name'=> 'cities[]'
                                        ]) !!}
                                    </div>
                                    <div class="col-12">
                                        <a href="#" title="View All Recipients" target="_blank" class="number-execution all-recipient" data-toggle="modal" data-target="#view-recipient"></a>
                                    </div>
                                    <div class="col-12">
                                        <div class="btn-group float-right">
                                            <span class="btn btn-success m-r-5 btn-generate-all">
                                                <i class="fa fa-hourglass-start" aria-hidden="true"></i>&nbsp;&nbsp;Generate recepient
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    @if ($type == 'one_time_send')
        @include('partials.admin.saving', [
            'text' => isset($task->exists) && $task->exists ? 'Update Task' : 'Send Task',
            'activation' => false,
            'withImage' => false,
            'imageName' => 'Logo',
            'entity' => isset($task) ? $task : '',
            'imgPath' =>  isset($task) && $task->exists ? asset($task->img_logo_path) : asset(\ATM\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER),
        ])
    @elseif($type == 'text_blast_send')
        @include('partials.admin.saving', [
            'textSend' => isset($task->exists) && $task->exists ? 'Send Task' : 'Send Task',
            'textUpdate' => isset($task->exists) && $task->exists ? 'Save Task' : 'Save Task',
            'activation' => false,
            'withImage' => false,
            'imageName' => 'Logo',
            'entity' => isset($task) ? $task : '',
            'buttonType' => true,
            'imgPath' =>  isset($task) && $task->exists ? asset($task->img_logo_path) : asset(\ATM\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER),
        ])
    @endif
    {!! Form::close() !!}

</div>

<div class="modal fade" id="view-recipient" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">All Recipient</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="display-recipients"></div>
            </div>
        </div>
    </div>
</div>

@if($type == 'text_blast_send')
@section('footer_scripts')
    <script>

		function copyToClipboard(element, xclass) {
			var $temp = $("<input>");
			$("body").append($temp);
			$temp.val($(element).text()).select();
			document.execCommand("copy");
			$temp.remove();
			$(xclass).text("COPIED");
			setTimeout(function(){
				$('.ul-listing').removeClass('active');
				$(xclass).text("COPY");
			}, 500);
		}
		$('.loader-wrapper').hide();
		$('.display-recipients > .loader-wrapper').hide();
		$('.btn-generate-all').on('click', function (e){
			e.preventDefault();
			var URL = $('#client_form').prop('action');
			$.ajax({
				url: URL + '/ajaxGenerateRecipient',
				type:"POST",
				beforeSend: function (xhr) {
					$('.loader-wrapper').show();
					var TOKEN = $('meta[name="csrf-token"]').attr('content');
					if (TOKEN) {
						return xhr.setRequestHeader('X-CSRF-TOKEN', TOKEN);
					}
				},
				data: {
					getOption : $('.select-option-filter').val(),
					subaccount_id : $('.get-subaccount-id').data('subaccount-id'),
					client_id: $('.get-client-id').data('client-id'),
					customergroup: $('.customergroup').val(),
					gender: $('.gender').val(),
					b_day: $('.dayfilter').val(),
					b_month: $('.monthfilter').val(),
					b_year: $('.yearpicker').val(),
					city: $('.cities').val(),
				},
				success:function(response){
					if(response.success) {
						$('.loader-wrapper').fadeOut(100);
						$('.number-execution').show();
						$('.number-execution').html(response.data);
					} else {
						$('.number-execution').html(response.data);
					}
				},
			});
		});
		$('.all-recipient').on('click', function (e){
			e.preventDefault();
			$('.loader-wrapper').hide();
			$('.display-recipients > .cloader-wrapper').show();
			$('.display-recipients').addClass('customHeight');
			var URL = $('#client_form').prop('action');
			$.ajax({
				url: URL + '/ajaxViewRecipient',
				type:"POST",
				beforeSend: function (xhr) {
					$('.loader-wrapper').show();
					$('.display-recipients > .cloader-wrapper').show();
					var TOKEN = $('meta[name="csrf-token"]').attr('content');
					if (TOKEN) {
						return xhr.setRequestHeader('X-CSRF-TOKEN', TOKEN);
					}
				},
				data: {
					getOption : $('.select-option-filter').val(),
					subaccount_id : $('.get-subaccount-id').data('subaccount-id'),
					client_id: $('.get-client-id').data('client-id'),
					customergroup: $('.customergroup').val(),
					gender: $('.gender').val(),
					dayfilter: $('.dayfilter').val(),
					monthfilter: $('.monthfilter').val(),
					yearpicker: $('.yearpicker').val(),
					city: $('.cities').val(),
				},
				success:function(response){
					$('.display-recipients > .cloader-wrapper').show();
					if(response)
					{
						if (response) {
							$('.loader-wrapper').hide();
							$('.display-recipients > .cloader-wrapper').show();
							$('.display-recipients').html(response);

							setTimeout(function(){
								$('.display-recipients > .cloader-wrapper').hide();
								$('.display-recipients').removeClass('customHeight');
							}, 1000);
						} else {
							$('.display-recipients').addClass('customHeight');
						}
					} else {
						$('.display-recipients').addClass('customHeight');
						$('.display-recipients').html(response);
					}
				}
			});
		});

    </script>

    @include('scripts.admin.credit-computation', [
        'charLimit' => isset($siteSettings) ? $siteSettings->sms_character_limit : 160,
        'credPerLimit' => isset($siteSettings)? $siteSettings->sms_credit_price_per_limit : 1
    ])
@stop
@endif
