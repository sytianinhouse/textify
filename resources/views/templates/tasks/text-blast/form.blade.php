@php
    $cantProceed = false;
@endphp
<div class="row">
    <div class="col-12">
        @if(!$credit || $credit->pending_balance < 1)
            @php $cantProceed = true; @endphp
            <div class="alert alert-danger alert-balance">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i>
                </button>
                <strong>Warning!</strong>
                Creation of this SMS blast will not proceed because you have 0 balance on your credits. Call us at +63917 559 6699 to load up.
            </div>
        @endif
        @if ( $no_sender_id )
            @php $cantProceed = true; @endphp
            <div class="alert alert-danger alert-balance">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
                <strong>Warning!</strong>
                There is no Sender ID for this Sub Account!
            </div>
        @endif
        @if(!$campaign || $campaign->count() < 1)
            @php $cantProceed = true; @endphp
            <div class="alert alert-primary alert-campaign">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i>
                </button>
                <strong>Info!</strong>
                Campaign is required for the creation of every task.
            </div>
        @endif
    </div>
</div>
{!! Form::model($task, ['method' => $method, 'class' => 'form-horizontal login_validator task-form', 'id' => 'client_form', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
    <div class="row">
        <div class="col-12 col-sm-9">
            <div class="row">
                <div class="col-12">
                    {{--{{ $type == 'text_blast_send' ? 'col-md-8' : '' }}--}}
                    <div class="card">
                        <div class="card-header">
                            Task Details
                        </div>
                        <div class="card-body {{ ($type == 'text_blast_send') ? 'text-blast-wrapper' : '' }}">
                            <div class="row">
                                <div class="col-8">
                                    <div class="form-group row align-items-center">
                                        <div class="col required position-relative campaign-options">
                                            @isset($subAccount)
                                                {!! '<h2 class="subTask">Account Name: '.$subAccount->account_name.'</h2>' !!}
                                            @endisset
                                            {!! Form::label('campaign', 'Campaign', ['class' => 'col-form-label']) !!}
                                            <a href="javascript:;" class="plus-icon btn btn-success campaignModalTrigger" data-toggle="modal" data-target="#create-quick-campaign">
                                                <i class="fa fa-plus" aria-hidden="true"></i><span>Add New</span>
                                            </a>
                                            {{-- <a href="javascript:;" title="Add Quick Campaign" target="_blank" class="number-execution all-recipient" v-b-modal.modal-2>
                                                <i class="fa fa-plus" aria-hidden="true"></i><span>Add New</span>
                                            </a> --}}
                                            @php
                                                $errorCampaign = 'form-control chzn-select customer-group decrease-size custom-select required';
                                                $errorCampaign .= $errors->has('campaign') ? 'input-error' : ''; @endphp
                                            {!! Form::select('campaign', $campaign, $selectedCampaign, [ 'placeholder' => '',
                                                'class' => $errorCampaign,
                                                'id' => 'campaign',
                                                'tabindex' => 2,
                                                'name'=>'campaign',
                                                'required'
                                            ]) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row position-relative">
                                        <div class="col required">
                                            {!! Form::label('message', 'Message', ['class' => 'col-form-label']) !!}
                                            <div class="list-of-short-code">
                                                <span class="shortcode-click" data-toggle="tooltip" data-placement="right" title="See all shortcodes">See list of Shortcodes</span>
                                                <div class="listing">
                                                    <ul class="ul-listing">
                                                        <li class="first-label">
                                                            <div class="span-label">Shortcodes</div>
                                                        </li>
                                                        @foreach($shortCodes as $key => $codes)
                                                            <li>
                                                                <b>
                                                                    <span id="p{{ $key }}">[{{ $codes }}]</span>
                                                                    <span class="copy-btn c1"
                                                                        data-toggle="tooltip" data-placement="right" title="Copy Shortcode"
                                                                        onclick="copyToClipboard('#p{{ $key }}', '.c{{ $key }}')">COPY
                                                                    </span>
                                                                </b>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col p-d-0 m-b-10">
                                                <div class="input-group input-group-prepend">
                                                        <span class="input-group-text br-0 border- right-0 rounded-left">
                                                            <i class="fa fa-list" aria-hidden="true"></i>
                                                        </span>
                                                    @php $errorMessage = $errors->has('message') ? 'form-control message input-error required' : 'form-control message required'; @endphp
                                                    {!! Form::textarea(
                                                        'message',
                                                        isset($task->message) ? $task->message : $messagePlaceholder,
                                                        ['class' => $errorMessage, 'id' => 'message', 'autocomplete' => 'new-description', 'required', 'rows' => '3', 
                                                        'maxlength' => 1530]
                                                    ) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col d-none">
                                            {!! Form::label('date', 'Send Immediate' , ['class' => 'col-form-label']) !!}
                                            <br>
                                            <div class="checkbox">
                                                <label>
                                                    {!! Form::checkbox(
                                                        'send_immediately',
                                                        0,
                                                        // is_null($task->date_to_send) ? true : false,
                                                        false,
                                                        array('class' => 'custom-control-input', 'id' => 'send-checkbox-blast')
                                                    ) !!}
                                                    <span class="cr custom-control-indicator custom_checkbox_default"><i class="cr-icon fa fa-check text-success"></i></span>
                                                    Yes
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col send-immediate-status">
                                            {!! Form::label('date', 'Date' , ['class' => 'col-form-label']) !!}
                                            <div class="input-group input-append date input-group-append" id="datepicker3"
                                                data-date-format="dd-mm-yyyy">
                                                @php $errorDate = $errors->has('date_to_send') ? 'date-send input-error' : 'date-send'; @endphp
                                                <input class="form-control required {{ $errorDate }}" type="text" value="{{ isset($task->date_to_send) ? $task->date_to_send : old('date_to_send') }}" name="date_to_send" placeholder="yyyy-mm-dd">
                                                <span class="input-group-text add-on rounded-right">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col send-immediate-status">
                                            {!! Form::label('date', 'Time' , ['class' => 'col-form-label']) !!}
                                            @php $errorTime = $errors->has('time_to_send') ? 'date-send input-error' : 'date-send'; @endphp
                                            <div class="input-group clockpicker1 input-group-append {{ $errorTime }}">
                                                <input type="text" name="time_to_send" class="form-control required" data-placement="top"
                                                    data-align="top" value="{{ isset($task->time_to_send) ? $task->time_to_send : old('time_to_send') }}">
                                                <span class="input-group-text add-on rounded-right">
                                                    <i class="fa fa-clock-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <input type="hidden" id="customer_ids" name="customer_ids">
                                        <input type="hidden" id="subaccount_id" name="subaccount_id"
                                            value="{{ ( Request::input('subaccount_id') ) ? Request::input('subaccount_id'): $task->subaccount_id }}">
                                        <input type="hidden" id="client_id" name="client_id" value="{{ $clientID }}">
                                        <input type="hidden" name="credit" class="form-control" disabled id="credit-val"
                                            value="{{ $credit && $credit->pending_balance ? $credit->pending_balance : '' }}" >

                                        <h6 class="roboto-light">Remaining Credits:</h6>
                                        <h3>{{ $credit && $credit->pending_balance ? number_format($credit->pending_balance) : '0' }}</h3>
                                    </div>
                                    <div class="form-group">
                                        <h6 class="roboto-light">Estimated Character Count:</h6>
                                        <h3><div id="charNum"></div></h3>
                                        <input type="hidden" name="echarc" value="{{ isset($task->message) ? strlen($task->message) : '' }}" id="echarc">
                                    </div>
                                    <div class="form-group">
                                        <h6 class="roboto-light">Estimated Credit Cost: </h6>
                                        <h3><div id="creditNum"></div></h3>
                                        @php
                                            $settingCharLimit = isset($siteSettings) ? $siteSettings->sms_character_limit : 160;
                                            $messageLength = isset($task->message) ? strlen($task->message) : 0;

                                            $eCreditC = $messageLength ? ceil($messageLength/$settingCharLimit) : null;

                                        @endphp
                                        <input type="hidden" name="ecreditc" value="{{ $eCreditC }}" id="ecreditc">
                                    </div>
                                    <div class="form-group">
                                        <h6 class="roboto-light">Total Cost: </h6>
                                        <h3><div id="total_cost"></div></h3>
                                    </div>
                                </div>
                            </div>
                            @if ( $getTotal && $getTotal < 1 )
                                <div class="row">
                                    <div class="col-12">
                                        <h3>
                                            Total Recipient: <span class="span-count">{{ $getTotal }}</span>
                                        </h3>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    @include('templates.tasks.partials.customer-filters')
                </div>
            </div>
        </div>
        @include('partials.admin.saving', [
            'disabledButton'     => $cantProceed,
            'textSend'           => isset($task->exists) && $task->exists ? 'Send Task': 'Send Task',
            'textUpdate'         => isset($task->exists) && $task->exists ? 'Save Task': 'Save Task',
            'activation'         => false,
            'withImage'          => false,
            'imageName'          => 'Logo',
            'entity'             => isset($task) ? $task : '',
            'buttonTypeSendOnly' => true,
            'imgPath'            => isset($task) && $task->exists ? asset($task->img_logo_path) : asset(\ATM\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER),
        ])
    </div>
{!! Form::close() !!}

<div class="modal fade" id="view-recipient" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-info" role="document" style="max-width: 1000px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">All Recipient</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="display-recipients body-dropdown" style="position: relative;width: 100%; height: 700px; overflow:initial">
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.task.modal-campaign-create', [
    'taskComp' => true
])
@section('footer_scripts')
    @include('scripts.campaign.quick-creation', [
        'subaccountId' => ( Request::input('sub_account_id') ) ? Request::input('sub_account_id'): $subAccount->id,
    ])
    <script>
		$('.loader-wrapper').hide();
		$('.display-recipients > .loader-wrapper').hide();

		$('.btn-generate-all').on('click', function (e){
			e.preventDefault();
			var URL = $('#client_form').prop('action');
			$.ajax({
				url: URL + '/ajaxGenerateRecipient',
				type:"POST",
				beforeSend: function (xhr) {
					$('.loader-wrapper').show();
					var TOKEN = $('meta[name="csrf-token"]').attr('content');
					if (TOKEN) {
						return xhr.setRequestHeader('X-CSRF-TOKEN', TOKEN);
					}
				},
				data: {
					getOption : $('.select-option-filter').val(),
					subaccount_id : $('.get-subaccount-id').data('subaccount-id'),
					client_id: $('.get-client-id').data('client-id'),
					customer_group_id: $('.customergroup').val(),
					gender: $('.gender').val(),
					b_day: $('.dayfilter').val(),
					b_month: $('.monthfilter').val(),
					b_year: $('.yearpicker').val(),
					city: $('.cities').val(),
				},
				success:function(response){
					if(response.success) {
						$('.loader-wrapper').fadeOut(100);
						$('.number-execution').show();
						$('.number-execution span').html(response.data);
						$('#customer_ids').val(response.ids);
					} else {
						$('.number-execution').html(response.data);
					}
				},
			});
		});

		$('.all-recipient').on('click', function (e){
			e.preventDefault();
			$('.loader-wrapper').hide();
			$('.display-recipients > .cloader-wrapper').show();
			$('.display-recipients').addClass('customHeight');
			var URL = $('#client_form').prop('action');
			$.ajax({
				url: URL + '/ajaxViewRecipient',
				type:"POST",
				beforeSend: function (xhr) {
					$('.loader-wrapper').show();
					$('.display-recipients > .cloader-wrapper').show();
					var TOKEN = $('meta[name="csrf-token"]').attr('content');
					if (TOKEN) {
						return xhr.setRequestHeader('X-CSRF-TOKEN', TOKEN);
					}
				},
				data: {
					getOption : $('.select-option-filter').val(),
					subaccount_id : $('.get-subaccount-id').data('subaccount-id'),
					client_id: $('.get-client-id').data('client-id'),
					customer_group_id: $('.customergroup').val(),
					gender: $('.gender').val(),
					b_day: $('.dayfilter').val(),
					b_month: $('.monthfilter').val(),
					b_year: $('.yearpicker').val(),
					city: $('.cities').val(),
				},
				success:function(response){
					$('.display-recipients > .cloader-wrapper').show();
					if(response)
					{
						if (response) {
							$('.loader-wrapper').hide();
							$('.display-recipients > .cloader-wrapper').show();
							$('.display-recipients').html(response);
							setTimeout(function(){
								$('.display-recipients > .cloader-wrapper').hide();
								$('.display-recipients').removeClass('customHeight');
							}, 1000);
						} else {
							$('.display-recipients').addClass('customHeight');
						}
					} else {
						$('.display-recipients').addClass('customHeight');
						$('.display-recipients').html(response);
					}
				}
			});
		});

    </script>
    @include('scripts.admin.credit-computation', [
        'charLimit' => isset($siteSettings) ? ( $siteSettings->sms_character_limit != null ? $siteSettings->sms_character_limit : 160 ) : 160,
        'credPerLimit' => isset($siteSettings) ? ( $siteSettings->sms_credit_price_per_limit != null ? $siteSettings->sms_credit_price_per_limit : 1 ) : 1
    ])
@stop
