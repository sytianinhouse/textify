<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-9">

        @if($task->exists)
            <div class="card">
                <div class="card-header">
                    How To Use?
                </div>
                <div class="card-body">
                    <div class="m-b-10">
                        <h6>Make an API call using this URL and it will create an SMS job that either to send immediately or scheduled, base on the settings you choose below.</h6>
                        <h5><code>{{ $task->getApiUrl() }}</code></h5>
                        <br>
                        <h6>Parameters Available:</h6>
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td class="text-center" width="160"><code>security_key</code></td>
                                    <td class="text-center" width="100px"><code><em>required</em></code></td>
                                    <td>Your SECURITY KEY: <em class="font-main-blue">{{ $task->subacc_security_key }}</em></td>
                                </tr>
                                <tr>
                                    <td class="text-center"><code>app_key</code></td>
                                    <td class="text-center"><code><em>required</em></code></td>
                                    <td>Your APP KEY: <em class="font-main-blue">{{ $task->task_key }}</em></td>
                                </tr>
                                <tr>
                                    <td class="text-center"><code>first_name</code></td>
                                    <td class="text-center"><code><em>optional</em></code></td>
                                    <td>Replace with the first name of the recipient</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><code>last_name</code></td>
                                    <td class="text-center"><code><em>optional</em></code></td>
                                    <td>Replace with the last name of the recipient</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><code>number</code></td>
                                    <td class="text-center"><code><em>required</em></code></td>
                                    <td>Replace with the recipients mobile number</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><code>items</code></td>
                                    <td class="text-center"><code><em>optional</em></code></td>
                                    <td>Replace with the name of the entity you want to include in the SMS, eg: product name, service name and etc.</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><code>misc_shortcodes</code></td>
                                    <td class="text-center"><code><em>optional</em></code></td>
                                    <td>JSON string of your custom shortcodes. eg: <code>{"date":"2019-01-01","title":"Sample Code"}</code></td>
                                </tr>
                                <tr>
                                    <td class="text-center"><code>date_of_sending</code></td>
                                    <td class="text-center"><code><em>optional</em></code></td>
                                    <td>Using this parameter the API would be able to accept a specific date of sms sending,
                                        if the option below 'Accept Date of Sending' is checked the date passed in here will be followed otherwise the schedule configure below.</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><code>product_id</code></td>
                                    <td class="text-center"><code><em>optional</em></code></td>
                                    <td>Use this parameter to pass an identification for the product or service relating to this SMS job</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif

        <div class="card m-t-10">
            <div class="card-header">
                Task Details
            </div>
            <div class="card-body">

                {!! Form::model($task, ['method' => $method, 'class' => 'form-horizontal login_validator','data-subaccount'=>$subAccount->id, 'id' => 'client_form', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
                <div class="row">

                    @if(!isset($hide_sub))
                        <div class="col-6 required">
                            {!! Form::label('campaign', 'Sub Account', ['class' => 'col-form-label']) !!}
                            <input type="text" name="sub_account_id" value="{{$subAccount->id}}" class="hidden">
                            <input type="text" name="sub_account_name_exclude" disabled value="{{$subAccount->account_name}}" class="form-control">
                        </div>
                    @endif

                    <div class="col-6 required campaign-options">
                        {!! Form::label('campaign', 'Campaign', ['class' => 'col-form-label']) !!}
                        @php $errorCampaign = $errors->has('campaign') ? 'form-control chzn-select customer-group input-error' : 'form-control chzn-select customer-group'; @endphp
                        <a href="javascript:;" class="plus-icon btn btn-success quick-create-campaign" data-toggle="modal" data-target="#create-quick-campaign">
                            <i class="fa fa-plus" aria-hidden="true"></i><span>Add New</span>
                        </a>
                        {!! Form::select('campaign', $campaigns, isset($task->campaign_id) ? $task->campaign_id : null, [ 'placeholder' => '',
                            'class' => $errorCampaign,
                            'id' => 'campaign',
                            'tabindex' => 2,
                        ]) !!}
                    </div>

                    <div class="col-3 m-t-5">
                        {!! Form::label('date', 'Send Immediate:' , ['class' => 'col-form-label']) !!}
                        <br>
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox(
                                    'send_immediately',
                                    1,
                                    is_null($task->date_to_send) ? true : false,
                                    array('class' => 'custom-control-input', 'id' => 'send-checkbox')
                                ) !!}
                                <span class="cr custom-control-indicator custom_checkbox_default"><i class="cr-icon fa fa-check text-success"></i></span>
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-3 m-t-5">
                        {!! Form::label('date', 'Enable Non-Returning:' , ['class' => 'col-form-label']) !!}
                        <br>
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox(
                                    'is_nonreturning',
                                    1,
                                    null,
                                    array('class' => 'custom-control-input')
                                ) !!}
                                <span class="cr custom-control-indicator custom_checkbox_default"><i class="cr-icon fa fa-check text-success"></i></span>
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-3 m-t-5">
                        {!! Form::label('date', 'Accept Date of Sending:' , ['class' => 'col-form-label']) !!}
                        <br>
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox(
                                    'accept_datesending',
                                    1,
                                    null,
                                    array('class' => 'custom-control-input')
                                ) !!}
                                <span class="cr custom-control-indicator custom_checkbox_default"><i class="cr-icon fa fa-check text-success"></i></span>
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-12 m-t-15">
                        <div class="row">
                            <div class="col-12 required {{ is_null($task->date_to_send) ? 'hidden' : ''  }}" id="time-to-send">
                                @php
                                    $hoursSelect = collect();
                                    for ($i=1; $i <= 23; $i++) {
                                        $string = $i == 1 || $i == 0 ?  ' hour' : ' hours';
                                        $hoursSelect[$i] = $i . $string;
                                    }
                                    $minutesSelect = collect();
                                    for ($i=1; $i <= 4; $i++) {
                                        $minutesSelect[$i * 15] = $i * 15 . ' mins';
                                    }
                                    $daysSelected = $task->ots_days;
                                @endphp
                                {!! Form::label('date', 'Send After (Configure Schedule)' , ['class' => 'col-form-label']) !!}
                                <div class="row">
                                    <div class="col-4">
                                        <div class="input-group">
                                            {!! Form::number('days_to_send', $daysSelected, [ 'placeholder' => 'Days',
                                                'class' => 'form-control',
                                                'id' => 'days_to_send',
                                                'tabindex' => 2,
                                            ]) !!}
                                            <span class="input-group-text">DAYS</span>
                                        </div>
                                    </div>

                                    <div class="col-4">
                                        <div class="input-group">
                                            {!! Form::select('time_to_send[]', $hoursSelect, (isset($task->time_to_send) ? $task->ots_hours : 0), [ 'placeholder' => '',
                                                'class' => 'form-control custom-select',
                                                'id' => 'time_to_send',
                                                'tabindex' => 2,
                                            ]) !!}
                                            <span class="input-group-text">HRS</span>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="input-group">
                                            {!! Form::select('time_to_send[]', $minutesSelect, (isset($task->time_to_send) ? ltrim($task->ots_minutes, 0) : 5), [ 'placeholder' => '',
                                                'class' => 'form-control custom-select',
                                                'id' => 'time_to_send',
                                                'tabindex' => 2,
                                            ]) !!}
                                            <span class="input-group-text">MINS</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 m-t-15">
                        <div class="form-group row position-relative">
                            <div class="col required">
                                {!! Form::label('message', 'Message', ['class' => 'col-form-label']) !!}
                                <div class="list-of-short-code">
                                    <span class="shortcode-click" data-toggle="tooltip" data-placement="right" title="See all shortcodes">See list of Shortcodes</span>
                                    <div class="listing">
                                        <ul class="ul-listing">
                                            <li class="first-label">
                                                <div class="span-label">Shortcodes</div>
                                            </li>
                                            @foreach($shortCodes as $key => $codes)
                                                <li>
                                                    <b>
                                                        <span id="p{{ $key }}">[{{ $codes }}]</span>
                                                        <span class="copy-btn c1"
                                                              data-toggle="tooltip" data-placement="right" title="Copy Shortcode"
                                                              onclick="copyToClipboard('#p{{ $key }}', '.c{{ $key }}')">COPY
                                                                    </span>
                                                    </b>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="col p-d-0 m-b-10">
                                    <div class="input-group input-group-prepend">
                                                        <span class="input-group-text br-0 border- right-0 rounded-left">
                                                            <i class="fa fa-list" aria-hidden="true"></i>
                                                        </span>
                                        @php $errorMessage = $errors->has('message') ? 'form-control message input-error required' : 'form-control message required'; @endphp
                                        {!! Form::textarea(
                                            'message',
                                            isset($task->message) ? $task->message : $messagePlaceholder,
                                            ['class' => $errorMessage, 'maxlength' => 1530, 'id' => 'message', 'autocomplete' => 'new-description', 'required']
                                        ) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if($task->exists && isset($jobs))
            <div class="card m-b-10">
                <div class="card-header">
                    Created Jobs
                </div>
                <div class="card-body">
                    @include('templates.tasks.partials.task-jobs-list')
                </div>
            </div>
        @endif

        {{-- </div>
    </div> --}}
    </div>

    <div class="col-12 col-lg-3 p-l-0">
        <div class="card" id="sticky">
            <div class="card-header">
                Actions
            </div>
            <div class="card-body">
                <div class="checkbox m-b-10">
                    <label>
                        {!! Form::checkbox('active', 1, ($task->exists) ? $task->active : true, array('class' => 'custom-control-input')) !!}
                        <span class="cr custom-control-indicator custom_checkbox_default"><i class="cr-icon fa fa-check text-success"></i></span>
                        Active <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Will this task accepts API calls."></i>
                    </label>
                </div>
                @if($task->exists)
                    @include('templates.tasks.partials.task-quick-stats')
                    <hr>
                @endif
            </div>
            <div class="card-footer">
                @include('partials.admin.saving', [
                     'text' => isset($task->exists) && $task->exists ? 'Update Task' : 'Save Task',
                     'ownLayout' => false
                 ])
            </div>
        </div>
    </div>

    {!! Form::close() !!}

</div>

<div class="modal fade" id="view-recipient" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">All Recipient</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="display-recipients"></div>
            </div>
        </div>
    </div>
</div>

@include('partials.task.modal-campaign-create')

@section('footer_scripts')
    <script>
		function copyToClipboard(element, xclass) {
			var $temp = $("<input>");
			$("body").append($temp);
			$temp.val($(element).text()).select();
			document.execCommand("copy");
			$temp.remove();
			$(xclass).text("COPIED");
			setTimeout(function(){
				$('.ul-listing').removeClass('active');
				$(xclass).text("COPY");
			}, 500);
		}

		$('.loader-wrapper').hide();

		$('body').on('change','#send-checkbox', function (e) {
			e.preventDefault();
			var _self = $(this);
			var boolVal = _self.prop("checked");

			if (boolVal) {
				$('#time-to-send').addClass('hidden');
			} else {
				$('#time-to-send').removeClass('hidden');
			}
		});

    </script>

    @include('scripts.campaign.quick-creation', [
        'subaccountId' => $subAccount->id
    ])

    @include('scripts.admin.credit-computation', [
        'charLimit' => isset($siteSettings) ? $siteSettings->sms_character_limit : 160,
        'credPerLimit' => isset($siteSettings)? $siteSettings->sms_credit_price_per_limit : 1
    ])
@stop
