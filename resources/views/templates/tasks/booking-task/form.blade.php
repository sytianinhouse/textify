<booking-form
    :task-details="{{ (isset($task)) ? json_encode($task) : '' }}"
    :task-jobs="{{ (isset($jobs['jobs'])) ? json_encode($jobs['jobs']) : '[]' }}"
    :task-i-ds="{{ (isset($jobs['task_ids'])) ? json_encode($jobs['task_ids']) : 0 }}"
    :sub-account-i-d="{{ (isset($context)) ? $context->id : $subAccount->id }}"
    :client-i-d="{{ (isset($context)) ? $context->client_id : $client->id }}"
    post-action="{{ (isset($postAction)) ? $postAction : '' }}"
    :short-codes="{{ ( isset($shortCodes) ? json_encode($shortCodes) : '' ) }}"
    message-placeholder="{{ ( isset($messagePlaceholder) ? $messagePlaceholder : '' ) }}"
    reminder-message-placeholder="{{ ( isset($reminderMessagePlaceholder) ? $reminderMessagePlaceholder : '' ) }}"
    account-type="{{ ( isset($sub) ? 'sub' : 'client' ) }}"
>
</booking-form>
@include('partials.task.modal-campaign-create', [
    'vue' => TRUE
])
@section('footer_scripts')
    @include('scripts.campaign.quick-creation', [
        'subaccountId' => ( Request::input('sub_account_id') ) ? Request::input('sub_account_id'): $subAccount->id,
        'multipleSubAccount' => true
    ])
@endsection
@section('before_global_scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/vue.js') }}"></script>
@stop
