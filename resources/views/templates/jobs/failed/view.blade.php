<div class="row">
    <div class="col">
        {!! Form::model($jobsFailed, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'jobs-failed-id', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
            <div class="row">
                <div class="col-12 col-lg-9">
                    {{-- @include('partials.form-errors')
                    @include('partials.form-success') --}}
                    <div class="card">
                        <div class="card-header bg-black">
                            Job Details
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-lg-6 required">
                                    <div class="form-group required">
                                        {!! Form::label('customer_name', 'Name', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-user text-default"></i>
                                            </span>
                                            <input type="text" name="customer_name" class="form-control required {{ $errors->has('customer_name') ? 'input-error' : '' }}" id="name" value="{{ isset($jobsFailed) && !empty($jobsFailed->customer_name) ? $jobsFailed->customer_name : '' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6 required">
                                    <div class="form-group">
                                        {!! Form::label('customer_number', 'Mobile', ['class' => 'col-form-label']) !!}
                                        <div class="input-group">
                                            <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                                <i class="fa fa-mobile text-default"></i>
                                            </span>
                                            <input type="text" name="customer_number" class="mobile-phone form-control {{ $errors->has('customer_number') ? 'input-error' : '' }}" id="customer_number" data-inputmask="'mask': '9999-999-9999'" data-mask value="{{ isset($jobsFailed) && !empty($jobsFailed->customer_number) ? rmZeroFirst($jobsFailed->customer_number) : '' }}"  autocomplete="new-mobile">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row divider">
                                <div class="col">
                                    <div class="input-group">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('change_date_time', 1, (isset($jobsFailed) && !$jobsFailed->exists) ? true : null, array('class' => 'custom-control-input')) !!}
                                                <span class="cr custom-control-indicator custom_checkbox_default"><i class="cr-icon fa fa-check text-success"></i></span>
                                                Change Date and Time
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="date-time-wrapper">
                                <div class="row">
                                    <div class="col required">
                                        {!! Form::label('date', 'Date' , ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-append date input-group-append" id="datepicker3"
                                                data-date-format="dd-mm-yyyy">
                                            @php $errorDate = $errors->has('date_to_send') ? 'date-send input-error' : 'date-send'; @endphp
                                            <input class="form-control required {{ $errorDate }}" type="text" value="{{ isset($date) ? $date->format('Y-m-d') : old('date_to_send') }}" name="date_to_send" placeholder="yyyy-mm-dd" required>
                                            <span class="input-group-text add-on rounded-right">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-6 required">
                                        {!! Form::label('time_to_send', 'Time' , ['class' => 'col-form-label']) !!}
                                        @php $errorTime = $errors->has('time_to_send') ? 'time_to_send input-error' : 'time_to_send'; @endphp
                                        <div class="input-group clockpicker1 input-group-append {{ $errorTime }}">
                                            <input type="text" name="time_to_send" class="form-control required" data-placement="top"
                                                    data-align="top" value="{{ isset($time) ? $time->format('h:i:s A') : old('time_to_send') }}" required>
                                            <span class="input-group-text add-on rounded-right">
                                                <i class="fa fa-clock-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-15">
                                <div class="col">
                                    <label for="">Message</label>
                                    <div class="preview-message">
                                        {!! $jobsFailed->message !!}
                                    </div>
                                    <textarea name="message" id="message" cols="30" rows="10" class="d-none">{!!$jobsFailed->message !!}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-3 p-l-0">
                    <div class="card" id="sticky">
                        <div class="card-body">
                            <h5>Statistic</h5>
                            <table class="table table-full-width table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td width="30%">Gateway:</td>
                                        <td width="70%" class="text-left">{{ $jobsFailed->gateway ? $jobsFailed->gateway->name : 'n/a' }}</td>
                                    </tr>
                                    <tr>
                                        <td width="50%">Credits to consumed:</td>
                                        <td width="50%" class="text-left">{{ $jobsFailed->credits_consumed }}</td>
                                    </tr>
                                    <tr>
                                        <td width="50%">Campaign:</td>
                                        <td width="50%" class="text-left">{{ $campaign->name }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            @if($jobsFailed->sms_response)
                                <div class="alert alert-success">
                                    <h5 class="text-white">Gateway Response:</h5>
                                    <span class="text-white">{{ $jobsFailed->sms_response }}</span>
                                </div>
                            @endif
                            @if($jobsFailed->error_log)
                                <div class="alert alert-danger">
                                    <h4 class="text-white">Error Response:</h4>
                                    <span class="text-white">{{ $jobsFailed->error_log }}</span>
                                </div>
                            @endif
                        </div>
                        <div class="card-footer">
                            @include('partials.admin.saving', [
                                'ownLayout' => true,
                                'text' => isset($jobsFailed) && $jobsFailed->exists ? 'Resend SMS' : 'Add SMS'
                            ])
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
