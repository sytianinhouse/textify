<div class="row">
    <div class="col">
        {!! Form::model($job, ['method' => $method, 'class' => 'form-horizontal', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
        <div class="row">
            <div class="col-12 col-lg-9">
                {{-- @include('partials.form-errors')
                @include('partials.form-success') --}}
                <div class="card">
                    <div class="card-header bg-black">
                        Job Details
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-lg-6 required">
                                <div class="form-group required">
                                    {!! Form::label('customer_name', 'Name', ['class' => 'col-form-label']) !!}
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-user text-default"></i>
                                        </span>
                                        {!! Form::text('customer_name', null, [
                                            'class' => 'form-control required ' . ($errors->has('customer_name') ? 'input-error' : ''),
                                            'id' => 'name',
                                            'required' => 'required'
                                            ]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6 required">
                                <div class="form-group">
                                    {!! Form::label('customer_number', 'Mobile', ['class' => 'col-form-label']) !!}
                                    <div class="input-group">
                                        <span class="input-group-text input-group-prepend br-0 border-right-0 rounded-left">
                                            <i class="fa fa-mobile text-default"></i>
                                        </span>
                                        {!! Form::text('customer_number', null, [
                                            'class' => 'form-control mobile-phone required ' . ($errors->has('customer_number') ? 'input-error' : ''),
                                            'id' => 'customer_number',
                                            'data-inputmask' => "'mask': '9999-999-9999'",
                                            'data-mask' => '',
                                            'required' => 'required'
                                            ]) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row divider">
                            <div class="col">
                                <div class="input-group">
                                    <div class="checkbox">
                                        <label>
                                            {!! Form::checkbox('change_date_time', 1, (isset($job) && !$job->exists) ? true : null, array('class' => 'custom-control-input')) !!}
                                            <span class="cr custom-control-indicator custom_checkbox_default"><i class="cr-icon fa fa-check text-success"></i></span>
                                            Change Date and Time
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="date-time-wrapper">
                            <div class="row">
                                <div class="col required">
                                    {!! Form::label('date', 'Date' , ['class' => 'col-form-label']) !!}
                                    <div class="input-group input-append date input-group-append" id="datepicker3"
                                         data-date-format="dd-mm-yyyy">
                                        @php $errorDate = $errors->has('date_to_send') ? 'date-send input-error' : 'date-send'; @endphp
                                        {!! Form::text('date_to_send', isset($date) ? $date->format('Y-m-d') : old('date_to_send'), [
                                            'class' => 'form-control required ' . $errorDate,
                                            'id' => 'date_to_send',
                                            'placeholder' => "yyyy-mm-dd",
                                            'required' => 'required'
                                        ]) !!}
                                        <span class="input-group-text add-on rounded-right">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6 required">
                                    {!! Form::label('time_to_send', 'Time' , ['class' => 'col-form-label']) !!}
                                    @php $errorTime = $errors->has('time_to_send') ? 'time_to_send input-error' : 'time_to_send'; @endphp
                                    <div class="input-group clockpicker1 input-group-append {{ $errorTime }}">
                                        {!! Form::text('time_to_send', isset($date) ? $date->format('h:i:s A') : old('time_to_send'), [
                                            'class' => 'form-control required ' . $errorDate,
                                            'id' => 'time_to_send',
                                            'data-align' => "top",
                                            'required' => 'required'
                                        ]) !!}
                                        <span class="input-group-text add-on rounded-right">
                                            <i class="fa fa-clock-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col">
                                <label for="">Message</label>
                                @if (($job->table != 'jobs_completed') && (isClientRelux() || isClientDreamNails() || isClientNailLuxe())) 
                                <br>
                                <textarea name="message" id="message" cols="120" rows="10">{!!$job->message !!}</textarea>
                                @else
                                <div class="preview-message">
                                    {!! $job->message !!}
                                </div>
                                <textarea name="message" id="message" cols="160" rows="10" class="d-none">{!!$job->message !!}</textarea>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-3 p-l-0">
                <div class="card" id="sticky">
                    <div class="card-body">
                        <h5>Statistic</h5>
                        <table class="table table-full-width table-striped table-bordered">
                            <tbody>
                            <tr>
                                <td width="30%">Status</td>
                                <td width="70%">
                                    @if( $job->table == 'jobs_completed' || strstr($job->table, 'jobs_completed') )
                                        <img src="{{ asset('images/sms-sent@20.png') }}" alt="sms-sent-icon" title="SMS Sent"> SMS Sent
                                    @elseif( $job->table == 'jobs_failed' || strstr($job->table, 'jobs_failed') )
                                        <img src="{{ asset('images/sms-fail@20.png') }}" alt="sms-fail-icon" title="SMS Failed"> SMS Failed
                                    @else
                                        <img src="{{ asset('images/sms-onqueue@20.png') }}" alt="sms-onqueue-icon" title="SMS Onqueue"> Onqueue
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td width="30%">Date/time of sending:</td>
                                <td width="70%" class="text-left">{{ $job->date_time_sending->toDayDateTimeString() }}</td>
                            </tr>
                            <tr>
                                <td width="30%">Gateway:</td>
                                <td width="70%" class="text-left">{{ $job->gateway ? $job->gateway->name : 'n/a' }}</td>
                            </tr>
                            <tr>
                                <td width="50%">Credits to consumed:</td>
                                <td width="50%" class="text-left">{{ $job->credits_consumed }}</td>
                            </tr>
                            <tr>
                                <td width="50%">Campaign:</td>
                                <td width="50%" class="text-left">{{ $campaign->name }}</td>
                            </tr>
                            </tbody>
                        </table>
                        @if($job->sms_response && request('admin'))
                            <div class="alert alert-success">
                                <h5 class="text-white">Gateway Response:</h5>
                                <span class="text-white">{{ $job->sms_response }}</span>
                            </div>
                        @endif
                        @if($job->error_log)
                            <div class="alert alert-danger">
                                <h4 class="text-white">Error Response:</h4>
                                <span class="text-white">{{ $job->error_log }}</span>
                            </div>
                        @endif
                    </div>
                    <div class="card-footer">
                        @if( !$job->table == 'jobs_completed' || !strstr($job->table, 'jobs_completed') )
                            <button type="submit" class="btn btn-success btn-block"><i class="fa fa-save"></i> &nbsp;&nbsp;Save Changes</button>
                        @elseif( $job->table == 'jobs_failed' || strstr($job->table, 'jobs_failed') )
                            <button type="submit" class="btn btn-success btn-block"><i class="fa fa-send"></i> &nbsp;&nbsp;Resend SMS</button>
                        @endif
                        @if( !$job->table == 'jobs_completed' || !strstr($job->table, 'jobs_completed') )
                            <a href="{{ $router($deleteRoute, [$job, get_class($job)]) }}" class="btn btn-warning btn-block confirmDelete"><i class="fa fa-trash"></i> &nbsp;&nbsp;Delete SMS</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
