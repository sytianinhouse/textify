{!! Form::model($user, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'profile_form', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
    <div class="row no-gutters align-items-center">
        <div class="col-12 col-md-4 align-middle">
            <div class="card m-b-10 m-r-10 card-same-height">
                <div class="card-header">
                    Profile Picture
                </div>
                <div class="card-body">
                    <div class="profile-pic align-middle">
                        <input type="text" name="has_upload" class="has-upload" value="" hidden>
                        <input name="thumb" type="file" data-placeholder="{{asset('images/placeholder.png')}}" data-default-file="{{ isset($user) && $user->exists ? asset($user->profile_img_path) : '' }}" class="dropify img-thumbnail" data-height="400"/>
                    </div>
                </div>
                <div class="card-footer">
                    <input name="input_submit_task" class="btn btn-info form-control" type="submit" value="Save Changes">
                </div>
            </div>
        </div>
        <div class="col-12 col-md-8">
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-10 card-same-height">
                        <div class="card-header">
                            Details
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered table-striped flip-content">
                                <tbody>
                                    <tr>
                                        <td width="15%" class="align-middle">Username:</td>
                                        <td>
                                            <input type="text" name="username" class="form-control required {{ $errors->has('username') ? 'input-error' : '' }}"
                                            id="username" value="{{ isset($user->username) ? $user->username : '...' }}" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%" class="align-middle">Password :</td>
                                        <td>
                                            <input type="password" name="password" class="form-control required" id="password" value="" {{ !isset($user->password) ? 'required' : '' }}>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%" class="align-middle">Confirm Password :</td>
                                        <td>
                                            <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" value="" {{ !isset($user->password) ? 'required' : '' }}>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%" class="align-middle">First Name :</td>
                                        <td>
                                            <input type="text" name="first_name" class="form-control required {{ $errors->has('first_name') ? 'input-error' : '' }}"
                                            id="first_name" value="{{ isset($user->first_name) ? $user->first_name : '...' }}" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%" class="align-middle">Last Name :</td>
                                        <td>
                                            <input type="text" name="last_name" class="form-control required {{ $errors->has('last_name') ? 'input-error' : '' }}"
                                            id="last_name" value="{{ isset($user->last_name) ? $user->last_name : '...' }}" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%" class="align-middle">Email :</td>
                                        <td>
                                            <input type="text" name="email" class="form-control required {{ $errors->has('email') ? 'input-error' : '' }}"
                                            id="email" value="{{ isset($user->email) ? $user->email : '...' }}" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%" class="align-middle">Mobile No. :</td>
                                        <td>
                                            <input type="text" name="mobile_no" class="form-control required {{ $errors->has('mobile_no') ? 'input-error' : '' }}"
                                            id="mobile_no" value="{{ isset($user->mobile_no) ? $user->mobile_no : '...' }}" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%" class="align-middle">Telephone No. :</td>
                                        <td>
                                            <input type="text" name="telephone" class="form-control required {{ $errors->has('telephone') ? 'input-error' : '' }}"
                                            id="telephone" value="{{ isset($user->telephone) ? $user->telephone : '...' }}" required>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{!! Form::close() !!}
