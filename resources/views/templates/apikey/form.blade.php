<div class="row">
    <div class="col">
        {!! Form::model($api_key, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'sub_account_form', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
            <div class="row">
                <div class="col-12 col-lg-9">
                    <div class="card">
                        <div class="card-header bg-black">
                            Primary Details
                        </div>
                        <div class="card-body">
                            <div class="row m-t-10">
                                <div class="col-12 {{ ($subAccount->count() > 1) ? 'col-lg-6' : 'col-lg-6' }}">
                                    <div class="form-group required">
                                        {!! Form::label('name', 'Name', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-user text-default"></i>
                                            </span>
                                            <input type="text" name="name" class="form-control required {{ $errors->has('name') ? 'input-error' : '' }}" id="name" value="{{ isset($api_key) && !empty($api_key->name) ?  $api_key->name : old('name') }}"  autocomplete="new-name" required>
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="col-12 {{ ($subAccount->count() > 1) ? 'col-lg-6' : 'd-none' }}">
                                    <div class="form-group required">
                                        {!! Form::label('sub_account_id', 'Sub-account', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-id-badge text-default"></i>
                                            </span>
                                            <select class="form-control {{ ($subAccount->count() > 1) ? 'chzn-select' : '' }} {{ $errors->has('sub_account_id') ? 'input-error' : '' }}" id="sub_account_id" {{ ($subAccount->count() > 1) ? 'multiple':'' }}   name="sub_account_id[]" style="display: inline-block;">
                                                @if ($subAccount)
                                                    @foreach($subAccount as $theSubAccount)
                                                        <option value="{{ $theSubAccount->id }}" {{ ( isset($currentSubAccount) && $currentSubAccount->contains( $theSubAccount->id ) ) ? 'selected' : '' }}>{{ $theSubAccount->account_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>--}}
                                <div class="col-12 {{ ($subAccount->count() > 1) ? 'col-lg-6' : 'col-lg-6' }}">
                                    <div class="form-group required">
                                        {!! Form::label('api_key', 'Key', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-key text-default"></i>
                                            </span>
                                            <input type="text" name="api_key"
                                                   class="form-control required {{ $errors->has('api_key') ? 'input-error' : '' }}"
                                                   id="api_key" value="{{ isset($api_key->api_key) ? $api_key->api_key : '' }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        {!! Form::label('description', 'Description', ['class' => 'col-form-label']) !!}
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text br-0 border-right-0 rounded-left">
                                                <i class="fa fa-comments text-default"></i>
                                            </span>
                                            <textarea name="description" class="form-control {{ $errors->has('description') ? 'input-error' : '' }}" id="description" cols="30" rows="3"  autocomplete="new-description">{{ isset($api_key) && !empty($api_key->description) ? $api_key->description : old('description') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('partials.admin.saving', [
                    'text'       => isset($api_key) && $api_key->exists ? 'Update Gateway Key': 'Add Gateway Key',
                    'activation' => false,
                    'withImage'  => false,
                    'entity'     => isset($api_key) ? $api_key : '',
                ])
            </div>
        {!! Form::close() !!}
    </div>
</div>
