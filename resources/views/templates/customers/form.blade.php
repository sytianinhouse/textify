<div class="row">
    <div class="col-12 col-sm-9">
        <div class="card customer-wrapper">
            <div class="card-header">
                Customer Details
            </div>
            <div class="card-body">
                {!! Form::model($customer, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'customer_form', 'enctype' => "multipart/form-data", 'autocomplete' => 'off']) !!}
                <div class="row">
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-sm-6 required">
                                {!! Form::label('first_name', 'First Name', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-user text-default"></i>
                                        </span>
                                        <input type="text" name="first_name" class="form-control required {{ $errors->has('first_name') ? 'input-error' : '' }}" id="first_name" value="{{ isset($customer) && !empty($customer->first_name) ? $customer->first_name : old('first_name') }}"  autocomplete="new-fname" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                {!! Form::label('last_name', 'Last Name', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-user text-default"></i>
                                        </span>
                                        <input type="text" name="last_name" class="form-control" id="last_name" value="{{ isset($customer) && !empty($customer->last_name) ? $customer->last_name : old('last_name') }}"  autocomplete="new-lname">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row m-t-15">
                            <div class="col">
                                {!! Form::label('email', 'email', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                        </span>
                                        <input class="form-control" id="email" name="email" type="text" value="{{ isset($customer) && !empty($customer->email) ? $customer->email : old('email') }}" autocomplete="new-email">
                                    </div>
                                </div>
                            </div>
                            <div class="col required">
                                {!! Form::label('mobile', 'Mobile', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-mobile" aria-hidden="true"></i>
                                        </span>
                                        <input class="mobile-phone form-control required{{ $errors->has('mobile') ? 'input-error' : '' }}" id="mobile" name="mobile" type="text" data-inputmask="'mask': '(999) 999-9999'" data-mask value="{{ isset($customer) && !empty($customer->mobile) ? rmZeroFirst($customer->mobile) : old('mobile') }}" required autocomplete="new-mobile">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                {!! Form::label('telephone', 'Telephone', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                        </span>
                                        <input class="phone form-control" id="telephone" name="telephone" type="text" data-inputmask="'mask': '(02) 000-0000'" data-mask value="{{ isset($customer) && !empty($customer->telephone) ? $customer->telephone : old('telephone') }}"  autocomplete="new-telephone">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row m-t-15">
                            <div class="col">
                                {!! Form::label('gender', 'Gender', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-venus-mars" aria-hidden="true"></i>
                                        </span>
                                        {!! Form::select('gender', [' ' => 'Select Gender', 'male' => 'Male', 'female' => 'Female'], ( isset($customer) && !empty($customer->gender) ) ? $customer->gender : '', ['class' => 'form-control', 'id' => 'gender']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                {!! Form::label('birthdate', 'Birth Date', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </span>
                                        <input class="form-control" id="birthdate" name="birthdate" type="date" value="{{ isset($birthday) && !empty($birthday) ? $birthday : old('birthdate') }}" autocomplete="new-bday">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                {!! Form::label('occupation', 'Occupation', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </span>
                                        <input class="form-control" id="occupation" name="occupation" type="text" value="{{ isset($customer) && !empty($customer->occupation) ? $customer->occupation : old('occupation') }}" autocomplete="new-occupation">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row m-t-15">
                            <div class="col">
                                {!! Form::label('address_1', 'Address 1', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-home text-default"></i>
                                        </span>
                                        <input class="form-control" id="address_1" name="address_1" type="text" value="{{ isset($customer) && !empty($customer->address_1) ? $customer->address_1 : old('address_1') }}"  autocomplete="new-address-1">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row m-t-15">
                            <div class="col">
                                {!! Form::label('address_2', 'Address 2', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-home text-default"></i>
                                        </span>
                                        <input class="form-control" id="address_2" name="address_2" type="text" value="{{ isset($customer) && !empty($customer->address_2) ? $customer->address_2 : old('address_2') }}"  autocomplete="new-address-2">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row m-t-15">
                            <div class="col">
                                {!! Form::label('city', 'City', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group">
                                        <span class="input-group-addon input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-building text-default"></i>
                                        </span>
                                        @php $errorcities = $errors->has('city') ? 'form-control chzn-select input-error' : 'form-control chzn-select'; @endphp
                                        {!! Form::select('city', $cities, $selectedCity, [
                                            'class' => $errorcities,
                                            'id' => 'city',
                                            'tabindex' => 2,
                                            'placeholder' => 'Select a City'
                                        ]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                {!! Form::label('postal_code', 'Postal Code', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-map-pin text-default"></i>
                                        </span>
                                        <input class="form-control" id="postal_code" name="postal_code" type="text" value="{{ isset($customer) && !empty($customer->postal_code) ? $customer->postal_code : old('postal_code') }}" autocomplete="new-postal">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                {!! Form::label('country', 'Country', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-flag text-default"></i>
                                        </span>

                                        @php $errorCountries = $errors->has('countries') ? 'form-control chzn-select input-error' : 'form-control chzn-select'; @endphp
                                        {!! Form::select('country', $countries, $selectedCountry, [
                                            'class' => $errorCountries,
                                            'id' => 'countries',
                                            'tabindex' => 2,
                                            'placeholder' => 'Select a Country'
                                        ]) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row m-t-15">
                            <div class="col">
                                {!! Form::label('notes', 'Notes', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-commenting" aria-hidden="true"></i>
                                        </span>
                                        <textarea class="form-control  {{ $errors->has('notes') ? 'input-error' : '' }}" id="notes" name="notes" autocomplete="new-notes">{{ isset($customer) && !empty($customer->notes) ? $customer->notes : old('notes') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row m-t-15">
                            @if($isRegularUser)
                                <div class="col required">
                                    {!! Form::label('sub_accounts', 'Sub Accounts', ['class' => 'col-form-label']) !!}
                                    @php
                                        $errorSubaccount = $errors->has('sub_accounts') ? 'form-control chzn-select sub-account input-error' : 'form-control chzn-select sub-account';
                                        if ($subaccounts->count() == 1) {
                                            foreach ($subaccounts as $key => $sub) {
                                                $selectedSubAccnt = $key;
                                            }
                                        }
                                    @endphp
                                    {!!
                                        Form::select('sub_accounts', $subaccounts, $selectedSubAccnt, [ 'placeholder' => '',
                                            'class' => $errorSubaccount,
                                            'id' => 'sub_accounts',
                                            'tabindex' => 2,
                                        ])
                                    !!}
                                </div>
                            @endif
                            <div class="col">
                                @php
                                    if ( $allCustomerGroups->count() > 0 ):
                                @endphp
                                    {!! Form::label('customer_group', 'Group', ['class' => 'col-form-label']) !!}
                                    @php
                                        echo '<div class="edit-display-wrapper">';
                                            echo '<select class="form-control chzn-select" id="customer_group" multiple name="customer_group[]">';
                                                foreach ($allCustomerGroups as $vars ):
                                                    echo '<option value="'.$vars->id.'" '.(( isset($selectedCustomerGroups) && $selectedCustomerGroups->contains( $vars->id ) ) ? 'selected' : '').'>'.$vars->name.'</option>';
                                                endforeach;
                                            echo '</select>';
                                        echo '</div>';
                                        echo '<div class="execute-here"></div>';
                                    @endphp
                                @php
                                    // else:
                                @endphp
                                    {{-- <div class="execute-here"></div> --}}
                                @php
                                    endif;
                                @endphp
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.admin.saving', [
        'text' => $customer->exists ? 'Update Contact' : 'Add Contact',
        'activation' => false,
        'withImage' => false,
        'entity' => isset($customer) ? $customer : '',
        'withCreationDetails' => true
    ])

    {!! Form::close() !!}
</div>

@section('footer_scripts')
    <script>
        /*// menu js
        $('.edit-display-wrapper').show();
        $('.sub-account').change( function (){
            var URL = $('#customer_form').prop('action');
            var $catId = $(this).val();
            $.ajax({
                url: URL+'/ajax_subaccount',
                type:"POST",
                beforeSend: function (xhr) {
                    var TOKEN = $('meta[name="csrf-token"]').attr('content');
                    if (TOKEN) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', TOKEN);
                    }
                },
                data: { subaccount : $catId },
                success:function(response){
                    console.log(response);
                    if(response.success)
                    {
                        var branchName = $('#customer_group').empty();
                        $.each(response.data, function(i, employee){
                            $('<option/>', {
                                value:employee.id,
                                text:employee.name
                            }).appendTo(branchName);
                        })
                        if (response.edit) {
                            $('.edit-display-wrapper').hide();
                            $('.execute-here').html(response.data);
                        } else {
                            $('.edit-display-wrapper').hide();
                            $('.execute-here').html(response.data);
                        }
                    } else {
                        // alert();
                    }
                },
                error:function(){
                    // alert("error!!!!");
                }
            }); //end of ajax
        });*/
    </script>
@stop
