<div class="modal fade" id="normal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="{{ $actionRoute }}" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Customers Excel Import</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    This import features supports only this <strong><a href="{{ asset('files/excel-templates/Customer Import Template.xlsx') }}" download>excel template</a></strong>, download this file then populate it according to your records;
                </p>
                    <div class="form-group">
                        <label for="file" class="control-label">Import File:</label>
                        <input type="file" name="file" class="form-control">
                        <input type="hidden" name="client_id" value="{{ $clientId }}">
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" type="submit">Start Import <i class="fa fa-arrow-up"></i></button>
                <button class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-close"></i></button>
            </div>
        </div>
        </form>
    </div>
</div>