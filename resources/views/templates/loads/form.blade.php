<div class="row">
    <div class="col-12">
        @if($projects->count() < 1)
            @php $cantProceed = true; @endphp
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i>
                </button>
                <strong>Warning!</strong>
                Creation of this load will not proceed because projects has no sub account.
            </div>
        @endif
    </div>
    <div class="col-12 col-sm-9">
        <div class="card">
            <div class="card-header">
                Load Details
            </div>
            <div class="card-body">
                {!! Form::model($load, ['method' => $method, 'class' => 'form-horizontal login_validator', 'id' => 'load_form', 'autocomplete' => 'off']) !!}
                <div class="row">
                    <div class="col-4" id="project_select_cont">
                        <div class="form-group row">
                            <div class="col required">
                                {!! Form::label('project', 'Project', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0 custom-select-absolute">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-text-width" aria-hidden="true"></i>
                                        </span>
                                        <select id="project_select" name="project" class="form-control chzn-select {{ $errors->has('project') ? 'input-error' : '' }}">
                                            <option value="">Select a Project</option>
                                            @foreach($projects as $project)
                                                <option value="{{ $project->id }}">{{ $project->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if($errors->has('project'))
                                    <span class="has-error">{{ $errors->first('project') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-4" id="client_select_cont">
                        <div class="form-group row">
                            <div class="col required">
                                {!! Form::label('client', 'Client', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0 custom-select-absolute">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-text-width" aria-hidden="true"></i>
                                        </span>
                                        <select id="client_select" name="client" class="form-control chzn-select {{ $errors->has('client') ? 'input-error' : '' }}">
                                            <option value="">Select Client</option>
                                            @foreach($clients as $client)
                                                @if(count(old()) > 0 && $client->project->id == old('project'))
                                                <option value="{{ $client->id }}">{{ $client->company_name }}</option>
                                                @else
                                                <option value="{{ $client->id }}">{{ $client->company_name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if($errors->has('client'))
                                    <span class="has-error">{{ $errors->first('client') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-4" id="sub_select_cont">
                        <div class="form-group row">
                            <div class="col required">
                                {!! Form::label('sub_account', 'Sub Account', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0 custom-select-absolute">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-text-width" aria-hidden="true"></i>
                                        </span>
                                        <select id="sub_account_select" name="sub_account" class="form-control chzn-select {{ $errors->has('sub_account') ? 'input-error' : '' }}">
                                            <option value="">Select Sub Account</option>
                                            @foreach($subAccounts as $subAccount)
                                                @if(count(old()) > 0 && $subAccount->client->project->id == old('project'))
                                                <option value="{{ $subAccount->id }}">{{ $subAccount->account_name }}</option>
                                                @else
                                                <option value="{{ $subAccount->id }}">{{ $subAccount->account_name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if($errors->has('sub_account'))
                                    <span class="has-error">{{ $errors->first('sub_account') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group row">
                            <div class="col required">
                                {!! Form::label('date', 'Date', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </span>
                                        <input type="text" name="date" autocomplete="off"
                                               class="form-control datepicker {{ $errors->has('date') ? 'input-error' : '' }}" id="date" value="{{ isset($load) && !empty($load->date) ? $load->date : '' }}" autocomplete="new-date-val">
                                    </div>
                                </div>
                                @if($errors->has('date'))
                                    <span class="has-error">{{ $errors->first('date') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group row">
                            <div class="col required">
                                {!! Form::label('load_amount', 'Load Amount', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                        </span>
                                        <input type="number" name="load_amount" class="form-control {{ $errors->has('load_amount') ? 'input-error' : '' }}" id="load_amount" value="{{ isset($load) && !empty($load->load_amount) ? $load->load_amount : '' }}" autocomplete="new-load-amount">
                                    </div>
                                </div>
                                @if($errors->has('load_amount'))
                                    <span class="has-error">{{ $errors->first('load_amount') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group row">
                            <div class="col">
                                {!! Form::label('notes', 'Notes', ['class' => 'col-form-label']) !!}
                                <div class="col p-d-0">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text br-0 border-right-0 rounded-left">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                        </span>
                                        @php $classString = $errors->has('notes') ? 'form-control input-error' : 'form-control'; @endphp
                                        {!! Form::textarea('notes', null, ['class' => $classString, 'id' => 'notes', 'rows' => '3', 'autocomplete' => 'new-notes']) !!}
                                        {{-- <textarea type="text" name="notes" class="form-control {{ $errors->has('notes') ? 'input-error' : '' }}" id="notes"  rows="3">
                                            {{ isset($load) && !empty($load->notes) ? $load->notes : '' }}
                                        </textarea> --}}
                                    </div>
                                </div>
                                @if($errors->has('notes'))
                                    <span class="has-error">{{ $errors->first('notes') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.admin.saving', [
        'text' => $load->exists ? 'Update Load' : 'Add Load',
        'activation' => false,
        'withImage' => false,
        'imageName' => 'Logo',
        'entity' => isset($load) ? $load : '',
        'imgPath' =>  isset($load) && $load->exists ? asset($load->img_logo_path) : asset(\ATM\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER),

    ])

    {!! Form::close() !!}
</div>
