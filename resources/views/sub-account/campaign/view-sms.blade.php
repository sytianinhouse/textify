@extends('layouts.fixed_header_sub_account')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="breadcrumb-item"><a href="{{ sroute('client.camp.index') }}">Campaigns</a></li>
    <li class="active breadcrumb-item">SMS Sent per Campaign</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row no-gutters">
                <div class="col">
                    <h3 class="m-b-0">
                        SMS Sent
                    </h3>
                </div>
            </div>
        </div>
    </div>
    @include('templates.campaigns.view-sms', [
        'jobs' => $jobs,
    ])
@stop
