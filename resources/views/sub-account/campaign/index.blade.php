@extends('layouts.fixed_header_sub_account')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="active breadcrumb-item">Campaigns</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row no-gutters">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    @can('access', 'create-campaigns')
                        <div class="btn-group float-right">
                            <a href="{{ sroute('sub.camp.create') }}" class="btn btn-success m-r-5">
                                Add Campaign <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    @endcan
                </div>
            </div>
        </div>
    </div>
    {{-- @include('partials.form-success') --}}
    @include('partials.filters')
    <div class="card">
        <div class="card-body" id="user_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer this-table" id="datatable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Campaign Name</th>
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Sub Account</th>
                            {{-- <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Description</th>
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Created At</th> --}}
                            <th class="sorting wid-25" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse( $campaign as $theCampaign )
                            <tr>
                                <td>
                                    <a href="{{ sroute('sub.camp.edit', $theCampaign) }}" title="Edit">
                                            {{ $theCampaign->name }}
                                    </a>
                                </td>
                                {{-- @foreach ( $subaccounts as $key => $sub )
                                    @if( $key == $theCampaign->subaccount_id )
                                        <td>{{ $sub }}</td>
                                    @endif
                                @endforeach --}}
                                <td>
                                @if ($subaccounts && $subaccounts->count() > 0)
                                <ul>
                                    @foreach ( $subaccounts as $key => $sub )
                                        @if( $key == $theCampaign->subaccount_id )
                                            <li>{{ $sub }}</li>
                                        @endif
                                    @endforeach
                                </ul>
                                @endif
                                </td>
                                {{-- <td>
                                    {{ str_limit( $theCampaign->description, 100, "...." ) }}
                                </td>
                                <td>
                                    {{ $theCampaign->created_at->toDateTimeString() }}
                                </td> --}}
                                <td>
                                    @can('access', 'update-campaigns')
                                        <a href="{{ sroute('sub.camp.edit', $theCampaign) }}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    @endcan
                                    <a href="{{ sroute('sub.camp.sentSMS', $theCampaign) }}" title="View Sent SMS" class="btn btn-sm btn-success"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                    {{-- <a href="{{ route('client.camp.delete', $theCampaign) }}" title="Delete" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a> --}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9">No records found!</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- /.inner -->
    <!-- /.outer -->
@stop
