@extends('layouts.fixed_header_sub_account')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="active breadcrumb-item">Groups</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row no-gutters">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ sroute('sub.customer-group.create') }}" class=" btn btn-success m-r-5">
                            Add Group&nbsp;&nbsp;<i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.filters', [
        'noDaterange' => true,
        'filterSearchAnyKey' => true
    ])

    <div class="card">
        <div class="card-body" id="project_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Name</th>
                        <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Description</th>
                        <th class="sorting wid-25" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                    </tr>
                    </thead>
                     <tbody>
                         @if( $customergroup )
                            @foreach($customergroup as $customerg)
                            <tr>
                                <td>{{ $customerg->name }}</td>
                                @php
                                    $string = substr( strip_tags(htmlspecialchars_decode($customerg->description)), 0, 30) . ' ...';
                                @endphp
                                <td>{{ $string }}</td>
                                <td>
                                    <a href="{{ sroute('sub.customer-group.edit', [$customerg]) }}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{ sroute('sub.customer-group.delete', [$customerg]) }}" title="Delete" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                             </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
               {{--  <div class="text-center">
                    @if(isLengthAware($customergroup))
                        {!! $customergroup->appends($filters)->links() !!}
                    @else
                        <div class="alert alert-dark" role="alert">
                            Showing all items
                        </div>
                    @endif
                </div> --}}
            </div>
        </div>
    </div>

@stop
