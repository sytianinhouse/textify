@extends('layouts.fixed_header_client')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="active breadcrumb-item">{{ $title }}</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row no-gutters">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}

                        @if($begin)
                            @include('partials.report.inc.excel-button', [
                                'url' => sroute('sub.report.job.sms.pending', array_merge($filters, ['export_excel' => true]))
                            ])
                        @endif
                    </h3>
                </div>
            </div>
        </div>
    </div>

    @include('partials.filters', [
        'campaignFilter' => true,
    ])

    @if($begin)
        @include('partials.report.jobs-pending', [
            'router' => 'route',
            'perDayLink' => 'client.report.job.sms.sent',
            'customerDetailsLink' => 'client.customer.edit',
            'campaignLink' => 'client.camp.edit',
            'jobLink' => 'client.jobs.view'
        ])
    @else
        <div class="card m-t-5">
            <div class="card-body text-center">
                <div class="m-t-10 m-b-10">
                    <h4>@lang('descriptions.reports.introduction')</h4>
                </div>
            </div>
        </div>
    @endif
@stop
