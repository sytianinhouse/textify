@extends('layouts.fixed_header_sub_account')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ $breadCrumbsUrl }}">{{ $breadCrumbsLabel }}</a>
    </li>
    <li class="breadcrumb-item">
    @if($task->isImmediateTask())
        <a href="{{ sroute('sub.task.it.edit', $task) }}">Edit - Task</a>
    @elseif($task->isOneTimeSendTask())
        <a href="{{ sroute('sub.task.ots.edit', $task) }}">Edit - Task</a>
    @elseif($task->isTextBlastTask())
        <a href="{{ sroute('sub.task.tbs.edit', $task) }}">Edit - Task</a>
    @endif
    </li>
    <li class="active breadcrumb-item">{{ $title }}</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row no-gutters">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
            </div>
        </div>
    </div>
    @include('templates.jobs.failed.view', [
        'jobs' => $jobsFailed,
        'method' => 'put',
    ])
@stop
