@extends('layouts.fixed_header_sub_account')

{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : '' }}
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="active breadcrumb-item">Users</li>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    @can('access', 'create-users')
                        <div class="btn-group float-right">
                            <a href="{{ sroute('sub.user.create') }}" class=" btn btn-success m-r-5">
                                Add User <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    @endcan
                </div>
            </div>
        </div>
    </div>

    @include('partials.filters',[
        'noDaterange' => true,
        'filterSearchAnyKey' => true
    ])

    <div class="card">
        <div class="card-body" id="user_body">
            <div id="sample_1_wrapper" class="dataTables_wrapper dt-boostrap no-footer">
                <div class="table-responsive">
                    <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Name</th>
                            <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">Username</th>
                            <th class="sorting wid-20" tabindex="0" rowspan="1" colspan="1">Acount Type</th>
                            <th class="sorting wid-20" tabindex="0" rowspan="1" colspan="1">Sub Account</th>
                            <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">Last Login</th>
                            <th class="sorting wid-10" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ ( $user->acct_type == 'Account User' ) ? 'Regular User' : $user->acct_type }}</td>
                                    <td>
                                        @foreach( $user->subAccounts as $sub )
                                            {!! "{$sub->account_name}<br>" !!}
                                        @endforeach
                                    </td>
                                    {{-- <td>{{ $user->roles }}</td> --}}
                                    <td>{{ \Carbon\Carbon::parse($user->last_login)->format('Y-m-d h:i a') }}</td>
                                    <td>
                                        @can('access', 'update-users')
                                            <a href="{{ sroute('sub.user.edit', $user)}}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        @endcan
                                        @can('access', 'delete-users')
                                            <a href="{{ sroute('sub.user.destroy', $user)}}" title="Delete" class="btn btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        @endcan
                                        {{-- @include('partials.inline-delete', [
                                            'action' => route('admin.client.users.destroy', [$client, $user]),
                                        ]) --}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- <div class="text-center">
                        @if(isLengthAware($users))
                            {!! $users->appends($filters)->links() !!}
                        @else
                            <div class="alert alert-dark" role="alert">
                                Showing all items
                            </div>
                        @endif
                    </div> --}}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

@stop
{{-- page level scripts --}}
@section('footer_scripts')
    <!--Plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/vendors/select2/js/select2.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.colVis.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons5.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.print.min.js')}}"></script>
    <!--End of plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/users.js')}}"></script>
    <!-- end page level scripts -->
@stop
