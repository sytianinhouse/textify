@extends('layouts.fixed_header_sub_account')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="breadcrumb-item"><a href="{{ sroute('sub.customer.index') }}">Contacts</a></li>
    <li class="active breadcrumb-item">Add Contact</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row no-gutters">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
            </div>
        </div>
    </div>
    @include('templates.customers.form', [
        'customer' => new \ATM\Repositories\Customers\Customer,
        'method'   => 'post',
    ])
@stop
