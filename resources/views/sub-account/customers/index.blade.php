@extends('layouts.fixed_header_sub_account')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="active breadcrumb-item">Contacts</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row no-gutters">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ sroute('sub.customer.create') }}" class=" btn btn-success m-r-5">
                            Add Contact&nbsp;&nbsp;<i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.filters', [
        'noDaterange' => true,
        'filterSearchAnyKey' => true,
        'filterSubAccount' => true
    ])
    <div class="card">
        <div class="card-body" id="project_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Full Name</th>
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Mobile</th>
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Gender</th>
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">City</th>
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Group</th>
                            <th class="sorting wid-25" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( $customer )
                            @foreach($customer as $custom )
                                <tr>
                                    <td>{{ $custom->first_name .' '. $custom->last_name }}</td>
                                    <td>{{ formattedNumber($custom->mobile) }}</td>
                                    <td>{{ ucwords($custom->gender) }}</td>
                                    <td>{{ isset($custom->city) ? ucwords($custom->cityRel->name) : '' }}</td>
                                    <td>
                                        @php
                                            $groupName = array();
                                            foreach ($custom->customerGroups as $customerGroup):
                                                $groupName[] = $customerGroup->name;
                                            endforeach;
                                            echo implode(", ", $groupName);
                                        @endphp
                                    </td>
                                    <td>
                                        <a href="{{ sroute('sub.customer.edit', [$custom] ) }}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="{{ sroute('sub.customer.delete', [$custom] ) }}" title="Delete" class="btn btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                {{-- <div class="text-center">
                        @if(isLengthAware($customer))
                            {!! $customer->appends($filters)->links() !!}
                        @else
                            <div class="alert alert-dark" role="alert">
                                Showing all items
                            </div>
                        @endif
                    </div> --}}
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@stop
