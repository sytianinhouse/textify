@extends('layouts.fixed_header_sub_account')
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('content')
    <div class="row no-gutters m-t-20">
        <div class="col m-r-10">
            <div class="card">
                <div class="card-header">
                    Pending Jobs
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table data-order='[[ 2, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer this-table" id="datatable" role="grid">
                            <thead>
                                <tr role="row">
                                    <th style="width: 20%;" class="text-center">Full Name</th>
                                    <th style="width: 20%;" class="text-center">Mobile #</th>
                                    <th style="width: 20%;" class="text-center">When to send</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse( $jobs as $theJob )
                                    <tr>
                                        <td class="text-center">
                                            {{ $theJob->customer_name }}
                                        </td>
                                        <td class="text-center">
                                            {{ $theJob->customer_number }}
                                        </td>
                                        <td class="text-center">
                                            @humanDateTimeFormat($theJob->when_to_send ?: '')
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No Records Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col m-l-10">
            <div class="card">
                <div class="card-header">
                    Jobs Failed
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table data-order='[[ 2, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer this-table" id="datatable" role="grid">
                            <thead>
                                <tr role="row">
                                    <th style="width: 20%;" class="text-center">Full Name</th>
                                    <th style="width: 20%;" class="text-center">Mobile #</th>
                                    <th style="width: 20%;" class="text-center">When to send</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse( $JobsFailed as $theJob )
                                    <tr>
                                        <td class="text-center">
                                            {{ $theJob->customer_name }}
                                        </td>
                                        <td class="text-center">
                                            {{ $theJob->customer_number }}
                                        </td>
                                        <td class="text-center">
                                            @humanDateTimeFormat($theJob->time_attempt_send ?: '')
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No Records Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card m-t-20">
                <div class="card-header">
                    Credit Consumption
                </div>
                <div class="card-body m-t-35">
                    <div id="area-chart" class="flotChart3" data-chart-details="{{ json_encode($credits) }}"></div>
                </div>
            </div>
        </div>
    </div>
@stop
