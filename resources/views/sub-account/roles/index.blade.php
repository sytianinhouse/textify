@extends('layouts.fixed_header_sub_account')
{{-- Page title --}}
@section('title')
    Roles
    @parent
@stop

@section('crumbs')
    <li class="active breadcrumb-item">Roles</li>
@stop
{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    @can('access', 'create-roles')
                        <div class="btn-group float-right">
                            <a href="{{ sroute('sub.role.create') }}" class=" btn btn-success m-r-5">
                                Add Role <i class="fa fa-plus"></i>
                            </a>
                            <a href="#" class=" btn btn-primary">
                                <i class="fa fa-file"></i>
                            </a>
                        </div>
                    @endcan
                </div>
            </div>
        </div>
    </div>

    {{-- @include('partials.filters') --}}

    <div class="card">
        <div class="card-body" id="project_body">
            <div id="sample_1_wrapper" class="dataTables_wrapper dt-boostrap no-footer">
                <div class="table-responsive">
                    <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Name</th>
                                {{-- <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Description</th> --}}
                                <th class="sorting wid-25" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                            </tr>
                        </thead>
                         <tbody>
                            @if( $roles )
                                @forelse($roles as $role)
                                <tr>
                                    <td>{{ $role->display_name }}</td>
                                    {{-- @php
                                        $string = substr( strip_tags(htmlspecialchars_decode($role->description)), 0, 30) . ' ...';
                                    @endphp
                                    <td>{{ $string }}</td> --}}
                                    <td>
                                        @can('access', 'update-roles')
                                            <a href="{{ sroute('sub.role.edit', $role) }}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        @endcan
                                        @can('access', 'delete-roles')
                                            @if( $role->users->count() == 0 )
                                                <a href="{{ sroute('sub.role.delete', $role) }}" title="Delete" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            @endif
                                        @endcan
                                    </td>
                                 </tr>
                                @empty
                                 <tr>
                                    <td colspan="6">No Records Found</td>
                                </tr>
                                @endforelse
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@stop
{{-- page level scripts --}}
