@extends('layouts.fixed_header_sub_account')
{{-- Page title --}}
@section('title')
    Template API
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="active breadcrumb-item">Template API</li>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ $title }}
                    </h3>
                </div>
                <div class="col">
                    @can('access', 'create-one-time-send-task')
                        <div class="btn-group float-right">
                            <a href="{{ sroute('sub.task.ots.create') }}" class=" btn btn-success m-r-5">
                                Add Task &nbsp;&nbsp;<i class="fa fa-plus"></i>
                            </a>
                        </div>
                    @endcan
                </div>
            </div>
        </div>
    </div>

    @include('partials.filters', [
        'campaignFilter' => true,
        'noDaterange' => false,
        'filterByDateToSendCreatedAt' => false,
    ])

    <div class="card">
        <div class="card-body" id="project_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Campaign Title</th>
                        <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Send After</th>
                        <th class="sorting wid-20" tabindex="0" rowspan="1" colspan="1">App Key</th>
                        <th class="sorting wid-10" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                        <tr>
                            <td>{{ $task->campaigns->name }}</td>
                            <td>{{ $task->isSendImmediate() ? 'Send Immediate' : $task->ots_formatted }}</td>
                            <td>{{ $task->task_key }}</td>
                            <td>
                                @can('access', 'update-one-time-send-task')
                                    <a href="{{sroute('sub.task.ots.edit', $task)}}" title="Edit" class="btn btn-sm btn-info">Edit</a>
                                @endcan
                                @can('access', 'delete-one-time-send-task')
                                    <a href="{{sroute('sub.task.ots.delete', $task)}}" title="Edit" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                @endcan
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- <div class="text-center">
                    @if(isLengthAware($tasks))
                        {!! $tasks->appends($filters)->links() !!}
                    @else
                        <div class="alert alert-dark" role="alert">
                            Showing all items
                        </div>
                    @endif
                </div> --}}
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@stop

{{-- @section('scripts')
    <script type="text/javascript">

        $('.btn-delete-item').on('click', function(event) {
            event.preventDefault();

            let deleteUrl = $(this).data('url');

            swal({
                title: 'Are you sure?',
                text: 'You won\'t be able to revert this.',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: 'deleteUrl'
                    })
                    .done(function() {
                        console.log("success");
                    })
                    .fail(function() {
                        console.log("error");
                    })
                    .always(function() {
                        console.log("complete");
                    });
                }
            });

            /* Act on the event */
        });

    </script>
@stop --}}
{{-- page level scripts --}}
