@extends('layouts.fixed_header_sub_account')

{{-- Page title --}}
@section('title')
    {{ $title ?: 'Title goes here' }}
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ sroute('sub.task.it.index') }}">Custom API</a>
    </li>
    <li class="breadcrumb-item active">Add Task</li>
@stop

@section('header_styles')
    <style type="text/css">
        .chosen-container {
            width: 100% !important;
        }
    </style>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
            </div>
        </div>
    </div>

    @include('templates.tasks.immediate-task.form', [
        'task' =>  new \ATM\Repositories\Tasks\Task,
        'method' => 'post',
    ])

@stop
