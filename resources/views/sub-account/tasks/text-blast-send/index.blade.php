@extends('layouts.fixed_header_sub_account')
{{-- Page title --}}
@section('title')
    Text Blast SMS
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="active breadcrumb-item">Text Blast SMS</li>
@stop

{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    @can('access', 'create-text-blast-send-task')
                        <div class="btn-group float-right">
                        <a href="{{ sroute('sub.task.tbs.create') }}" class=" btn btn-success m-r-5">
                                Add Task &nbsp;&nbsp;<i class="fa fa-plus"></i>
                            </a>
                        </div>
                    @endcan
                </div>
            </div>
        </div>
    </div>

    @include('partials.filters', [
        'campaignFilter' => true,
        'filterByDateToSendCreatedAt' => true,
        'filtersubAccounts' => true,
    ])

    <div class="card">
        <div class="card-body" id="project_body">
            <div class="table-responsive">
                <table class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1" width="15%">Campaign Title</th>
                        <th width="40%">Statistics</th>
                        <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1" width="15%">Date and Time to send</th>
                        <th class="sorting wid-10" data-orderable="false" tabindex="0" rowspan="1" colspan="1" width="20%">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                        <tr>
                            <td>{{ $task->campaigns->name }}</td>
                            <td>
                                <div class="row align-items-center">
                                    <div class="col-lg-4 col-sm-6">
                                        <div id="{{ 'gauge_'.$task->id.'-1' }}" class="gauge" data-type="jobs_count" data-color="#fdcb6e" data-total-count="{{ $task->jobs_count ? $task->jobs_count : 0 }}" data-count="{{ $task->jobs_count ? $task->jobs_count : 0 }}"></div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6">
                                        <div id="{{ 'gauge_'.$task->id.'-2' }}" class="gauge" data-type="success_jobs_count" data-color="#00b894" data-total-count="{{ $task->jobs_count ? $task->jobs_count : 0 }}" data-count="{{ $task->success_jobs_count ? $task->success_jobs_count : 0 }}"></div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6">
                                        <div id="{{ 'gauge_'.$task->id.'-3' }}" class="gauge" data-type="failed_jobs" data-color="#d63031" data-total-count="{{ $task->jobs_count ? $task->jobs_count : 0 }}" data-count="{{ $task->tasksjobsFailed ? $task->tasksjobsFailed->count() : 0 }}"></div>
                                    </div>
                                </div>
                            </td>
                            <td>{{ $task->date_time_to_send->toDayDateTimeString() }}</td>
                            <td style="text-align: center;">
                                @php
                                    if( $task->tasks_job_today_count > 0 || $task->tasks_job_tomorrow_count > 0 || $task->tasks_job_after_tomorrow_to14th_day_count > 0 || $task->tasks_job15th_day_to_end_month_count > 0 || $task->tasks_job_next_month_count > 0 || $task->tasksjobs_completed_count > 0 || $task->tasksjobs_failed_count > 0 ):
                                @endphp
                                    @can('access', 'view-text-blast-send-task')
                                        <a href="{{sroute('sub.task.tbs.view', $task) }}" title="View" class="btn btn-sm btn-info">View</a>
                                    @endcan
                                @php
                                    else:
                                @endphp
                                    @can('access', 'update-text-blast-send-task')
                                        <a href="{{sroute('sub.task.tbs.edit', $task) }}" title="Edit" class="btn btn-sm btn-info">Edit</a>
                                    @endcan
                                    @can('access', 'delete-text-blast-send-task')
                                        <a href="{{sroute('sub.task.tbs.delete', $task) }}" title="Edit" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    @endcan
                                @php
                                    endif;
                                @endphp
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- <div class="text-center">
                    @if(isLengthAware($tasks))
                        {!! $tasks->appends($filters)->links() !!}
                    @else
                        <div class="alert alert-dark" role="alert">
                            Showing all items
                        </div>
                    @endif
                </div> --}}
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@stop
{{-- page level scripts --}}
