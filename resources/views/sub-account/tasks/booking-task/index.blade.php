@extends('layouts.fixed_header_sub_account')
{{-- Page title --}}
@section('title')
    Custom API
    @parent
@stop
{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="active breadcrumb-item">Custom API</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ sroute('sub.task.booking-task.create') }}" class=" btn btn-success m-r-5">
                            Add Task &nbsp;&nbsp;<i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.filters', [
        'noDaterange' => true,
        'campaignFilter' => true,
        'filterSubAccount' => true,
    ])
    <div class="card">
        <div class="card-body" id="project_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc wid-20">Campaign</th>
                            <th class="sorting_asc wid-20">Date Created</th>
                            <th class="sorting_asc wid-10">Status</th>
                            <th class="sorting wid-30" data-orderable="false">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($tasks as $task)
                            <tr>
                                <td>{{ $task->campaign->name }}</td>
                                <td>@dateFormat($task->created_at)</td>
                                <td>{!! $task->human_status !!}</td>
                                <td>
                                    <a href="{{ sroute('sub.task.booking-task.edit', $task) }}" class="btn btn-sm btn-success">EDIT</a>
                                    @if( !$task->hasJobs() )
                                        <a href="{{ sroute('sub.task.booking-task.delete', $task) }}" class="btn btn-sm btn-danger">DELETE</a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5">No records found.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                <div class="text-center">
                    @if(isLengthAware($tasks))
                        {!! $tasks->appends($filters)->links() !!}
                    @else
                        <div class="alert alert-dark" role="alert">
                            Showing all items
                        </div>
                    @endif
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    @include('partials.task.modal-sub-accounts', [
        'url' => route('client.task.booking-task.create')
    ])
@stop
{{-- page level scripts --}}
