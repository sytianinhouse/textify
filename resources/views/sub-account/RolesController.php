<?php

namespace App\Http\Controllers\Client\Role;

use ATM\ContextInterface;
use ATM\Repositories\Client\Client;
use ATM\Repositories\Project\Project;
use ATM\Repositories\Roles\Role;
use ATM\Repositories\Roles\RoleRepository;
use ATM\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class RolesController extends Controller
{

    public function __construct(
        RoleRepository $roleRepo,
        ContextInterface $context)
    {
        $this->context = $context;
        $this->roleRepo = $this->modelRepo = $roleRepo; 
    }

    public function index(Request $request)
    {
        // $this->police('list-user-roles');
        $title = 'Roles';

        $client = $request->user()->client;
        $roles = $this->roleRepo->ofClient($client);

    	return view('sub-account.roles.index', compact('roles', 'client', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Add Role';

        return view('client.roles.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $input = $request->all();
            $client = $request->user()->client;

            $rules = [
                'name' => 'required'
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($validator);
            } else {
                $role = $this->roleRepo->add($request, $client);
            }

            if ( ! $role->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($role->getErrors())
                            ->withError("Please check required fields.");
            }

            return redirect()->route('client.role.index')
                        ->withSuccess("Successfully added Role.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client, Role $role)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $title = 'Edit User Role';
        return view('client.roles.edit', compact('role', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project, Client $client, Role $role)
    {
        try {

            $role = $this->roleRepo->update($role, $request);

            return redirect()->route('client.role.index')
                        ->withSuccess("Successfully Updated Role.");
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Client $client, Role $role)
    {
        try {
            $roleName = $this->roleRepo->delete($role);
            
            return redirect()->route('client.role.index')
                        ->withSuccess("Successfully deleted Role $roleName.");
        } catch (\DomainException $e) {
            return redirect()->back()
            				->withError($e->getMessage());
        }
    }

}
