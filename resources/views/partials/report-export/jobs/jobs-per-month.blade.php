@extends('partials.report-export.template')

@section('export_headers')
    <th>MONTH</th>
    <th style="text-align: right">RECIPIENTS</th>
    <th style="text-align: right">SMS SENT</th>
    <th style="text-align: right">SMS FAILED</th>
    <th style="text-align: right">CREDITS USED</th>
    <th style="text-align: right">SUCCESS RATE</th>
@stop

@section('export_body')
    @php $totalRecipient = 0; $totalSent = 0; $totalFailed = 0; $totalCredits = 0; @endphp
    @foreach($results as $key => $result )
        @php
            $totalRecipient += $result['recipients']; $totalSent += $result['success_sms'];
            $totalFailed += $result['failed_sms']; $totalCredits += $result['credits_used'];
        @endphp
    <tr>
        <td>{{ \Carbon\Carbon::create('2019', $key, 1)->format('F') }}</td>
        <td style="text-align: right">@numberComma($result['recipients'])</td>
        <td style="text-align: right">@numberComma($result['success_sms'])</td>
        <td style="text-align: right">@numberComma($result['failed_sms'])</td>
        <td style="text-align: right">@numberComma($result['credits_used'])</td>
        <td style="text-align: right">{{ successPercentage($result['recipients'], $result['failed_sms']) }}</td>
     </tr>
    @endforeach

@stop

@section('export_footer')
    <tr class="tfoot-reports">
        <td colspan="1">Total:</td>
        <td style="text-align: right;">@numberComma($totalRecipient)</td>
        <td style="text-align: right;">@numberComma($totalSent)</td>
        <td style="text-align: right;">@numberComma($totalFailed)</td>
        <td style="text-align: right;">@numberComma($totalCredits)</td>
        <td style="text-align: right;">{{ successPercentage( $totalRecipient, $totalFailed ) }}</td>
    </tr>
@stop

@section('export_footer_brand_colspan')
    colspan="6"
@stop