@extends('partials.report-export.template')

@section('export_headers')
    <th style="text-align: left">CAMPAIGN</th>
    <th style="text-align: left">DATE CREATED</th>
    <th style="text-align: right">RECIPIENTS</th>
    <th style="text-align: right">SMS SENT</th>
    <th style="text-align: right">SMS FAILED</th>
    <th style="text-align: right">CREDITS USED</th>
    <th style="text-align: right">SUCCESS RATE</th>
@stop

@section('export_body')
    @php $totalRecipient = 0; $totalSent = 0; $totalFailed = 0; $totalCredits = 0; @endphp
    @foreach($tasksPerCampaign as $key => $result )
    @php
        $result = (object) $result;  $date = new Carbon\Carbon($result->camp_date);
        $totalRecipient += $result->total_count; $totalSent += $result->completed_count; $totalFailed += $result->failed_count; $totalCredits += $result->total_credits;
    @endphp
    <tr>
        <td>{{ $result->name }}</td>
        <td>{{ $date->toFormattedDateString() }}</td>
        <td style="text-align:right">@numberComma($result->total_count)</td>
        <td style="text-align:right">@numberComma($result->completed_count)</td>
        <td style="text-align:right">@numberComma($result->failed_count)</td>
        <td style="text-align:right">@numberComma($result->total_credits)</td>
        <td style="text-align:right">{{ successPercentage( $result->total_count, $result->failed_count ) }}</td>
     </tr>
    @endforeach

@stop

@section('export_footer')
    <tr class="tfoot-reports">
        <td colspan="2">Total:</td>
        <td style="text-align:right">@numberComma($totalRecipient)</td>
        <td style="text-align:right">@numberComma($totalSent)</td>
        <td style="text-align:right">@numberComma($totalFailed)</td>
        <td style="text-align:right">@numberComma($totalCredits)</td>
        <td style="text-align:right">{{ successPercentage( $totalRecipient, $totalFailed ) }}</td>
    </tr>
@stop

@section('export_footer_brand_colspan')
    colspan="7"
@stop
