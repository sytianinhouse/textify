<table>
    <thead>
    <tr>
        <th>Full Name</th>
        @if(count($subaccounts) > 1)
        <th>Sub Account</th>
        @endif
        <th>Email</th>
        <th>Mobile</th>
        <th>Birthdate</th>
        <th>Gender</th>
        <th>City</th>
        <th>Group</th>
    </tr>
    </thead>
    <tbody>
    @if( $contact )
        @foreach($contact as $item )
            <tr>
                <td>
                    {{ $item->first_name .' '. $item->last_name }}
                </td>
                @if(count($subaccounts) > 1)
                    <td>{{ $item->subAccount->account_name }}</td>
                @endif
                <td>{{ $item->email }}</td>
                <td>{!! formattedNumber($item->mobile) !!}</td>
                <td>{{ $item->birth_date ? $item->birth_date->toFormattedDateString() : '' }}</td>
                <td>{{ ucwords($item->gender) }}</td>
                <td>{{ isset($item->city) ? ucwords($item->cityRel->name) : '' }}</td>
                <td>
                    @php
                        $groupName = array();
                        foreach ($item->customerGroups as $customerGroup):
                            $groupName[] = $customerGroup->name;
                        endforeach;
                        echo implode(", ",$groupName);
                    @endphp
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
    <tfoot>
    <tr>
        <td>Total</td>
        @if(count($subaccounts) > 1)
            <td></td>
        @endif
        <td colspan="6">{{ $contact->count() }}</td>
    </tr>
    </tfoot>
</table>