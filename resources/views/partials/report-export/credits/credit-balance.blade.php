@extends('partials.report-export.template')

@section('export_headers')
    <th style="text-align: right">Date</th>
    <th style="text-align: right">SMS SENT</th>
    <th style="text-align: right">CREDITS USED</th>
@stop

@section('export_body')

    @foreach($results as $key => $result )
    <tr>
        <td>{{ $result->when_to_send }}</td>
        <td style="text-align: right">{{ $result->sms_sent }}</td>
        <td style="text-align: right">{{ $result->credits_consumed }}</td>
     </tr>
    @endforeach

@stop

@section('export_footer')
@stop

@section('export_footer_brand_colspan')
    colspan="3"
@stop
