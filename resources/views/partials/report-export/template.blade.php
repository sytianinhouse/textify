<style type="text/css">
    tr:nth-child(odd) td {
        background: #e3e5e8;
        border: 1px solid #000;
    }
    table td, th {
        border: 1px solid #000;
    }
    tfoot td {
        background-color: #3ba7b0 !important;
        font-size: 14;
    }
</style>
<table>
    <thead>
        <tr style="color: #0fb0c0;">
            <td colspan="2" style="font-size: 15; font-weight: bold;">
                <span style="font-size: 13;">Client Name:</span> {{ $authClient->company_name }}
            </td>
        </tr>
        <tr style="color: #0fb0c0;">
            <td colspan="4" style="font-size: 15; font-weight: bold;">
                <span style="font-size: 13;">Date Generated:</span> {{ Carbon\Carbon::now()->toDayDateTimeString() }}
            </td>
        </tr>
        @isset($reportTitle)
        <tr style="color: #0fb0c0;">
            <td colspan="4" style="font-size: 15; font-weight: bold;">
                <span style="font-size: 13;">Report Title:</span> {{ $reportTitle }}
            </td>
        </tr>
        @endisset
        <tr></tr>
        <tr></tr>
        <tr style="background: #0fb0c0;">
            @yield('export_headers')
        </tr>
    </thead>
     <tbody>
        @yield('export_body')
    </tbody>

    <tfoot>

        @yield('export_footer')

        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr style="background-color: #2c3e50; text-align: center; border: 1px solid #000">
            <td @yield('export_footer_brand_colspan')>
                <p style="color: #0fb0c0; font-size: 9; font-weight: bold;">Powered By Textify</p>
            </td>
        </tr>
    </tfoot>
</table>