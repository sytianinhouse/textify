<div class="modal" id="create-quick-campaign">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        {!! Form::label('campaign_name', 'Campaign Name: ', ['class' => 'col-form-label']) !!}
                        <div class="input-group input-group-prepend">
                        <span class="input-group-text br-0 border-right-0 rounded-left">
                            <i class="fa fa-bullhorn" aria-hidden="true"></i>
                        </span>
                            <input type="text" name="campaign_name" class="form-control" value="" >
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" id="{{ ( isset($vue) ) ? 'vue_save_campaign' : 'save_campaign' }}">Save Campaign</button>
            </div>
        </div>
    </div>
</div>
