<div class="cloader-wrapper">
    <div class="wrap">
        <div class="wap">
            <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-12 data_tables">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                    <thead>
                    <tr>
                        <th width="20%">Fullname</th>
                        <th width="20%">Mobile Number</th>
                        <th width="20%">Gender</th>
                        <th width="20%">Birthday</th>
                        <th width="20%">Email</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ( $customers as $args )
                        @php
                            $b_year = isset($args->b_year)? $args->b_year :NULL;
                            $b_month = isset($args->b_month)? $args->b_month :NULL;
                            $b_day = isset($args->b_day)? $args->b_day :NULL;

                            if ( empty($b_year) && empty($b_month) && empty($b_day) ){
                                $buildDate = NULL;
                            } else {
                                $buildDate = strftime('%F', strtotime($args->b_year.'-'.$args->b_month.'-'.$args->b_day));
                            }
                        @endphp
                        <tr>
                            <td>{{ $args->first_name }} {{ $args->last_name }}</td>
                            <td>{{ $args->mobile }}</td>
                            <td>{{ $args->gender }}</td>
                            <td>{{ $buildDate }}</td>
                            <td>{{ $args->email }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function() {
         $(".dataTables_paginate .pagination").addClass("float-right");
         if ( $('.table-data').length ) {
             $('.table-data').each(function() {
                 var aoColumns = [],
                     $this = $(this);

                 var $doIndex,
                     $doOrdering,
                     options = {
                         responsive: true,
                         paging : true,
                         filter : true,
                         "lengthMenu": [
                             [10, 15],
                             [10, 15] // change per page values here
                         ],
                         "pageLength": 12,
                         "bInfo": false,
						 "bLengthChange": false
                     };

                 if( $(this).data('default-order') ) {
                     $doIndex = i;
                     $doOrdering = $(this).attr('data-default-order');
                 } else {
                     $doIndex = 0;
                     $doOrdering = 'desc';
                 }

                 if( $(this).data('no-default-order') ) {
                     options.order = [];
                 } else {
                     options.order = [[$doIndex, $doOrdering]];
                 }

                 if ($this.find('thead > tr').length == 1) {
                     $this.find('thead th').each(function(i) {
                         var sType = $(this).data("type"),
                             oFalse = $(this).data("orderable");
                         var lengthMenu = [
                             [5, 15, 20, -1],
                             [5, 15, 20, "All"] // change per page values here
                         ];
                         if(sType != '' || sType != undefined){
                             aoColumns.push({ "type" : sType, "targets" : i, "lengthMenu": lengthMenu });
                         }
                         if (oFalse != undefined){
                             aoColumns.push({ "orderable" : oFalse, "targets" : i });
                         }
                         options.columnDefs = aoColumns;
                     });
                 }

                 $.fn.dataTable.moment( 'MMM D, YYYY' );
                 $this.DataTable(options);
             });

             $('.table-data').parent().parent().css('width', '100%');
         }
	});
</script>
