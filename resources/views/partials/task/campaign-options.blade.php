<div class="form-group row align-items-center new-generated-campaign">
    <div class="col position-relative">
        {!! Form::label('campaign', 'Campaign', ['class' => 'col-form-label']) !!}
        <a href="javascript:;" class="plus-icon btn btn-success" data-toggle="modal" data-target="#create-quick-campaign">
            <i class="fa fa-plus" aria-hidden="true"></i><span>Add New</span>
        </a>
        {!! Form::select('campaign', $campaign, $lastCampaign->id, [ 'placeholder' => '',
            'class' => 'custom-select form-control chzn-select customer-group decrease-size',
            'id' => 'campaign',
            'tabindex' => 2,
            'name'=>'campaign',
            'required'
        ]) !!}
    </div>
</div>
<script>
    $(document).ready(function() {
        $(".chzn-select").chosen({allow_single_deselect: true});
    });
</script>
