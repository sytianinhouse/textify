<div id="sub_account_options" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Sub Account</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <form id="sub-accoount-modal-form">
                        <label for="subAccounts" class="control-label">Choose a Sub Account</label>
                        {{--http://staging/atimar_v2/public/client/tasks/tbs/create/?sub_account_id=--}}
                        <select name="subaccount_id" id="subAccounts" data-url="{{ $url }}"  class="form-control sumo-select zensoft-select">
                            <option value="">Select Sub Account</option>
                            @if(isset($subAccounts) && $subAccounts->count() > 0)
                                @foreach($subAccounts as $key => $sub)
                                    <option value="{{ $key }}">{{ $sub }}</option>
                                @endforeach
                            @endif
                        </select>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button id="sub-account-modal-submit" type="button" class="btn btn-success">Select</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
