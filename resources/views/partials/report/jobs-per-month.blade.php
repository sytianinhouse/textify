<div class="card">
    <div class="card-body" id="sms_body">
        <div class="table-responsive">
            <table data-order='[[ 0, "asc" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                <thead>
                <tr role="row">
                    <th class="sorting wid-20" tabindex="0" rowspan="1" colspan="1">Month</th>
                    <th class="sorting wid-25 text-right" tabindex="0" rowspan="1" colspan="1">Recipients</th>
                    <th class="sorting wid-25 text-right" tabindex="0" rowspan="1" colspan="1">SMS Sent</th>
                    <th class="sorting wid-25 text-right" tabindex="0" rowspan="1" colspan="1">SMS Failed</th>
                    <th class="sorting wid-25 text-right" tabindex="0" rowspan="1" colspan="1">Credits Used</th>
                    <th class="sorting wid-25 text-right" tabindex="0" rowspan="1" colspan="1">Success Rate</th>
                </tr>
                </thead>
                <tbody>
                @if( $results )
                    @php $totalRecipient = 0; $totalSent = 0; $totalFailed = 0; $totalCredits = 0; @endphp
                    @forelse($results as $key => $result )
                        <tr>
                            <td>
                                @php
                                    $params = [
                                        'from_date' => \Carbon\Carbon::create($filters['year'], $key, 1)->startOfMonth()->toDateString(),
                                        'to_date' => \Carbon\Carbon::create($filters['year'], $key, 1)->endOfMonth()->toDateString(),
                                        'start_filter' => true
                                    ];

                                    $params = array_merge($filters, $params);
                                    unset($params['month']);
                                    unset($params['year']);
                                    $totalRecipient += $result['recipients']; $totalSent += $result['success_sms'];
                                    $totalFailed += $result['failed_sms']; $totalCredits += $result['credits_used'];
                                @endphp
                                <span class="hidden">{{ \Carbon\Carbon::create('2019', $key, 1)->format('m') }}</span>
                                <a href="{{ $router($perDayLink, $params) }}">{{ \Carbon\Carbon::create('2019', $key, 1)->format('F') }}</a>
                            </td>
                            <td class="text-right">@numberComma($result['recipients'])</td>
                            <td class="text-right">@numberComma($result['success_sms'])</td>
                            <td class="text-right">@numberComma($result['failed_sms'])</td>
                            <td class="text-right">@numberComma($result['credits_used'])</td>
                            <td class="text-right">{{ successPercentage($result['recipients'], $result['failed_sms']) }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6">No Records Found</td>
                        </tr>
                    @endforelse
                @endif
                </tbody>
                <tfoot>
                    <tr class="tfoot-reports">
                        <td colspan="1">Total:</td>
                        <td class="text-right">@numberComma($totalRecipient)</td>
                        <td class="text-right">@numberComma($totalSent)</td>
                        <td class="text-right">@numberComma($totalFailed)</td>
                        <td class="text-right">@numberComma($totalCredits)</td>
                        <td class="text-right">{{ successPercentage( $totalRecipient, $totalFailed ) }}</td>
                    </tr>
                </tfoot>
            </table>
            <div class="text-center">
                @if(isLengthAware($results))
                    {!! $results->appends($filters)->links() !!}
                @else
                    <div class="alert alert-dark" role="alert">
                        Showing all items
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
