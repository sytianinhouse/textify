<div class="card">
    <div class="card-body" id="sms_body">
        <div class="table-responsive">
            <table data-order='[[ 0, "DESC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                <thead>
                <tr role="row">
                    <th class="sorting wid-20" tabindex="0" rowspan="1" colspan="1">Date</th>
                    <th class="sorting wid-25 text-right" tabindex="0" rowspan="1" colspan="1">Recipients</th>
                    <th class="sorting wid-25 text-right" tabindex="0" rowspan="1" colspan="1">SMS Sent</th>
                    <th class="sorting wid-25 text-right" tabindex="0" rowspan="1" colspan="1">SMS Failed</th>
                    <th class="sorting wid-25 text-right" tabindex="0" rowspan="1" colspan="1">Credits Used</th>
                    <th class="sorting wid-25 text-right" tabindex="0" rowspan="1" colspan="1">Success Rate</th>
                </tr>
                </thead>
                 <tbody>
                    @php $totalRecipient = 0; $totalSent = 0; $totalFailed = 0; $totalCredits = 0; @endphp
                    @forelse($results as $key => $result )
                    @php
                        $totalRecipient += $result['recipients']; $totalSent += $result['success_sms'];
                        $totalFailed += $result['failed_sms']; $totalCredits += $result['credits_used'];
                        $perSmsFilter = $filters;
                        unset($perSmsFilter['from_date']);
                        unset($perSmsFilter['to_date']);

                        $dateString = \Carbon\Carbon::parse($key)->toDateString();
                        $perSmsFilter['from_date'] = $dateString;
                        $perSmsFilter['to_date'] = $dateString;
                    @endphp
                    <tr>
                        <td>
                            <span class="hidden">{{ $dateString }}</span>
                            <a href="{{ route('client.report.job.sms.sent', array_merge($perSmsFilter, [ 'per_page' => 50 ])) }}" target="_blank">
                            {{ \Carbon\Carbon::parse($key)->toFormattedDateString() }}
                            </a>
                        </td>
                        <td class="text-right">@numberComma($result['recipients'])</td>
                        <td class="text-right">@numberComma($result['success_sms'])</td>
                        <td class="text-right">@numberComma($result['failed_sms'])</td>
                        <td class="text-right">@numberComma($result['credits_used'])</td>
                        <td class="text-right">{{ successPercentage($result['recipients'], $result['failed_sms']) }}</td>
                     </tr>
                    @empty
                     <tr>
                        <td colspan="5">No Records Found</td>
                    </tr>
                    @endforelse

                </tbody>
                <tfoot>
                    <tr class="tfoot-reports">
                        <td colspan="1">Total:</td>
                        <td class="text-right">@numberComma($totalRecipient)</td>
                        <td class="text-right">@numberComma($totalSent)</td>
                        <td class="text-right">@numberComma($totalFailed)</td>
                        <td class="text-right">@numberComma($totalCredits)</td>
                        <td class="text-right">{{ successPercentage( $totalRecipient, $totalFailed ) }}</td>
                    </tr>
                </tfoot>
            </table>
            <div class="text-center">

                @if(isLengthAware($results))
                    {!! $results->appends($filters)->links() !!}
                @else
                    <div class="alert alert-dark" role="alert">
                        Showing all items
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>
