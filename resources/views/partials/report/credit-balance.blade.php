<credit-balance
    :sub-account="{{ json_encode($subAccount) }}"
    :sub-accounts="{{ json_encode($subaccounts) }}"
    :credits="{{ json_encode($credits->first()) }}"
    :first-load="{{ json_encode($loads->first()) }}"
    :loads="{{ json_encode($loads->sortBy('date')->values()) }}"
    :usage="{{ json_encode($usage) }}"
></credit-balance>

@section('before_global_scripts')
    @parent
    <script type="text/javascript" src="{{asset('js/vue.js')}}"></script>
@stop