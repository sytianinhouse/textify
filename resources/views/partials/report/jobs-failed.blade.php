<div class="card">
    <div class="card-body" id="sms_body">
        <div class="table-responsive">
            <table data-order='[[ 0, "asc" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                <thead>
                <tr role="row">
                    <th class="sorting wid-20" tabindex="0" rowspan="1" colspan="1">Date & Time Sent</th>
                    <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Recipient Details</th>
                    <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Credit Cost</th>
                    <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">SMS Type</th>
                    <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Campaign</th>
                    @if(count($subaccounts) > 1)
                        <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Sub-Account</th>
                    @endif
                    @if (request()->has('admin'))
                    <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Actions</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @php $totalRecipient = 0; $totalCredits = 0; @endphp
                @foreach($results as $item)
                    @php $totalRecipient += 1; $totalCredits += $item->credits_consumed; $date = makeCarbon($item->time_attempt_send); @endphp
                    <tr>
                        <td><a href="{{ $router($jobLink, [$item->id, $item->instance]) }}">{{ $date->toDayDateTimeString() }}</a></td>
                        <td>
                            ({!! formattedNumber($item->customer_number) !!})
                            <br>
                            <em class="small">@if($item->customer_id)
                                    <a class="anc-neutral" href="{{ $router($customerDetailsLink, $item->customer_id) }}">
                                        {{ $item->customer_name }}
                                    </a>
                                @else
                                    {{ $item->customer_name }}
                                @endif</em>
                        </td>
                        <td class="text-right">
                            <i class="fa fa-exclamation-circle font-red" data-toggle="tooltip" data-html="true" title="{!! tooltipMessage(($item->error_log ?: $item->sms_response), 'Errors') !!}"></i>&nbsp;|&nbsp;
                            @if (request()->has('admin'))
                            {{ ($item->error_log ?: $item->sms_response) }}
                            @endif
                            <i class="fa fa-envelope" data-toggle="tooltip" data-html="true" title="{!! tooltipMessage($item->message) !!}"></i>&nbsp;|&nbsp;
                            @numberComma($item->credits_consumed)
                        </td>
                        <td>{{ getTaskType($item->task_type_id) }}</td>
                        <td><a class="anc-underlined" href="{{ $router($campaignLink, $item->campaign_id) }}">{{ $item->campaign_name }}</a></td>
                        @if(count($subaccounts) > 1)
                            <td>{{ $item->subaccount_name }}</td>
                        @endif
                        @if (request()->has('admin'))
                        <td>
                                @if( $authUser->isOwner() )
                                <a href="{{ route('client.jobs.jf.resend', $item->id) }}?admin=1" class="d-inline badge-sm badge badge-success p-2">Resend</a>
                                @elseif(  $authUser->isAccountUser() )
                                <a href="{{ sroute('sub.jobs.jf.resend', $item->id) }}" class="d-inline badge-sm badge badge-success p-2">Resend</a>
                                @endif
                        </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr class="tfoot-reports">
                    <td>Total</td>
                    <td class="text-right">@numberComma($totalRecipient)</td>
                    <td class="text-right">@numberComma($totalCredits)</td>
                    <td></td>
                    <td></td>
                    @if(count($subaccounts) > 1)
                    <td></td>
                    @endif
                    @if (request()->has('admin'))
                    <td></td>
                    @endif
                </tr>
                </tfoot>
            </table>
            <div class="text-center">
                @if(isLengthAware($results))
                    {!! $results->appends($filters)->links() !!}
                @else
                    <div class="alert alert-dark" role="alert">
                        Showing all items
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>