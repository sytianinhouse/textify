<div class="card">
    <div class="card-body" id="project_body">
        <div class="table-responsive">
            <table data-order='[[ 1, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                <thead>
                <tr role="row">
                    <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Campaign Name</th>
                    <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Date Created</th>
                    <th class="sorting wid-25 text-right" tabindex="0" rowspan="1" colspan="1">Recipients</th>
                    <th class="sorting wid-25 text-right" tabindex="0" rowspan="1" colspan="1">SMS Sent</th>
                    <th class="sorting wid-25 text-right" tabindex="0" rowspan="1" colspan="1">SMS Failed</th>
                    <th class="sorting wid-25 text-right" tabindex="0" rowspan="1" colspan="1">Credits Used</th>
                    <th class="sorting wid-25 text-right" tabindex="0" rowspan="1" colspan="1">Success Rate</th>
                </tr>
                </thead>
                @php $totalRecipient = 0; $totalSent = 0; $totalFailed = 0; $totalCredits = 0; @endphp
                <tbody>
                @if( $tasksPerCampaign )
                    @foreach($tasksPerCampaign as $camp )
                        @php
                            $camp = (object) $camp; $date = new Carbon\Carbon($camp->camp_date);
                            $totalRecipient += $camp->total_count; $totalSent += $camp->completed_count; $totalFailed += $camp->failed_count; $totalCredits += $camp->total_credits;
                        @endphp
                        <tr>
                            <td>{{ $camp->name }}</td>
                            <td data-order="{{ $date->toFormattedDateString() }}">{{ $date->toFormattedDateString() }}</td>
                            <td class="text-right">@numberComma($camp->total_count)</td>
                            <td class="text-right">@numberComma($camp->completed_count)</td>
                            <td class="text-right">@numberComma($camp->failed_count)</td>
                            <td class="text-right">@numberComma($camp->total_credits)</td>
                            <td class="text-right">{{ successPercentage( $camp->total_count, $camp->failed_count ) }}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
                <tfoot class="tfoot-reports">
                    <tr>
                        <td colspan="2">Total:</td>
                        <td class="text-right">@numberComma($totalRecipient)</td>
                        <td class="text-right">@numberComma($totalSent)</td>
                        <td class="text-right">@numberComma($totalFailed)</td>
                        <td class="text-right">@numberComma($totalCredits)</td>
                        <td class="text-right">{{ successPercentage( $totalRecipient, $totalFailed ) }}</td>
                    </tr>
                </tfoot>
            </table>
            <div class="text-center">
                @if(isLengthAware($tasksPerCampaign))
                    {!! $tasksPerCampaign->appends($filters)->links() !!}
                @else
                    <div class="alert alert-dark" role="alert">
                        Showing all items
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
