@if (isset($ownLayout))
    @if(isset($buttonType))
        <input name="input_submit_task" class="btn btn-info form-control" type="submit" value="<?php echo isset($textUpdate) ? $textUpdate : 'Save Changes' ?>"
                {{ isset($disabledButton) && $disabledButton ? 'disabled' : '' }}>
        <input name="input_send_task" class="btn btn-success form-control m-b-10" type="submit" value="<?php echo isset($textSend) ? $textSend : 'Save Changes' ?>"
                {{ isset($disabledButton) && $disabledButton ? 'disabled' : '' }}>
    @else
        <button class="btn btn-success form-control" type="submit">
            <i class="fa fa-save"></i> &nbsp;
            {{ isset($text) ? $text : 'Save Changes' }}
        </button>
    @endif
@else
    <div class="col-12 col-lg-3 p-l-0">
        <div class="card" id="sticky" style="margin-bottom:10px;">
            <div class="card-header">
                Actions
            </div>
            <div class="card-body">
                @if(isset($withCreationDetails) && isset($entity) && $entity->exists)
                    <ul class="list-unstyled text-right">
                        <li>
                            <h5>{{ $entity->created_at->toDayDateTimeString() }}
                            </h5>
                            <em class="font-weight-light">Date Created</em>
                        </li>
                        <li class="m-t-15">
                            <h5>{{ $entity->created_at->toDayDateTimeString() }}</h5>
                            <em class="font-weight-light">Date Last Updated</em>
                        </li>
                    </ul>
                    <hr>
                @endif
                @if(isset($activation) && $activation)
                        <div class="checkbox">
                            <label>
                                {{--&& !$entity->exists--}}
                                {!! Form::checkbox('active', 1, (isset($entity)) ? (isset($activeState) ? $activeState : true) : null, array('class' => 'custom-control-input')) !!}
                                <span class="cr custom-control-indicator custom_checkbox_default"><i class="cr-icon fa fa-check text-success"></i></span>
                                Activate
                            </label>
                        </div>
                @endif
            </div>
            <div class="card-footer" @if(!isset($activation)) style="border-radius: 3px;border-top: none" @endif>
                @if(isset($hasRemove) && $hasRemove)
                    <button class="btn btn-danger btn-block mrg10B" type="Remove Announcement" name="remove">
                        <span class="fa fa-close"></span> &nbsp;
                        Remove Announcement
                    </button>
                @endif
                @if( isset($buttonType) && !isset($buttonTypeSendOnly) )
                    <input name="input_submit_task" class="btn btn-info form-control" type="submit" value="<?php echo isset($textUpdate) ? $textUpdate : 'Save Changes' ?>"
                        {{ isset($disabledButton) && $disabledButton ? 'disabled' : '' }}>
                    <input data-confirm="" data-confirm-text="Kindly check if all the details are correct." name="input_send_task" class="confirmSubmit btn btn-success form-control m-b-10" type="submit" value="<?php echo isset($textSend) ? $textSend : 'Save Changes' ?>"
                        {{ isset($disabledButton) && $disabledButton ? 'disabled' : '' }}>

                    <input name="input_send_task" class="hidden sendTaskBtn btn btn-success form-control m-b-10" type="submit" value="<?php echo isset($textSend) ? $textSend : 'Save Changes' ?>" {{ isset($disabledButton) && $disabledButton ? 'disabled' : '' }}>
                @elseif( isset($buttonTypeSendOnly) )
                    <input data-confirm="" data-confirm-text="Kindly check if all the details are correct." name="input_send_task" class="confirmSubmit btn btn-success form-control m-b-10" type="submit" value="<?php echo isset($textSend) ? $textSend : 'Save Changes' ?>"
                    {{ isset($disabledButton) && $disabledButton ? 'disabled' : '' }}>
                    <img src="{{ asset('images/spinne11r.gif') }}" alt="" class="hidden" id="save_task_loader">
                    <input name="input_send_task" class="hidden sendTaskBtn btn btn-success form-control m-b-10" type="submit" value="<?php echo isset($textSend) ? $textSend : 'Save Changes' ?>" {{ isset($disabledButton) && $disabledButton ? 'disabled' : '' }}>
                @else
                    <button class="btn btn-success form-control" type="submit">
                        <i class="fa fa-save"></i> &nbsp;
                        {{ isset($text) ? $text : 'Save Changes' }}
                    </button>
                @endif
            </div>
        </div>
        @if(isset($withImage) && $withImage)
            <div class="card">
                <div class="card-header">
                    {{ $imageName }}
                </div>
                <div class="card-body">
                    <div class="col text-center text-lg-left">
                        <div class="alert alert-warning alert-dismissable">
                            <strong>Info!</strong>
                            Recommended resolution: 320 x 200
                        </div>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new img-thumbnail text-center">
                                <img src="{{ $imgPath }}"  id="myImage"  alt="not found" style="width: 100%;">
                            </div>
                            <div class="fileinput-preview fileinput-exists img-thumbnail"></div>
                            <div class="m-t-10 text-center">
                                <span class="btn btn-primary btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="thumb">
                                </span>
                                <a href="#" class="btn btn-warning fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(isset($logs) && $logs)
            <div class="card">
                <div class="card-header">
                    Audit Logs
                </div>
                <div class="card-body">
                    {{-- <div class="col text-center text-lg-left">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new img-thumbnail text-center">
                                <img src="{{ $imgPath }}"  id="myImage"  alt="not found" style="width: 100%;">
                            </div>
                            <div class="fileinput-preview fileinput-exists img-thumbnail"></div>
                            <div class="m-t-10 text-center">
                                <span class="btn btn-primary btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="thumb">
                                </span>
                                <a href="#" class="btn btn-warning fileinput-exists"
                                   data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </div> --}}
                    <div {{-- class="list-group" --}}>
                        <ul class="p-l-15" {{-- class="list-group lists_margin_bottom" style="padding: 0.5rem 0.5rem;" --}}>
                            @forelse($logs as $log)
                                <li class="list-group-item"><p>{{ $log->notes }}</p></li>
                            @empty
                                <li {{-- class="list-group-item" --}}><p>No Logs Recorded.</p></li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endif
