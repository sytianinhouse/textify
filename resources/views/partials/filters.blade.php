{{-- @push('stack-css')
<style>
#filter_service_category_chosen, #ex_payment_type_id_chosen {
    width: auto!important;
}
</style>
@endpush --}}

<div class="card m-b-10">
    <div class="card-body filter-form">
        <div class="row">
            <div class="col">
                {!! Form::open(['method' => 'GET', 'id' => 'form-filter-list', 'autocomplete' => 'off']) !!}
                    <div class="form-group row form_inline_inputs_bot filter-row align-items-end">
                        @if(isset($filterProject))
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="projectSelect" class="control-label">Project</label>
                                    <select name="project_id" id="projectSelect" class="form-control sumo-select">
                                        <option value="">Show All</option>
                                        @foreach($projects as $key => $project)
                                            <option value="{{ $key }}" {{ isset($filters['project_id']) && $filters['project_id'] == $key ? 'selected' : '' }}>{{ $project }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col {{ isset($clients) && $clients->count() > 0 ? '' : 'hidden' }} " id="clientSelectContainer">
                                <div class="filter-item form-group">
                                    <label for="ajaxClientSelect" class="control-label">Client</label>
                                    <select name="client_id" id="ajaxClientSelect" class="form-control sumo-select">
                                        <option value="">Show All</option>
                                        @if(isset($clients) && $clients->count() > 0)
                                            @foreach($clients as $key => $client)
                                                <option value="{{ $key }}" {{ isset($filters['client_id']) && $filters['client_id'] == $key ? 'selected' : '' }}>{{ $client }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="col {{ isset($subAccounts) && $subAccounts->count() > 0 ? '' : 'hidden' }}"  id="subSelectContainer">
                                <div class="filter-item form-group">
                                    <label for="ajaxSubSelect" class="control-label">Sub Account</label>
                                    <select name="sub_account_id" id="ajaxSubSelect" class="form-control sumo-select">
                                        <option value="">Show All</option>
                                        @if(isset($subAccounts) && $subAccounts->count() > 0)
                                            @foreach($subAccounts as $key => $sub)
                                                <option value="{{ $key }}" {{ isset($filters['sub_account_id']) && $filters['sub_account_id'] == $key ? 'selected' : '' }}>{{ $sub }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if(isset($logTypes))
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="projectSelect" class="control-label">Log Type</label>
                                    <select name="type" id="type" class="form-control sumo-select">
                                        <option value="">Show All</option>
                                        @foreach($logTypes as $type)
                                            <option value="{{ $type }}" {{ isset($filters['type']) && $filters['type'] == $type ? 'selected' : '' }}>{{ ucfirst($type) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if( isset($filterByDateToSendCreatedAt) && $filterByDateToSendCreatedAt != false )
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="date-range" class="control-label ">Filter Date By:</label>
                                    <select name="filter_date_by" id="month" class="form-control sumo-select">
                                        <option value="">Select Date By</option>
                                        <option value="date_to_send" {{ ($filters['filter_date_by'] == 'date_to_send' ) ? 'selected' : '' }}>Date to Send</option>
                                        <option value="created_at" {{ ($filters['filter_date_by'] == 'created_at' ) ? 'selected' : '' }}>Created At</option>
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if(!isset($noDaterange))
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="date-range" class="control-label ">Filter by Date</label>
                                    <div class="input-group input-daterange">
                                        <input type="text" name="from_date" id="from_date" class="form-control" value="{{ ($filters) ? $filters['from_date'] : request('from_date') }}" placeholder="Start date" autocomplete="off">
                                        <input type="text" name="to_date" id="to_date" class="form-control" value="{{ ($filters) ? $filters['to_date'] : request('to_date') }}" placeholder="End date" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(isset($singleDate))
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="date" class="control-label">Filter by Date</label>
                                    <input type="text" name="date" class="form-control datepicker" value="{{ request('date') }}" placeholder="Date" autocomplete="new-date">
                                </div>
                            </div>
                        @endif
                        @if( isset($filterSubAccount) && $filterSubAccount != false && isset($subAccounts) && $subAccounts->count() > 1 )
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="date-range" class="control-label ">Sub Account</label>
                                    <select name="subaccount_filter" id="month" class="form-control sumo-select">
                                        <option value="">Show All</option>
                                        @foreach($subAccounts as $key => $item)
                                            <option value="{{ $key }}" {{ ($filters['subaccount_filter'] == $key) ? 'selected' : '' }}>{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if(isset($subaccounts) && count($subaccounts) > 1)
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="selectSubAccount" class="control-label">Sub Account</label>
                                    <select name="sub_account_id" id="selectSubAccount" class="form-control sumo-select">
                                        @if(!isset($noSubAccountAll))
                                            <option value="">Show All</option>
                                        @endif
                                        @foreach($subaccounts as $key => $subaccount)
                                            <option value="{{ $key }}" {{ $filters['sub_account_id'] == $key ? 'selected' : '' }}>{{ $subaccount }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if(isset($filterSearchAnything))
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="filter_search" class="control-label">Search</label>
                                    <input type="text" class="form-control" id="filter_search" name="filter_search" value="{{ request('filter_search') }}" placeholder="Search..." autocomplete="new-search">
                                </div>
                            </div>
                        @endif
                        @if(isset($filterMonths))
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="month" class="control-label">Month</label>
                                    <select name="month" id="month" class="form-control sumo-select">
                                        @foreach($filterMonths as $key => $item)
                                            <option value="{{ $key }}" {{ $filters['month'] == $key ? 'selected' : ($filters['month'] == '' && (new Carbon\Carbon())->format('m') == $key ? 'selected' : '') }}>{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if(isset($filterYears))
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="year" class="control-label">Year</label>
                                    <select name="year" id="year" class="form-control sumo-select">
                                        @for($x = ($filterYears - 5); $x <= ($filterYears + 5); $x++)
                                            <option value="{{ $x }}" {{ $filters['year'] == $x ? 'selected' : ($filters['year'] == '' && (new Carbon\Carbon())->format('Y') == $x ? 'selected' : '') }}>{{ $x }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if(isset($campaignFilter))
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="date-range" class="control-label ">Campaign</label>
                                    <select name="campaign_filter" id="month" class="form-control sumo-select">
                                        <option value="">Show All</option>
                                        @foreach($campaigns as $key => $item)
                                            <option value="{{ $key }}" {{ ($filters['campaign_filter'] == $key) ? 'selected' : '' }}>{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if(isset($filterSearchAnyKey))
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="branchId">Search</label>
                                    {!! Form::text('filter_search', ( $filters ) ? $filters['filter_search'] : '', ['class' => 'form-control', 'placeholder' => 'Search here...', 'autocomplete' => 'new-filter-search']) !!}
                                </div>
                            </div>
                        @endif
                        @if(isset($filterTaskTypes))
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="date-task_type_id" class="control-label">Task Types</label>
                                    <select name="task_type_id" id="task_type_id" class="form-control sumo-select">
                                        <option value="">Show All</option>
                                        @foreach($filterTaskTypes as $key => $item)
                                            <option value="{{ $key }}" {{ $filters['task_type_id'] == $key ? 'selected' : '' }}>{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif

                        @if(isset($clientWithSub) && $clientWithSub)
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="selectSubAccount" class="control-label">Sub Account</label>
                                    <select name="sub_account_id" id="selectSubAccount" class="form-control sumo-select">
                                        <option value="">Show All</option>
                                        @foreach($clients as $client)
                                            <optgroup>{{ $client->company_name }}
                                                @foreach($client->subAccounts as $sub)
                                                    <option value="{{ $sub->id }}" {{ isset($filters['sub_account_id']) && $filters['sub_account_id'] == $sub->id ? 'selected' : '' }}>{{ $sub->account_name }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if(isset($filterCustomerDropdown))
                            <div class="col">
                                <div class="filter-item form-grou">
                                    <label for="customer-id" class="control-label">CUSTOMER NAME</label>
                                    {!! Form::select('customer_id', $customersDrop, $filters['customer_id'], ['class' => 'form-control sumo-select',
                                        'id' => 'customer-id', 'placeholder' => 'Show All']) !!}
                                </div>
                            </div>
                        @endif
                        @if(isset($filterCustomer))
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="customer-name" class="control-label">CUSTOMER NAME</label>
                                    <input type="text" class="form-control" id="customer-name" name="customer_name" value="{{ request('customer_name') }}" placeholder="Customer Name">
                                </div>
                            </div>
                        @endif
                        @if(isset($mothlyDropdown))
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="month" class="control-label">MONTH</label>
                                    <select name="month" id="month" class="form-control sumo-select">
                                        @foreach($monthsList as $key => $month)
                                            @php 
                                                $hasMonth = Request::has('month');
                                            @endphp
                                            <option value="{{ $key }}" {{ 
                                                $hasMonth && Request::input('month') == $key ? 'selected' : ((!$hasMonth && Carbon\Carbon::now()->format('M') == $month) ? 'selected' : '')
                                            }}>{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if(isset($yearlyDrop))
                            <div class="col">
                                <div class="filter-item form-group">
                                    <label for="year" class="control-label">YEAR</label>
                                    <select name="year" id="year" class="form-control sumo-select">
                                        @foreach($yearsList as $list)
                                            @php
                                                $hasYear = Request::has('year');
                                                $hasMonth = Request::has('month');
                                            @endphp
                                            <option value="{{ $list }}" {{ 
                                                ($hasYear && Request::input('year') == $list) ? 'selected' : ((!$hasMonth && Carbon\Carbon::now()->format('Y') == $list) ? 'selected' : '') }}>{{ $list }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if(!isset($noPerPage))
                            <div class="col">
                                <div class="form-group filter-item">
                                    <label for="category" class="control-label">Per Page</label>
                                    {!! Form::select('per_page', perPageDropdown(4, 200), isset($filters['per_page']) ? $filters['per_page'] : '', ['class' => 'form-control sumo-select', 'id' => 'perPage']) !!}
                                    {{--, 'placeholder' => 'Display All'--}}
                                </div>
                            </div>
                        @endif
                        @if(isset($activeFilter))
                        <div class="col">
                            <div class="form-group filter-item form-filter">
                                <div class="checkbox m-b-10">
                                    <label>
                                        {!! Form::checkbox('active', 1, isset($filters['active']) && $filters['active'] == true ? true : false, array('class' => 'custom-control-input')) !!}
                                        <span class="cr custom-control-indicator custom_checkbox_default"><i class="cr-icon fa fa-check text-success"></i></span>
                                        Active <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Will this task accepts API calls."></i>
                                    </label>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="col">
                            <div class="form-group filter-item form-filter-submit">
                                <input type="hidden" name="start_filter" value="1">
                                <label class="control-label">&nbsp;</label>
                                <button class="btn btn-info btn-filter-go" type="submit"><i class="fa fa-filter mrg5R"></i>&nbsp;Filter</button>
                                @isset($moreFilters)
                                &nbsp;&nbsp;
                                <a href="javascript:;" class="btn btn-info" data-toggle="modal" data-target="#modal-morefilters">&nbsp;More Filters</a>
                                @endisset
                            </div>
                        </div>
                    </div>
                    @isset($moreFilters)
                    <div id="modal-morefilters" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">More Filters</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    @if(isset($moreFilterGroups))
                                        <div class="filter-item form-group">
                                            <label for="filter-group" class="control-label">Groups</label>
                                            <select name="filter_group" id="filter-group" class="form-control sumo-select">
                                                <option value="">Show All</option>
                                                @foreach($moreFilterGroups as $key => $item)
                                                    <option value="{{ $key }}" {{ $filters['filter_group'] == $key ? 'selected' : '' }}>{{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                    @isset($moreFGender)
                                        <div class="filter-item form-group">
                                            <label for="filter_gender" class="control-label">Gender</label>
                                            <select name="filter_gender" id="filter_gender" class="form-control sumo-select">
                                                <option value="">Show All</option>
                                                <option value="male" {{ isset($filters['filter_gender']) && $filters['filter_gender'] == 'male' ? 'selected' : '' }}>Male</option>
                                                <option value="female" {{ isset($filters['filter_gender']) && $filters['filter_gender'] == 'female' ? 'selected' : '' }}>Female</option>
                                            </select>
                                        </div>
                                    @endif
                                    @isset($moreFilterCities)
                                        <div class="filter-item form-group">
                                            <label for="filter_city" class="control-label">City</label>
                                            <select name="filter_city" id="filter_city" class="form-control sumo-select">
                                                <option value="">Show All</option>
                                                @foreach($moreFilterCities as $key => $city)
                                                    <option value="{{ $key }}" {{ $filters['filter_city'] == $key ? 'selected' : '' }}>{{ $city }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-warning btn-clear-forms">Clear Selection</button>
                                    <button type="submit" class="btn btn-success">Filter</button>
                                    <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endisset
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('footer_scripts')
    {{-- <script type="text/javascript" src="{{asset('assets/vendors/moment/js/moment.min.js')}}"></script> --}}
    {{-- <script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js"></script> --}}
    <script type="text/javascript">
        $(document).ready(function(){

        	$('body').on('click', '.btn-clear-forms', function () {
                let $this = $(this),
                    form = $this.closest('form');

                form.find('input').not('[name=start_filter]').val('');
				form.find('select').not('[name=per_page]').val('');
			});

            $('#projectSelect').on('change', function() {

                var project_id = $('#projectSelect').val(),
                    data = {
                        project_id : project_id,
                    };

                $.ajax({
                    url: '{{ route('admin.get.clients') }}',
                    type: 'GET',
                    data: data,
                    success: function($result) {

                        $('#ajaxClientSelect').empty();
                        $('#subSelectContainer').hide();

                        var html = '<option value="">Show All</option>';
                        $.each($result, function(ind, val) {
                            html += '<option value="' + ind + '">' + val + '</option>';
                        });

                        $('#ajaxClientSelect').append(html);
                        $('#clientSelectContainer').show();

                    },
                });

            });

            $('#ajaxClientSelect').on('change', function() {

                var client_id = $('#ajaxClientSelect').val(),
                    data = {
                        client_id : client_id,
                    };

                if (client_id == '') {
                    $('#subSelectContainer').hide();
                }

                $.ajax({
                    url: '{{ route('admin.get.subs') }}',
                    type: 'GET',
                    data: data,
                    success: function($result) {

                        $('#ajaxSubSelect').empty();

                        var html = '<option value="">Show All</option>';
                        $.each($result, function(ind, val) {
                            html += '<option value="' + ind + '">' + val + '</option>';
                        });

                        $('#ajaxSubSelect').append(html);
                        $('#subSelectContainer').show();

                    },
                });

            });

        });
    </script>
@stop
