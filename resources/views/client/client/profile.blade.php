@extends('layouts.fixed_header_client')

{{-- Page title --}}
@section('title')
    {{ $title ?: 'Title goes here' }}
    @parent
@stop

@section('crumbs')
    <li class="active breadcrumb-item">Client Profile</li>
@stop


{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                {{-- <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.user.create') }}" class=" btn btn-success m-r-5">
                            Add Entity  <i class="fa fa-plus"></i>
                        </a>
                        <a href="{{ route('admin.user.create') }}" class=" btn btn-primary">
                            Print  <i class="fa fa-print"></i>
                        </a>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    
    @include('templates.clients.form', [
        'client' => $client,
        'method' => 'put',
    ])

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pluginjs/jasny-bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/holderjs/js/holder.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <!-- end of plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/validation.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/sweetalert/js/sweetalert2.min.js')}}"></script>
    <!-- end of page level js -->
@stop
