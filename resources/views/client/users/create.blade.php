@extends('layouts.fixed_header_client')

{{-- Page title --}}
@section('title')
    {{ $title ?: 'Title goes here' }}
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('client.user.index') }}">Users</a>
    </li>
    <li class="active breadcrumb-item">Add User</li>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                {{-- <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.user.create') }}" class=" btn btn-success m-r-5">
                            Add Entity  <i class="fa fa-plus"></i>
                        </a>
                        <a href="{{ route('admin.user.create') }}" class=" btn btn-primary">
                            Print  <i class="fa fa-print"></i>
                        </a>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>

    @include('templates.users.form', [
        'user' => new \app\User,
        'method' => 'post',
        'defaultAccountType' => false
    ])

@stop