@extends('layouts.fixed_header_client')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="active breadcrumb-item">Campaigns</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row no-gutters">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('client.camp.create') }}" data-subaccount-url="{{ route('client.sub.index') }}" data-validate-subaccounts="{{ $subAccounts->count() }}" data-confirm-text="You don't have any sub Accounts yet, would you like to create one?" class="btn btn-success m-r-5 campaignCreationCheckSubaccounts">
                            Add Campaign <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.filters',[
        'filterSubAccount' => true,
        'noDaterange' => false,
        'filterSearchAnyKey' => true,
        'noPerPage' => true,
    ])

    <div class="card">
        <div class="card-body" id="user_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer this-table" id="datatable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Campaign Name</th>
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Date Created</th>
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Sub Account</th>
                            {{-- <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Description</th>
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Created At</th> --}}
                            <th class="sorting wid-25" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse( $allCampaigns as $theCampaign )
                            <tr>
                                <td>
                                    <a href="{{ route('client.camp.edit', $theCampaign) }}" title="Edit">
                                            {{ $theCampaign->name }}
                                    </a>
                                </td>
                                <td>
                                    {{ $theCampaign->created_at->toDayDateTimeString() }}
                                </td>
                                <td>
                                    <ul class="m-b-0">
                                    @foreach ( $theCampaign->subAccounts->sortBy('id') as $key => $sub )
                                        <li>{!! $sub->account_name.'<br>' !!}</li>
                                    @endforeach
                                    </ul>
                                </td>
                                {{-- <td>
                                    {{ str_limit( $theCampaign->description, 100, "...." ) }}
                                </td>
                                <td>
                                    {{ $theCampaign->created_at->toDateTimeString() }}
                                </td> --}}
                                <td>
                                    <a href="{{ route('client.camp.edit', $theCampaign) }}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{ route('client.camp.delete', $theCampaign) }}" title="Delete" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    <a href="{{ route('client.camp.sentSMS', $theCampaign) }}" title="View Sent SMS" class="btn btn-sm btn-success"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">No records found!</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                {{-- <div class="text-center">
                    @if(isLengthAware($allCampaigns))
                        {!! $allCampaigns->appends($filters)->links() !!}
                    @else
                        <div class="alert alert-dark" role="alert">
                            Showing all items
                        </div>
                    @endif
                </div> --}}
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- /.inner -->
    <!-- /.outer -->
@stop
