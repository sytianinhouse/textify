@extends('layouts.fixed_header_client')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    @if(isset($breadCrumbsUrl))
    <li class="breadcrumb-item">
        <a href="{{ $breadCrumbsUrl }}">{{ $breadCrumbsLabel }}</a>
    </li>
    @endif
    <li class="breadcrumb-item">
    @if($task->isImmediateTask())
        <a href="{{ route('client.task.it.edit', $task) }}">Edit - Task</a>
    @elseif($task->isOneTimeSendTask())
        <a href="{{ route('client.task.ots.edit', $task) }}">Edit - Task</a>
    @elseif($task->isTextBlastTask())
        <a href="{{ route('client.task.tbs.edit', $task) }}">Edit - Task</a>
    @elseif($task->isBirthdayTask())
        <a href="{{ route('client.task.birthday-task.create') }}">Edit - Task</a>
    @endif
    </li>
    <li class="active breadcrumb-item">{{ $title }}</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row no-gutters">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
            </div>
        </div>
    </div>
    @include('templates.jobs.view', [
        'job' => $job,
        'method' => 'POST',
        'router' => 'route',
        'deleteRoute' => 'client.jobs.delete',
    ])
@stop
