@extends('layouts.fixed_header_client')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="breadcrumb-item"><a href="{{ route('client.api.index') }}">Api Keys</a></li>
    <li class="active breadcrumb-item">Add Api Key</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row no-gutters">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                {{-- <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('admin.user.create') }}" class=" btn btn-success m-r-5">
                            Add Entity  <i class="fa fa-plus"></i>
                        </a>
                        <a href="{{ route('admin.user.create') }}" class=" btn btn-primary">
                            Print  <i class="fa fa-print"></i>
                        </a>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    @include('templates.apikey.form', [
        'api_key' => new \ATM\Repositories\ApiKey\ApiKey,
        'method' => 'post',
    ])
@stop
