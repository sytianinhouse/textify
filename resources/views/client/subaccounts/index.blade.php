@extends('layouts.fixed_header_client')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop
@section('crumbs')
    <li class="active breadcrumb-item">Sub Accounts</li>
@stop
{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row no-gutters">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                {{-- <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('client.sub.create') }}" class=" btn btn-success m-r-5">
                            Add Sub Account  <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    {{-- @include('partials.form-success') --}}
    @include('partials.filters',[
        'noDaterange' => true,
        'noPerPage' => true,
        'filterSearchAnyKey' => true
    ])

    <div class="card">
        <div class="card-body" id="user_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer this-table" id="datatable" role="grid">
                    <thead>
                        <tr role="row">
                            {{-- <th class="sorting_asc wid-5" tabindex="0" rowspan="1" colspan="1">#</th> --}}
                            {{-- <th class="sorting_asc wid-10" tabindex="0" rowspan="1" colspan="1">Client ID</th> --}}
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Account Name</th>
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Phone</th>
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Mobile Phone</th>
                            <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">Status</th>
                            <th class="sorting wid-10" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $subaccountslists as $theSubAccount )
                            <tr>
                                {{-- <td>{{ $loop->iteration }}</td> --}}
                                {{-- <td>{{ $theSubAccount->client_id == null ? 'Not Set' : $theSubAccount->client_id }}</td> --}}
                                <td>
                                    <a href="{{ route('client.sub.edit', [$theSubAccount]) }}" title="Edit">
                                        {{ $theSubAccount->account_name }}</td>
                                    </a>
                                <td>{{ $theSubAccount->telephone }}</td>
                                <td>{{ $theSubAccount->mobile_phone }}</td>
                                <td>{!! $theSubAccount->active == 1 ? "<div class='active-display'>active</div>" : "<div class='inactive-display'>inactive</div>" !!}</td>
                                <td>
                                    <a href="{{ route('client.sub.edit', [$theSubAccount]) }}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{ route('client.sub.settings', [$theSubAccount]) }}" title="Edit" class="btn btn-sm btn-warning"><i class="fa fa-cog" aria-hidden="true"></i></a>
                                    {{-- <a href="{{ route('client.sub.delete', [$theSubAccount]) }}" title="Delete" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a> --}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- <div class="text-center">
                    @if(isLengthAware($subaccountslists))
                        {!! $subaccountslists->appends($filters)->links() !!}
                    @else
                        <div class="alert alert-dark" role="alert">
                            Showing all items
                        </div>
                    @endif
                </div> --}}
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- /.inner -->
    <!-- /.outer -->
@stop
