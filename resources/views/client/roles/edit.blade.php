@extends('layouts.fixed_header_client')
{{-- Page title --}}
@section('title')
    {{ isset($title) ? $title : 'Title goes here' }}
    @parent
@stop

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('client.role.index') }}">Roles</a>
    </li>
    <li class="active breadcrumb-item">Add Role</li>
@stop
{{-- Page content --}}
@section('content')
    
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
            </div>
        </div>
    </div>
    @include('templates.roles.form', [
        'role' => $role,
        'method' => 'put',
    ])
@stop
