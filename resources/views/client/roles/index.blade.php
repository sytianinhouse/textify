@extends('layouts.fixed_header_client')
{{-- Page title --}}
@section('title')
    Roles
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <!--Plugin styles-->
    {{-- <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/select2/css/select2.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datepicker/css/bootstrap-datepicker.min.css')}}" /> --}}
    <!--End of plugin styles-->
    <!--Page level styles-->
    {{-- <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}"/> --}}
    <!-- end of page level styles -->
@stop
@section('crumbs')
    <li class="active breadcrumb-item">Roles</li>
@stop
{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('client.role.create') }}" class=" btn btn-success m-r-5">
                            Add Role <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- @include('partials.filters', [
        'noDaterange' => false,
        'filterSearchAnyKey' => true,
    ]) --}}

    <div class="card">
        <div class="card-body" id="project_body">
            <div id="sample_1_wrapper" class="dataTables_wrapper dt-boostrap no-footer">
                <div class="table-responsive">
                    <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Name</th>
                                <th class="sorting wid-25" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                            </tr>
                        </thead>
                         <tbody>
                            @if( $roles )
                                @forelse($roles as $role)
                                <tr>
                                    <td>{{ $role->display_name }}</td>
                                    {{-- @php
                                        $string = substr( strip_tags(htmlspecialchars_decode($role->description)), 0, 30) . ' ...';
                                    @endphp
                                    <td>{{ $string }}</td> --}}
                                    <td>
                                        <a href="{{ route('client.role.edit', $role) }}" title="Edit" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        @if( $role->users->count() == 0 )
                                            <a href="{{ route('client.role.delete', $role) }}" title="Delete" class="btn btn-sm btn-danger confirmDelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        @endif
                                    </td>
                                 </tr>
                                @empty
                                 <tr>
                                    <td colspan="6">No Records Found</td>
                                </tr>
                                @endforelse
                            @endif
                        </tbody>
                    </table>
                    {{-- <div class="text-center">
                        @if(isLengthAware($roles))
                            {!! $roles->appends($filters)->links() !!}
                        @else
                            <div class="alert alert-dark" role="alert">
                                Showing all items
                            </div>
                        @endif
                    </div> --}}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@stop
{{-- page level scripts --}}
