@extends('layouts.fixed_header_client')
{{-- Page title --}}
@section('title')
    Template API
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="active breadcrumb-item">Template API</li>
@stop

{{-- Page content --}}
@section('content')

    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ $title }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('client.task.ots.create') }}" {{( $subAccounts->count() > 1 ) ? 'data-toggle=modal data-target=#sub_account_options' : '' }} class=" btn btn-success m-r-5">
                            Add Task &nbsp;&nbsp;<i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.filters', [
        'campaignFilter' => true,
        'noDaterange' => false,
        'filterByDateToSendCreatedAt' => false,
        'filterSubAccount' => true,
    ])

    <div class="card">
        <div class="card-body" id="project_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Campaign Title</th>
                            <th class="sorting_asc wid-20 {{ (count($subAccounts) <= 1) ? 'hidden' : '' }}" tabindex="0" rowspan="1" colspan="1">Sub Account</th>
                            <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Send After</th>
                            <th class="sorting wid-20" tabindex="0" rowspan="1" colspan="1">App Key</th>
                            <th class="sorting wid-10" data-orderable="false" tabindex="0" rowspan="1" colspan="1">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td>{{ optional($task->campaign)->name }}</td>
                                <td class="{{ (count($subAccounts) <= 1) ? 'hidden' : '' }}">{{ $task->subAccount->account_name }}</td>
                                <td>{{ $task->isSendImmediate() ? 'Send Immediate' : $task->ots_formatted }}</td>
                                <td>{{ $task->task_key }}</td>
                                <td>
                                    <a href="{{route('client.task.ots.edit', $task)}}" title="Edit" class="btn btn-sm btn-info">EDIT - {{ $task->id }}</a>
                                    @if (!$task->hasJobs())
                                    {{-- <a href="{{route('client.task.ots.delete', $task)}}" title="Edit" data-confirm-text="This SMS has job records, delete is not allowed!" class="{{ $task->hasJobs() ? 'confirmNotAllowed' : 'confirmDelete' }} btn btn-sm btn-danger ">DELETE</a> --}}
                                    <a href="{{route('client.task.ots.delete', $task)}}" title="Edit" data-confirm-text="You won't be able to revert this." class="{{ $task->hasJobs() ? 'confirmNotAllowed' : 'confirmDelete' }} btn btn-sm btn-danger ">DELETE</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- <div class="text-center">
                    @if(isLengthAware($tasks))
                        {!! $tasks->appends($filters)->links() !!}
                    @else
                        <div class="alert alert-dark" role="alert">
                            Showing all items
                        </div>
                    @endif
                </div> --}}
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    @include('partials.task.modal-sub-accounts', [
        'url' => route('client.task.ots.create')
    ])
@stop

{{-- @section('scripts')
    <script type="text/javascript">

        $('.btn-delete-item').on('click', function(event) {
            event.preventDefault();

            let deleteUrl = $(this).data('url');

            swal({
                title: 'Are you sure?',
                text: 'You won\'t be able to revert this.',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: 'deleteUrl'
                    })
                    .done(function() {
                        console.log("success");
                    })
                    .fail(function() {
                        console.log("error");
                    })
                    .always(function() {
                        console.log("complete");
                    });
                }
            });

            /* Act on the event */
        });

    </script>
@stop --}}
{{-- page level scripts --}}
