@extends('layouts.fixed_header_client')
{{-- Page title --}}
@section('title')
    Custom API
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="active breadcrumb-item">Custom API</li>
@stop

{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                        <a href="{{ route('client.task.it.create') }}" {{( $subAccounts->count() > 1 ) ? 'data-toggle=modal data-target=#sub_account_options' : '' }} class=" btn btn-success m-r-5">
                            Add Task &nbsp;&nbsp;<i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.filters', [
        'noDaterange' => true,
        'campaignFilter' => true,
        'filterSubAccount' => true,
    ])

    <div class="card">
        <div class="card-body" id="project_body">
            <div class="table-responsive">
                <table data-order='[[ 0, "ASC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc wid-20">Name</th>
                        <th class="sorting_asc wid-20">Campaign</th>
                        <th class="sorting_asc wid-20">Date Created</th>
                        <th class="sorting_asc wid-10">Status</th>
                        <th class="sorting wid-30" data-orderable="false">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($tasks as $task)
                        <tr>
                            <td><a href="{{ route('client.task.it.edit', $task) }}">{{ $task->name ?: 'Untitled' }}</a></td>
                            <td>{{ $task->campaign->name }}</td>
                            <td>@dateFormat($task->created_at)</td>
                            <td>{!! $task->human_status !!}</td>
                            <td>
                                <a href="{{ route('client.task.it.edit', $task) }}" class="btn btn-sm btn-success">EDIT</a>
                                @if( !$task->hasJobs() )
                                {{-- <a href="{{ route('client.task.it.delete', $task) }}" data-confirm-text="This SMS has job records, delete is not allowed!" class="btn btn-sm btn-danger {{ $task->hasJobs() ? 'confirmNotAllowed' : 'confirmDelete' }}">DELETE</a> --}}
                                <a href="{{ route('client.task.it.delete', $task) }}" data-confirm-text="You won't be able to revert this." class="btn btn-sm btn-danger {{ $task->hasJobs() ? 'confirmNotAllowed' : 'confirmDelete' }}">DELETE</a>
                                @endif
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5">No records found.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                {{-- <div class="text-center">
                    @if(isLengthAware($tasks))
                        {!! $tasks->appends($filters)->links() !!}
                    @else
                        <div class="alert alert-dark" role="alert">
                            Showing all items
                        </div>
                    @endif
                </div> --}}
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    @include('partials.task.modal-sub-accounts', [
        'url' => route('client.task.it.create')
    ])
@stop
{{-- page level scripts --}}
