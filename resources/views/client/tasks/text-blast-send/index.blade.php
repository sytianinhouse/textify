@extends('layouts.fixed_header_client')
{{-- Page title --}}
@section('title')
    Text Blast SMS
    @parent
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="active breadcrumb-item">Text Blast SMS</li>
@stop

{{-- Page content --}}
@section('content')
    <div class="card m-b-10">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3 class="m-b-0">
                        {{ isset($title) ? $title : 'Page Header Title' }}
                    </h3>
                </div>
                <div class="col">
                    <div class="btn-group float-right">
                    <a href="{{ route('client.task.tbs.create') }}" {{( $subAccounts->count() > 1 ) ? 'data-toggle=modal data-target=#sub_account_options' : '' }} class=" btn btn-success m-r-5">
                            Add Task &nbsp;&nbsp;<i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.filters', [
        'campaignFilter' => true,
        'filterByDateToSendCreatedAt' => true,
        'filterSubAccount' => true,
    ])

    <div class="card">
        <div class="card-body" id="project_body">
            <div class="table-responsive">
                <table data-order='[[ 1, "DESC" ]]' class="table table-data table-striped table-bordered table-hover no-footer" id="datatable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc wid-20 {{ (count($subAccounts) <= 1) ? 'hidden' : '' }}" tabindex="0" rowspan="1" colspan="1" width="15%">Sub Account</th>
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1" width="15%">Date Created</th>
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1" width="15%">Campaign Title</th>
                            <th width="40%">Statistics</th>
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1" width="15%">Date and Time to send</th>
                            <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1" width="15%">Status</th>
                            <th class="sorting wid-10" data-orderable="false" tabindex="0" rowspan="1" colspan="1" width="20%">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td width="10%" class="{{ (count($subAccounts) <= 1) ? 'hidden' : '' }}">{{ $task->subAccount->account_name }}</td>
                                <td width="10%">{{ $task->created_at->format('M d, Y') }}</td>
                                <td width="13%">{{ $task->campaigns->name }}</td>
                                <td>
                                    <div class="row align-items-center">
                                        <div class="col-lg-3 col-sm-6 text-center">
                                            <div id="{{ 'gauge_'.$task->id.'-1' }}" class="gauge" data-type="jobs_count" data-color="#fdcb6e" data-total-count="{{ $task->jobs_count ? $task->jobs_count : 0 }}" data-count="{{ $task->jobs_count ? $task->jobs_count : 0 }}"></div>
                                            Recipients
                                        </div>
                                        <div class="col-lg-3 col-sm-6 text-center">
                                            <div id="{{ 'gauge_'.$task->id.'-2' }}" class="gauge" data-type="success_jobs_count" data-color="#00b894" data-total-count="{{ $task->jobs_count ? $task->jobs_count : 0 }}" data-count="{{ $task->success_jobs_count ? $task->success_jobs_count : 0 }}"></div>
                                            SMS Sent
                                        </div>
                                        <div class="col-lg-3 col-sm-6 text-center">
                                            <div id="{{ 'gauge_'.$task->id.'-3' }}" class="gauge" data-type="failed_jobs" data-color="#d63031" data-total-count="{{ $task->jobs_count ? $task->jobs_count : 0 }}" data-count="{{ $task->tasksjobsFailed ? $task->tasksjobsFailed->count() : 0 }}"></div>
                                            SMS Failed
                                        </div>
                                        <div class="col-lg-3 col-sm-6 text-center">
                                            <div id="{{ 'gauge_'.$task->id.'-4' }}" class="gauge" data-type="failed_jobs" data-color="#00b894" data-total-count="{{ $task->jobs_count ? $task->jobs_count : 0 }}" data-count="{{ $task->actual_credits_consumed ? $task->actual_credits_consumed : 0 }}"></div>
                                            Credits Used
                                        </div>
                                    </div>
                                </td>
                                <td data-order="{{ $task->date_time_to_send }}">{{ $task->date_time_to_send->toDayDateTimeString() }}</td>
                                <td data-order="{{ $task->status }}">{!! ($task->status == 'completed' ) ? '<span class="btn btn-sm btn-success">Completed</span>' : '<span class="btn btn-sm btn-warning">On Queue</span>' !!}</td>
                                <td style="text-align: center;">
                                    @php
                                        $tasksJobToday = $task->tasks_job_today_count ? $task->tasks_job_today_count : $task->tasksJobToday->count();
                                        $tasksJobTomorrow = $task->tasks_job_tomorrow_count ? $task->tasks_job_tomorrow_count : $task->tasksJobTomorrow->count();
                                        $tasksJobAfterTomorrowTo14thDay = $task->tasks_job_after_tomorrow_to14th_day_count ? $task->tasks_job_after_tomorrow_to14th_day_count : $task->tasksJobAfterTomorrowTo14thDay->count();
                                        $tasksJob15thDayToEndMonth = $task->tasks_job15th_day_to_end_month_count ? $task->tasks_job15th_day_to_end_month_count : $task->tasksJob15thDayToEndMonth->count();
                                        $tasksJobNextMonth = $task->tasks_job_next_month_count ? $task->tasks_job_next_month_count : $task->tasksJobNextMonth->count();
                                        $tasksjobsCompleted = $task->tasksjobs_completed_count ? $task->tasksjobs_completed_count : $task->tasksjobsCompleted->count();
                                        $tasksjobsFailed = $task->tasksjobs_failed_count ? $task->tasksjobs_failed_count : $task->tasksjobsFailed->count();
                                    @endphp
                                    {{-- @if( $task->status == 'completed' || ($tasksJobToday > 0 || $tasksJobTomorrow > 0 || $tasksJobAfterTomorrowTo14thDay > 0 || $tasksJob15thDayToEndMonth > 0 || $tasksJobNextMonth > 0 || $tasksjobsCompleted > 0 || $tasksjobsFailed > 0) ) --}}
                                    @if( $task->status != 'completed' || ($tasksJobToday > 0 || $tasksJobTomorrow > 0 || $tasksJobAfterTomorrowTo14thDay > 0 || $tasksJob15thDayToEndMonth > 0 || $tasksJobNextMonth > 0 || $tasksjobsCompleted > 0 || $tasksjobsFailed > 0) )
                                        <a href="{{route('client.task.tbs.view', [$task])}}" title="View" class="btn btn-sm btn-info">VIEW</a>
                                    @else
                                        <a href="{{route('client.task.tbs.edit', [$task])}}" title="Edit" class="btn btn-sm btn-info">EDIT</a>
                                        @if( !$task->hasJobs() )
                                        {{-- <a href="{{route('client.task.tbs.delete', [$task])}}" title="Edit" class="btn btn-sm btn-danger confirmDelete">DELETE</a> --}}
                                        <a href="{{route('client.task.tbs.delete', [$task])}}" title="Edit" data-confirm-text="You won't be able to revert this." class="btn btn-sm btn-danger {{ $task->hasJobs() ? 'confirmNotAllowed' : 'confirmDelete' }}">DELETE</a>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- <div class="text-center">
                    @if(isLengthAware($tasks))
                        {!! $tasks->appends($filters)->links() !!}
                    @else
                        <div class="alert alert-dark" role="alert">
                            Showing all items
                        </div>
                    @endif
                </div> --}}
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    @include('partials.task.modal-sub-accounts', [
        'url' => route('client.task.tbs.create')
    ])
@stop
{{-- page level scripts --}}
