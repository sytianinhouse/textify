<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"> <!-- CSRF Token -->
    <meta name="base-url" content="{{ url('/') }}">
    <meta name="robots" content="noindex">

    <title> {{ isset($title) ? getAppTitle() . ' Admin | ' . $title : getAppTitle() . ' Admin Dashboard' }}</title>
    <meta name="description" content="{{ issSet('metDesc') }}">
    <meta name="keywords" content="{{ issSet('metKey') }}">

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('images/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('images/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ff0000">
    <meta name="api-base-url" content="{{ url('') }}" />

    {{-- <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/base.css') }}?v=0.0"/> --}}
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/all-plugins.css') }}?v=0.0"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}?v=0.0" />

    <!-- end of global styles-->

    @yield('header_styles')

    @yield('header_scripts')

</head>

<body @yield('body-id') class="fixed_header @yield('body-class')">

<div class="preloader">
    <div class="preloader-img">
        <img src="{{asset('assets/img/loader.gif')}}" alt="loading...">
    </div>
</div>

<div id="wrap">
    <div id="top" class="fixed">

        <!-- .navbar -->
        <nav class="navbar navbar-static-top">
            <div class="container-fluid m-0 position-relative">
                <div class="cs-logo-main-wrapper">
                    <a class="navbar-brand mr-0 cs-logo-wrapper" href="{{ route('admin.index') }}">
                        <h4 class="text-white">
                            <img src="{{asset('images/textify-edited.png')}}" class="img-fluid" alt="logo">
                        </h4>
                    </a>
                </div>
                <div class="menu mr-sm-auto admin">
                        <span class="toggle-left" id="menu-toggle">
                        <i class="fa fa-bars text-white"></i>
                    </span>
                </div>

                <div class="navbar-toggleable-sm m-lg-auto d-none d-lg-flex top_menu" id="nav-content">
                    <ul class="nav navbar-nav flex-row top_menubar">
                        @section('heading-navbar-center')
                    </ul>
                </div>

                <div class="topnav dropdown-menu-right ml-auto">

                    {{-- @include('layouts.partials.heading-notifications', [
                        'display' => false
                    ]) --}}

                    <div class="btn-group">
                        <div class="user-settings no-bg">
                            <button type="button" class="btn btn-default no-bg micheal_btn cs-avatar" id="cs_trigger">
                                <div class="profile-pic" style="background-image:url({{ ( isset($authUser) && $authUser->exists ) ? asset($authUser->profile_img_path) : '' }})"></div>
                            </button>
                            <div class="cs-dropdown {{ ( $authUser->account_type == 'superadmin' ) ? 'superadmin' : 'sub-account' }}">
                                <div class="head-dropdown">
                                    <ul>
                                        <li>
                                            <div class="profile-pic" style="background-image:url({{ ( isset($authUser) && $authUser->exists ) ? asset($authUser->profile_img_path) : '' }})"></div>
                                        </li>
                                        <li>
                                            {{ $authUser->name }}
                                            <ul>
                                                <li>
                                                    <span class="email">{{$authUser->email}}</span>
                                                </li>
                                                <li>
                                                    <a href="{{ route('admin.profile.update',$authUser->id) }}">Client Profile</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="foot-dropdown" style="padding-top: 0px !important">
                                    <a class="cs-logout" href="{{ url('logout') }}">
                                        <i class="fa fa-sign-out"></i>&nbsp;&nbsp;Logout
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </nav>
        <!-- /.navbar -->
        <!-- /.head -->
    </div>

    <!-- /#top -->
    <div class="wrapper">

    @include('layouts.partials.admin-menu')

    <!-- /#left -->
        <div id="content" class="bg-container @yield('content-class')">
            <!-- Content -->
            <div class="outer">
            <div class="inner bg-container">
                    <div class="main-bar stretch-wrapper">
                        <div class="row no-gutters">
                            <div class="col">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ route('admin.index') }}">
                                            <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                                        </a>
                                    </li>
                                    @yield('crumbs')
                                </ol>
                            </div>
                        </div>
                    </div>
                    @yield('content')
                </div>
            </div>
            <!-- Content end -->
        </div>

    </div>

    {{-- @include('partials.form-success-notify') --}}
    {{-- @include('layouts.right_sidebar') --}}

</div>

@yield('before_global_scripts')

<!-- global scripts-->

<!-- end of global scripts-->
<script type="text/javascript" src="{{asset('assets/js/all-plugins.js')}}"></script>
<script type="text/javascript" src="{{asset('js/admin.js')}}"></script>
<!-- page level js -->
@yield('footer_scripts')
@yield('scripts')

@include('partials.form-notifications')

<!-- end page level js -->

</body>
</html>
