<div id="left">
    <div class="menu_scroll">
        <ul id="menu" class="bg-blue dker">
            <li {!! (Request::is('index')? 'class="active"':"") !!}>
                <a href="{{ route('admin.index') }} ">
                    <i class="fa fa-home"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Dashboard</span>
                </a>
            </li>

            <li {!! (Request::is('users')? 'class="active"':"") !!}>
                <a href="{{ route('admin.project.index') }} ">
                    <i class="fa fa-folder"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Projects</span>
                </a>
                <!-- <ul>
                    <li>
                        <a href=" # ">
                            <i class="fa fa-cogs" aria-hidden="true"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Text Blast Settings</span>
                        </a>
                    </li>
                </ul> -->
            </li>

            <li {!! (Request::is('users')? 'class="active"':"") !!}>
                <a href="javascript:;">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Load</span>

                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin.load.index') }} ">
                            <i class="fa fa-plus"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Load</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('admin.load.auto-loads.index') }} ">
                            <i class="fa fa-recycle"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Auto Load</span>
                        </a>
                    </li>
                </ul>

            </li>

            <li {!! (Request::is('users')? 'class="active"':"") !!}>
                <a href="{{ route('admin.credit.index') }} ">
                    <i class="fa fa-money"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Credit</span>
                </a>
            </li>

            <li {!! (Request::is('users')? 'class="active"':"") !!}>
                <a href="{{ route('report-template') }} ">
                    <i class="fa fa-folder"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Report Template</span>
                </a>
            </li>

            <li {!! (Request::is('users')? 'class="active"':"") !!}>
                <a href="javascript:;">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Settings</span>

                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin.general.settings') }}">
                            <i class="fa fa-cog"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;General</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.general.sms-settings') }} ">
                            <i class="fa fa-envelope-o"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;SMS</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li {!! (Request::is('users')? 'class="active"':"") !!}>
                <a href="{{ route('admin.monitoring.index') }} ">
                    <i class="fa fa-line-chart"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Account Monitoring</span>
                </a>
            </li>

            {{--<li {!! (Request::is('index')? 'class="active"':"") !!}>
                <a href="javascript:;">
                    <i class="fa fa-sitemap"></i>
                    <span class="link-title menu_hide">&nbsp; Asset Pages</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('assetPage') }} ">
                            <i class="fa fa-home"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Assets</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('indexPage') }} ">
                            <i class="fa fa-home"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Index Sample Page</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('twoColumnPage') }} ">
                            <i class="fa fa-home"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Create/Edit Sample Page</span>
                        </a>
                    </li>
                </ul>
            </li>--}}

            {{--<li class="dropdown_menu">
                <a href="javascript:;">
                    <i class="fa fa-sitemap"></i>
                    <span class="link-title menu_hide">&nbsp; Multi Level Menu</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="javascript:;">
                            <i class="fa fa-angle-right"></i>
                            &nbsp;Level 1
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu sub-submenu">
                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Level 2
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>--}}
        </ul>
    </div>
</div>
