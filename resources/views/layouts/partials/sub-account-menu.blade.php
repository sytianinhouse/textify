<div id="left">
    <div class="menu_scroll">
        <ul id="menu" class="bg-blue dker">
            @can('access', 'access-dashboard')
            <li>
                <a href="{{ sroute('sub.index') }}">
                    <i class="fa fa-tachometer" aria-hidden="true"></i>
                    <span class="link-title menu_hide">&nbsp; Dashboard</span>
                </a>
            </li>
            @endcan
            @can('access', ['view-text-blast-send-task','view-immediate-task','view-one-time-send-task'])
            <li>
                <a href="javascript:;">
                    <i class="fa fa-th-large"></i>
                    <span class="link-title menu_hide">&nbsp; Send SMS</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    @can('access', 'view-immediate-task')
                    <li>
                        <a href="{{ sroute('sub.task.it.index') }} ">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Custom API
                        </a>
                    </li>
                    @endcan
                    @can('access', 'view-text-blast-send-task')
                    <li>
                        <a href="{{ sroute('sub.task.tbs.index') }} ">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Text Blast
                        </a>
                    </li>
                    @endcan
                    @can('access', 'view-one-time-send-task')
                    <li>
                        <a href="{{ sroute('sub.task.ots.index') }} ">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Template API
                        </a>
                    </li>
                    @endcan
                    <li>
                        <a href="{{ sroute('sub.task.birthday-task.create') }} ">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Birthday SMS
                        </a>
                    </li>
                    @can('access', 'view-booking-task')
                    <li>
                        <a href="{{ sroute('sub.task.booking-task.index') }} ">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Booking / Reservation API
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('access', 'view-contacts')
            <li class="dropdown_menu">
                <a href="" title="Customers">
                    <i class="fa fa-user"></i>
                    <span class="link-title">&nbsp;&nbsp;Contacts</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    @can('access', 'view-contacts')
                        <li>
                            <a href="{{ sroute('sub.customer.index') }}" title="Customers">
                                <i class="fa fa-user"></i>
                                <span class="link-title menu_hide">&nbsp;&nbsp;All Contacts</span>
                            </a>
                        </li>
                    @endcan
                    @can('access', 'view-customer-group')
                        <li class="dropdown_menu">
                            <a href="{{ sroute('sub.customer-group.index') }}" title="Customer Group">
                                <i class="fa fa-users"></i>
                                <span class="link-title menu_hide">&nbsp;&nbsp;Groups</span>
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('access', 'view-campaigns')
                <li class="dropdown_menu">
                    <a href="{{ sroute('sub.camp.index') }}" title="Campaign">
                        <i class="fa fa-bullhorn" aria-hidden="true"></i>
                        <span class="link-title menu_hide">&nbsp;&nbsp;Campaign</span>
                    </a>
                </li>
            @endcan
            @can('access', ['view-credits','view-sms-per-campaign','view-sms-per-month','view-sms-per-day'])
            <li>
                <a href="javascript:;">
                    <i class="fa fa-book"></i>
                    <span class="link-title menu_hide">&nbsp; Reports</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    @can('access', 'view-contact-info')
                    <li>
                        <a href="{{ sroute('sub.report.contact.statistics') }} ">
                            &nbsp; Contact Information
                        </a>
                    </li>
                    @endcan
                    @can('access', 'view-credits')
                    <li>
                        <a href="{{ sroute('sub.report.credit.balance') }}">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Credit Balance
                        </a>
                    </li>
                    @endcan
                    @can('access', ['view-sms-per-campaign','view-sms-per-month','view-sms-per-day'])
                    <li>
                        <a href="javascript:;">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; SMS Statistics <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu sub-submenu">
                            @can('access', 'view-sms-per-campaign')
                            <li>
                                <a href="{{ sroute('sub.report.job.sms.campaign') }} ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; SMS per Campaign
                                </a>
                            </li>
                            @endcan
                            @can('access', 'view-sms-per-month')
                            <li>
                                <a href="{{ sroute('sub.report.job.sms.month') }} ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; SMS per Month
                                </a>
                            </li>
                            @endcan
                            @can('access', 'view-sms-per-day')
                            <li>
                                <a href="{{ sroute('sub.report.job.sms.day') }} ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; SMS per Day
                                </a>
                            </li>
                            @endcan
                            <li>
                                <a href="{{ sroute('sub.report.job.sms.per-sub-account') }} ">
                                    &nbsp; SMS per Sub Account
                                </a>
                            </li>
                            <li>
                                <a href="{{ sroute('sub.report.job.sms.sent') }} ">
                                    &nbsp; SMS Sent
                                </a>
                            </li>
                            <li>
                                <a href="{{ sroute('sub.report.job.sms.pending') }} ">
                                    &nbsp; SMS Pending
                                </a>
                            </li>
                            <li>
                                <a href="{{ sroute('sub.report.job.sms.failed') }} ">
                                    &nbsp; SMS Failed
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            <li class="dropdown_menu">
                <a href="" title="Settings">
                    <i class="fa fa-cog" aria-hidden="true"></i>
                    <span class="link-title">&nbsp;&nbsp;Settings</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    @can('access', 'view-roles')
                        <li class="dropdown_menu">
                            <a href="{{ sroute('sub.role.index') }}" title="Roles">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                <span class="link-title menu_hide">&nbsp;&nbsp;Roles</span>
                            </a>
                        </li>
                    @endcan
                    @can('access', 'view-users')
                        <li class="dropdown_menu">
                            <a href="{{ sroute('sub.user.index') }}" title="Users">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span class="link-title menu_hide">&nbsp;&nbsp;Users</span>
                            </a>
                        </li>
                    @endcan
                    {{--@can('access', 'view-gateway-keys')
                        <li class="dropdown_menu">
                            <a href="{{ sroute('sub.api.index') }}" title="Api Keys">
                                <i class="fa fa-key"></i>
                                <span class="link-title menu_hide">&nbsp;&nbsp;Gateways Keys</span>
                            </a>
                        </li>
                    @endcan--}}
                </ul>
            </li>
        </ul>
    </div>
</div>
