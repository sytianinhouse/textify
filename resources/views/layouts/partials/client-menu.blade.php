<div id="left">
    <div class="menu_scroll">
        <ul id="menu" class="bg-blue dker">
            <li>
                <a href="{{ route('client.index') }}">
                    <i class="fa fa-tachometer" aria-hidden="true"></i>
                    <span class="link-title menu_hide">&nbsp; Dashboard</span>
                </a>
            </li>
            <li class="dropdown_menu">
                <a href="javascript:;">
                    <i class="fa fa-th-large"></i>
                    <span class="link-title menu_hide">&nbsp; Send SMS</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    <li>
                        <a href="{{ route('client.task.it.index') }} ">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Custom API
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('client.task.tbs.index') }} ">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Text Blast
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('client.task.ots.index') }} ">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Template API
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('client.task.birthday-task.create') }} ">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Birthday SMS
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('client.task.booking-task.index') }} ">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Booking / Reservation API
                        </a>
                    </li>
                </ul>
            </li>
            <li class="dropdown_menu">
                <a href="javascript:;" title="Customers">
                    <i class="fa fa-user"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Contacts</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    <li>
                        <a href="{{ route('client.customer.index') }}" title="All Contacts">
                            <i class="fa fa-user"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;All Contacts</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('client.customer-group.index') }}" title="Groups">
                            <i class="fa fa-users"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Groups</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="dropdown_menu">
                <a href="{{ route('client.camp.index') }}" title="Campaign">
                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Campaign</span>
                </a>
            </li>
            <li class="dropdown_menu">
                <a href="javascript:;">
                    <i class="fa fa-book"></i>
                    <span class="link-title menu_hide">&nbsp; Reports</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    <li>
                        <a href="{{ route('client.report.contact.statistics') }} ">
                            &nbsp; Contact Information
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('client.report.credit.balance') }} ">
                            &nbsp; Credit Balance
                        </a>
                    </li>
                    {{--<li>
                        <a href="javascript:;">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Jobs Report <span class="fa arrow"></span>
                        </a>
                    </li>--}}
                    <li>
                        <a href="javascript:;">
                            &nbsp; SMS Statistics <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu sub-submenu">
                            <li>
                                <a href="{{ route('client.report.job.sms.campaign') }} ">
                                    &nbsp; SMS per Campaign
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('client.report.job.sms.month') }} ">
                                    &nbsp; SMS per Month
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('client.report.job.sms.day') }} ">
                                    &nbsp; SMS per Day
                                </a>
                            </li>
                            @if($theSubs->count() > 1)
                                <li>
                                    <a href="{{ route('client.report.job.sms.per-sub-account') }} ">
                                        &nbsp; SMS per Sub Account
                                    </a>
                                </li>
                            @endif
                            <li>
                                <a href="{{ route('client.report.job.sms.sent') }} ">
                                    &nbsp; SMS Sent
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('client.report.job.sms.pending') }} ">
                                    &nbsp; SMS Pending
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('client.report.job.sms.failed') }} ">
                                    &nbsp; SMS Failed
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="dropdown_menu">
                <a href="javascript:;" title="Settings">
                    <i class="fa fa-cog" aria-hidden="true"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Settings</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    <li class="dropdown_menu">
                        <a href="{{ route('client.role.index') }}" title="Users">
                            <i class="fa fa-check" aria-hidden="true"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Roles</span>
                        </a>
                    </li>
                    <li class="dropdown_menu">
                        <a href="{{ route('client.user.index') }}" title="Users">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Users</span>
                        </a>
                    </li>
                    {{-- <li class="dropdown_menu">
                        <a href="{{ route('client.api.index') }}" title="Api Keys">
                            <i class="fa fa-key"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Gateway Keys</span>
                        </a>
                    </li> --}}
                    {{--<li class="dropdown_menu">
                        <a href="{{ route('client.sub.index') }}" title="Sub Accounts">
                            <i class="fa fa-users"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Sub Accounts</span>
                        </a>
                    </li>--}}
                </ul>
            </li>
        </ul>
    </div>
</div>
