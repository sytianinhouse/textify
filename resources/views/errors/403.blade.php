<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="base-url" content="{{ url('/') }}">
        <meta name="robots" content="noindex">

        <title> Access Denied</title>
        <link rel="apple-touch-icon-precomposed" sizes="144x144" hrefj="{{ asset('favicon.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('favicon.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('favicon.png') }}">
        <link rel="apple-touch-icon-precomposed" href="{{ asset('favicon.png') }}">
        <link rel="shortcut icon" href="{{ asset('favicon.png') }}">

        <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/all-plugins.css') }}"/>
        <link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}?v=0.0" />
    </head>
    <body>
        <section class="error-page">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col">
                        <div class="content text-center">
                            <h1>{{ ($exception->getMessage()) ? $exception->getMessage() : '403' }}</h1><br>
                            <h2>Access Denied</h2>
                            @if( $exception->getMessage() == 'Your Account is currently in-active, please contact Administrator!' )
                                <a href="{{ route('/') }}" class="btn btn-primary button-rounded">Return to Login Page</a>
                            @else
                                <button class="btn btn-primary button-rounded" onclick="window.history.back()">Go <span>Home!</span></button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script type="text/javascript" src="{{asset('assets/js/all-plugins.js')}}"></script>
    </body>
</html>
