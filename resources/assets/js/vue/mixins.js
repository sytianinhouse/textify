export default {
	mounted() {
	},
	data : function() {
		return {
			baseUrl: window.location.origin,
            urlPrefix : 'api/',
            clientPrefix : 'client/',
            subPrefix : 'sub/',
            csrf: $('meta[name=csrf-token]').attr("content"),
            uuid: $('meta[name=uuid]').attr("content"),
		}
	},
	computed : {
		api : function() {
			return {
				contactIndex : this.urlPrefix + 'contacts',
				generateContacts : this.urlPrefix + 'contacts/generate-contacts',
				viewContacts : this.urlPrefix + 'contacts/view-contacts',
				getPage : this.urlPrefix + 'contacts/get-page',
				asyncContact : this.urlPrefix + 'contacts/async-contact',
				createCampaign : this.urlPrefix + 'campaign/quick-create',
				getCampaign : this.urlPrefix + 'campaign/get-campaign',
			}
        },
        client: function() {
            return {
                index: this.clientPrefix + 'tasks/booking-task/',
                submitBooking : this.clientPrefix + 'tasks/booking-task/store',
                updateBooking : this.clientPrefix + 'tasks/booking-task/update',
                getJobPage : this.clientPrefix + 'tasks/booking-task/get-page',
            }
        },
        sub: function() {
            return {
                index: `${this.uuid}/${this.subPrefix}tasks/booking-task/`,
                submitBooking : `${this.uuid}/${this.subPrefix}tasks/booking-task/store`,
                updateBooking : `${this.uuid}/${this.subPrefix}tasks/booking-task/update`,
                getJobPage : `${this.uuid}/${this.subPrefix}tasks/booking-task/get-page`,
            }
        }
	},
	methods : {
        getPublicImages: function() {
            if( window.location.origin == 'https://app.textify.ph' ) {
                console.log(window.location.origin);
                return `${window.location.origin}/images/`;
            } else {
                // return `${window.location.origin}/atimar_v2/public/images/`;
                return `${window.location.origin}/textify/public/images/`;
            }
        },
        getAssetsImages: function() {
            if( window.location.origin == 'https://app.textify.ph' ) {
                return `${window.location.origin}/assets/img/`;
            } else {
                // return `${window.location.origin}/atimar_v2/public/assets/img/`;
                return `${window.location.origin}/textify/public/assets/img/`;
            }
        },
        getDomainUrl: function() {
            if( window.location.origin == 'https://app.textify.ph' ) {
                return `${window.location.origin}/`;
            } else {
                // return `${window.location.origin}/atimar_v2/public/`;
                return `${window.location.origin}/textify/public/`;
            }
        },
		countObject : function (collection) {
			return _.size(collection);
		},
		formatDate: function(string, format) {
			return moment(string).format(format);
		},
		numberFormat: function(number, decimal = 2) {
			return parseFloat(number).toFixed(decimal).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
		selectArr: function(data) {
			let getData = data;
			let selectFormattedArray = [];
			getData.forEach((item) => {
				selectFormattedArray.push({
					label:item.label,
					value:item.value,
				});
			});
			return selectFormattedArray;
        },
		createNotif: function(notifMessage, type = 'success') {
            new PNotify({
                text: notifMessage,
                type: (type == 'error') ? 'error' : ( type == 'warning' ? 'warning' : 'success' ),
                modules: {
                    Animate: {
                      animate: true,
                      inClass: 'bounceIn',
                      outClass: 'bounceOut'
                    }
                }
            });
		},
	},
}
