
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


import Router from 'vue-router';
Vue.use(Router);

import Datepicker from "vuejs-datepicker/dist/vuejs-datepicker.esm.js";
import * as lang from "vuejs-datepicker/src/locale";
Vue.component('datepicker', Datepicker);

import RecipentHolder from './components/Contact/Recipient.vue';
Vue.component('recipient-holder', RecipentHolder);

import BookingForm from './components/Task/Booking/FormComponent.vue';
Vue.component('booking-form', BookingForm);

import Multiselect  from 'vue-multiselect';
Vue.component('multiselect', Multiselect );

import CreditBalance from './components/Reports/CreditBalance.vue';
Vue.component('credit-balance', CreditBalance);

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI)

import language from 'element-ui/lib/locale/lang/en.js';
import locale from 'element-ui/lib/locale';
locale.use(language)

import VueDataTables from 'vue-data-tables';
Vue.use(VueDataTables)

import Mixin from './mixins';
Vue.mixin(Mixin);

import { SweetModal, SweetModalTab } from 'sweet-modal-vue';
Vue.use(SweetModal, SweetModalTab);

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue);

import VueFlashMessage from 'vue-flash-message';
Vue.use(VueFlashMessage);

import { async } from 'q';
import { setTimeout } from 'timers';

var _ = require('lodash');

if( $('.v-app').length > 0 ) {
    const app = new Vue({
        el: '.v-app',
    });
}

