/** Recipient Component
 * ===================================== */
import RecipentHolder from './components/Contact/Recipient.vue';
import BookingForm from './components/Task/Booking/FormComponent.vue';

export default
{
	base : '/atimar_v2/public',
	mode : 'history',
	routes: [
		{
			path : '/contact',
			name : 'contact',
			component : RecipentHolder,
			props: true
        },
        {
			path : '/api/campaign/quick-create',
			name : 'campaign',
			component : BookingForm,
			props: true
        },
        {
			path : '/tasks/booking-task',
            name : 'booking-index',
			props: true
		},
		{ path: "*", component: DashboardIndex }
	]
}
