$(document).ready(function () {

	// *************************
	// Token setup for AJAX call, Laravel requirement
	// *************************
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
    });
    $('body').on('click','.campaignModalTrigger', function() {
        setTimeout(function() {
            if($('.modal-backdrop.show').length > 1) {
                $('.modal-backdrop.show').first().remove();
            }
        },500);
    });

    $('body').on('click','.quick-create-campaign', function() {
        setTimeout(function() {
            if($('.modal-backdrop.show').length > 1) {
                $('.modal-backdrop.show').first().remove();
            }
        },500);
    });

    if( $('#send-checkbox-blast').is(":checked") ) {
       $('.send-immediate-status').removeClass('active');
       $('.send-immediate-status').fadeOut('slow');
    } else {
       $('.send-immediate-status').addClass('active');
       $('.send-immediate-status').fadeIn('slow');
    }

    $('body').on('click','#send-checkbox-blast', function() {
        var dateTime = $(this).closest('.form-group').find('.send-immediate-status');
        var value = $(this).val();
        console.log(value);

        if( dateTime.hasClass('active') ) {
            dateTime.removeClass('active');
            dateTime.fadeOut('slow');
        } else {
            dateTime.addClass('active');
            dateTime.fadeIn('slow');
        }
    });

    // $('body').on('click','.close', function() {
    //     $('.modal').modal('hide');
    //     $('body').removeClass('modal-open');
    //     $('.modal-backdrop').remove();
    //     $(".modal-backdrop.in").hide();
    // });
    // $(document).on('shown.bs.modal','#create-quick-campaign', function () {
    // }).modal('show');

    //start area chart
    if( $('#area-chart').length == 1 ) {

        $('#area-chart').each(function() {
            var _data = $(this).data('chart-details');
            var obj = [];
            var key = 0;

            $.each(_data, function(i, v){
                obj[key] = [i, v];
                key++;
            });

            $.plot("#area-chart", [{
                data: obj,
                label: "Credits Consumed per Day",
                color: "#ffb300"
            }], {
                series: {
                    lines: {
                        show: !0,
                        fill: .8
                    },
                    points: {
                        show: !0,
                        radius: 4
                    }
                },
                grid: {
                    borderColor: "#ddd",
                    borderWidth: 1,
                    hoverable: !0
                },
                tooltip: !0,
                tooltipOpts: {
                    content: "%x : %y",
                    defaultTheme: false
                },
                xaxis: {
                    tickColor: "#ddd",
                    mode: "categories"
                },
                yaxis: {
                    tickColor: "#ddd"
                },
                shadowSize: 0
            });
        });

    }

	$('body').on('change','.custom-control-input',function() {
		if($('.date-time-wrapper').hasClass('active')) {
			$('.date-time-wrapper').removeClass('active');
			$('.date-time-wrapper').slideUp();
		} else {
			$('.date-time-wrapper').addClass('active');
			$('.date-time-wrapper').slideDown();
		}
	});

	if( $('.body-dropdown').length > 0 ) {
		const container = document.querySelector('.body-dropdown');
		const ps = new PerfectScrollbar(container);
		var baseUrl = $('meta[name="base-url"]').attr('content', {
			wheelSpeed: 5,
			wheelPropagation: true,
			minScrollbarLength: 20,
			suppressScrollX:true
		});
	}

	$('.dropify').each(function() {
		// var defaultImg = $(this).data('default-file-cs');
		$(this).dropify();
	});

	$('body').on('click','.dropify-clear', function() {
		var has_upload = $(this).closest('.profile-pic').find('.has-upload');
		$(has_upload).val('false');
		$(has_upload).attr('value','false');
    });

	$('.card-same-height').matchHeight();
	$('li :checkbox').on('click', function () {
		var $chk = $(this),
			$li = $chk.closest('li'),
			$ul, $parent;
		if ($li.has('ul')) {
			$li.find(':checkbox').not(this).prop('checked', this.checked)
		}
		do {
			$ul = $li.parent();
			$parent = $ul.siblings(':checkbox');
			if ($chk.is(':checked')) {
				$parent.prop('checked', $ul.has(':checkbox:not(:checked)').length == 0)
			} else {
				$parent.prop('checked', false);
			}
			$chk = $parent;
			$li = $chk.closest('li');
		} while ($ul.is(':not(.someclass)'));
	});

	$(document).mouseup(function (e) {
		var container = $("#cs_trigger");
		if ( ! container.is(e.target) && container.has(e.target).length === 0) {
			$('#cs_trigger').removeClass('active');
			$(".cs-dropdown").fadeOut();
		}
	});

	$('body').on('click','#cs_trigger', function() {
		var _self = $(this);
		if( $(this).hasClass('active') ) {
			$(this).removeClass('active');
			$(this).next('.cs-dropdown').fadeOut();
		} else {
			$(this).addClass('active');
			$(this).next('.cs-dropdown').fadeIn();
		}
	});

	$('.switch-wrapper').on('click','.selected-wrapper', function() {
		if( $(this).hasClass('active') ) {
			$(this).removeClass('active');
			$(this).next('.options-wrapper').removeClass('active');
		} else {
			$(this).addClass('active');
			$(this).next('.options-wrapper').addClass('active');
		}
	});

	// $('body').on('click','#sub-account-modal-submit', function(e) {
 //        e.preventDefault();

 //    	var _selfValue = $('#sub-accoount-modal-form #subAccounts').val();
	// 	var _selfUrl = $('#sub-accoount-modal-form #subAccounts').data('url');
	// 	var _newUrl = _selfUrl.replace("###", _selfValue);

 //        if (_selfValue) {
	// 		window.location.href = _newUrl ;
	// 	} else {
	// 		swal({
	// 			title: 'Error!',
	// 			text: 'Select a Sub Account.',
	// 			type: "warning",
	// 			confirmButtonColor: "#DD6B55",
	// 			confirmButtonText: "Ok!",
	// 			closeOnConfirm: true
	// 		});
	// 	}
 //    });

	$('body').on('click','#sub-account-modal-submit', function() {
		var _selfValue = $('#sub-accoount-modal-form #subAccounts').val();
		var _selfUrl = $('#sub-accoount-modal-form #subAccounts').data('url');

		if (_selfValue) {
			_selfUrl += '?subaccount_id=' + _selfValue;

			window.location.href = _selfUrl ;
		} else {
			swal({
				title: 'Error!',
				text: 'Select a Sub Account.',
				type: "warning",
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ok!",
				closeOnConfirm: true
			});
		}

		// window.location.replace( _selfUrl );

	});


	$('body').on('change','#account_type', function() {
		if( $(this).val() == 'account_user' ) {
			$('#sub_account').prop('required',true);
			$('#sub_account').attr('required',true);
			$('#roles').prop('required',true);
			$('#roles').attr('required',true);
		} else {
			$('#sub_account').prop('required',false);
			$('#sub_account').attr('required',false);
			$('#roles').prop('required',false);
			$('#roles').attr('required',false);
		}
	});
	// *************************
	// Form Plugins
	// *************************

	var selector = '#select-filter-option > .option-hide';
	$('.select-option-filter').on('change', function() {
		var $setValue = $(this).val();
		$('.number-execution').hide();
		$('.customer').val('').trigger('chosen:updated');
		if ( $setValue.length > 0 ){
			var $getValue = [];
			$getValue.push($setValue);
			$.each($getValue, function(i, val){
				if ( val == $setValue ) {
					$(selector).removeClass('show');
					$('.'+val).addClass('show');
				} else {
					$('.'+val).removeClass('show');
					// $("#customer-group option:selected").prop("selected", false)
				}
			});
		} else {
			$('.option-hide').removeClass('show');
		}
		/* if multiple select */
		/*if ( $(this).val().length > 0 ){
			// if multiple
			var $getValue = [];
			$getValue.push($(this).val());
			$.each($getValue, function(i, val){
				$(selector).removeClass('show');
				$.each(val, function (e, arg) {
	    			$('.'+arg).addClass('show');
				});
			});
		} else {
			$(selector).removeClass('show');
		}*/
	});
	$('.shortcode-click').click( function () {
		$('.ul-listing').toggleClass('active');
		setTimeout(function(){
			$('.ul-listing').removeClass('active');
		}, 3000);
	});
	$('.day_filter').datepicker({
		format: 'dd',
		todayHighlight: true,
		autoclose: true,
		orientation: "bottom"
	});
	$('.month_filter').datepicker({
		showOtherMonths: true,
        selectOtherMonths: true,
		todayHighlight: true,
		autoclose: true,
		orientation: "bottom"
	});
	// loop for year
	for (i = new Date().getFullYear(); i > 1900; i--){
	    $('#yearpicker').append($('<option />').val(i).html(i));
	}
	for (i = 1; i < 31; i++){
	    $('#dayfilter').append($('<option />').val(i).html(i));
	}
	$('.datepicker').datepicker({
		format: 'yyyy-mm-dd',
		todayHighlight: true,
		autoclose: true,
		orientation: "bottom",
		popup: {
		    position: "top right",
		    // origin: "top right"
		},
	});

	var date = new Date();
	date.setDate(date.getDate());

	$('#datepicker3').datepicker({
		format: 'yyyy-mm-dd',
		todayHighlight: true,
		autoclose: true,
		orientation: "top",
		startDate: date,
		popup: {
		    position: "top",
		    // origin: "top left"
		},
	});

	$('#customer_listing_wrapper').DataTable( {

        "dom": "<'row'<'col-md-5 col-12'l><'col-md-7 col-12'f>r><'table-responsive't><'row'<'col-md-5 col-12'i><'col-md-7 col-12'p>>",
        "order": [[ 3, "desc" ]],
        "bLengthChange": false,
        // "bFilter": false,
        // "bInfo": false,
    } );

	$(".chzn-select").chosen({ allow_single_deselect: true, height:"80%" });
	$(".chsn-select").chosen({ allow_single_deselect: true, height:"80%" });

    // $(".chosen-container").css({width:'90%'});

	$(".phone").inputmask({"mask": "(02) 999-9999"});
	$(".mobile-phone").inputmask({"mask": "+63 (999)-999-9999"});

	// *************************
	// Tooltips Plugins
	// *************************

	// *************************
	// DataTable Plugin
	// use 'data-order="[[0, 'ASC']]"' @ table element to set the default sorting column
	// use 'data-orderable="false"' @ th element to set if column is sortable
	// use 'data-type="currency"' @ td element to set the data type of the cell eg: date, currency, number
	// *************************
	$(".dataTables_paginate .pagination").addClass("float-right");
	if ( $('.table-data').length ) {
        $('.table-data').each(function() {
            var aoColumns = [],
                $this = $(this);

            var $doIndex,
                $doOrdering,
                options = {
                    responsive: true,
                    paging : false,
					filter : false,
					"lengthMenu": [
						[50, 150, 250,300,450,500, -1],
						[50, 150, 250,300,450,500,"All"] // change per page values here
					],
					"pageLength": 50,
                    "bInfo": false
                };

            if( $(this).data('default-order') ) {
                $doIndex = i;
                $doOrdering = $(this).attr('data-default-order');
            } else {
                $doIndex = 0;
                $doOrdering = 'desc';
            }

            if( $(this).data('no-default-order') ) {
            	options.order = [];
            } else {
            	options.order = [[$doIndex, $doOrdering]];
            }

            if ($this.find('thead > tr').length == 1) {
                $this.find('thead th').each(function(i) {
                    var sType = $(this).data("type"),
                    	oFalse = $(this).data("orderable");
					var lengthMenu = [
						[5, 15, 20, -1],
						[5, 15, 20, "All"] // change per page values here
					];
                    if(sType != '' || sType != undefined){
                    	aoColumns.push({ "type" : sType, "targets" : i, "lengthMenu": lengthMenu });
                    	// if (sType != 'status-sort') {
                    	// 	aoColumns.push({ "type" : sType, "targets" : i });
                    	// } else {
                    	// 	aoColumns.push({ "targets" : i, "render" : 	function ( data, sType, full, meta ) {
                         // alert(sType);
						 //        if (stype == 'status-sort') {
						 //            return $(data).find('span').hasClass('Move Up') ? 1 : 0;
						 //        }  else {
						 //            return data;
						 //        }
					    //   	}
                    	// 	});
                    	// }
					}
                    if (oFalse != undefined){
	                    aoColumns.push({ "orderable" : oFalse, "targets" : i });
	                }
                    options.columnDefs = aoColumns;
                });
            }

            $.fn.dataTable.moment( 'MMM D, YYYY' );
            $this.DataTable(options);
        });

		$('.table-data').parent().parent().css('width', '100%');
    }

    // *************************
	// Sweetalert 2 prompts
	// *************************
	$(".confirm").click(function(e) {
		var msg = $(this).data('confirm') || 'Are you sure?';
		var msgText = $(this).data('confirm-text') || 'You won\'t be able to revert this.';
		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "info",
			showCancelButton: true,
			confirmButtonText: "Yes, Continue!",
			closeOnConfirm: false
		}).then(function(isConfirm) {
			if (isConfirm) {
				window.location = _self.attr("href")
			}
		});
	})

	$(".confirmNotAllowed").click(function(e) {
		var msg = $(this).data('confirm') || 'Action not allowed!';
		var msgText = $(this).data('confirm-text') || 'You won\'t be able to revert this.';
		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "info",
			showCancelButton: false,
			confirmButtonText: "Got it!",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: false
		}).then(function(isConfirm) {
			if (isConfirm) {

			}
		});
	})

	$(".confirmWarning").click(function(e) {
		var msg = $(this).data('confirm') || 'Are you sure?';
		var msgText = $(this).data('confirm-text') || 'You won\'t be able to revert this.';
		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Yes, continue!",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: false
		}).then(function(isConfirm) {
			if (isConfirm) {
				window.location = _self.attr("href")
			}
		});
	})

	$(".confirmDelete").click(function(e) {
		var msg = $(this).data('confirm') || 'Are you sure?';
		var msgText = $(this).data('confirm-text') || 'You won\'t be able to revert this.';
		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}).then(function(isConfirm) {
	      	if (isConfirm) {
				// _self.closest("form").submit();
				window.location.href = _self.attr('href');
	      	}
	    });
    });

	$(".confirmCancelSubmit").click(function(e) {
		var msg = $(this).data('confirm') || 'Are you sure?';
        var value = $(this).data('value');
		var msgText = $(this).data('confirm-text') || 'You can try this again later.';
		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#449d44",
			confirmButtonText: "Yes, Continue!",
			closeOnConfirm: false
		}).then(function(isConfirm) {
	      	if (isConfirm && value != true ) {
                _self.closest("form").submit();
            } else if( isConfirm && value == true ) {
                $('.cancelTask').trigger('click');
            }
	    });
    });

    $('.cancel-text-blast').click('click','.cancel-text-blast',function() {
        var msg = $(this).data('confirm') || 'Are you sure?';
		var value = $(this).data('value');
		var msgText = $(this).data('confirm-text') || 'You can try this again later.';
		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#449d44",
			confirmButtonText: "Yes, Continue!",
			closeOnConfirm: false
		}).then(function(isConfirm) {
            $('.cancelTask').trigger('click');
	    });
    })

	$(".confirmSubmit").click(function(e) {
        var msg = $(this).data('confirm') || 'Are you sure?';
        var msgText = $(this).data('confirm-text') || '';
        $('#save_task_loader').removeClass('hidden');
		e.preventDefault();
		var _self = $(this);
        _self.prop("disabled", true);
		swal({
            title: msg,
			text: msgText,
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#449d44",
			confirmButtonText: "Yes, Continue!",
			preConfirm: false
		}).then(
            function(isConfirm) {
                if (isConfirm) {
                    if( $('.task-form').length == 1 ) {
                        if( $('#campaign').val() == '')  {
                            // $.jGrowl("Campaign Field is required", "error");
                            new PNotify({
                                text: 'Campaign Field is required',
                                type: 'error',
                                addClass: 'error',
                                history: false,
                            });
                            _self.prop("disabled", false);
                            $('#save_task_loader').addClass('hidden');
                            return false;
                        } else {
                            $('.sendTaskBtn').click();
                            swal.disableConfirmButton();
                        }
                    } else {
                        _self.closest("form").submit();
                    }
                }
            },
            function() {
                _self.prop("disabled", false);
                $('#save_task_loader').addClass('hidden');
            }
        );
    });

	$(".campaignCreationCheckSubaccounts").click(function(e) {
		e.preventDefault();
		var _self   = $(this);
		var msg     = $(this).data('confirm') || 'Create a Sub Account';
		var msgText = $(this).data('confirm-text') || '';
		var validateSubaccounts = $(this).data('validate-subaccounts');
		if( validateSubaccounts > 0 ) {
			window.location.href = _self.attr('href');
		} else {
			swal({
				title: msg,
				text: msgText,
				type: "info",
				showCancelButton: true,
				confirmButtonColor: "#449d44",
				confirmButtonText: "Yes, Create One!",
				closeOnConfirm: false
			}).then(function(isConfirm) {
				  if (isConfirm) {
					window.location.href = _self.data('subaccount-url');
				  }
			});
		}
	});


	//FOR DATERANGE PICKER

	$('.input-daterange').daterangepicker({
	        autoApply: true,
	        autoUpdateInput: false,
	        ranges: {
	           'Today': [moment(), moment()],
	           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	           'This Month': [moment().startOf('month'), moment().endOf('month')],
	           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	        }
	    }, function(start, end, label) {

	      var selectedStartDate = start.format('YYYY-MM-DD');
	      var selectedEndDate = end.format('YYYY-MM-DD');

	      $checkinInput = $('#from_date');
	      $checkoutInput = $('#to_date');

	      $checkinInput.val(selectedStartDate);
	      $checkoutInput.val(selectedEndDate);

	      // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
	      // var checkOutPicker = $checkoutInput.data('daterangepicker');
	      // checkOutPicker.setStartDate(selectedStartDate);
	      // checkOutPicker.setEndDate(selectedEndDate);

	      // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
	      // var checkInPicker = $checkinInput.data('daterangepicker');
	      // checkInPicker.setStartDate(selectedStartDate);
	      // checkInPicker.setEndDate(selectedEndDate);
		});

	// SUMMERNOTE WYSIWYG

	$('.summernote-editor').summernote({
		placeholder: 'Your message here...'
	});

	// SELECT2

	$('.select2-select').select2({
		placeholder: "Select Sub Account",
	});

	// Selectize
	$('.selectize-select').selectize();

	$('.selectize-multiple').selectize({
		plugins: ['remove_button'],
		delimiter: ',',
		persist: false,
		create: function(input) {
			return {
				value: input,
				text: input
			}
		}
	});

    $('.gauge').each(function() {
        var _self = $(this);
        new JustGage({
            id: _self.attr('id'),
            relativeGaugeSize: true,
            value: _self.data('count'),
            min: 0,
            max: _self.data('total-count'),
            decimals: 0,
            levelColors: [_self.data('color')],
            valueFontFamily: "Source Sans Pro, sans-serif",
            counter: true
        });
    });

	copyToClipboard = function(element, xclass, type = 'default') {
		var $temp = $("<input>");
		$("body").append($temp);
		if (type == 'field') {
			$temp.val($(element).val()).select();
		} else {
			$temp.val($(element).text()).select();
		}
		document.execCommand("copy");
		$temp.remove();
		$(xclass).text("COPIED");
		setTimeout(function(){
			$('.ul-listing').removeClass('active');
			$(xclass).text("COPY");
		}, 500);
	}


});



