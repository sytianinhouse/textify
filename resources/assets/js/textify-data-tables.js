$(document).ready(function() {
    $('body').bind().on('click','.textify-more-details', function() {
        var _self = $(this);
        if( _self.hasClass('active') ) {
            _self.removeClass('active');
            _self.closest('.get-data').next('.details').removeClass('active');
        } else {
            _self.addClass('active');
            _self.closest('.get-data').next('.details').addClass('active');
        }
    });

    /** Data tables with expand details
     * ===================================== */
    var initTable4 = function() {
        var table = $('#datatable_with_expand_details');

        /* Formatting function for row expanded details */
        function fnFormatDetails(oTable, nTr) {
            var aData = oTable.fnGetData(nTr);
            console.log(aData);
            // nTr.each(function(item) {
            //     // if( item[ctr].data('more-details') == "true" ) {
            //     // }
            // });
            // console.log(nTr);
            // var aData = oTable.fnGetData(nTr);
            // var sOut = '<table>';
            // sOut += '<tr><td>Email:</td><td>' + aData[6] + '</td></tr>';
            // sOut += '<tr><td>Phone:</td><td>' + aData[7] + '</td></tr>';
            // sOut += '<tr><td>Contact Person:</td><td>' + aData[5] + '</td></tr>';
            // sOut += '</table>';
            // return sOut;
        }
        /*
         * Insert a 'details' column to the table
         */
        var nCloneTh = document.createElement('th');
        nCloneTh.className = "table-checkbox";

        var nCloneTd = document.createElement('td');
        nCloneTd.innerHTML = '<span class="row-details row-details-close"></span>';

        table.find('thead tr').each(function() {
            this.insertBefore(nCloneTh, this.childNodes[0]);
        });

        table.find('tbody tr').each(function() {
            this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
        });

        var oTable = table.dataTable({
            "dom": "<'row'<'col-md-5 col-12'l><'col-md-7 col-12'f>r><'table-responsive't><'row'<'col-md-5 col-12'i><'col-md-7 col-12'p>>", // datatable layout without  horizobtal scroll
            "paging"  : false,
            "ordering": false,
            "info"    : false,
            "filter"  : false,
            "columnDefs": [{
                "orderable": false,
                "targets": [0]
            }],
            "order": [
                [1, 'asc']
            ],
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5
        });

        var tableWrapper = $('#datatable_with_expand_details_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        var tableColumnToggler = $('#sample_4_column_toggler');

        /* modify datatable control inputs */
        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

        /* Add event listener for opening and closing details
         * Note that the indicator for showing which row is open is not controlled by DataTables,
         * rather it is done here
         */
        table.on('click', ' tbody td .row-details', function() {
            var nTr = $(this).parents('tr')[0];
            if (oTable.fnIsOpen(nTr)) {
                /* This row is already open - close it */
                $(this).addClass("row-details-close").removeClass("row-details-open");
                oTable.fnClose(nTr);
            } else {
                /* Open this row */
                $(this).addClass("row-details-open").removeClass("row-details-close");
                oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
            }
            return false;
        });

        /* handle show/hide columns*/
        $('input[type="checkbox"]', tableColumnToggler).on("change",function() {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
            return false;
        });
    }
    initTable4();
});
