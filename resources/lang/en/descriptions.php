<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Page Description Text
    |--------------------------------------------------------------------------
    |
    */

    'title' => 'What you can do here?',

    // 'rooms-create' => 'Add the treatment room numbers/names or seat numbers in your organization.',

    // 'users-create' => 'This is the module where you may add different admin users with different access levels.',

    // 'roles-create' => 'Roles dictate the access levels of each admin user. You may create a Manager role that can access everything in the backend or create a Cashier role who can only access the POS module.',

    // 'discounts-create' => 'Add the different discount types that you are offering in your organization such as Senior Citizen, PWD, and so on. You may add percentage based or fixed amount discounts/promos.',

    // 'employees-create' => 'Add the employees you have in your organization. If a certain employee gets commissions from services rendered or products sold, you have to specify his Commission Type. When an employee resigns change her status from Active to Inactive and specify the termination date.',

    // 'deputations-create' => 'Temporarily transfer an employee from one branch to another. Deputed employee will be shown in the schedule roster of the branch she will be transferred at. If you want to transfer an employee permanently, go the to the Employee Profile module and change the employee\'s Branch settings.',

    // 'petty-create' => 'Petty cash is a small amount of cash on hand that is left to the front-desk to be used for purchasing items for small amounts such as buying tissues, meals, supplies, etc. <br><br>In this page you can declare a petty cash expense or you can replenish it.',

    // 'branch' => [
    //     'has_night_shift' => 'Allow night shift on the plotting of employee\'s schedule.'
    // ],

    'reports' => [
        'introduction' => 'To display records, please select date above and click filter.'
    ]
];
