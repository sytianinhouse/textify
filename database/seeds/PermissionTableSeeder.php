<?php
use ATM\Repositories\Permission\Permission;
use ATM\Repositories\Roles\Role;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::truncate();

        $permissions = array(

            /** Dashboard
             * ===================================== */
            [
                'key'      => 'access-dashboard',
                'title'    => 'Access Dashboard',
                'group'    => 'Dashboard',
                'position' => 0
            ],

            /** Gateway Keys
             * ===================================== */
            [
                'key'      => 'create-gateway-keys',
                'title'    => 'Create Gateway Keys',
                'group'    => 'Gateway Keys',
                'position' => 0
            ],

            [
                'key'      => 'update-gateway-keys',
                'title'    => 'Update Gateway Keys',
                'group'    => 'Gateway Keys',
                'position' => 1
            ],

            [
                'key'      => 'view-gateway-keys',
                'title'    => 'View Gateway Keys',
                'group'    => 'Gateway Keys',
                'position' => 2
            ],

            [
                'key'      => 'delete-gateway-keys',
                'title'    => 'Delete Gateway Keys',
                'group'    => 'Gateway Keys',
                'position' => 3
            ],

            /** Role
             * ===================================== */
            [
                'key'      => 'create-roles',
                'title'    => 'Create Roles',
                'group'    => 'Roles',
                'position' => 0
            ],

            [
                'key'      => 'update-roles',
                'title'    => 'Update Roles',
                'group'    => 'Roles',
                'position' => 1
            ],

            [
                'key'      => 'view-roles',
                'title'    => 'View Roles',
                'group'    => 'Roles',
                'position' => 2
            ],

            [
                'key'      => 'delete-roles',
                'title'    => 'Delete Roles',
                'group'    => 'Roles',
                'position' => 3
            ],

            /** Users
             * ===================================== */
            [
                'key'      => 'create-users',
                'title'    => 'Create Users',
                'group'    => 'Users',
                'position' => 0
            ],

            [
                'key'      => 'update-users',
                'title'    => 'Update Users',
                'group'    => 'Users',
                'position' => 1
            ],

            [
                'key'      => 'view-users',
                'title'    => 'View Users',
                'group'    => 'Users',
                'position' => 2
            ],

            [
                'key'      => 'delete-users',
                'title'    => 'Delete Users',
                'group'    => 'Users',
                'position' => 3
            ],

            /** Contacts
             * ===================================== */
            [
                'key'      => 'create-contacts',
                'title'    => 'Create Contacts',
                'group'    => 'Contacts',
                'position' => 0
            ],

            [
                'key'      => 'update-contacts',
                'title'    => 'Update Contacts',
                'group'    => 'Contacts',
                'position' => 1
            ],

            [
                'key'      => 'view-contacts',
                'title'    => 'View Contacts',
                'group'    => 'Contacts',
                'position' => 2
            ],

            [
                'key'      => 'delete-contacts',
                'title'    => 'Delete Contacts',
                'group'    => 'Contacts',
                'position' => 3
            ],

            /** Customer Group
             * ===================================== */
            [
                'key'      => 'create-customer-group',
                'title'    => 'Create Customer Groups',
                'group'    => 'Customer Groups',
                'position' => 0
            ],

            [
                'key'      => 'update-customer-group',
                'title'    => 'Update Customer Groups',
                'group'    => 'Customer Groups',
                'position' => 1
            ],

            [
                'key'      => 'view-customer-group',
                'title'    => 'View Customer Groups',
                'group'    => 'Customer Groups',
                'position' => 2
            ],

            [
                'key'      => 'delete-customer-group',
                'title'    => 'Delete Customer Groups',
                'group'    => 'Customer Groups',
                'position' => 3
            ],

            /** Campaigns
             * ===================================== */
            [
                'key'      => 'create-campaigns',
                'title'    => 'Create Campaigns',
                'group'    => 'Campaigns',
                'position' => 0
            ],

            [
                'key'      => 'update-campaigns',
                'title'    => 'Update Campaigns',
                'group'    => 'Campaigns',
                'position' => 1
            ],

            [
                'key'      => 'view-campaigns',
                'title'    => 'View Campaigns',
                'group'    => 'Campaigns',
                'position' => 2
            ],

            [
                'key'      => 'delete-campaigns',
                'title'    => 'Delete Campaigns',
                'group'    => 'Campaigns',
                'position' => 3
            ],

            /** Text Blast Send Task
             * ===================================== */
            [
                'key'      => 'create-text-blast-send-task',
                'title'    => 'Create Text Blast Send Task',
                'group'    => 'Text Blast Send Task',
                'position' => 0
            ],

            [
                'key'      => 'update-text-blast-send-task',
                'title'    => 'Update Text Blast Send Task',
                'group'    => 'Text Blast Send Task',
                'position' => 1
            ],

            [
                'key'      => 'view-text-blast-send-task',
                'title'    => 'View Text Blast Send Task',
                'group'    => 'Text Blast Send Task',
                'position' => 2
            ],

            [
                'key'      => 'delete-text-blast-send-task',
                'title'    => 'Delete Text Blast Send Task',
                'group'    => 'Text Blast Send Task',
                'position' => 3
            ],

            /** One Time Send Task
             * ===================================== */
            [
                'key'      => 'create-one-time-send-task',
                'title'    => 'Create One-Time Send Task',
                'group'    => 'One-Time Send Task',
                'position' => 0
            ],

            [
                'key'      => 'update-one-time-send-task',
                'title'    => 'Update One-Time Send Task',
                'group'    => 'One-Time Send Task',
                'position' => 1
            ],

            [
                'key'      => 'view-one-time-send-task',
                'title'    => 'View One-Time Send Task',
                'group'    => 'One-Time Send Task',
                'position' => 2
            ],

            [
                'key'      => 'delete-one-time-send-task',
                'title'    => 'Delete One-Time Send Task',
                'group'    => 'One-Time Send Task',
                'position' => 3
            ],

            /** Reports
             * ===================================== */
            [
                'key'      => 'view-credits',
                'title'    => 'View Credits',
                'group'    => 'Credits',
                'position' => 0
            ],

            [
                'key'      => 'view-sms-per-campaign',
                'title'    => 'View SMS per Campaign',
                'group'    => ' SMS Campaign',
                'position' => 1
            ],

            [
                'key'      => 'view-sms-per-month',
                'title'    => 'View SMS per Month',
                'group'    => ' SMS per Month',
                'position' => 2
            ],

            [
                'key'      => 'view-sms-per-Day',
                'title'    => 'View SMS per Day',
                'group'    => ' SMS per Day',
                'position' => 3
            ],

            /** Immediate Task
             * ===================================== */
            [
                'key'      => 'create-immediate-task',
                'title'    => 'Create Immediate Task',
                'group'    => 'Immediate Task',
                'position' => 0
            ],

            [
                'key'      => 'update-immediate-task',
                'title'    => 'Update Immediate Task',
                'group'    => 'Immediate Task',
                'position' => 1
            ],

            [
                'key'      => 'view-immediate-task',
                'title'    => 'View Immediate Task',
                'group'    => 'Immediate Task',
                'position' => 2
            ],

            [
                'key'      => 'delete-immediate-task',
                'title'    => 'Delete Immediate Task',
                'group'    => 'Immediate Task',
                'position' => 3
            ],

            /** Booking Task
             * ===================================== */
            [
                'key'      => 'create-booking-task',
                'title'    => 'Create Booking Task',
                'group'    => 'Booking Task',
                'position' => 0
            ],

            [
                'key'      => 'update-booking-task',
                'title'    => 'Update Booking Task',
                'group'    => 'Booking Task',
                'position' => 1
            ],

            [
                'key'      => 'view-booking-task',
                'title'    => 'View Booking Task',
                'group'    => 'Booking Task',
                'position' => 2
            ],

            [
                'key'      => 'delete-booking-task',
                'title'    => 'Delete Booking Task',
                'group'    => 'Booking Task',
                'position' => 3
            ],

            /** Birthday Task
             * ===================================== */
            [
                'key'      => 'create-birthday-task',
                'title'    => 'Create Birthday Task',
                'group'    => 'Birthday Task',
                'position' => 0
            ],

            [
                'key'      => 'update-birthday-task',
                'title'    => 'Update Birthday Task',
                'group'    => 'Birthday Task',
                'position' => 1
            ],

            [
                'key'      => 'view-birthday-task',
                'title'    => 'View Birthday Task',
                'group'    => 'Birthday Task',
                'position' => 2
            ],

            [
                'key'      => 'delete-birthday-task',
                'title'    => 'Delete Birthday Task',
                'group'    => 'Birthday Task',
                'position' => 3
            ],

        );


        \DB::table('permissions')->insert($permissions);
        \DB::table('role_permissions')->truncate();

    }
}
