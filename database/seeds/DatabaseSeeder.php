<?php

use ATM\Repositories\Settings\SiteSettings;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
		$this->call(TaskTypeSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(ProvincesTableSeeder::class);
        $this->call(PermissionTableSeeder::class);

        //Seed the countries
        // $this->call('CitiesSeeder');
        $this->call('CitiesTableSeeder');
        $this->call('ProvincesTableSeeder');
        $this->call('CountriesSeeder');
        $this->call('PermissionTableSeeder');
		$this->call('ApiKeysTableSeeder');
        $this->command->info('Seeded the countries!');

        $settings = new SiteSettings();
        $settings->save();
    }
}
