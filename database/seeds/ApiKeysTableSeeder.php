<?php

use ATM\Repositories\ApiKey\ApiKey;
use Illuminate\Database\Seeder;

class ApiKeysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	ApiKey::truncate();

        $globeLabs = new ApiKey();
        $globeLabs->name = 'Globelabs';
		$globeLabs->api_key = 'globelabs';
		$globeLabs->save();

		$promotexter = new ApiKey();
		$promotexter->name = 'Promotexter';
		$promotexter->api_key = 'promotexter';
		$promotexter->save();

		$sytian = new ApiKey();
		$sytian->name = 'Sytian';
		$sytian->api_key = 'sytian';
		$sytian->save();
    }
}
