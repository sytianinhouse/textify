<?php

use ATM\Repositories\Roles\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        Role::truncate();

        /*$role = new Role();
        $role->name = 'super_admin';
        $role->display_name = 'Super Admin';
        $role->save();*/
        
        $suUser = new User();
        $suUser->first_name = 'Super Admin';
        $suUser->email = 'admin@email.com';
        $suUser->account_type = 'superadmin';
        $suUser->username = 'admin';
        $suUser->password = bcrypt('administrator');
        //$suUser->role()->attach($role);
        $suUser->save();

    }
}
