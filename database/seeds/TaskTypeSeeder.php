<?php

use Illuminate\Database\Seeder;

use ATM\Repositories\Tasks\TaskType;

class TaskTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TaskType::truncate();

        $textBlast = new TaskType();
        $textBlast->name = 'sms_blast';
		$textBlast->title = 'SMS Blast';
		$textBlast->save();

		$oneTimeSMS = new TaskType();
		$oneTimeSMS->name = 'one_time_send';
		$oneTimeSMS->title = 'One Time Send';
		$oneTimeSMS->save();

		$bithdateSms = new TaskType();
		$bithdateSms->name = 'birthdate_sms';
		$bithdateSms->title = 'Birthdate SMS';
		$bithdateSms->save();

		$nonVisit = new TaskType();
		$nonVisit->name = 'non_visit_sms';
		$nonVisit->title = 'Non Visit SMS';
		$nonVisit->save();

		$funnelSms = new TaskType();
		$funnelSms->name = 'funnel_sms';
		$funnelSms->title = 'Funnel SMS';
		$funnelSms->save();

		$appointmentSms = new TaskType();
		$appointmentSms->name = 'appointment_sms';
		$appointmentSms->title = 'Appointment SMS';
		$appointmentSms->save();

		$immediateSms = new TaskType();
		$immediateSms->name = 'immediate_sms';
		$immediateSms->title = 'Immediate SMS';
		$immediateSms->save();
    }
}
