<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSourceColumnInJobAfterTomorrowTo14thDayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_after_tomorrow_to_14th_day', function (Blueprint $table) {
            $table->integer('customer_id')->nullable()->after('id');
            $table->string('source')->nullable()->after('client_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_after_tomorrow_to_14th_day', function (Blueprint $table) {
            $table->dropColumn('customer_id');
            $table->dropColumn('source');
        });
    }
}
