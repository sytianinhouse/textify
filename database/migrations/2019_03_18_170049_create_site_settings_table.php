<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('system_email')->nullable();
            $table->string('system_name')->nullable();
			$table->integer('sms_character_limit')->nullable();
			// $table->decimal('credit_conversion', 8, 2)->nullable();
			$table->boolean('sending_disabled')->default(0)->nullable();
			$table->boolean('under_maintenance')->default(0)->nullable();
			$table->tinyInteger('jobs_audit_log_disabled')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_settings');
    }
}
