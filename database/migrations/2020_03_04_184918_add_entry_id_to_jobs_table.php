<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEntryIdToJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('jobs_completed', function (Blueprint $table) {
			$table->integer('entry_id')->nullable()->after('client_id');
		});
		Schema::table('jobs_failed', function (Blueprint $table) {
			$table->integer('entry_id')->nullable()->after('client_id');
		});

        Schema::table('job_today', function (Blueprint $table) {
			$table->integer('entry_id')->nullable()->after('client_id');
        });

		Schema::table('job_tomorrow', function (Blueprint $table) {
			$table->integer('entry_id')->nullable()->after('client_id');
		});

		Schema::table('job_next_month', function (Blueprint $table) {
			$table->integer('entry_id')->nullable()->after('client_id');
		});

		Schema::table('job_after_tomorrow_to_14th_day', function (Blueprint $table) {
			$table->integer('entry_id')->nullable()->after('client_id');
		});

		Schema::table('job_15th_day_to_end_month', function (Blueprint $table) {
			$table->integer('entry_id')->nullable()->after('client_id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('jobs_completed', function (Blueprint $table) {
			$table->dropColumn('entry_id');
		});
		Schema::table('jobs_failed', function (Blueprint $table) {
			$table->dropColumn('entry_id');
		});

        Schema::table('job_today', function (Blueprint $table) {
            $table->dropColumn('entry_id');
        });

		Schema::table('job_tomorrow', function (Blueprint $table) {
			$table->dropColumn('entry_id');
		});

		Schema::table('job_next_month', function (Blueprint $table) {
			$table->dropColumn('entry_id');
		});

		Schema::table('job_after_tomorrow_to_14th_day', function (Blueprint $table) {
			$table->dropColumn('entry_id');
		});

		Schema::table('job_15th_day_to_end_month', function (Blueprint $table) {
			$table->dropColumn('entry_id');
		});
    }
}
