<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJob15thDayToEndMonth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('job_15th_day_to_end_month', function (Blueprint $table) {
            $table->increments('id');
            $table->text('customer_name')->nullable();
            $table->text('customer_number')->nullable();
            $table->text('gateway_key')->nullable();
            $table->integer('campaign_id')->nullable();
            $table->integer('credits_consumed')->nullable();
            $table->dateTime('when_to_send')->nullable();
            $table->longText('message')->nullable();
            $table->integer('task_id')->nullable();
            $table->integer('subaccount_id')->nullable();
            $table->integer('client_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('job_15th_day_to_end_month');
    }
}
