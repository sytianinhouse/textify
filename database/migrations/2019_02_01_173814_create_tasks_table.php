<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->integer('subaccount_id');
            $table->integer('entry_id')->nullable();
            $table->integer('campaign_id');
            $table->date('date_to_send')->nullable();
            $table->time('time_to_send')->nullable();
            $table->integer('active')->default(1);
            $table->integer('success_jobs_count')->default(0);
            $table->integer('jobs_count')->default(0);
            $table->longText('message')->nullable();
            $table->integer('task_type_id');
            $table->string('task_key')->unique()->nullable();
            $table->string('gateway_key')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task');
    }
}
