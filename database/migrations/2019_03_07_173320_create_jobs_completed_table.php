<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsCompletedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs_completed', function (Blueprint $table) {
            $table->increments('id');
            $table->text('customer_name')->nullable();
            $table->text('customer_number')->nullable();
            $table->text('gateway_key')->nullable();
            $table->integer('credits_consumed')->nullable();
            $table->dateTime('when_to_send')->nullable();
            $table->longText('message')->nullable();
			$table->longText('sms_response')->nullable();
			$table->integer('campaign_id')->nullable();
            $table->integer('task_id')->nullable();
            $table->integer('subaccount_id')->nullable();
            $table->integer('client_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs_completed');
    }
}
